@extends('layouts.home')
<style type="text/css">
    #triple{
      /* width:760px; */
      margin-bottom:20px;
      overflow:hidden;
      /* border-top:1px solid #ccc; */
    }
    #triple li{
      line-height:2.0em;
      border-bottom:1px solid #f0f0f0;
      float:left;
      display:inline;
    }
    #double li  { width:50%;}
    #triple li  { width:33.333%; }
    #quad li    { width:25%; } 
    #six li     { width:16.666%; }
	.bottom-strip.p-both.white-bg{border-bottom: 4px solid #e4f0fa;}
	#triple li:before{content:url();}
	.progress{height:10px !important;}
</style>
@section('content')
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')									   
<!--==================================
	 Header parts ends here
  ==================================-->
<!--==================================
              service Quick links starts here
        ==================================--> 
<div class="first-section ptb">
		<div class="container">
			<div class="row" style="padding: 20px 0px;">
				<h3>All Reviews</h3>
				<hr>
				<div class="col-md-4" style="border-right:1px solid #eee; padding-right:0px !important;">					
					<div style="height:300px; width:300px; border-radius:50%;text-align:center; background:#fafafa; border:1px solid #eee;">	
					<span style="font-size:35px; padding:0px 94px; line-height:300px; color:#4a4a4a;">{{ round($profile->rating_cache,1) }}/5</span>
					</div>
					<p style="width:300px; font-size:22px; text-align:center; margin-top:20px;">{{ $profile->rating_count }} {{ str_plural('review',$profile->rating_count)}}</p>
				</div>
				
				<div class="col-md-4"  style="padding:25px 0px 20px 70px !important;">
				    <div class="row rows-01" style="margin-bottom:32px;">
						<div class="col-md-2"  style="padding-right:0px !important;">
						<span style="float:left; font-size:25px; line-height:1px;margin-top:-10px;">5<i class="fa fa-star"style="margin-left:20px; color:#ffd451;"></i></span>
						</div>
						
						<div class="col-md-8" style="padding-left:0px !important;">
							<div class="progress">
								<div class="progress-bar bg-success" role="progressbar" style="width:{{ ($star_count==0?0:$profile->rating_five_star_count/$star_count)*100 }}%; height:10px;  background-color:#00bfa5;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						
						<div class="col-md-2" style="padding-left:30px !important;">
						   <span style="font-size:20px; line-height:1px;">{{ $profile->rating_five_star_count }}</span>
						</div>
					</div>
					 <div class="row rows-01" style="margin-bottom:32px;">
						<div class="col-md-2"  style="padding-right:0px !important;">
						<span style="float:left; font-size:25px; line-height:1px;margin-top:-10px;">4<i class="fa fa-star"style="margin-left:20px; color:#ffd451;"></i></span>
						</div>
						
						<div class="col-md-8" style="padding-left:0px !important;">
							<div class="progress">
								<div class="progress-bar bg-success" role="progressbar" style="width:{{ ($star_count==0?0:$profile->rating_four_star_count/$star_count)*100 }}%; height:10px; background-color:#4db6ac;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						
						<div class="col-md-2" style="padding-left:30px !important;">
						   <span style="font-size:20px; line-height:1px;">{{ $profile->rating_four_star_count }}</span>
						</div>
					</div>
					 <div class="row rows-01" style="margin-bottom:32px;">
						<div class="col-md-2"  style="padding-right:0px !important;">
						<span style="float:left; font-size:25px; line-height:1px;margin-top:-10px;">3<i class="fa fa-star"style="margin-left:20px; color:#ffd451;"></i></span>
						</div>
						
						<div class="col-md-8" style="padding-left:0px !important;">
							<div class="progress">
								<div class="progress-bar bg-success" role="progressbar" style="width:{{ ($star_count==0?0:$profile->rating_three_star_count/$star_count)*100 }}%; height:10px; background-color:#f57c00;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						
						<div class="col-md-2" style="padding-left:30px !important;">
						   <span style="font-size:20px; line-height:1px;">{{ $profile->rating_three_star_count }}</span>
						</div>
					</div>
					 <div class="row rows-01" style="margin-bottom:32px;">
						<div class="col-md-2"  style="padding-right:0px !important;">
						<span style="float:left; font-size:25px; line-height:1px;margin-top:-10px;">2<i class="fa fa-star"style="margin-left:20px; color:#ffd451;"></i></span>
						</div>
						
						<div class="col-md-8" style="padding-left:0px !important;">
							<div class="progress">
								<div class="progress-bar bg-success" role="progressbar" style="width:{{ ($star_count==0?0:$profile->rating_two_star_count/$star_count)*100 }}%; height:10px; background-color:#f57c00;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						
						<div class="col-md-2" style="padding-left:30px !important;">
						   <span style="font-size:20px; line-height:1px;">{{ $profile->rating_two_star_count }}</span>
						</div>
					</div>
					 <div class="row rows-01">
						<div class="col-md-2"  style="padding-right:0px !important;">
						<span style="float:left; font-size:25px; line-height:1px;margin-top:-10px;">1<i class="fa fa-star"style="margin-left:20px; color:#ffd451;"></i></span>
						</div>
						
						<div class="col-md-8" style="padding-left:0px !important;">
							<div class="progress">
								<div class="progress-bar bg-success" role="progressbar" style="width:{{ ($star_count==0?0:$profile->rating_one_star_count/$star_count)*100 }}%; height:10px; background-color:#ff5722;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>
						
						<div class="col-md-2" style="padding-left:30px !important;">
						   <span style="font-size:20px; line-height:1px;">{{ $profile->rating_one_star_count }}</span>
						</div>
					</div>
					 
				</div>
				<div class="col-md-4">
				
				</div>
			</div>
			
		</div>
		@if(count($reviews)>0)
		@foreach($reviews as $review)
		<div class="col-xs-12 col-md-9 loop-search loop-search1" >
			<div class="col-xs-12 loo-aftr" style="background: white;padding: 5px;margin-bottom:5px;">
				<div class="col-xs-12 col-md-8">
					<div class="col-xs-4  col-md-3 thumnail">
						@if($review->user->user->image && !$review->post_anonymous)
							<img src="{{ $review->user->user->image }}" class="img-circle" style="border: 1px solid #f0f0f0;">
						@else
							<img src="{{ asset('public/img/doc_placeholder.png') }}" class="img-circle" style="border: 1px solid #f0f0f0;">
						@endif
					</div>
					<div class="col-xs-12 col-md-8 padding-0" style="margin-top:30px;">
						<h4>{{ $review->post_anonymous?'Anonymous user':$review->user->user->name }}</h4>
					</div>
					<div class="col-xs-12 ">
						<div class="clearfix" style="margin-top:20px;"></div>
						<h4>{{ $review->title }}</h4>
						<p class="color-grey">{!! $review->comment !!}</p>
						<span id="thums-up-count-review{{ $review->id }}">{{ $review->helpfullVote()->where('vote',1)->get()->count() }}</span> <i rel="{{ $review->id }}" style="cursor:pointer;" class="fa fa-thumbs-up text-muted thums-up"></i>
						<i rel="{{ $review->id }}" style="cursor:pointer;" class="fa fa-thumbs-down text-muted thums-down"></i> <span id="thums-down-count-review{{ $review->id }}">{{ $review->helpfullVote()->where('vote',0)->get()->count() }}</span>
					</div>
				</div>
				<div class="col-xs-12 col-md-4 padding-0">
					<div class="col-xs-12 col-md-12 text-right">
						<span>Overall Rating</span>
						<div class="col-xs-12 padding-0">
							<span class="color-grey">{{ $review->rating }}<span> 
							@for ($i=1; $i <= 5 ; $i++)
								<span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
							@endfor
						</div>
						@if($review->reviewDetail)
						<!-- indivisual rating part START-->
						@foreach($review->reviewDetail as $temp)
						<div class="col-xs-12 padding-0">
							<span class="color-grey">{{ $temp->param->name }}<span> 
							@for ($i=1; $i <= 5 ; $i++)
								<span class="glyphicon glyphicon-star{{ ($i <= $temp->rate) ? '' : '-empty'}}"></span>
							@endfor
						</div> 
						@endforeach
						@endif
						<!-- indivisual rating part END-->
						<div class="clearfix"></div>
						
					</div>
				</div>
			</div>
		</div>
		@endforeach
		@endif
</div>	
</div> 
<!-- =================================
		APP DOWNLOAD PART END 
	================================== -->
@include('layouts.footer')
@if (Auth::check())
<script>
     $(function(){
        $(".thums-down").click(function(){ 
            var uid = "{{ Auth::user()->id }}";
            var csrf_field = "{{ csrf_token() }}";
            var id = $(this).attr("rel");
            $.ajax({
                type: "POST",
                url: "{{ url('ajax/thusm-down') }}",
                data: {'user_id':uid, 'review_id':id, 'voting':0,'_token':csrf_field},
                success: function(result){ 
                    if(result.status == 1){
                        $("#thums-down-count-review" + id).html(result.down_count);
                    }
                },
                error:function(result){
                    console.log(result);
                }
            });
        });
        $(".thums-up").click(function(){
            var uid = "{{ Auth::user()->id }}";
            var csrf_field = "{{ csrf_token() }}";
            var id = $(this).attr("rel");
            $.ajax({
                type: "POST",
                url: "{{ url('ajax/thums-up') }}",
                data: {'user_id':uid, 'review_id':id, 'voting':1,'_token':csrf_field},
                success: function(result){ 
                   if(result.status == 1){
                        $("#thums-up-count-review" + id).html(result.up_count);
                   }
                },
                error:function(result){
                    console.log(result);
                }
            });
        });
        $('i[data-toggle="tooltip"]').tooltip();
        $("#bookmark").click(function(){
            var uid = "{{ Auth::user()->id }}";
            var csrf_field = "{{ csrf_token() }}";
            var id = $("#bookmark").attr("rel");
            $.ajax({
                type: "POST",
                url: "{{ url('ajax/add_bookmark') }}",
                data: {'user_id':uid, 'profile_id':id, '_token':csrf_field},
                success: function(result){ 
                     console.log(result);
                   if(result.status == 1){
                        $("#bookmark-msg").html(result.message);
                        $('#bookmark-msg').show();
                   }
                   setTimeout(function () {$('#bookmark-msg').hide();},4000);
                },
                error:function(result){
                    console.log(result);
                }
            });
        });
    });       
</script>
@endif
@endsection
