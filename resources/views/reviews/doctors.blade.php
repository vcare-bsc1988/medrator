@extends('layouts.home')

@section('content')
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')									   
<!--==================================
	 Header parts ends here
  ==================================-->
<!--==================================
	Banner parts starts here
  ==================================-->
<!--==================================
              Banner parts ends here
        ==================================-->
<!--==================================
              service Quick links starts here
        ==================================--> 
{{Form::open(['url' => url()->full(), 'method' => 'get', 'style'=>'padding-left:0px;', 'id'=>'filterForm', 'class' => 'search-form-0 col-xs-12', 'rol'=>'search'])}}
<div class="first-section ptb">
        <div class="container">
                <div class="row">
                        <section for="side-filter">
                                @include('search.filter-doctor')  
                                <div class="col-xs-12 col-md-9 padding-0" style="background:#e4f0fa;">
                                        <div class="col-md-6 col-xs-12 pull-left">
											<h5 style="padding:22px 0px;">{{ $paginator['result']->getNumFound() }} matches found for:</h5>
                                        </div>
										<div class="col-md-6 col-xs-12 pull-right text-right"  style="padding:10px;margin-top:0px;">
                                            <label>Sort by: </label>
                                            <select class='sort-select' name="filters[sorting]" id="sorting">
                                                <option value="distance-asc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='distance-asc')?'selected':''?> >Distance - Low to High</option>
                                                <option value="distance-desc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='distance-desc')?'selected':''?> >Distance - High to Low</option>
                                                <option value="ratings" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='ratings')?'selected':''?> >Ratings</option>
                                                <option value="ratings-count" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='ratings-count')?'selected':''?> >No of Ratings</option>
                                                <option value="fee-asc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='fee-asc')?'selected':''?> >Fee - Low to High</option>
                                                <option value="fee-desc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='fee-desc')?'selected':''?> >Fee - High to Low</option>
                                                <option value="experience" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='experience')?'selected':''?> >Experience</option>
                                            </select>
                                            <span class="caret caret1"></span>
                                        </div>
                                        @if(!empty($results))
                                        @foreach($results as $result)
                                        @if($result['role']==2)
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 loop-search" >
                                                    <div class="col-xs-4  col-md-2 thumnail">
                                                        @if($result['image'])
                                                            <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                                        @else
                                                            <img src="{{ asset("public/img/doc_placeholder.png") }}" class="img-circle" width="95">
                                                        @endif
                                                        
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <h4><a href="{{ url('doctor') }}/{{ $result['name_slug'] }}">{{$result['name'] }}</a></h4>
                                                        <p class="color-grey">{{ $result['address_one'] }}</p>
                                                        <p>                        
                                                            @if(isset($result['speciality']) && !empty($result['speciality']))
                                                            @for($i=0; ($i < count($result['speciality']) && $i<2); $i++)
																<span class="specility">
																	@if($i>0) @endif {{ $result['speciality'][$i] }} 
																</span>
                                                            @endfor
                                                            @endif
                                                            <span class="color-grey">{{ $result['experience'] }} yrs exp.</span></p>
                                                        <p class="color-grey">Consulting Fee - <span><i class="fa fa-inr"></i>{{ $result['consultation_fee'] }}</span></p>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 text-right">
                                                        <!--<p class="color-grey">4.5 <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></span></p>-->
                                                        @if( number_format($result['rating']) !=0)
														<p class="color-grey">
                                                            @for ($i=1; $i <= 5 ; $i++)
                                                              <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                                            @endfor
                                                            {{ number_format($result['rating'], 1) }}
                                                        </p>
                                                        <p class="color-grey">
                                                                {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                                        </p>
														@else
															<p>No Reviews</p>
														@endif
                                                        <p class="color-grey">
                                                           {{ number_format($result['distance'],1)!=0?number_format($result['distance'],1):0 }} Km <a href="{{ url('https://www.google.co.in/maps/dir')}}//{{$result['latlon']}}" target="new"><i class="fa fa-location-arrow"></i>  </a>
                                                        </p>
                                                    </div>
                                                    <div class="col-xs-12 text-right">
                                                        <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $result['phone']?$result['phone']:'Not Updated' }}</button>
													</div>
                                            </div>
                                        </div>
                                        @elseif($result['role']==4)
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 loop-search" >
                                                    <div class="col-xs-4  col-md-2 thumnail">
                                                        @if($result['image'])
                                                            <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                                        @else
                                                            <img src="{{ asset("public/img/diagnostic_placeholder.png") }}" class="img-circle" width="95">
                                                        @endif
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <h4><a href="{{ url('diagnostic/details') }}/{{ \Crypt::encrypt($result['id']) }}">{{$result['name'] }}</a></h4>
                                                        <p class="color-grey">{{ $result['address_one'] }}</p>
														<p>	  
														@if(isset($result['diagnostic_test']) && !empty($result['diagnostic_test']))
														@for($i=0; ($i < count($result['diagnostic_test']) && $i<2); $i++)
															<span class="specility">
																@if($i>0) @endif {{ $result['diagnostic_test'][$i] }} 
															</span>
														@endfor
														@endif
														</p>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 text-right">
                                                        <p class="color-grey">
                                                            @for ($i=1; $i <= 5 ; $i++)
                                                              <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                                            @endfor
                                                            {{ number_format($result['rating'], 1) }}
                                                        </p>
                                                        <p class="color-grey">
                                                                {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                                        </p>
                                                        <p class="color-grey">
                                                                {{ number_format($result['distance']) }} Km <i class="fa fa-location-arrow"></i> 
                                                        </p>
                                                    </div>
                                                    <div class="col-xs-12 text-right">
                                                        <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $result['phone']?$result['phone']:'Not Updated' }}</button>
													</div>
                                            </div>
                                        </div>
                                        @elseif($result['role']==5)
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 loop-search" >
                                                    <div class="col-xs-4  col-md-2 thumnail">
                                                        @if($result['image'])
                                                            <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                                        @else
                                                            <img src="{{ asset("public/img/hospital_placeholder.png") }}" class="img-circle" width="95">
                                                        @endif
                                                        
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <h4><a href="{{ url('hospital/details') }}/{{ \Crypt::encrypt($result['id']) }}">{{$result['name'] }}</a></h4>
                                                        <p class="color-grey">{{ $result['address_one'] }}</p>
                                                        <p> @if($result['24_hours_emergency'])<span class="color-grey">24 Hours Open</span> @endif </p>    
														<p>     
															@if(isset($result['speciality']) && !empty($result['speciality']))
															@for($i=0; ($i < count($result['speciality']) && $i<2); $i++)
																<span class="specility">
																	@if($i>0) @endif {{ $result['speciality'][$i] }} 
																</span>
															@endfor
															@endif
														</P>
														<p class="color-grey">Consulting Fee - <span><i class="fa fa-inr"></i>{{ $result['consultation_fee'] }}</span></p>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 text-right">
                                                        <p class="color-grey">
                                                            @for ($i=1; $i <= 5 ; $i++)
                                                              <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                                            @endfor
                                                            {{ number_format($result['rating'], 1) }}
                                                        </p>
                                                        <p class="color-grey">
                                                                {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                                        </p>
                                                        <p class="color-grey">
                                                                {{ number_format($result['distance']) }} Km <i class="fa fa-location-arrow"></i> 
                                                        </p>
													</div>
                                                    <div class="col-xs-12 text-right">
                                                        <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $result['phone']?$result['phone']:'Not Updated' }}</button>
													</div>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
										<div class="col-xs-12">
											{!! Helper::paginator($paginator['count'],$paginator['limit'],$paginator['page']) !!}
                                        </div>
                                        @else
                                        <div class="col-xs-12">
                                            <h1 style="text-align: center;padding: 90px 0px;color: #d8e3ec;">No result found.</h1>
                                        </div>
                                        @endif
                                </div>
                        </section>
                </div>
        </div>
</div>
<!--Popup for see more services--->
<div class="modal fade" id="more_services" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Specialist</h4>
      </div>
      <div class="modal-body">
        <ul class="nav nav-staced more-spec">
            @foreach($services_all  as $service)
            <li class="col-md-3 col-xs-12">
                <div class="col-md-2 padding-0"> 
                    <div class="squaredOne margin-left-0">
                    <input type="checkbox" value="{{ $service->id }}" <?=(!empty(Session::get('filters')['services']) && in_array($service->id,Session::get('filters')['services']))?'checked':''?> id="squared{{ $service->id }}" class="services" name="filters[services][]">
                    <label for="squared{{ $service->id }}"></label>
                    </div>
                </div>
                 <div class="col-md-10 padding-0" >
                <label class="nms" style="font-size:10.5px !important;">{{ $service->name }}</label>
                 </div>
            </li>
            @endforeach       
        </ul>
      </div>
      <div class="modal-footer hide">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!--End services popup -->
{{ Form::close() }}
<!-- =================================
		APP DOWNLOAD PART END 
	================================== -->
@include('layouts.footer')
<link rel="stylesheet" href="{{ asset("public/js/jquery-ui.css") }}">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
		// Clear the filter
        $("#clear-filters").click(function () { 
            document.getElementById("filterForm").reset();
            $('#filterForm').find("input[type=text], textarea, input[type=hidden]").val("");
            var default_distance = "<?php echo $default_distance; ?>";
            $( "#distance" ).val( default_distance + 'KM');
            $('#filterForm').find('input[type=checkbox]:checked').removeAttr('checked');
            console.log($('#filterForm').serialize()) ;
            $("#filterForm").submit();
        })
        //End
        $( "#slider-range-fee" ).slider({
            range: true,
            min: 0,
            max: 1000,
            values: [<?=(!empty(Session::get('filters')['min_fee'])?Session::get('filters')['min_fee']:0)?>, <?=!empty(Session::get('filters')['max_fee'])?Session::get('filters')['max_fee']:1000?>],
            slide: function( event, ui ) {
                $( "#fee" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
                $( "#min_fee" ).val( ui.values[ 0 ] );
                $( "#max_fee" ).val( ui.values[ 1 ] );
                $("#filterForm").submit();
            }
        });
        $( "#fee" ).val( $( "#slider-range-fee" ).slider( "values", 0 ) + " - " + $( "#slider-range-fee" ).slider( "values", 1 ) );
        
        // For service filter.
        $(".services").click(function () { 
            $("#filterForm").submit();
        })
		// For specialties filter.
		$(".specialties").click(function () { 
            $("#filterForm").submit();
        })
        $("#squaredOne").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(1);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        })
        $("#squaredTwo").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(2);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        }) 
        $("#squaredThree").click(function () {
			if ( $(this).is(':checked') ) {
				$( "#star" ).val(3);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit(); 
        }) 
        $("#squaredFour").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(4);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        }) 
        $("#squaredFive").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(5);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        }) 
        $( "#slider-range-distance" ).slider({
            value:<?=!empty(Session::get('filters')['max_distance'])?str_replace('KM','',Session::get('filters')['max_distance']):0?>,
            min: 0,
            max: 50,
            step: 1,
            slide: function( event, ui ) {
              $( "#distance" ).val( ui.value + 'KM');
              $("#filterForm").submit();
            }
        });
        $( "#distance" ).val( $( "#slider-range-distance" ).slider( "value" )+ 'KM' );
        
        // For experience filter
        $( "#slider-range-experience" ).slider({
            value:<?=!empty(Session::get('filters')['max_experience'])?str_replace('Year','',Session::get('filters')['max_experience']):0?>,
            min: 0,
            max: 50,
            step: 1,
            slide: function( event, ui ) {
              $( "#experience" ).val( ui.value +'Year');
              $("#filterForm").submit();
            }
        });
        $( "#experience" ).val( $( "#slider-range-experience" ).slider( "value" ) +'Year');
        
        // For working day filter
        $(".day-of-week").click(function () {
            $("#filterForm").submit();
        })
        // For gender filter.
        $(".gender").click(function () { 
            $("#filterForm").submit();
        })
        // For sorting filter.
        $("#sorting").change(function () { 
            $("#filterForm").submit();
        })
		
		$(function(){
			$("#location").geocomplete({
				details: ".location-details",
				detailsAttribute: "data-geo",
				location: "{{ $location }}",
			});
			$("#location").bind("geocode:result", function(event, result){
				$.ajax({
					type: "POST",
					url: "{{ url('session/stor_city') }}",
					data: $("#search-form").serialize(),
					success: function(result){ 
					   $("#entity-container").html(result);
					},
					error:function(result){
						console.log(result);
					}
				});
			});
		});
  
  });
</script>
@endsection
