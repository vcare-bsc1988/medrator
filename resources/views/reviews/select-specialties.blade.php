@extends('layouts.home')
<style type="text/css">
    #triple{
      /* width:760px; */
      margin-bottom:20px;
      overflow:hidden;
      /* border-top:1px solid #ccc; */
    }
    #triple li{
      line-height:2.0em;
      border-bottom:1px solid #f0f0f0;
      float:left;
      display:inline;
    }
    #double li  { width:50%;}
    #triple li  { width:33.333%; }
    #quad li    { width:25%; } 
    #six li     { width:16.666%; }
	.bottom-strip.p-both.white-bg{border-bottom: 4px solid #e4f0fa;}
	#triple li:before{content:url();}
</style>
@section('content')
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')									   
<!--==================================
	 Header parts ends here
  ==================================-->
<!--==================================
              service Quick links starts here
        ==================================--> 
<div class="first-section ptb">
    <div class="container">
        <div class="row">
            <section for="side-filter">
                <div class="col-md-12 col-xs-12 pull-left">
                    <h2 style="padding:22px 0px;">Select Specialties</h2>
                    <div style="margin-left: auto;margin-right: auto;/* width: 200px; */">
                        <input placeholder="Search..." class="col-xs-12 form-control" id="s" type="text" /> 
                        <hr style="float:left;width:100%;"/>
                        <ul class="countryList" style="clear:both;max-height:200px;overflow:auto;" id="triple">
                            @foreach($specialties as $specialty)
                            <li><a href="{{ url('review/doctor')}}/{{ $specialty->name }}">{{ $specialty->name }} </a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
{{ Form::close() }}
<!-- =================================
		APP DOWNLOAD PART END 
	================================== -->
@include('layouts.footer')
<script>
$( document ).ready(function() {
  
  $('#s').keyup(function(){
   var valThis = $(this).val().toLowerCase();
    $('.countryList>li').each(function(){
     var text = $(this).text().toLowerCase();
        (text.indexOf(valThis) == 0) ? $(this).show() : $(this).hide();            
   });
  });
});
</script>
@endsection
