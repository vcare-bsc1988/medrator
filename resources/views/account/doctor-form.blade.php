@extends('layouts.home')
@section('content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')
<div class="first-section ptb">
    <div class="col-xs-12">
        <div class="row">
            <h3 class="top-strips"></h3>
            <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                        <!--<i class="fa fa-pencil edit-pic"></i>-->
                    </div>
                </div>
            </form>
            <div class="col-md-2 col-xs-12" id="left-panel">
                @include('account.left_nav')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
                        <h3 class="head-01">Doctor Information<hr></h3>
                        <div class="clearfix" style="margin-top:10px;"></div>
                        <div class="col-xs-12 padding-0">
                            @include('common.errors')
                            @if (session('message'))
                            <div class="alert alert-success">
                                <strong>Success!</strong> {{ session('message') }}
                            </div>
                            @endif
                            {{ Form::open(array('url' =>'account/save-doctor','class'=>'form-horizontal', 'files' => true)) }}
                                {{ csrf_field() }}  
                                {{ Form::hidden('id', null) }} 
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <div class="col-md-4 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">Name</label>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                                        </div>
                                </div>
								<div class="form-group{{ $errors->has('about') ? ' has-error' : '' }}">
                                        <div class="col-md-4 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">About</label>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                                {{ Form::textarea('about', null, ['class' => 'form-control','rows'=>'4']) }}
                                        </div>
                                </div>
                                <div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Address</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        {{ Form::text('address_one', null, ['class'=>'form-control location-input', 'placeholder'=>'Location', 'id'=>'location']) }}
                                        <div class="location-details">
                                            <input type="hidden" value="" name="latitude" data-geo="lat">
                                            <input type="hidden" value="" name="longitude" data-geo="lng">
                                            <input type="hidden" value="" name="locality" data-geo="locality">
                                            <input type="hidden" value="" name="city_name" data-geo="locality">
                                            <input type="hidden" value="" name="state_name" data-geo="administrative_area_level_1">
                                            <input type="hidden" value="" name="country_name" data-geo="country">
                                            <input type="hidden" value="" name="pincode" data-geo="postal_code">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Mobile Number</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        {{ Form::number('mobile', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel">Gender</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                         {{ Form::radio('gender', 1)}} Male &nbsp;&nbsp;&nbsp;&nbsp; {{Form::radio('gender', 2)}}  Female
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('qualifications') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Qualification</label>
                                    </div>
                                    <div class="padding-0 col-md-8 padding-0 repeat-quali" style="position:relative;">
                                            <div class="col-md-3 col-xs-12">
                                                    <div id="content" class="clear">
                                                            {{ Form::select("degree[]", $qualifications, null, ["class" => "2col active form-control"]) }}
                                                     </div>
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                    {{ Form::select("college[]", $colleges, null, ["class"=>"form-control"]) }}
                                            </div>
                                            <div class="col-md-3 col-xs-12">
                                                    {{ Form::select("year[]",$years, null, ["class"=>"form-control"]) }}
                                            </div>
                                            <div class="col-md-2">
                                                    <i class="cur fa fa-plus btn btn-info btn-xs add-quali" style="margin-top:10px;"></i>
                                            </div>
                                    </div>
				</div>
                                
                                <div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Speciality</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										<div class="clear" id="content1">
                                        {{ Form::select('specialties[]', $specialties,null, ['class' => '1col active form-control','multiple'=>true]) }}
										</div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('reg_no') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Registration Number</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                         {{ Form::text('reg_no', null, ['class'=>'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('awards') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Award</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                         {{ Form::text('awards', null, ['class'=>'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('recognition') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Recognition</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                         {{ Form::text('recognition', null, ['class'=>'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Experience</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                         {{ Form::text('experience', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Upload Photo (optional)</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::file('image',['class'=>'form-control']) }}
                                    </div>
                                </div>
								<hr>
                                <div class="form-group">
                                    <div class="col-md-4 col-xs-12">

                                    </div>							
                                    <div class="col-md-6 col-xs-12">
                                            <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Save" />
                                    </div>							
                                </div>
                            {{ Form::close() }}
                        </div>							
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>                  
@include('layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    $("#location").geocomplete({
		details: ".location-details",
		detailsAttribute: "data-geo"
	});
        var x = 1;
	$('.add-quali').click(function(){
            var append_data = '<div class="rp-container"><div class="clearfix" style="float:left;width:100%;margin-top:2px;"></div><div class="col-md-3 col-xs-12"><div id="content" class="clear">{{ Form::select("degree[]", $qualifications, null, ["class" => "2col active form-control"]) }}</div></div><div class="col-md-3 col-xs-12">{{ Form::select("college[]", $colleges, null, ["class"=>"form-control"]) }}</div><div class="col-md-3 col-xs-12">{{ Form::select("year[]",$years, null, ["class"=>"form-control"]) }}</div><div class="col-md-2"><i class="cur fa fa-minus btn btn-danger btn-xs remove-repeat" style="margin-top:10px;"></i></div></div>';
            x++;
            $('.repeat-quali').append(append_data);
	})
	$('.repeat-quali').on('click','.remove-repeat',function(){
            $(this).parents('.rp-container').remove(); x--;
	})
	$('.d-ul-left li:nth-child(2)').siblings().removeClass('active');
	$('.d-ul-left li:nth-child(2)').addClass('active');	
	
	/** For accepting numeric value only.
	 *
	 */
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
