@extends('layouts.home')
@section('content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>
.green{
	background-color: yellow;
}
</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
				<form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
				<div class="col-md-2 col-xs-12" id="left-panel">
					@include('account.left_nav')
				</div>
				<div class="col-md-10 col-xs-12">
						<h1></h1>	
						<?php $contributor_levels = [
							['contributor'=>'Beginner','points_required'=>400,'level'=>'Level 1'],
							['contributor'=>'Intermediate','points_required'=>1000,'level'=>'Level 2'],
							['contributor'=>'Advanced','points_required'=>2000, 'level'=>'Level 3'],
							['contributor'=>'Expert','points_required'=>3000, 'level'=>'Level 4']
						];
						$i=0;
						foreach($contributor_levels as $level){
							if($points_collected >= $level['points_required']){
								$contributor_level =  $level['contributor'];
								$required_points = $contributor_levels[$i+1]['points_required'];
								$level = $level['level'];
								break;
							}
							$i++;
						}
						
						?>
						<div class="clearfix" style="margin-top:10px;"></div>
						<div class="col-xs-6">
							<h2 class="text-center">{{ $contributor_level }}</h2> 		
							<div class="clearfix" style="margin-top:10px;"></div>
							<span class="pull-left text-center" style="width:15%;">{{ $level }}</span>
							<div class="progress pull-left" style="width:70%" >
								<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
							</div>
							<span class="pull-left text-center" style="width:15%;">
								@if($level == 'Level 1')
									Level 2
								@elseif($level == 'Level 2')
								`	Level 3
								@elseif($level == 'Level 3')
									Level 4
								@elseif($level == 'Level 4')
									Level 5
								@endif
							</span>
						</div>
						<div class="clearfix"></div>
						<p>{{ $points_collected }} Points Collected ( {{ ( $required_points - $points_collected )}} to Level Up !)</p>
						<h1>&nbsp;</h1>
						<table class="table" style="width:35%;">
						<thead>
							<tr>
								<th>Level of Contributor</th>
								<th>Points Needed</th>
							</tr>
						</thead>
						<tbody>
						  <tr class="@if($points_collected==400)green @endif">
							<td>Beginner</td>
							<td>400</td>
						  </tr>
						  <tr class="@if($points_collected==1000)green @endif">
							<td>Intermediate</td>
							<td>1000</td>
						  </tr>
						  <tr class="@if($points_collected==2000)green @endif">
							<td>Advanced</td>
							<td>2000</td>
						  </tr>
						  <tr class="@if($points_collected==3000)green @endif">
							<td>Expert</td>
							<td>3000</td>
						  </tr>
						  <tr class="@if($points_collected==8000)green @endif">
							<td>Medrator</td>
							<td>8000</td>
						  </tr>
						</tbody>
					  </table>
				</div>
			</div>
		</div>
	</div>
@include('layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>

@endsection
