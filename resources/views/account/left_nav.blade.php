<aside for="sidebar-tab">
    <ul class="nav navbar-tab navbar-stacked navbar-pills d-ul-left" role="tablist">
        <li><a href="{{ url('profile') }}">Personal Info</a></li>
        <li><a href="{{ url('claimed-profile') }}">Claimed Profile</a></li>
        @if(@$user->role->role_id == 2)
        <li><a href="{{ url('account/clinics') }}">Associate Clinic</a></li>
        @endif
        <li><a href="{{ url('account/my-ratings') }}">My Rating & Reviews</a></li>
        <li><a href="{{ url('account/change-password') }}">Change Password</a></li>
        <li><a href="{{ url('account/bookmarks') }}">Saved List</a></li>
        <li><a href="{{ url('/') }}">Search</a></li>
        <li><a href="{{ url('account/points-collected') }}">Points Collected</a></li>
        <li><a href="{{ url('account/notifications') }}">Notifications</a></li>
		<li><a href="{{ url('account/recent-search') }}">Recent Search</a></li>
    </ul>				
</aside>
			
<script>
	$(document).ready(function(){
		var aurl = window.location.href; // Get the absolute url
		$('.d-ul-left li a').filter(function() { 
			return $(this).prop('href') === aurl;
		}).parent('li').addClass('active').siblings().removeClass('active');
		
	})
</script>			