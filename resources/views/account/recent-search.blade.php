@extends('layouts.home')
@section('content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
                <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                       <!-- <i class="fa fa-pencil edit-pic"></i>-->
                    </div>
                </div>
            </form>
            <div class="col-md-2 col-xs-12" id="left-panel">
                @include('account.left_nav')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
					<h3 class="head-01">Recent Search<hr></h3>
					<div class="clearfix" style="margin-top:10px;"></div>
					@if(!empty($recent_search_list))
					@foreach($recent_search_list as $recent_search)
                        @if($recent_search['entity']=='free-text')
							<div class="col-xs-12">
								<?php 
								$data = [
									'q' => $recent_search['q'],
									'location' => $recent_search['	address_one	'],
									'latitude' => $recent_search['latitude'],
									'longitude' => $recent_search['longitude'],
									'locality' => $recent_search['locality'],
									'city_name' => $recent_search['city_name'],
									'state_name' => $recent_search['state_name'],
									'country_name' => $recent_search['country_name'],
								];
								?>
								<a href="{{ url('find?'.http_build_query($data)) }}"><div class="col-xs-12 loop-search" >
									<div class="col-xs-12 col-md-6">
											<h4>{{$recent_search['q'] }}</h4>
											<p class="color-grey">{{ $recent_search['address_one'] }}</p>

									</div>
									<div class="col-xs-12 col-md-4 text-right">
                                        <p>{{ $recent_search['timeago'] }}</p>
                                    </div>
								</div></a>
							</div>
                        @elseif($recent_search['entity']=='doctor')
							<div class="col-xs-12">
								<a href="{{ url('find/doctor')}}/{{ $recent_search['state_name'] }}/{{$recent_search['q']}}"><div class="col-xs-12 loop-search" >
									<div class="col-xs-12 col-md-6">
											<h4>{{$recent_search['q'] }}</h4>
											<p class="color-grey">{{ $recent_search['address_one'] }}</p>

									</div>
									<div class="col-xs-12 col-md-4 text-right">
                                        <p>{{ $recent_search['timeago'] }}</p>
                                    </div>
								</div></a>
							</div>
						@elseif($recent_search['entity']=='hospital')
							<div class="col-xs-12">
								<a href="{{ url('find/hospital')}}/{{ $recent_search['state_name'] }}/{{$recent_search['q']}}"><div class="col-xs-12 loop-search" >
									<div class="col-xs-12 col-md-6">
											<h4>{{$recent_search['q'] }}(KM)</h4>
											<p class="color-grey">{{ $recent_search['address_one'] }}</p>

									</div>
									<div class="col-xs-12 col-md-4 text-right">
                                        <p>{{ $recent_search['timeago'] }}</p>
                                    </div>
								</div></a>
							</div>
						@elseif($recent_search['entity']=='diagnostic')
							<div class="col-xs-12">
								<a href="{{ url('find/diagnostic')}}/{{ $recent_search['state_name'] }}/{{$recent_search['q']}}"><div class="col-xs-12 loop-search" >
									<div class="col-xs-12 col-md-6">
											<h4>{{$recent_search['q'] }}</h4>
											<p class="color-grey">{{ $recent_search['address_one'] }}</p>

									</div>
									<div class="col-xs-12 col-md-4 text-right">
                                        <p>{{ $recent_search['timeago'] }}</p>
                                    </div>
								</div></a>
							</div>
						@endif
					@endforeach
					@else
						<div class="col-xs-12">
							<h1 style="text-align: center;padding: 90px 0px;color: #d8e3ec;">No Saved list was found.</h1>
						</div>
					@endif
                    </div>
                </div>
            </div>
	</div>
    </div>
</div>

    
@include('layouts.footer')
@endsection
