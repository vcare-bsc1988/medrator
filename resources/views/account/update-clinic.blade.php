@extends('layouts.home')
@section('content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
                                <div class="form-group" style="margin-bottom:15px;">							
                                    <div class="col-md-12 text-center col-xs-12">
                                        <div id="image-container">
                                            @if($user->user->image)
                                                    <img src="{{ $user->user->image }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>				
                                            @else
                                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                                            @endif
                                        </div>
                                    </div>
                                </div>
				<div class="col-md-2 col-xs-12" id="left-panel">
                                    @include('account.left_nav')
				</div>
				<div class="col-md-10 col-xs-12">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="pinfo">
                                            <h3 class="head-01">Update Clinic<hr></h3>
                                            <div class="clearfix" style="margin-top:10px;"></div>
                                            @include('common.errors')
                                            @if (session('message'))
                                            <div class="alert alert-success">
                                                <strong>Success!</strong> {{ session('message') }}
                                            </div>
                                            @endif
                                            {{ Form::model($clinics,array('url' =>'account/save-clinic','class'=>'form-horizontal', 'files' => true)) }}
                                            {{ csrf_field() }}  
                                            {{ Form::hidden('id', null) }} 
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <div class="col-md-5 col-xs-12">
                                                            <label class="control-lebel" style="margin-top:8px;">Name</label>
                                                    </div>
                                                    <div class="col-md-5 col-xs-12">
                                                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                                                    </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
                                                <div class="col-md-5 col-xs-12">
                                                        <label class="control-lebel" style="margin-top:8px;">Address</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
                                                    {{ Form::text('address_one', null, ['class'=>'form-control location-input','id'=>'geocomplete']) }}
                                                    <div class="location-details">
                                                        {{ Form::hidden('latitude', null, ['data-geo'=>'lat']) }}
                                                        {{ Form::hidden('longitude', null, ['data-geo'=>'lng']) }}
                                                        {{ Form::hidden('locality', null, ['data-geo'=>'locality']) }}
                                                        {{ Form::hidden('city_name', null, ['data-geo'=>'locality']) }}
                                                        {{ Form::hidden('state_name', null, ['data-geo'=>'administrative_area_level_1']) }}
                                                        {{ Form::hidden('country_name', null, ['data-geo'=>'country_short']) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                                <div class="col-md-5 col-xs-12">
                                                        <label class="control-lebel" style="margin-top:8px;">Phone Number</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
                                                    {{ Form::number('phone', null, ['class'=>'form-control']) }}
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
                                                <div class="col-md-5 col-xs-12">
                                                        <label class="control-lebel" style="margin-top:8px;">Speciality</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
													<div class="clear" id="content1">
                                                    {{ Form::select('specialties[]', $specialties,$clinics->specialtyList()->lists('speciality_id')->toArray(), ['class' => '1col active form-control','multiple'=>true]) }}
													</div>
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('consultation_fee') ? ' has-error' : '' }}">
                                                <div class="col-md-5 col-xs-12">
                                                    <label class="control-lebel" style="margin-top:8px;">Consulting Fee</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
                                                    {{ Form::text('consultation_fee', null, ['class'=>'form-control']) }}
                                                </div>
                                            </div>
                                            {{--<div class="form-group">
                                                <div class="col-md-5 col-xs-12">
                                                    <label class="control-lebel" style="margin-top:8px;">Schedular</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs schedule-ul" role="tablist">
                                                        <li role="presentation" class="active"><a href="#sun" aria-controls="sun" role="tab" data-toggle="tab">SUN</a></li>
                                                        <li role="presentation"><a href="#mon" aria-controls="mon" role="tab" data-toggle="tab">MON</a></li>
                                                        <li role="presentation"><a href="#tue" aria-controls="tue" role="tab" data-toggle="tab">TUE</a></li>
                                                        <li role="presentation"><a href="#wed" aria-controls="wed" role="tab" data-toggle="tab">WED</a></li>
                                                        <li role="presentation"><a href="#thu" aria-controls="thu" role="tab" data-toggle="tab">THU</a></li>
                                                        <li role="presentation"><a href="#fri" aria-controls="fri" role="tab" data-toggle="tab">FRI</a></li>
                                                        <li role="presentation"><a href="#sat" aria-controls="sat" role="tab" data-toggle="tab">SAT</a></li>
                                                    </ul>
                                                    <!-- Tab panes -->
                                                    <div class="tab-content tab-content1">
                                                          <div role="tabpanel" class="tab-pane active" id="sun">
                                                              {{ Form::hidden("scheduler[0][week_day]", 1) }}
                                                              {{ Form::text("scheduler[0][timings]", $schedular[0]['timings'], ["class" => "form-control tags"]) }}
                                                          </div>
                                                          <div role="tabpanel" class="tab-pane" id="mon">
                                                              {{ Form::hidden("scheduler[1][week_day]", 2) }}
                                                              {{ Form::text("scheduler[1][timings]", $schedular[1]['timings'], ["class" => "form-control tags"]) }}
                                                          </div>
                                                          <div role="tabpanel" class="tab-pane" id="tue">
                                                              {{ Form::hidden("scheduler[2][week_day]", 3) }}
                                                              {{ Form::text("scheduler[2][timings]", $schedular[2]['timings'], ["class" => "form-control tags"]) }}
                                                          </div>
                                                          <div role="tabpanel" class="tab-pane" id="wed">
                                                              {{ Form::hidden("scheduler[3][week_day]", 4) }}
                                                              {{ Form::text("scheduler[3][timings]", $schedular[3]['timings'], ["class" => "form-control tags"]) }}
                                                          </div>
                                                          <div role="tabpanel" class="tab-pane" id="thu">
                                                              {{ Form::hidden("scheduler[4][week_day]", 5) }}
                                                              {{ Form::text("scheduler[4][timings]", $schedular[4]['timings'], ["class" => "form-control tags"]) }}
                                                          </div>
                                                          <div role="tabpanel" class="tab-pane" id="fri">
                                                              {{ Form::hidden("scheduler[5][week_day]", 6) }}
                                                              {{ Form::text("scheduler[5][timings]", $schedular[5]['timings'], ["class" => "form-control tags"]) }}
                                                          </div>
                                                          <div role="tabpanel" class="tab-pane" id="sat">
                                                              {{ Form::hidden("scheduler[6][week_day]", 7) }}
                                                              {{ Form::text("scheduler[6][timings]", $schedular[6]['timings'], ["class" => "form-control tags"]) }}
                                                          </div>
                                                    </div>
                                                </div>
                                            </div>--}}
											<hr>
                                            <div class="form-group">
                                                <div class="col-md-5 col-xs-12"></div>							
                                                <div class="col-md-5 col-xs-12">
                                                    <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Update" />
                                                </div>							
                                            </div>
                                            {{ Form::close() }}     
                                        </div>
                                    </div>
				</div>
				
				
			</div>
		</div>
	</div>

    
@include('layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    "use strict"; 
    $(function(){    
        $("#geocomplete").geocomplete({
            details: ".location-details",
            detailsAttribute: "data-geo"
        });
    });
    $('.d-ul-left li:nth-child(3)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(3)').addClass('active');
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
