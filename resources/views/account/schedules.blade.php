<link rel="stylesheet" href="{{ asset('public/css/jquery.timepicker.css') }}" type ="text/css" />
<div role="tabpanel" class="tab-pane active" id="sun">
    {{ Form::open(array('url' => '', 'method' => 'post','id'=>'shcedule-form-sun')) }}
    {{ csrf_field() }}
    {{ Form::hidden("week_day", 1) }}
    {{ Form::hidden("doctor_id", $doctor->pivot->doctor_id, ['id'=>'schedule_doctor_id']) }}
    {{ Form::hidden("hospital_id", $doctor->pivot->hospital_id, ['id' => 'schedule_hospital_id']) }}
    <div class="row">
        <div class="col-xs-12 col-sm-6">
			<div class="row  input_fields_wrap_sun" style="margin-bottom:10px;" class="repeat-quali">
                @if($hospital->workingTimes()->where('week_day',1)->get()->count()>0)
                <?php $working_times = $hospital->workingTimes()->where('week_day',1)->get(); ?>
                @for($i=0; $i < $hospital->workingTimes()->where('week_day',1)->get()->count(); $i++)
                <div class="add-containers"  style="margin-bottom:3px;overflow:hidden;">
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][from]", $working_times[$i]->start_time, ["class" => "form-control time start text-center"]) }}
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][to]", $working_times[$i]->end_time, ["class" => "form-control time end text-center"]) }}
                    </div>
					<div class="col-sm-2">
                    <a href="#" class="remove_field"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a>
					</div>
                </div>
                @endfor
                @else
				<div class="add-containers"  style="margin-bottom:3px;overflow:hidden;">
					<div class="col-xs-12 col-sm-6">
						{{ Form::text("schedular[0][from]", null, ["class" => "form-control time start"]) }}
					</div>
					<div class="col-xs-12 col-sm-6">
						{{ Form::text("schedular[0][to]", null, ["class" => "form-control time start"]) }}
					</div>
				</div>
                @endif
            </div>
			<hr>	
            {{ Form::button('save',['class' =>'save-schedule-btn-sun btn-info']) }}
        </div>
        <div class="col-xs-12 col-sm-6"><button class="btn btn-info add_field_button_sun btn-xs">Add More Session</button></div>
    </div>
    {{ Form::close() }}
</div>
<div role="tabpanel" class="tab-pane" id="mon">
  {{ Form::open(array('url' => '', 'method' => 'post','id'=>'shcedule-form-mon')) }}
    {{ csrf_field() }}
    {{ Form::hidden("week_day", 2) }}
    {{ Form::hidden("doctor_id", $doctor->pivot->doctor_id, ['id'=>'schedule_doctor_id']) }}
    {{ Form::hidden("hospital_id", $doctor->pivot->hospital_id, ['id' => 'schedule_hospital_id']) }}
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="row  input_fields_wrap_mon" style="margin-bottom:10px;" class="repeat-quali">
                @if($hospital->workingTimes()->where('week_day',2)->get()->count()>0)
                <?php $working_times = $hospital->workingTimes()->where('week_day',2)->get(); ?>
                @for($i=0; $i < $hospital->workingTimes()->where('week_day',2)->get()->count(); $i++)
                <div class="add-containers-mon" id="datepairExample" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][from]", $working_times[$i]->start_time, ["class" => "form-control text-center time"]) }}
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][to]", $working_times[$i]->end_time, ["class" => "form-control  text-center time"]) }}
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="remove_field_mon"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a>
                    </div>
                </div>
                @endfor
                @else
				<div class="add-containers-mon" id="datepairExample" style="margin-bottom:3px;overflow:hidden;float:left;">
					<div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][from]", null, ["class" => "form-control time"]) }}
					</div>
					<div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][to]", null, ["class" => "form-control time"]) }}
					</div>
				</div>
                @endif
            </div>
			<hr>	
            {{ Form::button('save',['class' =>'save-schedule-btn-mon btn-info']) }}
        </div>
        <div class="col-xs-12 col-sm-6">
			<button class="btn btn-info add_field_button_mon btn-xs">Add More Session</button>
			<button class="btn btn-info copy_field_button_mon btn-xs">Copy from Previous</button>
		</div>
    </div>
    {{ Form::close() }}
</div>
<div role="tabpanel" class="tab-pane" id="tue">
    {{ Form::open(array('url' => '', 'method' => 'post','id'=>'shcedule-form-tue')) }}
    {{ csrf_field() }}
    {{ Form::hidden("week_day", 3) }}
    {{ Form::hidden("doctor_id", $doctor->pivot->doctor_id, ['id'=>'schedule_doctor_id']) }}
    {{ Form::hidden("hospital_id", $doctor->pivot->hospital_id, ['id' => 'schedule_hospital_id']) }}
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="row  input_fields_wrap_tue" style="margin-bottom:10px;" class="repeat-quali">
                @if($hospital->workingTimes()->where('week_day',3)->get()->count()>0)
                <?php $working_times = $hospital->workingTimes()->where('week_day',3)->get(); ?>
                @for($i=0; $i < $hospital->workingTimes()->where('week_day',3)->get()->count(); $i++)
                <div class="add-containers_tue" id="datepairExample" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][from]", $working_times[$i]->start_time, ["class" => "form-control text-center time"]) }}
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][to]", $working_times[$i]->end_time, ["class" => "form-control  text-center time"]) }}
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="remove_field_tue"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a>
                    </div>
                </div>
                @endfor
                @else
                <div class="add-containers_tue" id="datepairExample" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][from]", null, ["class" => "form-control time"]) }}
					</div>
					<div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][to]", null, ["class" => "form-control time"]) }}
					</div>
				</div>
                @endif
            </div>
			<hr>	
            {{ Form::button('save',['class' =>'save-schedule-btn-tue btn-info']) }}
        </div>
        <div class="col-xs-12 col-sm-6">
			<button class="btn btn-info add_field_button_tue btn-xs">Add More Session</button>
			<button class="btn btn-info copy_field_button_tue btn-xs">Copy from Previous</button>
		</div>
    </div>
    {{ Form::close() }}
</div>

<div role="tabpanel" class="tab-pane" id="wed">
    {{ Form::open(array('url' => '', 'method' => 'post','id'=>'shcedule-form-wed')) }}
    {{ csrf_field() }}
    {{ Form::hidden("week_day", 4) }}
    {{ Form::hidden("doctor_id", $doctor->pivot->doctor_id, ['id'=>'schedule_doctor_id']) }}
    {{ Form::hidden("hospital_id", $doctor->pivot->hospital_id, ['id' => 'schedule_hospital_id']) }}
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="row  input_fields_wrap_wed" style="margin-bottom:10px;" class="repeat-quali">
                @if($hospital->workingTimes()->where('week_day',4)->get()->count()>0)
                <?php $working_times = $hospital->workingTimes()->where('week_day',4)->get(); ?>
                @for($i=0; $i < $hospital->workingTimes()->where('week_day',4)->get()->count(); $i++)
                <div class="add-containers_wed" id="datepairExample" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][from]", $working_times[$i]->start_time, ["class" => "form-control text-center time"]) }}
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][to]", $working_times[$i]->end_time, ["class" => "form-control  text-center time"]) }}
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="remove_field_wed"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a>
                    </div>
                </div>
                @endfor
                @else
                <div class="add-containers_wed" id="datepairExample" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
                    {{ Form::text("schedular[0][from]", null, ["class" => "form-control time"]) }}
					</div>
					<div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][to]", null, ["class" => "form-control time"]) }}
					</div>
				</div>
                @endif
            </div>
			<hr>	
            {{ Form::button('save',['class' =>'save-schedule-btn-wed btn-info']) }}
        </div>
        <div class="col-xs-12 col-sm-6">
			<button class="btn btn-info add_field_button_wed btn-xs">Add More Session</button>
			<button class="btn btn-info copy_field_button_wed btn-xs">Copy from Previous</button>
		</div>
    </div>
    {{ Form::close() }}
</div>

<div role="tabpanel" class="tab-pane" id="thu">
    {{ Form::open(array('url' => '', 'method' => 'post','id'=>'shcedule-form-thu')) }}
    {{ csrf_field() }}
    {{ Form::hidden("week_day", 5) }}
    {{ Form::hidden("doctor_id", $doctor->pivot->doctor_id, ['id'=>'schedule_doctor_id']) }}
    {{ Form::hidden("hospital_id", $doctor->pivot->hospital_id, ['id' => 'schedule_hospital_id']) }}
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="row  input_fields_wrap_thu" style="margin-bottom:10px;" class="repeat-quali">
                @if($hospital->workingTimes()->where('week_day',5)->get()->count()>0)
                <?php $working_times = $hospital->workingTimes()->where('week_day',5)->get(); ?>
                @for($i=0; $i < $hospital->workingTimes()->where('week_day',5)->get()->count(); $i++)
                <div class="add-containers_thu" id="datepairExample" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][from]", $working_times[$i]->start_time, ["class" => "form-control text-center time"]) }}
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][to]", $working_times[$i]->end_time, ["class" => "form-control  text-center time"]) }}
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="remove_field_thu"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a>
                    </div>
                </div>
                @endfor
                @else
                <div class="add-containers_thu" id="datepairExample" style="margin-bottom:3px;overflow:hidden;float:left;">
						<div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][from]", null, ["class" => "form-control time"]) }}
					</div>
					<div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][to]", null, ["class" => "form-control time"]) }}
					</div>
				</div>
                @endif
            </div>
			<hr>	
            {{ Form::button('save',['class' =>'save-schedule-btn-thu btn-info']) }}
        </div>
        <div class="col-xs-12 col-sm-6">
			<button class="btn btn-info add_field_button_thu btn-xs" type="button">Add More Session</button>
			<button class="btn btn-info copy_field_button_thu btn-xs">Copy from Previous</button>
		</div>
    </div>
    {{ Form::close() }}
</div>
<div role="tabpanel" class="tab-pane" id="fri">
    {{ Form::open(array('url' => '', 'method' => 'post','id'=>'shcedule-form-fri')) }}
    {{ csrf_field() }}
    {{ Form::hidden("week_day", 6) }}
    {{ Form::hidden("doctor_id", $doctor->pivot->doctor_id, ['id'=>'schedule_doctor_id']) }}
    {{ Form::hidden("hospital_id", $doctor->pivot->hospital_id, ['id' => 'schedule_hospital_id']) }}
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="row  input_fields_wrap_fri" style="margin-bottom:10px;" class="repeat-quali">
                @if($hospital->workingTimes()->where('week_day',6)->get()->count()>0)
                <?php $working_times = $hospital->workingTimes()->where('week_day',6)->get(); ?>
                @for($i=0; $i < $hospital->workingTimes()->where('week_day',6)->get()->count(); $i++)
                <div class="add-containers_fri" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][from]", $working_times[$i]->start_time, ["class" => "form-control text-center time"]) }}
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][to]", $working_times[$i]->end_time, ["class" => "form-control  text-center time"]) }}
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="remove_field_fri"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a>
                    </div>
                </div>
                @endfor
                @else
                <div class="add-containers_fri" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][from]", null, ["class" => "form-control time"]) }}
					</div>
					<div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][to]", null, ["class" => "form-control time"]) }}
					</div>
				</div>
                @endif
            </div>
			<hr>	
            {{ Form::button('save',['class' =>'save-schedule-btn-fri btn-info']) }}
        </div>
        <div class="col-xs-12 col-sm-6">
			<button class="btn btn-info add_field_button_fri btn-xs">Add More Session</button>
			<button class="btn btn-info copy_field_button_fri btn-xs">Copy from Previous</button>
		</div>
    </div>
    {{ Form::close() }}
</div>
<div role="tabpanel" class="tab-pane" id="sat">
  {{ Form::open(array('url' => '', 'method' => 'post','id'=>'shcedule-form-sat')) }}
    {{ csrf_field() }}
    {{ Form::hidden("week_day", 7) }}
    {{ Form::hidden("doctor_id", $doctor->pivot->doctor_id, ['id'=>'schedule_doctor_id']) }}
    {{ Form::hidden("hospital_id", $doctor->pivot->hospital_id, ['id' => 'schedule_hospital_id']) }}
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="row  input_fields_wrap_sat" style="margin-bottom:10px;" class="repeat-quali">
                @if($hospital->workingTimes()->where('week_day',7)->get()->count()>0)
                <?php $working_times = $hospital->workingTimes()->where('week_day',7)->get(); ?>
                @for($i=0; $i < $hospital->workingTimes()->where('week_day',7)->get()->count(); $i++)
                <div class="add-containers_sat" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][from]", $working_times[$i]->start_time, ["class" => "form-control text-center time"]) }}
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        {{ Form::text("schedular[". $i ."][to]", $working_times[$i]->end_time, ["class" => "form-control  text-center time"]) }}
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="remove_field_sat"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a>
                    </div>
                </div>
                @endfor
                @else
                <div class="add-containers_sat" style="margin-bottom:3px;overflow:hidden;float:left;">
                    <div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][from]", null, ["class" => "form-control time"]) }}
					</div>
					<div class="col-xs-12 col-sm-5" style="margin-bottom:3px;">
						{{ Form::text("schedular[0][to]", null, ["class" => "form-control time"]) }}
					</div>
				</div>
                @endif
            </div>
			<hr>	
            {{ Form::button('save',['class' =>'save-schedule-btn-sat btn-info']) }}
        </div>
        <div class="col-xs-12 col-sm-6">
			<button class="btn btn-info add_field_button_sat btn-xs">Add More Session</button>
			<button class="btn btn-info copy_field_button_sat btn-xs">Copy from Previous</button>
		</div>
    </div>
    {{ Form::close() }}
</div>
<script type="text/javascript">
    "use strict"; 
    /**
     * @ Addinf dynamic field for schedular
     */
    var max_fields      = 10000; //maximum input boxes allowed
    var wrapper_sun         = $(".input_fields_wrap_sun"); //Fields wrapper
    var add_button_sun      = $(".add_field_button_sun"); //Add button ID
    var x = 1; //initlal text box count
    $(add_button_sun).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            var append_data = '<div class="add-containers" style="margin-bottom:3px;overflow:hidden;"><div class="col-xs-12 col-sm-5"><input class="form-control time start trns" name="schedular[' + x + '][from]" type="text"></div><div class="col-xs-12 col-sm-5"><input class="form-control time end trns" name="schedular[' + x + '][to]" type="text"></div><div class="col-sm-2 col-xs-12"><a href="#" class="remove_field"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a></div></div></div>';
            x++; //text box increment
            $(wrapper_sun).append(append_data); //add input box
        }
    });
    
    $(wrapper_sun).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parents('.add-containers').remove(); x--;
    })
    
    var wrapper_mon         = $(".input_fields_wrap_mon"); //Fields wrapper
    var add_button_mon      = $(".add_field_button_mon"); //Add button ID
    $(add_button_mon).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            var append_data = '<div class="add-containers-mon" style="margin-bottom:3px;overflow:hidden;float:left;"><div class="col-xs-12 col-sm-5"><input class="form-control time trns" name="schedular[' + x + '][from]" type="text"></div><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][to]" type="text"></div><div class="col-sm-2 col-xs-12"><a href="#" class="remove_field_mon"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a></div></div></div>';
            x++; //text box increment
            $(wrapper_mon).append(append_data); //add input box
        }
    });  
    $(wrapper_mon).on("click",".remove_field_mon", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parents('.add-containers_mon').remove(); x--;
    })
    
    var wrapper_tue         = $(".input_fields_wrap_tue"); //Fields wrapper
    var add_button_tue      = $(".add_field_button_tue"); //Add button ID
    $(add_button_tue ).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            var append_data = '<div class="add-containers_tue" style="margin-bottom:3px;overflow:hidden;float:left;"><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][from]" type="text"></div><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][to]" type="text"></div><div class="col-sm-2 col-xs-12"><a href="#" class="remove_field_tue"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a></div></div></div>';
            x++; //text box increment
            $(wrapper_tue ).append(append_data); //add input box
        }
    });  
    $(wrapper_tue ).on("click",".remove_field_tue", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parents('.add-containers_tue').remove(); x--;
    })
    
    var wrapper_wed         = $(".input_fields_wrap_wed"); //Fields wrapper
    var add_button_wed       = $(".add_field_button_wed"); //Add button ID
    $(add_button_wed ).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            var append_data = '<div class="add-containers_wed" style="margin-bottom:3px;overflow:hidden;float:left;"><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][from]" type="text"></div><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][to]" type="text"></div><div class="col-sm-2 col-xs-12"><a href="#" class="remove_field_wed"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a></div></div></div>';
            x++; //text box increment
            $(wrapper_wed ).append(append_data); //add input box
        }
    });  
    $(wrapper_wed ).on("click",".remove_field_wed", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parents('.add-containers_wed').remove(); x--;
    })
    
    var wrapper_thr         = $(".input_fields_wrap_thu"); //Fields wrapper
    var add_button_thr      = $(".add_field_button_thu"); //Add button ID
    $(add_button_thr ).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            var append_data = '<div class="add-containers_thu" style="margin-bottom:3px;overflow:hidden;float:left;"><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][from]" type="text"></div><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][to]" type="text"></div><div class="col-sm-2 col-xs-12"><a href="#" class="remove_field_thu"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a></div></div></div>';
            x++; //text box increment
            $(wrapper_thr ).append(append_data); //add input box
        }
    });  
    $(wrapper_thr ).on("click",".remove_field_thu", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parents('.add-containers_thu').remove(); x--;
    })
    
    var wrapper_fri       = $(".input_fields_wrap_fri"); //Fields wrapper
    var add_button_fri      = $(".add_field_button_fri"); //Add button ID
    $(add_button_fri ).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            var append_data = '<div class="add-containers_fri" style="margin-bottom:3px;overflow:hidden;float:left;"><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][from]" type="text"></div><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][to]" type="text"></div><div class="col-sm-2 col-xs-12"><a href="#" class="remove_field_fri"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a></div></div></div>';
            x++; //text box increment
            $(wrapper_fri ).append(append_data); //add input box
        }
    });  
    $(wrapper_fri ).on("click",".remove_field_fri", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parents('.add-containers_fri').remove(); x--;
    })
    
    var wrapper_sat         = $(".input_fields_wrap_sat"); //Fields wrapper
    var add_button_sat      = $(".add_field_button_sat"); //Add button ID
    $(add_button_sat ).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            var append_data = '<div class="add-containers_sat" style="margin-bottom:3px;overflow:hidden;float:left;"><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][from]" type="text"></div><div class="col-xs-12 col-sm-5"><input class="form-control trns time" name="schedular[' + x + '][to]" type="text"></div><div class="col-sm-2 col-xs-12"><a href="#" class="remove_field_sat"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a></div></div></div>';
            x++; //text box increment
            $(wrapper_sat ).append(append_data); //add input box
        }
    });  
    $(wrapper_sat ).on("click",".remove_field_sat", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parents('.add-containers_sat').remove(); x--;
    })
    /**
    * @ Populate add schedules form popup
    */
    $('.add-schedules').click(function(){
        var uid = "{{ Auth::user()->id }}";
        var id = $(this).attr("rel");
        var csrf_field = "{{ csrf_token() }}";     
        $.ajax({
            type: "POST",
            url: "{{ url('account/get-clinic') }}",
            data: {'id':id, '_token':csrf_field},
            beforeSend: function(){
                $('#scheduler_loader').css('display','block');
            },
            success: function(result){ 
                console.log(result);
                $('#scheduler_loader').css('display','none');
                $("#myModal").modal();
                $(".tab-content1").html(result);
            },
            error:function(result){
                console.log(result);
            }
        });
    })
    /**
    * @ Save schedules
    */
    $('.save-schedule-btn-sun').click(function(){
        var uid = "{{ Auth::user()->id }}";
        $.ajax({
            type: "POST",
            url: "{{ url('account/save-sun-schedules') }}",
            data: $('#shcedule-form-sun').serialize(),
            success: function(result){ 
                console.log(result);
                $("#schedule-msg").html(result.message);
                $('#schedule-msg').show();
                setTimeout(function () {$('#schedule-msg').hide();},4000);
            },
            error:function(result){
                console.log(result);
            }
        });
    })
	$('.save-schedule-btn-mon').click(function(){
        var uid = "{{ Auth::user()->id }}";
        $.ajax({
            type: "POST",
            url: "{{ url('account/save-mon-schedules') }}",
            data: $('#shcedule-form-mon').serialize(),
            success: function(result){ 
                console.log(result);
                $("#schedule-msg").html(result.message);
                $('#schedule-msg').show();
                setTimeout(function () {$('#schedule-msg').hide();},4000);
            },
            error:function(result){
                console.log(result);
            }
        });
    })
	$('.save-schedule-btn-tue').click(function(){
        var uid = "{{ Auth::user()->id }}";
        $.ajax({
            type: "POST",
            url: "{{ url('account/save-tue-schedules') }}",
            data: $('#shcedule-form-tue').serialize(),
            success: function(result){ 
                console.log(result);
                $("#schedule-msg").html(result.message);
                $('#schedule-msg').show();
                setTimeout(function () {$('#schedule-msg').hide();},4000);
            },
            error:function(result){
                console.log(result);
            }
        });
    })
	$('.save-schedule-btn-wed').click(function(){
        var uid = "{{ Auth::user()->id }}";
        $.ajax({
            type: "POST",
            url: "{{ url('account/save-wed-schedules') }}",
            data: $('#shcedule-form-wed').serialize(),
            success: function(result){ 
                console.log(result);
                $("#schedule-msg").html(result.message);
                $('#schedule-msg').show();
                setTimeout(function () {$('#schedule-msg').hide();},4000);
            },
            error:function(result){
                console.log(result);
            }
        });
    })
	$('.save-schedule-btn-thu').click(function(){
        var uid = "{{ Auth::user()->id }}";
        $.ajax({
            type: "POST",
            url: "{{ url('account/save-thr-schedules') }}",
            data: $('#shcedule-form-thu').serialize(),
            success: function(result){ 
                console.log(result);
                $("#schedule-msg").html(result.message);
                $('#schedule-msg').show();
                setTimeout(function () {$('#schedule-msg').hide();},4000);
            },
            error:function(result){
                console.log(result);
            }
        });
    })
	$('.save-schedule-btn-fri').click(function(){
        var uid = "{{ Auth::user()->id }}";
        $.ajax({
            type: "POST",
            url: "{{ url('account/save-fri-schedules') }}",
            data: $('#shcedule-form-fri').serialize(),
            success: function(result){ 
                console.log(result);
                $("#schedule-msg").html(result.message);
                $('#schedule-msg').show();
                setTimeout(function () {$('#schedule-msg').hide();},4000);
            },
            error:function(result){
                console.log(result);
            }
        });
    })
	$('.save-schedule-btn-sat').click(function(){
        var uid = "{{ Auth::user()->id }}";
        $.ajax({
            type: "POST",
            url: "{{ url('account/save-sat-schedules') }}",
            data: $('#shcedule-form-sat').serialize(),
            success: function(result){ 
                //console.log(result);
                $("#schedule-msg").html(result.message);
                $('#schedule-msg').show();
                setTimeout(function () {$('#schedule-msg').hide();},4000);
            },
            error:function(result){
                //console.log(result);
            }
        });
    })
	/****
	 Copy monday schedules from sunday
	 */
	$('.copy_field_button_mon').click(function(){
		$('.add-containers-mon').remove(); 
		var cloned_field = $('.add-containers').clone();
		$('.input_fields_wrap_mon').append(cloned_field);
		/**
		 * Rename class
		 */
		$(".input_fields_wrap_mon .add-containers").removeClass("add-containers").addClass("add-containers-mon");		
		return false;
	});
	/****
	 Copy Tuesday schedules from Monday
	 */
	$('.copy_field_button_tue').click(function(){
		$('.add-containers_tue').remove(); 
		var cloned_field = $('.add-containers-mon').clone();
		$('.input_fields_wrap_tue').append(cloned_field);
		/**
		 * Rename class
		 */
		$(".input_fields_wrap_tue .add-containers-mon").removeClass("add-containers-mon").addClass("add-containers_tue");		
		return false;
	});
	/****
	 Copy Wed schedules from Tuesday
	 */
	$('.copy_field_button_wed').click(function(){
		$('.add-containers_wed').remove(); 
		var cloned_field = $('.add-containers_tue').clone();
		$('.input_fields_wrap_wed').append(cloned_field);
		/**
		 * Rename class
		 */
		$(".input_fields_wrap_wed .add-containers_tue").removeClass("add-containers_tue").addClass("add-containers_wed");		
		return false;
	});
	/****
	 Copy Thrusday schedules from Wednesday
	 */
	$('.copy_field_button_thu').click(function(){
		$('.add-containers_thu').remove(); 
		var cloned_field = $('.add-containers_wed').clone();
		$('.input_fields_wrap_thu').append(cloned_field);
		/**
		 * Rename class
		 */
		$(".input_fields_wrap_thu .add-containers_wed").removeClass("add-containers_wed").addClass("add-containers_thu");		
		return false;
	});
	/****
	 Copy Friday schedules from Thrusday
	 */
	$('.copy_field_button_fri').click(function(){
		$('.add-containers_fri').remove(); 
		var cloned_field = $('.add-containers_thu').clone();
		$('.input_fields_wrap_fri').append(cloned_field);
		/**
		 * Rename class
		 */
		$(".input_fields_wrap_fri .add-containers_thu").removeClass("add-containers_thu").addClass("add-containers_fri");		
		return false;
	});
	/****
	 Copy Saturday schedules from Friday
	 */
	$('.copy_field_button_sat').click(function(){
		$('.add-containers_sat').remove(); 
		var cloned_field = $('.add-containers_fri').clone();
		$('.input_fields_wrap_sat').append(cloned_field);
		/**
		 * Rename class
		 */
		$(".input_fields_wrap_sat .add-containers_fri").removeClass("add-containers_fri").addClass("add-containers_sat");		
		return false;
	});
	/**
	 * Reload page while closing schedular poup.
	 */
	$('button.close').click(function(){
		location.reload();
	});
</script>
<script src="{{ asset('public/js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('public/js/jquery.timepicker.js') }}"></script>
<script src="{{ asset('public/js/datepair.js') }}"></script>
<script src="{{ asset('public/js/jquery.datepair.js') }}"></script>
<script>
$(function(){
	$(document).on('click','.add-containers .time',function(){
		$(this).timepicker({
			'showDuration': true,
			'timeFormat': 'g:ia'
		});
		$(this).parent('.add-containers').datepair();
	});
    $(document).on('click','.add-containers-mon .time',function(){
		$(this).timepicker({
			'showDuration': true,
			'timeFormat': 'g:ia'
		});
		$(this).parent('.add-containers_tue').datepair();
	});
	$(document).on('click','.add-containers_tue .time',function(){
		$(this).timepicker({
			'showDuration': true,
			'timeFormat': 'g:ia'
		});
		$(this).parent('.add-containers_wed').datepair();
	});
	$(document).on('click','.add-containers_wed .time',function(){
		$(this).timepicker({
			'showDuration': true,
			'timeFormat': 'g:ia'
		});
		$(this).parent('.add-containers_thu').datepair();
	});
	$(document).on('click','.add-containers_thu .time',function(){
		$(this).timepicker({
			'showDuration': true,
			'timeFormat': 'g:ia'
		});
		$(this).parent('.add-containers_thu').datepair();
	});
	$(document).on('click','.add-containers_fri .time',function(){
		$(this).timepicker({
			'showDuration': true,
			'timeFormat': 'g:ia'
		});
		$(this).parent('.add-containers_fri').datepair();
	});
	$(document).on('click','.add-containers_sat .time',function(){
		$(this).timepicker({
			'showDuration': true,
			'timeFormat': 'g:ia'
		});
		$(this).parent('.add-containers_sat').datepair();
	});
})
</script>
