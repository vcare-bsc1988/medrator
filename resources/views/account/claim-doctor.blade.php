@extends('layouts.home')

@section('content')
<style>
.my-list-group li{border:none;padding:5px 0px;}
.my-list-group1 li{padding:10px 0px;float:left;width:100%;}
.color-blue{color: #29b9e8;}
.radius-0{border-radius:0px;}
.w100{width:100%;}
.similar-img{border: 1px solid #ccc;border-radius: 50%;width:60px;height:60px;}
.review-box,.write-box1{position:relative;padding:10px !important;    border: 1px solid #f0f0f0;
    margin-top: 10px;}
.review-box:before{content:'';position:absolute;width:0;left:15px;right:0;top:-11px;border-bottom:10px solid #f0f0f0;border-left:10px solid transparent;border-right:10px solid transparent;z-index:1;}
.review-box:after{content:'';position:absolute;width:0;left:15px;right:0;top:-10px;border-bottom:10px solid #fff;border-left:10px solid transparent;border-right:10px solid transparent;z-index:2;}
.row[for="tabs-part"]{margin-top:30px;}
.mypanel{padding:15px;background:#e4f0fa;overflow:hidden;}
.tabs-01 li a{background:#e4f0fa;color:#5b5b5b !important;border-bottom:2px solid #5b5b5b;outline:none !important;}
.tabs-01 li.active a,.tabs-01 li.active a:focus{background:#31b0d5;color:white !important;border-top-color:#31b0d5;border-left-color:#31b0d5;border-right-color:#31b0d5;border-bottom:2px solid #1a8eb5;}
.write-box1:before{content:'';position:absolute;width:0;left:15px;right:0;top:-11px;border-bottom:10px solid #f0f0f0;border-left:10px solid transparent;border-right:10px solid transparent;z-index:1;}
.write-box1:after{content:'';position:absolute;width:0;left:15px;right:0;top:-10px;border-bottom:10px solid #fff;border-left:10px solid transparent;border-right:10px solid transparent;z-index:2;}
.service-containers{float:left;padding:10px;border: 1px dashed #a9a4a4; margin: 10px; color: #505050;}
</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')									   
<!--==================================
	 Header parts ends here
  ==================================-->
<!--==================================
              service Quick links starts here
        ==================================--> 
<div class="first-section ptb">
    <div class="container">
        <div class="row">
			@include('common.errors')
			@if (session('success'))
			<div class="alert alert-success">
				<strong>Success!</strong> {{ session('success') }}
			</div>
			@endif 
            <div class="col-md-4 col-xs-12">
                <div class="thumbnail">
                    @if($doctor->image)
                        <img src="{{ $doctor->image }}" class="w100">
                    @else
                        <img src="{{ asset("public/images/med_icons/doctor.png") }}" class="img-circle w100">
                    @endif
                </div>
				<div class="claim-doctor">
				{{Form::open(['url' => 'account/claiming-doctor', 'method' => 'post'])}}
				{{ Form::hidden ('id', $doctor->id) }}
				{{ Form::submit('Claim this Profile') }}
				{{Form::close()}}
				</div>	
            </div>
                <div class="col-md-4 col-xs-12">
                        <h1 style="margin-top:0px;">{{ $doctor->name }}</h1>
                        <ul class="list-group my-list-group">
                          <li class="list-group-item"><i class="fa fa-map-marker"></i>&nbsp;&nbsp; {{ $doctor->address_one }}</li>
                          <li class="list-group-item"><i class="fa fa-phone"></i>&nbsp;&nbsp; {{ $doctor->mobile }}</li>
                          <li>
                              @for ($i=1; $i <= 5 ; $i++)
                                <span class="glyphicon glyphicon-star{{ ($i <= $doctor->rating_cache) ? '' : '-empty'}}"></span>
                              @endfor
                              {{ number_format($doctor->rating_cache, 1)}} stars
                          </li>
                          <li>{{$doctor->rating_count}} {{ str_plural('review', $doctor->rating_count)}}</li>
                          <li>{{ $doctor->experience }} years experience</li>
                          <li>
                              @for($i=0; $i<count($doctor->serviceList); $i++)
                                {{ $doctor->serviceList[$i]->name }}
                              @endfor
                          </li>
                        </ul>
                        <button class="btn btn-info radius-0 wreview">WRITE A REVIEW</button>
                        <div class="col-xs-12 review-box" style="display:none;">
                               {{Form::open()}}
                                <input id="ratings-hidden" name="rating" type="hidden">
                                <div class="form-group">
                                    {{Form::text('title', null, array('id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter title'))}}
                                </div>
                                <div class="form-group">
                                    {{Form::textarea('comment', null, array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))}}
                                </div>
                                <div class="form-group">
                                    <span>Help the community by sharing your review of {{ $doctor->name }}</span>
                                    <div class="stars starrr param1" data-rating="{{ old('username') }}"></div>
                                </div>
                                <div class="form-group">
                                        <input type="submit" class="btn btn-success radius-0" value="SUBMIT">
                                </div>
                                {{Form::close()}}
                        </div>
                </div>
                <div class="col-md-4 col-xs-12">
                        <div class="panel panel-info">
                                <div class="panel-heading"><h2 class="margin-0">Similar Doctors</h2></div>
                                <div class="panel-body">
                                        <ul class="list-group my-list-group">
                                          <li class="list-group-item">
                                        <!-- 	<div class="col-xs-3">
                                                <img class="similar-img" src="images/expert7.jpg" />
                                                </div> -->
                                                <div class="col-xs-12">
                                                        <ul class="list-group my-list-group my-list-group1">
                                                          @if(!empty($similar_doctors))
                                                          @foreach($similar_doctors as $result)
                                                          <li class="list-group-item">
                                                                <div class="col-xs-3 padding-0  col-md-2 thumnail">
                                                                    @if($result['image'])
                                                                        <img src="{{ $result['image'] }}" class="img-circle" width="95">
                                                                    @else
                                                                        <img src="{{ asset("public/images/med_icons/doctor.png") }}" class="img-circle" width="95">
                                                                    @endif
                                                                </div>
                                                                <div class="col-xs-9 col-md-10">
                                                                    <h4><a href="{{ url('doctor/details') }}/{{ \Crypt::encrypt($result['id']) }}">{{$result['name'] }}</a></h4>
                                                                    <p class="color-grey">{{ $result['address_one'] }}</p>
                                                                    <p>
                                                                        <span class="specility">
                                                                            @if(isset($result['services']) && !empty($result['services']))
                                                                            @for($i=0; $i < count($result['services']); $i++)
                                                                               @if($i>0) & @endif {{ $result['services'][$i] }} 
                                                                            @endfor
                                                                            @endif
                                                                        </span>
                                                                        <span class="color-grey">{{ $result['experience'] }} years experience.</span>
                                                                    </p>
                                                                    <p class="color-grey">Consulting Fee <span><i class="fa fa-inr"></i>{{ $result['consultation_fee'] }}</span></p>
                                                                </div>
                                                          </li>
                                                          @endforeach
                                                          @endif
                                                        </ul>
                                                </div>
                                          </li>
                                        </ul>
                                </div>
                        </div>
                </div>
        </div>
        <!-- biography part START -->
        <div for="biography" class="row">
                <h1>About</h1>
                <hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
                <div class="clearfix"></div>
                <p>{!! $doctor->about !!}</p>
        </div>
        <!-- biography part END -->

        <!-- tabs part START -->
        <div class="row" for="tabs-part">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs-01" role="tablist">
                      <li role="presentation" class="active"><a href="#review" aria-controls="review" role="tab" data-toggle="tab">Review({{$doctor->rating_count}})</a></li>
                      <li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Services</a></li>
                      <li role="presentation"><a href="#hospitals" aria-controls="hospitals" role="tab" data-toggle="tab">Associated Hospitals</a></li>
                      <li role="presentation"><a href="#map-location" aria-controls="map-location" role="tab" data-toggle="tab">Location</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content" >
                    <div role="tabpanel" class="tab-pane active mypanel" id="review">
                        
                        <div class="col-md-6 col-xs-12">
                            <ul class="list-group my-list-group my-list-group1">
                              @foreach($reviews as $review)
                                <li class="list-group-item">
                                    <div class="col-xs-3   col-md-2 thumnail">
                                        @if($review->user->user->image)
                                            <img src="{{ $review->user->user->image }}" class="img-circle" style="width:50px;">
                                        @else
                                            <img src="{{ asset("public/images/med_icons/doctor.png") }}" class="img-circle" style="width:50px;">
                                        @endif
                                    </div>
                                    <div class="col-xs-9 col-md-10">
                                            <h4>{{ $review->user->user->name }}</h4>
                                            <h1>{{ $review->title }}</h1>
                                            <p class="color-grey write-box1">{!! $review->comment !!}</p>

                                    </div>
                                    <div class="col-xs-12 text-right">
                                        <i>Rating:</i>{{ $review->rating }}
                                        @for ($i=1; $i <= 5 ; $i++)
                                            <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                                        @endfor
                                    </div>
                                </li>
                              @endforeach
                            </ul>
                        </div>                       
                    </div>
                    <div role="tabpanel" class="tab-pane mypanel" id="services">
                        @if($doctor->serviceList()->get()->count()>0)
                        <div class="service-containers">
                            @foreach($doctor->serviceList as $service)
                            <br>{{ $service->name }}
                            @endforeach 
                        </div>
                        @endif    
                    </div>
                    <div role="tabpanel" class="tab-pane mypanel" id="hospitals">
                        @if($doctor->hospitals()->get()->count()>0)
                        @foreach($doctor->hospitals as $hospital)
                        <div class="col-xs-12 col-md-6 loop-search" >
                            <div class="col-xs-4  col-md-2 thumnail">
                                @if($hospital->image)
                                <img class="img-circle" width="95" src="{{ $hospital->image }}">
                                @else
                                <img class="img-circle" width="95" src="{{ asset('public/admin/img/user-placeholder.png') }}">
                                @endif
                            </div>
                            <div class="col-xs-12 col-md-6">
                                    <h4>{{ $hospital->name }}</h4>
                                    <p class="color-grey">{{ $hospital->address_one }}</p>
                                    <p class="color-grey">Consulting Fee <span><i class="fa fa-inr"></i>{{ $hospital->consultation_fee}}</span></p>
                            </div>
                            <div class="col-xs-12 col-md-4 text-right">
                                    <p class="color-grey">4.5 <span> 
                                    @for ($i=1; $i <= 5 ; $i++)
                                        <span class="glyphicon glyphicon-star{{ ($i <= $hospital->rating_cache) ? '' : '-empty'}}"></span>
                                    @endfor
                                    <p class="color-grey">
                                         {{ $hospital->rating_count }} {{ str_plural('review', $doctor->rating_count)}}
                                    </p>
                                    <p class="color-grey">
                                            2.5 Km <i class="fa fa-location-arrow"></i> 
                                    </p>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <p>No hospital was found.</p>
                        @endif
                    </div>
                    <div role="tabpanel" class="tab-pane mypanel" id="map-location">
                        <div class="map-cov">
                        <iframe class="m5" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d160843.97426970862!2d6.96732235!3d50.957244949999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1432776491523" width="100%" height="450" frameborder="0" ></iframe>
                    </div>                    
                    </div>
                </div>

                        </div>
        </div>
        <!-- tabs part END -->
    </div>	
</div>
<!--End services popup -->
{{ Form::close() }}
<!-- =================================
		APP DOWNLOAD PART END 
	================================== -->
@include('layouts.footer')
{{Html::script('public/js/expanding.js')}}
{{Html::script('public/js/starrr.js')}}
<script type="text/javascript">
    $('.wreview').click(function(){
            $('.review-box').slideToggle('slow');
    })
    $(function(){
        // initialize the autosize plugin on the review text area
        $('#new-review').autosize({append: "\n"});

        var reviewBox = $('#post-review-box');
        var newReview = $('#new-review');
        var openReviewBtn = $('#open-review-box');
        var closeReviewBtn = $('#close-review-box');
        var ratingsField1 = $('#ratings-hidden');
            var ratingsField2 = $('#ratings-hidden');

        openReviewBtn.click(function(e)
        {
          reviewBox.slideDown(400, function()
            {
              $('#new-review').trigger('autosize.resize');
              newReview.focus();
            });
          openReviewBtn.fadeOut(100);
          closeReviewBtn.show();
        });

        closeReviewBtn.click(function(e)
        {
          e.preventDefault();
          reviewBox.slideUp(300, function()
            {
              newReview.focus();
              openReviewBtn.fadeIn(200);
            });
          closeReviewBtn.hide();

        });

        // If there were validation errors we need to open the comment form programmatically 
        @if($errors->first('comment') || $errors->first('rating'))
          openReviewBtn.click();
        @endif

        // Bind the change event for the star rating - store the rating value in a hidden field
        $('.param').on('starrr:change', function(e, value){
          ratingsField.val(value);
        });
    });
</script>
@endsection
