@extends('layouts.home')
@section('content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
				<div class="form-group" style="margin-bottom:15px;">							
					<div class="col-md-12 text-center col-xs-12">
						<div id="image-container">
							@if($user->user->image)
									<img src="{{ $user->user->image }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>				
							@else
									<img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
							@endif
						</div>
					</div>
				</div>
				<div class="col-md-2 col-xs-12" id="left-panel">
                                    @include('account.left_nav')
				</div>
				<div class="col-md-10 col-xs-12">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="pinfo">
                                            <h3 class="head-01">Clinic Informations<hr><h3>
                                                @include('common.errors')
                                                @if (session('message'))
                                                <div class="alert alert-success">
                                                    <strong>Success!</strong> {{ session('message') }}
                                                </div>
                                                @endif
												<div class="col-sm-12 col-xs-12">
                                                    {{Form::open(['url' => 'account/search-clinics', 'method' => 'get', 'style'=>'padding-left:0px;', 'id'=>'filterForm', 'class' => 'search-form-0 col-xs-12', 'rol'=>'search'])}}
                                                        <div class="form-group">
                                                            <div class="col-xs-12 col-md-7 padding-0">
                                                                {{ Form::text('q',null,['id'=>'search-box', 'class'=>'form-control sinput sinput-claimed', 'placeholder'=>'Search and add a profile','autocomplete'=>'off']) }}
                                                                <div id="suggesstion-box"></div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-1 padding-0">
                                                                <button type="submit" id="search" class="btn btn-info search-btn search-claimed"><i class="glyphicon glyphicon-search"></i></button>
                                                            </div>
                                                        </div>
                                                    {{ Form::close() }}
                                                </div>
                                                <div class="clearfix" style="margin-top:10px;"></div>
                                                <h1 class="text-center ptb" style="color: #d9e3ea;">Didn’t find anyone with that name !</h1>
                                                <div class="col-xs-12 text-center">
                                                    <a href="{{ url('account/add-clinic') }}"><button class="btn btn-info btn-lg radius-0 add-clinic-btn" type="button">Add Profile</button></a>
                                                </div><br><br><br>
                                                @if($results)
                                                        @foreach($results as $result)
                                                        <div class="col-xs-12">
                                                                <div class="col-xs-12 loop-search" >
                                                                  <div class="col-xs-4  col-md-2 thumnail">
                                                                            @if($result['image'])
                                                                                <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                                                            @else
                                                                                <img src="{{ asset("public/img/hospital_placeholder.png") }}" class="img-circle" width="95">
                                                                            @endif
                                                                       </div>
                                                                        <div class="col-xs-12 col-md-6">
                                                                            <h4><a href="{{ url('hospital/details') }}/{{ \Crypt::encrypt($result['id']) }}">{{$result['name'] }}</a></h4>
                                                                            <p class="color-grey">{{ $result['address_one'] }}</p>
                                                                            <p> @if($result['24_hours_emergency'])<span class="color-grey">24 Hours Open</span> @endif </p> 
                                                                            <p>     
										@if(isset($result['speciality']) && !empty($result['speciality']))
                                                                                @for($i=0; ($i < count($result['speciality']) && $i<2); $i++)
                                                                                    <span class="specility">
                                                                                            @if($i>0) @endif {{ $result['speciality'][$i] }} 
                                                                                    </span>
                                                                                @endfor
                                                                                @endif
                                                                            </p>
                                                                            <p class="color-grey">Consulting Fee <span><i class="fa fa-inr"></i>{{ $result['consultation_fee'] }}</span></p>
                                                    
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-4 text-right">
                                                                            <p class="color-grey">
                                                                                @for ($i=1; $i <= 5 ; $i++)
                                                                                  <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                                                                @endfor
                                                                                {{ number_format($result['rating'], 1) }}
                                                                            </p>
                                                                            <p class="color-grey">
                                                                                    {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                                                            </p>
                                                                            <p class="color-grey">
                                                                                    {{ number_format($result['distance']) }} Km <i class="fa fa-location-arrow"></i> 
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-xs-12 text-right"></div>      
                                                                </div>
                                                        </div>
                                                        @endforeach
                                                @endif
                                                <!-- Showing the search result & claim it --->
                                        </div>
                                    </div>
				</div>
				
				
			</div>
		</div>
	</div>

    
@include('layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    "use strict";  
    $(function(){    
	$('.d-ul-left li:nth-child(3)').addClass('active');
        $("#geocomplete").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo"
        });
    });
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
