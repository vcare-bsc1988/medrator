<style>
#left-panel{background: #e4f0fa;border-bottom:2px solid #6acff0;}
#left-panel a{color: black;border-top-left-radius:6px;border-top-right-radius:6px;padding: 20px 30px;
    background: rgba(247, 253, 255, 0.52);margin: 5px 3px 0px 0px;}
#left-panel li.active a{border-top-left-radius:6px;border-top-right-radius:6px;background-color:#6acff0;color: white;}
</style>
<div class="container">
	<aside for="sidebar-tab">
		<ul class="nav  navbar-nav custom-tab-add-center" style="width" role="tablist">
			<li><a href="{{ url('account/add-doctor') }}">Add Doctor</a></li>
			<li><a href="{{ url('account/add-hospital') }}">Add Hospital</a></li>       
			<li><a href="{{ url('account/add-diagnostic') }}">Add Diagnostic Center</a></li>
		</ul>				
	</aside>
</div>			
<script>
	$(document).ready(function(){
		var aurl = window.location.href; // Get the absolute url
		$('.custom-tab-add-center li a').filter(function() { 
			return $(this).prop('href') === aurl;
		}).parent('li').addClass('active').siblings().removeClass('active');
		
	})
</script>			
