@extends('layouts.home')
@section('content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')
<div class="first-section ptb">
    <div class="col-xs-12">
        <div class="row">
            <h3 class="top-strips"></h3>
            <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
							<div class="loader-containers" style="display:none;"></div>
								<div class="loader"  style="display:none;">
									<i class="fa fa-spin fa-spinner loader-icon"></i>
								</div>
							
                        </div>
                        <i class="fa fa-pencil edit-pic"></i>
                    </div>
                </div>
            </form>
            <div class="col-md-2 col-xs-12" id="left-panel">
                @include('account.left_nav')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
                        <h3 class="head-01">Personal Information<hr></h3>
                        <div class="clearfix" style="margin-top:10px;"></div>
                        <div class="col-xs-12 padding-0">
							@include('common.errors')
                            @if (session('status') == 1)
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                            @endif
							@if (!empty(session('status')) && session('status') == 0)
                            <div class="alert alert-danger">
                                {{ session('message') }}
                            </div>
                            @endif
                            {{ Form::model($user->user,array('url' =>'profile/save','class'=>'form-horizontal', 'files' => true)) }}
                                {{ csrf_field() }}
                                {{ Form::hidden('id', $user->id) }}     
                               <div class="form-group">
                                    <div class="col-md-5 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Email ID</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                        {{ $user->email }}    
                                    </div>
                                </div>-
                                <div class="form-group">
                                        <div class="col-md-5 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">Full Name</label>
                                        </div>
                                        <div class="col-md-5 col-xs-12">
                                           {{ Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Name']) }}     
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-5 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Gender</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                        {{ Form::radio('gender', 1)}} Male &nbsp;&nbsp;&nbsp;&nbsp; {{Form::radio('gender', 2)}} Female
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class="col-md-5 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">Mobile</label>
                                        </div>
                                        <div class="col-md-5 col-xs-12">
                                            {{ Form::number('mobile', null, ['class'=>'form-control', 'placeholder'=>'Mobile','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
											@if(auth()->user()->user->is_mobile_verified ==0)
											<a href="JavaScript:void(0);" class="btn btn-link" id="verify-mobile" data-toggle="modal" data-target="#myModal">Verfy</a>
											@elseif(auth()->user()->user->is_mobile_verified ==1)
											<button type="button" class="btn btn-link">Verified</button>
											@endif
										</div>
                                </div>
                                 <div class="form-group">
                                        <div class="col-md-5 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">About</label>
                                        </div>
                                        <div class="col-md-5 col-xs-12">
                                           {{ Form::textarea('about', null, ['class'=>'form-control', 'placeholder'=>'']) }}     
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-5 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Date of Birth</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
										{{ Form::text('dob',null, ['class'=>'datepicker form-control', 'placeholder'=>'Date of Birth']) }}
									</div>
                                </div>
                                

                                <div class="form-group">
                                    <div class="col-md-5 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Address</label>
                                    </div>							
                                    <div class="col-md-5 col-xs-12">
                                        {{ Form::text('address_one', null, ['class'=>'form-control location-input', 'placeholder'=>'Location', 'id'=>'location']) }}
                                        <div class="location-details">
                                            <input type="hidden" value="" name="latitude" data-geo="lat">
                                            <input type="hidden" value="" name="longitude" data-geo="lng">
                                            <input type="hidden" value="" name="locality" data-geo="locality">
                                            <input type="hidden" value="" name="city_name" data-geo="locality">
                                            <input type="hidden" value="" name="state_name" data-geo="administrative_area_level_1">
                                            <input type="hidden" value="" name="country_name" data-geo="country">
                                            <input type="hidden" value="" name="pincode" data-geo="postal_code">
                                        </div>
                                    </div>							
                                </div>
                                <div class="form-group">
                                    <div class="col-md-5 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Interests</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
										<div id="content3" class="clear">
											{{ Form::select('interests[]', $interests,$user_interests, ['class' => '1col active form-control','multiple'=>true]) }}
										</div>
									</div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-5 col-xs-12">

                                    </div>							
                                    <div class="col-md-5 col-xs-12">
                                            <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="SAVE" />
                                    </div>							
                                </div>
                            {{ Form::close() }}
                        </div>							
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Enter OTP</h4>
			</div>
			<div class="modal-body">
				@if (!empty(session('error_status')) && session('error_status') == 'failed')
				<div class="alert alert-danger">
					{{ session('erro_message') }}
				</div>
				@endif
				<form action="{{'account/verify-mobile'}}" method="post" id="verify-mobile-form">
					{{ csrf_field() }}
					<input type="text" required name="otp">
					<button type="submit" class="btn btn-success">Verify</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
    </div>
</div>                 
@include('layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    "use strict";
	$('#verify-mobile').click(function(){ 
		$.ajax({
            type: "POST",
            url: "{{ url('account/send-otp') }}",
            data:{"_token":"{{ csrf_token() }}","id":"{{ auth()->user()->id }}"},
            success: function(result){ 
               console.log(result);
            },
            error:function(result){
                console.log(result);
            }
        });
	});
    $('.edit-pic').click(function(){
        $('.ifile').click();
    })
    $("#image").change( function (){
        //alert($("#uploadImage").attr('action'));
		
        var form_upload = new FormData();
		form_upload.append('constructor',$("#uploadImage"));
        form_upload.append("_token","{{csrf_token()}}");
        var imagefile = $("input[type='file']#image").eq(0)[0].files[0];
        form_upload.append("photo", imagefile);
        form_upload.append("id", $("input[name='id']").val());
		$('.loader-containers').show();
                    $('.loader').show();
        $.ajax({		
            //Getting the url of the uploadphp from action attr of form 
            //this means currently selected element which is our form  
            url: $("#uploadImage").attr('action'),

            //For file upload we use post request
            type: "POST",

            //Creating data from form 
            data: form_upload,

            //Setting these to false because we are sending a multipart request
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){
                    $('.loader-containers').show();
                    $('.loader').show();
            },
            success: function(data){ 
                    //console.log(data);
                    $('.loader-containers').hide();
                    $('.loader').hide();
                    //If the request is successfull we will get the scripts output in data variable 
                    //Showing the result in our html element 
                    $('#image-container').html(data);
            },
            error: function(data){
                    //console.log(data);
            }
        });
    })
    	
    $('.add-clinic-btn').click(function(){
        $('.add-clinic-form').slideToggle()
    })
    $("#location").geocomplete({
            details: ".location-details",
            detailsAttribute: "data-geo"
    });
    $('.d-ul-left li:nth-child(2)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(2)').addClass('active');	 
    /**
     * Mobile field validation
     */
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    /***
     * Hide notify(success/failed) message
     */
    setTimeout(function () {$('.alert').hide();},4000);
	
	$(document).ready(function(){
		/**
		 Showing OTP model if errors
		 */
		var status = "{{ session('error_status') }}";
		if(status =='failed')
			$('#myModal').modal('show');
		
			
	})
</script>
{{-- <script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script> --}}
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
