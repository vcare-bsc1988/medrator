<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Profile Approved</title>
</head>
<p>Dear Member,<br></p>
<h1></h1>
<p>Your request to add {{ $data['entity_name'] }} HERE on Medrator has been rejected because of one of the following reasons:</p>
<h1></h1>
<p><ul>
	<li>Record already exists in our database</li>
	<li>Incorrect information submitted</li>
	<li>We are unable to verify the records shared by you</li>
</ul></p>
<h1></h1>
<p>Please reconsider adding the record again if you have corrected the above errors related to this request.</p>
<h1></h1>
<p>Best Regards,<br>Medrator Team</p>
<body>
</body>
</html>