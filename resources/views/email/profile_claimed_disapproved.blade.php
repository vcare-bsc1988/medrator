<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Email on rejection of claim profile</title>
</head>
<body>
<p>Dear Member {{-- {{ $data['user_name'] }} --}},</P>
<h1></h1>
Your request for claiming profile of {{ $data['entity_name'] }} Medrator has been rejected because of one of the following reasons:</p>
<h1></h1>
<p><ul>
	<li>We are unable to verify your credentials required for approval</li>
	<li>We are unable to reach you for verification</li>
</ul></p>
<h1></h1>
<P>Best Regards,<br>
Medrator Team</p>
</body>
</html>