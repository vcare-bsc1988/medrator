@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Hospital Info</div>

                <div class="panel-body">
                    @if (session('success'))
                        <div class="flash-message">
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        </div>
                    @endif
                    @include('common.errors')                   
                    {{ Form::open(array('url' => 'hospital', 'method' => 'post','files' => true)) }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                            <?=$errors->first('name','<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('service') ? ' has-error' : '' }}">
                            {{ Form::label('service', 'Service') }}
                            {{ Form::select('service[]', $services, null, ['class' => 'form-control','multiple'=>true]) }}
                            <?=$errors->first('service', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('specialty') ? ' has-error' : '' }}">
                            {{ Form::label('specialty', 'Specialty') }}
                            {{ Form::select('specialty[]', $specialties, null, ['class' => 'form-control','multiple'=>true]) }}
                            <?=$errors->first('specialty', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('no_of_doctors') ? ' has-error' : '' }}">
                            {{ Form::label('no_of_doctors', 'No of Doctors') }}
                            {{ Form::text('no_of_doctors', null, ['class' => 'form-control']) }}
                            <?=$errors->first('no_of_doctors', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('no_of_beds') ? ' has-error' : '' }}">
                            {{ Form::label('no_of_beds', 'No of Beds') }}
                            {{ Form::text('no_of_beds', null, ['class' => 'form-control']) }}
                            <?=$errors->first('no_of_beds', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('icu_facility') ? ' has-error' : '' }}">
                            {{ Form::label('icu_facility', 'Icu Facility') }}
                            {{ Form::checkbox('icu_facility', 1) }}
                            <?=$errors->first('icu_facility', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('diagnostic_lab_facility') ? ' has-error' : '' }}">
                            {{ Form::label('diagnostic_lab_facility', 'Diagnostic Lab Facility') }}
                            {{ Form::checkbox('diagnostic_lab_facility', 1) }}
                            <?=$errors->first('diagnostic_lab_facility', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('24_hours_emergency') ? ' has-error' : '' }}">
                            {{ Form::label('24_hours_emergency', '24 Hours Emergency') }}
                            {{ Form::checkbox('24_hours_emergency', 1) }}
                            <?=$errors->first('24_hours_emergency', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            {{ Form::label('image', 'Upload Photo (optional)') }}
                            {{ Form::file('image') }}</br></br>
                            <?=$errors->first('image', '<p class="help-block">:message</p>')?>
                        </div>
                         <div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
                            {{ Form::label('cashless_mediclaim', 'Cashless Mediclaim') }}
                            {{ Form::checkbox('cashless_mediclaim', 1) }}
                            <?=$errors->first('cashless_mediclaim', '<p class="help-block">:message</p>')?>
                        </div>
						<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            {{ Form::label('country', 'Country') }}
                            {{ Form::select('country',['India'=>'India'],null,['class' => 'form-control']) }}
                            <?=$errors->first('country', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            {{ Form::label('address', 'Address') }}
                            {{ Form::text('address', null, ['class' => 'form-control']) }}
                            <?=$errors->first('address', '<p class="help-block">:message</p>')?>
                        </div>
						<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            {{ Form::label('state', 'State') }}
                            {{ Form::select('state', [' ' => 'Select State'] +\App\Model\States::where('country_id',101)->lists('name','id')->toArray(),null,['class' => 'form-control','onChange'=>'getCity(this.value)'])}}
                            <?=$errors->first('state', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            {{ Form::label('city', 'City') }}
                            {{ Form::select('city', [' '=>'Select City'], null,['id'=>'city-list','class' => 'form-control']) }}
                            <?=$errors->first('city', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('pincode') ? ' has-error' : '' }}">
                            {{ Form::label('pincode', 'Pincode') }}
                            {{ Form::text('pincode', null, ['class' => 'form-control']) }}
                            <?=$errors->first('pincode', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Save!') }}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function getCity(val) {
    $.ajax({
        type: "POST",
        url: "{{ url('get_city') }}",
        data:'state_id='+val,
        success: function(data){ 
        $("#city-list").html(data);
        }
    });
}
</script>
@endsection
