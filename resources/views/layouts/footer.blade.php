<!--==================================
		footer parts starts here
	  ==================================-->     
	  
<footer class="footer p82-topbot">
<div class="container">
<div class="row">
	<div class="col-sm-6 col-md-3 col-lg-3 all-need">
		<h2>Address</h2>
		<div class="faddress">
		   Medrator<br>Sector 31, Gurugram, Haryana (India)<br>
		   <span>General Email:</span> <a href="mailto:contactus@medrator.com">contactus@medrator.com</a><br>
		   <span>Technical Issues :</span> <a href="mailto:administrator@medrator.com">administrator@medrator.com</a>
		</div>
	</div><!-- col-sm-3 -->
	<div class="col-sm-6 col-md-3 col-lg-3 all-need list-styles">
		<h2>Service</h2>
		<ul>
                    @foreach(\App\Model\Specialties::select('id','name')->orderBy('is_top', 'desc')->skip(0)->take(7)->get() as $specialty)
                    <li><a href="{{ url('find/doctor')}}/{{ session('address')['state_name'] }}/{{ $specialty->name }}">{{ $specialty->name }}</a></li>
                    @endforeach 
		</ul>
	</div>
	<div class="col-sm-6 col-md-3 col-lg-3 all-need list-styles">
		<h2>Medrator</h2>
		<ul>
			<li><a href="{{ url('about-us')}}" target="new">About Us</a></li>
			<li><a href="{{ url('contact-us')}}" target="new">Contact Us</a></li>
			<li><a href="{{ url('copyright')}}" target="new">Copyright</a></li>
			<li><a href="{{ url('privacy-policy')}}" target="new">Privacy Policy</a></li>
			<li><a href="{{ url('terms-of-service')}}" target="new">Terms of Service</a></li>
			<li><a href="{{ url('organ-donation')}}" target="new">Organ donation with third part link</a></li>
			<li><a href="{{ url('medical-tourism')}}" target="new">Medical Tourism Section</a></li>
			<li><a href="{{ url('buy-insurance')}}" target="new">Buy Insurance / Fitness product</a></li>	
		</ul>
	</div>
	<div class="col-sm-6 col-md-3 col-lg-3 all-need list-styles">
		<h2>news letter</h2>
		<div class="news-letter">
			<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
			<style type="text/css">
				#mc_embed_signup{background:transparent; clear:left; font:14px Helvetica,Arial,sans-serif; }
				#mc-embedded-subscribe{position: absolute !important;float: none !important;right: 0 !important;top: 0 !important;height: 44px !important;background:#333 !important;width:auto !important;margin:0px !important;}
				#mce-responses{    margin: 0px !important;
				padding: 0px !important;}
				/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
				   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
			</style>
			<div id="mc_embed_signup">
			<form action="//medrator.us15.list-manage.com/subscribe/post?u=9fe5a7aa817162a89ceba0d82&amp;id=7dd23c553d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" style="padding:0px !important;" class="validate" target="_blank" novalidate>
				<div id="mc_embed_signup_scroll">
				
					<!--<div class="mc-field-group">
						<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
					</label>-->
						<input type="email" value="" name="EMAIL" class="required email" placeholder="Enter Your Email" id="mce-EMAIL">
					</div>
					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9fe5a7aa817162a89ceba0d82_7dd23c553d" tabindex="-1" value=""></div>
					<div class="clear"><input type="submit" value="Go" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none;color:white;"></div>
					</div> 
				</div>
			</form>
			</div>
			<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
			<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);setTimeout(function () {$('.response').hide();},4000);</script>
			<!--End mc_embed_signup-->
		</div><!-- /.new-letter -->
	</div><!-- col-sm-3 -->
</div><!-- ./row -->
</div><!-- /.container --> 
	   
</footer>
<div class="copyrights text-left">
	<div class="container">
		<span class="pull-left white">Medrator © 2016 | All Rights Reserved</span>
		<ul class="nav navbar-nav pull-right-large social-icons">
			<li class="pull-left-small"><a href="https://twitter.com/medrator" class="padding-0 white"><i class="fa fa-twitter"></i></a></li>
			<li class="pull-left-small"><a href="https://www.facebook.com/Medrator-1319094681504203/" class="padding-0 white"><i class="fa fa-facebook"></i></a></li>
			<li class="pull-left-small"><a href="javascript:;" class="padding-0 white"><i class="fa fa-linkedin"></i></a></li>
		</ul>
	</div>
</div><!-- /.copy-rights -->
<!-- Login Modal START -->
<style type="text/css">
	.pac-container{z-index:1100000;}
</style>
<!-- reset password popup START -->
	<div class="modal fade" id="reset-pwd" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">Reset Password</h4></div>
				<div class="modal-body">
					<!--- Reset password model @$poup_tyep =5--->
					@if(isset($popup_type) && $popup_type==5)
					<div class="reset-password-model">
						@if (session('message'))
							<div class="alert alert-success">
								{{ session('message') }}
							</div>
						@endif
						<form class="form-horizontal" role="reset-password-form" method="POST" action="{{ url('/password/reset') }}">
							{{ csrf_field() }}
							<input type="hidden" name="token" value="{{ $token }}">
							<div class="modal-body" style="overflow:hidden;">
								<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
									<label>Email ID</label>
									<input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}">
									@if ($errors->has('email'))
										<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
								</div>   
							</div>
							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<div class="col-md-12">
								<label for="password">Password</label>
									<input id="password" type="password" class="form-control" name="password">

									@if ($errors->has('password'))
										<span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
									@endif
								</div>
							</div>
							<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
									<div class="col-md-12">
									<label for="password-confirm">Confirm Password</label>
									<input id="password-confirm" type="password" class="form-control" name="password_confirmation">

									@if ($errors->has('password_confirmation'))
										<span class="help-block">
											<strong>{{ $errors->first('password_confirmation') }}</strong>
										</span>
									@endif
									
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-info btn-block btn-lg radius-0"><i class="fa  fa-refresh"></i> RESET PASSWORD</button>
								<!--<button type="button" class="btn btn-default btn-block btn-lg radius-0 btl-btn">BACK TO SIGN IN</button>-->
								</h3>
							</div>	  
						</form>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
<!-- reset password popup END -->


<div class="modal fade" id="signin-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title log-head" id="myModalLabel">Sign In</h4>
                    <h4 class="modal-title reg-head" id="myModalLabel" style="display:none;">Register</h4>
                    <h4 class="modal-title fp-head" id="myModalLabel" style="display:none;">Forgot Password</h4>
		</div>
		<!-- forgot password form START -->
		<div class="fp-form" style="display:none;">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}
                        <div class="modal-body" style="overflow:hidden;">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                @if (session('message'))
                                    <div class="alert alert-success">
                                            {{ session('message') }}
                                    </div>
                                @endif
                                <label>Email ID</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>   
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info btn-block btn-lg radius-0">SEND</button>
                            <button type="button" class="btn btn-default btn-block btn-lg radius-0 btl-btn">BACK TO SIGN IN</button>
                            </h3>
                        </div>	  
                    </form>
		</div>
		
		<!-- login form START -->
		<div class="log-form">
                    <form role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="modal-body" style="overflow:hidden;">
                            <div class="form-group">
								@if (session('message'))
									<div class="alert alert-success">
											{{ session('message') }}
									</div>
								@endif
							</div>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label>E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control"/>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 padding-0">
                                    <div class="squaredOne" style="margin-left:0px;">
                                            <input type="checkbox"  id="squaredThirteen" name="remember"/>
                                            <label for="squaredThirteen"></label>
                                    </div>
                                    <label class="nms">Remember Me</label>
                                </div>
                                <div class="col-md-6 padding-0 text-right">
                                        <a href="javascript:;" class="fp-btn">Forgot Password?</a>
                                </div>
                            </div>		   
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info btn-block btn-lg radius-0">SIGN IN</button>
                            <button type="button" class="btn btn-default btn-block btn-lg radius-0 s-btn">SIGN UP</button>
                            <hr>
                            <div class="col-md-6" style="padding:2px 2px 0px 0px;">
                                    <a href="{{ url('/redirect/facebook') }}"><img src="{{ asset('public/images/fbs.png')}}"/></a>
                            </div>
                            <div class="col-md-6" style="padding:2px 0px 0px 2px;">
                                    <a href="{{ url('/redirect/google') }}"><img src="{{ asset('public/images/gp.png')}}"/></a>
                            </div>
                            </h3>
                        </div>	  
                    </form>
		</div>
		<!-- login form END -->
		
		<!-- registration form START -->
		<!-- sign up form START -->
		<div class="sign-form" style="display:none">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body">
                                <!--<span class="alert alert-success w100">Updated Successfully !!!</span>-->
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label>Email ID</label>
                                        <input type="email" name="email" value="{{ old('email')}}" class="form-control" placeholder="Email ID"/>
                                        @if ($errors->has('gender'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password</label>
                                    <input type="password" value="{{ old('password')}}" name="password" class="form-control"/>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Full Name</label>
                                    <input type="text" name="name" value="{{ old('name')}}" class="form-control" placeholder="Full Name"/>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                        <label>Mobile Number</label>
                                        <input type="text" name="mobile" value="{{ old('mobile')}}" class="form-control" placeholder="Mobile Number" onkeypress="return isNumber(event)" maxlength="10" />
                                        @if ($errors->has('mobile'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mobile') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                                        <label>Date of Birth</label>
                                        <input type="text" class="datepicker form-control" name="dob" value="{{ old('dob')}}" />
                                        <span class="input-group-adon input-group-adon-datepicker">
                                            <span class="fa fa-calendar"></span>
                                        </span>
										@if ($errors->has('dob'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dob') }}</strong>
                                            </span>
                                        @endif
                                </div>
                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                    <label>Gender</label>
                                    <div class="col-xs-12 padding-0">
                                        <div class="col-xs-6 col-md-6 padding-0">
                                            <div class="cirlceddOne g1 pull-left">
                                                <input name="gender" type="radio" value="{{ old('gender')?old('gender'):1}} " id="circledOne" name="gender">
                                                    <label for="squaredOne"></label>
                                            </div>								
                                            <label class="nms pull-left">Male</label>
                                        </div>
                                        <div class="col-xs-6 col-md-6 padding-0">
                                            <div class="circledOne g1 pull-left">
                                                    <input type="radio" value="{{ old('gender')?old('gender'):2}} " id="circledTwo" name="gender" />
                                                    <label for="squaredOne"></label>
                                            </div>								
                                            <label class="nms pull-left">Female</label>
                                        </div>
                                    </div>
                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
                                    <label>Address</label>
                                    <input type="text" name="address_one" value="{{ old('address_one') }}" id="location-reg" class="form-control" placeholder="Location"/>
                                    @if ($errors->has('address_one'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address_one') }}</strong>
                                        </span>
                                    @endif
                                    <div class="location-details-reg">
                                        <input type="hidden" value="" name="latitude" data-geo="lat">
                                        <input type="hidden" value="" name="longitude" data-geo="lng">
                                        <input type="hidden" value="" name="locality" data-geo="locality">
                                        <input type="hidden" value="" name="city_name" data-geo="locality">
                                        <input type="hidden" value="" name="state_name" data-geo="administrative_area_level_1">
                                        <input type="hidden" value="" name="country_name" data-geo="country">
                                        <input type="hidden" value="" name="pincode" data-geo="postal_code">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label>Profile Image</label>
                                        <input type="file" name="image" style="margin-top:20px;"/>
                                </div>
								<div class="form-group{{ $errors->has('agree_terms') ? ' has-error' : '' }}">
									<input type="checkbox" name="agree_terms" value="1" style="margin-top:20px;visibility:visible;"/> Agree to <a href="{{ url('terms-of-service') }}" target="new">Terms and conditions.</a>
									@if ($errors->has('agree_terms'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('agree_terms') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info btn-block btn-lg radius-0">Sign Up</button>
                            <button type="button" class="btn btn-default btn-block btn-lg radius-0 si-btn">SIGN IN</button>
                        </div>	  
                </form>
		</div>
		<!-- sign up form END -->
		<!-- registration form END --> 
            </div>		
    </div>
  </div>
<!-- Login Modal END -->
<div class="modal fade" id="message-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog w400" role="document">
      <div class="modal-content "> 
		 
		   
          @if(session('status')==1)
           <div class="modal-body text-center">                              
               <p><span class="text-info f50"><i class="fa fa-check-square"></i></span></p>
				
			   <h1>Thank you!</h1>
			   <p style="margin-top:15px;">We have mailed you a link on your registered email id.Please click on that to activate your account.</p>
			   
			   <p class="text-info" data-dismiss="modal" style="cursor:pointer;margin-top:20px;">Close It</p>
          </div>
          @endif
          @if(session('status')==0)
          <div class="modal-body text-center">                              
               <p><span class="text-danger f50"><i class="fa fa-window-close"></i></span></p>
				
			   <h1>Failed!</h1>
			   <p style="margin-top:15px;">Due to some reason, your registration could'nt be completed.</p>
			   
			   <p class="text-info rg-again" data-dismiss="modal" style="cursor:pointer;margin-top:20px;">Try Again</p>
          </div>
          @endif
      </div>
    </div>
</div> 
<script type="text/javascript">
$(document).ready(function(){
	$('.notification-list').hide();
	$('.noti-icos').click(function(){
		$('.notification-list').slideToggle();
	})
	/* $('#reset-pwd').modal(); */
	var aurl = window.location.href; // Get the absolute url
		$('.main-menus li').find('a').filter(function() { 			
			return $(this).prop('href') === aurl;
			
		}).addClass('active');
		
})
		
		$(document).on('click','.rg-again',function(){
			
			$('.fp-form,.fp-head').hide();
			$('.log-form,.log-head').hide();
			$('.sign-form,.reg-head').show();
			$('#signin-form').modal();
		})
		function isNumber(evt) {
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			}
			return true;
		}
	$("#location-reg").geocomplete({
		details: ".location-details-reg",
		detailsAttribute: "data-geo"
	});
</script>