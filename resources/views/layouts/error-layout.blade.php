<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<!-- Mirrored from tradeskerala.com/medical/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 26 Aug 2016 06:44:57 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Medical Theams">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Medrator</title>
    <link rel="stylesheet" href="{{ asset("public/css/inputTags.css") }}">
    <link rel="stylesheet" href="{{ asset("public/css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset("public/css/custom-variation2.css") }}">
    <link rel="stylesheet" href="{{ asset("public/css/responsive.css") }}"> 
    <link rel="stylesheet" href="{{ asset("public/css/style.css") }}"> 
    

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lora:400,400italic' rel='stylesheet' type='text/css'>
    
    <!-- choosen js and style START -->
    <link href="{{ asset("public/css/chosen.css") }}" rel="stylesheet" type="text/css">
    <!-- wizard style/js START -->
    <link href="{{ asset("public/css/jquery.wizard.css") }}" rel='stylesheet' type='text/css'>
    <!-- Range Slider style/script START-->
    <link rel="stylesheet" href="{{ asset("public/js/rangeslider/range_picker.min.css") }}">

    
    <script src="{{ asset("public/js/vendor/jquery-min.js") }}"></script>
    <script src="{{ asset("public/js/custom.js") }}"></script>
    <script src="{{ asset("public/js/jquery.validate.min.js") }}"></script>
	
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC00fGCV5rT-LH9xZZVwjnTn39jW64URLM&amp;libraries=places"></script>
    <script src="{{ asset('public/js/jquery.geocomplete.min.js') }}"></script>   
</head>
<body class="in-align">
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="outer-sync var2">
        <!--Content start -->
        @yield('content')
        <!--Finished-->                
    </div><!-- /.outer-sync -->
	<script src="{{ asset('public/js/vendor/jquery-min.js') }}"></script>
    <script src="{{ asset('public/js/vendor/modernizr.custom.68477.js') }}"></script>
    <script src="{{ asset('public/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/js/plugins.js') }}"></script>
</body>
</html>