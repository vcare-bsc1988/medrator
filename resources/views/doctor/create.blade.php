@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Doctor Info</div>

                <div class="panel-body">
                    @if (session('success'))
                        <div class="flash-message">
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        </div>
                    @endif
                    @include('common.errors')                   
                    {{ Form::open(array('url' => 'doctor', 'method' => 'post','files' => true)) }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                            <?=$errors->first('name','<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            {{ Form::label('gender', 'Gender') }}<br>
                            Male{{ Form::radio('gender', 1)}} Female: {{Form::radio('gender', 2)}}
                            <?=$errors->first('gender', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('service') ? ' has-error' : '' }}">
                            {{ Form::label('service', 'Service') }}
                            {{ Form::select('service[]', $services, null, ['class' => 'form-control','multiple'=>true]) }}
                            <?=$errors->first('service', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('specialty') ? ' has-error' : '' }}">
                            {{ Form::label('specialty', 'Specialty') }}
                            {{ Form::select('specialty[]', $specialties, null, ['class' => 'form-control','multiple'=>true]) }}
                            <?=$errors->first('specialty', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('registration_no') ? ' has-error' : '' }}">
                            {{ Form::label('registration_no', 'Registration No (optional)') }}
                            {{ Form::text('registration_no', null, ['class' => 'form-control']) }}
                            <?=$errors->first('registration_no', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            {{ Form::label('website', 'Website (optional)') }}
                            {{ Form::text('website', null, ['class' => 'form-control']) }}
                            <?=$errors->first('website', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            {{ Form::label('mobile', 'Mobile (optional)') }}
                            {{ Form::text('mobile', null, ['class' => 'form-control']) }}
                            <?=$errors->first('mobile', '<p class="help-block">:message</p>')?>
                        </div>
						<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            {{ Form::label('country', 'Country') }}
                            {{ Form::select('country',['India'=>'India'],null,['class' => 'form-control']) }}
                            <?=$errors->first('country', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            {{ Form::label('address', 'Address') }}
                            {{ Form::text('address', null, ['class' => 'form-control']) }}
                            <?=$errors->first('address', '<p class="help-block">:message</p>')?>
                        </div>
						<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            {{ Form::label('state', 'State') }}
                            {{ Form::select('state', [' ' => 'Select State'] +\App\Model\States::where('country_id',101)->lists('name','id')->toArray(),null,['class' => 'form-control','onChange'=>'getCity(this.value)'])}}
                            <?=$errors->first('state', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            {{ Form::label('city', 'City') }}
                            {{ Form::select('city', [' '=>'Select City'], null,['id'=>'city-list','class' => 'form-control']) }}
                            <?=$errors->first('city', '<p class="help-block">:message</p>')?>
                        </div>
                
                        <div class="form-group{{ $errors->has('pincode') ? ' has-error' : '' }}">
                            {{ Form::label('pincode', 'Pincode') }}
                            {{ Form::text('pincode', null, ['class' => 'form-control']) }}
                            <?=$errors->first('pincode', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('awards') ? ' has-error' : '' }}">
                            {{ Form::label('awards', 'Awards (optional)') }}
                            {{ Form::text('awards', null, ['class' => 'form-control']) }}
                            <?=$errors->first('awards', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            {{ Form::label('image', 'Upload Photo (optional)') }}
                            {{ Form::file('image') }}</br></br>
                            <?=$errors->first('image', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Save!') }}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>
function getCity(val) {
    $.ajax({
        type: "POST",
        url: "{{ url('get_city') }}",
        data:'state_id='+val,
        success: function(data){ 
		//console.log(data);
        $("#city-list").html(data);
        }
    });
}
</script>
@endsection
