@extends('layouts.home')

@section('content')
<style>
.other-schedule{display:none;}
#other-schedule{display:none;}
.my-list-group li{border:none;padding:2px 0px;}
.my-list-group1 li{padding:10px 0px;float:left;width:100%;}
.color-blue{color: #29b9e8;}
.radius-0{border-radius:0px;}
.w100{width:100%;}
.similar-img{border: 1px solid #ccc;border-radius: 50%;width:60px;height:60px;}
.review-box,.write-box1{position:relative;padding:10px !important;    border: 1px solid #f0f0f0;
    margin-top: 10px;}
.review-box:before{content:'';position:absolute;width:0;left:15px;right:0;top:-11px;border-bottom:10px solid #f0f0f0;border-left:10px solid transparent;border-right:10px solid transparent;z-index:1;}
.review-box:after{content:'';position:absolute;width:0;left:15px;right:0;top:-10px;border-bottom:10px solid #fff;border-left:10px solid transparent;border-right:10px solid transparent;z-index:2;}
.row[for="tabs-part"]{margin-top:30px;}
.mypanel{padding:15px;background:#e4f0fa;overflow:hidden;}
.tabs-01 li a{background:#e4f0fa;color:#5b5b5b !important;border-bottom:2px solid #5b5b5b;outline:none !important;}
.tabs-01 li.active a,.tabs-01 li.active a:focus{background:#31b0d5;color:white !important;border-top-color:#31b0d5;border-left-color:#31b0d5;border-right-color:#31b0d5;border-bottom:2px solid #1a8eb5;}
.write-box1:before{content:'';position:absolute;width:0;left:15px;right:0;top:-11px;border-bottom:10px solid #f0f0f0;border-left:10px solid transparent;border-right:10px solid transparent;z-index:1;}
.write-box1:after{content:'';position:absolute;width:0;left:15px;right:0;top:-10px;border-bottom:10px solid #fff;border-left:10px solid transparent;border-right:10px solid transparent;z-index:2;}
.service-containers{float:left;padding:10px;border: 1px dashed #a9a4a4; margin: 10px; color: #505050;}
.marker1{height: 18px;float: left;margin-top: 6px;width: 17px;}
.loop-search1{background: #e4f0fa !important;}
.loop-aftr{padding:10px !important;}
.schedule-times:first-of-type{float:left;}
.schedule-times:not(:first-of-type){ clear: both;
    float: left;
    margin-left: 0px;}
	.quali-head{color:#87daf6;margin-top:20px;}
	.read-mores{color: #5bc0de;position: absolute;margin-top: 45px;cursor:pointer;}
	.hide-mores{color:darkred;cursor:pointer;}
	.height50{height:50px;-webkit-transition-duration:3s;}
#double{
  width:760px;
  margin-bottom:20px;
  overflow:hidden;
}
#double li{
  line-height:1.5em;
  float:left;
  display:inline;
}
#bookmark-msg{display:none;position: absolute; left: 0;z-index: 10;right: 0;margin: 0 auto;
    width: 150px;font-size: 14px;box-shadow: 0px 0px 15px rgba(136, 133, 133, 0.33);text-align: center;}
#bookmark{cursor: pointer;font-size: 18px;float: right;margin-top: 10px;}
#double li  { width:50%;} 
#triple li  { width:33.333%; } 
#quad li    { width:25%; } 
#six li     { width:16.666%; } 
.ls:last-child{padding-bottom:10px !important;}
.timings article p:not(:first-child) {
    margin-left: 0px !important;
}
</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')									   
<!--==================================
	 Header parts ends here
  ==================================-->
<!--==================================
              service Quick links starts here
        ==================================--> 
<div class="first-section ptb">
    <div class="container">
		<div class="row">
			<div class="col-md-12">
			  @if(Session::get('errors'))
				<div class="alert alert-danger">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				   {{--<h5>There were errors while submitting this review:</h5>--}}
				   @foreach($errors->all('<li>:message</li>') as $message)
					  <?=$message?>
				   @endforeach
				</div>
			  @endif
			  @if(Session::has('review_posted'))
				<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <h5>Your review has been submitted for approval.</h5>
				</div>
			  @endif
			  @if(Session::has('review_removed'))
				<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <h5>Your review has been removed!</h5>
				</div>
			  @endif
			</div>
		</div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="thumbnail" style="overflow:hidden;">
                    @if($hospital->image)
                        <img src="{{ $hospital->image }}" class="w100">
                    @else
                        <img src="{{ asset("public/img/hospital_placeholder.png") }}" class="img-circle w100">
                    @endif
                </div>
                <div class="claim-hospital">
					@if(Auth::check())
					{{Form::open(['url' => 'account/claiming-hospital', 'method' => 'post','onsubmit'=>'return confirm("'.'Claiming this profile means that you officially represent '.$hospital->name.'. Are you sure you want to proceed?'.'")'])}}
                    @else
					{{Form::open(array('id' => 'claim-form'))}}	
					@endif
                    {{ Form::hidden ('id', $hospital->id) }}
                    {{ Form::submit('Claim this Profile',['class'=>'btn btn-default']) }}
                    {{Form::close()}}
                </div><br>
                @if (session('success'))
                <div class="alert alert-success">
                        {{ session('success') }}
                </div>
                @endif
            </div>
                <div class="col-md-4 col-xs-12">
					<h3 style="margin-top:0px;">{{ $hospital->name }} <i rel="{{ $hospital->id }}" id="bookmark" class="fa fa-bookmark" data-toggle="tooltip" title="Bookmark" data-placement="top"></i><span class="alert alert-success" id="bookmark-msg"><span></h3>
					<ul class="list-group my-list-group" style="list-style:none;">
					  <li>
					  @if( number_format($hospital->rating_cache) !=0)
						<a href="{{ url('reviews') }}?hospital={{ $hospital->id }}">
						{{ round($hospital->rating_cache, 1)}}
						@for ($i=1; $i <= 5 ; $i++)
							<span class="color-grey">	
							{{-- <span class="glyphicon glyphicon-star{{ ($i <= $hospital->rating_cache) ? '' : '-empty'}}"></span> --}}
							<span class="fa fa-star<?php if($i < $hospital->rating_cache or $i == $hospital->rating_cache){ echo '';}elseif($i-1 < $hospital->rating_cache &&  $hospital->rating_cache < $i){ echo '-half'; } else{ echo '-o';} ?>"></span>
							</span>
						@endfor<br>
						({{count($reviews )}} {{ str_plural('review',count($reviews ))}})
						</a>
						@else
							No Reviews
						@endif
					  </li>
					  <li>
						  @foreach($hospital->specialties()->skip(0)->take(1)->get() as $specialty) 
							{{ $specialty->specialty->name }}
						  @endforeach
					  </li>
					  <li>
					  @if($hospital->consultation_fee !=0)
						<i class="fa fa-inr"></i>{{ $hospital->consultation_fee }}
					@else
						(Fee data not available with us.)
					@endif
					</li>
					</ul>
					<div class="text-left">
						<a href="#reviews-anchor" id="open-review-box" class="btn btn-success btn-green">Add Review</a>
					</div>
					<div class="row" id="post-review-box" style="display:none;">
						<div class="col-md-12">
							{{Form::open(array('id' => 'review-form'))}}
							<input id="id" name="id" value="{{ $hospital->id }}" type="hidden">
							<input id="ratings-hidden" name="rating" value="{{ old('rating') }}" type="hidden">
							<input id="ratings-hidden1" name="ratings[]" value="{{ old('ratings')[0] }}" type="hidden">
							<input id="ratings-hidden2" name="ratings[]" value="{{ old('ratings')[1] }}" type="hidden">
							<input id="ratings-hidden3" name="ratings[]" value="{{ old('ratings')[2] }}" type="hidden">
							<input id="ratings-hidden4" name="ratings[]" value="{{ old('ratings')[3] }}" type="hidden">
							<input id="ratings-hidden5" name="ratings[]" value="{{ old('ratings')[4] }}" type="hidden">
							<input id="ratings-hidden6" name="ratings[]" value="{{ old('ratings')[5] }}" type="hidden">
							<div class="form-group">
							   Overall Rating<sup><span style="color:red;">*</span></sup> <span class="color-grey" style="letter-spacing: 3px;"><div class="stars starrr param" data-rating="{{ old('username') }}"></div></span>
							</div>
							<div class="form-group">
								Review Title<sup><span style="color:red;">*</span></sup> {{Form::text('title', old('title'), array('id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter title'))}}
							</div>
							<div class="form-group">
								For which disease you visited {{Form::text('disease_visited', old('disease_visited'), array('id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter title'))}}
							</div>
							<div class="form-group text-right">
								<div class="col-xs-12 padding-0" style="margin-bottom:8px;">
									<span class="pull-left">Effectiveness</span>
									<span class="color-grey" style="letter-spacing: 3px;"><div class="stars starrr param1" data-rating="{{ old('username') }}"></div></span>
								</div>
								<div class="col-xs-12 padding-0" style="margin-bottom:8px;">
									<span class="pull-left">Helpfulness</span>
									<span class="color-grey" style="letter-spacing: 3px;"><div class="stars starrr param2" data-rating="{{ old('username') }}"></div></span>
								</div>
								<div class="col-xs-12 padding-0" style="margin-bottom:8px;">
									<span class="pull-left">Worth Recommending</span>
									<span class="color-grey" style="letter-spacing: 3px;"><div class="stars starrr param3" data-rating="{{ old('username') }}"></div></span>
								</div>
								<div class="col-xs-12 padding-0" style="margin-bottom:8px;">
									<span class="pull-left">Punctuality</span>
									<span class="color-grey" style="letter-spacing: 3px;"><div class="stars starrr param4" data-rating="{{ old('username') }}"></div></span>
								</div>
								<div class="col-xs-12 padding-0" style="margin-bottom:8px;">
									<span class="pull-left">Cleanliness & Facilities</span>
									<span class="color-grey" style="letter-spacing: 3px;"><div class="stars starrr param5" data-rating="{{ old('username') }}"></div></span>
								</div>
								<div class="col-xs-12 padding-0" style="margin-bottom:8px;">
									<span class="pull-left">Value for Money</span>
									<span class="color-grey" style="letter-spacing: 3px;"><div class="stars starrr param6" data-rating="{{ old('username') }}"></div></span>
								</div>
							</div>
							<div class="form-group">
								Your Experience(Review)<sup><span style="color:red;">*</span></sup> {{Form::textarea('comment', old('comment'), array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))}}
							</div>
							<div class="form-group">
								{{ Form::checkbox('post_anonymous', '1', old('post_anonymous'),['style'=>'visibility:visible']) }} Post Anonymously
							</div>
							<div class="form-group">
								<a href="#" class="btn btn-danger btn-sm" id="close-review-box" style="display:none; margin-right:10px;"> <span class="glyphicon glyphicon-remove"></span> Cancel</a>
								<button class="btn btn-success btn-sm" type="submit"><span class="glyphicon glyphicon-send"></span> Submit</button>
							</div>
							{{Form::close()}}
						</div>					  
					</div>
                </div>
                <div class="col-md-4 col-xs-12">
                        <div class="panel panel-info">
                                <div class="panel-heading"><h2 class="margin-0">Similar Hospitals</h2></div>
                                <div class="panel-body">
									<div class="panel-body">
											<ul class="list-group my-list-group">
											  <li class="list-group-item">
													<div class="col-xs-12 padding-0">
															<ul class="list-group my-list-group my-list-group1">
															 @if(!empty($similar_hospitals))
															 @foreach($similar_hospitals as $result)
															  <li class="list-group-item">
																	<div class="col-xs-3 padding-0  col-md-2 thumnail">
																		@if($result['image'])
																			<img src="{{ $result['image'] }}" class="img-circle" width="95">
																		@else
																			<img src="{{ asset("public/img/hospital_placeholder.png") }}" class="img-circle" width="95">
																		@endif
																	</div>
																	<div class="col-xs-9 col-md-10">
																		<h4><a href="{{ url('hospital') }}/{{$result['name_slug']}}">{{$result['name'] }}</a></h4>
																		<p class="color-grey"><i class="fa fa-map-marker"></i> {{ $result['address_one'] }}</p>
																		<p>		
																			@if(isset($result['speciality']) && !empty($result['speciality']))
																			@for($i=0; ($i < count($result['speciality']) && $i<1); $i++)
																				<span class="specility">{{ $result['speciality'][$i] }}</span>
																			@endfor
																			@endif<br>
																		</p>
																		<p class="color-grey">Consulting Fee -  <span><b>
																		@if($result['consultation_fee'] !=0)
																			<i class="fa fa-inr"></i>{{ $result['consultation_fee'] }}
																		@else
																			(Fee data not available with us.)
																		@endif
																		</span></b></p>
																	</div>
															  </li>
															  @endforeach
															  @else
																<li class="list-group-item"><p>No result was found.</p></li>	
															  @endif
															</ul>
													</div>
											  </li>
											</ul>
										<a type="button" href="javascript:;" style="display:none;" class="btn btn-block btn-info">See More</a>	
									</div>
                                </div>
                        </div>
                </div>
        </div>
		<!-- biography part START -->
        {{--<div for="biography" class="row">
                <h3>About</h3>
                <hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
                <div class="clearfix"></div>
                <section id="demo1">
					<article><p>{!! $hospital->about !!}..</p></article>
				</section>
        </div> --}}
		<!-- biography part END -->
		<div for="biography" class="row timings" style="margin-top:20px;">
			<h3>Timings</h3>
			<hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
			<div class="clearfix"></div>
			<div class="col-md-9 col-xs-12" style="background:#e4f0fa !important;" >
				<div class="padding-0">
					<section class="demos" >
						<article class="schedule-artcle">
						{!! Helper::widgetTimingsHospital($hospital) !!}
						</article>
					</section>
				</div>
			</div>
        </div>
		<!-- biography part END -->
		<!-- biography part START -->
        <div for="biography" class="row" style="margin-top:20px;">
			<h3>Facilities</h3>
			<hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
			<div class="clearfix"></div>
			<div class="col-md-9 col-xs-12" style="background:#e4f0fa !important;" >
			<div class="clearfix"></div>
			 <div class="col-xs-12 padding-0" style="padding:10px !important;">
			 <ul style="list-style:none;" id="double" class="services-ul">
				@if($hospital->diagnostic_lab_facility)<li class="color-grey">Diagnostic Lab Facility</li>@endif
				<?php $hospit = (array)$hospital['attributes'];?>
				@if($hospit['24_hours_emergency'])<li class="color-grey">24 Hours Emergency</li>@endif
				@if($hospital->cashless_mediclaim)<li class="color-grey">Cashless Mediclaim</li>@endif
				@if($hospital->icu_facility)<li class="color-grey">ICU Facility</li>@endif
			</ul> 
			</div>
			</div>
        </div>
        <div for="biography" class="row" style="margin-top:20px;">
			<h3>Specialties</h3>
			<hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
			<div class="clearfix"></div>
			<div class="col-md-9 col-xs-12" style="background:#e4f0fa !important;" >
			<div class="clearfix"></div>
			 <div class="col-xs-12 padding-0" style="padding:10px !important;">
			 <ul style="list-style:none;" id="double" class="services-ul">
				@foreach($hospital->specialtyList as $specialty)    
					<li class="color-grey">
						{{ $specialty->name }}
					</li>
				@endforeach
			 </ul> 
			</div>
			</div>
        </div>
		<?php //print_r($hospital->services()); ?>
		@if($hospital->services()->get()->count()>0)
		<div for="biography" class="row" style="margin-top:20px;">
			<h3>Services</h3>
			<hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
			<div class="clearfix"></div>
			<div class="col-md-9 col-xs-12" style="background:#e4f0fa !important;" >
			<div class="clearfix"></div>
			 <div class="col-xs-12 padding-0" style="padding:10px !important;">
			 <ul style="list-style:none;" id="double" class="services-ul">
				@foreach($hospital->services as $service)    
					<li class="color-grey">
						{{ $service->service->name }}
					</li>
				@endforeach
			 </ul> 
			</div>
			</div>
        </div>
		@endif
        <div for="biography" class="row loopis" style="margin-top:20px;">
				<h3>Doctors</h3>
                <hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
                <div class="clearfix"></div>
                @if($hospital->doctors()->get()->count()>0)
                        @foreach($hospital->doctors as $doctor)
                        <div class="col-xs-12 col-md-9 loop-search loop-search1" >
							<div class="col-xs-12 loo-aftr" style="background: white;padding: 5px;margin-bottom:5px;">
                            <div class="col-xs-4  col-md-2 thumnail">
                                @if($doctor->image)
                                <img class="img-circle" width="95" src="{{ $doctor->image }}" style="border: 1px solid #f0f0f0;">
                                @else
                                <img class="img-circle" width="95" src="{{ asset('public/img/doc_placeholder.png') }}" style="border: 1px solid #f0f0f0;">
                                @endif
                            </div>
                            <div class="col-xs-12 col-md-7 padding-0">
                                    <h4><a href="{{ url('doctor') }}/{{ $doctor->name_slug}}">{{ $doctor->name }}</a></h4>
                                    <p class="color-grey"><i class="fa fa-map-marker marker1"></i>{{ $doctor->address_one }}</p>
									<p>     
									@if($doctor->specialties)
									@foreach($doctor->specialties as $specialty) 
										<span class="specility">
											{{ $specialty->specialty->name }}
										</span>
									@endforeach
									@endif
									<span class="color-grey"><strong>{{ $doctor->experience }}</strong> yrs exp.</span></p>
									<p class="color-grey">
										<i class="fa fa-clock-o marker1"></i>
										<div class="padding-0">
											<section class="demos" >
												<article class="schedule-artcle">
													@if($doctor->getSchedules($doctor))
													@foreach($doctor->getSchedules($doctor) as $schedule) 
														<?php
															$class="other-schedule";												   
													    ?>
														@if($schedule['week_day']==1 && !empty($schedule['timings']))
															
															<p class="schedule-times color-grey {{$class}}" > <strong class="set-first">SUN</strong>&nbsp;&nbsp;{{ $schedule['timings'] }} </p>
														@elseif($schedule['week_day']==2 && !empty($schedule['timings']))
															<p class="schedule-times color-grey {{$class}}"><strong class="set-first">MON</strong>&nbsp;&nbsp;{{ $schedule['timings'] }} </p>
														@elseif($schedule['week_day']==3 && !empty($schedule['timings']))
															<p class="schedule-times color-grey {{$class}}"><strong class="set-first">TUE</strong>&nbsp;&nbsp;{{ $schedule['timings'] }} </p>
														@elseif($schedule['week_day']==4 && !empty($schedule['timings']))
															<p class="schedule-times color-grey {{$class}}"><strong class="set-first">WED</strong>&nbsp;&nbsp;{{ $schedule['timings'] }} </p>
														@elseif($schedule['week_day']==5 && !empty($schedule['timings']))
															<p class="schedule-times color-grey {{$class}}"><strong class="set-first">THU</strong>&nbsp;&nbsp;{{ $schedule['timings'] }} </p>
														@elseif($schedule['week_day']==6 && !empty($schedule['timings']))
															<p class="schedule-times color-grey {{$class}}"><strong class="set-first">FRI</strong>&nbsp;&nbsp;{{ $schedule['timings'] }} </p>
														@elseif($schedule['week_day']==7 && !empty($schedule['timings'])) 
															<p class="schedule-times color-grey  {{$class}}"><strong class="set-first">SAT</strong>&nbsp;&nbsp;{{ $schedule['timings'] }} </p>
														@endif
													@endforeach
													<button class="btn btn-primary btn-md pull-left transparent" id="default-schedule">show more</button>
													<button class="btn btn-primary btn-md pull-left trns transparent" id="other-schedule">show less</button>
													@else
														<p class="schedule-times color-grey">Not Updated</p>
													@endif
													<p class="read-mores schedule-times color-grey" style="display:none;">See More</p>
													<p class="hide-mores schedule-times color-grey" style="display:none;color:darkred;">Hide</p>
												</article>
											</section>
										</div>
										
									</p>			
									<div class="col-xs-12 padding-0" style="margin-top:10px;">
										<button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $doctor->mobile?$doctor->mobile:'Not Updated' }}</button>
									</div>
                            </div>
							<div class="col-xs-12 col-md-3 text-right">
								<p class="color-grey"><i class="fa fa-money"></i> Consulting Fee - <span>
								@if($doctor->consultation_fee !=0)
									<i class="fa fa-inr"></i>{{ $doctor->consultation_fee }}
								@else
									(Fee data not available with us.)
								@endif</span><p>
								<p class="color-grey">{{ $distance = round(\Vcareall\Admin\Helper::distance($latitude, $longitude, $doctor->latitude, $doctor->longitude, 'K'),1) }}KM <a href="{{ url('https://www.google.co.in/maps/dir')}}//{{$doctor->latitude}},{{$doctor->longitude }}" target="new"><i class="fa fa-location-arrow"></i></a></p>
                            </div>
							</div>
						</div>
						<div class="clearfix"></div>
						@endforeach
                        @else
                        <p>No hospital was found.</p>
                        @endif
        </div>        
		<div class="clearfix"></div>
		<div for="biography" class="row" style="margin-top:20px;">
			<h3>Photo Gallery</h3>
			<hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
			<div class="clearfix"></div>
			<div class="col-md-9 col-xs-12" style="background:#e4f0fa !important;" >
			<div class="clearfix"></div>
			 <div class="col-xs-12 padding-0" style="padding:10px !important;">
			 @foreach($hospital->galleries as $gallery)    
				<div class="color-grey" style="float:left; margin-right:5px;">
					<img class="img-circle" width="95" src="{{ $gallery->image_url }}" style="border: 1px solid #f0f0f0;"> 
				</div>
			 @endforeach
			</div>
			</div>
        </div>
        <!-- biography part END -->
		<!-- biography part START -->
        <div for="biography" class="row" style="margin-top:20px;">
				<div class="col-md-9 padding-0">
					<div class="col-md-6 padding-0">
					<h3>Review</h3>
					<hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
					</div>
					<div class="col-md-6 padding-0 text-right">
						@if( number_format($hospital->rating_cache) !=0)
						<a href="{{ url('reviews') }}?hospital={{ $hospital->id }}">
							<span class="color-grey">{{ round($hospital->rating_cache, 1)}}<span> 
							@for ($i=1; $i <= 5 ; $i++)
								<span class="color-grey">	
								<span class="glyphicon glyphicon-star{{ ($i <= $hospital->rating_cache) ? '' : '-empty'}}"></span>
								</span>
							@endfor<br>
							<p>({{count($reviews )}} {{ str_plural('review',count($reviews ))}})</p>
						</a>
						@else
							No Reviews
						@endif
					</div>
				</div>
                <div class="clearfix"></div>
                @if(count($reviews)>0)
                        @foreach($reviews as $review)
                        <div class="col-xs-12 col-md-9 loop-search loop-search1" >
							<div class="col-xs-12 loo-aftr" style="background: white;padding: 5px;margin-bottom:5px;">
								<div class="col-xs-12 col-md-8">
									<div class="col-xs-4  col-md-3 thumnail">
										@if($review->user->user->image && !$review->post_anonymous)
											<img src="{{ $review->user->user->image }}" class="img-circle" style="border: 1px solid #f0f0f0;">
										@else
											<img src="{{ asset('public/img/doc_placeholder.png') }}" class="img-circle" style="border: 1px solid #f0f0f0;">
										@endif
									</div>
									<div class="col-xs-12 col-md-8 padding-0" style="margin-top:30px;">
										<h4>{{ $review->post_anonymous?'Anonymous user':$review->user->user->name }}</h4>
									</div>
									<div class="col-xs-12 ">
										<div class="clearfix" style="margin-top:20px;"></div>
										<h4>{{ $review->title }}</h4>
										<p class="color-grey">{!! $review->comment !!}</p>
										<span id="thums-up-count-review{{ $review->id }}">{{ $review->helpfullVote()->where('vote',1)->get()->count() }}</span> <i rel="{{ $review->id }}" style="cursor:pointer;" class="fa fa-thumbs-up text-muted thums-up"></i>
										<i rel="{{ $review->id }}" style="cursor:pointer;" class="fa fa-thumbs-down text-muted thums-down"></i> <span id="thums-down-count-review{{ $review->id }}">{{ $review->helpfullVote()->where('vote',0)->get()->count() }}</span>
									</div>
								</div>
								<div class="col-xs-12 col-md-4 padding-0">
									<div class="col-xs-12 col-md-12 text-right">
										<span>Overall Rating</span>
										<div class="col-xs-12 padding-0">
											<span class="color-grey">{{ $review->rating }}<span> 
											@for ($i=1; $i <= 5 ; $i++)
												<span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
											@endfor
										</div>
										@if($review->reviewDetail)
										<!-- indivisual rating part START-->
										@foreach($review->reviewDetail as $temp)
										<div class="col-xs-12 padding-0">
											<span class="color-grey">{{ $temp->param->name }}<span> 
											@for ($i=1; $i <= 5 ; $i++)
												<span class="glyphicon glyphicon-star{{ ($i <= $temp->rate) ? '' : '-empty'}}"></span>
											@endfor
										</div> 
										@endforeach
										@endif
										<!-- indivisual rating part END-->
										<div class="clearfix"></div>
										
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						@endforeach
						<p><a href="{{ url('reviews') }}?hospital={{ $hospital->id }}">View More..</a></p>
				@else
				<p>No Review was found.</p>
				@endif
        </div>
        <!-- biography part END -->
		<div class="clearfix"></div>
		<!-- biography part START -->
        <div for="biography" class="row" style="margin-top:20px;">
                <h3>Map</h3>
                <hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
                <div class="clearfix"></div>
                <section id="demo1">
					<article><div id="map_canvas" style="width:100%; height:400px;"></div>  </article>
				</section>
        </div> 
		<hr>
		<div class="row error_report" style="margin-top:20px;color:red"><a href="{{ url('report-errors') }}?id={{ $hospital->id }}" style="color:red">Report an Error </a></div>
	</div>	
</div>
<!-- =================================
		APP DOWNLOAD PART END 
	================================== -->
@include('layouts.footer')
{{Html::script('public/js/expanding.js')}}
{{Html::script('public/js/starrr.js')}}
<script type="text/javascript">
	$(document).ready(function() {
		
		var map;		
		var markers = [
			{
				"title": "{{ session('address')['address_one']}}",
				"lat": "{{ session('address')['latitude'] }}",
				"lng": "{{ session('address')['longitude'] }}",
				"description": "{{ session('address')['address_one'] }}",
			}
		,
			{
				"title": "{{ $hospital->address_one }}",
				"lat": "{{ $hospital->latitude }}",
				"lng": "{{ $hospital->longitude }}",
				"description": "{{ $hospital->address_one }}"
			}
		];
		var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
				infoWindow.setContent(data.description);
                infoWindow.open(map, marker);
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
 
        //***********ROUTING****************//
 
        //Initialize the Path Array
        var path = new google.maps.MVCArray();
 
        //Initialize the Direction Service
        var service = new google.maps.DirectionsService();
 
        //Set the Path Stroke Color
        var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
 
        //Loop and Draw Path Route between the Points on MAP
        for (var i = 0; i < lat_lng.length; i++) {
		if ((i + 1) < lat_lng.length) {
			var src = lat_lng[i];
			var des = lat_lng[i + 1];
			path.push(src);
			poly.setPath(path);
			service.route({
				origin: src,
				destination: des,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			}, function (result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
							path.push(result.routes[0].overview_path[i]);
						}
					}
				});
			}
		}				
		// End Map

		$("#btn-claim").click(function(){
			var is_logged  = "{{ Auth::check() }}"; 
			if(!is_logged){
				$('#claim-form').submit();
				return false;
			}
		});
		/* show/hide schedule part script START */
		$('.loop-search article').each(function(){ 
		var count = $(this).find('p.color-grey').length;
		if(count<=1)
		{
			$(this).find('#other-schedule').remove();
			$(this).find('#default-schedule').remove();
		}
		$(this).find('.other-schedule:first-of-type').show();})
		$(document).on("click","#other-schedule",function(){
			$(this).hide();
			$(this).siblings('.other-schedule:not(:first-of-type)').hide();
			$(this).siblings("#default-schedule").show();
		});
		$(document).on("click","#default-schedule",function(){
			$(this).hide();
			$(this).parent('article').find("#other-schedule").show(); 
			$(this).parent('article').find(".other-schedule").show();
			
			$(this).parents('.loop-search').siblings().find('.other-schedule:not(:first-of-type)').hide();
			
			
			if($(this).parents('.loop-search').siblings().find('article p.color-grey').length>1){
				
			$(this).parents('.loop-search').siblings().find('#other-schedule').hide();
			$(this).parents('.loop-search').siblings().find('#default-schedule').show();
			
			$(this).siblings("#other-schedule").show();
			}
		});
		/* show/hide schedule part script END */
	});
	
    $('.wreview').click(function(){
            $('.review-box').slideToggle('slow');
    })
    $(function(){
        // initialize the autosize plugin on the review text area
        $('#new-review').autosize({append: "\n"});

        var reviewBox = $('#post-review-box');
        var newReview = $('#new-review');
        var openReviewBtn = $('#open-review-box');
        var closeReviewBtn = $('#close-review-box');
		var reviewForm = $('#review-form');
        var ratingsField = $('#ratings-hidden');
        var ratingsField1 = $('#ratings-hidden1');
        var ratingsField2 = $('#ratings-hidden2');
        var ratingsField3 = $('#ratings-hidden3');
        var ratingsField4 = $('#ratings-hidden4');
        var ratingsField5 = $('#ratings-hidden5');
        var ratingsField6 = $('#ratings-hidden6');

        openReviewBtn.click(function(e)
        {
			var is_logged  = "{{ Auth::check() }}"; 
            if(!is_logged){
                reviewForm.submit();
                return false;
            }
			reviewBox.slideDown(400, function()
            {
              $('#new-review').trigger('autosize.resize');
              newReview.focus();
            });
			openReviewBtn.fadeOut(100);
			closeReviewBtn.show();
        });

        closeReviewBtn.click(function(e)
        {
          e.preventDefault();
          reviewBox.slideUp(300, function()
            {
              newReview.focus();
              openReviewBtn.fadeIn(200);
            });
          closeReviewBtn.hide();

        });

        // If there were validation errors we need to open the comment form programmatically 
        @if($errors->first('comment') || $errors->first('rating'))
          openReviewBtn.click();
        @endif

        // Bind the change event for the star rating - store the rating value in a hidden field
        $('.param').on('starrr:change', function(e, value){
            ratingsField.val(value);
        });
        $('.param1').on('starrr:change', function(e, value){ 
            ratingsField1.val(value);
        });
        $('.param2').on('starrr:change', function(e, value){
            ratingsField2.val(value);
        });
        $('.param3').on('starrr:change', function(e, value){
            ratingsField3.val(value);
        });
        $('.param4').on('starrr:change', function(e, value){
            ratingsField4.val(value);
        });
        $('.param5').on('starrr:change', function(e, value){
            ratingsField5.val(value);
        });
        $('.param6').on('starrr:change', function(e, value){
            ratingsField6.val(value);
        });
		
		/*
		 * 
		 * Setting default rating
		 $('.starrr').starrr({
		  rating: 1
		});
		 *
		 */
		$('.starrr').starrr();
    });
    
    function initMap() {
        var uluru = {lat: <?=$hospital->latitude?>, lng: <?=$hospital->longitude?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
	/***
     * Hide notify(success/failed) message
     */
    setTimeout(function () {$('.alert').hide();},4000);
</script>
@if (Auth::check())
<script>
     $(function(){
        $(".thums-down").click(function(){ 
            var uid = "{{ Auth::user()->id }}";
            var csrf_field = "{{ csrf_token() }}";
            var id = $(this).attr("rel");
            $.ajax({
                type: "POST",
                url: "{{ url('ajax/thusm-down') }}",
                data: {'user_id':uid, 'review_id':id, 'voting':0,'_token':csrf_field},
                success: function(result){ 
                    if(result.status == 1){
                        $("#thums-down-count-review" + id).html(result.down_count);
                    }
                },
                error:function(result){
                    console.log(result);
                }
            });
        });
        $(".thums-up").click(function(){
            var uid = "{{ Auth::user()->id }}";
            var csrf_field = "{{ csrf_token() }}";
            var id = $(this).attr("rel");
            $.ajax({
                type: "POST",
                url: "{{ url('ajax/thums-up') }}",
                data: {'user_id':uid, 'review_id':id, 'voting':1,'_token':csrf_field},
                success: function(result){ 
                   if(result.status == 1){
                        $("#thums-up-count-review" + id).html(result.up_count);
                   }
                },
                error:function(result){
                    console.log(result);
                }
            });
        });
        $('i[data-toggle="tooltip"]').tooltip();
        $("#bookmark").click(function(){
            var uid = "{{ Auth::user()->id }}";
            var csrf_field = "{{ csrf_token() }}";
            var id = $("#bookmark").attr("rel");
            $.ajax({
                type: "POST",
                url: "{{ url('ajax/add_bookmark') }}",
                data: {'user_id':uid, 'profile_id':id, '_token':csrf_field},
                success: function(result){ 
                     console.log(result);
                   if(result.status == 1){
                        $("#bookmark-msg").html(result.message);
                        $('#bookmark-msg').show();
                   }
                   setTimeout(function () {$('#bookmark-msg').hide();},4000);
                },
                error:function(result){
                    console.log(result);
                }
            });
        });
    });       
</script>
@endif
@endsection
