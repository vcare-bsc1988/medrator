<aside>
        <div class="col-xs-12 col-md-3">
				<!-- Clear filter Part START -->
                <div class="col-xs-12 padding-0" align="right"><p><a href="JavaScript:Void(0);"  id="clear-filters"> Clear Filters</a></p></div><!-- Clear filter Part END -->
		        <!-- Name Part START -->
				<!-- Name Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p>Name</p>
					<div class="col-xs-12 col-md-12 padding-0">
                        {{ Form::text('filters[name]',(isset(Session::get('filters')['name']))?stripslashes(Session::get('filters')['name']):null,['class'=>'form-control sinput name-search-box', 'placeholder'=>'Enter Name','autocomplete'=>'off']) }}
                        <div class="name-suggesstion-box"></div>
						<input type="hidden" disabled="disabled" name="search_result" id="search_result" value="{{ json_encode($results) }}">
                    </div>
                </div><!-- Name Part END -->
                <!-- Distance Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p>Distance</p>
                    <!--<p>
                            <div id="number_range" style="margin-top: 10px;"></div>
                    </p>-->
                    <input type="text" name="filters[max_distance]" id="distance" readonly style="border:0; color:#f6931f; font-weight:bold;">
                    <div id="slider-range-distance"></div>
                </div><!-- Distance Part END -->

                <!-- Rating Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <input type="hidden" id="star" value="<?=(isset(Session::get('filters')['rating']))?Session::get('filters')['rating']:0?>" name="filters[rating]">    
                    <p class="margin-bottom-10">Rating</p>
                    <div class="squaredOne margin-left-0">
                        <input type="checkbox" value="5" <?=(Session::get('filters')['rating']==5)?'checked':''?> id="squaredFive" name="check" />
                            <label for="squaredFive"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="4" <?=(Session::get('filters')['rating']==4)?'checked':''?> id="squaredFour" name="check" />
                            <label for="squaredFour"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="glyphicon glyphicon-star-empty"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="3" <?=(Session::get('filters')['rating']==3)?'checked':''?> id="squaredThree" name="check"/>
                            <label for="squaredThree"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                        <input type="checkbox" value="2" <?=(Session::get('filters')['rating']==2)?'checked':''?> id="squaredTwo" name="check" />
                            <label for="squaredTwo"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="1" <?=(Session::get('filters')['rating']==1)?'checked':''?> id="squaredOne" name="check"  />
                            <label for="squaredOne"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        
                </div><!-- Rating Part END -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p class="margin-bottom-10">Consultation Fee (<i class="fa fa-inr"></i>)</p>
                    <input type="hidden" id="min_fee" <?php if(!empty(Session::get('filters')['min_fee'])){?>value ="<?=Session::get('filters')['min_fee']?>" <?php } ?> name="filters[min_fee]">
                    <input type="hidden" id="max_fee" <?php if(!empty(Session::get('filters')['max_fee'])){?>value ="<?=Session::get('filters')['max_fee']?>" <?php } ?>  name="filters[max_fee]">
                    <input type="text" id="fee" readonly style="border:0; color:#f6931f; font-weight:bold;">
                    <div id="slider-range-fee"></div>
                </div>
				<!-- Filter for 24 Hour Emergency -->
                <div class="col-xs-12 padding-0 margin-bottom-10">
                    <div class="margin-bottom-10 col-xs-12 padding-0">24 Hour Emergency
						<div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="1" <?=(isset(Session::get('filters')['hours24_emergency']) && Session::get('filters')['hours24_emergency']==1)?'checked':''?> id="squaredHours24emergency" class="hours24_emergency" name="filters[hours24_emergency]"/>
                        <label for="squaredHours24emergency"></label>
                    </div>
					</div>
                    
                </div>
                <div class="col-xs-12 padding-0 margin-bottom-10">
                    <div class="margin-bottom-10 col-xs-12 padding-0">ICU Facility
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="1" <?=(isset(Session::get('filters')['icu_facility']) && Session::get('filters')['icu_facility']==1)?'checked':''?> id="squaredIcuFacility" class="icu_facility" name="filters[icu_facility]"/>
                        <label for="squaredIcuFacility"></label>
                    </div>
					</div>
                </div>
                <div class="col-xs-12 padding-0 margin-bottom-10">
                    <div  class="margin-bottom-10 col-xs-12 padding-0">Cashless Mediclaim
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="1" <?=(isset(Session::get('filters')['cashless_mediclaim']) && Session::get('filters')['cashless_mediclaim']==1)?'checked':''?> id="squaredCashlessMediclaim" class="cashless_mediclaim" name="filters[cashless_mediclaim]"/>
                        <label for="squaredCashlessMediclaim"></label>
                    </div>
					</div>
                </div>
                <!-- Working Day Part START -->							
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p class="margin-bottom-10">Working Day</p>
                    <label class="nms">Monday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="1" <?=(isset(Session::get('filters')['working_day'][0]) && Session::get('filters')['working_day'][0]==1)?'checked':''?> id="squaredMon" class="day-of-week"  name="filters[working_day][0]"/>
                        <label for="squaredMon"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Tuesday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="2" <?=(isset(Session::get('filters')['working_day'][1]) && Session::get('filters')['working_day'][1]==2)?'checked':''?> id="squaredTh" class="day-of-week" name="filters[working_day][1]"/>
                        <label for="squaredTh"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Wednesday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="3" <?=(isset(Session::get('filters')['working_day'][2]) && Session::get('filters')['working_day'][2]==3)?'checked':''?> id="squaredWed" class="day-of-week" name="filters[working_day][2]"/>
                        <label for="squaredWed"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Thursday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="4" <?=(isset(Session::get('filters')['working_day'][3]) && Session::get('filters')['working_day'][3]==4)?'checked':''?> id="squaredThr" class="day-of-week" name="filters[working_day][3]"/>
                        <label for="squaredThr"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Friday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="5" <?=(isset(Session::get('filters')['working_day'][4]) && Session::get('filters')['working_day'][4]==5)?'checked':''?> id="squaredFri" class="day-of-week" name="filters[working_day][4]"/>
                        <label for="squaredFri"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Saturday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="6" <?=(isset(Session::get('filters')['working_day'][5]) && Session::get('filters')['working_day'][5]==6)?'checked':''?> id="squaredSat" class="day-of-week" name="filters[working_day][5]"/>
                        <label for="squaredSat"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
					<label class="nms">Sunday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="1" <?=(isset(Session::get('filters')['working_day'][6]) && Session::get('filters')['working_day'][6]==1)?'checked':''?> id="squaredSun" class="day-of-week" name="filters[working_day][6]"/>
                        <label for="squaredSun"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                </div><!-- Working Day Part END -->						
                <!-- Gender part END -->
                <!-- Services Part START -->							
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p class="margin-bottom-10">Specialist <?php //print_r(Session::get('filters')['services']) ?></p>
                    @foreach($specialties  as $specialty)
                    <div class="squaredOne margin-left-0">
                        {{ Form::checkbox('filters[specialties][]',$specialty->id,(!empty(Session::get('filters')['specialties']) && in_array($specialty->id,Session::get('filters')['specialties']))?true:false,["class" => "specialties","id"=>"squared".$specialty->id]) }}
                        <label for="squared{{ $specialty->id }}"></label>
                    </div>
                    <label class="nms">{{$specialty->name}}</label>
                    <hr class="pull-left col-xs-12 divide"/>
                    @endforeach
                    <a href="javascript:(0);" class="pull-left black" style="color:black;font-size:14px;" data-toggle="modal" data-target="#more_services">View More <i class="fa fa-angle-right"></i></a>
                </div>  
        </div>
</aside>