<aside>
        <div class="col-xs-12 col-md-3">
            <!-- Clear filter Part START -->
                <div class="col-xs-12 padding-0" align="right"><p><a href="JavaScript:Void(0);"  id="clear-filters"> Clear Filters</a></p></div><!-- Clear filter Part END -->
				<!-- Name Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p>Name</p>
					<div class="col-xs-12 col-md-12 padding-0">
                        {{ Form::text('filters[name]',(isset(Session::get('filters')['name']))?Session::get('filters')['name']:null,['class'=>'form-control sinput name-search-box', 'placeholder'=>'Enter Name','autocomplete'=>'off']) }}
                        <div class="name-suggesstion-box"></div>
						<input type="hidden" disabled="disabled" name="search_result" id="search_result" value="{{ json_encode($results) }}">
                    </div>
                </div><!-- Name Part END -->

                <!-- Distance Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p>Distance</p>
                    <input type="text" name="filters[max_distance]" id="distance" readonly style="border:0; color:#f6931f; font-weight:bold;">
                    <div id="slider-range-distance"></div>
                </div><!-- Distance Part END -->

                <!-- Rating Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <input type="hidden" id="star" value="<?=(isset(Session::get('filters')['rating']))?Session::get('filters')['rating']:0?>" name="filters[rating]">    
                    <p class="margin-bottom-10">Rating</p>
                    <div class="squaredOne margin-left-0">
                        <input type="checkbox" value="5" <?=(isset(Session::get('filters')['rating']) && Session::get('filters')['rating']==5)?'checked':''?> id="squaredFive" name="check" />
                            <label for="squaredFive"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="4" <?=(isset(Session::get('filters')['rating']) && Session::get('filters')['rating']==4)?'checked':''?> id="squaredFour" name="check" />
                            <label for="squaredFour"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="glyphicon glyphicon-star-empty"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="3" <?=(isset(Session::get('filters')['rating']) && Session::get('filters')['rating']==3)?'checked':''?> id="squaredThree" name="check"/>
                            <label for="squaredThree"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                        <input type="checkbox" value="2" <?=(isset(Session::get('filters')['rating']) && Session::get('filters')['rating']==2)?'checked':''?> id="squaredTwo" name="check" />
                            <label for="squaredTwo"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="1" <?=(isset(Session::get('filters')['rating']) && Session::get('filters')['rating']==1)?'checked':''?> id="squaredOne" name="check"  />
                            <label for="squaredOne"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        
                </div><!-- Rating Part END -->
				<div class="col-xs-12 padding-0 margin-bottom-30">
                    <input type="hidden" id="entity" value="<?=(isset(Session::get('filters')['entity']))?Session::get('filters')['entity']:0?>" name="filters[entity]">    
                    <p class="margin-bottom-10">Type of Entity</p>
                    <div class="squaredOne margin-left-0">
                        <input type="checkbox" value="2" <?=(isset(Session::get('filters')['entity']) && Session::get('filters')['entity']==2)?'checked':''?> id="squaredDoctor" name="check" />
                            <label for="squaredDoctor"></label>
                    </div>
					<label class="nms">Doctor</label>
                    <hr class="pull-left col-xs-12 divide"/>
					<div class="squaredOne margin-left-0">
                        <input type="checkbox" value="5" <?=(isset(Session::get('filters')['entity']) && Session::get('filters')['entity']==5)?'checked':''?> id="squaredHospital" name="check" />
                            <label for="squaredHospital"></label>
                    </div>
					<label class="nms">Hospital</label>
                    <hr class="pull-left col-xs-12 divide"/>
					<div class="squaredOne margin-left-0">
                        <input type="checkbox" value="4" <?=(isset(Session::get('filters')['entity']) && Session::get('filters')['entity']==4)?'checked':''?> id="squaredDiagnostic" name="check" />
                            <label for="squaredDiagnostic"></label>
                    </div>
					<label class="nms">Diagnostic</label>
                    <hr class="pull-left col-xs-12 divide"/>
				</div>
        </div>
</aside>