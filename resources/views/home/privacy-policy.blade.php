@extends('layouts.home')

@section('content')
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')		
<style>
.first-section.p82-topbot{
	border-top: 3px solid #e4f0fa;
}
p{margin-bottom:15px;}
</style>					 
<div class="first-section p82-topbot">
	<div class="container">
		<div class="row">	
			<div class="col-md-12 text-center">
				<h1>Privacy Policy</h1>	
				<hr style="width:50px;">
			</div>
			<div class="col-sm-12 col-xs-12">
			<p> Privacy Policy for Medrator (<a href="http://www.medrator.com">www.medrator.com</a>)
			If you require any more information or have any questions about our privacy policy, please feel free to contact us (hyperlink this to contact us page).

			At <a href="http://www.medrator.com">www.medrator.com</a>, the privacy of our visitors is of extreme importance to us. This privacy policy document outlines the types of personal information is received and collected by <a href="http://www.medrator.com">www.medrator.com</a> and how it is used. </p>

			<h3>Log Files</h3>

			<p>Like many other Web sites, <a href="http://www.medrator.com">www.medrator.com</a> makes use of log files. The information inside the log files includes internet protocol ( IP ) addresses, type of browser, Internet Service Provider ( ISP ), date/time stamp, referring/exit pages, and number of clicks to analyze trends, administer the site, track user’s movement around the site, and gather demographic information. IP addresses, and other such information are not linked to any information that is personally identifiable. These logs will not be made public unless required by law to do so.</p>

			<h3>Cookies and Web Beacons</h3>

			<p><a href="http://www.medrator.com">www.medrator.com</a> does use cookies to store information about visitors preferences, record user-specific information on which pages the user access or visit, customize Web page content based on visitors browser type or other information that the visitor sends via their browser.</p>

			<p>Some of our advertising partners may use cookies and web beacons on our site. Our advertising partners include Google Adsense.</P>

			<p>These third-party ad servers or ad networks use technology to the advertisements and links that appear on <a href="http://www.medrator.com">www.medrator.com</a> send directly to your browsers. They automatically receive your IP address when this occurs. Other technologies ( such as cookies, JavaScript, or Web Beacons ) may also be used by the third-party ad networks to measure the effectiveness of their advertisements and / or to personalize the advertising content that you see.</p>

			<p><a href="http://www.medrator.com">www.medrator.com</a> has no access to or control over these cookies that are used by third-party advertisers.</p>

			<p>You should consult the respective privacy policies of these third-party ad servers for more detailed information on their practices as well as for instructions about how to opt-out of certain practices. www.medrator.com’s privacy policy does not apply to, and we cannot control the activities of, such other advertisers or web sites.</p>
			<p>If you wish to disable cookies, you may do so through your individual browser options. More detailed information about cookie management with specific web browsers can be found at the browsers’ respective websites.</p>

			</div><!-- /.col-sm-8 -->
		</div>
	</div>
</div>
@include('layouts.footer')

@endsection
