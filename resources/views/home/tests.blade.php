<div class="row">
    <div class="col-xs-12">        
        <div class="col-xs-12 padding-0"> 
            <div class="row">
                <section for="side-filter">
                    <div class="col-md-12 col-xs-12 pull-left">
                        <div style="margin-left: auto;margin-right: auto;/* width: 200px; */">
                            <div class="col-md-4 padding-0"><input placeholder="Search..." class="col-xs-12 form-control" id="s" type="text" /> </div>
                            <div class="col-md-2" style="margin-top:10px;"><a id="other-tests-cancel" href="javascript:void(0);" >Cancel</a></div>
                            </div>
                            <hr style="float:left;width:100%;"/>
                            <ul class="countryList" style="clear:both;max-height:200px;overflow:auto;" id="triple">
                                @for($i=1; $i<count($tests); $i++)
                                <li><a href="{{ url('find/diagnostic')}}/{{ $address['state_name'] }}/{{ $tests[$i]->name }}">{{ $tests[$i]->name }}</a></li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#s').keyup(function(){
            var valThis = $(this).val().toLowerCase();
            $('.countryList>li').each(function(){
                var text = $(this).text().toLowerCase();
                (text.indexOf(valThis) == 0) ? $(this).show() : $(this).hide();            
            });
        });
    });
    $("#other-tests-cancel").click(function(){ 
        $.ajax({
            type: "POST",
            url: "{{ url('home-tests') }}",
            data:{"_token":"{{ csrf_token() }}"},
            success: function(result){ 
               $("#tests-container").html(result);
            },
            error:function(result){
                console.log(result);
            }
        });
    });
</script>