@extends('layouts.home')

@section('content')
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')		
<style>
.first-section.p82-topbot{
	border-top: 3px solid #e4f0fa;
}
.mb15{margin-bottom:15px;}
p{margin-bottom:15px;}
</style>					 
<div class="first-section p82-topbot">
	<div class="container">
		<div class="row">	
			<div class="col-md-12 text-center">
				<h1>Terms of Services</h1>	
				<hr style="width:50px;">
			</div>
			<div class="col-sm-12 col-xs-12 must member">
				<p class="mb15">These terms of service ("Terms", "Agreement") are an agreement between "Medrator", "us", "we" or "our") and you ("User", "you" or "your"). This Agreement sets forth the general terms and conditions of your use of the <a href="http://www.medrator.com">www.medrator.com</a> website and any of its products or services (collectively, "Website" or "Services").</p>
				<h3>Accounts and membership</h3>
				<p class="mb15">If you create an account at the Website, you are responsible for maintaining the security of your account and you are fully responsible for all activities that occur under the account and any other actions taken in connection with it. Providing false contact information of any kind may result in the termination of your account. You must immediately notify us of any unauthorized uses of your account or any other breaches of security. We will not be liable for any acts or omissions by you, including any damages of any kind incurred as a result of such acts or omissions. We may suspend, disable, or delete your account (or any part thereof) if we determine that you have violated any provision of this Agreement or that your conduct or content would tend to damage our reputation and goodwill. If we delete your account for the foregoing reasons, you may not re-register for the our Services. We may block your email address and Internet protocol address to prevent further registration.</p>
				<h3>User content</h3>
				<p class="mb15">We do not own any data, information or material ("Content") that you submit to the Website in the course of using the Service. You shall have sole responsibility for the accuracy, quality, integrity, legality, reliability, appropriateness, and intellectual property ownership or right to use of all submitted Content. We may, but have no obligation to, monitor Content on the Website submitted or created using our Services by you. Unless specifically permitted by you, your use of the Website does not grant us the license to use, reproduce, adapt, modify, publish or distribute the Content created by you or stored in your user account for commercial, marketing or any similar purpose. But you grant us permission to access, copy, distribute, store, transmit, reformat, display and perform the Content of your user account solely as required for the purpose of providing the Services to you. Without limiting any of those representations or warranties, we have the right, though not the obligation, to, in our own sole discretion, refuse or remove any Content that, in our reasonable opinion, violates any of our policies or is in any way harmful or objectionable.</p>
				<h3>Backups</h3>
				<p class="mb15">We perform regular backups of the Website and Content, however, these backups are for our own administrative purposes only, and are in no way guaranteed. You are responsible for maintaining your own backups of your data. We do not provide any sort of compensation for lost or incomplete data in the event that backups do not function properly. We will do our best to ensure complete and accurate backups, but assume no responsibility for this duty.</p>
				<h3>Prohibited uses</h3>
				<p class="mb15">In addition to other terms as set forth in the Agreement, you are prohibited from using the website or its content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.</p>
				<h3>Intellectual property rights</h3>
				<p class="mb15">This Agreement does not transfer from Medrator or any third party intellectual property on Medrator to you(user). All rights, title, and interest in and to such property will remain (as between the parties) solely with Medrator. All trademarks, service marks, graphics and logos used in connection with our Website or Services, are trademarks or registered trademarks of Medrator or Medrator licensors. Other trademarks, service marks, graphics and logos used in connection with our Website or Services may be the trademarks of other third parties. Your use of our Website and Services grants you no right or license to reproduce or otherwise use any Medrator or third-party trademarks.</p>
				<h3>Disclaimer of warranty</h3>
				<p class="mb15">You agree that your use of our Website or Services is solely at your own risk. You agree that such Service is provided on an "as is" and "as available" basis. We expressly disclaim all warranties of any kind, whether express or implied, including but not limited to the implied warranties of merchantability, fitness for a particular purpose and non-infringement. We make no warranty that the Services will meet your requirements, or that the Service will be uninterrupted, timely, secure, or error free; nor do we make any warranty as to the results that may be obtained from the use of the Service or as to the accuracy or reliability of any information obtained through the Service or that defects in the Service will be corrected. You understand and agree that any material and/or data downloaded or otherwise obtained through the use of Service is done at your own discretion and risk and that you will be solely responsible for any damage to your computer system or loss of data that results from the download of such material and/or data. We make no warranty regarding any goods or services purchased or obtained through the Service or any transactions entered into through the Service. No advice or information, whether oral or written, obtained by you from us or through the Service shall create any warranty not expressly made herein.</p>
				<h3>Indemnification</h3>
				<p class="mb15">You agree to indemnify and hold Medrator and its affiliates, directors, officers, employees, and agents harmless from and against any liabilities, losses, damages or costs, including reasonable attorneys' fees, incurred in connection with or arising from any third-party allegations, claims, actions, disputes, or demands asserted against any of them as a result of or relating to your Content, your use of the Website or Services or any willful misconduct on your part.</p>
				<h3>Changes and amendments</h3>
				<p class="mb15">We reserve the right to modify this Agreement or its policies relating to the Website or Services at any time, effective upon posting of an updated version of this Agreement on the Website. When we do we will revise the updated date at the bottom of this page. Continued use of the Website after any such changes shall constitute your consent to such changes.</p>
				<h3>Acceptance of these terms</h3>
				<p class="mb15">You acknowledge that you have read this Agreement and agree to all its terms and conditions. By using the Website or its Services you agree to be bound by this Agreement. If you do not agree to abide by the terms of this Agreement, you are not authorized to use or access the Website and its Services.</p>
				<h3>Contacting us</h3>
				<p>If you have any questions about this Policy, please contact us.(Link to contact us)</p>
			</div><!-- /.col-sm-8 -->
		</div>
	</div>
</div>
@include('layouts.footer')

@endsection
