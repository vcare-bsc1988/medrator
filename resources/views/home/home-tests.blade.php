<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 padding-0">
        @for($i=1; $i<15; $i++)
            <div class="col-md-3 col-xs-6  text-center sublist-container">
                <a href="{{ url('find/diagnostic')}}/{{ $address['state_name'] }}/{{ $tests[$i]->name }}"><p><img src="{{ ($i>8?asset("public/images/med_icons/diagnos.png"):asset("public/images/med_icons/".$tests[$i]->name.".png") ) }}"/></p>
                <p>{{ $tests[$i]->name }}</p></a>
            </div>
        @if($i%4==0)
        </div>
        <div class="col-xs-12 padding-0"> 
        @endif
        @endfor
            <div class="col-md-3 col-xs-6  text-center sublist-container" style="margin: 30px 0px 0px 0px;"><a href="javascript:void(0);" id="other-tests">Other Tests</a></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#other-tests").click(function(){ 
        $.ajax({
            type: "POST",
            url: "{{ url('get-tests') }}",
            data:{"_token":"{{ csrf_token() }}"},
            success: function(result){ 
               $("#tests-container").html(result);
            },
            error:function(result){
                console.log(result);
            }
        });
    });
</script>