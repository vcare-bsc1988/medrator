@extends('layouts.home')

@section('content')
<!--==================================
	 Header parts starts here
==================================-->
@include('layouts.header')							 
<div class="first-section p82-topbot">
	<div class="container">
		<div class="row">	
			<div class="col-sm-8 col-xs-12 must member">
			<h1>Report Errors</h1>	
			@include('common.errors')
			@if (session('message'))
			<div class="alert alert-success">
				<strong>Success!</strong> {{ session('message') }}
			</div>
			@endif
			{{ Form::open(['url' => 'report-errors','methos'=>'post']) }}
				{{csrf_field()}}
				{{ Form::hidden('id',request('id')) }}
				<div class="form-group col-lg-12">
					Error Title : {{ Form::text('title', null,['class'=>'form-control']) }}<br>
				</div>
				<div class="form-group col-lg-12">
					Error Descriptions : {{ Form::textarea ('message', null,['class'=>'form-control']) }}
				</div>
				<div class="form-group col-lg-12">
					{{ Form::submit('Send') }}
				</div>
			{{ Form::close() }}
			</div><!-- /.col-sm-8 -->
		</div>
	</div>
</div>
@include('layouts.footer')

@endsection
