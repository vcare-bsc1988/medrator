@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Diagnostic Center Info</div>

                <div class="panel-body">
                    @if (session('success'))
                        <div class="flash-message">
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        </div>
                    @endif
                    @include('common.errors')                   
                    {{ Form::open(array('url' => 'diagnostic', 'method' => 'post','files' => true)) }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                            <?=$errors->first('name','<p class="help-block">:message</p>')?>
                        </div>                 
                        <div class="form-group{{ $errors->has('home_collection_facility') ? ' has-error' : '' }}">
                            {{ Form::label('home_collection_facility', 'Home Collection Facility') }}
                            {{ Form::checkbox('home_collection_facility', 1) }}
                            <?=$errors->first('home_collection_facility', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('online_bookings') ? ' has-error' : '' }}">
                            {{ Form::label('online_bookings', 'Online Bookings') }}
                            {{ Form::checkbox('online_bookings', 1) }}
                            <?=$errors->first('online_bookings', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('online_reports') ? ' has-error' : '' }}">
                            {{ Form::label('online_reports', 'Online Reports') }}
                            {{ Form::checkbox('online_reports', 1) }}
                            <?=$errors->first('online_reports', '<p class="help-block">:message</p>')?>
                        </div>
						<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            {{ Form::label('country', 'Country') }}
                            {{ Form::select('country',['India'=>'India'],null,['class' => 'form-control']) }}
                            <?=$errors->first('country', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            {{ Form::label('address', 'Address') }}
                            {{ Form::text('address', null, ['class' => 'form-control']) }}
                            <?=$errors->first('address', '<p class="help-block">:message</p>')?>
                        </div>
						<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            {{ Form::label('state', 'State') }}
                            {{ Form::select('state', [' ' => 'Select State'] +\App\Model\States::where('country_id',101)->lists('name','id')->toArray(),null,['class' => 'form-control','onChange'=>'getCity(this.value)'])}}
                            <?=$errors->first('state', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            {{ Form::label('city', 'City') }}
                            {{ Form::select('city', [' '=>'Select City'], null,['id'=>'city-list','class' => 'form-control']) }}
                            <?=$errors->first('city', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('pincode') ? ' has-error' : '' }}">
                            {{ Form::label('pincode', 'Pincode') }}
                            {{ Form::text('pincode', null, ['class' => 'form-control']) }}
                            <?=$errors->first('pincode', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            {{ Form::label('phone', 'Phone (optional)') }}
                            {{ Form::text('phone', null, ['class' => 'form-control']) }}
                            <?=$errors->first('phone', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            {{ Form::label('image', 'Upload Photo (optional)') }}
                            {{ Form::file('image') }}</br></br>
                            <?=$errors->first('image', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Save!') }}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function getCity(val) {
    $.ajax({
        type: "POST",
        url: "{{ url('get_city') }}",
        data:'state_id='+val,
        success: function(data){ 
        $("#city-list").html(data);
        }
    });
}
</script>
@endsection
