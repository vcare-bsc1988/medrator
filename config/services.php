<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '341235656207659',
        'client_secret' => '1f5523ffc426bea5467e4314a7f47e55',
        'redirect' => 'http://139.59.24.155/auth/facebook',
    ],
    'twitter' => [
        'client_id' => 'jvVgZoMJFDGF0nuSRFzrE4jO3',
        'client_secret' => 'onPJd84dU1ZrHgsGmWCFBI4iv5octmWDLQooJ1TZix1T34ttjf',
        'redirect' => 'http://139.59.24.155/auth/twitter',
    ],
    'google' => [
        'client_id' => '871342679502-j0vitaigp7m1q1j3fnrnjb6akv4ajc9s.apps.googleusercontent.com',
        'client_secret' => 'oLYZxtyrnIkOarc3-J7KlNLx',
        'redirect' => 'http://www.medrator.com/auth/google',
    ],

];
