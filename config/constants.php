<?php
return [
    'message' => [
        'profile_updated' => 'Profile updated successfully.',
		'delete_recent_search' => 'Deleted successfully.',
		'delete_bookmark'=>'Bokmarked deleted successfully.',
		'add_bookmark'=>'Profile not bookmarked,because this profile is associate your account',
		'user_not_found' => 'User not found',
    ],
    'review_cycle' => 10,
    'error_report_cycle' => 24,
    'gcm_apikey' => 'AIzaSyBd2cX1NOSWd_1MNZPavC7ygsmfUSIjvFI',
    'search_radius' => 20,
	'admin_email' => 'admin@medrator.com',
];

