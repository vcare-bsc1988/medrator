<?php

use Illuminate\Database\Seeder;
use App\Permission;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
        	/*[
        		'name' => 'role-list',
        		'display_name' => 'Display Role Listing',
        		'description' => 'See only Listing Of Role'
        	],*/
        	[
        		'name' => 'role-create',
        		'display_name' => 'Create Role',
        		'description' => 'Create New Role'
        	],
        	[
        		'name' => 'role-edit',
        		'display_name' => 'Edit Role',
        		'description' => 'Edit Role'
        	],
        	[
        		'name' => 'role-delete',
        		'display_name' => 'Delete Role',
        		'description' => 'Delete Role'
        	],
        	[
        		'name' => 'user-list',
        		'display_name' => 'Display User Listing',
        		'description' => 'See only Listing Of User'
        	],
        	[
        		'name' => 'user-create',
        		'display_name' => 'Create User',
        		'description' => 'Create New User'
        	],
        	[
        		'name' => 'user-edit',
        		'display_name' => 'Edit User',
        		'description' => 'Edit User'
        	],
        	[
        		'name' => 'user-delete',
        		'display_name' => 'Delete User',
        		'description' => 'Delete User'
        	],
                [
        		'name' => 'backend-user-list',
        		'display_name' => 'Display Backend User Listing',
        		'description' => 'See only Listing Of User'
        	],
        	[
        		'name' => 'backend-user-create',
        		'display_name' => 'Create  Backend User',
        		'description' => 'Create New User'
        	],
        	[
        		'name' => 'backend-user-edit',
        		'display_name' => 'Edit Backend User',
        		'description' => 'Edit User'
        	],
        	[
        		'name' => 'backend-user-delete',
        		'display_name' => 'Delete User',
        		'description' => 'Delete User'
        	],
                [
        		'name' => 'entities-list',
        		'display_name' => 'Display Entities Listing',
        		'description' => 'See only Listing Of Entities'
        	],
        	[
        		'name' => 'entities-create',
        		'display_name' => 'Create Entities',
        		'description' => 'Create New Entities'
        	],
        	[
        		'name' => 'entities-edit',
        		'display_name' => 'Edit Entities',
        		'description' => 'Edit Entities'
        	],
        	[
        		'name' => 'entities-delete',
        		'display_name' => 'Delete Entities',
        		'description' => 'Delete Entities'
        	],
                [
        		'name' => 'reviews-list',
        		'display_name' => 'Display Reviews Listing',
        		'description' => 'See only Listing Of Reviews'
        	],
        	[
        		'name' => 'reviews-create',
        		'display_name' => 'Create Reviews',
        		'description' => 'Create New Reviews'
        	],
        	[
        		'name' => 'reviews-edit',
        		'display_name' => 'Edit Reviews',
        		'description' => 'Edit Reviews'
        	],
        	[
        		'name' => 'reviews-delete',
        		'display_name' => 'Delete Reviews',
        		'description' => 'Delete Reviews'
        	],
                [
        		'name' => 'post-list',
        		'display_name' => 'Display Post Listing',
        		'description' => 'See only Listing Of Post'
        	],
        	[
        		'name' => 'post-create',
        		'display_name' => 'Create Post',
        		'description' => 'Create New Post'
        	],
        	[
        		'name' => 'post-edit',
        		'display_name' => 'Edit Post',
        		'description' => 'Edit Post'
        	],
        	[
        		'name' => 'post-delete',
        		'display_name' => 'Delete Post',
        		'description' => 'Delete Post'
        	],
                [
        		'name' => 'forum-list',
        		'display_name' => 'Display Forum Listing',
        		'description' => 'See only Listing Of Forum'
        	],
        	[
        		'name' => 'forum-create',
        		'display_name' => 'Create Forum',
        		'description' => 'Create New Forum'
        	],
        	[
        		'name' => 'forum-edit',
        		'display_name' => 'Edit Forum',
        		'description' => 'Edit Forum'
        	],
        	[
        		'name' => 'forum-delete',
        		'display_name' => 'Delete Forum',
        		'description' => 'Delete Forum'
        	],
                [
        		'name' => 'setting-radius',
        		'display_name' => 'Setting Radius',
        		'description' => 'Setting Radius'
        	],
                [
        		'name' => 'export-data',
        		'display_name' => 'Export Data',
        		'description' => 'Export Data'
        	],
                [
        		'name' => 'import-data',
        		'display_name' => 'Import Data',
        		'description' => 'Import Data'
        	],
                [
        		'name' => 'claim-requests-list',
        		'display_name' => 'Display Claim Requests Listing',
        		'description' => 'See only Listing Of Claim Requests'
        	],
        	[
        		'name' => 'claim-requests-create',
        		'display_name' => 'Create Claim Requests',
        		'description' => 'Create New Claim Requests'
        	],
        	[
        		'name' => 'claim-requests-edit',
        		'display_name' => 'Edit Claim Requests',
        		'description' => 'Edit Claim Requests'
        	],
        	[
        		'name' => 'claim-requests-delete',
        		'display_name' => 'Delete Claim Requests',
        		'description' => 'Delete Claim Requests'
        	],
                [
        		'name' => 'notifications-list',
        		'display_name' => 'Display Claim Notifications',
        		'description' => 'See only Listing Of Notifications'
        	],
        	[
        		'name' => 'notifications-create',
        		'display_name' => 'Create Notifications',
        		'description' => 'Create Notifications'
        	],
        	[
        		'name' => 'notifications-edit',
        		'display_name' => 'Edit Notifications',
        		'description' => 'Edit Notifications'
        	],
        	[
        		'name' => 'notifications-delete',
        		'display_name' => 'Delete Notifications',
        		'description' => 'Delete Notifications'
        	]
        ];

        foreach ($permission as $key => $value) {
        	Permission::create($value);
        }
    }
}
