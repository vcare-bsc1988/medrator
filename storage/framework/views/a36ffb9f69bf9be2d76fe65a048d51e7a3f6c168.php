<?php $__env->startSection('us::content'); ?>
<!--==================================
	 Header parts starts here
==================================-->
<?php echo $__env->make('us::layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>									   
<!--==================================
	 Header parts ends here
  ==================================-->
<!--==================================
	Banner parts starts here
  ==================================-->
<div class="banner-wrapper">
	<div class="banner banner1">
    	<!-- <img src="images/banner2.jpg" alt="banner" /> -->
    </div><!-- /.banner -->
    <div class="magnet magnet1">
    <div class="container">
    <div class="row">
    	<div class="col-sm-12 col-xs-12">
				<div class="form-group">
                    <?php echo e(Form::open(['url' => 'find', 'method' => 'get', 'style'=>'padding-left:0px;', 'id'=>'search-form', 'class' => 'search-form-0 col-xs-12', 'rol'=>'search'])); ?>

					<div class="col-xs-12 col-md-4 padding-0">
                        <?php echo e(Form::text('location', null, ['class'=>'form-control location-input', 'placeholder'=>'Location','id'=>'location'])); ?>

                        <div class="location-details">
                            <?php echo e(Form::hidden('latitude', $latitude,['data-geo'=>'lat'])); ?>

                            <?php echo e(Form::hidden('longitude', $longitude,['data-geo'=>'lng'])); ?>

                            <?php echo e(Form::hidden('locality', $locality,['data-geo'=>'locality'])); ?>

                            <?php echo e(Form::hidden('city_name', $city_name,['data-geo'=>'locality'])); ?>

                            <?php echo e(Form::hidden('state_name', $state_name,['data-geo'=>'administrative_area_level_1'])); ?>

                            <?php echo e(Form::hidden('country_name', $country_name,['data-geo'=>'country_short'])); ?>

                            <?php echo e(Form::hidden('pincode', $pincode,['data-geo'=>'postal_code'])); ?>

                        </div>
                    </div>
                    <div class="col-xs-12 col-md-7 padding-0">
                        <?php echo e(Form::text('q',$q,['id'=>'search-box', 'class'=>'form-control sinput', 'placeholder'=>'Specialities, Doctors, Hospitals, Clinics, Labs','autocomplete'=>'off'])); ?>

						<div id="suggesstion-box"></div>
                    </div>
                    <div class="col-xs-12 col-md-1 padding-0">
                        <button type="submit" id="search" class="btn btn-info search-btn"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
					<?php echo e(Form::close()); ?>

                </div>
        </div><!--/.main-page-form-->
   		
    </div><!-- /.row -->
    </div><!-- /.container -->  
    </div><!-- /.magnet --><!-- /.magnet -->
</div><!-- /.banner-wrapper -->         
<!--==================================
              Banner parts ends here
        ==================================-->
<!--==================================
              service Quick links starts here
        ==================================--> 
<?php echo e(Form::open(['url' => url()->full(), 'method' => 'get', 'style'=>'padding-left:0px;', 'id'=>'filterForm', 'class' => 'search-form-0 col-xs-12', 'rol'=>'search'])); ?>

<div class="first-section ptb">
        <div class="container">
                <div class="row">
                        <section for="side-filter">
                                <?php echo $__env->make('search.filter-diagnostic', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
                                <div class="col-xs-12 col-md-9 padding-0" style="background:#e4f0fa;">
                                        <div class="col-md-6 col-xs-12 pull-left">
											
											<h5 style="padding:22px 0px;"><?php echo e($paginator['result']->getNumFound()); ?> matches found for: <?php echo e($q); ?></h5>
										</div>
                                        <div class="col-md-6 col-xs-12 pull-right text-right"  style="padding:10px;margin-top:0px;">
											
											<label>Sort by: </label>
											<select class='sort-select' name="filters[sorting]" id="sorting">
												<option value="distance-asc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='distance-asc')?'selected':''?> >Distance - Low to High</option>
                                                <option value="distance-desc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='distance-desc')?'selected':''?> >Distance - High to Low</option>
                                                <option value="ratings" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='ratings')?'selected':''?> >Ratings</option>
                                                <option value="ratings-count" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='ratings-count')?'selected':''?> >No of Ratings</option>
                                            </select>
											<span class="caret caret1"></span>
                                        </div>
                                        <?php if(!empty($results)): ?>
                                        <?php foreach($results as $result): ?>
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 loop-search" >
                                                <div class="col-xs-4  col-md-2 thumnail">
                                                    <?php if($result['image']): ?>
                                                        <img src="<?php echo e($result['image']); ?>" style="width:80px; text-align: left;">
                                                    <?php else: ?>
                                                        <img src="<?php echo e(asset("public/img/diagnostic_placeholder.png")); ?>" class="img-circle" width="95">
                                                    <?php endif; ?>

                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <h4><a href="<?php echo e(url('us/diagnostic')); ?>/<?php echo e($result['name_slug']); ?>"><?php echo e($result['name']); ?></a></h4>
                                                    <p class="color-grey"><?php echo e($result['address_one']); ?></p>
													<p>     
														<?php if(isset($result['diagnostic_test']) && !empty($result['diagnostic_test'])): ?>
														<?php for($i=0; ($i < count($result['diagnostic_test']) && $i<2); $i++): ?>
															<span class="specility">
																<?php if($i>0): ?> <?php endif; ?> <?php echo e($result['diagnostic_test'][$i]); ?> 
															</span>
														<?php endfor; ?>
														<?php endif; ?>
													</p>
													<p><?php if($result['online_reports']): ?> Online Report <?php endif; ?> </p>
                                                </div>
                                                <div class="col-xs-12 col-md-4 text-right">
                                                    <?php if( number_format($diagnostic->rating_cache) !=0): ?>
													<p class="color-grey">
                                                        <?php for($i=1; $i <= 5 ; $i++): ?>
                                                        <span class="fa fa-star<?php if($i < $result['rating'] or $i == $result['rating']){ echo '';}elseif($i-1 < $result['rating'] &&  $result['rating'] < $i){ echo '-half'; } else{ echo '-o';} ?>"></span>
                                                        <?php endfor; ?>
                                                        <?php echo e(round($result['rating'], 1)); ?>

                                                    </p>
                                                    <p class="color-grey">
                                                            <?php echo e($result['review_count']); ?> <?php echo e(str_plural('Review', $result['review_count'])); ?>

                                                    </p>
													<?php else: ?>
														<p>No Reviews</p>
													<?php endif; ?>
                                                    <p class="color-grey">
                                                            <?php echo e(number_format($result['distance'],1)); ?> Km <a href="<?php echo e(url('us/https://www.google.co.in/maps/dir')); ?>//<?php echo e($result['latlon']); ?>" target="new"><i class="fa fa-location-arrow"></i> </a>
                                                    </p>
                                                </div>
                                               <div class="col-xs-12 text-right">
													<button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> <?php echo e($result['phone']?$result['phone']:'Not Updated'); ?></button>
												</div>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                        <div class="col-xs-12">
											<?php echo Helper::paginator($paginator['count'],$paginator['limit'],$paginator['page']); ?>

                                        </div>
                                        <?php else: ?>
                                        <div class="col-xs-12">
                                            <h1 style="text-align: center;padding: 90px 0px;color: #d8e3ec;">No result found.</h1>
                                        </div>
                                        <?php endif; ?>
                                </div>
                        </section>
                </div>
        </div>
</div>
<!--Popup for see more services--->
<div class="modal fade" id="more_services" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-us::content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tests</h4>
      </div>
      <div class="modal-body">
        <ul class="nav nav-staced more-spec">
            <?php foreach($test_all  as $test): ?>
            <li class="col-md-3 col-xs-12">
                <div class="col-md-2 padding-0"> 
                    <div class="squaredOne margin-left-0">
                    <input type="checkbox" value="<?php echo e($test->id); ?>" <?=(!empty(Session::get('filters')['tests']) && in_array($test->id,Session::get('filters')['tests']))?'checked':''?> id="squared<?php echo e($test->id); ?>" class="tests" name="filters[tests][]" />
                    <label for="squared<?php echo e($test->id); ?>"></label>
                    </div>
                </div>
                 <div class="col-md-10 padding-0" >
                <label class="nms" style="font-size:10.5px !important;"><?php echo e($test->name); ?></label>
                 </div>
            </li>
            <?php endforeach; ?>       
        </ul>
      </div>
      <div class="modal-footer hide">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!--End services popup -->
<?php echo e(Form::close()); ?>

<!-- =================================
		APP DOWNLOAD PART END 
	================================== -->
<?php echo $__env->make('us::layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link rel="stylesheet" href="<?php echo e(asset("public/js/jquery-ui.css")); ?>">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
		// Clear the filter
        $("#clear-filters").click(function () { 
            document.getElementById("filterForm").reset();
            $('#filterForm').find("input[type=text], textarea, input[type=hidden]").val("");
            var default_distance = "<?php echo $default_distance; ?>";
            $( "#distance" ).val( default_distance + 'KM');
            $('#filterForm').find('input[type=checkbox]:checked').removeAttr('checked');
            console.log($('#filterForm').serialize()) ;
            $("#filterForm").submit();
        })
        //End
        $( "#slider-range-fee" ).slider({
            range: true,
            min: 0,
            max: 1000,
            values: [<?=(!empty(Session::get('filters')['min_fee'])?Session::get('filters')['min_fee']:0)?>, <?=!empty(Session::get('filters')['max_fee'])?Session::get('filters')['max_fee']:1000?>],
            slide: function( event, ui ) {
                $( "#fee" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                $( "#min_fee" ).val( ui.values[ 0 ] );
                $( "#max_fee" ).val( ui.values[ 1 ] );
                $("#filterForm").submit();
            }
        });
        $( "#fee" ).val( "$" + $( "#slider-range-fee" ).slider( "values", 0 ) + " - $" + $( "#slider-range-fee" ).slider( "values", 1 ) );
        
        // For service filter.
        $(".tests").click(function () { 
            $("#filterForm").submit();
        })

        $("#squaredOne").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(1);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        })
        $("#squaredTwo").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(2);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        }) 
        $("#squaredThree").click(function () {
			if ( $(this).is(':checked') ) {
				$( "#star" ).val(3);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit(); 
        }) 
        $("#squaredFour").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(4);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        }) 
        $("#squaredFive").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(5);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        }) 
        $( "#slider-range-distance" ).slider({
            value:<?=!empty(Session::get('filters')['max_distance'])?str_replace('KM','',Session::get('filters')['max_distance']):0?>,
            min: 0,
            max: 50,
            step: 1,
            slide: function( event, ui ) {
              $( "#distance" ).val( ui.value + 'KM');
              $("#filterForm").submit();
            }
        });
        $( "#distance" ).val( $( "#slider-range-distance" ).slider( "value" )+ 'KM' );
        
        // For experience filter
        $( "#slider-range-experience" ).slider({
            value:<?=!empty(Session::get('filters')['max_experience'])?str_replace('Year','',Session::get('filters')['max_experience']):0?>,
            min: 0,
            max: 50,
            step: 1,
            slide: function( event, ui ) {
              $( "#experience" ).val( ui.value +'Year');
              $("#filterForm").submit();
            }
        });
        $( "#experience" ).val( $( "#slider-range-experience" ).slider( "value" ) +'Year');
        
        // For working day filter
        $(".day-of-week").click(function () {
            $("#filterForm").submit();
        })
        
		// For sorting filter.
        $("#sorting").change(function () { 
            $("#filterForm").submit();
        })
        /** filter Online Report
         * 
         * 
         */
        $("#sorting").change(function () { 
            $(".online_reports").submit();
        })
        /***
         * 
         * Filter Home collection facility.
         */
        $(".home_collection_facility").change(function () { 
            $("#filterForm").submit();
        })
  
  });
	$(function(){
        $("#location").geocomplete({
            details: ".location-details",
            detailsAttribute: "data-geo",
            location: "<?php echo e($location); ?>",
        });
        $("#location").bind("geocode:result", function(event, result){
            $.ajax({
                type: "POST",
                url: "<?php echo e(url('session/stor_city')); ?>",
                data: $("#search-form").serialize(),
                success: function(result){ 
                   $("#entity-container").html(result);
                },
                error:function(result){
                    console.log(result);
                }
            });
        });
    });
 </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('us::layouts.home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>