<header class="body-color">
<div class="container">
<div class="row">
	<div class="col-sm-12 top-strip ptb1">
		<div class="col-xs-12 col-md-6 text-center-small">
                    <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset("public/images/med_icons/logos.png")); ?>" alt="logos.png" style="height:44px;"/> </a>
		</div>
		<div class="col-xs-12 col-md-6 z-index-1">
			<ul class="nav navbar-nav pull-right mynav-01">
				<?php if(Auth::check()): ?> 
					<li class="pull-left-small"><a href="javascript:void(0);" class="anchor-padding noti-icos"><i class="fa fa-bell"></i><sup><?php if(count(Helper::getNotifications(Auth::user()->id,'unread'))>0): ?><span class="noti-counter"><?php echo e(count(Helper::getNotifications(Auth::user()->id,'unread'))); ?></span><?php endif; ?></sup></a>
						<?php if(Helper::getNotifications(Auth::user()->id, 'both')): ?>
						<ul class='notification-list' style="display:none;">
							<?php foreach(Helper::getNotifications(Auth::user()->id, 'both') as $notification): ?>
							<li class="<?php echo e($notification->status==1?'read':'unread'); ?>">
								<?php /* <h2><?php echo e($notification->title); ?></h2> */ ?>
								<p class="message-part-noti"><?php echo e($notification->message); ?></p>
                                                                <p class="time " style="overflow:hidden;"><i class="fa fa-clock-o"></i> <?php echo e($notification->notification_time); ?><br><span class="noti-action-btn"> Mark as <a href="<?php echo e($notification->status==1?url('notification-unread/'.$notification->id):url('notification-read/'.$notification->id)); ?>"><?php echo e($notification->status==1?'Unread':'Read'); ?></a></span> <span style="float:left;padding:10px 5px 5px 5px;">|</span> <span class="noti-action-btn"> <a href="<?php echo e(url('clear-notification/'.$notification->id)); ?>" style="color: #E91E63;">Clear</a></span></p>
							</li>
							<?php endforeach; ?>
                                                        <li class="text-center mt10"><a href="<?php echo e(url('account/notifications')); ?>">View All</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo e(url('home/clear-notifications')); ?>/<?php echo e(Auth::user()->id); ?>" style="color:#e91e63;">Clear All</a> </li>
						</ul>
						<?php endif; ?>
					</li>
				<?php endif; ?>					
				<li class="pull-right"><a href="javascript:void(0);" class="anchor-padding "><img src="<?php echo e(asset("public/images/india_flag.png")); ?>" width="16">&nbsp;&nbsp;<i class="fa fa-angle-down"></i></a></li>
				<?php if(!Auth::check()): ?>
					<li class="pull-left-small"><li class="pull-left-small">
						<a id="sign-in" class="anchor-padding btn btn-info radius-0 signin-popup" data-toggle="modal" data-target="#signin-form" >Sign In</a>
					</li>
					<?php endif; ?>
				
				<?php if(Auth::check()): ?>
				<li>
					 <div class="dropdown ac-main">
					  <button class="btn btn-default dropdown-toggle btn-acount" type="button" data-toggle="dropdown">
						<?php $nameArr = explode(" ",Auth::user()->user->name); ?>
						<?php echo e($nameArr['0']); ?>...
						<span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i></span>
					  </button>
					  <ul class="dropdown-menu acnt-drop">
						<li>
							<div class="col-xs-3">
								<img src="<?php echo e(Auth::user()->user->image!=""?Auth::user()->user->image:asset('public/img/doc_placeholder.png')); ?>" class="img-profile"/>
							</div>
							<div class="col-xs-9">
								<h4><?php echo e(Auth::user()->user->name); ?></h4>
								<p class="grey"><?php echo e(Auth::user()->user->mobile); ?></p>
							</div>
						</li>
						<li class='seperator'></li>
						<li class="active"><a href="<?php echo e(url('/profile')); ?>"><!--<i class="fa fa-key is">&nbsp; --></i> My Account</a></li>
						<li><a href="<?php echo e(url('/logout')); ?>"><!--<i class="fa fa-power-off is">&nbsp; </i> -->Logout</a></li>
					  </ul>
					</div> 								
				</li>
				<?php endif; ?>
				<li>
					<div id="selectLanguageDropdown" class="localizationTool"></div>
				</li>
			</ul>
		</div>
		<!-- <h2 class="delt-bord">Email :<a href="#">info@medicaltheme.com</a></h2>
		<h2>Call :<a href="#"> 8876 7786 999</a></h2> -->
	</div><!-- /.col-sm-12 -->
</div><!-- row -->
</div><!-- container -->

<div class="col-sm-12 bottom-strip p-both white-bg">
<!-- <div class="col-sm-3 logo">
	<a href="#"><img src="images/logo-var2.png" alt="medical theams" /></a>
</div> --><!-- /.col-sm-3 -->
<div class="container">
<div class="row">
<div class="col-sm-12 navigation">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed drawl-left" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		</div>
	
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav pull-right-large main-menus">
			<li class="active drop">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Add Review</a>
				<ul class="dropdown-menu" role="menu">
                                    <li class="drop-down-bg"></li>
                                    <li><a href="<?php echo e(url('review/select-specialty')); ?>">Review A Doctor</a></li>
                                    <li><a href="<?php echo e(url('review/hospital')); ?>">Review A Hospital</a></li>
                                    <li><a href="<?php echo e(url('review/diagnostic')); ?>">Review A Diagnostic</a></li>
				</ul>
			</li>
			<li class="dropdown drop">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">HEALTH TOOLS</a>
			  <ul class="dropdown-menu" role="menu">
			  <li class="drop-down-bg"></li>
				<li><a href="<?php echo e(url('home/healt-tools')); ?>">TOOL1</a></li>
				<li><a href="<?php echo e(url('home/healt-tools')); ?>">TOOL2</a></li>
				<li><a href="<?php echo e(url('home/healt-tools')); ?>">TOOL3</a></li>
				<li><a href="<?php echo e(url('home/healt-tools')); ?>">TOOL4</a></li>
			  </ul>
			</li>
			<li><a href="<?php echo e(url('/blogs-forums')); ?>">BLOG</a></li>
			<li><a href="<?php echo e(url('blogs-forums/activity')); ?>">FORUM</a></li>
			<li class="dropdown drop">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">More</a>
			  <ul class="dropdown-menu" role="menu">
			  <li class="drop-down-bg"></li>
				<li><a href="<?php echo e(url('organ-donation')); ?>">Organ Donation</a></li>
				<li><a href="<?php echo e(url('medical-tourism')); ?>">Medical Tourism</a></li>
				<li><a href="<?php echo e(url('account/add-doctor')); ?>">Add A Listing</a></li>
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
</div><!-- /.col-sm-9 -->		
</div><!-- /.row -->
</div><!-- /.container -->
</div><!-- /.col-sm-12 -->
</header><!-- #header --> 