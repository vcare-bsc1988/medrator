<?php $__env->startSection('us::content'); ?>
<!--==================================
	 Header parts starts here
==================================-->
<?php echo $__env->make('us::layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>									   
<!--==================================
	 Header parts ends here
  ==================================-->
<!--==================================
	Banner parts starts here
  ==================================-->
<div class="banner-wrapper">
	<div class="banner banner1">
    	<!-- <img src="images/banner2.jpg" alt="banner" /> -->
    </div><!-- /.banner -->
    <div class="magnet magnet1">
    <div class="container">
    <div class="row">
    	<div class="col-sm-12 col-xs-12">
				<div class="form-group">
					<?php echo e(Form::open(['url' => 'find', 'method' => 'get', 'style'=>'padding-left:0px;', 'id'=>'search-form', 'class' => 'search-form-0 col-xs-12', 'rol'=>'search'])); ?>

					<div class="col-xs-12 col-md-4 padding-0">
                        <?php echo e(Form::text('location', null, ['class'=>'form-control location-input', 'placeholder'=>'Location','id'=>'location'])); ?>

                        <div class="location-details">
                            <?php echo e(Form::hidden('latitude', $latitude,['data-geo'=>'lat'])); ?>

                            <?php echo e(Form::hidden('longitude', $longitude,['data-geo'=>'lng'])); ?>

                            <?php echo e(Form::hidden('locality', $locality,['data-geo'=>'locality'])); ?>

                            <?php echo e(Form::hidden('city_name', $city_name,['data-geo'=>'locality'])); ?>

                            <?php echo e(Form::hidden('state_name', $state_name,['data-geo'=>'administrative_area_level_1'])); ?>

                            <?php echo e(Form::hidden('country_name', $country_name,['data-geo'=>'country_short'])); ?>

                            <?php echo e(Form::hidden('pincode', $pincode,['data-geo'=>'postal_code'])); ?>

                        </div>
                    </div>
                    <div class="col-xs-12 col-md-7 padding-0">
                        <?php echo e(Form::text('q',stripslashes($q),['id'=>'search-box', 'class'=>'form-control sinput', 'placeholder'=>'Specialities, Doctors, Hospitals, Clinics, Labs','autocomplete'=>'off'])); ?>

						<div id="suggesstion-box"></div>
                    </div>
                    <div class="col-xs-12 col-md-1 padding-0">
                        <button type="submit" id="search" class="btn btn-info search-btn"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
					<?php echo e(Form::close()); ?>

                </div>
        </div><!--/.main-page-form-->
   		
    </div><!-- /.row -->
    </div><!-- /.container -->  
    </div><!-- /.magnet --><!-- /.magnet -->
</div><!-- /.banner-wrapper -->         
<!--==================================
              Banner parts ends here
        ==================================-->
<!--==================================
              service Quick links starts here
        ==================================--> 
<?php echo e(Form::open(['url' => url()->full(), 'method' => 'get', 'style'=>'padding-left:0px;', 'id'=>'filterForm', 'class' => 'search-form-0 col-xs-12', 'rol'=>'search'])); ?>

<div class="first-section ptb">
        <div class="container">
                <div class="row">
                        <section for="side-filter">
                                <?php echo $__env->make('search.filter-doctor', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
                                <div class="col-xs-12 col-md-9 padding-0" style="background:#e4f0fa;">
                                        <div class="col-md-6 col-xs-12 pull-left">
                                                <h5 style="padding:22px 0px;"><?php echo e($paginator['result']->getNumFound()); ?> matches found for: <?php echo e($q); ?></h5>
                                        </div>
                                        <div class="col-md-6 col-xs-12 pull-right text-right"  style="padding:10px;margin-top:0px;">
                                            <label>Sort by: </label>
                                            <select class='sort-select' name="filters[sorting]" id="sorting">
											
                                                <option value="distance-asc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='distance-asc')?'selected':''?> >Distance - Low to High</option>
                                                <option value="distance-desc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='distance-desc')?'selected':''?> >Distance - High to Low</option>
                                                <option value="ratings" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='ratings')?'selected':''?> >Ratings</option>
                                                <option value="ratings-count" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='ratings-count')?'selected':''?> >No of Ratings</option>
                                                <option value="fee-asc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='fee-asc')?'selected':''?> >Fee - Low to High</option>
                                                <option value="fee-desc" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='fee-desc')?'selected':''?> >Fee - High to Low</option>
                                                <option value="experience" <?=(isset(Session::get('filters')['sorting']) && Session::get('filters')['sorting']=='experience')?'selected':''?> >Experience</option>
                                            </select>
                                            <span class="caret caret1"></span>
                                        </div>
                                        <?php if(!empty($results)): ?>
                                        <?php foreach($results as $result): ?>
                                        <?php if($result['role']==2): ?>
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 loop-search" >
                                                    <div class="col-xs-4  col-md-2 thumnail">
                                                        <?php if($result['image']): ?>
                                                            <img src="<?php echo e($result['image']); ?>" style="width:80px; text-align: left;">
                                                        <?php else: ?>
                                                            <img src="<?php echo e(asset("public/img/doc_placeholder.png")); ?>" class="img-circle" width="95">
                                                        <?php endif; ?>
                                                        
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <h4><a href="<?php echo e(url('us/doctor')); ?>/<?php echo e($result['name_slug']); ?>"><?php echo e($result['name']); ?></a></h4>
                                                        <p class="color-grey"><?php echo e($result['address_one']); ?></p>
                                                        <p>     
															<?php if(isset($result['speciality']) && !empty($result['speciality'])): ?>
                                                            <?php for($i=0; ($i < count($result['speciality']) && $i<2); $i++): ?>
																<span class="specility">
																	<?php if($i>0): ?> <?php endif; ?> <?php echo e($result['speciality'][$i]); ?> 
																</span>
                                                            <?php endfor; ?>
                                                            <?php endif; ?>
                                                            <span class="color-grey"><strong><?php echo e($result['experience']); ?></strong> yrs exp.</span></p>
                                                        <p class="color-grey">Consulting Fee - <span>
														<?php if($result['consultation_fee'] !=0): ?>
															<i class="fa fa-inr"></i><?php echo e($result['consultation_fee']); ?>

														<?php else: ?>
															(Fee data not available with us.)
														<?php endif; ?>
														</span></p>
                                                    </div>
                                                    <div class="col-xs-12 col-md-4 text-right">
                                                        <?php if( number_format($result['rating']) !=0): ?>
														<p class="color-grey">
                                                            <?php for($i=1; $i <= 5 ; $i++): ?>
															<span class="fa fa-star<?php if($i < $result['rating'] or $i == $result['rating']){ echo '';}elseif($i-1 < $result['rating'] &&  $result['rating'] < $i){ echo '-half'; } else{ echo '-o';} ?>"></span>
                                                            <?php endfor; ?>
                                                            <?php echo e(round($result['rating'], 1)); ?>

                                                        </p>
                                                        <p class="color-grey">
                                                                <?php echo e($result['review_count']); ?> <?php echo e(str_plural('Review', $result['review_count'])); ?>

                                                        </p>
														<?php else: ?>
															<p>No Reviews</p>
														<?php endif; ?>
                                                        <p class="color-grey">
                                                                <?php echo e(number_format($result['distance'],1)!=0?number_format($result['distance'],1):0); ?> Km <a href="<?php echo e(url('us/https://www.google.co.in/maps/dir')); ?>//<?php echo e($result['latlon']); ?>" target="new"><i class="fa fa-location-arrow"></i> </a>
                                                        </p>
                                                    </div>
                                                    <div class="col-xs-12 text-right ptb">
														<div class="col-xs-12">
                                                            <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> <?php echo e(isset($result['mobile'] )&& !empty($result['mobile'])?$result['mobile']:'Not Updated'); ?></button>
														</div>
                                                    </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                        <div class="col-xs-12">
											<?php echo Helper::paginator($paginator['count'],$paginator['limit'],$paginator['page']); ?>

                                        </div> 
                                        <?php else: ?>
                                        <div class="col-xs-12">
                                            <h1 style="text-align: center;padding: 90px 0px;color: #d8e3ec;">No result found.</h1>
                                        </div>
                                        <?php endif; ?>
                                </div>
                        </section>
                </div>
        </div>
</div>
<!--End services popup -->
<?php echo e(Form::close()); ?>

<!-- =================================
		APP DOWNLOAD PART END 
	================================== -->
<?php echo $__env->make('us::layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link rel="stylesheet" href="<?php echo e(asset("public/js/jquery-ui.css")); ?>">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
        // Clear the filter
        $("#clear-filters").click(function () { 
            document.getElementById("filterForm").reset();
            $('#filterForm').find("input[type=text], textarea, input[type=hidden]").val("");
            var default_distance = "<?php echo $default_distance; ?>";
            $( "#distance" ).val( default_distance + 'KM');
            $('#filterForm').find('input[type=checkbox]:checked').removeAttr('checked');
            console.log($('#filterForm').serialize()) ;
            $("#filterForm").submit();
        })
        //End
		$( "#slider-range-fee" ).slider({
            range: true,
            min: 0,
            max: 1000,
            values: [<?=(!empty(Session::get('filters')['min_fee'])?Session::get('filters')['min_fee']:0)?>, <?=!empty(Session::get('filters')['max_fee'])?Session::get('filters')['max_fee']:1000?>],
            slide: function( event, ui ) {
                $( "#fee" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
                $( "#min_fee" ).val( ui.values[ 0 ] );
                $( "#max_fee" ).val( ui.values[ 1 ] );
                $("#filterForm").submit();
            }
        });
        $( "#fee" ).val( $( "#slider-range-fee" ).slider( "values", 0 ) + " - " + $( "#slider-range-fee" ).slider( "values", 1 ) );
        
        // For service filter.
        $(".services").click(function () { 
            $("#filterForm").submit();
        })
		// For specialties filter.
		$(".specialties").click(function () { 
            $("#filterForm").submit();
        })
		// For rating & review filter.
        $("#squaredOne").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(1);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        })
        $("#squaredTwo").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(2);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        }) 
        $("#squaredThree").click(function () {
			if ( $(this).is(':checked') ) {
				$( "#star" ).val(3);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit(); 
        }) 
        $("#squaredFour").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(4);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        }) 
        $("#squaredFive").click(function () {
            if ( $(this).is(':checked') ) {
				$( "#star" ).val(5);
			} 
			else {
				$( "#star" ).val(0);
			}
            $("#filterForm").submit();
        }) 
		// For distance filter.
        $( "#slider-range-distance" ).slider({
            value:<?=!empty(Session::get('filters')['max_distance'])?str_replace('KM','',Session::get('filters')['max_distance']):0?>,
            min: 0,
            max: 50,
            step: 1,
            slide: function( event, ui ) {
              $( "#distance" ).val( ui.value + 'KM');
              $("#filterForm").submit();
            }
        });
        $( "#distance" ).val( $( "#slider-range-distance" ).slider( "value" )+ 'KM' );
        
        // For experience filter
        $( "#slider-range-experience" ).slider({
            value:<?=!empty(Session::get('filters')['max_experience'])?str_replace('Year','',Session::get('filters')['max_experience']):0?>,
            min: 0,
            max: 50,
            step: 1,
            slide: function( event, ui ) {
              $( "#experience" ).val( ui.value +'Year');
              $("#filterForm").submit();
            }
        });
        $( "#experience" ).val( $( "#slider-range-experience" ).slider( "value" ) +'Year');
        
        // For working day filter
        $(".day-of-week").click(function () {
            $("#filterForm").submit();
        })
        // For gender filter.
        $(".gender").click(function () { 
            $("#filterForm").submit();
        })
        // For sorting filter.
        $("#sorting").change(function () { 
            $("#filterForm").submit();
        })
		$(function(){
			$("#location").geocomplete({
				details: ".location-details",
				detailsAttribute: "data-geo",
				location: "<?php echo e($location); ?>",
			});
			$("#location").bind("geocode:result", function(event, result){
				$.ajax({
					type: "POST",
					url: "<?php echo e(url('session/stor_city')); ?>",
					data: $("#search-form").serialize(),
					success: function(result){ 
					   $("#entity-container").html(result);
					},
					error:function(result){
						console.log(result);
					}
				});
			});
		});
  
  });
  </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('us::layouts.home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>