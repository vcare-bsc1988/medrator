<h3 class="text-center">BROWSE BY CATEGORY</h3>
<p>
    <ul class="nav nav-tabs nav-justified search-advanced-tabs" role="tablist">
        <li role="presentation" class="active text-center">
            <a href="#doctors" aria-controls="doctors" role="tab" data-toggle="tab">
                <p class="text-center"><img src="<?php echo e(asset("public/images/med_icons/doctor.png")); ?>" width="60"/></p>
                <p>Doctors</p>
                <p><i class="fa fa-minus"></i></p>
            </a>
        </li>
        <li role="presentation">
            <a href="#hospitals" aria-controls="hospitals" role="tab" data-toggle="tab">
                <p class="text-center"><img src="<?php echo e(asset("public/images/med_icons/hospital.png")); ?>" width="70"/></p>
                <p>Hospitals</p>
                <p><i class="fa fa-plus"></i></p>								
            </a>
        </li>
        <li role="presentation">
            <a href="#dcenter" aria-controls="dcenter" role="tab" data-toggle="tab">
                <p class="text-center"><img src="<?php echo e(asset("public/images/med_icons/diagonstic_center.png")); ?>" width="42"/></p>
                <p>Diagnostic Centers</p>
                <p><i class="fa fa-plus"></i></p>
            </a>
        </li>
     </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="doctors">
            <div class="container sublist-top-container" id="specilities-container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-12 padding-0">
                        <?php for($i=1; $i<15; $i++): ?>
                            <div class="col-md-3 col-xs-6  text-center sublist-container">
                                <a href="<?php echo e(url('find/doctor')); ?>/<?php echo e($address['state_name']); ?>/<?php echo e($specialties[$i-1]->name); ?>">
								<p>
								<?php if(file_exists(public_path("images/med_icons/".$specialties[$i-1]->name.".png"))): ?>
									<img src="<?php echo e(asset("public/images/med_icons/".$specialties[$i-1]->name.".png")); ?>"/></p>
								<?php endif; ?>
								<p><?php echo e($specialties[$i-1]->name); ?></p></a>
                            </div>
                        <?php if($i%4==0): ?>
                        </div>
                        <div class="col-xs-12 padding-0"> 
                        <?php endif; ?>
                        <?php endfor; ?>
                            <div class="col-md-3 col-xs-6  text-center sublist-container" style="margin: 30px 0px 0px 0px;"><a href="javascript:void(0);" id="other-specialties">Other Specialties</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="hospitals">
            <div class="container sublist-top-container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-md-3 col-xs-6  text-center sublist-container">
                            <a href="<?php echo e(url('find/hospital')); ?>/<?php echo e($address['state_name']); ?>/2"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                            <p>Within 2KM</p></a>
                        </div>
                        <div class="col-md-3 col-xs-6  text-center sublist-container">
                             <a href="<?php echo e(url('find/hospital')); ?>/<?php echo e($address['state_name']); ?>/4"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                            <p>Within 4KM</p></a>
                        </div>
                        <div class="col-md-3 col-xs-6  text-center sublist-container">
                             <a href="<?php echo e(url('find/hospital')); ?>/<?php echo e($address['state_name']); ?>/6"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                            <p>Within 6KM</p></a>
                        </div>
                        <div class="col-md-3 col-xs-6 text-center sublist-container">
                             <a href="<?php echo e(url('find/hospital')); ?>/<?php echo e($address['state_name']); ?>/8"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                            <p>Within 8KM</p></a>
                        </div>
                        <div class="col-md-3 col-xs-6  text-center sublist-container">
                             <a href="<?php echo e(url('find/hospital')); ?>/<?php echo e($address['state_name']); ?>/10"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                            <p>Within 10KM</p></a>
                        </div>
                        <div class="col-md-3 col-xs-6  text-center sublist-container">
                             <a href="<?php echo e(url('find/hospital')); ?>/<?php echo e($address['state_name']); ?>/12"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                            <p>Within 12KM</p></a>
                        </div>
                        <div class="col-md-3 col-xs-6  text-center sublist-container">
                             <a href="<?php echo e(url('find/hospital')); ?>/<?php echo e($address['state_name']); ?>/14"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                            <p>Within 14KM</p></a>
                        </div>
                        <div class="col-md-3 col-xs-6  text-center sublist-container">
                             <a href="<?php echo e(url('find/hospital')); ?>/<?php echo e($address['state_name']); ?>/16"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                                 <p>Within 16KM</p></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="dcenter">
            <div class="container sublist-top-container" id="tests-container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-12 padding-0">
                        <?php for($j=1; $j<15; $j++): ?>
                            <div class="col-md-3 col-xs-6  text-center sublist-container">
                                <a href="<?php echo e(url('find/diagnostic')); ?>/<?php echo e($address['state_name']); ?>/<?php echo e($tests[$j-1]->name); ?>"><p><img src="<?php echo e(($j>8?asset("public/images/med_icons/diagnos.png"):asset("public/images/med_icons/".$tests[$j-1]->name.".png") )); ?>"/></p>
                                    <p><?php echo e($tests[$j-1]->name); ?></p></a>
                            </div>
                        <?php if($j%4==0): ?>
                        </div>
                        <div class="col-xs-12 padding-0"> 
                        <?php endif; ?>
                        <?php endfor; ?>
                            <div class="col-md-3 col-xs-6  text-center sublist-container" style="margin: 30px 0px 0px 0px;"><a href="javascript:void(0);" id="other-tests">Other Tests</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</p>
<script type="text/javascript">
    $("#other-specialties").click(function(){ 
        $.ajax({
            type: "POST",
            url: "<?php echo e(url('get-specialties')); ?>",
            data:{"_token":"<?php echo e(csrf_token()); ?>"},
            success: function(result){ 
               $("#specilities-container").html(result);
            },
            error:function(result){
                console.log(result);
            }
        });
    });
    $("#other-tests").click(function(){ 
        $.ajax({
            type: "POST",
            url: "<?php echo e(url('get-tests')); ?>",
            data:{"_token":"<?php echo e(csrf_token()); ?>"},
            success: function(result){ 
               $("#tests-container").html(result);
            },
            error:function(result){
                console.log(result);
            }
        });
    });
</script>