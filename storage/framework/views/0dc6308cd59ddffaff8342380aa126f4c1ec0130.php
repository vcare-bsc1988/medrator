<style type="text/css">
    #triple{
      /* width:760px; */
      margin-bottom:20px;
      overflow:hidden;
      /* border-top:1px solid #ccc; */
    }
    #triple li{
      line-height:2.0em;
      border-bottom:1px solid #f0f0f0;
      float:left;
      display:inline;
    }
    #double li  { width:50%;}
    #triple li  { width:33.333%; }
    #quad li    { width:25%; } 
    #six li     { width:16.666%; }
	.bottom-strip.p-both.white-bg{border-bottom: 4px solid #e4f0fa;}
	#triple li:before{content:url();}
</style>
<?php $__env->startSection('us::content'); ?>
<!--==================================
	 Header parts starts here
==================================-->
<?php echo $__env->make('us::layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>									   
<!--==================================
	 Header parts ends here
  ==================================-->
<!--==================================
	Banner parts starts here
  ==================================-->
<div class="banner-wrapper">
<div class="banner">
	<img src="<?php echo e(asset("public/images/banner2.jpg")); ?>" alt="banner" />
</div><!-- /.banner -->
<div class="magnet">
<div class="container">
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <?php echo e(Form::open(array('url' => 'find', 'method' => 'get', 'id'=>'search-form', 'class'=>'search-form-0 col-xs-12'))); ?>

        <div class="form-group">
            <div class="col-xs-12 col-md-4 padding-0">
                <?php echo e(Form::text('location', null, ['class'=>'form-control location-input', 'placeholder'=>'Location','id'=>'location'])); ?>

                <div class="location-details">
                    <?php echo e(Form::hidden('latitude', null,['data-geo'=>'lat'])); ?>

                    <?php echo e(Form::hidden('longitude', null,['data-geo'=>'lng'])); ?>

                    <?php echo e(Form::hidden('locality', null,['data-geo'=>'locality'])); ?>

                    <?php echo e(Form::hidden('city_name', null,['data-geo'=>'locality'])); ?>

                    <?php echo e(Form::hidden('state_name', null,['data-geo'=>'administrative_area_level_1'])); ?>

                    <?php echo e(Form::hidden('country_name', null,['data-geo'=>'country_short'])); ?>

                    <?php echo e(Form::hidden('pincode', null,['data-geo'=>'postal_code'])); ?>

                </div>
            </div>
            <div class="col-xs-12 col-md-7 padding-0">
                <?php echo e(Form::text('q', null,['id'=>'search-box', 'class'=>'form-control sinput', 'placeholder'=>'Specialities, Doctors, Hospitals, Clinics, Labs','autocomplete'=>'off'])); ?>

                <div id="suggesstion-box"></div>
            </div>
            <div class="col-xs-12 col-md-1 padding-0">
                <button type="submit" id="search" class="btn btn-info search-btn"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        <?php echo e(Form::close()); ?>

    </div><!--/.main-page-form-->	
</div><!-- /.row -->
</div><!-- /.container -->  
</div><!-- /.magnet --><!-- /.magnet -->
</div><!-- /.banner-wrapper -->    
<!--==================================
              Banner parts ends here
        ==================================-->
<!--==================================
              service Quick links starts here
        ==================================--> 
<div class="first-section ptb">
    <div class="container">
        <div class="row" id="entity-container">
            <h3 class="text-center">BROWSE BY CATEGORY</h3>
            <p>
                <ul class="nav nav-tabs nav-justified search-advanced-tabs" role="tablist">
                    <li role="presentation" class="active text-center dumpo">
                        <a href="#doctors" aria-controls="doctors" role="tab" data-toggle="tab">
                            <p class="text-center"><img src="<?php echo e(asset("public/images/med_icons/doctor.png")); ?>" width="60"/></p>
                            <p>Doctors</p>
                            <p><i class="fa fa-minus"></i></p>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#hospitals" aria-controls="hospitals" role="tab" data-toggle="tab">
                            <p class="text-center"><img src="<?php echo e(asset("public/images/med_icons/hospital.png")); ?>" width="70"/></p>
                            <p>Hospitals</p>
                            <p><i class="fa fa-plus"></i></p>								
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#dcenter" aria-controls="dcenter" role="tab" data-toggle="tab">
                            <p class="text-center"><img src="<?php echo e(asset("public/images/med_icons/diagonstic_center.png")); ?>" width="42"/></p>
                            <p>Diagnostic Centers</p>
                            <p><i class="fa fa-plus"></i></p>
                        </a>
                    </li>
                 </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="doctors">
                        <div class="container sublist-top-container">
                            <div class="row">
                                <div class="col-xs-12">
								<div class="col-xs-12 padding-0">
									<?php $count = \App\Model\Specialty::get()->count();?>
                                    <?php for($i=1; $i<$count; $i++): ?>
                                        <div class="col-md-3 col-xs-6  text-center sublist-container">
                                            <a href="<?php echo e(url('us/find/doctor')); ?>/<?php echo e($address['state_name']); ?>/<?php echo e($specialties[$i-1]->name); ?>">
											<p>
											<?php if(file_exists(public_path("images/med_icons/".$specialties[$i-1]->name.".png"))): ?>
												<img src="<?php echo e(asset("public/images/med_icons/".$specialties[$i-1]->name.".png")); ?>"/>
											<?php endif; ?>
											</p>
                                            <p><?php echo e($specialties[$i-1]->name); ?></p></a>
                                        </div>
                                    <?php if($i%4==0): ?>
                                    </div>
                                    <div class="col-xs-12 padding-0"> 
                                    <?php endif; ?>
                                    <?php endfor; ?>
                                        <div class="col-md-3 col-xs-6  text-center sublist-container" style="margin: 30px 0px 0px 0px;"><a href="javascript:void(0);" id="other-specialties">Other Specialties</a></div>                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="hospitals">
                        <div class="container sublist-top-container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-md-3 col-xs-6  text-center sublist-container">
                                        <a href="<?php echo e(url('us/find/hospital')); ?>/<?php echo e($address['state_name']); ?>/2"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                                        <p>Within 2KM</p></a>
                                    </div>
                                    <div class="col-md-3 col-xs-6  text-center sublist-container">
                                         <a href="<?php echo e(url('us/find/hospital')); ?>/<?php echo e($address['state_name']); ?>/4"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                                        <p>Within 4KM</p></a>
                                    </div>
                                    <div class="col-md-3 col-xs-6  text-center sublist-container">
                                         <a href="<?php echo e(url('us/find/hospital')); ?>/<?php echo e($address['state_name']); ?>/6"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                                        <p>Within 6KM</p></a>
                                    </div>
                                    <div class="col-md-3 col-xs-6 text-center sublist-container">
                                         <a href="<?php echo e(url('us/find/hospital')); ?>/<?php echo e($address['state_name']); ?>/8"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                                        <p>Within 8KM</p></a>
                                    </div>
                                    <div class="col-md-3 col-xs-6  text-center sublist-container">
                                         <a href="<?php echo e(url('us/find/hospital')); ?>/<?php echo e($address['state_name']); ?>/10"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                                        <p>Within 10KM</p></a>
                                    </div>
                                    <div class="col-md-3 col-xs-6  text-center sublist-container">
                                         <a href="<?php echo e(url('us/find/hospital')); ?>/<?php echo e($address['state_name']); ?>/12"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                                        <p>Within 12KM</p></a>
                                    </div>
                                    <div class="col-md-3 col-xs-6  text-center sublist-container">
                                         <a href="<?php echo e(url('us/find/hospital')); ?>/<?php echo e($address['state_name']); ?>/14"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                                        <p>Within 14KM</p></a>
                                    </div>
                                    <div class="col-md-3 col-xs-6  text-center sublist-container">
                                         <a href="<?php echo e(url('us/find/hospital')); ?>/<?php echo e($address['state_name']); ?>/16"><p><img src="<?php echo e(asset("public/images/loc_icon.png")); ?>"/></p>
                                             <p>Within 16KM</p></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dcenter">
                        <div class="container sublist-top-container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 padding-0">
                                    <?php for($j=1; $j<15; $j++): ?>
                                        <div class="col-md-3 col-xs-6  text-center sublist-container">
                                            <a href="<?php echo e(url('us/find/diagnostic')); ?>/<?php echo e($address['state_name']); ?>/<?php echo e($tests[$j]->name); ?>"><p><img src="<?php echo e(($j>8?asset("public/images/med_icons/diagnos.png"):asset("public/images/med_icons/".$tests[$j]->name.".png") )); ?>"/></p>
                                                <p><?php echo e($tests[$j]->name); ?></p></a>
                                        </div>
                                    <?php if($j%4==0): ?>
                                    </div>
                                    <div class="col-xs-12 padding-0"> 
                                    <?php endif; ?>
                                    <?php endfor; ?>
                                        <div class="col-md-3 col-xs-6  text-center sublist-container" style="margin: 30px 0px 0px 0px;"><a href="javascript:void(0);" id="other-specialties">Other Tests</a></div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </p>
        </div>
    </div>
</div>
   
 <!--==================================
		Service Quick liniks ends here
	  ==================================-->
 <!--==================================
		News update and our doctors starts here
	  ==================================-->
<div class="second-section p82-topbot" style="min-height:600px;">
<div class="container">
<div class="row">
	
	<div class="col-sm-8 col-xs-12 must member">
            <h2 class="title-grp">
              <span>Best Doctor</span>
                            Around You
            </h2>
            <div class="all-doc">
            <ul class="sliding">
            <li> 
            <?php for($i=1; $i < count($homeDoctorList); $i++): ?>
                <div class="col-sm-6">
                        <div class="white-bx">
                                <?php if($homeDoctorList[$i]['image']): ?>
                                    <img src="<?php echo e($homeDoctorList[$i]['image']); ?>" alt="doc">
                                <?php else: ?>
                                    <img src="<?php echo e(asset("public/img/doc_placeholder.png")); ?>" alt="doc">
                                <?php endif; ?>
                                <div class="letter" style="padding-bottom:15px;">
                                <h2><span><?php echo e($homeDoctorList[$i]['name']); ?> </span>
                                    <?php if(isset($homeDoctorList[$i]['speciality']) && !empty($homeDoctorList[$i]['speciality'])): ?>
                                        <?php for($j=0; ($j < count($homeDoctorList[$i]['speciality']) && $j<2); $j++): ?>
                                            <?php if($j>0): ?> , <?php endif; ?> <?php echo e($homeDoctorList[$i]['speciality'][$j]); ?> 
                                        <?php endfor; ?>
                                    <?php endif; ?>
									<br><?php echo e($homeDoctorList[$i]['address_one']); ?>

                                </h2>
                                </div>
                                <div class="social-media">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo e(url('us/doctor')); ?>/<?php echo e($homeDoctorList[$i]['name_slug']); ?>" class="bg">More</a></div><!-- /.social-media -->	
                        </div><!-- /.white-bx -->
                </div><!-- /.col-sm-6 -->
                <?php if($i%4==0 && $i<(count($homeDoctorList)-1)): ?> </li><li>
				<?php elseif($i==(count($homeDoctorList)-1)): ?>
				</li>
                <?php endif; ?>				
            <?php endfor; ?>		       
            </ul>
            </div><!-- /.all-doc -->   
			<div class="clearfix"></div>
			<div class="col-xs-12 padding-0 member" >
			<h2 class="title-grp">
              <span>Doctors</span>
                             You Maybe Interested In
            </h2>
            <div class="all-doc">
            <ul class="sliding1">
            <li> 
            <?php for($i=1; $i < count($homeSearchDoctorList); $i++): ?>
                <div class="col-sm-6">
                        <div class="white-bx">
                                <?php if($homeSearchDoctorList[$i]['image']): ?>
                                    <img src="<?php echo e($homeSearchDoctorList[$i]['image']); ?>" alt="doc">
                                <?php else: ?>
                                    <img src="<?php echo e(asset("public/img/doc_placeholder.png")); ?>" alt="doc">
                                <?php endif; ?>
                                <div class="letter" style="padding-bottom:15px;">
                                <h2><span><?php echo e($homeSearchDoctorList[$i]['name']); ?> </span>
                                    <?php if(isset($homeSearchDoctorList[$i]['speciality']) && !empty($homeSearchDoctorList[$i]['speciality'])): ?>
                                        <?php for($j=0; ($j < count($homeSearchDoctorList[$i]['speciality']) && $j<2); $j++): ?>
                                            <?php if($j>0): ?> , <?php endif; ?> <?php echo e($homeSearchDoctorList[$i]['speciality'][$j]); ?> 
                                        <?php endfor; ?>
                                    <?php endif; ?>
									<br><?php echo e($homeSearchDoctorList[$i]['address_one']); ?>

                                </h2>
                                </div>
                                <div class="social-media">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo e(url('us/doctor')); ?>/<?php echo e($homeSearchDoctorList[$i]['name_slug']); ?>" class="bg">More</a></div><!-- /.social-media -->	
                        </div><!-- /.white-bx -->
                </div><!-- /.col-sm-6 --> 
                <?php if($i%4==0 && $i<(count($homeSearchDoctorList)-1)): ?>  </li><li>
				<?php elseif($i==(count($homeSearchDoctorList)-1)): ?>
				</li>
                <?php endif; ?> 				
            <?php endfor; ?>		       
            </ul>
            </div><!-- /.all-doc -->   
            </div><!-- /.all-doc -->   
		
	 </div><!-- /.col-sm-8 -->
	<div class="col-sm-4 col-xs-12 member">
	<h2 class="title-grp">
	<span>Articles & Discussions</span>
		 For You
	</h2>
	
	<div class="all-news">
		<ul class="disast">
			<?php foreach($blogs as $blog): ?>
			<li style="padding: 8px 0px 30px 0px;">
				<div class="news-date">
					<?php $date = \Carbon\Carbon::parse($blog->post_date) ?>
					<span><?php echo e($date->day); ?></span>
					<?php echo e($date->month); ?>-<?php echo e($date->year); ?>

				</div>
				<div class="news-text">
				<h2><?php echo e($blog->post_title); ?> </h2> 
				<p><?php echo substr(strip_tags( $blog->post_content),0,72); ?>...<a href="<?php echo e($blog->guid); ?>">more</a
				</div>
			</li>
			<?php endforeach; ?>			
		</ul><!-- ./disast -->
	</div><!-- /.all-news -->
	</div><!-- /.col-sm-4 -->
</div><!-- /.row -->
</div><!-- /.container -->  
</div><!-- /.second-section -->      
 <!--==================================
		COLLECTION PART START
	  ==================================-->  
	  
 <!--==================================
		COLLECTION PART
	  ==================================-->
<div class="first-section padding-bottom-80">
<div class="container">
<div class="row">
			<h3 class="text-center ptb">COLLECTION</h3>
			 <div class="col-sm-3 element-lft lft-bottom">
				  <img src="<?php echo e(asset("public/images/med_icons/CLOG.png")); ?>" alt="blog.png") }}" width="60">
				  <div class="text-needleft">
				  <h2>TOP BLOGS</h2>
				  <p>Scrambled it to make a type specimen book. It has survived not only five centuries</p>
				  </div>
				  <a href="<?php echo e(url('us/blogs-forums')); ?>" class="var2-bt"></a>
			 </div>

			 <div class="col-sm-3 element-lft lft-bottom">
				  <img src="<?php echo e(asset("public/images/med_icons/forums.png")); ?>" alt="Forum.png") }}" width="60">
				  <div class="text-needleft">
				  <h2>TOP FORUMS</h2>
				  <p>Scrambled it to make a type specimen book. It has survived not only five centuries</p>
				  </div>
				  <a href="<?php echo e(url('us/blogs-forums/activity')); ?>" class="var2-bt active"></a>
			 </div>

			 <div class="col-sm-3 element-lft">
				   <img src="<?php echo e(asset("public/images/med_icons/donation.png")); ?>" alt="donation.png") }}" width="60">
				   <div class="text-needleft">
				   <h2>ORGAN DONATION</h2>
				   <p>Scrambled it to make a type specimen book. It has survived not only five centuries</p>
				   </div>
				   <a href="<?php echo e(url('us/home/organ-donation')); ?>" class="var2-bt"></a>
			 </div>

			 <div class="col-sm-3 element-lft">
				   <img src="<?php echo e(asset("public/images/med_icons/medicaltourism.png")); ?>" alt="medical_tourism.png") }}" width="60">
				   <div class="text-needleft">
				   <h2>MEDICAL TOURISM</h2>
				   <p>Scrambled it to make a type specimen book. It has survived not only five centuries</p>
				   </div>
				   <a href="<?php echo e(url('us/home/medical-tourism')); ?>" class="var2-bt"></a>
			 </div>
</div><!-- /.row -->
</div><!-- /.container -->  
</div><!-- /.first-section -->   	  
<!-- =================================
		APP DOWNLOAD PART START 
	================================== -->  
<div class="second-section download-parts">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-sm-6 padding-top-80">
				<h3>MEDRATOR</h3>
				<p>Merator is platform where you can find, create profile for doctors, Hospitals, Clinics, Labs etc.<p>
				<p>
					Download our app by clicking the below buttons and put the features of medrator in your pocket.
				</p>
				<p class="ptb">
				</p>
				<p class="text-center-small">
					<a href=""><img src="<?php echo e(asset("public/images/med_icons/googleplay.png")); ?>" style="height:60px;border-radius:5px;margin-bottom:5px;"/> </a>
					<a href=""> <img src="<?php echo e(asset("public/images/med_icons/appstoredownload.png")); ?>" style="height:60px;border-radius:5px;margin-bottom:5px;"/></a>
				</p>					
			</div>
			<div class="col-xs-12 col-md-4 col-sm-6 mobile-part hidden-xs">
				<img src="<?php echo e(asset("public/images/med_icons/mobile.png")); ?>" />
			</div>
		</div>
	</div>
</div>
<!--==================================
       		footer parts starts here
          ==================================-->  
<script type="text/javascript">
    $(function(){
		
		// $('body,html').bind('mousewheel', function(e) {
			
			// $('#ui-datepicker-div').hide();
		// });
        $("#location").geocomplete({
            details: ".location-details",
            detailsAttribute: "data-geo",
            location: "<?php echo e($address['address_one']); ?>",
        });
        $("#location").bind("geocode:result", function(event, result){
            $.ajax({
                type: "POST",
                url: "<?php echo e(url('us/session/stor_city')); ?>",
                data: $("#search-form").serialize(),
                success: function(result){ 
                   $("#entity-container").html(result);
                },
                error:function(result){
                    console.log(result);
                }
            });
        });
    });   
    $("#other-specialties").click(function(){ 
        $.ajax({
            type: "POST",
            url: "<?php echo e(url('get-specialties')); ?>",
            data:{"_token":"<?php echo e(csrf_token()); ?>"},
            success: function(result){ 
               $("#specilities-container").html(result);
            },
            error:function(result){
                console.log(result);
            }
        });
    });
    $("#other-tests").click(function(){ 
        $.ajax({
            type: "POST",
            url: "<?php echo e(url('get-tests')); ?>",
            data:{"_token":"<?php echo e(csrf_token()); ?>"},
            success: function(result){ 
               $("#tests-container").html(result);
            },
            error:function(result){
                console.log(result);
            }
        });
    });
</script>		  
<?php echo $__env->make('us::layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('us::layouts.home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>