<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
class User extends Authenticatable
{
    use EntrustUserTrait; // add this trait to your user model
    protected $primaryKey = 'id';
    protected $table = 'login';
    protected $fillable = [
        'name', 'email','mobile', 'password','role',
		'device_token','device_type','country_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	public function user()
    {
         return $this->hasOne('App\Model\Users');
    }
	public function doctor(){
		return $this->hasOne('App\Model\Doctor');
	}
	public function diagnostic(){
		return $this->hasOne('App\Model\Diagnostic');
	}
	public function hospital(){
		return $this->hasOne('App\Model\Hospital');
	}
	public function clinic(){
		return $this->hasOne('App\Model\Clinics');
	}
	public function claimedProfile()
	{
		return $this->hasOne('App\Model\ClaimedProfile');
	}
    public function role(){
        return $this->hasOne('App\Model\Role');
    }
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
	public function socialAccount(){
		return $this->hasMany('App\SocialAccount');
	}
	public function interests(){
		return $this->hasMany('App\Model\UserInterest');
	}
}
