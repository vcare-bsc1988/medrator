<?php

namespace App\Http\Controllers\Api\Beta;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Vcareall\Admin\Helper;
class SearchController extends Controller
{
    protected $client;
	protected $helper;
    public function __construct(\Solarium\Client $client)
    {
        $this->client = $client;
		$this->helper = new \App\Helpers\Helper;
    }
	public function index(Request $request)
	{
		try {
			$query = $this->client->createSelect();
			$helper = $query->getHelper();
			if(trim($request->q)){
				/***
                 * Matching indexrs.
                 */
                $special_characters = array('/', '&', '!', '.', '-', '+');
                $q = str_replace($special_characters,'',$request->q);
                $query->setQuery('*'.$q.'*');
                $query->setQueryDefaultOperator('AND');
			}
			$latitude =$request->input('latitude');
			$longitude=$request->input('longitude');
			
			# Applying filter
			$settings = \App\Model\Setting::where('title','radius')->get()->first();              
			if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
				$gender = $request->input('filters')['gender'];
				$query->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$gender, 'tag'=>'exclude')); 
			}
			if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
				$rating = $request->input('filters')['rating'];
				$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
			}
			if(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']<16){
				$distance = $request->input('filters')['distance'];
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($distance)
					)
				);
			}
			elseif(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']>15){
				$query->createFilterQuery('distance')->setQuery($helper->rangeQuery('distance', 16, 18024));
			}
			else{
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($settings->value)
					)
				);
			}
			if(isset($request->input('filters')['experience']) && $request->input('filters')['experience']!=""){
				$min = $request->input('filters')['experience']['minvalue'];
				$max = $request->input('filters')['experience']['maxvalue'];
				$query->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', $min, $max));
			}
			if(isset($request->input('filters')['consulting_fee']) && $request->input('filters')['consulting_fee']!=""){
				$min = $request->input('filters')['consulting_fee']['minvalue'];
				$max = $request->input('filters')['consulting_fee']['maxvalue'];
				$query->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $min, $max));
			}
			if(isset($request->input('filters')['entity']) && $request->input('filters')['entity']!=""){
				$query->addFilterQuery(array('key'=>'role', 'query'=>'role:'.$request->input('filters')['entity'], 'tag'=>'exclude'));
			}
			if(isset($request->input('filters')['working_days']) && $request->input('filters')['working_days']!=""){
			
				
			}
			## Finsihed applying filter.
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			);
			$query->addSort('{!func}' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				),'asc');
			###### Paggination 
			$page = $request->input('page');
			if($request->input('limit')){
				$limit = $request->input('limit');
			}else{
				$limit = 50;
			}                     
			if(!is_numeric($page) or $page==0){
				$page = 1;
			}
			$start=($page - 1)*$limit;
			$query->setStart($start);
			$query->setRows($limit);
			## Paggination finished
			$resultset = $this->client->select($query);
			$resultset = $resultset->getData()['response']['docs'];			
			$i=0;
			$resultsetFinal = [];
			foreach($resultset as $temp){ 
				/***
				 * Finding doctor associated with hospital.
				 */
				if(isset($temp['role']) && $temp['role'] ==5){
					### Finding associated doctor
					$assoDocResultset = [];
					$hospital = \App\Model\Hospital::find($temp['id']);
					if($hospital->doctors()->get()->count()>0){
						$doctors = $hospital->doctors()->get();
						$ids = "(";
						$i=1;
						foreach($doctors as $var){
							$ids.=$var->id;
							if(count($doctors)>$i++){
									$ids.=' or ';
							}
						}
						$ids.=')';
						$assoDocQuery = $this->client->createSelect();
						$assoDocHelper = $assoDocQuery->getHelper();
						# Adding distance field
						$assoDocQuery->addField('distance:' . $assoDocHelper->geodist(
								'latlon', 
								doubleval($latitude), 
								doubleval($longitude)
							)
						); 
						$assoDocQuery->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$ids, 'tag'=>'exclude'));
						if(isset($request->specialties)){
							$specialties = "(";
							$i=1;
							foreach($request->specialties as $var){
									$specialties.=$var['id'];
									if(count($request->specialties)>$i++){
											$specialties.=' or ';
									}				
							}
							$specialties.=')';
							$assoDocQuery->addFilterQuery(array('key'=>'speciality', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
						}
						if(isset($request->qualifications)){
							$qualifications = "(";
							$i=1;
							foreach($request->qualifications as $var){
									$qualifications.=$var['id'];
									if(count($request->qualifications)>$i++){
											$qualifications.=' or ';
									}				
							}
							$qualifications.=')';
							$assoDocQuery->addFilterQuery(array('key'=>'qualification', 'query'=>'edu_id:'.$qualifications, 'tag'=>'exclude'));
						}
						if($request->services){
							$services = "(";
							$i=1;
							foreach($request->services as $var){
									$services.=$var['id'];
									if(count($request->services)>$i++){
											$services.=' or ';
									}
							}
							$services.=')';
							$assoDocQuery->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
						}               
						if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
							$gender = $request->input('filters')['gender'];
							$assoDocQuery->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$gender, 'tag'=>'exclude')); 
						}
						if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){ 
							$rating = $request->input('filters')['rating'];
							$assoDocQuery->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
						}
						if(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']<16){
							$distance = $request->input('filters')['distance'];
							$assoDocQuery->createFilterQuery('distance')->setQuery(
								$helper->geofilt(
									'latlon', 
									doubleval($latitude),
									doubleval($longitude),
									doubleval($distance)
								)
							);
						}
						elseif(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']>15){
							$assoDocQuery->createFilterQuery('distance')->setQuery($helper->rangeQuery('distance', 16, 18024));
						}
						else{
							$assoDocQuery->createFilterQuery('distance')->setQuery(
								$helper->geofilt(
									'latlon', 
									doubleval($latitude),
									doubleval($longitude),
									doubleval($settings->value)
								)
							);
						}
						if(isset($request->input('filters')['experience']) && $request->input('filters')['experience']!=""){
							$min = $request->input('filters')['experience']['minvalue'];
							$max = $request->input('filters')['experience']['maxvalue'];
							$assoDocQuery->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', $min, $max));
						}
						if(isset($request->input('filters')['consulting_fee']) && $request->input('filters')['consulting_fee']!=""){
							$min = $request->input('filters')['consulting_fee']['minvalue'];
							$max = $request->input('filters')['consulting_fee']['maxvalue'];
							$assoDocQuery->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $min, $max));
						}
						if(isset($request->input('filters')['working_days']) && $request->input('filters')['working_days']!=""){
						
							
						}
						$assoDocResultset = $this->client->select($assoDocQuery);				
						$assoDocResultset = $assoDocResultset->getData()['response']['docs'];
					}
					$resultsetFinal[] = $temp;
					foreach($assoDocResultset as $tempAssoDoc){
						$tempAssoDoc['distance'] = Helper::distance($latitude,$longitude,$temp['latitude'],$temp['longitude'],'K');
						$tempAssoDoc['rating'] = $temp['rating'];
						$tempAssoDoc['review_count'] = $temp['review_count'];
						$tempAssoDoc['rating_one_star_count'] = $temp['rating_one_star_count'];
						$tempAssoDoc['rating_two_star_count'] = $temp['rating_two_star_count'];
						$tempAssoDoc['rating_three_star_count'] = $temp['rating_three_star_count'];
						$tempAssoDoc['rating_four_star_count'] = $temp['rating_four_star_count'];
						$tempAssoDoc['rating_five_star_count'] = $temp['rating_five_star_count'];
						$tempAssoDoc['address_one'] = $temp['address_one'];
						$resultsetFinal[] = $tempAssoDoc;
					}
				}else{
					$resultsetFinal[] = $temp;
				}
			}
			/**
			 * Adding doctor qualification.
			 */
			for($i=0; $i<count($resultsetFinal); $i++){
				if(isset($resultsetFinal[$i]['role']) && $resultsetFinal[$i]['role'] == 2):
					$resultsetFinal[$i]['qualifications']= \App\Model\Doctor::getQualifications($resultsetFinal[$i]['id']);
				endif;
			}
			/**
			 * Remove duplicat entries.
			 */
			$result = Helper::removeDuplicateArr($resultsetFinal);
			if(!$result){
				$response = array('status'=>0, 'message'=>'No result found.'); 
				$response = json_encode($response);
				return $response;
			}
			#Save search
			if($request->input('user_id')){
				if(\App\Model\RecentSearch::where('user_id',$request->input('user_id'))->get()->count()>9){
					#Delete old entries & and new entry
					$id = \App\Model\RecentSearch::where('user_id',$request->input('user_id'))->orderBy('id','asc')->get()->first()->id;
					\App\Model\RecentSearch::destroy($id);                        
					if(\App\Model\RecentSearch::where('q',$request->input('q'))->where('user_id',$request->input('user_id'))->get()->count()>0){
						$recentId = \App\Model\RecentSearch::where('q',$request->input('q'))->where('user_id',$request->input('user_id'))->get()->first()->id;
						\App\Model\RecentSearch::destroy($recentId);  
						\App\Model\RecentSearch::create([
							"user_id"=>$request->input('user_id'),
							"q"=>$request->input('q'),
							"locality"=>$request->input('locality'),
							"address_one"=>$request->input('address_one'),
							"address_two"=>$request->input('address_two'),
							"latitude"=>$request->input('latitude'),
							"latitude"=>$request->input('latitude'),
							"longitude"=>$request->input('longitude'),
							"city_name"=>$request->input('city_name'),
							"state_name"=>$request->input('state_name'),
							"country_name"=>$request->input('country_name'),
						]);
					}else{
						\App\Model\RecentSearch::create([
							"user_id"=>$request->input('user_id'),
							"q"=>$request->input('q'),
							"locality"=>$request->input('locality'),
							"address_one"=>$request->input('address_one'),
							"address_two"=>$request->input('address_two'),
							"latitude"=>$request->input('latitude'),
							"latitude"=>$request->input('latitude'),
							"longitude"=>$request->input('longitude'),
							"city_name"=>$request->input('city_name'),
							"state_name"=>$request->input('state_name'),
							"country_name"=>$request->input('country_name'),
						]);
					}
				}else{
					if(\App\Model\RecentSearch::where('q',$request->input('q'))->where('user_id',$request->input('user_id'))->get()->count()>0){
						$recentId = \App\Model\RecentSearch::where('q',$request->input('q'))->where('user_id',$request->input('user_id'))->get()->first()->id;
						\App\Model\RecentSearch::destroy($recentId); 
						\App\Model\RecentSearch::create([
							"user_id"=>$request->input('user_id'),
							"q"=>$request->input('q'),
							"locality"=>$request->input('locality'),
							"address_one"=>$request->input('address_one'),
							"address_two"=>$request->input('address_two'),
							"latitude"=>$request->input('latitude'),
							"latitude"=>$request->input('latitude'),
							"longitude"=>$request->input('longitude'),
							"city_name"=>$request->input('city_name'),
							"state_name"=>$request->input('state_name'),
							"country_name"=>$request->input('country_name'),
						]); 
					}else{
					   \App\Model\RecentSearch::create([
							"user_id"=>$request->input('user_id'),
							"q"=>$request->input('q'),
							"locality"=>$request->input('locality'),
							"address_one"=>$request->input('address_one'),
							"address_two"=>$request->input('address_two'),
							"latitude"=>$request->input('latitude'),
							"latitude"=>$request->input('latitude'),
							"longitude"=>$request->input('longitude'),
							"city_name"=>$request->input('city_name'),
							"state_name"=>$request->input('state_name'),
							"country_name"=>$request->input('country_name'),
						]); 
					}
				}
			}
			$response = array('status'=>1, 'message'=>'success','data'=>$result); 
			$response = json_encode($response);
			return $response;
		} 
		catch (\Solarium\Exception $e) {
			$response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
			$response = json_encode($response);
			return $response;
		}
		catch(\Illuminate\Database\QueryException $e){
				return array('status'=>0, 'message'=>$e->getMessage());
		}
	}
	public function doctor(Request $request){
            try {
                $query = $this->client->createSelect();
                $helper = $query->getHelper();
				$latitude =$request->input('latitude');
                $longitude=$request->input('longitude');
				## Adding distance field
				$query->addField('distance:' . $helper->geodist(
						'latlon', 
						doubleval($latitude), 
						doubleval($longitude)
					)
                );
                #### Applying filter
				$settings = \App\Model\Setting::where('title','radius')->get()->first();              
				if(isset($request->specialties)){
                    $specialties = "(";
                    $i=1;
                    foreach($request->specialties as $var){
                            $specialties.=$var['id'];
                            if(count($request->specialties)>$i++){
                                    $specialties.=' or ';
                            }				
                    }
                    $specialties.=')';
                    $query->addFilterQuery(array('key'=>'speciality', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
                }
                if(isset($request->qualifications)){
                    $qualifications = "(";
                    $i=1;
                    foreach($request->qualifications as $var){
                            $qualifications.=$var['id'];
                            if(count($request->qualifications)>$i++){
                                    $qualifications.=' or ';
                            }				
                    }
                    $qualifications.=')';
                    $query->addFilterQuery(array('key'=>'qualification', 'query'=>'edu_id:'.$qualifications, 'tag'=>'exclude'));
                }
                if($request->services){
                    $services = "(";
                    $i=1;
                    foreach($request->services as $var){
                            $services.=$var['id'];
                            if(count($request->services)>$i++){
                                    $services.=' or ';
                            }
                    }
                    $services.=')';
                    $query->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
                }               
				if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
					$gender = $request->input('filters')['gender'];
					$query->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$gender, 'tag'=>'exclude')); 
				}
				if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){ 
					$rating = $request->input('filters')['rating'];
					$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
				}
				if(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']<16){
					$distance = $request->input('filters')['distance'];
					$query->createFilterQuery('distance')->setQuery(
						$helper->geofilt(
							'latlon', 
							doubleval($latitude),
							doubleval($longitude),
							doubleval($distance)
						)
					);
				}
				elseif(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']>15){
					$query->createFilterQuery('distance')->setQuery($helper->rangeQuery('distance', 16, 18024));
				}
				else{
					$query->createFilterQuery('distance')->setQuery(
						$helper->geofilt(
							'latlon', 
							doubleval($latitude),
							doubleval($longitude),
							doubleval($settings->value)
						)
					);
				}
				if(isset($request->input('filters')['experience']) && $request->input('filters')['experience']!=""){
					$min = $request->input('filters')['experience']['minvalue'];
					$max = $request->input('filters')['experience']['maxvalue'];
					$query->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', $min, $max));
				}
				if(isset($request->input('filters')['consulting_fee']) && $request->input('filters')['consulting_fee']!=""){
					$min = $request->input('filters')['consulting_fee']['minvalue'];
					$max = $request->input('filters')['consulting_fee']['maxvalue'];
					$query->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $min, $max));
				}
				if(isset($request->input('filters')['working_days']) && $request->input('filters')['working_days']!=""){				
					$weekDays = "(";
                    $i=1;
                    foreach($request->input('filters')['working_days'] as $var){
                        $weekDays.=$var['value'];
                        if(count($request->input('filters')['working_days'])>$i++){
                            $weekDays.=' or ';
                        }				
                    }
                    $weekDays.=')';
                    $query->addFilterQuery(array('key'=>'week_day', 'query'=>'days_of_week:'.$weekDays, 'tag'=>'exclude'));
				}
                $facets = $query->getFacetSet();
                $facets->createFacetField(array('field'=>'speciality', 'exclude'=>'exclude'));
                $query->addSort('{!func}' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				),'asc');
				###### Paggination 
				$page = $request->input('page');
				if($request->input('limit')){
					$limit = $request->input('limit');
				}else{
					$limit = 50;
				}                     
				if(!is_numeric($page) or $page==0){
					$page = 1;
				}
				$start=($page - 1)*$limit;
				$query->setStart($start);
				$query->setRows($limit);
				## Paggination finished
                $resultset = $this->client->select($query);
				
                $resultset = $resultset->getData()['response']['docs'];
				$i=0;
				$doctorArr = [];
                foreach($resultset as $temp){ 
					if(isset($temp['role']) && $temp['role'] ==2){
						$doctorArr[] = $temp;
					}
                    /***
					 * Finding doctor associated with clinic
					 */
					if(isset($temp['role']) && $temp['role'] ==5){
						### Finding associated doctor
						$assoDocResultset = [];
						$hospital = \App\Model\Hospital::find($temp['id']);
						if($hospital->doctors()->get()->count()>0){
							$doctors = $hospital->doctors()->get();
							$ids = "(";
							$i=1;
							foreach($doctors as $var){
								$ids.=$var->id;
								if(count($doctors)>$i++){
										$ids.=' or ';
								}
							}
							$ids.=')';
							$assoDocQuery = $this->client->createSelect();
							$assoDocHelper = $assoDocQuery->getHelper();
							# Adding distance field
							$assoDocQuery->addField('distance:' . $assoDocHelper->geodist(
									'latlon', 
									doubleval($latitude), 
									doubleval($longitude)
								)
							); 
							$assoDocQuery->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$ids, 'tag'=>'exclude'));
							if(isset($request->specialties)){
								$specialties = "(";
								$i=1;
								foreach($request->specialties as $var){
										$specialties.=$var['id'];
										if(count($request->specialties)>$i++){
												$specialties.=' or ';
										}				
								}
								$specialties.=')';
								$assoDocQuery->addFilterQuery(array('key'=>'speciality', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
							}
							if(isset($request->qualifications)){
								$qualifications = "(";
								$i=1;
								foreach($request->qualifications as $var){
										$qualifications.=$var['id'];
										if(count($request->qualifications)>$i++){
												$qualifications.=' or ';
										}				
								}
								$qualifications.=')';
								$assoDocQuery->addFilterQuery(array('key'=>'qualification', 'query'=>'edu_id:'.$qualifications, 'tag'=>'exclude'));
							}
							if($request->services){
								$services = "(";
								$i=1;
								foreach($request->services as $var){
										$services.=$var['id'];
										if(count($request->services)>$i++){
												$services.=' or ';
										}
								}
								$services.=')';
								$assoDocQuery->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
							}               
							if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
								$gender = $request->input('filters')['gender'];
								$assoDocQuery->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$gender, 'tag'=>'exclude')); 
							}
							if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){ 
								$rating = $request->input('filters')['rating'];
								$assoDocQuery->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
							}
							if(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']<16){
								$distance = $request->input('filters')['distance'];
								$assoDocQuery->createFilterQuery('distance')->setQuery(
									$helper->geofilt(
										'latlon', 
										doubleval($latitude),
										doubleval($longitude),
										doubleval($distance)
									)
								);
							}
							elseif(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']>15){
								$assoDocQuery->createFilterQuery('distance')->setQuery($helper->rangeQuery('distance', 16, 18024));
							}
							else{
								$assoDocQuery->createFilterQuery('distance')->setQuery(
									$helper->geofilt(
										'latlon', 
										doubleval($latitude),
										doubleval($longitude),
										doubleval($settings->value)
									)
								);
							}
							if(isset($request->input('filters')['experience']) && $request->input('filters')['experience']!=""){
								$min = $request->input('filters')['experience']['minvalue'];
								$max = $request->input('filters')['experience']['maxvalue'];
								$assoDocQuery->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', $min, $max));
							}
							if(isset($request->input('filters')['consulting_fee']) && $request->input('filters')['consulting_fee']!=""){
								$min = $request->input('filters')['consulting_fee']['minvalue'];
								$max = $request->input('filters')['consulting_fee']['maxvalue'];
								$assoDocQuery->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $min, $max));
							}
							$assoDocResultset = $this->client->select($assoDocQuery);				
							$assoDocResultset = $assoDocResultset->getData()['response']['docs'];
						}
						foreach($assoDocResultset as $tempAssoDoc){
							$tempAssoDoc['distance'] = Helper::distance($latitude,$longitude,$temp['latitude'],$temp['longitude'],'K');
							/* $tempAssoDoc['rating'] = $temp['rating'];
                            $tempAssoDoc['review_count'] = $temp['review_count'];
                            $tempAssoDoc['rating_one_star_count'] = $temp['rating_one_star_count'];
                            $tempAssoDoc['rating_two_star_count'] = $temp['rating_two_star_count'];
                            $tempAssoDoc['rating_three_star_count'] = $temp['rating_three_star_count'];
                            $tempAssoDoc['rating_four_star_count'] = $temp['rating_four_star_count'];
                            $tempAssoDoc['rating_five_star_count'] = $temp['rating_five_star_count'];
                            $tempAssoDoc['address_one'] = $temp['address_one']; */
							$doctorArr[] = $tempAssoDoc;
						}
					}
				}
				/**
				 * Adding doctor qualification.
				 */
				for($i=0; $i<count($doctorArr); $i++){
					if(isset($doctorArr[$i]['role']) && $doctorArr[$i]['role'] == 2):
						$doctorArr[$i]['qualifications']= \App\Model\Doctor::getQualifications($doctorArr[$i]['id']);
					endif;
				}
				/**
				 * Remove duplicat entries.
				 */
				$result = Helper::removeDuplicateArray($doctorArr);
                if(!$result){
                    $response = array('status'=>0, 'message'=>'No result found.'); 
                    $response = json_encode($response);
                    return $response;
                }
                $response = array('status'=>1, 'message'=>'success','data'=>$result); 
                $response = json_encode($response);
                return $response;
            } 
            catch (\Solarium\Exception $e) {
                $response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
                $response = json_encode($response);
                return $response;
            }
	}
	public function hospital(Request $request)
	{
            try {
                $query = $this->client->createSelect();
                $helper = $query->getHelper();
				$settings = \App\Model\Setting::where('title','radius')->get()->first();              
				$latitude =$request->input('latitude');
                $longitude=$request->input('longitude');
				$query->addFilterQuery(array('key'=>'role', 'query'=>'role:5', 'tag'=>'exclude'));
                #Adding distance field
                $query->addField('distance:' . $helper->geodist(
                        'latlon', 
                        doubleval($latitude), 
                        doubleval($longitude)
                    )
                );
				if($request->specialties){
                    $specialties = "(";
                    $i=1;
                    foreach($request->specialties as $var){
                            $specialties.=$var['id'];
                            if(count($request->specialties)>$i++){
                                    $specialties.=' or ';
                            }				
                    }
                    $specialties.=')';
                    $query->addFilterQuery(array('key'=>'speciality', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
                }
                if($request->services){
                    $i=0;
                    $services = "(";
                    foreach($request->services as $var){
                            $services.=$var['id'];
                            if(count($request->services)>$i++){
                                    $services.=' or ';
                            }


                    }
                    $services.=')';
                    $query->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
                }
                $i=0;
                if($request->days_of_week){
                    $weeks = "(";
                    foreach($request->days_of_week as $var){
                            $weeks.=$var;
                            if(count($request->days_of_week)>$i++){
                                    $weeks.=' or ';
                            }										
                    }
                    $weeks.=')';
                    $query->addFilterQuery(array('key'=>'weeks', 'query'=>'days_of_week:'.$weeks, 'tag'=>'exclude'));
                }
                if($request->diagnostic_lab_facility ==0){
                    $query->addFilterQuery(array('key'=>'diagnostic_lab_facility', 'query'=>'diagnostic_lab_facility:false', 'tag'=>'exclude'));
                }elseif($request->diagnostic_lab_facility ==1){
                    $query->addFilterQuery(array('key'=>'diagnostic_lab_facility', 'query'=>'diagnostic_lab_facility:true', 'tag'=>'exclude'));
                }
                # Adding distance filter
				if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
					$rating = $request->input('filters')['rating'] -($request->input('filters')['rating']==0?0:1) ;
					$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
				}
				if(isset($request->input('filters')['cashless_mediclaim']) && $request->input('filters')['cashless_mediclaim']!=""){
					$cashless_mediclaim = $request->input('filters')['cashless_mediclaim'];
					$query->addFilterQuery(array('key'=>'cashless_mediclaim', 'query'=>'cashless_mediclaim:'.$cashless_mediclaim, 'tag'=>'exclude')); 
				}
				if(isset($request->input('filters')['hours24_emergency']) && $request->input('filters')['hours24_emergency']!=""){
					$hours24_emergency = $request->input('filters')['hours24_emergency'];
					$query->addFilterQuery(array('key'=>'24_hours_emergency', 'query'=>'24_hours_emergency:'.$hours24_emergency, 'tag'=>'exclude')); 
				}
				if(isset($request->input('filters')['icu_facility']) && $request->input('filters')['icu_facility']!=""){
					$icu_facility = $request->input('filters')['icu_facility'];
					$query->addFilterQuery(array('key'=>'icu_facility', 'query'=>'icu_facility:'.$icu_facility, 'tag'=>'exclude')); 
				}
				if(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']<16){
					$distance = $request->input('filters')['distance'];
					$query->createFilterQuery('distance')->setQuery(
						$helper->geofilt(
							'latlon', 
							doubleval($latitude),
							doubleval($longitude),
							doubleval($distance)
						)
					);
				}
				elseif(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']>15){
					$query->createFilterQuery('distance')->setQuery($helper->rangeQuery('distance', 16, 500000));
				}
				elseif($request->input('distance')!=""){ 
					$distance = $request->input('distance');
					$query->createFilterQuery('distance')->setQuery(
						$helper->geofilt(
							'latlon', 
							doubleval($latitude),
							doubleval($longitude),
							doubleval(trim($request->input('distance')))
						)
					);
				}
				else{ 
					$query->createFilterQuery('distance')->setQuery(
						$helper->geofilt(
							'latlon', 
							doubleval($latitude),
							doubleval($longitude),
							doubleval($settings->value)
						)
					);
				}
				if(isset($request->input('filters')['working_days']) && $request->input('filters')['working_days']!=""){				
					$weekDays = "(";
                    $i=1;
                    foreach($request->input('filters')['working_days'] as $var){
                        $weekDays.=$var['value'];
                        if(count($request->input('filters')['working_days'])>$i++){
                            $weekDays.=' or ';
                        }				
                    }
                    $weekDays.=')';
                    $query->addFilterQuery(array('key'=>'week_day', 'query'=>'days_of_week:'.$weekDays, 'tag'=>'exclude'));
				}
                $facets = $query->getFacetSet();
                $facets->createFacetField(array('field'=>'speciality', 'exclude'=>'exclude'));
                $query->addSort('{!func}' . $helper->geodist(
						'latlon', 
						doubleval($latitude), 
						doubleval($longitude)
					),'asc');
				###### Paggination 
				$page = $request->input('page');
				if($request->input('limit')){
					$limit = $request->input('limit');
				}else{
					$limit = 50;
				}                     
				if(!is_numeric($page) or $page==0){
					$page = 1;
				}
				$start=($page - 1)*$limit;
				$query->setStart($start);
				$query->setRows($limit);
				## Paggination finished
                $resultset = $this->client->select($query);
                $resultset = $resultset->getData()['response']['docs'];
                if(!$resultset){
                        $response = array('status'=>0, 'message'=>'No result found.'); 
                        $response = json_encode($response);
                        return $response;
                }
                $response = array('status'=>1, 'message'=>'success','data'=>$resultset); 
                $response = json_encode($response);
                return $response;
            } 
            catch (\Solarium\Exception $e) {
                $response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
                $response = json_encode($response);
                return $response;
            }
	}
	/**
	******[DEPRECATED]**************************************************
	public function clinic(Request $request)
	{
		try {
            $query = $this->client->createSelect();
			if($request->q){
				$query->setQuery($request->q);
			}if($request->role){
				$query->addFilterQuery(array('key'=>'role', 'query'=>'role:'.$request->role, 'tag'=>'exclude'));
			}
			if($request->specialties){
				$specialties = "(";
				$i=1;
				foreach($request->specialties as $var){
					$specialties.=$var['id'];
					if(count($request->specialties)>$i++){
						$specialties.=' or ';
					}
					
					
				}
				$specialties.=')';
				$query->addFilterQuery(array('key'=>'speciality', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
			}
			if($request->qualifications){
				$qualifications = "(";
				$i=1;
				foreach($request->qualifications as $var){
					$qualifications.=$var['id'];
					if(count($request->qualifications)>$i++){
						$qualifications.=' or ';
					}
					
					
				}
				$qualifications.=')';
				$query->addFilterQuery(array('key'=>'qualification', 'query'=>'edu_id:'.$qualifications, 'tag'=>'exclude'));
			}
			if($request->services){
				$services = "(";
				$i=1;
				foreach($request->services as $var){
					$services.=$var['id'];
					if(count($request->services)>$i++){
						$services.=' or ';
					}
					
					
				}
				$services.=')';
				$query->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
			}
			if($request->consultation_fee){
				$query->addFilterQuery(array('key'=>'consultation_fee', 'query'=>'consultation_fee:'.$specialties, 'tag'=>'exclude'));
			}
			if($request->gender){
				$query->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$specialties, 'tag'=>'exclude'));
			}
			if($request->address){
				$query->addFilterQuery(array('key'=>'address', 'query'=>'address:'.$specialties, 'tag'=>'exclude'));
			}
			if($request->city){
				$query->addFilterQuery(array('key'=>'city', 'query'=>'city:'.$specialties, 'tag'=>'exclude'));
			}
			if($request->country){
				$query->addFilterQuery(array('key'=>'country', 'query'=>'country:'.$specialties, 'tag'=>'exclude'));
			}
			$query->clearFields()->addFields('*');

			$facets = $query->getFacetSet();
			$facets->createFacetField(array('field'=>'speciality', 'exclude'=>'exclude'));
			$query->setStart(0);
			$query->setRows(5000);
			$resultset = $this->client->select($query);
			$resultset = $resultset->getData()['response']['docs'];

			$response = array('status'=>1, 'message'=>'success','data'=>$resultset); 
			$response = json_encode($response);
			return $response;
        } 
		catch (\Solarium\Exception $e) {
			$response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
			$response = json_encode($response);
			return $response;
        }
	}
	*/
	public function diagnostic(Request $request)
	{
		try {
            $query = $this->client->createSelect();
			$helper = $query->getHelper();
			$settings = \App\Model\Setting::where('title','radius')->get()->first();              
			$latitude =$request->input('latitude');
			$longitude=$request->input('longitude');
			## Applying filter
			$query->addFilterQuery(array('key'=>'role', 'query'=>'role:4', 'tag'=>'exclude'));
			if($request->diagnostic_tests){
				$tests = "(";
				$i=1;
				foreach($request->diagnostic_tests as $var){
					$tests.=$var['id'];
					if(count($request->diagnostic_tests)>$i++){
						$tests.=' or ';
					}										
				}
				$tests.=')';
				$query->addFilterQuery(array('key'=>'diagnostic_test', 'query'=>'test_id:'.$tests, 'tag'=>'exclude'));
			}
			if((int)$request->online_bookings==1){
				$query->addFilterQuery(array('key'=>'online_bookings', 'query'=>'online_bookings:1', 'tag'=>'exclude'));
			}elseif((int)$request->online_bookings==0){
				$query->addFilterQuery(array('key'=>'online_bookings', 'query'=>'online_bookings:0', 'tag'=>'exclude'));
			}			
			# Adding distance filter
			$latitude =$request->input('latitude');
			$longitude=$request->input('longitude');
			if($request->input('filters') && $request->input('filters')!=""){
				if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
					$rating = $request->input('filters')['rating'] -($request->input('filters')['rating']==0?0:1) ;
					$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
				}
				if(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']<16){
					$distance = $request->input('filters')['distance'];
					$query->createFilterQuery('distance')->setQuery(
						$helper->geofilt(
							'latlon', 
							doubleval($latitude),
							doubleval($longitude),
							doubleval($distance)
						)
					);
				}
				elseif(isset($request->input('filters')['distance']) && $request->input('filters')['distance']!="" && $request->input('filters')['distance']>15){
					$query->createFilterQuery('distance')->setQuery($helper->rangeQuery('distance', 16, 18024));
				}
				else{
					$query->createFilterQuery('distance')->setQuery(
						$helper->geofilt(
							'latlon', 
							doubleval($latitude),
							doubleval($longitude),
							doubleval($settings->value)
						)
					);
				}
				if(isset($request->input('filters')['online_reports']) && $request->input('filters')['online_reports']!=""){
					$online_reports = $request->input('filters')['online_reports'];
					$query->addFilterQuery(array('key'=>'online_reports', 'query'=>'online_reports:1', 'tag'=>'exclude'));
				}
				if(isset($request->input('filters')['home_collection_facility']) && $request->input('filters')['home_collection_facility']!=""){
					$home_collection_facility = $request->input('filters')['home_collection_facility'];
					$query->addFilterQuery(array('key'=>'home_collection_facility', 'query'=>'home_collection_facility:1', 'tag'=>'exclude')); 
				}
				if(isset($request->input('filters')['working_days']) && $request->input('filters')['working_days']!=""){				
					
				}
			}
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			);
			#Finished distance filter

			$facets = $query->getFacetSet();
			$facets->createFacetField(array('field'=>'diagnostic_test', 'exclude'=>'exclude'));
			$query->addSort('{!func}' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				),'asc');
			###### Paggination 
			$page = $request->input('page');
			if($request->input('limit')){
				$limit = $request->input('limit');
			}else{
				$limit = 50;
			}                     
			if(!is_numeric($page) or $page==0){
				$page = 1;
			}
			$start=($page - 1)*$limit;
			$query->setStart($start);
			$query->setRows($limit);
			## Paggination finished
			$resultset = $this->client->select($query);
			$resultset = $resultset->getData()['response']['docs'];
			if(!$resultset){
				$response = array('status'=>0, 'message'=>'No result found.'); 
				$response = json_encode($response);
				return $response;
			}
			$response = array('status'=>1, 'message'=>'success','data'=>$resultset); 
			$response = json_encode($response);
			return $response;
        } 
		catch (\Solarium\Exception $e) {
			$response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
			$response = json_encode($response);
			return $response;
        }
	}
	public function claimingSearch(Request $request)
	{
		try {
			$validator =Validator::make($request->all(), [
                'q' => 'required',
				'latitude' => 'required',	
				'longitude' => 'required'				
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return array('status'=>0, 'message'=>$error); 
            }
			$query = $this->client->createSelect();
			$helper = $query->getHelper();
			$query->setQuery('*'.trim($request->q).'*');
			$query->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
                
			$latitude =$request->input('latitude');
			$longitude=$request->input('longitude');
			
			## Adding distance field
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			);
			###### Paggination 
			$page = $request->input('page');
			if($request->input('limit')){
				$limit = $request->input('limit');
			}else{
				$limit = 50;
			}                     
			if(!is_numeric($page) or $page==0){
				$page = 1;
			}
			$start=($page - 1)*$limit;
			$query->setStart($start);
			$query->setRows($limit);
			
			$resultset = $this->client->select($query);
			$resultset = $resultset->getData()['response']['docs'];
			$i=0;
			foreach($resultset as $temp){ 
				$resultset[$i]['days_of_week'] = array(1,2,3,4);
				$resultset[$i++]['qualifications']= \App\Model\Doctor::getQualifications($temp['id']);
			}
			if(!$resultset){
				$response = array('status'=>0, 'message'=>'No result found.'); 
				$response = json_encode($response);
				return $response;
			}
			$response = array('status'=>1, 'message'=>'success','data'=>$resultset); 
			$response = json_encode($response);
			return $response;
		} 
		catch (\Solarium\Exception $e) {
			$response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
			$response = json_encode($response);
			return $response;
		}
		catch(\Illuminate\Database\QueryException $e){
				return array('status'=>0, 'message'=>$e->getMessage());
		}
	}
}
