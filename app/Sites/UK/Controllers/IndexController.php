<?php

namespace App\Sites\Uk\Controllers;
use App\Http\Controllers\Controller;

class IndexController extends Controller {
     public function index(){
         return view('uk::index.index');
     }
}
