<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Doctor extends Model
{
	protected $table = 'doctors';
	protected $guarded = [];
	public $timestamps = false;
	public static function boot() {
        parent::boot();

        static::creating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = is_null($value) ? " " : $value;
            }
			$model->updated_at = \Carbon\Carbon::today()->format('Y-m-d H:i:s');
			$title = $model->name." ". $model->locality;
			$name_slug = str_slug($title, "-");
			$model->name_slug = $name_slug;
        });
		static::updating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = is_null($value) ? " " : $value;
            }
			$model->updated_at = \Carbon\Carbon::today()->format('Y-m-d H:i:s');
			$title = $model->name." ". $model->locality;
			$name_slug = str_slug($title, "-");
			$model->name_slug = $name_slug;
        });
    }
	public function isAvailDoctor(){
		
	}
	public function claimedProfiles()
	{
		return $this->hasMany('App\Model\ClaimedProfile');
	}
	public function reviews()
	{
	    return $this->hasMany('App\Model\Review');
	}
	public function reviewDetails()
	{
	    return $this->hasMany('App\Model\ReviewDetail');
	}
	public function serviceDetails()
	{
	    return $this->hasMany('App\Model\ServiceDetail');
	}
	public function specialtyDetail()
	{
	    return $this->hasMany('App\Model\SpecialtyDetail');
	}
	// The way average rating is calculated (and stored) is by getting an average of all ratings, 
	// storing the calculated value in the rating_cache column (so that we don't have to do calculations later)
	// and incrementing the rating_count column by 1

    public function recalculateRating($rating)
    {
    	$reviews = $this->reviews()->notSpam()->approved();
	    $avgRating = $reviews->avg('rating');
		switch ($rating) {
            case 1:
                $this->rating_one_star_count = $reviews->where('rating',1)->count(); 
                break;
            case 2:
                $this->rating_two_star_count = $reviews->where('rating',2)->count();
                break;
            case 3:
                $this->rating_three_star_count = $reviews->where('rating',3)->count();
                break;
            case 4:
                 $this->rating_four_star_count = $reviews->where('rating',4)->count();
                break;
            case 5:
                $this->rating_five_star_count = $reviews->where('rating',5)->count();
                break;
        }
		$this->rating_cache = round($avgRating,1);
		$this->rating_count = $this->reviews()->notSpam()->approved()->count();
    	$this->save();
    }
	public static function doctorList($limit=null)
	{
		$q = DB::table('doctors');
		if($limit != ""){
		  $q->where('something', 'something');
		}	
		$doctors = $q->take(5)->get();  
		$i=0;
		foreach($doctors as $doctor)
		{
			#Getting Qualification
			$qualifications = DB::table('doctor_qualification')
				->join('qualifications', 'doctor_qualification.qualification_id', '=', 'qualifications.id')
				->where('doctor_qualification.doctor_id', '=', $doctor->id)
				->select('qualifications.id','qualifications.name')
				->get();
			$doctors[$i]->qualifications = $qualifications;
			
			#Getting Specialties
			$specialties = DB::table('specialty_details')
				->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
				->where('specialty_details.doctor_id', '=', $doctor->id)
				->select('specialties.id','specialties.name')
				->get();
			$doctors[$i]->specialties = $specialties;
			
			#Getting Services
			$services = DB::table('service_details')
				->join('service_type', 'service_details.service_id', '=', 'service_type.id')
				->where('service_details.doctor_id', '=', $doctor->id)
				->select('service_type.id','service_type.name')
				->get();
			$doctors[$i++]->services = $services;
		}	
		return $doctors;
	}
	public function hospitals()
    {
        return $this->belongsToMany('App\Model\Hospital');
    }
	public function diagnostics()
    {
        return $this->belongsToMany('App\Model\Diagnostic');
    }
	public static function userAssociatedProfile($userId){
        $doctor = array();
        if(Doctor::select('doctors.*')->where('user_id',$userId)->get()->count()){
            $doctor =Doctor::select('doctors.*')->where('user_id',$userId)->first();
            #Getting Qualification
            $qualifications = DB::table('doctor_qualification')
                    ->join('qualifications', 'doctor_qualification.qualification_id', '=', 'qualifications.id')
                    ->where('doctor_qualification.doctor_id', '=', $doctor->id)
                    ->select('qualifications.id','qualifications.name','doctor_qualification.college','doctor_qualification.year')
                    ->get();
            $doctor->qualifications = $qualifications;

            #Getting Specialties
            $specialties = DB::table('specialty_details')
                    ->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
                    ->where('specialty_details.doctor_id', '=', $doctor->id)
                    ->select('specialties.id','specialties.name')
                    ->get();
            $doctor->specialties = $specialties;

            #Getting Services
            $services = DB::table('service_details')
                    ->join('service_type', 'service_details.service_id', '=', 'service_type.id')
                    ->where('service_details.doctor_id', '=', $doctor->id)
                    ->select('service_type.id','service_type.name')
                    ->get();
            $doctor->services = $services;           
        }
        return $doctor;
    }
    public function qualifications()
    {
        return $this->belongsToMany('App\Model\Qualifications','doctor_qualification','doctor_id', 'qualification_id')
			->withPivot('college', 'year');;
    }
    public function colleges()
    {
        return $this->hasMany('App\Model\DoctorQualification');
    }
	public static function getQualifications($doctorId){
		$doctor = Doctor::find($doctorId);
		$qualifications = array();
		if($doctor && $doctor->qualifications()->get()->count()){
			foreach($doctor->qualifications as $qualification){ 
				$qualifications[] = ['id'=>$qualification->id,'name'=>$qualification->name,'college'=>is_null($qualification->pivot->college)?'':$qualification->pivot->college,'year'=>is_null($qualification->pivot->year)?'':$qualification->pivot->year] ;
			}
		}
		return $qualifications;
	}
	public function errorsReported()
	{
	    return $this->hasMany('App\Model\ErrorsReported');
	}
    public function services(){
        return $this->hasMany('App\Model\ServiceDetail');
    }
    public function serviceList(){
        return $this->belongsToMany('App\Model\Service','service_details');
    }
    public function specialties(){
        return $this->hasMany('App\Model\SpecialtyDetail');
    }
    public function  schedular(){
        return $this->hasMany('App\Model\WorkingDatetimes');
    }
    public static function getSchedules($doctor){
        ## Finding sunday schedules
		if(!$doctor->schedular()->get()->count())
			return $schedular=[];
        $sunShedules = $doctor->schedular()->where('week_day',1)->get();
        $schedular[0]['week_day'] = 1;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[0]['timings'] = $timings;

        ## Finding moday schedules
        $sunShedules = $doctor->schedular()->where('week_day',2)->get();
        $schedular[1]['week_day'] = 2;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[1]['timings'] = $timings;

        ## Finding tusday schedules
        $sunShedules = $doctor->schedular()->where('week_day',3)->get();
        $schedular[2]['week_day'] = 3;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[2]['timings'] = $timings;

        ## Finding wednesday schedules
        $sunShedules = $doctor->schedular()->where('week_day',4)->get();
        $schedular[3]['week_day'] = 4;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[3]['timings'] = $timings;

        ## Finding thrusday schedules
        $sunShedules = $doctor->schedular()->where('week_day',5)->get();
        $schedular[4]['week_day'] = 5;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[4]['timings'] = $timings;

        ## Finding friday schedules
        $sunShedules = $doctor->schedular()->where('week_day',6)->get();
        $schedular[5]['week_day'] = 6;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[5]['timings'] = $timings;

        ## Finding saturday schedules
        $sunShedules = $doctor->schedular()->where('week_day',7)->get();
        $schedular[6]['week_day'] = 7;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[6]['timings'] = $timings;
        return $schedular;
    }
}
