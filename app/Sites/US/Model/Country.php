<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'id','name'
    ];
	public $timestamps = false;
	public function isAvail($name)
	{
		if(Country::where('name', $name)->count() >0)
			return true;
		else
			return false;
	}
}
