<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceDetails extends Model
{
    protected $guarded = [];
	public $timestamps = false;
}
