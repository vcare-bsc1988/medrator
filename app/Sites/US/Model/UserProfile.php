<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
     protected $table = 'users_profile';
     public $primaryKey = 'id';
     public $timestamps = false;
     protected $guarded = [];
}
