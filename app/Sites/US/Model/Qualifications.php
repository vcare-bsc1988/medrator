<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Qualifications extends Model
{
    protected $guarded = [];
	public $timestamps = false;
	public function doctors()
    {
        return $this->belongsToMany('App\Model\Doctor','doctor_qualification','doctor_id', 'qualification_id');
    }
}
