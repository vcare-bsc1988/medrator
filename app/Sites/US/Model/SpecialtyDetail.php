<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpecialtyDetail extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $table = 'specialty_details';
    public function specialty(){
        return $this->belongsTo(\App\Model\Specialty::class,'speciality_id', 'id');
    }
}
