<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClaimedProfile extends Model
{
	public $timestamps = false;
	protected $guarded = [];
	public function doctor(){
	   return $this->belongsTo('App\\Model\Doctor');
	}
	public function hospital(){
	   return $this->belongsTo('App\Model\Hospital');
	}
	public function diagnostic(){
	   return $this->belongsTo('App\Model\Diagnostic');
	}
	public function user()
	{
		return $this->belongsTo('App\User');
	} 
}
