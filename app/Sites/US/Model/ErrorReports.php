<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ErrorReports extends Model
{
	protected $guarded = [];
	public $timestamps = false;
	public function doctors()
    {
        return $this->belongsToMany('App\Model\Doctor');
    }
	public function hospitas()
    {
        return $this->belongsToMany('App\Model\Hospital');
    }
	public function diagnostics()
    {
        return $this->belongsToMany('App\Model\Diagnoctic');
    }
}
