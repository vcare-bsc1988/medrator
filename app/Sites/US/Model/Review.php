<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use App\Model\Product;
use App\Model\Doctor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
class Review extends Model
{
	protected $guarded = [];
	public $timestamps = false;
	protected $helper;
	public function __construct($attributes = array())  {
        parent::__construct($attributes); // Eloquent
        $this->helper = new \App\Helpers\Helper;
    }
	public static function boot() {
        parent::boot();
        static::creating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = is_null($value) ? "" : $value;
            }
			$model->created_at = \Carbon\Carbon::now();
			$model->created_at = \Carbon\Carbon::now();
        });
		static::saving(function($model){  
			$model->created_at = \Carbon\Carbon::now();
			$model->updated_at = \Carbon\Carbon::now();
        });
    }
    // Validation rules for the ratings
    public function getCreateRules()
    {
        return array(
			'rating'=>'required|numeric|min:1',
            'comment'=>'required',
			'title'=>'required'
        );
    }

    // Relationships
	public function reviewDetail(){
		return $this->hasMany('App\Model\ReviewDetail');
	}
	public function helpfullVote(){
		return $this->hasMany('App\Model\HelpfullVote');
	}
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('Product');
    }
	public function doctor()
    {
        return $this->belongsTo('App\Model\Doctor');
    }
	public function hospital()
    {
        return $this->belongsTo('App\Model\Hospital');
    }
	public function diagnostic()
    {
        return $this->belongsTo('App\Model\Diagnostic');
    }

    // Scopes
    public function scopeApproved($query)
    {
       	return $query->where('approved', true);
    }

    public function scopeSpam($query)
    {
       	return $query->where('spam', true);
    }

    public function scopeNotSpam($query)
    {
       	return $query->where('spam', false);
    }

    // Attribute presenters
    public function getTimeagoAttribute()
    {
    	$date = \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
    	return $date;
    }

    // this function takes in product ID, comment and the rating and attaches the review to the product by its ID, then the average rating for the product is recalculated
    public function storeReviewForProduct($productID, $title, $comment, $rating)
    {
        
		$product = Product::find($productID);

        //$this->user_id = Auth::user()->id;
        $this->comment = $comment;
        $this->rating = $rating;
        $product->reviews()->save($this);

        // recalculate ratings for the specified product
        $product->recalculateRating($rating);
    }
	// this function takes in product ID, comment and the rating and attaches the review to the product by its ID, then the average rating for the product is recalculated
    public function storeReviewForDoctor($doctorID, $title, $comment, $rating, $post_anonymous, $userId)
    {
        $doctor = Doctor::find($doctorID);
        $this->user_id = $userId;
        $this->title = $title;
        $this->comment = $comment;
        $this->rating = $rating;
		$this->post_anonymous = $post_anonymous;
        $doctor->reviews()->save($this);
		
		$review = $doctor->reviews()->orderBy('id', 'desc')->first(); 
		/*foreach($ratingParams as $temp):
			\App\Model\ReviewDetail::create(['review_id'=>$review->id,'doctor_id'=>$doctorID,'review_parameter_id'=> $temp['id'],'rate' => $temp['rate']]);
		endforeach;*/
		
		// recalculate ratings for the specified product
        $doctor->recalculateRating($rating);
        $data = [
                'email' => $review->user->email,
                'user_name'=>$review->user->user->name,
                'entity_name'=>$review->doctor->name
        ];
        Mail::send('email.send_review',['data'=>$data], function ($message) use ($data) {
                $message->to($data['email'])->subject('New review');
        });
    }
    public function storeReviewForWebDoctor($input, $doctorID, $title, $comment, $rating,$ratings, $userId)
    {
        $doctor = Doctor::find($doctorID);
        $this->user_id = $userId;
        $this->title = $title;
        $this->comment = $comment;
        $this->rating = $rating;
        $this->disease_visited = $input['disease_visited'];
		$this->post_anonymous = $input['post_anonymous'];
        $doctor->reviews()->save($this);
		/** 
         * @Storing Review parameter
         */
        $reviewParams = \App\Model\ReviewParameter::where('entity','doctor-hospital')->get();
        $i=0;
        foreach($reviewParams as $var){		
            $ratingParams[]= array('id'=>$var->id,'name'=>$var->name,'rate'=>$ratings[$i++]);
        }
        $review = $doctor->reviews()->orderBy('id', 'desc')->first(); 	       
        foreach($ratingParams as $temp):
            \App\Model\ReviewDetail::create(['review_id'=>$review->id,'doctor_id'=>$doctorID,'review_parameter_id'=> $temp['id'],'rate' => $temp['rate']]);
        endforeach;
        // recalculate ratings for the specified product
        $doctor->recalculateRating($rating);
        ###### Sending notification
		$deviceToken = $review->user->device_token;
		$gcmApiKey = config('constants.gcm_apikey');
		$deviceType = $review->user->device_type;
		$name = $review->doctor->name;
		$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
		\App\Model\Notification::create([
			'title'=>config('message.review_notification_pendin_title'),
			'message'=>str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'receiver'=>$userId,
			'type'=>0,
			'status'=>0
		]);
		# Sending user notification
		$notifications= $notifications->send([
			'message' 	=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'subtitle'	=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'tickerText'	=> 'Medrator',
			'vibrate'	=> 1,
			'sound'		=> 1,
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		]);
		/**
		 * Sending admin notification.
		 */
		$this->helper->sendAdminNotification([
			'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
		]);
		/**
		 * Sending moderator notification.
		 */
		$this->helper->sendModeratorNotification([
			'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
		]);
		/**
		 * Sending user email
		 */
		$data = [
                'email' => $review->user->email,
                'user_name'=>$review->user->user->name,
                'entity_name'=>$review->doctor->name
        ];
        Mail::send('email.send_review',['data'=>$data], function ($message) use ($data) {
                $message->to($data['email'])->subject('New review');
        });
		/**
		 * Generate points
		 */
		\App\Model\Point::create([
			'user_id' => $review->user->id,
			'entity_id' => $review->id,
			'point_collected' => 100,
			'descriptioins' => 'Points collected for adding review.'
		]);
    }
    public function storeReviewForHospital($hospitalID, $title, $comment, $rating, $post_anonymous, $userId)
    {
		$hospital = Hospital::find($hospitalID);
		$this->user_id = $userId;
		$this->title = $title;
        $this->comment = $comment;
        $this->rating = $rating;
		$this->post_anonymous = $post_anonymous;
		
        $hospital->reviews()->save($this);
		$review = $hospital->reviews()->orderBy('id', 'desc')->first(); 	
		/*foreach($ratingParams as $temp):
			\App\Model\ReviewDetail::create(['review_id'=>$review->id,'hospital_id'=>$hospitalID,'review_parameter_id'=> $temp['id'],'rate' => $temp['rate']]);
		endforeach;*/
		// recalculate ratings for the specified product
        $hospital->recalculateRating($rating);
		$data = [
			'email' => $review->user->email,
			'user_name'=>$review->user->user->name,
			'entity_name'=>$review->hospital->name
		];
		Mail::send('email.send_review',['data'=>$data], function ($message) use ($data) {
			$message->to($data['email'])->subject('New review');
		});
    }
    
    public function storeReviewForWebHospital($input, $hospitalID, $title, $comment, $rating,$ratings, $userId)
    {
        $hospital = Hospital::find($hospitalID);
        $this->user_id = $userId;
        $this->title = $title;
        $this->comment = $comment;
        $this->rating = $rating;
        $this->disease_visited = $input['disease_visited'];
		$this->post_anonymous = $input['post_anonymous'];
        $hospital->reviews()->save($this);
        /** 
         * @Storing Review parameter
         */
        $reviewParams = \App\Model\ReviewParameter::where('entity','doctor-hospital')->get();
        $i=0;
        foreach($reviewParams as $var){		
            $ratingParams[]= array('id'=>$var->id,'name'=>$var->name,'rate'=>$ratings[$i++]);
        }
        $review = $hospital->reviews()->orderBy('id', 'desc')->first(); 	       
        foreach($ratingParams as $temp):
            \App\Model\ReviewDetail::create(['review_id'=>$review->id,'hospital_id'=>$hospitalID,'review_parameter_id'=> $temp['id'],'rate' => $temp['rate']]);
        endforeach;
        // recalculate ratings for the specified product
        $hospital->recalculateRating($rating);
        ###### Sending notification
		$deviceToken = $review->user->device_token;
		$gcmApiKey = config('constants.gcm_apikey');
		$deviceType = $review->user->device_type;
		$name = $review->hospital->name;
		$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
		\App\Model\Notification::create([
			'title'=>config('message.review_notification_pendin_title'),
			'message'=>str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'receiver'=>$userId,
			'type'=>0,
			'status'=>0
		]);
		# Sending user notification
		$notifications= $notifications->send([
			'message' 	=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'subtitle'	=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'tickerText'	=> 'Medrator',
			'vibrate'	=> 1,
			'sound'		=> 1,
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		]);
		/**
		 * Sending admin notification.
		 */
		$this->helper->sendAdminNotification([
			'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
		]);
		/**
		 * Sending moderator notification.
		 */
		$this->helper->sendModeratorNotification([
			'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
		]);
		/**
		 * Sending user email
		 */
		$data = [
                'email' => $review->user->email,
                'user_name'=>$review->user->user->name,
                'entity_name'=>$review->hospital->name
        ];
        Mail::send('email.send_review',['data'=>$data], function ($message) use ($data) {
                $message->to($data['email'])->subject('New review');
        });
		/**
		 * Generate points
		 */
		\App\Model\Point::create([
			'user_id' => $review->user->id,
			'entity_id' => $review->id,
			'point_collected' => 100,
			'descriptioins' => 'Points collected for adding review.'
		]);
    }
    public function storeReviewForDiagnostic($diagnosticID, $title,$comment, $rating, $post_anonymous, $userId)
    {
		$diagnostic = Diagnostic::find($diagnosticID);
		$this->user_id = $userId;
		$this->title = $title;
        $this->comment = $comment;
        $this->rating = $rating;
		$this->post_anonymous = $post_anonymous;
        $diagnostic->reviews()->save($this);
		$review = $diagnostic->reviews()->orderBy('id', 'desc')->first(); 
		/*foreach($ratingParams as $temp):
			\App\Model\ReviewDetail::create(['review_id'=>$review->id,'diagnostic_id'=>$diagnosticID,'review_parameter_id'=> $temp['id'],'rate' => $temp['rate']]);
		endforeach;*/
		// recalculate ratings for the specified product
        $diagnostic->recalculateRating($rating);
		$data = [
			'email' => $review->user->email,
			'user_name'=>$review->user->user->name,
			'entity_name'=>$review->diagnostic->name
		];
		Mail::send('email.send_review',['data'=>$data], function ($message) use ($data) {
			$message->to($data['email'])->subject('New review');
		});
    }
    public function storeReviewForWebDiagnostic($input, $diagnosticID, $title,$comment, $rating, $ratings, $userId)
    {
        $diagnostic = Diagnostic::find($diagnosticID);
        $this->user_id = $userId;
        $this->title = $title;
        $this->comment = $comment;
        $this->rating = $rating;
        $this->disease_visited = $input['disease_visited'];
		$this->post_anonymous = $input['post_anonymous'];
        $diagnostic->reviews()->save($this);
		/** 
         * @Storing Review parameter
         */
        $reviewParams = \App\Model\ReviewParameter::where('entity','diagnostic')->get();
        $i=0;
        foreach($reviewParams as $var){		
            $ratingParams[]= array('id'=>$var->id,'name'=>$var->name,'rate'=>$ratings[$i++]);
        }
        $review = $diagnostic->reviews()->orderBy('id', 'desc')->first(); 	       
        foreach($ratingParams as $temp):
            \App\Model\ReviewDetail::create(['review_id'=>$review->id,'diagnostic_id'=>$diagnosticID,'review_parameter_id'=> $temp['id'],'rate' => $temp['rate']]);
        endforeach;
        // recalculate ratings for the specified product
        $diagnostic->recalculateRating($rating);
        ###### Sending notification
		$deviceToken = $review->user->device_token;
		$gcmApiKey = config('constants.gcm_apikey');
		$deviceType = $review->user->device_type;
		$name = $review->diagnostic->name;
		$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
		\App\Model\Notification::create([
			'title'=>config('message.review_notification_pendin_title'),
			'message'=>str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'receiver'=>$userId,
			'type'=>0,
			'status'=>0
		]);
		# Sending user notification
		$notifications= $notifications->send([
			'message' 	=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'subtitle'	=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'tickerText'	=> 'Medrator',
			'vibrate'	=> 1,
			'sound'		=> 1,
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		]);
		/**
		 * Sending admin notification.
		 */
		$this->helper->sendAdminNotification([
			'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
		]);
		/**
		 * Sending moderator notification.
		 */
		$this->helper->sendModeratorNotification([
			'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
		]);
		/**
		 * Sending user email
		 */
		$data = [
            'email' => $review->user->email,
            'user_name'=>$review->user->user->name,
            'entity_name'=>$review->diagnostic->name
        ];
        Mail::send('email.send_review',['data'=>$data], function ($message) use ($data) {
            $message->to($data['email'])->subject('New review');
        });
		/**
		 * Generate points
		 */
		\App\Model\Point::create([
			'user_id' => $review->user->id,
			'entity_id' => $review->id,
			'point_collected' => 100,
			'descriptioins' => 'Points collected for adding review.'
		]);
    }
}