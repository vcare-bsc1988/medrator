<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Hospital extends Model
{
    protected $table = 'hospitals';
    protected $guarded = [];
	public static function boot() {
        parent::boot();

        static::creating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = is_null($value) ? " " : $value;
            }
			$model->updated_at = \Carbon\Carbon::today()->format('Y-m-d H:i:s');
			$title = $model->name." ". $model->locality;
			$name_slug = str_slug($title, "-");
			$model->name_slug = $name_slug;
        });
		static::updating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = is_null($value) ? " " : $value;
            }
			$model->updated_at = \Carbon\Carbon::today()->format('Y-m-d H:i:s');
			$title = $model->name." ". $model->locality;
			$name_slug = str_slug($title, "-");
			$model->name_slug = $name_slug;
        });
    }
	public function specialties()
	{
		return $this->hasMany('App\Model\SpecialtyDetail');
	}
	public function getHospitals()
	{
		
	}
	public function getHospital($hospitalId)
	{
		$hospital= DB::table('hospitals')
			->where('hospitals.id', '=', $hospitalId)
			->select('hospitals.*')
			->first();
		
		$specialties = DB::table('specialty_details')
			->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
			->where('specialty_details.hospital_id', '=', $hospital->id)
			->select('specialties.id','specialties.name')
			->get();
			
		$hospital->specialties = $specialties;
			
		#Getting Services
		$services = DB::table('service_details')
			->join('service_type', 'service_details.service_id', '=', 'service_type.id')
			->where('service_details.hospital_id', '=', $hospital->id)
			->select('service_type.id','service_type.name')
			->get();
		
		$hospital->services = $services;
		return $hospital;
	}
	public function getDoctorAssociateHospitals($doctorId){
		$hospitals= DB::table('doctor_associated')
			->join('hospitals', 'doctor_associated.hospital_id', '=', 'hospitals.id')
			->where('doctor_associated.hospital_id', '=', $doctorId)
			->select('hospitals.*')
			->get();
		$i=0; 		
		foreach($hospitals as $hospital){
			
			$specialties = DB::table('specialty_details')
				->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
				->where('specialty_details.hospital_id', '=', $hospital->id)
				->select('specialties.id','specialties.name')
				->get();
			
			$hospitals[$i]->specialties = $specialties;
			
			#Getting Services
			$services = DB::table('service_details')
				->join('service_type', 'service_details.service_id', '=', 'service_type.id')
				->where('service_details.hospital_id', '=', $hospital->id)
				->select('service_type.id','service_type.name')
				->get();
			$hospitals[$i]->services = $services;
		}
		return $hospitals;
	}
	public function claimedProfiles()
	{
		return $this->hasMany('App\Model\ClaimedProfile');
	}
	public function reviews()
	{
	    return $this->hasMany('App\Model\Review');
	}

	// The way average rating is calculated (and stored) is by getting an average of all ratings, 
	// storing the calculated value in the rating_cache column (so that we don't have to do calculations later)
	// and incrementing the rating_count column by 1

    public function recalculateRating($rating)
    {
    	$reviews = $this->reviews()->notSpam()->approved();
	    $avgRating = $reviews->avg('rating');
		switch ($rating) {
            case 1:
                $this->rating_one_star_count = $reviews->where('rating',1)->count(); 
                break;
            case 2:
                $this->rating_two_star_count = $reviews->where('rating',2)->count();
                break;
            case 3:
                $this->rating_three_star_count = $reviews->where('rating',3)->count();
                break;
            case 4:
                 $this->rating_four_star_count = $reviews->where('rating',4)->count();
                break;
            case 5:
                $this->rating_five_star_count = $reviews->where('rating',5)->count();
                break;
        }
		$this->rating_cache = round($avgRating,1);
		$this->rating_count = $this->reviews()->notSpam()->approved()->count();
    	$this->save();
    }
	public static function hospitalList()
	{
		$hospitals= DB::table('hospitals')->take(5)->get();
		$i=0;
		foreach($hospitals as $hospital){
			$specialties = DB::table('specialty_details')
				->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
				->where('specialty_details.hospital_id', '=', $hospital->id)
				->select('specialties.id','specialties.name')
				->get();
				
			$hospitals[$i]->specialties = $specialties;
				
			#Getting Services
			$services = DB::table('service_details')
				->join('service_type', 'service_details.service_id', '=', 'service_type.id')
				->where('service_details.hospital_id', '=', $hospital->id)
				->select('service_type.id','service_type.name')
				->get();
			
			$hospitals[$i++]->services = $services;
		}
		return $hospitals;
	}
    public function doctors()
    {
        return $this->belongsToMany('App\Model\Doctor');
    }
	public function errorsReported()
	{
	    return $this->hasMany('App\Model\ErrorsReported');
	}
    public function specialtyList(){
        return $this->belongsToMany('App\Model\Specialty','specialty_details','hospital_id','speciality_id');
    }
    public function  schedular(){
        return $this->hasMany('App\Model\WorkingDatetimes');
    }
	public static function getSchedules($hospital){
		if(!$hospital->schedular()->get()->count())
			return $schedular=[];
        ## Finding sunday schedules
        $sunShedules = $hospital->schedular()->where('week_day',1)->get();
        $schedular[0]['week_day'] = 1;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[0]['timings'] = $timings;

        ## Finding moday schedules
        $sunShedules = $hospital->schedular()->where('week_day',2)->get();
        $schedular[1]['week_day'] = 2;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[1]['timings'] = $timings;

        ## Finding tusday schedules
        $sunShedules = $hospital->schedular()->where('week_day',3)->get();
        $schedular[2]['week_day'] = 3;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[2]['timings'] = $timings;

        ## Finding wednesday schedules
        $sunShedules = $hospital->schedular()->where('week_day',4)->get();
        $schedular[3]['week_day'] = 4;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[3]['timings'] = $timings;

        ## Finding thrusday schedules
        $sunShedules = $hospital->schedular()->where('week_day',5)->get();
        $schedular[4]['week_day'] = 5;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[4]['timings'] = $timings;

        ## Finding friday schedules
        $sunShedules = $hospital->schedular()->where('week_day',6)->get();
        $schedular[5]['week_day'] = 6;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[5]['timings'] = $timings;

        ## Finding saturday schedules
        $sunShedules = $hospital->schedular()->where('week_day',7)->get();
        $schedular[6]['week_day'] = 7;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[6]['timings'] = $timings;
        return $schedular;
    }
    public function workingTimes(){
        return $this->hasMany('App\Model\WorkingDatetimes');
    }
    public function galleries()
    {
        return $this->hasMany('App\Model\Gallery');
    }
    public function services(){
        return $this->hasMany('App\Model\ServiceDetail');
    }
}
