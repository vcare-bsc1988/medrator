<?php

namespace App\Http\Controllers\Specialist;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
   public function index()
   {
       return view('us::us::specialist.home.index');
   }
}
