<?php

namespace App\Sites\US\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
class StateController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if($request->input('country_id')){         
                $results = \App\Model\States::where('country_id',$request->input('country_id'))->get();
                $response = '<option value="">Select State</option>';
                foreach($results as $state):
                    $response .='<option value="'.$state["id"].'">'.$state["name"].'</option>';
                endforeach;
                return $response;
            }
            return false;
        }
        return false;
    }
}
