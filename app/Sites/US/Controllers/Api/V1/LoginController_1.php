<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\Model\Doctor;
use DB;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       if($request->input('email') && $request->input('password') && $request->input('device_token') && $request->device_type)
	   {
			if(Auth::attempt(array('email' => $request->email, 'password' => $request->password))){
				$user = User::select('login.*','up.*','role_user.role_id as role')
						->join('role_user', 'login.id', '=', 'role_user.user_id')
						->join('users as up', 'login.id', '=', 'up.user_id')
						->where('email', $request->input('email'))->first();
				if (!$user) {
					$response = array("status"=>0,"message"=>"Login failed please try latter.");
					return $response;
				}		
				$user->device_type = $request->device_type;
                $user->device_token = $request->device_token;
				$user->save();
				
				if (!$user->activated) {
					$response = array("status"=>0,"message"=>"You need to confirm your account. We have sent you an activation code, please check your email.");
					return $response;
				}
				if($user->activated ==2){
					$response = array("status"=>0,"message"=>"Your account has been disabled. Please contact to admin.");
					return $response;
				}
				$associated_doctor_profile="";
				$doctor =Doctor::select('doctors.*')
					->where('user_id',$user->user_id)->first();
				if($doctor):
				#Getting Qualification
				$qualifications = DB::table('doctor_qualification')
					->join('qualifications', 'doctor_qualification.qualification_id', '=', 'qualifications.id')
					->where('doctor_qualification.doctor_id', '=', $doctor->id)
					->select('qualifications.id','qualifications.name','doctor_qualification.college','doctor_qualification.year')
					->get();
				$doctor->qualifications = $qualifications;
				
				#Getting Specialties
				$specialties = DB::table('specialty_details')
					->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
					->where('specialty_details.doctor_id', '=', $doctor->id)
					->select('specialties.id','specialties.name')
					->get();
				$doctor->specialties = $specialties;
				
				#Getting Services
				$services = DB::table('service_details')
					->join('service_type', 'service_details.service_id', '=', 'service_type.id')
					->where('service_details.doctor_id', '=', $doctor->id)
					->select('service_type.id','service_type.name')
					->get();
				$doctor->services = $services;
				$associated_doctor_profile=$doctor;
				endif;
				$has_doctor_profile=0;
				if(count($doctor)>0)
					$has_doctor_profile=1;
				
				$result = array(
					"basic_profile" =>$user,
					"has_associate_doctor_profile"=>$has_doctor_profile,
					"associate_doctor_profile"=>$associated_doctor_profile
				);
				$response = array("status"=>1,"message"=>"success!", "data"=>$result);
			}else{
				$response = array("status"=>0,"message"=>"Invalid credential !");
			}
	   }
	   else{
		   $response = array("status"=>0,"message"=>"Invalid request !");
	   }
	   return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return array("status"=>1,"message"=>"success");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return array("status"=>1,"message"=>"store");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return array("status"=>1,"message"=>"success");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         return array("status"=>1,"message"=>"success");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         return array("status"=>1,"message"=>"success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         return array("status"=>1,"message"=>"success");
    }
}
