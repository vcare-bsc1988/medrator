<?php
namespace App\Http\Controllers\Api\V1;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TestingController
 *
 * @author VCareAll
 */
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\SpecialtyDetails;
use App\Model\Specialties;
use App\Model\Qualifications;
use DB;
use App\Model\Service;
use App\Model\Test;
use Illuminate\Support\Collection;
class TestingController extends Controller{
    public function pushNotification(Request $request){
        try{
            $validator =Validator::make($request->all(), [
                    'device_token' => 'required',
            ]);
            if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    return array('status'=>0, 'message'=>$error); 
            }
            $deviceToken = $request->input('device_token');
            $deviceType = $request->input('device_type');
            $notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey="AIzaSyBosWG5b46RZV6gi1vYQQm_Xgfdh5zXXXw",$deviceType);
			$message = array
            (
				'message' 	=> 'Hello Himanshu, This is test notifications',
				'title'		=> 'Hello Himanshu, This is test notifications',
				'subtitle'	=> 'Hello Himanshu, This is test notifications',
				'tickerText'	=> 'Testing',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
            );
			$notifications= $notifications->send($message);
			if($notifications){
				$response = array('status'=>1, 'message'=>'success');
			}else{
				$response = array('status'=>0, 'message'=>'Failed');
			}           
        }
        catch(\Illuminate\Database\QueryException $e){
                $response=  array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
    }
}
