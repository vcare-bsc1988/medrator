<?php

namespace App\Http\Controllers\Api\Beta;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\SpecialtyDetails;
use App\Model\Specialties;
use App\Model\Qualifications;
use DB;
use App\Model\Service;
use App\Model\Test;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
class ServiceController extends Controller
{
    protected $client;
	protected $helper;
    public function __construct(\Solarium\Client $client)
    {
        $this->client = $client;
		$this->helper = new \App\Helpers\Helper;
    }
	public function index()
    {
        //
    }
	public function countries()
	{
		$countries = Country::all();
		$response = array('status'=>1, 'message'=>'success','data'=>$countries); 
		$response = json_encode($response);
		return $response; 
	}
	public function cities(Request $request)
	{
		if(!$request->country_id && !$request->country_name){
			$response = array('status'=>0, 'message'=>'Invalid request'); 
			$response = json_encode($response);
			return $response;
		}
		
		$query =Country::select('cities.id','cities.name')->join('states','countries.id','=','states.country_id')
				->join('cities','states.id','=','cities.state_id');
		if($request->country_id)
			$query =$query->where('countries.id',$request->country_id);
		if($request->country_name){
			$query =$query->where('countries.name',$request->country_name);
		}
		$result = $query->orderBy('cities.name', 'asc')
				->get();
		if(empty($result)){
			$response = array('status'=>0, 'message'=>'No data found !'); 
			$response = json_encode($response);
			return $response;
		}
		
		$response = array('status'=>1, 'message'=>'success','data'=>$result); 
		$response = json_encode($response);
		return $response;
	}
	public function specialties(Request $request)
	{
		try {
            # Get top 5 speialties
            $popular = Specialties::select('id','name')->skip(0)->orderBy('name', 'asc')->take(5)->get();
            # Get after top 5 specialties
            $other = Specialties::select('id','name')->orderBy('name', 'asc')->skip(5)->take(100)->get();
            $entities = compact("popular", "other");
            if(!$entities){
                $response = array('status'=>0, 'message'=>'No data found.'); 
                return $response;
            }
        }catch(\Illuminate\Database\QueryException $e){
                return array('status'=>0, 'message'=>$e->getMessage());
        }
        $response = array('status'=>1, 'message'=>'success.','data'=>$entities); 	
        return $response;
	}
	public function qualifications()
    {
        $qualification = Qualifications::all();
        $response = array('status'=>1, 'message'=>'success','data'=>$qualification); 
		$response = json_encode($response);
        return $response;
    }
	public function service_list()
	{
		try {
            # Get top 5 speialties
            $popular = Service::select('id','name')->orderBy('name', 'asc')->skip(0)->take(5)->get();
            # Get after top 5 specialties
            $other = Service::select('id','name')->orderBy('name', 'asc')->skip(5)->take(100)->get();
            $entities = compact("popular", "other");
            if(!$entities){
                $response = array('status'=>0, 'message'=>'No data found.'); 
                return $response;
            }
        }catch(\Illuminate\Database\QueryException $e){
                return array('status'=>0, 'message'=>$e->getMessage());
        }
        $response = array('status'=>1, 'message'=>'success.','data'=>$entities); 	
        return $response;  
	}
    public function test(Request $request)
    {
       $input = $request->all();
	   return json_encode($input);
    }
	public function diagnosticTests()
	{
		try {
            # Get top 5 speialties
            $popular = Test::select('id','name')->orderBy('name', 'asc')->skip(0)->take(5)->get();
            # Get after top 5 specialties
            $other = Test::select('id','name')->orderBy('name', 'asc')->skip(5)->take(100)->get();
            $entities = compact("popular", "other");
            if(!$entities){
                $response = array('status'=>0, 'message'=>'No data found.'); 
                return $response;
            }
        }catch(\Illuminate\Database\QueryException $e){
                return array('status'=>0, 'message'=>$e->getMessage());
        }
        $response = array('status'=>1, 'message'=>'success.','data'=>$entities); 	
        return $response;      
	}
	public function ratingReview(Request $request)
	{
		
		$collection = collect($request->input('ratings'));
		$input = array(
			'title' => $request->input('review_title'),
			'comment' => $request->input('review_description'),
			'rating'  => $request->input('rating'),   
			'post_anonymous'  => $request->input('post_anonymous'),
		);
		# @instantiate Rating model
		$review = new \App\Model\Review;
		# @Validate that the user's input corresponds to the rules specified in the review model
		if($request->input('role') == 2){
			$entity = 'doctors';
			$profile = "doctor";
		}elseif($request->input('role') == 3){
			$entity = 'login';
		}elseif($request->input('role') == 4){
			$entity = 'diagnostics';
			$profile = "diagnostic";
		}elseif($request->input('role') == 5){
			$entity = 'hospitals';
			$profile = "hospital";
		}else{ 
			$response = array('status'=>0, 'message'=>'Invalid role.');
			return $response;
		}
		$validator = Validator::make($request->all(), 
		[
			'user_id'=>'required|exists:login,id',
			'profile_id'=>'required|'.'exists:'.$entity.',id',
			'rating'=>'required',
			'review_title'=>'required',
			'review_description'=>'required',
			//'visited_for'=>'required',
			'role'=>'required',
			'timestamp'=>'required'
		]);
		# @If input passes validation - store the review in DB, otherwise return to product page with error message 
		if (!$validator->passes()) {
			$response = array('status'=>0, 'message'=>$validator->errors()->first()); 
			return $response;
		}
		$isSubmitedReview = \App\Model\Review::where('user_id', $request->input('user_id'))->where(function ($query) use ($request) {
				$query->where('doctor_id', $request->input('profile_id'))
				->orWhere('hospital_id', $request->input('profile_id'))
				->orWhere('diagnostic_id', $request->input('profile_id'));
			})->count();
		if($isSubmitedReview){
			$review = \App\Model\Review::where('user_id', $request->input('user_id'))->where(function ($query) use ($request) {
				$query->where('doctor_id', $request->input('profile_id'))
				->orWhere('hospital_id', $request->input('profile_id'))
				->orWhere('diagnostic_id', $request->input('profile_id'));
			})->orderBy('id','desc')->get()->first();
			$days =  \Carbon\Carbon::createFromTimeStamp(strtotime($review->created_at))->diffInDays();
			if($days < config('constants.review_cycle')){
				$response = array('status'=>0, 'message'=>'You can review after '.config('constants.review_cycle').' days'); 	
				$response = json_encode($response);
				return $response;
			}
		}                
		switch($request->input('role')){
		case 2:
			$doctor = \App\Model\Doctor::where('user_id', $request->input('user_id'))->get()->first();
			if($doctor && $doctor->id == $request->input('profile_id')){
				$response = array('status'=>0, 'message'=>'Can not review yourself.'); 
				return $response;
			}else{
				$review->storeReviewForDoctor(
					$request->input('profile_id'),
					$input['title'], 				
					$input['comment'], 
					$input['rating'],
					$input['post_anonymous'],
					$request->input('user_id')
				);
				$name = $review->doctor->name;
				$response = array('status'=>1, 'message'=>'Your review has been submitted for approval.'); 
			}
			break;
		case 4:
			$diagnostic = \App\Model\Diagnostic::where('user_id', $request->input('user_id'))->get()->first();
			if($diagnostic && $diagnostic->iid == $request->input('profile_id')){
				$response = array('status'=>0, 'message'=>'Can not review yourself.'); 
				return $response;
			}else{
				$review->storeReviewForDiagnostic(
					$request->input('profile_id'), 
					$input['title'], 
					$input['comment'], 
					$input['rating'],
					$input['post_anonymous'],
					$request->input('user_id')
				);
				$name = $review->diagnostic->name;
				$response = array('status'=>1, 'message'=>'Your review has been submitted for aproval.'); 
			}
			break;
		case 5:
			$hospital = \App\Model\Hospital::where('user_id', $request->input('user_id'))->get()->first();
			if($hospital &&  $hospital->id== $request->input('profile_id')){
				$response = array('status'=>0, 'message'=>'Can not review yourself.'); 
				return $response;
			}else{
				$review->storeReviewForHospital(
					$request->input('profile_id'),
					$input['title'], 	
					$input['comment'], 
					$input['rating'],
					$input['post_anonymous'],
					$request->input('user_id')
				);
				$name = $review->hospital->name;
				$response = array('status'=>1, 'message'=>'Your review has been submitted for aproval.'); 
			}
			break;
		default:
			$response = array('status'=>0, 'message'=>'Oops, something went wrong. Please try again later.'); 
			break;
		} 
		###### Sending notification
		$deviceToken = $review->user->device_token;
		$gcmApiKey = config('constants.gcm_apikey');
		$deviceType = $review->user->device_type;
		$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
		\App\Model\Notification::create([
			'title'=>config('message.review_notification_pendin_title'),
			'message'=>str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'receiver'=>$request->input('user_id'),
			'type'=>0,
			'status'=>0
		]);
		# Sending user notification
		$notifications= $notifications->send([
			'message' 	=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'subtitle'	=> str_replace('[NAME]',$name,config('message.review_notification_pendin_message')),
			'tickerText'	=> 'Medrator',
			'vibrate'	=> 1,
			'sound'		=> 1,
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon'
		]);
		/**
		 * Sending admin notification.
		 */
		$this->helper->sendAdminNotification([
			'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
		]);
		/**
		 * Sending moderator notification.
		 */
		$this->helper->sendModeratorNotification([
			'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
			'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_pendin_message')),
		]);
		## Sending email to admin
		$data = [
			'email' => $review->user->email,
			'user_name'=>$review->user->user->name,
			'entity_name'=>$name
		];
		Mail::send('email.send_review',['data'=>$data], function ($message) use ($data) {
			$message->to($data['email'])->subject('New review');
		});
		/**
		 * Sending user email
		 */
		$data = [
                'email' => $review->user->email,
                'user_name'=>$review->user->user->name,
                'entity_name'=>$name
        ];
        Mail::send('email.send_review',['data'=>$data], function ($message) use ($data) {
                $message->to($data['email'])->subject('New review');
        });
		/**
		 * Generate points
		 */
		\App\Model\Point::create([
			'user_id' => $review->user->id,
			'entity_id' => $review->id,
			'point_collected' => 100,
			'descriptioins' => 'Points collected for adding review.'
		]);
		$response = json_encode($response);
		return $response;		
	}
	public function reviewHelpfullVote(Request $request)
	{
		try {
			$helpfullVote = new \App\Model\HelpfullVote(['user_id'=>$request->input('user_id'),'vote'=>$request->input('voting')]);	
			$validator = Validator::make($request->all(), $helpfullVote->getCreateVotingRules());
			if (!$validator->passes()) {
					$response = array('status'=>0, 'message'=>$validator->errors()->first()); 
					return $response;
			}
			if(\App\Model\HelpfullVote::where(array('user_id'=>$request->input('user_id'),'review_id'=>$request->input('review_id')))->count()){
				$status = \App\Model\HelpfullVote::where(array('user_id'=>$request->input('user_id'),'review_id'=>$request->input('review_id')))->first()->vote;
				switch ($status) {
					case 0:
						$response = array('status'=>0, 'message'=>'Already submitted thumbs down.');
						break;
					case 1:
						$response = array('status'=>0, 'message'=>'Already submitted thumbs up.');
						break;					
				}
				return  $response;
			}
			$review = \App\Model\Review::find($request->input('review_id'));
			$helpfullVote->review()->associate($review);
			$helpfullVote->save();
		}catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
		}
		$response = array('status'=>1, 'message'=>'Thank you for your feedback.'); 	
		$response = json_encode($response);
		return $response;		
	}
	public function reviewsList(Request $request)
	{
		switch($request->input('role')){
		case 2:
			$reviews = \App\Model\Review::select('reviews.id','reviews.user_id','name','image','rating','comment','approved','reviews.created_at')->join('users','reviews.user_id','=','users.user_id')->where('doctor_id',$request->input('id'))->get();
			if(!$reviews->count()){
				$response = array('status'=>0, 'message'=>'No review was found.'); 
				return $response;
			}
			$i=0;
			foreach($reviews as $review){
				$reviewDetails = \App\Model\ReviewDetail::select('rp.id','rp.name','rate')->join('review_parameters as rp','review_details.review_parameter_id','=','rp.id')
						->where('review_details.review_id',$review->id)->get();
				$reviews[$i]['helpfull_count']=\App\Model\HelpfullVote::where(array('review_id'=>$review->id, 'vote'=>1))->count();
				$reviews[$i]['user_voting_status']=(\App\Model\HelpfullVote::where(array('user_id'=>$request->input('user_id'),'review_id'=>$review->id))->count()>0?1:0);
				$reviews[$i++]['ratings']=$reviewDetails;
			}
			$response = array('status'=>1, 'message'=>'Sueccess.', 'data'=>$reviews); 
			break;
		case 4:
			$reviews = \App\Model\Review::select('reviews.id','reviews.user_id','name','image','rating','comment','approved','reviews.created_at')->join('users','reviews.user_id','=','users.user_id')->where('diagnostic_id',$request->input('id'))->get();
			if(!$reviews->count()){
				$response = array('status'=>0, 'message'=>'No review was found.'); 
				return $response;
			}
			$i=0;
			foreach($reviews as $review){
				$reviewDetails = \App\Model\ReviewDetail::select('rp.id','rp.name','rate')->join('review_parameters as rp','review_details.review_parameter_id','=','rp.id')
						->where('review_details.review_id',$review->id)->get();
				$reviews[$i]['helpfull_count']=\App\Model\HelpfullVote::where(array('review_id'=>$review->id, 'vote'=>1))->count();
				$reviews[$i]['user_voting_status']=(\App\Model\HelpfullVote::where(array('user_id'=>$request->input('user_id'),'review_id'=>$review->id))->count()>0?1:0);
				$reviews[$i++]['ratings']=$reviewDetails;
			}
			$response = array('status'=>1, 'message'=>'Sueccess.', 'data'=>$reviews); 
			break;
		case 5:
			$reviews = \App\Model\Review::select('reviews.id','reviews.user_id','name','image','rating','comment','approved','reviews.created_at')->join('users','reviews.user_id','=','users.user_id')->where('hospital_id',$request->input('id'))->get();
			if(!$reviews->count()){
				$response = array('status'=>0, 'message'=>'No review was found.'); 
				return $response;
			}
			$i=0;
			foreach($reviews as $review){
				$reviewDetails = \App\Model\ReviewDetail::select('rp.id','rp.name','rate')->join('review_parameters as rp','review_details.review_parameter_id','=','rp.id')
						->where('review_details.review_id',$review->id)->get();
				$reviews[$i]['helpfull_count']=\App\Model\HelpfullVote::where(array('review_id'=>$review->id, 'vote'=>1))->count();
				$reviews[$i]['user_voting_status']=(\App\Model\HelpfullVote::where(array('user_id'=>$request->input('user_id'),'review_id'=>$review->id))->count()>0?1:0);
				$reviews[$i++]['ratings']=$reviewDetails;
			}
			$response = array('status'=>1, 'message'=>'Sueccess.', 'data'=>$reviews); 
			break;
		default:
			$response = array('status'=>0, 'message'=>'Oops, something went wrong. Please try again later.'); 
			break;
		}
		return $response;	
	}
	public function entityList(Request $request)
	{	
			
		try {
			/**
			$validator = Validator::make($request->all(), [
				'latitude'=>'required',
				'longitude'=>'required',
			]);
			if (!$validator->passes()) {
				$response = array('status'=>0, 'message'=>$validator->errors()->first()); 
				return $response;
			}
			*/
			$latitude  = $request->input('latitude')?$request->input('latitude'):0;
			$longitude= $request->input('longitude')?$request->input('longitude'):0;
			
			$query = $this->client->createSelect();
			$helper = $query->getHelper();
			$facets = $query->getFacetSet();
			
			$query->addFilterQuery(array('key'=>'entity', 'query'=>'entity:doctor or hospital or diagnostic', 'tag'=>'exclude'));
			
			$facets->createFacetField(array('field'=>'speciality', 'exclude'=>'exclude'));
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			);
			#finding overall top 10 combine
			$result = $this->client->select($query
				->setStart(0)
				->setRows(10))
				->getData()['response']['docs'];
			$combine = [];
			for($i=0; $i < count($result); $i++){
				$combine[$i] = $result[$i];
				$combine[$i]['days_of_week'] = array(1,2,3,4);  
				if(isset($result[$i]['role']) && $result[$i]['role']==2){
					$combine[$i]['qualifications'] = \App\Model\Doctor::getQualifications($result[$i]['id']);			 
				}
			}
			#finding overall top 10 doctor
			$query->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
			$result = $this->client->select($query
				->setStart(0)
				->setRows(10))
				->getData()['response']['docs'];
			
			$doctors = [];
			for($i=0; $i < count($result); $i++){
				$doctors[$i] = $result[$i];
				$doctors[$i]['days_of_week'] = array(1,2,3,4);
				$doctors[$i]['qualifications'] = \App\Model\Doctor::getQualifications($result[$i]['id']);
			}
			#Finding top 10 location based doctor.
			$queryDoc = $this->client->createSelect();
			$helperDoc = $queryDoc->getHelper();
			$facets = $queryDoc->getFacetSet();
			$settings = \App\Model\Setting::where('title','radius')->get()->first();              
            $queryDoc ->createFilterQuery('distance')->setQuery(
				$helperDoc->geofilt(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude),
					doubleval($settings->value)
				)
			);
			/**
			 * filter best rated doctor.
			 */
			$request->request->add([
				'filters' =>['sorting'=>'ratings']
			]);
			$queryDoc = $this->helper->solriumSort($queryDoc, $request,$latitude,$longitude);
			
			$resultDoc = $this->client->select($queryDoc
				->setStart(0)
				->setRows(10))
				->getData()['response']['docs'];
			$locDoctors=[];
			for($i=0; $i < count($resultDoc); $i++){
				$locDoctors[$i] = $resultDoc[$i];
				$locDoctors[$i]['days_of_week'] = array(1,2,3,4);
				$locDoctors[$i]['qualifications'] = \App\Model\Doctor::getQualifications($result[$i]['id']);
			} 
			switch (count($locDoctors)) {
				case 0:
					$homeDoctorList = [];
					for($i=0; ($i < 10 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				case 1:
					$homeDoctorList = [];
					for($i=0; ($i < 9 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				case 2:
					$homeDoctorList = [];
					for($i=0; ($i < 8 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				case 3:
					$homeDoctorList = [];
					for($i=0; ($i < 7 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				case 4:
					$homeDoctorList = [];
					for($i=0; ($i < 6 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				case 5:
					$homeDoctorList = [];
					for($i=0; ($i < 5 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				case 6:
					$homeDoctorList = [];
					for($i=0; ($i < 4 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				case 7:
					$homeDoctorList = [];
					for($i=0; ($i < 3 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				case 8:
					$homeDoctorList = [];
					for($i=0; ($i < 2 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				case 9:
					$homeDoctorList = [];
					for($i=0; ($i < 1 && $i<count($doctors)); $i++){
						$homeDoctorList[$i] = $doctors[$i];
						$homeDoctorList[$i]['days_of_week'] = array(1,2,3,4);
						$homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
					}   
					$homeDoctorList = array_merge($locDoctors,$homeDoctorList);
					break;
				default:
					$homeDoctorList = $locDoctors;
			}
			/***
			* @ Finding usert interes related blogs
			*/
			if($request->input('user_id')){
				$tags =  \App\User::find(trim($request->input('user_id')))->interests()->get();
				if ($tags->count()>0) {
					$tag_ids = array();
					foreach($tags as $individual_tag) 
						$tag_ids[] = $individual_tag->interest_id;           
					$args=array(
						'tag__in' => $tag_ids,
						'posts_per_page'=>4, // Number of related posts to display.
						'caller_get_posts'=>1
					);
					$posts = get_posts($args);  
				}else{
					$args=array(
						'posts_per_page'=>4, // Number of related posts to display.
						'caller_get_posts'=>1
					);
					$posts = get_posts($args);  
				}
				
			}else{
				$args=array(
					'posts_per_page'=>4, // Number of related posts to display.
					'caller_get_posts'=>1
				);
				$posts = get_posts($args);  
			}
			$blogs	=[];
			if(count($posts)>0)	{		
				$i=0;
				foreach($posts as $post):
					$images = [];
					$mediaImages = get_attached_media( 'image', $post->ID);
					foreach($mediaImages as $image){
						$images[]['img_url'] = $image->guid;
					}
					//return get_the_content();
					$posts[$i]->images= $images;
					$content = strip_tags($posts[$i]->post_content);
					$content= preg_replace('/[ ]{2,}|[\r\n]/', ' ', trim($content));
					/*$content = preg_replace("/<img[^>]+\>/i", "(image) ", $posts[$i]->post_content); 		
					$content = preg_replace("/<iframe[^>]+\>/i", "(video) ", $content);             
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]>', $content);
					return $content;*/
					$posts[$i]->post_content = $content;
					$i++;
				endforeach;
				$blogs =$posts;
			}
			
			$entities = compact("homeDoctorList", "combine","blogs");
			if(!$entities){
				$response = array('status'=>1, 'message'=>'No data found.'); 
				return $response;
			}
		}catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
		}
		$response = array('status'=>1, 'message'=>'success.','data'=>$entities); 	
		return $response;				
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function groupwiseSpecialties(Request $request)
    {
        
        try {
            # Get top 5 speialties
            $popular = Specialties::select('id','name')->orderBy('name', 'asc')->skip(0)->take(5)->get();
            # Get after top 5 specialties
            $other = Specialties::select('id','name')->orderBy('name', 'asc')->skip(5)->take(100)->get();
            $entities = compact("popular", "other");
            if(!$entities){
                $response = array('status'=>1, 'message'=>'No data found.'); 
                return $response;
            }
        }catch(\Illuminate\Database\QueryException $e){
                return array('status'=>0, 'message'=>$e->getMessage());
        }
        $response = array('status'=>1, 'message'=>'success.','data'=>$entities); 	
        return $response;	
    }
	public function entitityItems(Request $request)
	{
		try {
            # Get speialties
            $specialties = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
            # Get Services
			/**
			@Deprecated
			$services = Service::select('id','name')->orderBy('name', 'asc')->get();
			*/
			# Get diagnostic tests
			$tests = Test::select('id','name')->orderBy('is_top', 'desc')->get();            
			$entities = compact("specialties","tests");
            if(!$entities){
                $response = array('status'=>0, 'message'=>'No data found.'); 
                return $response;
            }
			if(!$entities){
                $response = array('status'=>0, 'message'=>'No data found.'); 
                return $response;
            }
        }catch(\Illuminate\Database\QueryException $e){
                return array('status'=>0, 'message'=>$e->getMessage());
        }
        $response = array('status'=>1, 'message'=>'success.','data'=>$entities); 	
        return $response;
	}
	public function doctorDescription(Request $request){
		
		$validator = Validator::make($request->all(), 
		[
			'id'=>'required|exists:doctors,id',
			'latitude'=>'required',
			'longitude'=>'required',
		]);
		# @If input passes validation - store the review in DB, otherwise return to product page with error message 
		if (!$validator->passes()) {
				$response = array('status'=>0, 'message'=>$validator->errors()->first()); 
				return $response;
		}
		# Finding associated hospital
		$associated_hospitals = [];
		$doctor = \App\Model\Doctor::find($request->input('id'));
		$hospitals= $doctor->hospitals;
		if($doctor->hospitals()->get()->count()>0){
			$ids = "(";
			$i=1;
			foreach($hospitals as $var){
				$ids.=$var->id;
				if(count($hospitals)>$i++){
						$ids.=' or ';
				}
			}
			$ids.=')';
			$assoHospQuery = $this->client->createSelect();
			$assoHospHelper = $assoHospQuery->getHelper();
			# Adding distance field
			$assoHospQuery->addField('distance:' . $assoHospHelper->geodist(
					'latlon', 
					doubleval(trim($request->input('latitude'))), 
					doubleval(trim($request->input('longitude')))
				)
			); 
			$assoHospQuery->addFilterQuery(array('key'=>'role', 'query'=>'role:5', 'tag'=>'exclude'));
			$assoHospQuery->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$ids, 'tag'=>'exclude'));
			$assoHospResultset = $this->client->select($assoHospQuery);				
			$assoHospResultset = $assoHospResultset->getData()['response'];
			$associated_hospitals = $assoHospResultset['docs'];		
		}
		# Finding rating & review details
		$query = $this->client->createSelect();
		$helper = $query->getHelper();
		$query->addFilterQuery(array('key'=>'entity', 'query'=>'entity:review', 'tag'=>'exclude'));
		$query->addFilterQuery(array('key'=>'doctor_id', 'query'=>'doctor_id:'.$request->input('id'), 'tag'=>'exclude'));
		$facets = $query->getFacetSet();
		$query->setStart(0);
		$query->setRows(5000);
		$resultset = $this->client->select($query);
		$resultset = $resultset->getData()['response']['docs'];
		$reviews = [];
		if(count($resultset)>0){
			for($i=0; $i<count($resultset); $i++){ 
				$review = \App\Model\Review::where('id', $resultset[$i]['id'])
							->where('approved',1)->get()->first();
				if(!empty($review)){
					if(!$review->user)
						continue;
					$rvw['id'] = $review->id;
					$rvw['user_id'] = $review->user_id;
					$rvw['name'] = $review->user->user->name;
					$rvw['disease_visited'] = $review->disease_visited;
					$rvw['rating'] = $review->rating;
					$rvw['title'] = $review->title;
					$rvw['comment'] = $review->comment;
					$rvw['created_at'] = $review->created_at;
					$rvw['approved'] = $review->approved;
					$rvw['post_anonymous'] = $review->post_anonymous;
					$rvw['image'] = $review->user->user->image;
					$rvw['thumbsup_count']=\App\Model\HelpfullVote::where(array('review_id'=>$review->id, 'vote'=>1))->count();
					$rvw['thumbsdown_count']=\App\Model\HelpfullVote::where(array('review_id'=>$review->id, 'vote'=>0))->count();
					$reviews[] = $rvw;
				}
			}
		}
		$galleries =  \App\Model\Gallery::where('doctor_id',$request->input('id'))->get();		
		$entities = compact("associated_hospitals", "reviews","galleries");
		if(!$entities){
				$response = array('status'=>0, 'message'=>'No data found.'); 
				return $response;
		}
		$response = array('status'=>1, 'message'=>'success.','data'=>$entities); 	
		return $response;
	}
	public function hospitalDescription(Request $request){
		
		$validator = Validator::make($request->all(), 
		[
			'id'=>'required|exists:hospitals,id',
			'latitude'=>'required',
			'longitude'=>'required',
		]);
		# @If input passes validation - store the review in DB, otherwise return to product page with error message 
		if (!$validator->passes()) {
				$response = array('status'=>0, 'message'=>$validator->errors()->first()); 
				return $response;
		}
		
		#Finding associted doctor
		$associated_doctor = [];
		$hospital = \App\Model\Hospital::find($request->input('id'));
		$doctors= $hospital->doctors;
		if($hospital->doctors()->get()->count()>0){
			$ids = "(";
			$i=1;
			foreach($doctors as $var){
				$ids.=$var->id;
				if(count($doctors)>$i++){
						$ids.=' or ';
				}
			}
			$ids.=')';
			$assoDocQuery = $this->client->createSelect();
			$assoDocHelper = $assoDocQuery->getHelper();
			# Adding distance field
			$assoDocQuery->addField('distance:' . $assoDocHelper->geodist(
					'latlon', 
					doubleval(trim($request->input('latitude'))), 
					doubleval(trim($request->input('longitude')))
				)
			); 
			$assoDocQuery->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
			$assoDocQuery->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$ids, 'tag'=>'exclude'));
			$assoDocResultset = $this->client->select($assoDocQuery);				
			$assoDocResultset = $assoDocResultset->getData()['response'];
			$associated_doctor = $assoDocResultset['docs'];	
			$i=0;
			foreach($associated_doctor as $temp){ 
				$associated_doctor[$i] = $temp;
				$associated_doctor[$i]['days_of_week'] = array(1,2,3,4);
				$associated_doctor[$i++]['qualifications']= \App\Model\Doctor::getQualifications($temp['id']);
				
			}
		}
		# Finding rating & review details
		$query = $this->client->createSelect();
		$helper = $query->getHelper();
		$query->addFilterQuery(array('key'=>'entity', 'query'=>'entity:review', 'tag'=>'exclude'));
		$query->addFilterQuery(array('key'=>'hospital_id', 'query'=>'hospital_id:'.$request->input('id'), 'tag'=>'exclude'));
		$facets = $query->getFacetSet();
		$query->setStart(0);
		$query->setRows(5000);
		$resultset = $this->client->select($query);
		$resultset = $resultset->getData()['response']['docs'];
		$reviews = [];
		if(count($resultset)>0){
			for($i=0; $i<count($resultset); $i++){ 
				$review = \App\Model\Review::where('id', $resultset[$i]['id'])
							->where('approved',1)->get()->first();
				if(!empty($review)){
					if(!$review->user)
						continue;
					$rvw['id'] = $review->id;
					$rvw['user_id'] = $review->user_id;
					$rvw['name'] = $review->user->user->name;
					$rvw['disease_visited'] = $review->disease_visited;
					$rvw['rating'] = $review->rating;
					$rvw['title'] = $review->title;
					$rvw['comment'] = $review->comment;
					$rvw['created_at'] = $review->created_at;
					$rvw['approved'] = $review->approved;
					$rvw['image'] = $review->user->user->image;
					$rvw['thumbsup_count']=\App\Model\HelpfullVote::where(array('review_id'=>$review->id, 'vote'=>1))->count();
					$rvw['thumbsdown_count']=\App\Model\HelpfullVote::where(array('review_id'=>$review->id, 'vote'=>0))->count();
					$reviews[] = $rvw;
				}
			}
		}
		$galleries =  \App\Model\Gallery::where('hospital_id',$request->input('id'))->get();
		$entities = compact("associated_doctor","reviews","galleries");
		if(!$entities){
				$response = array('status'=>0, 'message'=>'No data found.'); 
				return $response;
		}
		$response = array('status'=>1, 'message'=>'success.','data'=>$entities);
		return $response;
	}
	public function diagnosticDescription(Request $request){
		$validator = Validator::make($request->all(), 
		[
			'id'=>'required|exists:diagnostics,id',
			'latitude'=>'required',
			'longitude'=>'required',
		]);
		# @If input passes validation - store the review in DB, otherwise return to product page with error message 
		if (!$validator->passes()) {
				$response = array('status'=>0, 'message'=>$validator->errors()->first()); 
				return $response;
		}
		# Finding rating & review details
		$query = $this->client->createSelect();
		$helper = $query->getHelper();
		$query->addFilterQuery(array('key'=>'entity', 'query'=>'entity:review', 'tag'=>'exclude'));
		$query->addFilterQuery(array('key'=>'diagnostic_id', 'query'=>'diagnostic_id:'.$request->input('id'), 'tag'=>'exclude'));
		$facets = $query->getFacetSet();
		$query->setStart(0);
		$query->setRows(5000);
		$resultset = $this->client->select($query);
		$resultset = $resultset->getData()['response']['docs']; 
		$reviews = [];
		if(count($resultset)>0){
			for($i=0; $i<count($resultset); $i++){ 
				$review = \App\Model\Review::where('id', $resultset[$i]['id'])
							->where('approved',1)->get()->first();
				if(!empty($review)){
					if(!$review->user)
						continue;
					$rvw['id'] = $review->id;
					$rvw['user_id'] = $review->user_id;
					$rvw['name'] = $review->user->user->name;
					$rvw['disease_visited'] = $review->disease_visited;
					$rvw['rating'] = $review->rating;
					$rvw['title'] = $review->title;
					$rvw['comment'] = $review->comment;
					$rvw['created_at'] = $review->created_at;
					$rvw['approved'] = $review->approved;
					$rvw['image'] = $review->user->user->image;
					$rvw['thumbsup_count']=\App\Model\HelpfullVote::where(array('review_id'=>$review->id, 'vote'=>1))->count();
					$rvw['thumbsdown_count']=\App\Model\HelpfullVote::where(array('review_id'=>$review->id, 'vote'=>0))->count();
					$reviews[] = $rvw;
				}
			}
		}
		$galleries =  \App\Model\Gallery::where('diagnostic_id',$request->input('id'))->get();		
		$entities = compact("reviews","galleries");
		if(!$entities){
				$response = array('status'=>0, 'message'=>'No data found.'); 
				return $response;
		}
		$response = array('status'=>1, 'message'=>'success.','data'=>$entities); 	
        return $response;
	}
	public function associatedHospitals(Request $request)
	{
		try {
			$doctor = \App\Model\Doctor::find($request->input('id'));
			$result= $doctor->hospitals;
			if(count($result)<1){
                $response = array('status'=>0, 'message'=>'No data found.'); 
                return $response;
            }
        }catch(\Illuminate\Database\QueryException $e){
                return array('status'=>0, 'message'=>$e->getMessage());
        }
        $response = array('status'=>1, 'message'=>'success.','data'=>$result); 	
        return $response;
	}
	public function nearByDoctors(Request $request)
	{
		try{
			$validator = Validator::make($request->all(), ['latitude'=>'required','longitude'=>'required',]);
			if (!$validator->passes()) {
				$response = array('status'=>0, 'message'=>'Failed to processed the request.'); 
				return $response;
			}
			$query = $this->client->createSelect();
			$helper = $query->getHelper();
			$query->setQuery('role:2');
			$latitude =$request->input('latitude');
			$longitude =$request->input('longitude');
			$distance=10;
			$query->createFilterQuery('distance')->setQuery(
				$helper->geofilt(
					'latlon', 
					doubleval($latitude),
					doubleval($longitude),
					doubleval($distance)
				)
			);
			$query->addField($helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			);
			$query->clearFields()->addFields('*');
			$query->setStart(0);
			$query->setRows(5000);
			$resultset = $this->client->select($query);
			$resultset = $resultset->getData()['response']['docs']; 
		}catch (\Solarium\Exception $e) {
			$response = array('status'=>0, 'message'=>$e->getMessage); 
		}
		$response = array('status'=>1, 'message'=>'success','data'=>$resultset); 
		return $response;
	}
	public function recentSearch(Request $request){
        try{
            $validator =Validator::make($request->all(), [
                'user_id' => 'required',
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                $response = array('status'=>0, 'message'=>$error); 
            }
			if(!$request->input('isDelete')){
				$result = \App\Model\RecentSearch::where('user_id',$request->input('user_id'))->orderBy('id','desc')->get();
				if(!\App\Model\RecentSearch::where('user_id',$request->input('user_id'))->orderBy('id','desc')->get()->count()){
					return array('status'=>0, 'message'=>'No data found.');
				}
				$i=0;
				foreach($result as $temp){
					$result[$i++]['time'] =  \Carbon\Carbon::createFromTimeStamp(strtotime($temp->created_at))->diffForHumans();
				}
				$response = array('status'=>1, 'message'=>'success','data'=>$result);
			}else{
				\App\Model\RecentSearch::where('user_id', '=', $request->input('user_id'))->delete();
				$response = array('status'=>1, 'message'=>config('constants.message.delete_recent_search'));
			}				
        } catch (\Illuminate\Database\QueryException $ex) {
            $response = array('status'=>0, 'message'=>$e->getMessage); 
        }
        return $response;
    }
    public function addBookmark(Request $request){
        #Save search
        try{
            $validator =Validator::make($request->all(), [
                'user_id' => 'required',
                'profile_id'=>'required'
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return array('status'=>0, 'message'=>$error); 
            }
			if(\App\User::where('id',$request->input('user_id'))->get()->count()<1){
				return array('status'=>0, 'message'=>config('constants.message.user_not_found'));  
			}
			$role = DB::table('role_user')->where('user_id',$request->input('user_id'))->first()->role_id;
            switch ($role) {               
                case 3:
                    if($request->input('profile_id') == @\App\User::find($request->input('user_id'))->doctor->id){
                      return array('status'=>0, 'message'=>config('constants.message.add_bookmark'));  
                    }
                    break;
                case 4:
                    if($request->input('profile_id') == @\App\User::find($request->input('user_id'))->diagnostic->id){
                      return array('status'=>0, 'message'=>config('constants.message.add_bookmark'));  
                    }
                    break;
                case 5:
                    if($request->input('profile_id') == \App\User::find($request->input('user_id'))->hospital->id){
                      return array('status'=>0, 'message'=>config('constants.message.add_bookmark'));  
                    }
                    break;
                default:
                    return array('status'=>0, 'message'=>'User is not found.');
            }
			if(!$request->input('isDelete')){
				if(\App\Model\Bookmark::where('user_id',$request->input('user_id'))->Where('entity_id',$request->input('profile_id'))->get()->count()>0){
					return array('status'=>1, 'message'=>'You have already bookmarked.'); 
				}
				\App\Model\Bookmark::create([
					"user_id"=>$request->input('user_id'),
					"entity_id"=>$request->input('profile_id'),              
				]);
				$response = array('status'=>1, 'message'=>'Added successfully'); 
			}else{
				\App\Model\Bookmark::where('user_id', '=', $request->input('user_id'))->delete();
				$response = array('status'=>1, 'message'=>config('constants.message.delete_bookmark'));
			}
        } catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
    }
    public function bookmarks(Request $request){
        try{
            $validator =Validator::make($request->all(), [
                'user_id' => 'required',
				'latitude' => 'required',
				'longitude' => 'required',
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return array('status'=>0, 'message'=>$error); 
            }
			if(!\App\Model\Bookmark::where('user_id',$request->input('user_id'))->get()->count()){
				return array('status'=>0, 'message'=>'No result found.'); 
			}
			$result = \App\Model\Bookmark::where('user_id',$request->input('user_id'))->get();
			$id = "(";
			$i=1;
			foreach($result as $var){
				$id.=$var['entity_id'];
				if(count($result)>$i++){
						$id.=' or ';
				}
			}
			$id.=')';
			$query = $this->client->createSelect();
			$helper = $query->getHelper();			
			$query->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$id, 'tag'=>'exclude'));
			$facets = $query->getFacetSet();
			$facets->createFacetField(array('field'=>'speciality', 'exclude'=>'exclude'));
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval(trim($request->input('latitude'))), 
					doubleval(trim($request->input('longitude')))
				)
			);
			$query->setStart(0);
			$query->setRows(5000);
			$resultset = $this->client->select($query);
			$resultset = $resultset->getData()['response']['docs'];
			$i=0;
			foreach($resultset as $temp){ 
				## Fimding rating and review.
				$resultset[$i]['days_of_week'] = array(1,2,3,4);
				if($temp['role'] ==2):
					$resultset[$i++]['qualifications']= \App\Model\Doctor::getQualifications($temp['id']);
				endif;
			}
            $response = array('status'=>1, 'message'=>'success','data'=>$resultset); 
        } catch (\Solarium\Exception $ex) {
            $response = array('status'=>0, 'message'=>$e->getMessage); 
        }
		catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
    }
	public function deleteAccount(Request $request){
		try{
            $validator =Validator::make($request->all(), [
            'user_id' => 'required',
			]);
			if ($validator->fails()) {
				$error = $validator->errors()->first();
				$response = array('status'=>0, 'message'=>$error); 
				$response = json_encode($response);
				return $response;
			}
			$user = \App\User::where('id',$request->input('user_id'))
							->where('email',$request->input('email'))->get()->first();
			if(!$user){
				return array('status'=>0, 'message'=>'Invalid user ID or email.');
			}
			$user->activated=2;
			$user->save();
			$response= array('status'=>1, 'message'=>'Account deleted successfully.');
        }
		catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
	}
	public function sendFeedback(Request $request){
		try{
            $validator =Validator::make($request->all(), [
				'user_id' => 'required',
				'feedback_message' => 'required',
			]);
			if ($validator->fails()) {
				$error = $validator->errors()->first();
				$response = array('status'=>0, 'message'=>$error); 
				$response = json_encode($response);
				return $response;
			}
			\App\Model\Feedback::create([
				'user_id'=>$request->input('user_id'),
				'message'=>$request->input('feedback_message'),
			]);
			$user = \App\User::find($request->input('user_id'));
			## Sending email to admin
			$data = [
				'email' => 'medratorsystem@gmail.com',
				'user_name'=>$user->user->name,
				'feedback_message'=>$request->input('feedback_message'),
			];
			Mail::send('email.send_feedback',['data'=>$data], function ($message) use ($data) {
				$message->to($data['email'])->subject('New Feedback');
			});
			$response= array('status'=>1, 'message'=>'Feedback sent successfully.');
        }
		catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
	}
	public function getUserAssociatedDoctorProfile(Request $request){
		try{
            $validator =Validator::make($request->all(), [
				'user_id' => 'required',
			]);
			if ($validator->fails()) {
				$error = $validator->errors()->first();
				return array('status'=>0, 'message'=>$error); 
			}
			if($result = \App\Model\Doctor::userAssociatedProfile($request->input('user_id'))){
				$response= array('status'=>1, 'message'=>'Success.', 'data'=>$result);
			}else{
				$response= array('status'=>0, 'message'=>'No associated profile.');
			}
			
        }
		catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
	}
	public function reportErrors(Request $request){
		try{
            $validator =Validator::make($request->all(), [
				'user_id' => 'required',
				'profile_id' => 'required',
				'error_title' => 'required',
				'error_description' => 'required',
			]);
			if ($validator->fails()) {
				$error = $validator->errors()->first();
				return array('status'=>0, 'message'=>$error); 
			}
			if(\App\Model\ErrorReports::where('user_id',$request->input('user_id'))->where('profile_id',$request->input('profile_id'))->get()->count()>0){
				$erroeReport =\App\Model\ErrorReports::where('user_id',$request->input('user_id'))->where('profile_id',$request->input('profile_id'))->orderBy('id', 'desc')->get()->first();
				$hours =  \Carbon\Carbon::createFromTimeStamp(strtotime($erroeReport->created_at))->diffInHours();
				if($hours < config('constants.error_report_cycle')){
					$response = array('status'=>1, 'message'=>'You can report after '.config('constants.error_report_cycle').' hours'); 	
					return $response;
				}
			}
			\App\Model\ErrorReports::create($request->except('_token','timestamp'));
			### Sendin mail ############
			$response =  array('status'=>1, 'message'=>config('message.error_report'));
        }
		catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
	}
	public function getUserReviewList(Request $request){
		try{
            $validator =Validator::make($request->all(), [
                'user_id' => 'required',
				'latitude' => 'required',
				'longitude' => 'required',
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return array('status'=>0, 'message'=>$error); 
            }
			if(!\App\Model\Review::where('user_id',$request->input('user_id'))->get()->count()){
				return array('status'=>0, 'message'=>'No review was found.'); 
			}
			$result = \App\Model\Review::where('user_id',$request->input('user_id'))->get();
			$id = "(";
			$i=1;
			foreach($result as $var){
				if($var['doctor_id']){
					$id.=$var['doctor_id'];
					if(count($result)>$i++){
							$id.=' or ';
					}
				}
				if($var['hospital_id']){
					$id.=$var['hospital_id'];
					if(count($result)>$i++){
							$id.=' or ';
					}
				}
				if($var['diagnostic_id']){
					$id.=$var['diagnostic_id'];
					if(count($result)>$i++){
							$id.=' or ';
					}
				}
			}
			$id.=')';
			$query = $this->client->createSelect();
			$helper = $query->getHelper();			
			$query->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$id, 'tag'=>'exclude'));
			$facets = $query->getFacetSet();
			$facets->createFacetField(array('field'=>'speciality', 'exclude'=>'exclude'));
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval(trim($request->input('latitude'))), 
					doubleval(trim($request->input('longitude')))
				)
			);
			$query->setStart(0);
			$query->setRows(5000);
			$resultset = $this->client->select($query);
			$resultset = $resultset->getData()['response']['docs'];
			$i=0;
			foreach($resultset as $temp){ 
				## Fimding rating and review.
				$resultset[$i]['days_of_week'] = array(1,2,3,4);
				if($temp['role'] ==2):
					$resultset[$i++]['qualifications']= \App\Model\Doctor::getQualifications($temp['id']);
				endif;
			}
            $response = array('status'=>1, 'message'=>'success','data'=>$resultset); 
        } catch (\Solarium\Exception $ex) {
            $response = array('status'=>0, 'message'=>$e->getMessage); 
        }
		catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
	}
	public function userInterests(Request $request){
		try{ 
			if($request->input('user_id')){
				$validator =Validator::make($request->all(), [
					'user_id' => 'exists:login,id'				
				]);
				if ($validator->fails()) {
					$error = $validator->errors()->first();
					return array('status'=>0, 'message'=>$error); 
				}
				$interests = \App\User::find(trim($request->input('user_id')))->interests()->select('interest_id as id','name')->get();
			}else{
				$wp_tags = get_tags();
				for($i=0; $i<count($wp_tags); $i++)
				{
					$interests[] = [
						'id' =>$wp_tags[$i]->term_id,
						'name' =>$wp_tags[$i]->name
					];
				}		
			}
			$response = array('status'=>1, 'message'=>'success', 'data'=>$interests);
        }
		catch(\Illuminate\Database\QueryException $e){
			$response=  array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
	}
	public function addInterest(Request $request){
		try{
			$validator =Validator::make($request->all(), [
				'user_id' => 'required|exists:login,id',
				'interests' => 'required',	
			]);
			if ($validator->fails()) {
				$error = $validator->errors()->first();
				return array('status'=>0, 'message'=>$error); 
			}
			\App\Model\UserInterest::where('user_id',$request->input('user_id'))->delete();
			foreach($request->input('interests') as $interest){
				$interest = \App\Model\UserInterest::updateOrCreate(
				[ 
					'user_id'=> $request->input('user_id'),
					'name' =>$interest['name']
				],
				[
					'name' =>$interest['name'],
					'interest_id' =>$interest['id']
				]);
			}
			$response = array('status'=>1, 'message'=>'success');
        }
		catch(\Illuminate\Database\QueryException $e){
			$response=  array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
	}
	public function getNotifications(Request $request){
       try{
            $validator =Validator::make($request->all(), [
                    'user_id' => 'required|exists:login,id',	
            ]);
            if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    return array('status'=>0, 'message'=>$error); 
            }
            $result = \App\Model\Notification::where('status',0)->where('receiver',$request->input('user_id'))->orderBy('id', 'desc')->get();
			for($i=0; $i<count($result); $i++){
				$date = \Carbon\Carbon::createFromTimeStamp(strtotime($result[$i]->created_at))->diffForHumans();
				$result[$i]->notification_time= $date;
			}
            if($result->count()){
                $response = array('status'=>1, 'message'=>'success', 'data'=>$result);
            }else{
                $response = array('status'=>0, 'message'=>'No result found', 'data'=>$result);
            }

        }
        catch(\Illuminate\Database\QueryException $e){
                $response=  array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
   }
   public function setReadNotification(Request $request){
       try{
            $validator =Validator::make($request->all(), [
                    'user_id' => 'required|exists:login,id',	
            ]);
            if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    return array('status'=>0, 'message'=>$error); 
            }
            $notification = \App\Model\Notification::where('receiver',$request->input('user_id'))->update([
				'status' => 1
			]);
            $response = array('status'=>1, 'message'=>'success');
        }
        catch(\Illuminate\Database\QueryException $e){
                $response=  array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
   }
   public function points(Request $request){
       try{
            $validator =Validator::make($request->all(), [
                    'user_id' => 'required|exists:login,id',	
            ]);
            if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    return array('status'=>0, 'message'=>$error); 
            }
            $level = \App\Model\ContributorLevel::all();
            $data['levels'] = $level;
            $points = \App\Model\Point::where('user_id',$request->input('user_id'))->sum('point_collected');
			$data['points_collected'] = ($points?$points:0);
            $response = array('status'=>1, 'message'=>'success','data'=>$data);
        }
        catch(\Illuminate\Database\QueryException $e){
                $response=  array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
   }
   public function schedules(Request $request){
	   try{
            $validator =Validator::make($request->all(), [
				'doctor_id' => 'required|exists:doctors,id',
				'hospital_id' => 'required|exists:hospitals,id',					
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return array('status'=>0, 'message'=>$error); 
            }
            ## Finding monday schedule.
            for($i=1; $i<=7; $i++){
                $timings = \App\Model\WorkingDatetimes::select('start_time','end_time')
                        ->where('doctor_id', $request->input('doctor_id'))
                        ->where('hospital_id', $request->input('hospital_id'))
                         ->where('week_day', $i)
                        ->get();
                $data[] =['week_day'=>$i,'timings'=>$timings];
            }
            $response = array('status'=>1, 'message'=>'success','data'=>$data);
        }
        catch(\Illuminate\Database\QueryException $e){
                $response=  array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
   }
   public function sendOtp(Request $request){
       try{
            $validator =Validator::make($request->all(), [
                'user_id' => 'required|exists:login,id',               				
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return array('status'=>0, 'message'=>$error); 
            }
			$user = \App\Model\Users::where('user_id','<>',$request->input('user_id'))
								->where('mobile',$request->input('mobile'))
								->get();
			if($user->count()>0){
				$response=  array('status'=>0, 'message'=>'Mobile already exist.');
				return $response;
			}
            ## Send otp
            //$otp = mt_rand(100000, 999999);
			$otp = 1234;
            $data['otp'] = $otp;
            $data['mobile'] = $request->input('mobile');
            $response = array('status'=>1, 'message'=>'success','data'=>$data);
        }
        catch(\Illuminate\Database\QueryException $e){
                $response=  array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
   }
   public function verifyMobile(Request $request){
       try{
            $validator =Validator::make($request->all(), [
                'user_id' => 'required|exists:login,id',					
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return array('status'=>0, 'message'=>$error); 
            }
            $user = \App\User::find($request->input('user_id'));
            $user->user->is_mobile_verified =1;
            $user->user->save();
            $response = array('status'=>1, 'message'=>'Mobile verified successfully.');
        }
        catch(\Illuminate\Database\QueryException $e){
                $response=  array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
   }
   public function getBlogs(Request $request){
		if($request->input('user_id')){
			$tags =  \App\User::find(trim($request->input('user_id')))->interests()->get();
			if ($tags->count()>0) {
				$tag_ids = array();
				foreach($tags as $individual_tag) 
					$tag_ids[] = $individual_tag->interest_id;           
				$args=array(
					'tag__in' => $tag_ids,
					'posts_per_page'=>4, // Number of related posts to display.
					'caller_get_posts'=>1
				);
				$posts = get_posts($args);  
			}else{
				$args=array(
					'posts_per_page'=>4, // Number of related posts to display.
					'caller_get_posts'=>1
				);
				$posts = get_posts($args);  
			}
			
		}else{
			$args=array(
                'posts_per_page'=>4, // Number of related posts to display.
                'caller_get_posts'=>1
            );
			$posts = get_posts($args);  
		}       
		$i=0;
		foreach($posts as $post):
			$images = [];
			$mediaImages = get_attached_media( 'image', $post->ID);
			foreach($mediaImages as $image){
				$images[]['img_url'] = $image->guid;
			}
			//return get_the_content();
			$posts[$i]->images= $images;
			$content = strip_tags($posts[$i]->post_content);
			$content= preg_replace('/[ ]{2,}|[\r\n]/', ' ', trim($content));
			/*$content = preg_replace("/<img[^>]+\>/i", "(image) ", $posts[$i]->post_content); 		
			$content = preg_replace("/<iframe[^>]+\>/i", "(video) ", $content);             
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]>', $content);
			return $content;*/
			$posts[$i]->post_content = $content;
			$i++;
		endforeach;
		$response=  array('status'=>1, 'message'=>'succes', 'data'=>$posts);
		return $response;
   }
}
