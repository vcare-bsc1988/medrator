<?php

namespace App\Http\Controllers\Api\Beta;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccount;
use App\User;
use App\Role;
use App\Model\Doctor;
use App\Model\Hospital;
use App\Model\Diagnostic;
use App\Model\Users;
use DB;
use Illuminate\Support\Facades\Mail;
class SocialLoginController extends Controller
{

    public function index(Request $request)
    {
        try{
			$email = $request->input('email');
			if($request->provider && $request->name && $request->email && $request->device_token && $request->device_type){
				$account = SocialAccount::whereProvider($request->provider)
					->whereProviderUserId($request->id)
					->first();
				if ($account) {
					try{						
						#Updated on 13th Aug 16						
						$user = User::select('login.*','up.user_id', 'up.name', 'up.mobile', 'up.is_mobile_verified', 'up.phone', 'up.gender', 'up.age', 'up.dob', 'up.image', 'up.additional_image', 'up.about', 'up.address_one', 'up.address_two', 'up.locality', 'up.city_name', 'up.state_name', 'up.country_name', 'up.country_code', 'up.latitude', 'up.longitude', 'up.pincode', 'up.rating_counts', 'up.review_counts', 'up.forum_post_counts', 'up.attachement_counts', 'up.vote_counts', 'up.profile_claimed', 'up.tags', 'up.created_at', 'up.updated_at','role_user.role_id as role')
								->join('role_user', 'login.id', '=', 'role_user.user_id')
								->join('users as up', 'login.id', '=', 'up.user_id')
								->where('email', trim($request->input('email')))->first();
						if(!$user){
							$response = array("status"=>0,"message"=>'Your account is disabled. Contact to admin.');
							return $response;
						}
						$user->device_type = $request->device_type;
						$user->device_token = $request->device_token;
						$user->save();
						Users::where('user_id',$user->user_id)->update([
							'image'=>'' //$request->image
						]);
						$user = User::select('login.*','up.user_id', 'up.name', 'up.mobile', 'up.is_mobile_verified', 'up.phone', 'up.gender', 'up.age', 'up.dob', 'up.image', 'up.additional_image', 'up.about', 'up.address_one', 'up.address_two', 'up.locality', 'up.city_name', 'up.state_name', 'up.country_name', 'up.country_code', 'up.latitude', 'up.longitude', 'up.pincode', 'up.rating_counts', 'up.review_counts', 'up.forum_post_counts', 'up.attachement_counts', 'up.vote_counts', 'up.profile_claimed', 'up.tags', 'up.created_at', 'up.updated_at','role_user.role_id as role')
								->join('role_user', 'login.id', '=', 'role_user.user_id')
								->join('users as up', 'login.id', '=', 'up.user_id')
								->where('email', $request->input('email'))->first();
						if($user->city_name =="")
							$user->city_name = "";
						if($user->country_name == "")
							$user->country_name = "";
						
						$facebookCount = $user->socialAccount()->where('provider','facebook')->get()->count();
						
						$googleCount = $user->socialAccount()->where('provider','googleplus')->get()->count();
						
						$user->is_facebook = ($facebookCount > 0?1:0); 
						
						$user->is_google = ($googleCount > 0?1:0); 
						
						$associated_doctor_profile="";	
						$doctor =Doctor::select('doctors.*')
							->where('user_id',$user->user_id)->first();
						if($doctor):
						#Getting Qualification
						$qualifications = DB::table('doctor_qualification')
							->join('qualifications', 'doctor_qualification.qualification_id', '=', 'qualifications.id')
							->where('doctor_qualification.doctor_id', '=', $doctor->id)
							->select('qualifications.id','qualifications.name','doctor_qualification.college','doctor_qualification.year')
							->get();
						$doctor->qualifications = $qualifications;
						
						#Getting Specialties
						$specialties = DB::table('specialty_details')
							->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
							->where('specialty_details.doctor_id', '=', $doctor->id)
							->select('specialties.id','specialties.name')
							->get();
						$doctor->specialties = $specialties;
						
						#Getting Services
						$services = DB::table('service_details')
							->join('service_type', 'service_details.service_id', '=', 'service_type.id')
							->where('service_details.doctor_id', '=', $doctor->id)
							->select('service_type.id','service_type.name')
							->get();
						$doctor->services = $services;
						$associated_doctor_profile = $doctor;
						endif;
						$has_doctor_profile=0;
						if(count($doctor)>0)
							$has_doctor_profile=1;
						if($user->activated ==2){
							$response = array("status"=>0,"message"=>"Your account has been disabled. Please contact to admin.");
							return $response;
						}
						if(!$user->status){
							$response = array("status"=>0,"message"=>"Your account has been disabled. Please contact to admin.");
							return $response;
						}
						$result = array(
							"basic_profile" =>$user,
							"has_associate_doctor_profile"=>$has_doctor_profile,
							"associate_doctor_profile"=>$associated_doctor_profile
						);
					}catch (\Illuminate\Database\QueryException $e) {
						$response = array("status"=>0,"message"=>$e->getMessage());
						return $response;
					}
				}
				else {
					try{
						$account = new SocialAccount([
							'provider_user_id' => $request->id,
							'provider' => $request->provider
						]);
						$user = User::where('email', $request->email)->first();
						if($user)
						{
							$user->device_type = $request->device_type;
							$user->device_token = $request->device_token;
						}
						else
						{
							### wordpress registration start here
							$wp_user = \wp_insert_user([
								'user_pass'=>$request->password,
								'user_login'=>$request->email,
								'user_email'=>$request->email,
								'role'=>'contributor'
							]);
							### Finished
							User::insert([
								'id' => $wp_user,
								'email' => $request->email,
								'device_token' => $request->device_token,
								'device_type' => $request->device_type,
							]);
							$user = \App\User::find($wp_user);
							\App\Model\Users::create([
								'user_id' => $user->id,
								'name' => $request->input('name'),
								'image' => $request->input('image'),
								'dob'=>($request->input('dob')?$request->input('dob'):''),
								'city_name' => ($request->input('city_name')?$request->input('city_name'):''),
								'state_name'=> ($request->input('state_name')?$request->input('state_name'):''),
								'country_name' => ($request->input('country_name')?$request->input('country_name'):''),
								'country_code' => ($request->input('country_code')?$request->input('country_code'):''),
								'address_one' => ($request->input('address_one')?$request->input('address_one'):''),
								'address_two' => ($request->input('address_two')?$request->input('address_two'):''),
								'locality' => ($request->input('locality')?$request->input('locality'):''),
								'latitude' => ($request->input('latitude')?$request->input('latitude'):''),
								'longitude' => ($request->input('longitude')?$request->input('longitude'):'')
							]);
							$user->roles()->attach(3);
							/**
							 * Sending welcome mail to user
							 */
							$data = [
								'email' => $user->email,
								'user_name'=>$user->user->name
							];
							Mail::send('email.welcome',['data'=>$data], function ($message) use ($data) {
								$message->to($data['email'])->subject('Welcome Email');
							});
						}
						$account->user()->associate($user);
						$account->save();
						
						#Updated on 13th Aug 16
						$user = User::select('login.*','up.user_id', 'up.name', 'up.mobile', 'up.is_mobile_verified', 'up.phone', 'up.gender', 'up.age', 'up.dob', 'up.image', 'up.additional_image', 'up.about', 'up.address_one', 'up.address_two', 'up.locality', 'up.city_name', 'up.state_name', 'up.country_name', 'up.country_code', 'up.latitude', 'up.longitude', 'up.pincode', 'up.rating_counts', 'up.review_counts', 'up.forum_post_counts', 'up.attachement_counts', 'up.vote_counts', 'up.profile_claimed', 'up.tags', 'up.created_at', 'up.updated_at','role_user.role_id as role')
								->join('role_user', 'login.id', '=', 'role_user.user_id')
								->join('users as up', 'login.id', '=', 'up.user_id')
								->where('email', $request->input('email'))->first();

						$user->device_type = $request->device_type;
						$user->device_token = $request->device_token;
						$user->activated = 1;
						$user->status = 1;
						$user->save();

						if($user->city_name =="")
							$user->city_name = "";
						if($user->country_name == "")
							$user->country_name = "";
						
						$facebookCount = $user->socialAccount()->where('provider','facebook')->get()->count();
						
						$googleCount = $user->socialAccount()->where('provider','googleplus')->get()->count();
						
						$user->is_facebook = ($facebookCount>0)?1:0;
						
						$user->is_google = ($googleCount>0)?1:0; 
						
						$associated_doctor_profile="";
						$doctor =Doctor::select('doctors.*')
							->where('user_id',$user->user_id)->first();
						if($doctor):
						#Getting Qualification
						$qualifications = DB::table('doctor_qualification')
							->join('qualifications', 'doctor_qualification.qualification_id', '=', 'qualifications.id')
							->where('doctor_qualification.doctor_id', '=', $doctor->id)
							->select('qualifications.id','qualifications.name')
							->get();
						$doctor->qualifications = $qualifications;
						
						#Getting Specialties
						$specialties = DB::table('specialty_details')
							->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
							->where('specialty_details.doctor_id', '=', $doctor->id)
							->select('specialties.id','specialties.name')
							->get();
						$doctor->specialties = $specialties;
						
						#Getting Services
						$services = DB::table('service_details')
							->join('service_type', 'service_details.service_id', '=', 'service_type.id')
							->where('service_details.doctor_id', '=', $doctor->id)
							->select('service_type.id','service_type.name')
							->get();
						$doctor->services = $services;
						$associated_doctor_profile=$doctor;
						endif;
						
						$has_doctor_profile=0;
						if(count($doctor)>0)
							$has_doctor_profile=1;
						if($user->activated ==2){
							$response = array("status"=>0,"message"=>"Your account has been disabled. Please contact to admin.");
							return $response;
						}
						if(!$user->status){
							$response = array("status"=>0,"message"=>"Your account has been disabled. Please contact to admin.");
							return $response;
						}
						$result = array(
							"basic_profile" =>$user,
							"has_associate_doctor_profile"=>$has_doctor_profile,
							"associate_doctor_profile"=>$has_doctor_profile
						);
					}catch (\Illuminate\Database\QueryException $e) {
						$response = array("status"=>0,"message"=>$e->getMessage());
						return $response;
					}
					
				}
				
				$response = array("status"=>1,"message"=>"success!", "data"=>$result);
			}
			else{
				$response = array("status"=>0,"message"=>"Invalid request !");
			}
			return $response;
		}catch(\Illuminate\Database\QueryException $e){
			
		}
    }

    
}
