<?php

namespace App\Http\Controllers\Api\Beta;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Contracts\Provider;
use App\Role;
use App\User;
use App\Model\Users;
use Hash;
use Image;
use DB;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\ActivationService;
use Illuminate\Support\Facades\Mail;
class RegisterController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $activationService;
    public function __construct(ActivationService $activationService)
    {
        //$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->activationService = $activationService;
    }
	
	public function index(Request $request)
    {
		try{
            $validator =Validator::make($request->all(), [
                'email' => 'required',
				'password' => 'required',
				'device_token' => 'required'
            ]);
            if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    $response = array('status'=>0, 'message'=>$error); 
                    $response = json_encode($response);
                    return $response;
            }
            $user = User::whereEmail($request->input('email'))->first();
            if($user){
                    $response = array("status"=>0,"message"=>"Email already exist !");
                    return $response;
            }
			### wordpress registration start here
			$wp_user = \wp_insert_user([
				'user_pass'=>$request->password,
				'user_login'=>$request->email,
				'user_email'=>$request->email,
				'role'=>'contributor'
			]);
			### Finished
            $user = User::insert([
				'id' => $wp_user,
				'email' => $request->input('email'),
				'password'=>Hash::make($request->input('password')),                   
				'device_token' => $request->input('device_token'),
				'device_type' => $request->input('device_type')              
            ]);
            $role = DB::table('role_user')->insert([
                    'user_id' => $wp_user,
                    'role_id' => 3
            ]);
            Users::create([
                'user_id' => $wp_user,
				'name' => $request->input('name'),
				'mobile' => $request->input('mobile'),
                'dob'=>($request->input('dob')?$request->input('dob'):' '),
                'city_name' => ($request->input('city_name')?$request->input('city_name'):' '),
				'state_name'=> ($request->input('state_name')?$request->input('state_name'):' '),
                'country_name' => ($request->input('country_name')?$request->input('country_name'):' '),
				'country_code' => ($request->input('country_code')?$request->input('country_code'):' '),
                'address_one' => ($request->input('address_one')?$request->input('address_one'):' '),
				'address_two' => ($request->input('address_two')?$request->input('address_two'):' '),
                'locality' => ($request->input('locality')?$request->input('locality'):' '),
                'latitude' => ($request->input('latitude')?$request->input('latitude'):' '),
                'longitude' => ($request->input('longitude')?$request->input('longitude'):' ')
            ]);			
            $user = User::select('login.id','up.name','login.email','role_user.role_id as role','login.device_token','login.created_at','login.updated_at')
                    ->join('role_user', 'login.id', '=', 'role_user.user_id')
                    ->join('users as up', 'login.id', '=', 'up.user_id')
                    ->where('email', $request->input('email'))->first();

            if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('uploaded/images/thumbnail');
                    $img = Image::make($image->getRealPath());
                    $img->resize(100, 100, function ($constraint) {
                                    $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$input['imagename']);

                    $destinationPath = public_path('uploaded/images');
                    $image->move($destinationPath, $input['imagename']);
                    Users::where('user_id',$wp_user)->update([
                            'image'=>url('/').'/public/uploaded/images/'.$input['imagename']
                    ]);
            }
        } catch (\Illuminate\Database\QueryException $e) {
            $response = array("status"=>0,"message"=>$e->getMessage());
            return $response;
        }
        /**
         * Send account activation mail
         */
        $this->activationService->sendActivationMail($user);
		/**
		 * Sending welcome mail to user
		 */
		$data = [
			'email' => $user->email,
			'user_name'=>$user->user->name
		];
		Mail::send('email.welcome',['data'=>$data], function ($message) use ($data) {
				$message->to($data['email'])->subject('Welcome Email');
		});
        $response = array("status"=>1,"message"=>"We sent you an activation code. Check your email.");
        return $response;
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
