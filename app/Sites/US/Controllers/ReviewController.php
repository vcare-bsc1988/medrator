<?php

namespace App\Sites\US\Controllers;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Requests;
use DB;
use App\Model\Specialty;
use Vcareall\Admin\Helper;
class ReviewController extends Controller
{
    protected $client;
	protected $helper;
    public function __construct(\Solarium\Client $client)
    {
            $this->client = $client;
			$this->helper = new \App\Helpers\Helper;
    }
    public function selectSpecialty(){
        $results =[
            'specialties' => \App\Model\Specialty::all()
        ];
        return view('us::reviews.select-specialties',$results);
    }
    public function doctor($specialty,Request $request){
        try {
			$query = $this->client->createSelect();
			$helper = $query->getHelper();
			if(isset($request->input('filters')['name']) && $request->input('filters')['name']!=null){
				$query->setQuery('%P1%', array($request->input('filters')['name']));
			}
			$query->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
			# Adding distance field
			$address = $request->session()->get('address');
			$latitude = ($address['latitude']?$address['latitude']:28.7040592);
			$longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			); 
			/**
			 * Applying filter
			 */
			$settings = \App\Model\Setting::where('title','radius')->get()->first();              
			if(isset($request->input('filters')['max_distance']) && $request->input('filters')['max_distance'] !='0KM'){
				$distance = substr($request->input('filters')['max_distance'],0,-2);
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($distance)
					)
				);
			}else{
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($settings->value)
					)
				);
			}
			if(isset($request->input('filters')['min_fee']) && $request->input('filters')['max_fee']){
				$minFee = $request->input('filters')['min_fee'];
				$maxFee = $request->input('filters')['max_fee'];
				$query->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $minFee, $maxFee));
			}
			if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
				$rating = $request->input('filters')['rating'];
				$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
			}
			if(isset($request->input('filters')['max_experience']) && (int)$request->input('filters')['max_experience'] !=0){
				$maxExp = (int)$request->input('filters')['max_experience'];
				$query->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', 0, $maxExp));
			}
			if(isset($request->input('filters')['working_day']) && $request->input('filters')['working_day']!=""){
				$weekDays = "(";
				$i=1;
				foreach($request->input('filters')['working_day'] as $var){
					$weekDays.=$var;
					if(count($request->input('filters')['working_day'])>$i++){
						$weekDays.=' or ';
					}				
				}
				$weekDays.=')';
				$query->addFilterQuery(array('key'=>'week_day', 'query'=>'days_of_week:'.$weekDays, 'tag'=>'exclude'));
			}
			if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
				$query->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$request->input('filters')['gender'], 'tag'=>'exclude'));
			}
			if(isset($request->input('filters')['services']) && $request->input('filters')['services']!=""){
				$services = "(";
				$i=1;
				foreach($request->input('filters')['services'] as $var){
					$services.=$var;
					if(count($request->input('filters')['services'])>$i++){
							$services.=' or ';
					}
				}
				$services.=')';
				$query->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
			}				
			if(isset($request->input('filters')['specialties']) && $request->input('filters')['specialties']!=""){
				$specialties = "(";
				$i=1;
				foreach($request->input('filters')['specialties'] as $var){
					$specialties.=$var;
					if(count($request->input('filters')['specialties'])>$i++){
							$specialties.=' or ';
					}
				}
				$specialties.=')';
				$query->addFilterQuery(array('key'=>'Specialties', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
			}
			elseif($specialty){					
                $query->addFilterQuery(array('key'=>'speciality', 'query'=>'speciality:'.$specialty, 'tag'=>'exclude'));
            }
			/****
			 * Applying sorting..
			 */
			$query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
			
			# Storing filters into the session value
			if($request->input('filters')){
					$request->session()->put('filters', $request->input('filters'));
			}else{
				$specialtyItem = Specialty::where('name', $specialty)->get()->first();
				$filters = [
						'max_distance' => $settings->value,
						'rating' =>'',
						'sorting' =>'relevence',
						'specialties' =>[$specialtyItem->id]
				];
				$request->session()->put('filters', $filters);
		}

			/**
			 * Paggination 
			 */
			$page = 1;
			$limit = 25;
			if($request->input('page')) {
				$page = $request->input('page');
			}
			$start=($page - 1)*$limit;
			$query->setStart($start);
			$query->setRows($limit);
			
			$resultset = $this->client->select($query);
			$paginator = [
				'result' => $resultset,
				'count' => $resultset->getNumFound(),
				'limit' => $limit,
				'page' => $page,
				'query' => $query
			];			
			$resultset = $resultset->getData()['response']['docs'];
			/**
			 * Remove duplicat entries.
			 */
			$resultset = Helper::removeDuplicateArr($resultset);
			/****
			 * Adding qualification in case of doctor.
			 */
			for($i=0; $i<count($resultset); $i++){
				if(isset($resultset[$i]['role']) && $resultset[$i]['role'] == 2):
					$resultset[$i]['qualifications']= \App\Model\Doctor::getQualifications($resultset[$i]['id']);
				endif;
			}
			$results =[
				'results'=>$resultset,
				'location'=>session('address')['address_one'],
				'latitude'=>session('address')['latitude'],
				'longitude'=>session('address')['longitude'],
				'locality'=>session('address')['locality'],
				'city_name'=>session('address')['city_name'],
				'state_name'=>session('address')['state_name'],
				'country_name'=>session('address')['country_name'],
				'pincode'=>session('address')['pincode'],
				'services' => \App\Model\Service::take(5)->get(),
				'services_all' => \App\Model\Service::skip(5)->take(134)->get(),
				'specialties' => \App\Model\Specialty::take(5)->get(),
				'specialties_all' => \App\Model\Specialty::orderBy('name','asc')->skip(5)->take(2000)->get(),
				'settings' => $settings,
				'default_distance' => $settings->value,
				'paginator' =>$paginator
			];
		} 
		catch (\Solarium\Exception $e) {
			$response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
			$response = json_encode($response);
			return $response;
		}
		return view('us::reviews.doctors', $results);
    }
    public function hospital(Request $request){
		try {
			$query = $this->client->createSelect();
			$helper = $query->getHelper();
			if(isset($request->input('filters')['name']) && $request->input('filters')['name']!=null){
				$query->setQuery('%P1%', array($request->input('filters')['name']));
			}
			$query->addFilterQuery(array('key'=>'role', 'query'=>'role:5', 'tag'=>'exclude'));
			# Adding distance fielld
			$address = $request->session()->get('address');
			$latitude = ($address['latitude']?$address['latitude']:28.7040592);
			$longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			); 
			#Finished distance feild
			/**
			 * Start filet.
			 */
			$settings = \App\Model\Setting::where('title','radius')->get()->first(); 
			if(isset($request->input('filters')['max_distance']) && $request->input('filters')['max_distance'] !='0KM'){
				$distance = substr($request->input('filters')['max_distance'],0,-2);
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($distance)
					)
				);
			}else{
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($settings->value)
					)
				);
			}
			#### For sorting by distance
			if(isset($request->input('filters')['min_fee']) && $request->input('filters')['max_fee']){
				$minFee = $request->input('filters')['min_fee'];
				$maxFee = $request->input('filters')['max_fee'];
				$query->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $minFee, $maxFee));
			}
			if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
				$rating = $request->input('filters')['rating'];
				$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
			}
			if(isset($request->input('filters')['experience']) && $request->input('filters')['experience']!=""){
				$query->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', 0, $max));
			}
			if(isset($request->input('filters')['working_day']) && $request->input('filters')['working_day']!=""){
				$weekDays = "(";
				$i=1;
				foreach($request->input('filters')['working_day'] as $var){
					$weekDays.=$var;
					if(count($request->input('filters')['working_day'])>$i++){
						$weekDays.=' or ';
					}				
				}
				$weekDays.=')';
				$query->addFilterQuery(array('key'=>'week_day', 'query'=>'days_of_week:'.$weekDays, 'tag'=>'exclude'));
			}
			if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
				$query->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$request->input('filters')['gender'], 'tag'=>'exclude'));
			}
			if(isset($request->input('filters')['services']) && $request->input('filters')['services']!=""){
				$services = "(";
				$i=1;
				foreach($request->input('filters')['services'] as $var){
					$services.=$var;
					if(count($request->input('filters')['services'])>$i++){
							$services.=' or ';
					}
				}
				$services.=')';
				$query->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
			}
			if(isset($request->input('filters')['specialties']) && $request->input('filters')['specialties']!=""){
				$specialties = "(";
				$i=1;
				foreach($request->input('filters')['specialties'] as $var){
					$specialties.=$var;
					if(count($request->input('filters')['specialties'])>$i++){
							$specialties.=' or ';
					}
				}
				$specialties.=')';
				$query->addFilterQuery(array('key'=>'Specialties', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
			}
			/**
			 * Filter 24 Hours Emergency
			 */
			if(isset($request->input('filters')['hours24_emergency']) && $request->input('filters')['hours24_emergency']==1){
			
				$query->addFilterQuery(array('key'=>'24_hours_emergency', 'query'=>'24_hours_emergency:'.$request->input('filters')['hours24_emergency'], 'tag'=>'exclude'));
			}
			/**
			 * Filter ICU Facility
			 */
			if(isset($request->input('filters')['icu_facility']) && $request->input('filters')['icu_facility']==1){
			
				$query->addFilterQuery(array('key'=>'icu_facility', 'query'=>'icu_facility:'.$request->input('filters')['icu_facility'], 'tag'=>'exclude'));
			}
			/**
			 * Filter Mediclaim
			 */
			if(isset($request->input('filters')['cashless_mediclaim']) && $request->input('filters')['cashless_mediclaim']==1){
			
				$query->addFilterQuery(array('key'=>'cashless_mediclaim', 'query'=>'cashless_mediclaim:'.$request->input('filters')['cashless_mediclaim'], 'tag'=>'exclude'));
			}
			/****
			 * Applying sorting..
			 */
			$query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
			/**
			 * Paggination 
			 */
			$page = 1;
			$limit = 25;
			if($request->input('page')) {
				$page = $request->input('page');
			}
			$start=($page - 1)*$limit;
			$query->setStart($start);
			$query->setRows($limit);
			
			$resultset = $this->client->select($query);
			$paginator = [
				'result' => $resultset,
				'count' => $resultset->getNumFound(),
				'limit' => $limit,
				'page' => $page,
				'query' => $query
			];			
			$resultset = $resultset->getData()['response']['docs'];
			# Storing session valeue
			if($request->input('filters')){
				$request->session()->put('filters', $request->input('filters'));
			}else{
				$filters = [
					'max_distance' => $settings->value.'KM',
					'rating' =>'',
					'min_fee' =>'',
					'max_fee' =>'',
					'sorting' =>'relevence'
				];
				$request->session()->put('filters', $filters);
			}
			$results =[
				'q'=>session('specialty'),
				'results'=>$resultset,
				'location'=>session('address')['address_one'],
				'latitude'=>session('address')['latitude'],
				'longitude'=>session('address')['longitude'],
				'locality'=>session('address')['locality'],
				'city_name'=>session('address')['city_name'],
				'state_name'=>session('address')['state_name'],
				'country_name'=>session('address')['country_name'],
				'pincode'=>session('address')['pincode'],
				'specialties' => \App\Model\Specialty::take(5)->get(),
				'specialties_all' => \App\Model\Specialty::skip(5)->take(134)->get(),
				'default_distance' => $settings->value,
				'paginator' =>$paginator
			];
		} 
		catch (\Solarium\Exception $e) {
			$response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
			$response = json_encode($response);
			return $response;
		}
		return view('us::reviews.hospital', $results);
    }
    public function diagnostic(Request $request){
        try {
			$query = $this->client->createSelect();
			$helper = $query->getHelper();
			if(isset($request->input('filters')['name']) && $request->input('filters')['name']!=null){
				$query->setQuery('%P1%', array($request->input('filters')['name']));
			}
			$query->addFilterQuery(array('key'=>'role', 'query'=>'role:4', 'tag'=>'exclude'));
			# Adding distance feild
			$address = $request->session()->get('address');
			$latitude = ($address['latitude']?$address['latitude']:28.7040592);
			$longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			); 
			#Finished distance field
			/**
			 * Start field.
			 */
			$settings = \App\Model\Setting::where('title','radius')->get()->first();              
			if(isset($request->input('filters')['max_distance']) && $request->input('filters')['max_distance'] !='0KM'){
				$distance = substr($request->input('filters')['max_distance'],0,-2);
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($distance)
					)
				);
			}else{
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($settings->value)
					)
				);
			}
			if(isset($request->input('filters')['min_fee']) && $request->input('filters')['max_fee']){
				$minFee = $request->input('filters')['min_fee'];
				$maxFee = $request->input('filters')['max_fee'];
				$query->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $minFee, $maxFee));
			}
			if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
				$rating = $request->input('filters')['rating'];
				$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
			}
			if(isset($request->input('filters')['experience']) && $request->input('filters')['experience']!=""){
				$query->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', 0, $max));
			}
			if(isset($request->input('filters')['working_day']) && $request->input('filters')['working_day']!=""){
				$weekDays = "(";
				$i=1;
				foreach($request->input('filters')['working_day'] as $var){
					$weekDays.=$var;
					if(count($request->input('filters')['working_day'])>$i++){
						$weekDays.=' or ';
					}				
				}
				$weekDays.=')';
				$query->addFilterQuery(array('key'=>'week_day', 'query'=>'days_of_week:'.$weekDays, 'tag'=>'exclude'));
			}
			if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
				$query->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$request->input('filters')['gender'], 'tag'=>'exclude'));
			}			
			if(isset($request->input('filters')['tests']) && $request->input('filters')['tests']!=""){
				$tests = "(";
				$i=1;
				foreach($request->input('filters')['tests'] as $var){
					$tests.=$var;
					if(count($request->input('filters')['tests'])>$i++){
							$tests.=' or ';
					}
				}
				$tests.=')';
				$query->addFilterQuery(array('key'=>'Tests', 'query'=>'test_id:'.$tests, 'tag'=>'exclude'));
			}
			/**
			 * Filter Home Collection facility
			 */
			if(isset($request->input('filters')['online_reports']) && $request->input('filters')['online_reports']==1){
			
				$query->addFilterQuery(array('key'=>'online_reports', 'query'=>'online_reports:'.$request->input('filters')['online_reports'], 'tag'=>'exclude'));
			}
			/**
			 * Filter Online report
			 */
			if(isset($request->input('filters')['home_collection_facility']) && $request->input('filters')['home_collection_facility']==1){
			
				$query->addFilterQuery(array('key'=>'home_collection_facility', 'query'=>'home_collection_facility:'.$request->input('filters')['home_collection_facility'], 'tag'=>'exclude'));
			}
			/****
			 * Applying sorting..
			 */
			$query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
			
			/**
			 * Paggination 
			 */
			$page = 1;
			$limit = 25;
			if($request->input('page')) {
				$page = $request->input('page');
			}
			$start=($page - 1)*$limit;
			$query->setStart($start);
			$query->setRows($limit);
			
			$resultset = $this->client->select($query);
			$paginator = [
				'result' => $resultset,
				'count' => $resultset->getNumFound(),
				'limit' => $limit,
				'page' => $page,
				'query' => $query
			];
						
			$resultset = $resultset->getData()['response']['docs'];
			# Storing session valeue
			if($request->input('filters')){
				$request->session()->put('filters', $request->input('filters'));
			}else{
				$filters = [
					'max_distance' => $settings->value.'KM',
					'rating' =>'',
					'sorting' =>'relevence'
				];
				$request->session()->put('filters', $filters);
			}
			$results =[
				'results'=>$resultset,
				'location'=>session('address')['address_one'],
				'latitude'=>session('address')['latitude'],
				'longitude'=>session('address')['longitude'],
				'locality'=>session('address')['locality'],
				'city_name'=>session('address')['city_name'],
				'state_name'=>session('address')['state_name'],
				'country_name'=>session('address')['country_name'],
				'pincode'=>session('address')['pincode'],
				'tests' => \App\Model\Test::take(5)->get(),
				'test_all' => \App\Model\Test::skip(5)->take(134)->get(),
				'default_distance' => $settings->value,
				'paginator' =>$paginator
			];
		} 
		catch (\Solarium\Exception $e) {
			$response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
			$response = json_encode($response);
			return $response;
		}
		return view('us::reviews.diagnostic', $results);
    }
	public function reviews(Request $request){
		if($request->doctor){
			$profile = \App\Model\Doctor::find($request->doctor);
			$result = [
				'profile'=> $profile,
				'star_count' =>($profile->rating_five_star_count+$profile->rating_four_star_count+$profile->rating_three_star_count+$profile->rating_two_star_count
				+ $profile->rating_one_star_count),
				'reviews' => \App\Model\Doctor::find($request->doctor)->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100)
			];
		}elseif($request->hospital){
			$profile = \App\Model\Hospital::find($request->hospital);
			$result = [
				'profile'=>$profile,
				'star_count' =>($profile->rating_five_star_count+$profile->rating_four_star_count+$profile->rating_three_star_count+$profile->rating_two_star_count
				+ $profile->rating_one_star_count),
				'reviews' => \App\Model\Hospital::find($request->hospital)->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100)
			];
		}elseif($request->diagnostic){
			$profile = \App\Model\Diagnostic::find($request->diagnostic);
			$result = [
				'profile'=>$profile,
				'star_count' =>($profile->rating_five_star_count+$profile->rating_four_star_count+$profile->rating_three_star_count+$profile->rating_two_star_count
				+ $profile->rating_one_star_count),
				'reviews' => \App\Model\Diagnostic::find($request->diagnostic)->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100)
			];
		}else{
			abort(404);
		}
		return view('us::reviews.reviews', $result);
	}
}
