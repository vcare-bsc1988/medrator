<?php
namespace App\Sites\US\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Contracts\Provider;
use App\SocialAccount;
use App\User;
use App\Http\Requests;
use Socialite;
use Hash;
use Illuminate\Support\Facades\Mail;
class SocialAuthController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function auth($provider)
    {
        $user = $this->create(Socialite::driver($provider));
        auth()->login($user);
        return redirect()->to('/home');
    }
    public function create(Provider $provider)
    {
        $providerUser = $provider->user();
        $providerName = class_basename($provider);
        $account = SocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
			# wordpress code start here
			$wp_login = \wp_signon(array('user_login'=>$providerUser->getEmail(),'user_password'=>'123456'));       
            return $account->user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName
            ]);
            $user = User::whereEmail($providerUser->getEmail())->first();
            if (!$user) {
                $profile = new \App\Model\Users([
					'name'=>$providerUser->getName(),
					'image'=>$providerUser->getAvatar(),
				]);
				### wordpress registration start here
				$wp_user = \wp_insert_user([
					'user_pass'=>'123456',
					'user_login'=>$providerUser->getEmail(),
					'user_email'=>$providerUser->getEmail(),
					'role'=>'contributor'
				]);
				### Finished
				User::insert([
                    'id' => $wp_user,
					'email' => $providerUser->getEmail(),
                ]);
				$user = \App\User::find($wp_user);
				$user->roles()->attach(3);
				$profile->user()->associate($user);
				$profile->save();
				/**
				 * Sending welcome mail to user
				 */
				$data = [
					'email' => $user->email,
					'user_name'=>$user->user->name
				];
				Mail::send('email.welcome',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('Welcome Email');
				});
            }
            $account->user()->associate($user);
            $account->save();
			# wordpress code start here
			$wp_login = \wp_signon(array('user_login'=>$providerUser->getEmail(),'user_password'=>'123456'));
			return $user;
        }
    }    
}
