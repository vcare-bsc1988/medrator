$(document).ready(function(){
	$('.name-search-box').change(function(){if($(this).val()==""){$(this).val(""); 
    $(".name-suggesstion-box").hide();
	$("#filterForm").submit();}})
	
    $("#search-box").keyup(function(){
		
        $.ajax({
            type: "POST",
            url: "http://139.59.24.155/find/autocomplet",
            data:'keyword='+$(this).val(),
            beforeSend: function(){
                $("#search-box").css("background","#FFF url(public/images/LoaderIcon.gif) no-repeat 165px");
            },
            success: function(data){
                $("#suggesstion-box").show();
                $("#suggesstion-box").html(data);
                $("#search-box").css("background","#FFF");
            },
            error:function(data){
                console.log(data);
            }
        });
    });
    $("#search").click(function() { 
        var location = $("#location").val();
        var search_box = $("#search-box").val();
        if(location == ''){ 
            $("#location").focus();
            return false;
        }else if(search_box == '') {
            $("#search-box").focus();
            return false;
        }else{
            $("#search-form").submit();
        }
    });
    $("#claim-search").click(function() { 
        var search_box = $("#search-box").val();
		if(search_box == '') {
            $("#search-box").focus();
            return false;
        }else{
            $("#search-form").submit();
        }
    });
    $(document).mouseup(function (e)
	{
		var container = $("#suggesstion-box");
		 var container1 = $("#search-box");

		if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0 || !container1.is(e.target)) // ... nor a descendant of the container
		{
			container.hide();
		}
	});
	$(".name-search-box").keyup(function(){ 
        $.ajax({
            type: "POST",
            url: "http://139.59.24.155/name/autocomplet",
            data:'keyword='+$(this).val(),
            beforeSend: function(){
                $(".name-search-box").css("background","#FFF url(public/images/LoaderIcon.gif) no-repeat 165px");
            },
            success: function(data){
                $(".name-suggesstion-box").show();
                $(".name-suggesstion-box").html(data);
                $(".name-search-box").css("background","#FFF");
            },
            error:function(data){
                console.log(data);
            }
        });
    });
    
});

function selectCountry(val) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
}
function selectName(val) {
    $(".name-search-box").val(val);
    $(".name-suggesstion-box").hide();
	$("#filterForm").submit();
}
