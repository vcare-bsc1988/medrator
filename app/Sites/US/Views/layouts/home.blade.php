<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<!-- Mirrored from tradeskerala.com/medical/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 26 Aug 2016 06:44:57 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Medical Theams">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Medrator</title>
    <link rel="stylesheet" href="{{ asset("public/css/inputTags.css") }}">
    <link rel="stylesheet" href="{{ asset("public/css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset("public/css/custom-variation2.css") }}">
    <link rel="stylesheet" href="{{ asset("public/css/responsive.css") }}"> 
    <link rel="stylesheet" href="{{ asset("public/css/style.css") }}"> 
    

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lora:400,400italic' rel='stylesheet' type='text/css'>
    
    <!-- choosen js and style START -->
    <link href="{{ asset("public/css/chosen.css") }}" rel="stylesheet" type="text/css">
    <!-- wizard style/js START -->
    <link href="{{ asset("public/css/jquery.wizard.css") }}" rel='stylesheet' type='text/css'>
    <!-- Range Slider style/script START-->
    <link rel="stylesheet" href="{{ asset("public/js/rangeslider/range_picker.min.css") }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.min.js"></script>
    <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
    
    <script src="{{ asset("public/js/vendor/jquery-min.js") }}"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset("public/js/custom.js") }}"></script>
    <script src="{{ asset("public/js/jquery.validate.min.js") }}"></script>
	
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC00fGCV5rT-LH9xZZVwjnTn39jW64URLM&amp;libraries=places"></script>
    <script src="{{ asset('public/js/jquery.geocomplete.min.js') }}"></script>
    
</head>
<body class="in-align">
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="outer-sync var2">
        <!--Content start -->
        @yield('us::content')
        <!--Finished-->                
    </div><!-- /.outer-sync -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.js"></script>
    <script src="{{ asset("public/js/vendor/modernizr.custom.68477.js") }}"></script>
    <script src="{{ asset("public/js/vendor/bootstrap.min.js") }}"></script>
    <script src="{{ asset("public/js/plugins.js") }}"></script>
    <script src="{{ asset("public/js/vendor/jquery.bxslider.js") }}"></script>
    <script src="{{ asset("public/js/main.js") }}"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	
    <!--<script type="text/javascript" src="{{ asset('public/js/bootstrap-datetimepicker.js') }}" charset="UTF-8"></script>-->
    <script type="text/javascript" src="{{ asset("public/js/rangeslider/range_picker.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("public/js/jquery.selectbox-0.2.js") }}"></script>
    <script type="text/javascript">
    "use strict";
    $(function(){
        $(".chosen").chosen();
        $('.search-advanced-tabs li.active i').removeClass('fa-plus');
        $('.search-advanced-tabs li.active i').addClass('fa-minus');
        $('.search-advanced-tabs li').click(function(e)
        {
            e.preventDefault();
            $(this).find('i').removeClass('fa-plus');
            $(this).find('i').addClass('fa-minus');
            $(this).siblings().find('i').removeClass('fa-minus');
            $(this).siblings().find('i').addClass('fa-plus');
        })
    })
    $( ".datepicker" ).datepicker({
        changeMonth: true,
        changeYear: true,
		beforeShow: function(input, obj) {
			$(".datepicker").after($(input).datepicker('widget'));
		}
    }); 

    $(function(){
        $('#messge-modal').modal();
        /* login sign-up script start */
        $('.signin-popup').click(function(){
                $('.fp-form').hide();
                $('.fp-head').hide();
                $('.sign-form').hide();
                $('.reg-head').hide();	
                $('.log-head').fadeIn('slow');		
                $('.log-form').fadeIn('slow');
        })

        $('.s-btn').click(function(){
                $('.fp-form').hide();
                $('.fp-head').hide();
                $('.log-form').hide();
                $('.log-head').hide();
                $('.reg-head').fadeIn('slow');
                $('.sign-form').fadeIn('slow');			
        })
        $('.si-btn').click(function(){
                $('.fp-form').hide();
                $('.fp-head').hide();
                $('.sign-form').hide();
                $('.reg-head').hide();
                $('.log-head').fadeIn('slow');
                $('.log-form').fadeIn('slow');			
        })
        $('.fp-btn').click(function(){
                $('.log-head').hide();
                $('.log-form').hide();
                $('.fp-head').fadeIn('slow');
                $('.fp-form').fadeIn('slow');			
        })
        $('.btl-btn').click(function(){
                $('.fp-head').hide();
                $('.fp-form').hide();
                $('.log-head').fadeIn('slow');
                $('.log-form').fadeIn('slow');			
        })
        /* login sign-up script end */
        $(".chosen").chosen();
        $("#number_range").rangepicker({

        type:'double',
        startValue: 0,
        endValue: 100,
            translateSelectLabel: function(currentPosition, totalPosition) {
                return parseInt(100 * (currentPosition / totalPosition));
            }
        });
        $('.search-advanced-tabs li.active i').removeClass('fa-plus');
        $('.search-advanced-tabs li.active i').addClass('fa-minus');
        $('#entity-container').on('click','.search-advanced-tabs li',function()
        {

                $(this).find('i').removeClass('fa-plus');
                $(this).find('i').addClass('fa-minus');
                $(this).siblings().find('i').removeClass('fa-minus');
                $(this).siblings().find('i').addClass('fa-plus');
        })
    })
    /**
    * Note error type
    * type = 1 for login
    * type = 2 registration
    * type = 3 forgot password
    * type = 4 for after registration success.
    */
    $(document).ready(function(){
        $("#search").click(function() {
            var location = $("#location").val();
            var search_box = $("#search-box").val();
            if(location == ''){ 
                $("#location").focus();
            }else if(search_box == '') {
                $("#search-box").focus();
            }else{
                $("#search-form").submit();
            }
        });
        var popup = "{{ session('status') }}";
        var type = "{{ session('type') }}";
        if(popup && type==1){
            $('#signin-form').modal('show');
        }
        $('#sign-in').click(function(){ 
            $('#alert').css("display","none");
			$('.alert').css("display","none");
            $(".form-group").removeClass("has-error");
        });
        if(popup && type==2){
            $('.fp-form').hide();
            $('.fp-head').hide();
            $('.log-form').hide();
            $('.log-head').hide();
            $('#signin-form').modal();
            $('.reg-head').fadeIn('slow');
            $('.sign-form').fadeIn('slow');		
        }
        /**
         * Showing login popup on any pages other than home.
         */
        var login_popup = "{{ session('login_popup') }}";
        if(login_popup){
            $('#signin-form').modal('show');	
        }
        if(popup && type==3){
                    
            $('.reg-head').hide();
            $('.sign-form').hide();	
            $('.log-form').hide();
            $('.log-head').hide();
            $('#signin-form').modal();	
            $('.fp-form').fadeIn('slow');
            $('.fp-head').fadeIn('slow');    
        }
        if(popup && type==4){  
            $('#message-popup').modal();
        }
		/****
		 * For showing password reset popup.
		 *
		 */
		var popup_type = "{{ isset($popup_type)?$popup_type:0}}";
		if(popup_type==5){  
			/* $('.reg-head').hide();
            $('.sign-form').hide();	
            $('.log-form').hide();
            $('.log-head').hide(); */
            $('#reset-pwd').modal();
			/* $('.fp-form').fadeIn('slow');
            $('.fp-head').fadeIn('slow'); */  
        }
		
    })
	</script>
</body>
</html>