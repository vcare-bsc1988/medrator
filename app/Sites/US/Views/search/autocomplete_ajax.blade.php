@if(!empty($result))
    <ul id="country-list">
    @foreach($result as $temp)
        <li onClick="selectCountry('{{ addslashes($temp->name) }}');">{{ $temp->name }}</li>
    @endforeach 
    </ul>
@endif