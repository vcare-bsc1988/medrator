<aside>
        <div class="col-xs-12 col-md-3" id="filter_area">
				<!-- Clear filter Part START -->
                <div class="col-xs-12 padding-0" align="right"><p><a href="JavaScript:Void(0);"  id="clear-filters"> Clear Filters</a></p></div><!-- Clear filter Part END -->
		        <!-- Name Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p>Name</p>
					<div class="col-xs-12 col-md-12 padding-0">
                        {{ Form::text('filters[name]',(isset(Session::get('filters')['name']))?stripslashes(Session::get('filters')['name']):null,['class'=>'form-control sinput name-search-box', 'placeholder'=>'Enter Name','autocomplete'=>'off']) }}
                        <div class="name-suggesstion-box"></div>
						<input type="hidden" disabled="disabled" name="search_result" id="search_result" value="{{ json_encode($results) }}">
                    </div>
                </div><!-- Name Part END -->
                <!-- Distance Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p>Distance</p>
                    <input type="text" name="filters[max_distance]" id="distance" readonly style="border:0; color:#f6931f; font-weight:bold;">
                    <div id="slider-range-distance"></div>
                </div><!-- Distance Part END -->

                <!-- Rating Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <input type="hidden" id="star" value="<?=(isset(Session::get('filters')['rating']))?Session::get('filters')['rating']:0?>" name="filters[rating]">    
                    <p class="margin-bottom-10">Rating</p>
                    <div class="squaredOne margin-left-0">
                        <input type="checkbox" value="5" <?=(Session::get('filters')['rating']==5)?'checked':''?> id="squaredFive" name="check" />
                            <label for="squaredFive"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="4" <?=(Session::get('filters')['rating']==4)?'checked':''?> id="squaredFour" name="check" />
                            <label for="squaredFour"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="glyphicon glyphicon-star-empty"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="3" <?=(Session::get('filters')['rating']==3)?'checked':''?> id="squaredThree" name="check"/>
                            <label for="squaredThree"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                        <input type="checkbox" value="2" <?=(Session::get('filters')['rating']==2)?'checked':''?> id="squaredTwo" name="check" />
                            <label for="squaredTwo"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="1" <?=(Session::get('filters')['rating']==1)?'checked':''?> id="squaredOne" name="check"  />
                            <label for="squaredOne"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i><i class="glyphicon glyphicon-star-empty"></i></label>
                        
                </div><!-- Rating Part END -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p class="margin-bottom-10">Consultation Fee (<i class="fa fa-inr"></i>)</p>
                    <input type="hidden" id="min_fee" <?php if(!empty(Session::get('filters')['min_fee'])){?>value ="<?=Session::get('filters')['min_fee']?>" <?php } ?> name="filters[min_fee]">
                    <input type="hidden" id="max_fee" <?php if(!empty(Session::get('filters')['max_fee'])){?>value ="<?=Session::get('filters')['max_fee']?>" <?php } ?>  name="filters[max_fee]">
                    <input type="text" id="fee" readonly style="border:0; color:#f6931f; font-weight:bold;">
                    <div id="slider-range-fee"></div>
                </div>
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p>Minimum Experience</p>
                    <input type="text" name="filters[max_experience]" id="experience" readonly style="border:0; color:#f6931f; font-weight:bold;">
                    <div id="slider-range-experience"></div>
                </div>
                <!-- Working Day Part START -->							
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p class="margin-bottom-10">Working Day</p>
                    <label class="nms">Monday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="1" <?=(isset(Session::get('filters')['working_day'][0]) && Session::get('filters')['working_day'][0]==1)?'checked':''?> id="squaredMon" class="day-of-week"  name="filters[working_day][0]"/>
                        <label for="squaredMon"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Tuesday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="2" <?=(isset(Session::get('filters')['working_day'][1]) && Session::get('filters')['working_day'][1]==2)?'checked':''?> id="squaredTh" class="day-of-week" name="filters[working_day][1]"/>
                        <label for="squaredTh"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Wednesday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="3" <?=(isset(Session::get('filters')['working_day'][2]) && Session::get('filters')['working_day'][2]==3)?'checked':''?> id="squaredWed" class="day-of-week" name="filters[working_day][2]"/>
                        <label for="squaredWed"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Thursday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="4" <?=(isset(Session::get('filters')['working_day'][3]) && Session::get('filters')['working_day'][3]==4)?'checked':''?> id="squaredThr" class="day-of-week" name="filters[working_day][3]"/>
                        <label for="squaredThr"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Friday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="5" <?=(isset(Session::get('filters')['working_day'][4]) && Session::get('filters')['working_day'][4]==5)?'checked':''?> id="squaredFri" class="day-of-week" name="filters[working_day][4]"/>
                        <label for="squaredFri"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
                    <label class="nms">Saturday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="6" <?=(isset(Session::get('filters')['working_day'][5]) && Session::get('filters')['working_day'][5]==6)?'checked':''?> id="squaredSat" class="day-of-week" name="filters[working_day][5]"/>
                        <label for="squaredSat"></label>
                    </div>
                    <hr class="pull-left col-xs-12 divide"/>
					<label class="nms">Sunday</label>
                    <div class="squaredOne margin-right-0 pull-right">
                        <input type="checkbox" value="1" <?=(isset(Session::get('filters')['working_day'][6]) && Session::get('filters')['working_day'][6]==1)?'checked':''?> id="squaredSun" class="day-of-week" name="filters[working_day][6]"/>
                        <label for="squaredSun"></label>
                    </div>
                </div><!-- Working Day Part END -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p class="margin-bottom-10">Gender</p>
                    <div class="col-xs-6 col-md-6 padding-0">
                        <div class="cirlceddOne g1 pull-left">
                            <input type="checkbox" style="visibility:visible;" value="1" <?=(@Session::get('filters')['gender']==1)?'checked':''?> class="gender" id="circledOne" name="filters[gender]"/>
                            <label for="circledOne"></label>
                        </div>								
                        <label class="nms pull-left">Male</label>
                    </div>
                    <div class="col-xs-6 col-md-6 padding-0">
                        <div class="circledOne g1 pull-left">
                            <input type="checkbox" style="visibility:visible;" value="2" <?=(@Session::get('filters')['gender']==2)?'checked':''?> class="gender" id="circledTwo" name="filters[gender]"/>
                            <label for="circledTwo"></label>
                        </div>								
                        <label class="nms pull-left">Female</label>
                    </div>
                </div>							
                <!-- Gender part END -->
        </div>
</aside>