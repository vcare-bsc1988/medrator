@extends('us::layouts.app')
@section('us::content')
<style>
.selectize-input{width:250px !important;}
.ui-draggable, .ui-droppable {
	background-position: top;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Search Doctors</div>
                    <div class="panel-body">
                            <div class="row">
                                <div class="search">
                                {{Form::open(['url' => 'search', 'method' => 'get', 'class' => 'navbar-form', 'rol'=>'search'])}}
                                        <div class="form-group">
                                                {{ Form::select('state', \App\Model\States::where('country_id',101)->lists('name','id')->toArray(), (Session::has('city')?\App\Model\States::where('id',Session::get('city'))->first()->id:null), ['class' => 'form-control','id'=>'city'])}}
                                        </div>
                                        <div class="form-group" style="width: 240px;">
                                                <select id="searchbox" name="q" placeholder="Search products or categories..." class="form-control"></select>
                                        </div>
                                {{ Form::close() }}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                            <div class="col-md-3">
                                @include('us::search.diagnostic_filter')
                            </div>
                            <div class="col-md-9">
                                @if(!empty($results))
                                @foreach($results as $result)
                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                      <div class="thumbnail">
                                        <div class="caption">
                                            <h4 class="pull-right"></h4>
                                            <h4><a href="{{url('diagnostic_center/'.$result['name_slug'])}}">{{$result['name']}}</a></h4>
                                            <p>{{$result['state_name']}}</p>
                                            <p>{{$result['country_name']}}</p>
                                        </div>
                                        <div class="ratings">
                                            <p class="pull-right">{{$result['review_count']}} {{ str_plural('review', $result['review_count'])}}</p>
                                            <p>
                                                @for ($i=1; $i <= 5 ; $i++)
                                                <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                                @endfor
                                                {{ number_format($result['rating'], 1)}} stars
                                            </p>
                                        </div>
                                      </div>
                                    </div>
                                @endforeach
                                @else
                                <div class="col-md-9">
                                    <h1>No result found.</h1>
                                </div>
                                @endif
                            </div>


                            </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#slider-range-fee" ).slider({
      range: true,
      min: 1,
      max: 500,
      values: [<?=(!empty(Session::get('filters')['min_fee'])?Session::get('filters')['min_fee']:1)?>, <?=!empty(Session::get('filters')['max_fee'])?Session::get('filters')['max_fee']:300?>],
      slide: function( event, ui ) {
            $( "#fee" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            $( "#min_fee" ).val( ui.values[ 0 ] );
            $( "#max_fee" ).val( ui.values[ 1 ] );
            $("#filterForm").submit();
      }
    });
    $( "#fee" ).val( "$" + $( "#slider-range-fee" ).slider( "values", 0 ) + " - $" + $( "#slider-range-fee" ).slider( "values", 1 ) );
    
    $(".tests").click(function () {
        $("#filterForm").submit();
    })
    
    $("#star4").click(function () {
        $( "#star" ).val( 4 );
        $("#filterForm").submit();
    })
    $("#star3").click(function () {
        $( "#star" ).val( 3 );
        $("#filterForm").submit();
    }) 
    $("#star2").click(function () {
        $( "#star" ).val( 2 );
        $("#filterForm").submit();
    }) 
    $("#star1").click(function () {
        $( "#star" ).val( 1 );
        $("#filterForm").submit();
    }) 
    $( "#slider-range-distance" ).slider({
      value:<?=!empty(Session::get('filters')['max_distance'])?Session::get('filters')['max_distance']:0?>,
      min: 0,
      max: 50,
      step: 1,
      slide: function( event, ui ) {
        $( "#distance" ).val( ui.value );
        $("#filterForm").submit();
      }
    });
    $( "#distance" ).val( $( "#slider-range-distance" ).slider( "value" ) );
  
  } );
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
        $("#city").change(function () {
            var id =$('#city').val();
            var city = $("#city option[value='"+id+"']");
            $.ajax({
                type: "POST",
                url: "{{ url('session/stor_city') }}",
                //data:'city='+city.text(),
                data:'city='+id,
                success: function(data){ 
                    $("#content").html(data);
                }
            });
        })
    });
  </script>
@endsection
