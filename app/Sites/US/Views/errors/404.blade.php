@extends('us::layouts.error-layout')

@section('us::content')
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')							 
<div class="first-section p82-topbot">
	<div class="container">
		<div class="row text-center">
			<img src="{{ asset("public/images/404.png") }}"/>
			<p>Sorry, but the page you are looking for can't be found.It might be changed or deleted.</p>
			
		</div><!-- /.row -->
	</div><!-- /.container -->   
</div>
<!--==================================
		footer parts starts here
	  ==================================-->     
	  
<footer class="footer p82-topbot">
<div class="container">
<div class="row">
	<div class="col-sm-6 col-md-3 col-lg-3 all-need">
		<h2>Address</h2>
			 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			<div class="faddress">
		   <span>Address :</span> Medical Theme<br>
			22, North Avenue post here<br>
			New York city 55862<br>
		   <span>Call :</span> +8856 9984 222<br>
		   <span>Email :</span> <a href="mailto:info@medicaltheme.com">info@medicaltheme.com</a>
		</div>
	</div><!-- col-sm-3 -->
	<div class="col-sm-6 col-md-3 col-lg-3 all-need list-styles">
		<h2>Service</h2>
		<ul>
                    @foreach(\App\Model\Specialties::select('id','name')->orderBy('is_top', 'desc')->skip(0)->take(7)->get() as $specialty)
                    <li><a href="{{ url('us/find/doctor')}}/{{ session('address')['state_name'] }}/{{ $specialty->name }}">{{ $specialty->name }}</a></li>
                    @endforeach 
		</ul>
	</div>
	<div class="col-sm-6 col-md-3 col-lg-3 all-need list-styles">
		<h2>Medrator</h2>
		<ul>
			<li><a href="{{ url('us/about-us')}}" target="new">About Us</a></li>
			<li><a href="{{ url('us/disclaimer')}}" target="new">Disclaimer</a></li>
			<li><a href="{{ url('us/copyright')}}" target="new">Copyright</a></li>
			<li><a href="{{ url('us/privacy-policy')}}" target="new">Privacy Policy</a></li>
			<li><a href="{{ url('us/healt-tools')}}" target="new">Healt Tools</a></li>
			<li><a href="{{ url('us/organ-donation')}}" target="new">Organ donation with third part link</a></li>
			<li><a href="{{ url('us/medical-tourism')}}" target="new">Medical Tourism Section</a></li>
			<li><a href="{{ url('us/buy-insurance')}}" target="new">Buy Insurance / Fitness product</a></li>	
		</ul>
	</div>
	<div class="col-sm-6 col-md-3 col-lg-3 all-need list-styles">
		<form id="newsletter-form" method="post">
			 {{ csrf_field() }}
			<h2>news letter</h2>
			<div class="news-letter">
				<input type="text" placeholder="Enter Email Address" name="email" id="mce-EMAIL"/>
				<div id="mce-responses" class="clear">
					<div class="block-hide response" id="mce-error-response"></div>
					<div class="block-hide response" id="mce-success-response"></div>
				</div>   
				<p><a href="{{ url('us/report-errors') }}">Report Errors</a></p>
				<button type="button" name="subscribe" id="mc-embedded-subscribe" style="background:#000;color:white;">go</button>
			</div><!-- /.new-letter -->
			<span id="success-msg" style="display:hidden"></span>
		</form>
	</div><!-- col-sm-3 -->
</div><!-- ./row -->
</div><!-- /.container --> 
	   
</footer>
<div class="copyrights text-left">
	<div class="container">
		<span class="pull-left white">Medrator © 2016 | All Rights Reserved</span>
		<ul class="nav navbar-nav pull-right-large social-icons">
			<li class="pull-left-small"><a href="javascript:;" class="padding-0 white"><i class="fa fa-twitter"></i></a></li>
			<li class="pull-left-small"><a href="javascript:;" class="padding-0 white"><i class="fa fa-facebook"></i></a></li>
			<li class="pull-left-small"><a href="javascript:;" class="padding-0 white"><i class="fa fa-linkedin"></i></a></li>
		</ul>
	</div>
</div><!-- /.copy-rights -->
@endsection

