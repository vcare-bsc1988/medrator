{{ Form::model($user->diagnostic,array('url' =>'account/save-diagnostic','class'=>'form-horizontal', 'files' => true)) }}{{ csrf_field() }}  
{{ Form::hidden('id', null) }} 
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		<div class="col-md-4 col-xs-12">
				<label class="control-lebel" style="margin-top:8px;">Name</label>
		</div>
		<div class="col-md-6 col-xs-12">
				{{ Form::text('name', null, ['class' => 'form-control']) }}
		</div>
</div>
<div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Address</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::text('address_one', null, ['class'=>'form-control location-input','id'=>'geocomplete']) }}
		<div class="location-details">
			{{ Form::hidden('latitude', null, ['data-geo'=>'lat']) }}
			{{ Form::hidden('longitude', null, ['data-geo'=>'lng']) }}
			{{ Form::hidden('locality', null, ['data-geo'=>'locality']) }}
			{{ Form::hidden('city_name', null, ['data-geo'=>'locality']) }}
			{{ Form::hidden('state_name', null, ['data-geo'=>'administrative_area_level_1']) }}
			{{ Form::hidden('country_name', null, ['data-geo'=>'country_short']) }}
		</div>
	</div>
</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Phone Number</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::number('phone', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'12']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('home_collection_facility') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">Home Collection Facility</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::checkbox('home_collection_facility', 1,null,['class'=>'squaredFives']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('online_bookings') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">Online Bookings</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::checkbox('online_bookings', 1,null,['class'=>'squaredFives']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('online_reports') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">Online Reports</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::checkbox('online_reports', 1,null,['class'=>'squaredFives']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Tests</label>
	</div>
	<div class="col-md-6 col-xs-12">
		<div class="clear" id="content1">
			{{ Form::select('tests[]', $tests,$user->diagnostic->tests()->lists('test_id')->toArray(), ['placeholder'=>'Select Tests', 'class' => '1col active form-control','multiple'=>true]) }}
		</div>
	</div>
</div>
<div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">Upload Photo (optional)</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::file('image',['class'=>'form-control']) }}
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-md-4 col-xs-12">

	</div>							
	<div class="col-md-6 col-xs-12">
			<input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Save" />
	</div>							
</div>
{{ Form::close() }}