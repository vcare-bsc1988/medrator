{{ Form::model($user->doctor,array('url' =>'account/save-doctor','class'=>'form-horizontal', 'files' => true)) }}
{{ csrf_field() }}  
{{ Form::hidden('id', null) }} 
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		<div class="col-md-4 col-xs-12">
				<label class="control-lebel" style="margin-top:8px;">Name</label>
		</div>
		<div class="col-md-6 col-xs-12">
			{{ Form::text('name', null, ['class' => 'form-control']) }}
		</div>
</div>
<div class="form-group{{ $errors->has('about') ? ' has-error' : '' }}">
		<div class="col-md-4 col-xs-12">
				<label class="control-lebel" style="margin-top:8px;">About</label>
		</div>
		<div class="col-md-6 col-xs-12">
				{{ Form::textarea('about', null, ['class' => 'form-control','rows'=>'4']) }}
		</div>
</div>
<div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Address</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::text('address_one', null, ['class'=>'form-control location-input','id'=>'geocomplete']) }}
		<div class="location-details">
			{{ Form::hidden('latitude', null, ['data-geo'=>'lat']) }}
			{{ Form::hidden('longitude', null, ['data-geo'=>'lng']) }}
			{{ Form::hidden('locality', null, ['data-geo'=>'locality']) }}
			{{ Form::hidden('city_name', null, ['data-geo'=>'locality']) }}
			{{ Form::hidden('state_name', null, ['data-geo'=>'administrative_area_level_1']) }}
			{{ Form::hidden('country_name', null, ['data-geo'=>'country_short']) }}
		</div>
	</div>
</div>
<div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Mobile Number</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::number('mobile', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel">Gender</label>
	</div>
	<div class="col-md-6 col-xs-12">
		 {{ Form::radio('gender', 1)}} Male &nbsp;&nbsp;&nbsp;{{Form::radio('gender', 2)}} Female 
	</div>
</div>
{{--<div class="form-group{{ $errors->has('qualifications') ? ' has-error' : '' }}">
		<div class="col-md-4 col-xs-12">
				<label class="control-lebel" style="margin-top:8px;">Degree</label>
		</div>
		<div class="col-md-6 col-xs-12">
			<div id="content" class="clear">
				{{ Form::select('qualification', $qualifications, $user->doctor->qualifications->id, ['class' => '2col active form-control degree-select']) }}
			 </div>
		</div>
</div>
<div class="form-group{{ $errors->has('college') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">College Name</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::text('college', $user->doctor->qualifications->pivot->college_id, ['class'=>'form-control']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">

	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Year of Passing</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::text('year', $user->doctor->qualifications->pivot->year, ['class'=>'form-control']) }}
	</div>
</div>--}}
<div class="form-group{{ $errors->has('qualifications') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Qualification</label>
	</div>
	<div class="padding-0 col-md-8 padding-0 repeat-quali" style="position:relative;">
            @for($i=0; $i<$user->doctor->colleges()->get()->count(); $i++)
            @if($i<1)
            <div class="col-md-3 col-xs-12">
                <div id="content" class="clear">
                        {{ Form::select('degree[]', $qualifications, $user->doctor->colleges[$i]->qualification_id, ['class' => '2col active form-control']) }}
                 </div>
            </div>
            <div class="col-md-3 col-xs-12">
                    {{ Form::select('college[]', $colleges, $user->doctor->colleges[$i]->college_id, ['class'=>'form-control']) }}
            </div>
            <div class="col-md-3 col-xs-12">
                    {{ Form::select('year[]',$years, $user->doctor->colleges[$i]->year, ['class'=>'form-control']) }}
            </div>
            <div class="col-md-2">
                    <i class="cur fa fa-plus btn btn-info btn-xs add-quali" style="margin-top:10px;"></i>
            </div>
            @else
            <div class="rp-container">
                <div class="clearfix" style="float:left;width:100%;margin-top:2px;"></div>
                <div class="col-md-3 col-xs-12"><div id="content" class="clear">
                        {{ Form::select("degree[]", $qualifications, $user->doctor->colleges[$i]->qualification_id, ["class" => "2col active form-control"]) }}
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    {{ Form::select("college[]", $colleges, $user->doctor->colleges[$i]->college_id, ["class"=>"form-control"]) }}
                </div>
                <div class="col-md-3 col-xs-12">
                    {{ Form::select("year[]",$years, $user->doctor->colleges[$i]->year, ["class"=>"form-control"]) }}
                </div>
                <div class="col-md-2">
                    <i class="cur fa fa-minus btn btn-danger btn-xs remove-repeat" style="margin-top:10px;"></i>
                </div>
            </div>
            @endif
            @endfor
        </div>
</div>
<div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Speciality</label>
	</div>
	<div class="col-md-6 col-xs-12">
		<div id="content1" class="clear">
		{{ Form::select('specialties[]', $specialties,$user->doctor->specialties()->lists('speciality_id')->toArray(), ['class' => '1col  form-control active','multiple'=>true]) }}
		</div>
	</div>
</div>
<div class="form-group{{ $errors->has('reg_no') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Registration Number</label>
	</div>
	<div class="col-md-6 col-xs-12">
		 {{ Form::text('reg_no', null, ['class'=>'form-control']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('awards') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Award</label>
	</div>
	<div class="col-md-6 col-xs-12">
		 {{ Form::text('awards', null, ['class'=>'form-control']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('recognition') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Recognition</label>
	</div>
	<div class="col-md-6 col-xs-12">
		 {{ Form::text('recognition', null, ['class'=>'form-control']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Experience</label>
	</div>
	<div class="col-md-6 col-xs-12">
		 {{ Form::text('experience', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-md-4 col-xs-12">

	</div>							
	<div class="col-md-6 col-xs-12">
			<input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Update" />
	</div>							
</div>
{{ Form::close() }}