{{ Form::model($user->hospital,array('url' =>'account/save-hospital','class'=>'form-horizontal', 'files' => true)) }}{{ csrf_field() }}  
{{ Form::hidden('id', null) }} 
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		<div class="col-md-4 col-xs-12">
				<label class="control-lebel" style="margin-top:8px;">Name</label>
		</div>
		<div class="col-md-6 col-xs-12">
				{{ Form::text('name', null, ['class' => 'form-control']) }}
		</div>
</div>
<div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Address</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::text('address_one', null, ['class'=>'form-control location-input', 'placeholder'=>'Location', 'id'=>'location']) }}
		<div class="location-details">
			{{ Form::hidden('latitude', null, ['data-geo'=>'lat']) }}
			{{ Form::hidden('longitude', null, ['data-geo'=>'lng']) }}
			{{ Form::hidden('locality', null, ['data-geo'=>'locality']) }}
			{{ Form::hidden('city_name', null, ['data-geo'=>'locality']) }}
			{{ Form::hidden('state_name', null, ['data-geo'=>'administrative_area_level_1']) }}
			{{ Form::hidden('country_name', null, ['data-geo'=>'country_short']) }}
		</div>
	</div>
</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Phone Number</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::number('phone', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('no_of_doctors') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">No of Doctors</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::text('no_of_doctors', null, ['class' => 'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('no_of_beds') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">No of Beds</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::text('no_of_beds', null, ['class' => 'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('icu_facility') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">ICU Facility</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::checkbox('icu_facility', 1,null,['class'=>'squaredFives']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('diagnostic_lab_facility') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">Lab Facility</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::checkbox('diagnostic_lab_facility', 1,null,['class'=>'squaredFives']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">Cashless Mediclaim</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::checkbox('cashless_mediclaim', 1,null,['class'=>'squaredFives']) }}
	</div>
</div>
<div class="form-group{{ $errors->has('24_hours_emergency') ? ' has-error' : '' }}">
    <div class="col-md-4 col-xs-12">
        <label class="control-lebel" style="margin-top:8px;">24 Hours Emergency</label>
    </div>
    <div class="col-md-6 col-xs-12">
                                                {{ Form::checkbox('24_hours_emergency', 1,null,['class'=>'squaredFives']) }}
    </div>
</div>
<div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
			<label class="control-lebel" style="margin-top:8px;">Speciality</label>
	</div>
	<div class="col-md-6 col-xs-12">
		<div class="clear" id="content1">
		{{ Form::select('specialties[]', $specialties,$user->hospital->specialtyList()->lists('speciality_id')->toArray(), ['class' => '1col active form-control','multiple'=>true]) }}
		</div>
	</div>
</div>
<div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
	<div class="col-md-4 col-xs-12">
		<label class="control-lebel" style="margin-top:8px;">Upload Photo (optional)</label>
	</div>
	<div class="col-md-6 col-xs-12">
		{{ Form::file('image',['class'=>'form-control']) }}
	</div>
</div>
<hr>
<div class="form-group">
	<div class="col-md-4 col-xs-12">

	</div>							
	<div class="col-md-6 col-xs-12">
			<input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Save" />
	</div>							
</div>
{{ Form::close() }}