@if($doctor->image)
	<img src="{{ $doctor->image }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>				
@else
	<img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
@endif
	