@extends('us::layouts.app')

@section('us::content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Clinic Info</div>

                <div class="panel-body">
                    @if (session('success'))
                        <div class="flash-message">
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        </div>
                    @endif
                    @include('us::common.errors')                   
                    {{ Form::open(array('url' => 'clinic', 'method' => 'post','files' => true)) }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                            <?=$errors->first('name','<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('service') ? ' has-error' : '' }}">
                            {{ Form::label('service', 'Service') }}
                            {{ Form::select('service[]', $services, null, ['class' => 'form-control','multiple'=>true]) }}
                            <?=$errors->first('service', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('specialty') ? ' has-error' : '' }}">
                            {{ Form::label('specialty', 'Specialty') }}
                            {{ Form::select('specialty[]', $specialties, null, ['class' => 'form-control','multiple'=>true]) }}
                            <?=$errors->first('specialty', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('consultation_fee') ? ' has-error' : '' }}">
                            {{ Form::label('consultation_fee', 'Consultation Fee (optional)') }}
                            {{ Form::text('consultation_fee', null, ['class' => 'form-control']) }}
                            <?=$errors->first('consultation_fee', '<p class="help-block">:message</p>')?>
                        </div>
                       
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            {{ Form::label('phone', 'Phone (optional)') }}
                            {{ Form::text('phone', null, ['class' => 'form-control']) }}
                            <?=$errors->first('phone', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            {{ Form::label('address', 'Address') }}
                            {{ Form::text('address', null, ['class' => 'form-control']) }}
                            <?=$errors->first('address', '<p class="help-block">:message</p>')?>
                        </div>
						<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            {{ Form::label('country', 'Country') }}
                            {{ Form::select('country',['India'=>'India'],null,['class' => 'form-control']) }}
                            <?=$errors->first('country', '<p class="help-block">:message</p>')?>
                        </div>
						<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            {{ Form::label('state', 'State') }}
                            {{ Form::select('state', [' ' => 'Select State'] +\App\Model\States::where('country_id',101)->lists('name','id')->toArray(),null,['class' => 'form-control','onChange'=>'getCity(this.value)'])}}
                            <?=$errors->first('state', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            {{ Form::label('city', 'City') }}
                            {{ Form::select('city', [' '=>'Select City'], null,['id'=>'city-list','class' => 'form-control']) }}
                            <?=$errors->first('city', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('pincode') ? ' has-error' : '' }}">
                            {{ Form::label('pincode', 'Pincode') }}
                            {{ Form::text('pincode', null, ['class' => 'form-control']) }}
                            <?=$errors->first('pincode', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            {{ Form::label('image', 'Upload Photo (optional)') }}
                            {{ Form::file('image') }}</br></br>
                            <?=$errors->first('image', '<p class="help-block">:message</p>')?>
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Save!') }}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function getCity(val) {
    $.ajax({
        type: "POST",
        url: "{{ url('get_city') }}",
        data:'state_id='+val,
        success: function(data){ 
        $("#city-list").html(data);
        }
    });
}
</script>
@endsection
