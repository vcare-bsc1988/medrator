@extends('us::layouts.app')
@section('us::content')
<style>
.selectize-input{width:250px !important;}
.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{display:table-row}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
.process-step{display:table-cell;text-align:center;position:relative}
.process-step p{margin-top:4px}
.btn-circle{width:80px;height:80px;text-align:center;font-size:12px;border-radius:50%}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Search Doctor/Diagnostic/Hospital and Clinic</div>
				<div class="panel-body">
					<div>
						 <div class="row">
							<div class="search" style="vertical-align: middle;">
							{{Form::open(['url' => 'search', 'method' => 'get', 'class' => 'navbar-form', 'id'=>'search-form', 'rol'=>'search'])}}
								<div class="form-group">
									{{ Form::select('state', \App\Model\States::where('country_id',101)->lists('name','id')->toArray(),$cityId,['id'=>'city','class' => 'form-control'])}}
								</div>
								<div class="form-group" style="width: 240px;">
									<select id="searchbox" name="q" placeholder="Doctors, Clinics, Hospitals" class="form-control"></select>
								</div>
							{{ Form::close() }}
							</div>
						</div>
						<br><br><br><br><br>
						 <div class="row">
						  <div class="process">
						   <div class="process-row nav nav-tabs">
							<div class="process-step">
							 <button type="button" class="btn btn-info btn-circle" data-toggle="tab" href="#menu1">Doctors</button>
							</div>
							<div class="process-step">
							 <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu2">Hospitals/Clinics</button>
							</div>
							<div class="process-step">
							 <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu3">Diagnostic Centers</button>
							</div>
							<!--<div class="process-step">
							 <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu4">Diagnostic Centers</i></button>

							</div>
							<div class="process-step">
							 <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu5"><i class="fa fa-check fa-3x"></i></button>
							</div>-->
						   </div>
						  </div>
						  <div class="tab-content" id="content">
						   <div id="menu1" class="tab-pane fade active in">
							<table cellpadding="10" cellpadding="50">
								<tr>
								<?php $i=1; 
								foreach(App\Model\Service::all()->take(20) as $service):?>						
									<td><a href="{{url('search')}}/doctors/{{str_slug($service->name,'-')}}/{{$city}}">{{$service->name}}</a></td>
									<?php 
									if($i++%3==0): ?>
									</tr><tr>	
									<?php 
									endif; 								
								endforeach; ?>
								</tr>
							</table>
						   </div>
						   <div id="menu2" class="tab-pane fade">
							<table cellpadding="10" cellpadding="50">
								<tr>
								<?php $i=1; 
								foreach(App\Model\Service::all()->take(20) as $service):?>						
									<td><a href="{{url('search')}}/hospitals/{{$service->name}}/{{$city}}">{{$service->name}}</a></td>
									<?php 
									if($i++%3==0): ?>
									</tr><tr>	
									<?php 
									endif; 								
								endforeach; ?>
								</tr>
							</table>
						   </div>
						   <div id="menu3" class="tab-pane fade">
							<table cellpadding="10" cellpadding="50">
								<tr>
								<?php $i=1; 
								foreach(App\Model\Test::all()->take(20) as $test):?>						
									<td><a href="{{url('search')}}/diagnostic_centers/{{$test->name}}/{{$city}}">{{$test->name}}</a></td>
									<?php 
									if($i++%3==0): ?>
									</tr><tr>	
									<?php 
									endif; 								
								endforeach; ?>
								</tr>
							</table>
						   </div>
						   
						  </div>
						 </div>
						</div>
					
				</div>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(function(){
    $("#city").change(function () {
        var id =$('#city').val();
        var city = $("#city option[value='"+id+"']");
	$.ajax({
            type: "POST",
            url: "{{ url('session/stor_city') }}",
            //data:'city='+city.text(),
            data:'city='+id,
            success: function(data){ 
                $("#content").html(data);
            }
        });
    })
});
</script>
@endsection
