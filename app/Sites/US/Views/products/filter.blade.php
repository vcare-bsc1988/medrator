<aside>
        <div class="col-xs-12 col-md-3">
                <!-- Distance Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <p>Distance</p>
                    <!--<p>
                            <div id="number_range" style="margin-top: 10px;"></div>
                    </p>-->
                    <input type="text" name="filters[max_distance]" id="distance" readonly style="border:0; color:#f6931f; font-weight:bold;">
                    <div id="slider-range-distance"></div>
                </div><!-- Distance Part END -->

                <!-- Rating Part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                    <input type="hidden" id="star" <?php if(!empty(Session::get('filters')['star'])){?>value ="<?=Session::get('filters')['star']?>" <?php } ?> name="filters[star]">    
                    <p class="margin-bottom-10">Rating</p>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="5" @if(Session::get('filters')['star']==5) checked="1" @endif id="squaredFive" name="check" />
                            <label for="squaredFive"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="4" @if(Session::get('filters')['star']==4) checked="1" @endif id="squaredFour" name="check" />
                            <label for="squaredFour"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="3" @if(Session::get('filters')['star']==3) checked="1" @endif id="squaredThree" name="check"/>
                            <label for="squaredThree"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                        <input type="checkbox" value="2" @if(Session::get('filters')['star']==2) checked="1" @endif id="squaredTwo" name="check" />
                            <label for="squaredTwo"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i> <i class="fa fa-star grey"></i></label>
                    <hr class="pull-left col-xs-12 divide"/>
                    <div class="squaredOne margin-left-0">
                            <input type="checkbox" value="1" @if(Session::get('filters')['star']==1) checked="1" @endif id="squaredOne" name="check"  />
                            <label for="squaredOne"></label>
                    </div>
                    <label class="nms"><i class="fa fa-star grey"></i></label>
                        
                </div><!-- Rating Part END -->

                <!-- Working Day Part START -->							

                <div class="col-xs-12 padding-0 margin-bottom-30">
                        <p class="margin-bottom-10">Working Day</p>

                        <label class="nms">Monday</label>
                        <div class="squaredOne margin-right-0 pull-right">
                                <input type="checkbox" value="Male" id="squaredFive" name="check" onclick="co(this.value);" />
                                <label for="squaredFive"></label>
                        </div>
                        <hr class="pull-left col-xs-12 divide"/>
                        <label class="nms">Tuesday</label>
                        <div class="squaredOne margin-right-0 pull-right">
                                <input type="checkbox" value="Male" id="squaredSix" name="check" onclick="co(this.value);" />
                                <label for="squaredSix"></label>
                        </div>
                        <hr class="pull-left col-xs-12 divide"/>
                        <label class="nms">Wednesday</label>
                        <div class="squaredOne margin-right-0 pull-right">
                                <input type="checkbox" value="Male" id="squaredSeven" name="check" onclick="co(this.value);" />
                                <label for="squaredSeven"></label>
                        </div>
                        <hr class="pull-left col-xs-12 divide"/>
                        <label class="nms">Thursday</label>
                        <div class="squaredOne margin-right-0 pull-right">
                                <input type="checkbox" value="Male" id="squaredEight" name="check" onclick="co(this.value);" />
                                <label for="squaredEight"></label>
                        </div>
                        <hr class="pull-left col-xs-12 divide"/>
                        <label class="nms">Friday</label>
                        <div class="squaredOne margin-right-0 pull-right">
                                <input type="checkbox" value="Male" id="squaredNine" name="check" onclick="co(this.value);" />
                                <label for="squaredNine"></label>
                        </div>
                        <hr class="pull-left col-xs-12 divide"/>
                        <label class="nms">Saturday</label>
                        <div class="squaredOne margin-right-0 pull-right">
                                <input type="checkbox" value="Male" id="squaredTen" name="check" onclick="co(this.value);" />
                                <label for="squaredTen"></label>
                        </div>
                        <hr class="pull-left col-xs-12 divide"/>
                </div><!-- Working Day Part END -->

                <!-- Gender part START -->
                <div class="col-xs-12 padding-0 margin-bottom-30">
                        <p class="margin-bottom-10">Gender</p>
                        <div class="col-xs-6 col-md-6 padding-0">
                                <div class="cirlceddOne g1 pull-left">
                                        <input type="radio" value="Male" id="circledOne" name="radio" onclick="co(this.value);" checked/>
                                        <label for="squaredOne"></label>
                                </div>								
                                <label class="nms pull-left">Male</label>
                        </div>
                        <div class="col-xs-6 col-md-6 padding-0">
                                <div class="circledOne g1 pull-left">
                                        <input type="radio" value="Female" id="circledTwo" name="radio" onclick="co(this.value);" />
                                        <label for="squaredOne"></label>
                                </div>								
                                <label class="nms pull-left">Female</label>
                        </div>
                </div>							
                <!-- Gender part END -->

                <!-- Services Part START -->							

                <div class="col-xs-12 padding-0 margin-bottom-30">
                        <p class="margin-bottom-10">Services</p>								
                        <div class="squaredOne margin-left-0">
                                <input type="checkbox" value="Male" id="squaredFive" name="check" onclick="co(this.value);" />
                        <label for="squaredFive"></label>
                        </div>
                        <label class="nms">Dentist</label>
                        <hr class="pull-left col-xs-12 divide"/>
                        <div class="squaredOne margin-left-0">
                                <input type="checkbox" value="Male" id="squaredSix" name="check" onclick="co(this.value);" />
                                <label for="squaredSix"></label>
                        </div>
                        <label class="nms">Urologist</label>
                        <hr class="pull-left col-xs-12 divide"/>
                        <div class="squaredOne margin-left-0">
                                <input type="checkbox" value="Male" id="squaredSeven" name="check" onclick="co(this.value);" />
                                <label for="squaredSeven"></label>
                        </div>

                        <label class="nms">Dermatologist</label>
                        <hr class="pull-left col-xs-12 divide"/>
                        <div class="squaredOne margin-left-0">
                                <input type="checkbox" value="Male" id="squaredEight" name="check" onclick="co(this.value);" />
                                <label for="squaredEight"></label>	
                        </div>																
                        <label class="nms">Skin Specialist</label>
                        <hr class="pull-left col-xs-12 divide"/>
                        <a href="javascript:(0);" class="pull-left black" style="color:black;font-size:14px;" data-toggle="modal" data-target="#more_services">View More <i class="fa fa-angle-right"></i></a>
                </div><!-- Services Part END -->   
        </div>
</aside>