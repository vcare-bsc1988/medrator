<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Claim Request</title>
</head>
<body>
<p>Dear Member,<br></p>
<h1></h1>
<p>Your request for claiming profile of  {{ $data['entity_name'] }} on Medrator has been successfully submitted and is pending for approval.</p>
<h1></h1>
<p>We appreciate your effort as it helps community to find best doctors!</p>
<h1></h1>
<p>Best Regards,<br>Medrator Team</p>
</body>
</html>