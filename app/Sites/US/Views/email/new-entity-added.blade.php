<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Profile Request</title>
</head>
<p>Dear Member,</p>
<h1></h1>
<p>Your request to add {{ $data['entity_name'] }} on Medrator has been successfully received and is pending for approval.</p>
<h1></h1>
<p>We appreciate your contribution as it helps community to find best doctors!</p>
<h1></h1>
<p>Best Regards,<br>Medrator Team</p>
<body>
</body>
</html>