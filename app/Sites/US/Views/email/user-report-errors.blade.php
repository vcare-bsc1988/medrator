<html>
    <body>
		<p>Dear member,</p>
		<h1></h1>
		<p>Thank you for reporting the error on page of {{ $data['entity_name'] }}. </p>
		<h1></h1>
		<p>The same has been notified to the concerned department and it will correct the error shortly. We appreciate your effort to improve our platform.</p>
        <h1></h1>
		<P>Best Regards,<br>Medrator Team</p>
	</body>
</html>