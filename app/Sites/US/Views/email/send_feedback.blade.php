<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Feedback</title>
</head>
<body>
<p>{{ $data['user_name'] }} has feedback:<br> {{ $data['feedback_message'] }}</p>
<h1></h1>
<p>Best Regards,<br>Medrator Team</p>
</body>
</html>