@if($user->user->image)
	<img src="{{ $user->user->image }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>				
@else
	<img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
@endif
	<div class="loader-containers" style="display:none;"></div>
								<div class="loader"  style="display:none;">
									<i class="fa fa-spin fa-spinner loader-icon"></i>
								</div>