@extends('us::layouts.app')

@section('us::content')
<style>
    a[href="//captcha.org/captcha.html?laravel"]{opacity:0 !important;}
    </style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
                            <label for="address_one" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="geocomplete" type="text" class="form-control" name="address_one" value="{{ old('address_one') }}">

                                @if ($errors->has('address_one'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_one') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="details">
                            <input type ="hidden" value="" name="latitude" data-geo="lat" />
                            <input type ="hidden" value="" name="longitude" data-geo="lng" />
                            <input type ="hidden" value="" name="locality" data-geo="locality" />
                            <input type ="hidden" value="" name="city_name" data-geo="locality" />
                            <input type ="hidden" value="" name="state_name" data-geo="administrative_area_level_1" />
                            <input type ="hidden" value="" name="country_name" data-geo="country_short" />
                            <input type ="hidden" value="" name="pincode" data-geo="postal_code" />
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group{{ $errors->has('CaptchaCode') ? ' has-error' : '' }}">
						  <label class="col-md-4 control-label"></label>

						  <div class="col-md-6">
							{!! captcha_image_html('RegisterCaptcha') !!}
							<input type="text" class="form-control" name="CaptchaCode" id="CaptchaCode">

							@if ($errors->has('CaptchaCode'))
							  <span class="help-block">
								<strong>{{ $errors->first('CaptchaCode') }}</strong>
							  </span>
							@endif
						  </div>
						</div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                Click <a href="{{ url('us//signup') }}"><strong>here</strong> </a>if you are a docter/hospital/diagnostic center.
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC00fGCV5rT-LH9xZZVwjnTn39jW64URLM&amp;libraries=places"></script>
<script src="{{ asset('public/js/jquery.geocomplete.min.js') }}"></script>
<script>
$(function(){
    $("#geocomplete").geocomplete({
        details: ".details",
        detailsAttribute: "data-geo"
    });
});
</script>
@endsection
