<h1></h1>
<p>Your request for password reset has been successfully received. Please click on the below link to reset the password:<br>
<a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
</p>
<h1></h1>
<p>Best Regards,<br>
Medrator Team
</p>