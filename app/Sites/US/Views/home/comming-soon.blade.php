@extends('us::layouts.home')

@section('us::content')

<style>
.heading{text-align:center;}
.first-section.p82-topbot{
border-top: 3px solid #e4f0fa;}
.para{    margin-bottom: 15px;}
.btn-info{color: #fff;
    background-color: #5bc0de !important;
    border-color: #46b8da; border-radius:0px !important;}
	.calculate{padding: 12px 12px !important; font-size: 17px !important; }
	.box{ margin-left:200px;    border: 1px solid #eeeeee;}
	.button_container{margin-top:20px; }
	.subheading{  text-align:center;   background-color: #e4f0fa;     padding: 10px 10px;  margin-bottom: 20px; font-size: 20px !important;}
	.placehldr{margin-bottom:15px !important;}
	.heading2{text-align:center; font-size:22px; margin-top:20px;     font-weight: bold; margin-bottom:20px; padding-right: 18px;}
	.container{padding-right:0px !important;  padding-left:0px !important; float:none !important;}
	.bottom_heading{    margin-left: 196px; font-size: 13px;  margin-top: 10px;}
</style>

<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')							 
<div class="first-section p82-topbot">
	<div class="container ">
		<div class="row">	
			<div class="col-sm-12 col-xs-12 must member">
				<h1 class="heading">Health Tools</h1>		
				<hr style="width:50px;">
				
<div class=" col-md-8 container box">
				<form>
				<h3 class="subheading">BMI Calculator :</h3>
    <div class="form-group">
      <label class="control-label col-sm-4" for="email">Enter your height : (in cm)</label>
      <div class="col-sm-8">
        <input type="text" class="form-control placehldr" id="email" placeholder="">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4" for="pwd">Enter your weight : (in Kg)</label>
      <div class="col-sm-8">          
        <input type="text" class="form-control" id="pwd" placeholder="">
      </div>
    </div>
 
    <div class="form-group">        
      <div class="col-sm-offset-4 col-sm-8 button_container">
        <button type="button" class="btn btn-default btn-info calculate">Calculate</button>
      </div>
    </div>
	<hr style="float:left;width:100%;"/>
	<h3 class="col-sm-offset-3 col-sm-5 heading2" style="margin-top:0px;">Your BMI is 21.72</h3>
  </form>
  
   </div>
				
				<h5 class="bottom_heading"><sup>*</sup>BMI formula = weight in Kg / (height in meters)x(height in meters)</h5>
				
				<ul class="bottom_heading">BMI Range-
				<li style="list-style:none !important;">A BMI below 18.5 is considered underweight.</li>
				<li style="list-style:none !important;">A BMI of 18.5 to 24.9 is considered healthy.</li>
				<li style="list-style:none !important;">A BMI of 25 to 29.9 is considered overweight.</li>
				<li style="list-style:none !important;">A BMI of 30 or higher is considered obese.</li>
		</ul>
			</div><!-- /.col-sm-8 -->
		</div>
	</div>
</div>

@include('us::layouts.footer')

@endsection
