<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 padding-0">
        @for($i=1; $i<15; $i++)
            <div class="col-md-3 col-xs-6  text-center sublist-container">
                <a href="{{ url('us/find/doctor')}}/{{ $address['state_name'] }}/{{ $specialties[$i-1]->name }}">
				<p>
					@if(file_exists(public_path("images/med_icons/".$specialties[$i-1]->name.".png")))
						<img src="{{ asset("public/images/med_icons/".$specialties[$i-1]->name.".png") }}"/>
					@endif
				</p>
                <p>{{ $specialties[$i-1]->name}}</p>
				</a>
            </div>
        @if($i%4==0)
        </div>
        <div class="col-xs-12 padding-0"> 
        @endif
        @endfor
        <div class="col-md-3 col-xs-6  text-center sublist-container" style="margin: 30px 0px 0px 0px;"><a href="javascript:void(0);" id="other-specialties">Other Specialties</a></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#other-specialties").click(function(){ 
        $.ajax({
            type: "POST",
            url: "{{ url('get-specialties') }}",
            data:{"_token":"{{ csrf_token() }}"},
            success: function(result){ 
               $("#specilities-container").html(result);
            },
            error:function(result){
                console.log(result);
            }
        });
    });
</script>