@extends('us::layouts.home')

@section('us::content')
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')							 
<div class="first-section p82-topbot">
	<div class="container">
		<div class="row">	
			<div class="col-sm-8 col-xs-12 must member">
			<h1>Organ Donation</h1>	
			</div><!-- /.col-sm-8 -->
		</div>
	</div>
</div>
@include('us::layouts.footer')

@endsection
