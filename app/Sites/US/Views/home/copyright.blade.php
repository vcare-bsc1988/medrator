@extends('us::layouts.home')

@section('us::content')

<style>
.heading{text-align:center;}
.para{    margin-bottom: 15px;}
</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')							 
<div class="first-section p82-topbot" style="border-top: 3px solid #e4f0fa;">
	<div class="container">
		<div class="row">	
			<div class="col-sm-12  col-xs-12 must member">
			<h1 class="heading">Copyright</h1>	
			<hr style="width:50px;">
			<div class="col-sm-12 col-xs-12">

			<h3>Medrator – All Rights Reserved</h3>

			<p class="para">If you are interested in linking, quoting, or re-printing any article(s), post(s), information(s), or any piece of content from Medrator.com in whole or in part, please do read our copyright policy:</p>

			<h3>Copy Limitations</h3>

			<p class="para">Please don’t copy the whole part of the content. A short paragraph with content in quotes is acceptable with a link-back to the post at Medrator.com</p>

			
			<h3>Credit the Source</h3>

			<p class="para">You need to clearly highlight the fact that the content was derived from Medrator.com</p>

		
			<h3>Link Back</h3>

			<p class="para">You need to link back to the article/ post/ information from which you derived the excerpt.</p>
			

			<h3>Noncommercial Use</h3>

			<p class="para">If you intend to use the content for other purposes, contact us[Link this to our contact us page].</p>
			
			
			</div>
			
			
			
			</div><!-- /.col-sm-8 -->
		</div>
	</div>
</div>
@include('us::layouts.footer')

@endsection
