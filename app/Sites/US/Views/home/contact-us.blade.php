@extends('us::layouts.home')

@section('us::content')
<style>
.first-section.p82-topbot{
	border-top: 3px solid #e4f0fa;
}
p{margin-bottom:15px;}
</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')	
<script type="text/javascript">
        var markers = [
            
            {
                "title": "{{ session('address')['address_one'] }}",
                "lat": "{{ session('address')['latitude'] }}",
                "lng": "{{ session('address')['longitude'] }}",
                "description": "{{ session('address')['address_one'] }}"
            }
			,
            {
                "title": 'Sector 31, Gurugram, Haryana',
                "lat": '28.4538',
                "lng": '77.0493',
                "description": 'Sector 31, Gurugram, Haryana'
            }
		];
        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            var infoWindow = new google.maps.InfoWindow();
            var lat_lng = new Array();
            var latlngbounds = new google.maps.LatLngBounds();
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                lat_lng.push(myLatlng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title
                });
                latlngbounds.extend(marker.position);
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);

            //***********ROUTING****************//

            //Intialize the Path Array
            var path = new google.maps.MVCArray();

            //Intialize the Direction Service
            var service = new google.maps.DirectionsService();

            //Set the Path Stroke Color
            var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7',strokeWeight: 6 });

            //Loop and Draw Path Route between the Points on MAP
            for (var i = 0; i < lat_lng.length; i++) {
                if ((i + 1) < lat_lng.length) {
                    var src = lat_lng[i];
                    var des = lat_lng[i + 1];
                    path.push(src);
                    poly.setPath(path);
                    service.route({
                        origin: src,
                        destination: des,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    }, function (result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                path.push(result.routes[0].overview_path[i]);
                            }
                        }
                    });
                }
            }
        }
    </script>					 
<div class="first-section p82-topbot">
	<div class="container">
		<div class="row">	
			<div class="col-sm-12 col-xs-12 must member">
				<div class="col-md-12 text-center">
					<h1>Contact Us</h1>	
					<hr style="width:50px;">
				</div>
				<div class="col-md-8 col-xs-12 ">
					<div id="dvMap" style="width: 100%; height: 350px"></div>
				</div>
				<div class="col-md-4 col-xs-12">
					<p>
						<strong>Address</strong><br>
						Medrator, Sector 31, Gurugram, Haryana (India)
					</p>
					<p>
						<strong>General Email</strong><br>
						<a href="mailto:contactus@medrator.com">contactus@medrator.com</a>
					</p>
					<p>
						<strong>Technical Issues</strong><br>
						<a href="mailto:administrator@medrator.com">administrator@medrator.com</a>
					</p>
					
				</div>
			</div><!-- /.col-sm-8 -->
		</div>
	</div>
</div>
@include('us::layouts.footer')

@endsection
