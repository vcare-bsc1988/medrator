@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
                <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                       <!-- <i class="fa fa-pencil edit-pic"></i>-->
                    </div>
                </div>
            </form>
				<div class="col-md-2 col-xs-12" id="left-panel">
                                    @include('us::account.left_nav')
				</div>
				<div class="col-md-10 col-xs-12">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="pinfo">
                                            <h3 class="head-01">Change Password<hr></h3>
											<div class="clearfix" style="margin-top:10px;"></div>
                                            @if (session('success'))
                                                <div class="flash-message">
                                                <div class="alert alert-success">
                                                    {{ session('success') }}
                                                </div>
                                                </div>
                                            @endif
                                            @include('us::common.errors')

                                            {{ Form::open(array('url' => 'account/change-password','class'=>'form-horizontal','method' => 'post')) }}
											<div class="form-group">
											<div class="col-md-5 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">
													Old Password
												</label>
											</div>
											<div class="col-md-5 col-xs-12">
												{{ Form::password('old_password',['class'=>'form-control']) }}
											</div>
											</div>
											<div class="form-group">
											<div class="col-md-5 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">	
													New Password
												</label>
											</div>
											<div class="col-md-5 col-xs-12">
												{{ Form::password('password',['class'=>'form-control']) }}
											</div>
											</div>
											<div class="form-group">
											<div class="col-md-5 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">
													Confirm new Password
												</label>
											</div>
											<div class="col-md-5 col-xs-12">
												{{ Form::password('confirm_password',['class'=>'form-control']) }}
											</div>
											</div>
											<hr>
											<div class="form-group">
												<div class="col-md-5 col-xs-12">

												</div>							
												<div class="col-md-5 col-xs-12">
													{{ Form::submit('Change',['class'=>'btn btn-info radius-0']) }}
												</div>
											</div>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
				</div>
				
				
			</div>
		</div>
	</div>

    
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
	$('.edit-pic').click(function(){
        $('.ifile').click();
    })
    $("#image").change( function (){
        //alert($("#uploadImage").attr('action'));
        var form_upload = new FormData($("#uploadImage"));
        form_upload.append("_token","{{csrf_token()}}");
        var imagefile = $("input[type='file']#image").eq(0)[0].files[0];
        form_upload.append("photo", imagefile);
        form_upload.append("id", $("input[name='id']").val());
		$('.loader').show();
        $.ajax({		
            //Getting the url of the uploadphp from action attr of form 
            //this means currently selected element which is our form 
            url: $("#uploadImage").attr('action'),

            //For file upload we use post request
            type: "POST",

            //Creating data from form 
            data: form_upload,

            //Setting these to false because we are sending a multipart request
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){
                    $('.loader').show();
            },
            success: function(data){ 
                    //console.log(data);
                    $('.loader').hide();
                    //If the request is successfull we will get the scripts output in data variable 
                    //Showing the result in our html element 
                    $('#image-container').html(data);
            },
            error: function(data){
                    //console.log(data);
            }
        });
    })	
</script>
@endsection
