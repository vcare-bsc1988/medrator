@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
				<form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
                    <div class="col-md-2 col-xs-12" id="left-panel">
                            @include('us::account.left_nav')
                    </div>
                    <div class="col-md-10 col-xs-12">
                        <h1></h1>	
                        <h1 style="margin-bottom:15px;">Notifications</h1>  
						<p></p>
                        @if($notifications)
                        <ul class="notification-ul-list">
                            @foreach($notifications as $notification)
                            <li class="{{ $notification->status==1?'read':'unread'}} list-noti" style="margin-bottom:0px !important;">
                                    {{-- <h2>{{ $notification->title }}</h2> --}}
                                    <p class="message-part-noti">{{ $notification->message }}</p>
                                    <p class="time"><i class="fa fa-clock-o"></i> {{ $notification->notification_time }} <br><span class="noti-action-btn"> Mark as <a href="{{ $notification->status==1?url('notification-unread/'.$notification->id):url('notification-read/'.$notification->id) }}">{{ $notification->status==1?'Unread':'Read' }}</a></span> <span style="float:left;padding:10px 5px 5px 5px;">|</span> <span class="noti-action-btn"> <a href="{{ url('us/clear-notification/'.$notification->id) }}" style="color: #E91E63;">Clear</a></span></p>
                            </li>
                            @endforeach
                            <li class="text-left mt10"><a href="{{ url('us/home/clear-notifications') }}/{{ Auth::user()->id }}" class="btn btn-danger">Clear All</a></li>
                        </ul>
                        @endif
                    </div>
                </div>
            </div>
	</div>
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>

@endsection
