@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section">
    <div class="col-xs-12">
        <div class="row">
            <!--<h3 class="top-strips"></h3>-->
            
            <div class="col-md-12 col-xs-12" id="left-panel">
                @include('us::account.left_nav_new')
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
                        <!--<h3 class="head-01">Personal Inforamtion<hr></h3>-->
                        <div class="clearfix" style="margin-top:10px;"></div>
                        <div class="col-xs-12 padding-0">                           
                            <form class="form-horizontal">    
                                <div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Name</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="text" class="form-control"/> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Mobile Number</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="text" class="form-control"/> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Email ID</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="text" class="form-control"/> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Address</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="text" class="form-control"/> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel">Gender</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                        <input type="radio" name="gend"  checked/> Male &nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="gend"/> Female
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Degree</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                        <select class="form-control">
											<option disabled selected>-- Select Degree --</option>
											<option>MBBS</option>
											<option>MABS</option>
										</select>
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">College Name</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="text" class="form-control"/> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Year of Passing</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="date" class="form-control"/> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Speciality</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
										<div class="clear" id="content1">
											<select class="form-control 1col active" multiple="true">
												<option>MBBS</option>
												<option>MABS</option>
											</select>
										</div>
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Awards</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="text" class="form-control"/> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Recognition</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="text" class="form-control"/> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Experience</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="date" class="form-control"/> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Upload Photo(optional)</label>
                                    </div>
                                    <div class="col-md-5 col-xs-12">
                                       <input type="file" class="form-control"/> 
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-4 col-xs-12">

                                    </div>							
                                    <div class="col-md-5 col-xs-12">
                                            <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="SAVE" />
                                    </div>							
                                </div>
                            </form>
                        </div>							
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>                  
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    "use strict";
    $('.edit-pic').click(function(){
        $('.ifile').click();
    })
    $("#image").change( function (){
        //alert($("#uploadImage").attr('action'));
		
        var form_upload = new FormData();
		form_upload.append('constructor',$("#uploadImage"));
        form_upload.append("_token","{{csrf_token()}}");
        var imagefile = $("input[type='file']#image").eq(0)[0].files[0];
        form_upload.append("photo", imagefile);
        form_upload.append("id", $("input[name='id']").val());
		$('.loader-containers').show();
                    $('.loader').show();
        $.ajax({		
            //Getting the url of the uploadphp from action attr of form 
            //this means currently selected element which is our form  
            url: $("#uploadImage").attr('action'),

            //For file upload we use post request
            type: "POST",

            //Creating data from form 
            data: form_upload,

            //Setting these to false because we are sending a multipart request
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){
                    $('.loader-containers').show();
                    $('.loader').show();
            },
            success: function(data){ 
                    //console.log(data);
                    $('.loader-containers').hide();
                    $('.loader').hide();
                    //If the request is successfull we will get the scripts output in data variable 
                    //Showing the result in our html element 
                    $('#image-container').html(data);
            },
            error: function(data){
                    //console.log(data);
            }
        });
    })
    	
    $('.add-clinic-btn').click(function(){
        $('.add-clinic-form').slideToggle()
    })
	$("#location").geocomplete({
		details: ".location-details",
		detailsAttribute: "data-geo"
	});
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
