@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<script src="{{ asset('public/js/') }}"></script>
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
                                <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                        <!--<i class="fa fa-pencil edit-pic"></i>-->
                    </div>
                </div>
            </form>
				<div class="col-md-2 col-xs-12" id="left-panel">
                                    @include('us::account.left_nav')
				</div>
				<div class="col-md-10 col-xs-12">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="pinfo">
                                            <h3 class="head-01">Add Clinic<hr></h3>
                                            <div class="clearfix" style="margin-top:10px;"></div>
                                            @include('us::common.errors')
                                            @if (session('message'))
                                            <div class="alert alert-success">
                                                <strong>Success!</strong> {{ session('message') }}
                                            </div>
                                            @endif
                                            {{ Form::open(array('url' =>'account/save-clinic','class'=>'form-horizontal', 'files' => true)) }}
                                            {{ csrf_field() }}  
                                            {{ Form::hidden('id',null) }} 
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <div class="col-md-5 col-xs-12">
                                                            <label class="control-lebel" style="margin-top:8px;">Name</label>
                                                    </div>
                                                    <div class="col-md-5 col-xs-12">
                                                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                                                    </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
                                                <div class="col-md-5 col-xs-12">
                                                        <label class="control-lebel" style="margin-top:8px;">Address</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
                                                    {{ Form::text('address_one', null, ['class'=>'form-control location-input', 'placeholder'=>'Location', 'id'=>'location']) }}
													<div class="location-details">
														<input type="hidden" value="" name="latitude" data-geo="lat">
														<input type="hidden" value="" name="longitude" data-geo="lng">
														<input type="hidden" value="" name="locality" data-geo="locality">
														<input type="hidden" value="" name="city_name" data-geo="locality">
														<input type="hidden" value="" name="state_name" data-geo="administrative_area_level_1">
														<input type="hidden" value="" name="country_name" data-geo="country">
														<input type="hidden" value="" name="pincode" data-geo="postal_code">
													</div>
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                                <div class="col-md-5 col-xs-12">
                                                        <label class="control-lebel" style="margin-top:8px;">Phone Number</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
                                                    {{ Form::number('phone', null, ['class'=>'form-control']) }}
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
                                                <div class="col-md-5 col-xs-12">
                                                        <label class="control-lebel" style="margin-top:8px;">Speciality</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
													<div class="clear" id="content1">
                                                    {{ Form::select('specialties[]', $specialties,null, ['class' => '1col active form-control','multiple'=>true]) }}
													</div>
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('consultation_fee') ? ' has-error' : '' }}">
                                                <div class="col-md-5 col-xs-12">
                                                    <label class="control-lebel" style="margin-top:8px;">Consulting Fee</label>
                                                </div>
                                                <div class="col-md-5 col-xs-12">
                                                    {{ Form::text('consultation_fee', null, ['class'=>'form-control']) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5 col-xs-12"></div>							
                                                <div class="col-md-5 col-xs-12">
                                                    <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Save" />
                                                </div>							
                                            </div>
                                            {{ Form::close() }}     
                                        </div>
                                    </div>
				</div>
				
				
			</div>
		</div>
	</div>

    
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    $("#location").geocomplete({
		details: ".location-details",
		detailsAttribute: "data-geo"
	});
    $('.d-ul-left li:nth-child(2)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(2)').addClass('active');
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
