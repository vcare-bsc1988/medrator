@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
                <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                       <!-- <i class="fa fa-pencil edit-pic"></i>-->
                    </div>
                </div>
            </form>
            <div class="col-md-2 col-xs-12" id="left-panel">
                @include('us::account.left_nav')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
                        <h3 class="head-01">Saved List<hr></h3>
                        <div class="clearfix" style="margin-top:10px;"></div>
                        @if(!empty($results))
                        @foreach($results as $result)
                        @if($result['role']==2)
                        <div class="col-xs-12">
                            <div class="col-xs-12 loop-search" >
                                    <div class="col-xs-4  col-md-2 thumnail">
                                        @if($result['image'])
                                            <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                        @else
                                            <img src="{{ asset("public/img/doc_placeholder.png") }}" class="img-circle" width="95">
                                        @endif

                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <h4><a href="{{ url('us/doctor') }}/{{ $result['name_slug'] }}">{{$result['name'] }}</a></h4>
                                        <p class="color-grey">{{ $result['address_one'] }}</p>
                                        <p>                        
                                            @if(isset($result['speciality']) && !empty($result['speciality']))
                                            @for($i=0; ($i < count($result['speciality']) && $i<2); $i++)
                                                                                                                <span class="specility">
                                                                                                                        @if($i>0) @endif {{ $result['speciality'][$i] }} 
                                                                                                                </span>
                                            @endfor
                                            @endif
                                            <span class="color-grey">{{ $result['experience'] }} yrs exp.</span></p>
                                        <p class="color-grey">Consulting Fee <span><i class="fa fa-inr"></i>{{ $result['consultation_fee'] }}</span></p>
                                    </div>
                                    <div class="col-xs-12 col-md-4 text-right">
                                        <!--<p class="color-grey">4.5 <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></span></p>-->
                                        @if( number_format($result['rating']) !=0)
										<p class="color-grey">
                                            @for ($i=1; $i <= 5 ; $i++)
                                              <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                            @endfor
                                            {{ number_format($result['rating'], 1) }}
                                        </p>
                                        <p class="color-grey">
                                                {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                        </p>
										@else
											<p>No Reviews</p>
										@endif
                                        <p class="color-grey">
                                                {{ number_format($result['distance'],1)!=0?number_format($result['distance'],1):0 }} Km <a href="{{ url('us/https://www.google.co.in/maps/dir')}}//{{$result['latlon']}}" target="new"><i class="fa fa-location-arrow"></i></a>  
                                        </p>
                                    </div>
                                    <div class="col-xs-12 text-right">
                                        <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $result['phone']?$result['phone']:'Not Updated' }}</button>
                                                                                        </div>
                            </div>
                        </div>
                        @elseif($result['role']==4)
                        <div class="col-xs-12">
                            <div class="col-xs-12 loop-search" >
                                    <div class="col-xs-4  col-md-2 thumnail">
                                        @if($result['image'])
                                            <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                        @else
                                            <img src="{{ asset("public/img/diagnostic_placeholder.png") }}" class="img-circle" width="95">
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <h4><a href="{{ url('us/diagnostic') }}/{{ $result['name_slug'] }}">{{$result['name'] }}</a></h4>
                                        <p class="color-grey">{{ $result['address_one'] }}</p>
                                        <p>	  
                                        @if(isset($result['diagnostic_test']) && !empty($result['diagnostic_test']))
                                        @for($i=0; ($i < count($result['diagnostic_test']) && $i<2); $i++)
                                                <span class="specility">
                                                        @if($i>0) @endif {{ $result['diagnostic_test'][$i] }} 
                                                </span>
                                        @endfor
                                        @endif
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-md-4 text-right">
                                        @if( number_format($result['rating']) !=0)
										<p class="color-grey">
                                            @for ($i=1; $i <= 5 ; $i++)
                                              <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                            @endfor
                                            {{ number_format($result['rating'], 1) }}
                                        </p>
                                        <p class="color-grey">
                                                {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                        </p>
										@else
											<p>No Reviews</p>
										@endif
                                        <p class="color-grey">
                                                {{ number_format($result['distance'],1)!=0?number_format($result['distance'],1):0 }} Km <a href="{{ url('us/https://www.google.co.in/maps/dir')}}//{{$result['latlon']}}" target="new"><i class="fa fa-location-arrow"></i></a>  
                                        </p>
                                    </div>
                                    <div class="col-xs-12 text-right">
                                        <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $result['phone']?$result['phone']:'Not Updated' }}</button>
                                                                                        </div>
                            </div>
                        </div>
                        @elseif($result['role']==5)
                        <div class="col-xs-12">
                            <div class="col-xs-12 loop-search" >
                                <div class="col-xs-4  col-md-2 thumnail">
                                    @if($result['image'])
                                        <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                    @else
                                        <img src="{{ asset("public/img/hospital_placeholder.png") }}" class="img-circle" width="95">
                                    @endif

                                </div>
                                    <div class="col-xs-12 col-md-6">
                                        <h4><a href="{{ url('us/hospital') }}/{{ $result['name_slug'] }}">{{$result['name'] }}</a></h4>
                                        <p class="color-grey">{{ $result['address_one'] }}</p>
                                        <p> @if($result['24_hours_emergency'])<span class="color-grey">24 Hours Open</span> @endif </p>    
                                            <p>     
                                                    @if(isset($result['speciality']) && !empty($result['speciality']))
                                                    @for($i=0; ($i < count($result['speciality']) && $i<2); $i++)
                                                            <span class="specility">
                                                                    @if($i>0) @endif {{ $result['speciality'][$i] }} 
                                                            </span>
                                                    @endfor
                                                    @endif
                                            </P>
                                            <p class="color-grey">Consulting Fee <span><i class="fa fa-inr"></i>{{ $result['consultation_fee'] }}</span></p>
                                    </div>
                                    <div class="col-xs-12 col-md-4 text-right">
                                        @if( number_format($result['rating']) !=0)
										<p class="color-grey">
                                            @for ($i=1; $i <= 5 ; $i++)
                                              <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                            @endfor
                                            {{ number_format($result['rating'], 1) }}
                                        </p>
                                        <p class="color-grey">
                                                {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                        </p>
										@else
											<p>No Reviews</p>
										@endif
                                        <p class="color-grey">
                                                {{ number_format($result['distance'],1)!=0?number_format($result['distance'],1):0 }} Km <a href="{{ url('us/https://www.google.co.in/maps/dir')}}//{{$result['latlon']}}" target="new"><i class="fa fa-location-arrow"></i></a>  
                                        </p>
                                                                                        </div>
                                    <div class="col-xs-12 text-right">
                                        <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $result['phone']?$result['phone']:'Not Updated' }}</button>
                                                                                        </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @else
                        <div class="col-xs-12">
                            <h1 style="text-align: center;padding: 90px 0px;color: #d8e3ec;">No Saved list was found.</h1>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
	</div>
    </div>
</div>

    
@include('us::layouts.footer')
@endsection
