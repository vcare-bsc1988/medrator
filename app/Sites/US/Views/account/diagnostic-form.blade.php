@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section ptb">
    <div class="col-xs-12">
        <div class="row">
            <h3 class="top-strips"></h3>
            <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-md-2 col-xs-12" id="left-panel">
                @include('us::account.left_nav')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
                        <h3 class="head-01">Diagnostic Center Information<hr></h3>
                        <div class="clearfix" style="margin-top:10px;"></div>
                        <div class="col-xs-12 padding-0">
                            @include('us::common.errors')
                            @if (session('message'))
                            <div class="alert alert-success">
                                <strong>Success!</strong> {{ session('message') }}
                            </div>
                            @endif
                            {{ Form::open(array('url' =>'account/save-diagnostic','class'=>'form-horizontal', 'files' => true)) }}
                                {{ csrf_field() }}  
                                {{ Form::hidden('id', null) }} 
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <div class="col-md-4 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">Name</label>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                                        </div>
                                </div>
                                <div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Address</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        {{ Form::text('address_one', null, ['class'=>'form-control location-input','id'=>'location']) }}
										<div class="location-details">
											{{ Form::hidden('latitude', null, ['data-geo'=>'lat']) }}
											{{ Form::hidden('longitude', null, ['data-geo'=>'lng']) }}
											{{ Form::hidden('locality', null, ['data-geo'=>'locality']) }}
											{{ Form::hidden('city_name', null, ['data-geo'=>'locality']) }}
											{{ Form::hidden('state_name', null, ['data-geo'=>'administrative_area_level_1']) }}
											{{ Form::hidden('country_name', null, ['data-geo'=>'country_short']) }}
										</div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Phone Number</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        {{ Form::number('phone', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'12']) }}
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('home_collection_facility') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Home Collection Facility</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::checkbox('home_collection_facility', 1,null,['class'=>'squaredFives']) }}
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('online_bookings') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Online Bookings</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::checkbox('online_bookings', 1,null,['class'=>'squaredFives']) }}
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('online_reports') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Online Reports</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::checkbox('online_reports', 1,null,['class'=>'squaredFives']) }}
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Tests</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										<div class="clear" id="content1">
											{{ Form::select('tests[]', $tests,null, ['placeholder'=>'Select Tests','class' => '1col active form-control','multiple'=>true]) }}
										</div>
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Upload Photo (optional)</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::file('image',['class'=>'form-control']) }}
                                    </div>
                                </div>
								<hr>
                                <div class="form-group">
                                    <div class="col-md-4 col-xs-12">

                                    </div>							
                                    <div class="col-md-6 col-xs-12">
                                            <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Save" />
                                    </div>							
                                </div>
                            {{ Form::close() }}
                        </div>							
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>                  
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    $("#location").geocomplete({
		details: ".location-details",
		detailsAttribute: "data-geo"
	});
    $('.d-ul-left li:nth-child(2)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(2)').addClass('active');
	/** For accepting numeric value only.
	 *
	 */
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
