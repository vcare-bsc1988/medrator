@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<span id="scheduler_loader" style="display: none;">Loading...Please Wait ! <img src="{{ asset('public/images/Preloader_1.gif') }}" width="32"/></span>
<div class="first-section ptb">
    <div class="col-xs-12">
        <div class="row">
            <h3 class="top-strips"></h3>
            <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-md-2 col-xs-12" id="left-panel">
                @include('us::account.left_nav')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
                        <h3 class="head-01">Diagnostic Informations<hr></h3>
                        <div class="clearfix" style="margin-top:10px;"></div>
                        @include('us::common.errors')
                        @if (session('message'))
                        <div class="alert alert-success">
                            {{-- <strong>Success!</strong> --}} {{ session('message') }}
                        </div>
                        @endif 
                        @if(count($diagnostic)>0)
                        <div class="col-xs-12 padding-0" >				
                            <div class="col-xs-12 col-md-6" style="border:1px dashed #f0f0f0;padding:5px;margin:6px;">
                                <h4 style="margin-bottom:10px;">{{ $diagnostic->name }}</h4>
                                <p class="color-grey"><i class="fa fa-map-marker"></i>&nbsp;&nbsp; {{ $diagnostic->address_one }}</p>
                                <p class="color-grey"><i class="fa fa-money"></i> Consulting Fee - &nbsp;&nbsp;<span><strong><i class="fa fa-inr"></i>{{ $diagnostic->consultation_fee }}</strong></span></p>
                                <p style="margin-bottom:10px;">
                                @if($diagnostic->tests()->get()->count()>0)
                                     @foreach($diagnostic->tests as $test)
                                    <span class="specility">
                                        {{ $test->name }}
                                    </span>
                                    @endforeach 
                                    @else
                                        Not Updated.
                                    @endif
                                </p>
                                <p><span class='text-info'>Schedules</span> <button rel="{{ $diagnostic->id }}" class="btn btn-primary btn-xs add-schedules">Add/Edit</button></P>
                                <p class="pschedule">
                                    <?php $schedules = \App\Model\Diagnostic::getSchedules($diagnostic);
                                    if($schedules){
                                        foreach($schedules as $schedule){
                                            ?>
                                            <span>
                                            @if($schedule['week_day']==1 && !empty($schedule['timings']))<span>SUN</span> 
                                            @elseif($schedule['week_day']==2 && !empty($schedule['timings']))<br><span>MON</span>
                                            @elseif($schedule['week_day']==3 && !empty($schedule['timings']))<br><span>TUE</span>
                                            @elseif($schedule['week_day']==4 && !empty($schedule['timings']))<br><span>WED</span>
                                            @elseif($schedule['week_day']==5 && !empty($schedule['timings']))<br><span>THU</span>
                                            @elseif($schedule['week_day']==6 && !empty($schedule['timings']))<br><span>FRI</span> 
                                            @elseif($schedule['week_day']==7 && !empty($schedule['timings'])) 
                                            <br><span>SAT</span>
                                            @endif
                                            {{ strtoupper($schedule['timings']) }}</span>
                                            <?php 
                                        }
                                    } ?>
                                </p><br><br>
                                <p>Photo Gallery <button rel="{{ $diagnostic->id }}" class="btn btn-primary btn-xs clinic-upload_gallry" data-toggle="modal" data-target="#photoModal">Add</button>
                                    <div class="col-xs-12 padding-0" style="margin-top:4px;margin-bottom:4px;">
                                        @if($diagnostic->image)
										<div class="col-xs-6 col-md-2" style="padding:0px 5px;">
                                            <a href="#" class="thumbnail">
                                              <img src="{{ $diagnostic->image }}" alt="...">
                                            </a>
                                         </div>
										@endif
										@if($diagnostic->galleries()->get()->count()>0)
                                        @foreach($diagnostic->galleries()->get() as $gallery)
                                        <div class="col-xs-6 col-md-2" style="padding:0px 5px;">
                                            <a href="#" class="thumbnail">
                                              <img src="{{ $gallery->image_url }}" alt="...">
                                            </a>
                                            <a href="{{ url('us/account/delete-gallery')}}/{{$gallery->id}}">delete</a>
                                         </div>
                                        @endforeach
                                        @endif
                                    </div>                                                 
                                </span></P>
                                <a href="{{ url('us/account/update-diagnostic') }}/{{ $diagnostic->id }}" style="position: absolute;top: 10px;right: 10px;"><i class="fa fa-pencil curs pull-right"></i></a>
                            </div>
                            <br>
                        </div> 
                        @endif          
                    </div>
                </div>
            </div>
				
				
        </div>
    </div>
</div>

 <!-- Modal for adding schedular -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p><h4 class="modal-title">Add/Edit Schedules</h4> <span id="schedule-msg"></span></p>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs schedule-ul" role="tablist">
                        <li role="presentation" class="active"><a href="#sun" aria-controls="sun" role="tab" data-toggle="tab">SUN</a></li>
                        <li role="presentation"><a href="#mon" aria-controls="mon" role="tab" data-toggle="tab">MON</a></li>
                        <li role="presentation"><a href="#tue" aria-controls="tue" role="tab" data-toggle="tab">TUE</a></li>
                        <li role="presentation"><a href="#wed" aria-controls="wed" role="tab" data-toggle="tab">WED</a></li>
                        <li role="presentation"><a href="#thu" aria-controls="thu" role="tab" data-toggle="tab">THU</a></li>
                        <li role="presentation"><a href="#fri" aria-controls="fri" role="tab" data-toggle="tab">FRI</a></li>
                        <li role="presentation"><a href="#sat" aria-controls="sat" role="tab" data-toggle="tab">SAT</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tab-content1">
                        {{-- Content is loading from ajax --}}
                        
                </div>
            </div>
            <!--<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>-->
        </div>
    </div>
</div> 
 <!-- Modal for adding schedular -->
<div id="photoModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Clinic Image</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('url' =>'diagnostic/upload-image','class'=>'form-horizontal', 'id'=>'upload_gallry', 'files' => true)) }}
                {{ csrf_field() }}  
                {{ Form::hidden('id', null, ['id'=>'clinic_id']) }} 
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                        <label class="control-lebel" style="margin-top:8px;">Upload Photo (optional)</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        {{ Form::file('image',['class'=>'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-5 col-xs-12"></div>							
                    <div class="col-md-5 col-xs-12">
                            <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="SAVE" />
                    </div>							
                </div>
                {{ Form::close() }}
            </div>
            <!--<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>-->
        </div>
    </div>
</div> 
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    "use strict"; 
    /**
    * @ Populate add schedules form popup
    */
    $('.add-schedules').click(function(){
        var uid = "{{ Auth::user()->id }}";
        var id = $(this).attr("rel");
        var csrf_field = "{{ csrf_token() }}";
        $.ajax({
            type: "POST",
            url: "{{ url('diagnostic/schedules') }}",
            data: {'id':id, '_token':csrf_field},
            beforeSend: function(){
                $('#scheduler_loader').css('display','block');
            },
            success: function(result){ 
                console.log(result);
                $('#scheduler_loader').css('display','none');
		$("#myModal").modal();
                $(".tab-content1").html(result);                
            },
            error:function(result){
                console.log(result);
            }
        });
    })
    /**
    * @ Showing gallery popup
    */
    $('.clinic-upload_gallry').click(function(){
        var uid = "{{ Auth::user()->id }}";
        var id = $(this).attr("rel");
        $("#clinic_id").val(id);
        return true;
    })
    /**
    * @ Save schedules
    */
</script>
@endsection
