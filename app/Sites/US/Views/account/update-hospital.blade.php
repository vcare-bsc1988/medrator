@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section ptb">
    <div class="col-xs-12">
        <div class="row">
            <h3 class="top-strips"></h3>
            <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="{{ $user->id }}">
            <input type="file" class="hide ifile" name="photo" id="image"/>
            <div class="form-group" style="margin-bottom:15px;">							
                <div class="col-md-12 text-center col-xs-12">
                    <div id="image-container">
                        @if($user->user->image)
                                <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                        @else
                                <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                        @endif
                    </div>
                </div>
            </div>
            </form>
            <div class="col-md-2 col-xs-12" id="left-panel">
                @include('us::account.left_nav')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
                        <h3 class="head-01">Claimed Profile<hr></h3>
                                                                    <div class="clearfix" style="margin-top:10px;"></div>
                            @include('us::common.errors')
                            @if (session('message'))
                            <div class="alert alert-success">
                                <strong>Success!</strong> {{ session('message') }}
                            </div>
                            @endif   
                            @include('us::forms.hospital')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    $('.d-ul-left li:nth-child(2)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(2)').addClass('active');	
    $("#location").geocomplete({
		details: ".location-details",
		detailsAttribute: "data-geo"
	});
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
