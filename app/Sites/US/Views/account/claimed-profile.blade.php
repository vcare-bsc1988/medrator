@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
				<form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
                <div class="col-md-2 col-xs-12" id="left-panel">
                                    @include('us::account.left_nav')
				</div>
				<div class="col-md-10 col-xs-12">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="pinfo">
                                            <h3 class="head-01">Claimed Profile<hr></h3>
											<div class="clearfix" style="margin-top:10px;"></div>
                                                @include('us::common.errors')
                                                @if (session('message'))
                                                <div class="alert alert-success">
                                                    {{-- <strong>Success!</strong> --}} {{ session('message') }}
                                                </div>
												
                                                @endif   
                                                @if(!empty($user->doctor))
                                                    @include('us::forms.doctor')
                                                @elseif(!empty($user->hospital))
                                                    @include('us::account.hospital')
                                                @elseif(!empty($user->diagnostic))
                                                    @include('us::account.diagnostic')
                                                @else	
                                                    <h1 class="text-center ptb" style="color: #d9e3ea;">You currently have no profile claimed.<br>Would you like to search and claim one.</h1>
                                                    <div class="clearfix" style="margin-top:10px;"></div>
                                                    <div class="col-sm-12 col-xs-12">
                                                        {{Form::open(['url' => 'account/search-profile', 'method' => 'get', 'style'=>'padding-left:0px;', 'id'=>'filterForm', 'class' => 'search-form-0 col-xs-12', 'rol'=>'search'])}}
                                                            <div class="form-group">
                                                                <div class="col-xs-12 col-md-7 col-md-offset-2 padding-0">
                                                                    {{ Form::text('q',null,['id'=>'search-box', 'class'=>'form-control sinput sinput-claimed', 'placeholder'=>'Search and add a profile','autocomplete'=>'off','required'=>true]) }}
                                                                    <div id="suggesstion-box"></div>
                                                                </div>
																
                                                                <div class="col-xs-12 col-md-1 padding-0">
                                                                    <button type="submit" id="claim-search" class="btn btn-info search-btn search-claimed"><i class="glyphicon glyphicon-search"></i></button>
                                                                </div>
                                                            </div>
                                                        {{ Form::close() }}
                                                    </div>
                                                @endif          
                                        </div>
                                    </div>
				</div>
				
				
			</div>
		</div>
	</div>

    
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    $('.d-ul-left li:nth-child(5)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(5)').addClass('active');	
    $("#location").geocomplete({
            details: ".location-details",
            detailsAttribute: "data-geo"
    });
    $('.add-quali').click(function(){
        var append_data = '<div class="rp-container"><div class="clearfix" style="float:left;width:100%;margin-top:2px;"></div><div class="col-md-3 col-xs-12"><div id="content" class="clear">{{ Form::select("degree[]", $qualifications, null, ["class" => "2col active form-control"]) }}</div></div><div class="col-md-3 col-xs-12">{{ Form::select("college[]", $colleges, null, ["class"=>"form-control"]) }}</div><div class="col-md-3 col-xs-12">{{ Form::select("year[]",$years, null, ["class"=>"form-control"]) }}</div><div class="col-md-2"><i class="cur fa fa-minus btn btn-danger btn-xs remove-repeat" style="margin-top:10px;"></i></div></div>';
        $('.repeat-quali').append(append_data);
    })
    $('.repeat-quali').on('click','.remove-repeat',function(){
        $(this).parents('.rp-container').remove();
    })
	/** For accepting bumeric value only.
	 *
	 */
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
