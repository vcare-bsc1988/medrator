@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section ptb">
		<div class="col-xs-12">
			<div class="row">
				<h3 class="top-strips"></h3>
                                <div class="form-group" style="margin-bottom:15px;">							
                                    <div class="col-md-12 text-center col-xs-12">
                                        <div id="image-container">
                                            @if($user->user->image)
                                                    <img src="{{ $user->user->image }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>				
                                            @else
                                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                                            @endif
                                        </div>
                                    </div>
                                </div>
				<div class="col-md-2 col-xs-12" id="left-panel">
                                    @include('us::account.left_nav')
				</div>
				<div class="col-md-10 col-xs-12">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="pinfo">
                                            <h3 class="head-01">Claim Profile<hr><h3>
                                                @include('us::common.errors')
                                                @if (session('message'))
                                                <div class="alert alert-success">
                                                    <strong>Success!</strong> {{ session('message') }}
                                                </div>
                                                @endif
												<div class="col-sm-12 col-xs-12">
                                                    {{Form::open(['url' => 'account/search-profile', 'method' => 'get', 'style'=>'padding-left:0px;', 'id'=>'filterForm', 'class' => 'search-form-0 col-xs-12', 'rol'=>'search'])}}
                                                        <div class="form-group">
                                                            <div class="col-xs-12 col-md-7 col-md-offset-2 padding-0">
                                                                {{ Form::text('q',null,['id'=>'search-box', 'class'=>'form-control sinput sinput-claimed', 'placeholder'=>'Search and add a profile','autocomplete'=>'off']) }}
                                                                <div id="suggesstion-box"></div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-1 padding-0">
                                                                <button type="submit" id="search" class="btn btn-info search-btn search-claimed"><i class="glyphicon glyphicon-search"></i></button>
                                                            </div>
                                                        </div>
                                                    {{ Form::close() }}
                                                </div>
                                                <div class="clearfix" style="margin-top:10px;"></div>
                                                <h1 class="text-center ptb" style="color: #d9e3ea;">Didn’t find anyone with that name !</h1>
                                                <div class="col-xs-12 text-center">
                                                    <a href="{{ url('us/add-profile') }}"><button class="btn btn-info btn-lg radius-0 add-clinic-btn" type="button">Add Profile</button></a>
                                                </div><br><br><br>
                                                    @if(!empty($results))
                                                    @foreach($results as $result)
                                                        @if($result['role']==2)
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 loop-search" >
                                                                    <div class="col-xs-4  col-md-2 thumnail">
                                                                        @if($result['image'])
                                                                            <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                                                        @else
                                                                            <img src="{{ asset("public/img/doc_placeholder.png") }}" class="img-circle" width="95">
                                                                        @endif

                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6">
                                                                        <h4><a href="{{ url('us/doctor/details') }}/{{ \Crypt::encrypt($result['id']) }}">{{$result['name'] }}</a></h4>
                                                                        <p class="color-grey">{{ $result['address_one'] }}</p>
                                                                        <p>                        
                                                                            @if(isset($result['speciality']) && !empty($result['speciality']))
                                                                            @for($i=0; ($i < count($result['speciality']) && $i<2); $i++)
                                                                                                                                                <span class="specility">
                                                                                                                                                        @if($i>0) @endif {{ $result['speciality'][$i] }} 
                                                                                                                                                </span>
                                                                            @endfor
                                                                            @endif
                                                                            <span class="color-grey">{{ $result['experience'] }} yrs exp.</span></p>
                                                                        <p class="color-grey">Consulting Fee <span><i class="fa fa-inr"></i>{{ $result['consultation_fee'] }}</span></p>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-4 text-right">
                                                                        <!--<p class="color-grey">4.5 <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></span></p>-->
                                                                         <p class="color-grey">
                                                                            @for ($i=1; $i <= 5 ; $i++)
                                                                              <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                                                            @endfor
                                                                            {{ number_format($result['rating'], 1) }}
                                                                        </p>
                                                                        <p class="color-grey">
                                                                                {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                                                        </p>
                                                                        <p class="color-grey">
                                                                                {{ number_format($result['distance']) }} Km <i class="fa fa-location-arrow"></i> 
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-xs-12 text-right">
                                                                        <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $result['phone']?$result['phone']:'Not Updated' }}</button>
                                                                                                                        </div>
                                                            </div>
                                                        </div>
                                                        @elseif($result['role']==4)
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 loop-search" >
                                                                    <div class="col-xs-4  col-md-2 thumnail">
                                                                        @if($result['image'])
                                                                            <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                                                        @else
                                                                            <img src="{{ asset("public/img/diagnostic_placeholder.png") }}" class="img-circle" width="95">
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6">
                                                                        <h4><a href="{{ url('us/diagnostic/details') }}/{{ \Crypt::encrypt($result['id']) }}">{{$result['name'] }}</a></h4>
                                                                        <p class="color-grey">{{ $result['address_one'] }}</p>
                                                                                                                                <p>	  
                                                                                                                                @if(isset($result['diagnostic_test']) && !empty($result['diagnostic_test']))
                                                                                                                                @for($i=0; ($i < count($result['diagnostic_test']) && $i<2); $i++)
                                                                                                                                        <span class="specility">
                                                                                                                                                @if($i>0) @endif {{ $result['diagnostic_test'][$i] }} 
                                                                                                                                        </span>
                                                                                                                                @endfor
                                                                                                                                @endif
                                                                                                                                </p>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-4 text-right">
                                                                        <p class="color-grey">
                                                                            @for ($i=1; $i <= 5 ; $i++)
                                                                              <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                                                            @endfor
                                                                            {{ number_format($result['rating'], 1) }}
                                                                        </p>
                                                                        <p class="color-grey">
                                                                                {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                                                        </p>
                                                                        <p class="color-grey">
                                                                                {{ number_format($result['distance']) }} Km <i class="fa fa-location-arrow"></i> 
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-xs-12 text-right">
                                                                        <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $result['phone']?$result['phone']:'Not Updated' }}</button>
                                                                                                                        </div>
                                                            </div>
                                                        </div>
                                                        @elseif($result['role']==5)
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 loop-search" >
                                                                    <div class="col-xs-4  col-md-2 thumnail">
                                                                        @if($result['image'])
                                                                            <img src="{{ $result['image'] }}" style="width:80px; text-align: left;">
                                                                        @else
                                                                            <img src="{{ asset("public/img/hospital_placeholder.png") }}" class="img-circle" width="95">
                                                                        @endif

                                                                    </div>
                                                                    <div class="col-xs-12 col-md-6">
                                                                        <h4><a href="{{ url('us/hospital/details') }}/{{ \Crypt::encrypt($result['id']) }}">{{$result['name'] }}</a></h4>
                                                                        <p class="color-grey">{{ $result['address_one'] }}</p>
                                                                        <p> @if($result['24_hours_emergency'])<span class="color-grey">24 Hours Open</span> @endif </p>    
                                                                                                                                <p>     
                                                                                                                                        @if(isset($result['speciality']) && !empty($result['speciality']))
                                                                                                                                        @for($i=0; ($i < count($result['speciality']) && $i<2); $i++)
                                                                                                                                                <span class="specility">
                                                                                                                                                        @if($i>0) @endif {{ $result['speciality'][$i] }} 
                                                                                                                                                </span>
                                                                                                                                        @endfor
                                                                                                                                        @endif
                                                                                                                                </P>
                                                                                                                                <p class="color-grey">Consulting Fee <span><i class="fa fa-inr"></i>{{ $result['consultation_fee'] }}</span></p>
                                                                    </div>
                                                                    <div class="col-xs-12 col-md-4 text-right">
                                                                        <p class="color-grey">
                                                                            @for ($i=1; $i <= 5 ; $i++)
                                                                              <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                                                            @endfor
                                                                            {{ number_format($result['rating'], 1) }}
                                                                        </p>
                                                                        <p class="color-grey">
                                                                                {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}}
                                                                        </p>
                                                                        <p class="color-grey">
                                                                                {{ number_format($result['distance']) }} Km <i class="fa fa-location-arrow"></i> 
                                                                        </p>
                                                                                                                        </div>
                                                                    <div class="col-xs-12 text-right">
                                                                        <button class="btn btn-info radius-0 phone-number-container"><i class="fa fa-phone phone-fa-font"></i> {{ $result['phone']?$result['phone']:'Not Updated' }}</button>
                                                                                                                        </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    @endforeach
                                                    @endif
                                        </div>
                                    </div>
				</div>
				
				
			</div>
		</div>
	</div>

    
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    "use strict"; 
    $(function(){    
        $("#geocomplete").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo"
        });
    });
	$('.d-ul-left li:nth-child(2)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(2)').addClass('active');	
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
