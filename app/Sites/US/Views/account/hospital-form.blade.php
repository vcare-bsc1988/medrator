@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<div class="first-section ptb">
    <div class="col-xs-12">
        <div class="row">
            <h3 class="top-strips"></h3>
            <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-md-2 col-xs-12" id="left-panel">
                @include('us::account.left_nav')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
                        <h3 class="head-01">Hospital Information<hr></h3>
                        <div class="clearfix" style="margin-top:10px;"></div>
                        <div class="col-xs-12 padding-0">
                            @include('us::common.errors')
                            @if (session('message'))
                            <div class="alert alert-success">
                                <strong>Success!</strong> {{ session('message') }}
                            </div>
                            @endif
                            {{ Form::open(array('url' =>'account/save-hospital','class'=>'form-horizontal', 'files' => true)) }}
                                {{ csrf_field() }}  
                                {{ Form::hidden('id', null) }} 
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <div class="col-md-4 col-xs-12">
                                                <label class="control-lebel" style="margin-top:8px;">Name</label>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                                        </div>
                                </div>
                                <div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Address</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        {{ Form::text('address_one', null, ['class'=>'form-control location-input', 'placeholder'=>'Location', 'id'=>'location']) }}
                                        <div class="location-details">
                                            <input type="hidden" value="" name="latitude" data-geo="lat">
                                            <input type="hidden" value="" name="longitude" data-geo="lng">
                                            <input type="hidden" value="" name="locality" data-geo="locality">
                                            <input type="hidden" value="" name="city_name" data-geo="locality">
                                            <input type="hidden" value="" name="state_name" data-geo="administrative_area_level_1">
                                            <input type="hidden" value="" name="country_name" data-geo="country_short">
                                            <input type="hidden" value="" name="pincode" data-geo="postal_code">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Phone Number</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        {{ Form::number('phone', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'12']) }}
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('no_of_doctors') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">No of Doctors</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::text('no_of_doctors', null, ['class' => 'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('no_of_beds') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">No of Beds</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::text('no_of_beds', null, ['class' => 'form-control','onkeypress'=>'return isNumber(event)', 'maxlength'=>'10']) }}
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('icu_facility') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">ICU Facility</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::checkbox('icu_facility', 1,null,['class'=>'squaredFives']) }}
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('diagnostic_lab_facility') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Lab Facility</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::checkbox('diagnostic_lab_facility', 1,null,['class'=>'squaredFives']) }}
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Cashless Mediclaim</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::checkbox('cashless_mediclaim', 1,null,['class'=>'squaredFives']) }}
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('24_hours_emergency') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">24 Hours Emergency</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::checkbox('24_hours_emergency', 1,null,['class'=>'squaredFives']) }}
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                            <label class="control-lebel" style="margin-top:8px;">Speciality</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										<div class="clear" id="content1">
                                        {{ Form::select('specialties[]', $specialties,null, ['class' => '1col active form-control','multiple'=>true]) }}
										</div>
                                    </div>
                                </div>
								<div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-xs-12">
                                        <label class="control-lebel" style="margin-top:8px;">Upload Photo (optional)</label>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
										{{ Form::file('image',['class'=>'form-control']) }}
                                    </div>
                                </div>
								<hr>
                                <div class="form-group">
                                    <div class="col-md-4 col-xs-12">

                                    </div>							
                                    <div class="col-md-6 col-xs-12">
                                            <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Save" />
                                    </div>							
                                </div>
                            {{ Form::close() }}
                        </div>							
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>                  
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
	$('.edit-pic').click(function(){
        $('.ifile').click();
    })
    $("#image").change( function (){
        //alert($("#uploadImage").attr('action'));
        var form_upload = new FormData($("#uploadImage"));
        form_upload.append("_token","{{csrf_token()}}");
        var imagefile = $("input[type='file']#image").eq(0)[0].files[0];
        form_upload.append("photo", imagefile);
        form_upload.append("id", $("input[name='id']").val());
		$('.loader').show();
        $.ajax({		
            //Getting the url of the uploadphp from action attr of form 
            //this means currently selected element which is our form 
            url: $("#uploadImage").attr('action'),

            //For file upload we use post request
            type: "POST",

            //Creating data from form 
            data: form_upload,

            //Setting these to false because we are sending a multipart request
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){
                    $('.loader').show();
            },
            success: function(data){ 
                    //console.log(data);
                    $('.loader').hide();
                    //If the request is successfull we will get the scripts output in data variable 
                    //Showing the result in our html element 
                    $('#image-container').html(data);
            },
            error: function(data){
                    //console.log(data);
            }
        });
    })
    $('.d-ul-left li:nth-child(2)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(2)').addClass('active');
    $("#location").geocomplete({
		details: ".location-details",
		detailsAttribute: "data-geo"
	});
	/** For accepting numeric value only.
	 *
	 */
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
</script>
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
