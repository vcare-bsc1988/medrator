@extends('us::layouts.home')
@section('us::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')
<span id="scheduler_loader" style="display: none;">Loading...Please Wait ! <img src="{{ asset('public/images/Preloader_1.gif') }}" width="32"/></span>
<div class="first-section ptb">
    <div class="col-xs-12">
        <div class="row">
            <h3 class="top-strips"></h3>
            <form id="uploadImage" action="{{ url('user/upload-image') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="file" class="hide ifile" name="photo" id="image"/>
                <div class="form-group" style="margin-bottom:15px;">							
                    <div class="col-md-12 text-center col-xs-12">
                        <div id="image-container">
                            @if($user->user->image)
                                    <img src="{{ $user->user->image }}" class=" imgs-doctor-profile" width="130" height="130"/>				
                            @else
                                    <img src="{{ asset('public/images/user-placeholder.png') }}" class="img-circle imgs-doctor-profile" width="150" height="150"/>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-md-2 col-xs-12" id="left-panel">
                @include('us::account.left_nav')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="pinfo">
                        <h3 class="head-01">Hospital Informations<hr></h3>
                        <div class="clearfix" style="margin-top:10px;"></div>
                        @include('us::common.errors')
                        @if (session('message'))
                        <div class="alert alert-success">
                            {{-- <strong>Success!</strong> --}} {{ session('message') }}
                        </div>
                        @endif 
                        @if(count($hospital)>0)
                        <div class="col-xs-12 padding-0" >				
                            <div class="col-xs-12 col-md-6" style="border:1px dashed #f0f0f0;padding:5px;margin:6px;">
                                <h4 style="margin-bottom:10px;">{{ $hospital->name }}</h4>
                                <p class="color-grey"><i class="fa fa-map-marker"></i>&nbsp;&nbsp; {{ $hospital->address_one }}</p>
                                <p class="color-grey"><i class="fa fa-money"></i> Consulting Fee - &nbsp;&nbsp;<span><strong><i class="fa fa-inr"></i>{{ $hospital->consultation_fee }}</strong></span></p>
                                <p style="margin-bottom:10px;">
                                @if($hospital->specialties()->get()->count()>0)
                                    @foreach($hospital->specialtyList as $specialty)
                                    <span class="specility">
                                        {{ $specialty->name }}
                                    </span>
                                    @endforeach 
                                    @else
                                        Not Updated.
                                    @endif
                                </p>
                                <p><span class='text-info'>Schedules</span> <button rel="{{ $hospital->id }}" class="btn btn-primary btn-xs add-schedules">Add/Edit</button></P>
                                <p class="pschedule">
                                    <?php $schedules = \App\Model\Hospital::getSchedules($hospital); 
                                    if($schedules){
                                        foreach($schedules as $schedule){
                                            ?>
                                            <span>
                                            @if($schedule['week_day']==1 && !empty($schedule['timings']))<span>SUN</span> 
                                            @elseif($schedule['week_day']==2 && !empty($schedule['timings']))<br><span>MON</span>
                                            @elseif($schedule['week_day']==3 && !empty($schedule['timings']))<br><span>TUE</span>
                                            @elseif($schedule['week_day']==4 && !empty($schedule['timings']))<br><span>WED</span>
                                            @elseif($schedule['week_day']==5 && !empty($schedule['timings']))<br><span>THU</span>
                                            @elseif($schedule['week_day']==6 && !empty($schedule['timings']))<br><span>FRI</span> 
                                            @elseif($schedule['week_day']==7 && !empty($schedule['timings']))<br><span>SAT</span>
                                            @endif
                                            {{ strtoupper($schedule['timings']) }}</span>
                                            <?php 
                                        }
                                    } ?>
                                </p><br><br>
                                <p>Photo Gallery <button rel="{{ $hospital->id }}" class="btn btn-primary btn-xs clinic-upload_gallry" data-toggle="modal" data-target="#photoModal">Add</button>
                                    <div class="col-xs-12 padding-0" style="margin-top:4px;margin-bottom:4px;">
                                        @if($hospital->image)
										<div class="col-xs-6 col-md-2" style="padding:0px 5px;">
                                            <a href="#" class="thumbnail">
                                              <img src="{{ $hospital->image }}" alt="...">
                                            </a>
                                         </div>
										@endif
										@if($hospital->galleries()->get()->count()>0)
                                        @foreach($hospital->galleries()->get() as $gallery)
                                        <div class="col-xs-6 col-md-2" style="padding:0px 5px;">
                                            <a href="#" class="thumbnail">
                                              <img src="{{ $gallery->image_url }}" alt="...">
                                            </a>
                                            <a href="{{ url('us/account/delete-gallery/')}}{{$gallery->id}}">delete</a>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>                                                 
                                </span></P>
                                <a href="{{ url('us/account/update-hospital') }}/{{ $hospital->id }}" style="position: absolute;top: 10px;right: 10px;"><i class="fa fa-pencil curs pull-right"></i></a>
                            </div>
                            <br>
                        </div> 
                        @endif          
                    </div>
                </div>
            </div>
				
				
        </div>
    </div>
</div>

 <!-- Modal for adding schedular -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p><h4 class="modal-title">Add/Edit Schedules</h4> <span id="schedule-msg"></span></p>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs schedule-ul" role="tablist">
                        <li role="presentation" class="active"><a href="#sun" aria-controls="sun" role="tab" data-toggle="tab">SUN</a></li>
                        <li role="presentation"><a href="#mon" aria-controls="mon" role="tab" data-toggle="tab">MON</a></li>
                        <li role="presentation"><a href="#tue" aria-controls="tue" role="tab" data-toggle="tab">TUE</a></li>
                        <li role="presentation"><a href="#wed" aria-controls="wed" role="tab" data-toggle="tab">WED</a></li>
                        <li role="presentation"><a href="#thu" aria-controls="thu" role="tab" data-toggle="tab">THU</a></li>
                        <li role="presentation"><a href="#fri" aria-controls="fri" role="tab" data-toggle="tab">FRI</a></li>
                        <li role="presentation"><a href="#sat" aria-controls="sat" role="tab" data-toggle="tab">SAT</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tab-content1">
                        {{-- Content is loading from ajax --}}
                        
                </div>
            </div>
            <!--<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>-->
        </div>
    </div>
</div> 
 <!-- Modal for adding schedular -->
<div id="photoModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Clinic Image</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(array('url' =>'hospital/upload-image','class'=>'form-horizontal', 'id'=>'upload_gallry', 'files' => true)) }}
                {{ csrf_field() }}  
                {{ Form::hidden('id', null, ['id'=>'clinic_id']) }} 
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                        <label class="control-lebel" style="margin-top:8px;">Upload Photo (optional)</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        {{ Form::file('image',['class'=>'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-5 col-xs-12"></div>							
                    <div class="col-md-5 col-xs-12">
                            <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="SAVE" />
                    </div>							
                </div>
                {{ Form::close() }}
            </div>
            <!--<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>-->
        </div>
    </div>
</div> 
@include('us::layouts.footer')
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
    "use strict"; 
    /**
     * @ Addinf dynamic field for schedular
     */
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper_sun         = $(".input_fields_wrap_sun"); //Fields wrapper
    var add_button_sun      = $(".add_field_button_sun"); //Add button ID
    
    var wrapper_mon         = $(".input_fields_wrap_mon"); //Fields wrapper
    var add_button_mon      = $(".add_field_button_mon"); //Add button ID
    
    var wrapper_tue         = $(".input_fields_wrap_tue"); //Fields wrapper
    var add_button_tue      = $(".add_field_button_tue"); //Add button ID
    
    var wrapper_wed         = $(".input_fields_wrap_wed"); //Fields wrapper
    var add_button_wed       = $(".add_field_button_wed"); //Add button ID
    
    var wrapper_thr         = $(".input_fields_wrap_thr"); //Fields wrapper
    var add_button_thr      = $(".add_field_button_thr"); //Add button ID
    
    var wrapper_fri       = $(".input_fields_wrap_fri"); //Fields wrapper
    var add_button_fri      = $(".add_field_button_fri"); //Add button ID
    
    var wrapper_sat         = $(".input_fields_wrap_sat"); //Fields wrapper
    var add_button_sat      = $(".add_field_button_sat"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button_sun).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            var append_data = '<div class="add-containers" style="margin-bottom:3px;overflow:hidden;"><div class="col-xs-12 col-sm-5"><input class="form-control trns" name="schedular[' + x + '][from]" type="text"></div><div class="col-xs-12 col-sm-5"><input class="form-control trns" name="schedular[' + x + '][to]" type="text"></div><div class="col-sm-2 col-xs-12"><a href="#" class="remove_field"><i class="fa fa-close" style="margin-top:12px;color:darkred;"></i></a></div></div></div>';
            x++; //text box increment
            $(wrapper_sun).append(append_data); //add input box
        }
    });
    
    $(wrapper_sun).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parent().remove(); x--;
    })
    /**
    * @ Populate add schedules form popup
    */
    $('.add-schedules').click(function(){
        var uid = "{{ Auth::user()->id }}";
        var id = $(this).attr("rel");
        var csrf_field = "{{ csrf_token() }}";
        $.ajax({
            type: "POST",
            url: "{{ url('hospital/schedules') }}",
            data: {'id':id, '_token':csrf_field},
            beforeSend: function(){
                $('#scheduler_loader').css('display','block');
            },
            success: function(result){ 
                console.log(result);
                $('#scheduler_loader').css('display','none');
		$("#myModal").modal();
                $(".tab-content1").html(result);                
            },
            error:function(result){
                console.log(result);
            }
        });
    })
    /**
    * @ Save schedules
    */
    $('.save-schedule-btn').click(function(){
        var uid = "{{ Auth::user()->id }}";
        
        $.ajax({
            type: "POST",
            url: "{{ url('account/save-schedules') }}",
            data: $('#shcedule-form-sun').serialize(),
            success: function(result){ 
                console.log(result);
                $("#schedule-msg").html(result.message);
                $('#schedule-msg').show();
                setTimeout(function () {$('#schedule-msg').hide();},4000);
            },
            error:function(result){
                console.log(result);
            }
        });
    })
    /**
    * @ Populate add schedules form popup
    */
    $('.clinic-upload_gallry').click(function(){
        var uid = "{{ Auth::user()->id }}";
        var id = $(this).attr("rel");
        $("#clinic_id").val(id);
        return true;
    })
    /**
    * @ Save schedules
    */
</script>
@endsection
