<aside for="sidebar-tab">
    <ul class="nav navbar-tab navbar-stacked navbar-pills d-ul-left" role="tablist">
        <li><a href="{{ url('us/profile') }}">Personal Info</a></li>
        <li><a href="{{ url('us/claimed-profile') }}">Claimed Profile</a></li>
        @if(@$user->role->role_id == 2)
        <li><a href="{{ url('us/account/clinics') }}">Associate Clinic</a></li>
        @endif
        <li><a href="{{ url('us/account/my-ratings') }}">My Rating & Reviews</a></li>
        <li><a href="{{ url('us/account/change-password') }}">Change Password</a></li>
        <li><a href="{{ url('us/account/bookmarks') }}">Saved List</a></li>
        <li><a href="{{ url('us//') }}">Search</a></li>
        <li><a href="{{ url('us/account/points-collected') }}">Points Collected</a></li>
        <li><a href="{{ url('us/account/notifications') }}">Notifications</a></li>
		<li><a href="{{ url('us/account/recent-search') }}">Recent Search</a></li>
    </ul>				
</aside>
			
<script>
	$(document).ready(function(){
		var aurl = window.location.href; // Get the absolute url
		$('.d-ul-left li a').filter(function() { 
			return $(this).prop('href') === aurl;
		}).parent('li').addClass('active').siblings().removeClass('active');
		
	})
</script>			