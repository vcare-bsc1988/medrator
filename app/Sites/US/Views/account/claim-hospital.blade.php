@extends('us::layouts.home')

@section('us::content')
<style>
.my-list-group li{border:none;padding:5px 0px;}
.my-list-group1 li{padding:10px 0px;float:left;width:100%;}
.color-blue{color: #29b9e8;}
.radius-0{border-radius:0px;}
.w100{width:100%;}
.similar-img{border: 1px solid #ccc;border-radius: 50%;width:60px;height:60px;}
.review-box,.write-box1{position:relative;padding:10px !important;    border: 1px solid #f0f0f0;
    margin-top: 10px;}
.review-box:before{content:'';position:absolute;width:0;left:15px;right:0;top:-11px;border-bottom:10px solid #f0f0f0;border-left:10px solid transparent;border-right:10px solid transparent;z-index:1;}
.review-box:after{content:'';position:absolute;width:0;left:15px;right:0;top:-10px;border-bottom:10px solid #fff;border-left:10px solid transparent;border-right:10px solid transparent;z-index:2;}
.row[for="tabs-part"]{margin-top:30px;}
.mypanel{padding:15px;background:#e4f0fa;overflow:hidden;}
.tabs-01 li a{background:#e4f0fa;color:#5b5b5b !important;border-bottom:2px solid #5b5b5b;outline:none !important;}
.tabs-01 li.active a,.tabs-01 li.active a:focus{background:#31b0d5;color:white !important;border-top-color:#31b0d5;border-left-color:#31b0d5;border-right-color:#31b0d5;border-bottom:2px solid #1a8eb5;}
.write-box1:before{content:'';position:absolute;width:0;left:15px;right:0;top:-11px;border-bottom:10px solid #f0f0f0;border-left:10px solid transparent;border-right:10px solid transparent;z-index:1;}
.write-box1:after{content:'';position:absolute;width:0;left:15px;right:0;top:-10px;border-bottom:10px solid #fff;border-left:10px solid transparent;border-right:10px solid transparent;z-index:2;}
.service-containers{float:left;padding:10px;border: 1px dashed #a9a4a4; margin: 10px; color: #505050;}
#map {
    height: 400px;
    width: 100%;
}
</style>
<!--==================================
	 Header parts starts here
==================================-->
@include('us::layouts.header')									   
<!--==================================
	 Header parts ends here
  ==================================-->
<!--==================================
              service Quick links starts here
        ==================================--> 
<div class="first-section ptb">
    <div class="container">
        <div class="row">
			@include('us::common.errors')
			@if (session('success'))
			<div class="alert alert-success">
				<strong>Success!</strong> {{ session('success') }}
			</div>
			@endif 
            <div class="col-md-4 col-xs-12">
                <div class="thumbnail">
                    @if($hospital->image)
                        <img src="{{ $hospital->image }}" class="w100">
                    @else
                        <img src="{{ asset("public/images/med_icons/doctor.png") }}" class="img-circle w100">
                    @endif
                </div>
				<div class="claim-hospital">
				{{Form::open(['url' => 'account/claiming-hospital', 'method' => 'post'])}}
				{{ Form::hidden ('id', $hospital->id) }}
				{{ Form::submit('Claim this Profile') }}
				{{Form::close()}}
				</div>	
            </div>
                <div class="col-md-4 col-xs-12">
                        <h1 style="margin-top:0px;">{{ $hospital->name }}</h1>
                        <ul class="list-group my-list-group">
                          <li class="list-group-item"><i class="fa fa-map-marker"></i>&nbsp;&nbsp; {{ $hospital->address_one }}</li>
                          <li class="list-group-item"><i class="fa fa-phone"></i>&nbsp;&nbsp; {{ $hospital->phone }}</li>
                          <li>
                              {{ number_format($hospital->rating_cache, 1)}}
                              @for ($i=1; $i <= 5 ; $i++)
                                <span class="glyphicon glyphicon-star{{ ($i <= $hospital->rating_cache) ? '' : '-empty'}}"></span>
                              @endfor
                              ( {{$hospital->rating_count}} {{ str_plural('review', $hospital->rating_count)}} )
                          </li>
                        </ul>
                        <button class="btn btn-info radius-0 wreview">WRITE A REVIEW</button>
                        <div class="col-xs-12 review-box" style="display:none;">
                               {{Form::open()}}
                                <input id="ratings-hidden" name="rating" type="hidden">
                                 <div class="form-group">
                                    {{Form::text('title', null, array('id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter title'))}}
                                </div>
                                <div class="form-group">
                                    {{Form::textarea('comment', null, array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))}}
                                </div>
                                <div class="form-group">
                                    <span>Help the community by sharing your review of {{$hospital->name}}</span>
                                    <div class="stars starrr param1" data-rating="{{ old('username') }}"></div>
                                </div>
                                <div class="form-group">
                                        <input type="submit" class="btn btn-success radius-0" value="SUBMIT">
                                </div>
                                {{Form::close()}}
                        </div>
                </div>
                <div class="col-md-4 col-xs-12">
                        <div class="panel panel-info">
                                <div class="panel-heading"><h2 class="margin-0">Similar Hospitals</h2></div>
                                <div class="panel-body">
                                        <ul class="list-group my-list-group">
                                          <li class="list-group-item">
                                        <!-- 	<div class="col-xs-3">
                                                <img class="similar-img" src="images/expert7.jpg" />
                                                </div> -->
                                                <div class="col-xs-12">
                                                        <ul class="list-group my-list-group my-list-group1">
                                                          @if(!empty($similar_hospitals))
                                                          @foreach($similar_hospitals as $result)
                                                          <li class="list-group-item">
                                                                <div class="col-xs-3 padding-0  col-md-2 thumnail">
                                                                    @if($result['image'])
                                                                        <img src="{{ $result['image'] }}" class="img-circle" width="95">
                                                                    @else
                                                                        <img src="{{ asset("public/images/med_icons/doctor.png") }}" class="img-circle" width="95">
                                                                    @endif
                                                                </div>
                                                                <div class="col-xs-9 col-md-10">
                                                                    <h4><a href="{{ url('us/hospital/details') }}/{{ \Crypt::encrypt($result['id']) }}">{{$result['name'] }}</a></h4>
                                                                    <p>
                                                                        {{ number_format($result['rating'], 1) }}
                                                                        @for ($i=1; $i <= 5 ; $i++)
                                                                            <span class="glyphicon glyphicon-star{{ ($i <= $result['rating']) ? '' : '-empty'}}"></span>
                                                                        @endfor
                                                                        ( {{$result['review_count']}} {{ str_plural('Review', $result['review_count'])}} )
                                                                    </p>
                                                                    <p class="color-grey">{{ $result['address_one'] }}</p>
                                                                    <p>
                                                                        <span class="specility">
                                                                            @if(isset($result['services']) && !empty($result['services']))
                                                                            @for($i=0; $i < count($result['services']); $i++)
                                                                               @if($i>0) & @endif {{ $result['services'][$i] }} 
                                                                            @endfor
                                                                            @endif
                                                                        </span>
                                                                        
                                                                    </p>
                                                                    <p class="color-grey">@if($result['24_hours_emergency'])<span class="color-grey">24 Hours Open</span> @endif</p>
                                                                </div>
                                                          </li>
                                                          @endforeach
                                                          @endif
                                                        </ul>
                                                </div>
                                          </li>
                                        </ul>
                                </div>
                        </div>
                </div>
        </div>
        <!-- biography part START -->
        <div for="biography" class="row">
                <h1>About</h1>
                <hr style="border-top:2px solid #29b9e8;width:50px;float:left;">
                <div class="clearfix"></div>
                <p>{!! $hospital->about !!}</p>
        </div>
        <!-- biography part END -->

        <!-- tabs part START -->
        <div class="row" for="tabs-part">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs-01" role="tablist">
                      <li role="presentation" class="active"><a href="#review" aria-controls="review" role="tab" data-toggle="tab">Review({{$hospital->rating_count}})</a></li>
                      <li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Specialties</a></li>
                      <li role="presentation"><a href="#facilities" aria-controls="facilities" role="tab" data-toggle="tab">Facilities</a></li>
                      <li role="presentation"><a href="#hospitals" aria-controls="hospitals" role="tab" data-toggle="tab">Associated Doctors</a></li>
                      <li role="presentation"><a href="#map-location" aria-controls="map-location" role="tab" data-toggle="tab">Location</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content" >
                    <div role="tabpanel" class="tab-pane active mypanel" id="review">
                        
                        <div class="col-md-6 col-xs-12">
                            <ul class="list-group my-list-group my-list-group1">
                              @foreach($reviews as $review)
                                <li class="list-group-item">
                                    <div class="col-xs-3   col-md-2 thumnail">
                                        @if($review->user->user->image)
                                            <img src="{{ $review->user->user->image }}" class="img-circle" style="width:50px;">
                                        @else
                                            <img src="{{ asset("public/images/med_icons/doctor.png") }}" class="img-circle" style="width:50px;">
                                        @endif
                                    </div>
                                    <div class="col-xs-9 col-md-10">
                                            <h4>{{ $review->user->user->name }}</h4>
                                            <h1>{{ $review->title }}</h1>
                                            <p class="color-grey write-box1">{!! $review->comment !!}</p>

                                    </div>
                                    <div class="col-xs-12 text-right">
                                        <i>Rating:</i>{{ $review->rating }}
                                        @for ($i=1; $i <= 5 ; $i++)
                                            <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                                        @endfor
                                    </div>
                                </li>
                              @endforeach
                            </ul>
                        </div>                       
                    </div>
                    <div role="tabpanel" class="tab-pane mypanel" id="services">
                        @if($hospital->specialties()->get()->count()>0)
                        <div class="service-containers">
                            @foreach($hospital->specialtyList as $specialty)
                            <br>{{ $specialty->name }}
                            @endforeach 
                        </div>
                        @endif    
                    </div>
                    <div role="tabpanel" class="tab-pane mypanel" id="facilities">
                        <div class="specialties-containers">
                            <p>Cashless Mediclaim @if($hospital->cashless_mediclaim) Yes @else No @endif</p>
                            <p>ICU @if($hospital->icu_facility) Yes @else No @endif</p>
                            <p>24 Hour Emergency @if(1) Yes @else No @endif</p>
                            <p>Diagnostic Lab Facility @if($hospital->diagnostic_lab_facility) Yes @else No @endif</p>
                        </div>  
                    </div>
                    <div role="tabpanel" class="tab-pane mypanel" id="hospitals">
                         @if($hospital->doctors()->get()->count()>0)
                        @foreach($hospital->doctors as $doctors)
                        <div class="act-time">                                      
                            <div class="activity-body act-in">
                                <span class="arrow"></span>
                                <div class="col-xs-12 padding-0">
                                    <div class="col-md-1" style="padding:0px;">
                                        <a href="javascript::void(0)" class="activity-img activity-img1">
                                            @if($doctors->image)
                                            <img class="avatar" width="70" src="{{ $doctors->image }}">
                                            @else
                                            <img class="avatar" width="70" src="{{ asset('public/admin/img/user-placeholder.png') }}">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="col-md-10">
                                        <p class="attribution act-in" style="margin-bottom:0px;"><a href="javascript::void(0)">{{ $doctors->name }}</a></p>
                                        <p style="margin-bottom:0px;">{{ $doctors->address_one }}</p>
                                        <p class="margin-0">
                                            <label><b>Consulting Fee </b></label> {{ $doctors->consultation_fee}}
                                        </p>
                                    </div>
                                    <div class="clearfix" style="margin-bottom:10px;"></div>
                                    <p> 
                                        @for ($i=1; $i <= 5 ; $i++)
                                        <span class="glyphicon glyphicon-star{{ ($i <= $hospital->rating_cache) ? '' : '-empty'}}"></span>
                                        @endfor
                                        {{ $doctors->rating_count }} Reviews
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <p>No hospital was found.</p>
                        @endif
                    </div>
                    <div role="tabpanel" class="tab-pane mypanel" id="map-location">
                        <div id="map"></div>                   
                    </div>
                </div>

                        </div>
        </div>
        <!-- tabs part END -->
    </div>
</div>
<!--End services popup -->
{{ Form::close() }}
<!-- =================================
		APP DOWNLOAD PART END 
	================================== -->
@include('us::layouts.footer')
{{Html::script('public/js/expanding.js')}}
{{Html::script('public/js/starrr.js')}}
<script type="text/javascript">
    $('.wreview').click(function(){
            $('.review-box').slideToggle('slow');
    })
    $(function(){
        // initialize the autosize plugin on the review text area
        $('#new-review').autosize({append: "\n"});

        var reviewBox = $('#post-review-box');
        var newReview = $('#new-review');
        var openReviewBtn = $('#open-review-box');
        var closeReviewBtn = $('#close-review-box');
        var ratingsField1 = $('#ratings-hidden');
            var ratingsField2 = $('#ratings-hidden');

        openReviewBtn.click(function(e)
        {
          reviewBox.slideDown(400, function()
            {
              $('#new-review').trigger('autosize.resize');
              newReview.focus();
            });
          openReviewBtn.fadeOut(100);
          closeReviewBtn.show();
        });

        closeReviewBtn.click(function(e)
        {
          e.preventDefault();
          reviewBox.slideUp(300, function()
            {
              newReview.focus();
              openReviewBtn.fadeIn(200);
            });
          closeReviewBtn.hide();

        });

        // If there were validation errors we need to open the comment form programmatically 
        @if($errors->first('comment') || $errors->first('rating'))
          openReviewBtn.click();
        @endif

        // Bind the change event for the star rating - store the rating value in a hidden field
        $('.param').on('starrr:change', function(e, value){
          ratingsField.val(value);
        });
    });
    
    function initMap() {
        var uluru = {lat: <?=$hospital->latitude?>, lng: <?=$hospital->longitude?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
</script>
@endsection
