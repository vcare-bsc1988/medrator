<?php

namespace App\Sites\US\Controllers;
use App\Http\Controllers\Controller;

class IndexController extends Controller {
    public function index(){
        return view('us::index.index');
    }
}
