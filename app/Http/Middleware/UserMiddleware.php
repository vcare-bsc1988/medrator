<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Http\Controllers\HomeController;
class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $helper;
    protected $homeController;
    function __construct(Helper $helper,HomeController $homeController) {
       $this->helper = $helper;
       $this->homeController = $homeController;
    }
    public function handle($request, Closure $next, $guard = null)
    {
        
        if (!Auth::guard($guard)->check()) {
            /****
            * Getting user related interest blogs by Baleshwar
            */
            $posts = $this->helper->userInterestblogs();
            $data['blogs'] = $posts;
            $data['specialties'] = \App\Model\Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
            $data['tests'] = \App\Model\Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
            $data['address'] = session('address');
            $result = $this->homeController->entityList($data['address'],$request);
            $data = array_merge($data,$result);
            session()->flash('login_popup',1);
            $request->session()->put('redirected_from_url', url()->previous());
			return response(view('home', $data));
        }
        return $next($request);
    }
}
