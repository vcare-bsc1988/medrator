<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            return response(view('admin::login')); 
        }
        if(Auth::check()){
            $user = Auth::user();
            if(!$user->hasRole(['administrator','super-admin','data-entry-user','moderator','manager'])){
               return response(view('admin::login')); 
            }
        }
        return $next($request);
    }
}
