<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'session/stor_city',
        'us/session/stor_city',
        'find/autocomplet',
		'name/autocomplet',
        'get_city',
        'api/v1.0/*',
        'admin/*',
        'api/beta/*',
		'news-letter/subscriber',
    ];
}
