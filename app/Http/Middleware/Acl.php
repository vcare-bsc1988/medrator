<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class Acl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(Auth::check()){
            if(Auth::user()->hasRole(['administrator','super-admin','data-entry-user','moderator','manager'])){
               return redirect('admin/dashboard'); 
            }
        }
		return $next($request);
    }
}
