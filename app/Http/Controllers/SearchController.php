<?php
namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Requests;
use DB;
use App\Model\Specialty;
use Vcareall\Admin\Helper;
class SearchController extends Controller
{
	protected $client;
        protected $helper;
	public function __construct(\Solarium\Client $client)
	{
            $this->client = $client;
            $this->helper = new \App\Helpers\Helper;
	}
	public function index(Request $request)
	{
		try {
			
			$validator = Validator::make($request->all(), 
			[
				'q'=>'required',
			]);
			# @If input passes validation - store the review in DB, otherwise return to product page with error message 
			if (!$validator->passes()) {
					abort(404) ;        
			}
			$query = $this->client->createSelect();
			$helper = $query->getHelper();
			/**
			 * Applying name filter.
			 */
			if(isset($request->input('filters')['name']) && $request->input('filters')['name']!=null){
				$request->q = $request->input('filters')['name'];
				$query->createFilterQuery('name')->setQuery('name:%P1%', array($request->input('filters')['name']));  
			}else{
				/***
				 * Matchin indexes.
				 */
				$special_characters = array('/', '&', '!', '.', '-', '+');
				$q = str_replace($special_characters,'',$request->q);
				$query->setQuery('*'.$q.'*');
				$query->setQueryDefaultOperator('AND');
			}
			$address = $request->session()->get('address');
			$latitude = ($address['latitude']?$address['latitude']:28.7040592);
			$longitude = ($address['longitude']?$address['longitude']:77.10249019999992);                              
			
			# Adding distance field
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			); 
			/**
			 * Applying distance filter.
			 */
			$settings = \App\Model\Setting::where('title','radius')->get()->first();              
			if(isset($request->input('filters')['max_distance'])){
				$distance = substr($request->input('filters')['max_distance'],0,-2);
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($distance)
					)
				);
			}else{
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($settings->value)
					)
				);
			}
			/**
			 * Sort by max/min fee.
			 */
			if(isset($request->input('filters')['min_fee']) && $request->input('filters')['max_fee']){
				$minFee = $request->input('filters')['min_fee'];
				$maxFee = $request->input('filters')['max_fee'];
				$query->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $minFee, $maxFee));
			}
			/**
			 * Applying rating filter/
			 */
			if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
				$rating = $request->input('filters')['rating'];
				$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
			}
			/***
			 * Applying entity filter/
			 */
			if(isset($request->input('filters')['entity']) && $request->input('filters')['entity']!=0){
				$query->addFilterQuery(array('key'=>'role', 'query'=>'role:'.$request->input('filters')['entity'], 'tag'=>'exclude'));
			}				
			/****
			 * Applying sorting.
			 */
			$query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
			/**
			 * Paggination 
			 */
			$page = 1;
			$limit = 25;
			if($request->input('page')) {
				$page = $request->input('page');
			}
			$start=($page - 1)*$limit;
			$query->setStart($start);
			$query->setRows($limit);
			
			$resultset = $this->client->select($query);
			$paginator = [
				'result' => $resultset,
				'count' => $resultset->getNumFound(),
				'limit' => $limit,
				'page' => $page,
				'query' => $query
			];
			$resultset = $resultset->getData()['response']['docs'];				
			# Storing filters into the session value
			if($request->input('filters')){
				$request->session()->put('filters', $request->input('filters'));
			}else{
				$filters = [
						'max_distance' => $settings->value,
						'rating' =>'',
						'sorting' =>'relevence'
				];
				$request->session()->put('filters', $filters);
			}
			$i=0;
			$resultsetFinal = [];
			foreach($resultset as $temp){ 
				/***
				 * Finding doctor associated with hospital.
				 */
				if(isset($temp['role']) && $temp['role'] ==5 && (isset($request->input('filters')['entity']) && $request->input('filters')['entity']!=5)){
					### Finding associated doctor
					$assoDocResultset = [];
					$hospital = \App\Model\Hospital::find($temp['id']);
					if($hospital->doctors()->get()->count()>0){
						$doctors = $hospital->doctors()->get();
						$ids = "(";
						$i=1;
						foreach($doctors as $var){
							$ids.=$var->id;
							if(count($doctors)>$i++){
											$ids.=' or ';
							}
						}
						$ids.=')';
						$assoDocQuery = $this->client->createSelect();
						$assoDocHelper = $assoDocQuery->getHelper();
						$special_characters = array('/', '&', '!', '.', '-', '+');
						$q = str_replace($special_characters,'',$request->q);
						$assoDocQuery->setQuery('*'.$q.'*');
						$assoDocQuery->setQueryDefaultOperator('AND');
						# Adding distance field
						$assoDocQuery->addField('distance:' . $assoDocHelper->geodist(
								'latlon', 
								doubleval($latitude), 
								doubleval($longitude)
							)
						);
						$assoDocQuery->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$ids, 'tag'=>'exclude'));	
						$assoDocResultset = $this->client->select($assoDocQuery);				
						$assoDocResultset = $assoDocResultset->getData()['response']['docs'];
					}
					$resultsetFinal[] = $temp;
					foreach($assoDocResultset as $tempAssoDoc){
						$tempAssoDoc['distance'] = Helper::distance($latitude,$longitude,$temp['latitude'],$temp['longitude'],'K');
						$tempAssoDoc['rating'] = $temp['rating'];
						$tempAssoDoc['review_count'] = $temp['review_count'];
						$tempAssoDoc['rating_one_star_count'] = $temp['rating_one_star_count'];
						$tempAssoDoc['rating_two_star_count'] = $temp['rating_two_star_count'];
						$tempAssoDoc['rating_three_star_count'] = $temp['rating_three_star_count'];
						$tempAssoDoc['rating_four_star_count'] = $temp['rating_four_star_count'];
						$tempAssoDoc['rating_five_star_count'] = $temp['rating_five_star_count'];
						$tempAssoDoc['address_one'] = $temp['address_one'];
						$resultsetFinal[] = $tempAssoDoc;
					}
				}
				else{
						$resultsetFinal[] = $temp;
				}
			}
			/****
			 * Adding qualification in case of doctor.
			 */
			for($i=0; $i<count($resultsetFinal); $i++){
				if(isset($resultsetFinal[$i]['role']) && $resultsetFinal[$i]['role'] == 2):
					$resultsetFinal[$i]['qualifications']= \App\Model\Doctor::getQualifications($resultsetFinal[$i]['id']);
				endif;
			}
			/**
			 * Remove duplicat entries.
			 */
			$result = $resultsetFinal;
			/** Commented b/c doctor, hospital, diagnostic can have same id.
			 * $result = Helper::removeDuplicateArr($resultsetFinal);
			 */
			 
			/**
			 * Save recent search.
			 */
			if(auth()->user()){
				$request->request->add([
					'address_one' => $address['address_one'],
					'entity' => 'free-text',
				]);
				$this->saveSearch(auth()->user()->id, $request);               
			}
			$results =[
				'q'=>$request->input('q'),
				'results'=>$result,
				'location'=>$request->input('location'),
				'latitude'=>$request->input('latitude'),
				'longitude'=>$request->input('longitude'),
				'locality'=>$request->input('locality'),
				'city_name'=>$request->input('city_name'),
				'state_name'=>$request->input('state_name'),
				'country_name'=>$request->input('country_name'),
				'pincode'=>$request->input('pincode'),
				'services' => \App\Model\Service::take(5)->get(),
				'services_all' => \App\Model\Service::skip(5)->take(134)->get(),
				'paginator' =>$paginator,
				'default_distance' => $settings->value
			];
		} 
		catch (\Solarium\Exception $e) {
			$response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
			$response = json_encode($response);
			return $response;
		}
		return view('search.index', $results);
	}
	public function appendValue($data, $type, $element)
	{
		// operate on the item passed by reference, adding the element and type
		foreach ($data as $key => & $item) {
			$item[$element] = $type;
		}
		return $data;		
	}
	public function doctor($state,$specialty, Request $request)
	{
            try {
                $query = $this->client->createSelect();
                $helper = $query->getHelper();
				if(isset($request->input('filters')['name']) && $request->input('filters')['name']!=null){
					$query->createFilterQuery('name')->setQuery('name:%P1%', array(stripcslashes($request->input('filters')['name'])));    
				}
				# Adding distance field
				$address = $request->session()->get('address');
                $latitude = ($address['latitude']?$address['latitude']:28.7040592);
                $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
                $query->addField('distance:' . $helper->geodist(
                        'latlon', 
                        doubleval($latitude), 
                        doubleval($longitude)
                    )
                ); 
                /**
                 * Applying filter
                 */
                $settings = \App\Model\Setting::where('title','radius')->get()->first();              
                if(isset($request->input('filters')['max_distance']) && $request->input('filters')['max_distance'] !='0KM'){
                    $distance = substr($request->input('filters')['max_distance'],0,-2);
                    $query->createFilterQuery('distance')->setQuery(
                        $helper->geofilt(
                            'latlon', 
                            doubleval($latitude),
                            doubleval($longitude),
                            doubleval($distance)
                        )
                    );
                }else{
                    $query->createFilterQuery('distance')->setQuery(
                        $helper->geofilt(
                            'latlon', 
                            doubleval($latitude),
                            doubleval($longitude),
                            doubleval($settings->value)
                        )
                    );
                }
                if(isset($request->input('filters')['min_fee']) && $request->input('filters')['max_fee']){
                    $minFee = $request->input('filters')['min_fee'];
                    $maxFee = $request->input('filters')['max_fee'];
                    $query->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $minFee, $maxFee));
                }
                if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
                    $rating = $request->input('filters')['rating'];
                    $query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
                }
                if(isset($request->input('filters')['max_experience']) && (int)$request->input('filters')['max_experience'] !=0){
                    $maxExp = (int)$request->input('filters')['max_experience'];
                    $query->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', $maxExp, 200));
                }
                if(isset($request->input('filters')['working_day']) && $request->input('filters')['working_day']!=""){
                    $weekDays = "(";
                    $i=1;
                    foreach($request->input('filters')['working_day'] as $var){
                        $weekDays.=$var;
                        if(count($request->input('filters')['working_day'])>$i++){
                            $weekDays.=' or ';
                        }				
                    }
                    $weekDays.=')';
                    $query->addFilterQuery(array('key'=>'week_day', 'query'=>'days_of_week:'.$weekDays, 'tag'=>'exclude'));
                }
                if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
                    $query->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$request->input('filters')['gender'], 'tag'=>'exclude'));
                }
                if(isset($request->input('filters')['services']) && $request->input('filters')['services']!=""){
                    $services = "(";
                    $i=1;
                    foreach($request->input('filters')['services'] as $var){
                        $services.=$var;
                        if(count($request->input('filters')['services'])>$i++){
                                $services.=' or ';
                        }
                    }
                    $services.=')';
                    $query->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
                }				
                if(isset($request->input('filters')['specialties']) && $request->input('filters')['specialties']!=""){
                    $specialties = "(";
                    $i=1;
                    foreach($request->input('filters')['specialties'] as $var){
                        $specialties.=$var;
                        if(count($request->input('filters')['specialties'])>$i++){
                                $specialties.=' or ';
                        }
                    }
                    $specialties.=')';
                    $query->addFilterQuery(array('key'=>'Specialties', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
                }
                elseif($specialty){
					$specialties = Specialty::where('name', $specialty)->get()->first()->id;
					$query->addFilterQuery(array('key'=>'Specialties', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
                }
                /****
                 * Applying sorting..
                 */
                $query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
                
				# Storing filters into the session value
                if($request->input('filters')){ 
                        $request->session()->put('filters', $request->input('filters'));
                }else{
                        $specialtyItem = Specialty::where('name', $specialty)->get()->first();
                        $filters = [
                                'max_distance' => $settings->value,
                                'rating' =>'',
                                'sorting' =>'relevence',
                                'specialties' =>[$specialtyItem->id]
                        ];
                        $request->session()->put('filters', $filters);
                }

				/**
				 * Paggination 
				 */
				$page = 1;
				$limit = 25;
				if($request->input('page')) {
					$page = $request->input('page');
				}
				$start=($page - 1)*$limit;
				$query->setStart($start);
				$query->setRows($limit);
				
				$resultset = $this->client->select($query);
				$paginator = [
					'result' => $resultset,
					'count' => $resultset->getNumFound(),
					'limit' => $limit,
					'page' => $page,
					'query' => $query
				];			
                $resultset = $resultset->getData()['response']['docs'];
                $i=0;
				$doctorArr = [];
                foreach($resultset as $temp){ 
					if(isset($temp['role']) && $temp['role'] ==2){
						$doctorArr[] = $temp;
					}
                    /***
					 * Finding doctor associated with clinic
					 */
					if(isset($temp['role']) && $temp['role'] ==5){
						$assoDocResultset = [];
						$hospital = \App\Model\Hospital::find($temp['id']);
						if($hospital->doctors()->get()->count()>0){
							/** Finding doctor working in this Hospital
							 *
							 */
							$doctors = $hospital->doctors()->get();
							$ids = "(";
							$i=1;
							foreach($doctors as $var){
								$ids.=$var->id;
								if(count($doctors)>$i++){
										$ids.=' or ';
								}
							}
							$ids.=')';
							$assoDocQuery = $this->client->createSelect();
							$assoDocHelper = $assoDocQuery->getHelper();
							$assoDocQuery->addField('distance:' . $assoDocHelper->geodist(
									'latlon', 
									doubleval($hospital->latitude), 
									doubleval($hospital->longitude)
								)
							); 
							$assoDocQuery->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$ids, 'tag'=>'exclude'));
							$assoDocQuery->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
							if(isset($request->input('filters')['min_fee']) && $request->input('filters')['max_fee']){
								$minFee = $request->input('filters')['min_fee'];
								$maxFee = $request->input('filters')['max_fee'];
								$assoDocQuery->createFilterQuery('consultation_fee')->setQuery($assoDocHelper->rangeQuery('consultation_fee', $minFee, $maxFee));
							}
							if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
								$rating = $request->input('filters')['rating'];
								$assoDocQuery->createFilterQuery('rating')->setQuery($assoDocHelper->rangeQuery('rating', $rating, 5));
							}
							if(isset($request->input('filters')['experience']) && $request->input('filters')['experience']!=""){
								$assoDocQuery->createFilterQuery('experience')->setQuery($assoDocHelper->rangeQuery('experience', 0, $max));
							}
							if(isset($request->input('filters')['working_day']) && $request->input('filters')['working_day']!=""){
								$weekDays = "(";
								$i=1;
								foreach($request->input('filters')['working_day'] as $var){
									$weekDays.=$var;
									if(count($request->input('filters')['working_day'])>$i++){
										$weekDays.=' or ';
									}				
								}
								$weekDays.=')';
								$query->addFilterQuery(array('key'=>'week_day', 'query'=>'days_of_week:'.$weekDays, 'tag'=>'exclude'));
							}
							if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
								$assoDocQuery->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$request->input('filters')['gender'], 'tag'=>'exclude'));
							}
							if(isset($request->input('filters')['services']) && $request->input('filters')['services']!=""){
								$services = "(";
								$i=1;
								foreach($request->input('filters')['services'] as $var){
									$services.=$var;
									if(count($request->input('filters')['services'])>$i++){
											$services.=' or ';
									}
								}
								$services.=')';
								$assoDocQuery->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
							}							
							if(isset($request->input('filters')['specialties']) && $request->input('filters')['specialties']!=""){
								$specialties = "(";
								$i=1;
								foreach($request->input('filters')['specialties'] as $var){
									$specialties.=$var;
									if(count($request->input('filters')['specialties'])>$i++){
											$specialties.=' or ';
									}
								}
								$specialties.=')';
								$assoDocQuery->addFilterQuery(array('key'=>'Specialties', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
							}
							elseif($specialty){
								$specialties = Specialty::where('name', $specialty)->get()->first()->id;
								$assoDocQuery->addFilterQuery(array('key'=>'Specialties', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
							}
							/****
							 * Applying sorting..
							 */
							$assoDocQuery = $this->helper->solriumSort($assoDocQuery, $request,$latitude,$longitude);
							
							### Paggination
							$start = 0;
							$limit = 10;
							if($request->input('start')) {
								$start = $request->input('start');
								$assoDocQuery->setStart($start);
							}
							$assoDocQuery->setRows($limit);
							$assoDocResultset = $this->client->select($assoDocQuery);				
							$assoDocResultset = $assoDocResultset->getData()['response']['docs'];
						}
						foreach($assoDocResultset as $tempAssoDoc){
							$tempAssoDoc['distance'] = Helper::distance($latitude,$longitude,$temp['latitude'],$temp['longitude'],'K');
							$tempAssoDoc['rating'] = $temp['rating'];
                            $tempAssoDoc['review_count'] = $temp['review_count'];
                            $tempAssoDoc['rating_one_star_count'] = $temp['rating_one_star_count'];
                            $tempAssoDoc['rating_two_star_count'] = $temp['rating_two_star_count'];
                            $tempAssoDoc['rating_three_star_count'] = $temp['rating_three_star_count'];
                            $tempAssoDoc['rating_four_star_count'] = $temp['rating_four_star_count'];
                            $tempAssoDoc['rating_five_star_count'] = $temp['rating_five_star_count'];
                            $tempAssoDoc['address_one'] = $temp['address_one'];
							$doctorArr[] = $tempAssoDoc;
						}
					}
				}  			
				/**
				 * Remove duplicat entries.
				 */
				$doctorArr = Helper::removeDuplicateArr($doctorArr);
				$result = [];
				for($i=0; $i<(count($doctorArr)); $i++){
					if($i>0 && $doctorArr[$i-1]['id'] !=$doctorArr[$i]['id']){
						$result[$i] = $doctorArr[$i];
						$result[$i]['qualifications']= \App\Model\Doctor::getQualifications($doctorArr[$i]['id']);
					}elseif($i==0){
						$result[$i] = $doctorArr[$i];
						$result[$i]['qualifications']= \App\Model\Doctor::getQualifications($doctorArr[$i]['id']);
					}
				}
				/**
				 * Save recent search
				 */
				if(auth()->user() && $request->session()->get('address')){													
					$request->country_name = $address['country_name'];	
					$request->request->add([
						'q' => $specialty,
						'locality' => $address['locality'],
						'address_one' => $address['address_one'],
						'latitude' => $address['latitude'],
						'longitude' => $address['longitude'],
						'city_name' => $address['city_name'],
						'state_name' => $address['state_name'],
						'country_name' => $address['country_name'],
						'entity' => 'doctor',
					]);
					$this->saveSearch(auth()->user()->id, $request);               
				}
				$results =[
                    'results'=>$result,
					'q'=>$specialty,
                    'location'=>session('address')['address_one'],
                    'latitude'=>session('address')['latitude'],
                    'longitude'=>session('address')['longitude'],
                    'locality'=>session('address')['locality'],
                    'city_name'=>session('address')['city_name'],
                    'state_name'=>session('address')['state_name'],
                    'country_name'=>session('address')['country_name'],
                    'pincode'=>session('address')['pincode'],
                    'services' => \App\Model\Service::take(5)->get(),
                    'services_all' => \App\Model\Service::skip(5)->take(134)->get(),
					'specialties' => \App\Model\Specialty::take(5)->get(),
                    'specialties_all' => \App\Model\Specialty::orderBy('name','asc')->skip(5)->take(2000)->get(),
					'settings' => $settings,
					'paginator' =>$paginator,
					'default_distance' => $settings->value
                ];
			} 
            catch (\Solarium\Exception $e) {
                $response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
                $response = json_encode($response);
                return $response;
            }
            return view('search.doctors', $results);
	}
	public function doctor_details($name_slug, Request $request)
	{   
            if(!$name_slug){
                abort(404);
            }
            /**
             * Finding similar doctor
             */
            $query = $this->client->createSelect();
            $helper = $query->getHelper();
            $query->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
            $doctor =\App\Model\Doctor::where('name_slug', $name_slug)->first();
            /**
			 *Adding distance field
			 */
            $address = $request->session()->get('address');
            $latitude = ($address['latitude']?$address['latitude']:28.7040592);
            $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
			$query->addField('distance:' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                )
            ); 
            /**
             * Start filter.
             */
            $settings = \App\Model\Setting::where('title','radius')->get()->first();              
            //return [$latitude,$longitude,$settings->value];
            
			$query->createFilterQuery('distance')->setQuery(
                $helper->geofilt(
                    'latlon', 
                    doubleval($latitude),
                    doubleval($longitude),
                    doubleval($settings->value)
                )
            );
			/****
             * Finding doctor specialties
             */
            $specialties = "(";
            $i=1;
            foreach( $doctor->specialties as $var){
                $specialties.=$var->speciality_id;
                if($doctor->specialties->count()>$i++){
                    $specialties.=' or ';
                }
            }
            $specialties.=')';
            $query->addFilterQuery(array('key'=>'Specialties', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
            /****
			 * Applying sorting.
			 */
			$request->request->add([
				'filters' =>['sorting'=>'distance-asc']
			]);
			$query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
			
            $query->setStart(0);
            $query->setRows(3);
			
            ## Paggination finished
            $resultset = $this->client->select($query);				

			// Get all reviews that are not spam for the product and paginate them
			$reviews = $doctor->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100);
			$data = [
                'doctor'=>$doctor,
                'reviews'=>$reviews,
                'similar_doctors'=>$resultset,
				'latitude'=>$latitude,
                'longitude'=>$longitude
            ];
            return view('search.doctor_details', $data);
	}
        public function hospital($state,$distance, Request $request){
            try {
                $query = $this->client->createSelect();
                $helper = $query->getHelper();
				if(isset($request->input('filters')['name']) && $request->input('filters')['name']!=null){
					$query->createFilterQuery('name')->setQuery('name:%P1%', array(stripcslashes($request->input('filters')['name'])));  
				}
                $query->addFilterQuery(array('key'=>'role', 'query'=>'role:5', 'tag'=>'exclude'));
                # Adding distance fielld
                $address = $request->session()->get('address');
                $latitude = ($address['latitude']?$address['latitude']:28.7040592);
                $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
                $query->addField('distance:' . $helper->geodist(
                        'latlon', 
                        doubleval($latitude), 
                        doubleval($longitude)
                    )
                ); 
                #Finished distance feild
                /**
                 * Start filet.
                 */
                if(isset($request->input('filters')['max_distance']) && $request->input('filters')['max_distance'] !='0KM'){
                    $distance = substr($request->input('filters')['max_distance'],0,-2);
                    $query->createFilterQuery('distance')->setQuery(
                        $helper->geofilt(
                            'latlon', 
                            doubleval($latitude),
                            doubleval($longitude),
                            doubleval($distance)
                        )
                    );
                }else{
                    $query->createFilterQuery('distance')->setQuery(
                        $helper->geofilt(
                            'latlon', 
                            doubleval($latitude),
                            doubleval($longitude),
                            doubleval($distance)
                        )
                    );
                }
                #### For sorting by distance
                if(isset($request->input('filters')['min_fee']) && $request->input('filters')['max_fee']){
                    $minFee = $request->input('filters')['min_fee'];
                    $maxFee = $request->input('filters')['max_fee'];
                    $query->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $minFee, $maxFee));
                }
                if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
					$rating = $request->input('filters')['rating'];
					$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
				}
                if(isset($request->input('filters')['experience']) && $request->input('filters')['experience']!=""){
                    $query->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', 0, $max));
                }
                if(isset($request->input('filters')['working_day']) && $request->input('filters')['working_day']!=""){
                    $weekDays = "(";
					$i=1;
                    foreach($request->input('filters')['working_day'] as $var){
                        $weekDays.=$var;
                        if(count($request->input('filters')['working_day'])>$i++){
                            $weekDays.=' or ';
                        }				
                    }
                    $weekDays.=')';
                    $query->addFilterQuery(array('key'=>'week_day', 'query'=>'days_of_week:'.$weekDays, 'tag'=>'exclude'));
                }
                if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
                    $query->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$request->input('filters')['gender'], 'tag'=>'exclude'));
                }
                if(isset($request->input('filters')['services']) && $request->input('filters')['services']!=""){
                    $services = "(";
                    $i=1;
                    foreach($request->input('filters')['services'] as $var){
                        $services.=$var;
                        if(count($request->input('filters')['services'])>$i++){
                                $services.=' or ';
                        }
                    }
                    $services.=')';
                    $query->addFilterQuery(array('key'=>'service', 'query'=>'service_id:'.$services, 'tag'=>'exclude'));
                }
				if(isset($request->input('filters')['specialties']) && $request->input('filters')['specialties']!=""){
					$specialties = "(";
					$i=1;
					foreach($request->input('filters')['specialties'] as $var){
						$specialties.=$var;
						if(count($request->input('filters')['specialties'])>$i++){
								$specialties.=' or ';
						}
					}
					$specialties.=')';
					$query->addFilterQuery(array('key'=>'Specialties', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
				}
                /**
                 * Filter 24 Hours Emergency
                 */
                if(isset($request->input('filters')['hours24_emergency']) && $request->input('filters')['hours24_emergency']==1){
                
                    $query->addFilterQuery(array('key'=>'24_hours_emergency', 'query'=>'24_hours_emergency:'.$request->input('filters')['hours24_emergency'], 'tag'=>'exclude'));
                }
                /**
                 * Filter ICU Facility
                 */
                if(isset($request->input('filters')['icu_facility']) && $request->input('filters')['icu_facility']==1){
                
                    $query->addFilterQuery(array('key'=>'icu_facility', 'query'=>'icu_facility:'.$request->input('filters')['icu_facility'], 'tag'=>'exclude'));
                }
                /**
                 * Filter Mediclaim
                 */
                if(isset($request->input('filters')['cashless_mediclaim']) && $request->input('filters')['cashless_mediclaim']==1){
                
                    $query->addFilterQuery(array('key'=>'cashless_mediclaim', 'query'=>'cashless_mediclaim:'.$request->input('filters')['cashless_mediclaim'], 'tag'=>'exclude'));
                }
                /****
                 * Applying sorting..
                 */
                $query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
                /**
				 * Paggination 
				 */
				$page = 1;
				$limit = 25;
				if($request->input('page')) {
					$page = $request->input('page');
				}
				$start=($page - 1)*$limit;
				$query->setStart($start);
				$query->setRows($limit);
				
				$resultset = $this->client->select($query);
				$paginator = [
					'result' => $resultset,
					'count' => $resultset->getNumFound(),
					'limit' => $limit,
					'page' => $page,
					'query' => $query
				];			
                $resultset = $resultset->getData()['response']['docs'];
				# Storing session valeue
				if($request->input('filters')){
					$request->session()->put('filters', $request->input('filters'));
                }else{
					$filters = [
						'max_distance' => $distance.'KM',
						'rating' =>'',
						'min_fee' =>'',
						'max_fee' =>'',
						'sorting' =>'relevence'
					];
					$request->session()->put('filters', $filters);
				}
				/**
				 * Save search
				 */
				if(auth()->user() && $request->session()->get('address')){														
					$request->country_name = $address['country_name'];	
					$request->request->add([
						'q' => $distance,
						'locality' => $address['locality'],
						'address_one' => $address['address_one'],
						'latitude' => $address['latitude'],
						'longitude' => $address['longitude'],
						'city_name' => $address['city_name'],
						'state_name' => $address['state_name'],
						'country_name' => $address['country_name'],
						'entity' => 'hospital',
					]);
					$this->saveSearch(auth()->user()->id, $request);               
				}
                $results =[
                    'q'=>session('specialty'),
                    'results'=>$resultset,
                    'location'=>session('address')['address_one'],
                    'latitude'=>session('address')['latitude'],
                    'longitude'=>session('address')['longitude'],
                    'locality'=>session('address')['locality'],
                    'city_name'=>session('address')['city_name'],
                    'state_name'=>session('address')['state_name'],
                    'country_name'=>session('address')['country_name'],
                    'pincode'=>session('address')['pincode'],
                    'specialties' => \App\Model\Specialty::take(5)->get(),
                    'specialties_all' => \App\Model\Specialty::skip(5)->take(134)->get(),
					'paginator' =>$paginator,
					'default_distance' => $distance
                ];
            } 
            catch (\Solarium\Exception $e) {
                $response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
                $response = json_encode($response);
                return $response;
            }
            return view('search.hospital', $results);
    }
	public function hospital_details($name_slug,Request $request)
	{           
            if(!$name_slug){
                    abort(404);
            }
            /**
             * Finding similar doctor
             */
            $query = $this->client->createSelect();
            $helper = $query->getHelper();
            $query->addFilterQuery(array('key'=>'role', 'query'=>'role:5', 'tag'=>'exclude'));
            $hospital =  \App\Model\Hospital::where('name_slug',$name_slug)->first();
            # Adding distance feild
            $address = $request->session()->get('address');
            $latitude = ($address['latitude']?$address['latitude']:28.7040592);
            $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
            $query->addField('distance:' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                )
            ); 
            #Finished distance feild
            /**
             * Start filet.
             */
            $settings = \App\Model\Setting::where('title','radius')->get()->first();              
            $query->createFilterQuery('distance')->setQuery(
                $helper->geofilt(
                    'latlon', 
                    doubleval($latitude),
                    doubleval($longitude),
                    doubleval($settings->value)
                )
            );
			/****
             * Finding hospital specialties
             */
            $specialties = "(";
            $i=1;
            foreach( $hospital->specialties as $var){
                $specialties.=$var->speciality_id;
                if($hospital->specialties->count()>$i++){
                    $specialties.=' or ';
                }
            }
            $specialties.=')';
            $query->addFilterQuery(array('key'=>'Specialties', 'query'=>'specl_id:'.$specialties, 'tag'=>'exclude'));
            /****
			 * Applying sorting.
			 */
			$request->request->add([
				'filters' =>['sorting'=>'distance-asc']
			]);
			$query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
			
            $query->setStart(0);
            $query->setRows(3);
            ## Paggination finished
            $resultset = $this->client->select($query);				
            $resultset = $resultset->getData()['response']['docs'];
            // Get all reviews that are not spam for the product and paginate them
            $reviews = $hospital->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100);
            $data = [
                'hospital'=>$hospital,
                'reviews'=>$reviews,
                'similar_hospitals'=>$resultset,
				'latitude'=>$latitude,
                'longitude'=>$longitude
            ];
            return view('search.hospital_details', $data);
	}
        public function diagnostic($state,$test, Request $request){
            try {
                $query = $this->client->createSelect();
                $helper = $query->getHelper();
				if(isset($request->input('filters')['name']) && $request->input('filters')['name']!=null){
					$query->createFilterQuery('name')->setQuery('name:%P1%', array(stripcslashes($request->input('filters')['name'])));  
				}
                $query->addFilterQuery(array('key'=>'role', 'query'=>'role:4', 'tag'=>'exclude'));
                # Adding distance feild
                $address = $request->session()->get('address');
                $latitude = ($address['latitude']?$address['latitude']:28.7040592);
                $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
                $query->addField('distance:' . $helper->geodist(
                        'latlon', 
                        doubleval($latitude), 
                        doubleval($longitude)
                    )
                ); 
                #Finished distance field
                /**
                 * Start field.
                 */
                $settings = \App\Model\Setting::where('title','radius')->get()->first();              
                if(isset($request->input('filters')['max_distance']) && $request->input('filters')['max_distance'] !='0KM'){
                    $distance = substr($request->input('filters')['max_distance'],0,-2);
                    $query->createFilterQuery('distance')->setQuery(
                        $helper->geofilt(
                            'latlon', 
                            doubleval($latitude),
                            doubleval($longitude),
                            doubleval($distance)
                        )
                    );
                }else{
                    $query->createFilterQuery('distance')->setQuery(
                        $helper->geofilt(
                            'latlon', 
                            doubleval($latitude),
                            doubleval($longitude),
                            doubleval($settings->value)
                        )
                    );
                }
				if(isset($request->input('filters')['min_fee']) && $request->input('filters')['max_fee']){
                    $minFee = $request->input('filters')['min_fee'];
                    $maxFee = $request->input('filters')['max_fee'];
                    $query->createFilterQuery('consultation_fee')->setQuery($helper->rangeQuery('consultation_fee', $minFee, $maxFee));
                }
                if(isset($request->input('filters')['rating']) && $request->input('filters')['rating']!=""){
					$rating = $request->input('filters')['rating'];
					$query->createFilterQuery('rating')->setQuery($helper->rangeQuery('rating', $rating, 5));
				}
                if(isset($request->input('filters')['experience']) && $request->input('filters')['experience']!=""){
                    $query->createFilterQuery('experience')->setQuery($helper->rangeQuery('experience', 0, $max));
                }
                if(isset($request->input('filters')['working_day']) && $request->input('filters')['working_day']!=""){
                    $weekDays = "(";
                    $i=1;
                    foreach($request->input('filters')['working_day'] as $var){
                        $weekDays.=$var;
                        if(count($request->input('filters')['working_day'])>$i++){
                            $weekDays.=' or ';
                        }				
                    }
                    $weekDays.=')';
                    $query->addFilterQuery(array('key'=>'week_day', 'query'=>'days_of_week:'.$weekDays, 'tag'=>'exclude'));
                }
                if(isset($request->input('filters')['gender']) && $request->input('filters')['gender']!=""){
                    $query->addFilterQuery(array('key'=>'gender', 'query'=>'gender:'.$request->input('filters')['gender'], 'tag'=>'exclude'));
                }
				if($test){
					$query->addFilterQuery(array('key'=>'Test', 'query'=>'diagnostic_test:'.$test, 'tag'=>'exclude'));
				}
                if(isset($request->input('filters')['tests']) && $request->input('filters')['tests']!=""){
                    $tests = "(";
                    $i=1;
                    foreach($request->input('filters')['tests'] as $var){
                        $tests.=$var;
                        if(count($request->input('filters')['tests'])>$i++){
                                $tests.=' or ';
                        }
                    }
                    $tests.=')';
                    $query->addFilterQuery(array('key'=>'Tests', 'query'=>'test_id:'.$tests, 'tag'=>'exclude'));
                }
				/**
                 * Filter Home Collection facility
                 */
                if(isset($request->input('filters')['online_reports']) && $request->input('filters')['online_reports']==1){
                
                    $query->addFilterQuery(array('key'=>'online_reports', 'query'=>'online_reports:'.$request->input('filters')['online_reports'], 'tag'=>'exclude'));
                }
                /**
                 * Filter Online report
                 */
                if(isset($request->input('filters')['home_collection_facility']) && $request->input('filters')['home_collection_facility']==1){
                
                    $query->addFilterQuery(array('key'=>'home_collection_facility', 'query'=>'home_collection_facility:'.$request->input('filters')['home_collection_facility'], 'tag'=>'exclude'));
                }
				/****
                 * Applying sorting..
                 */
                $query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
				
                /**
				 * Paggination 
				 */
				$page = 1;
				$limit = 25;
				if($request->input('page')) {
					$page = $request->input('page');
				}
				$start=($page - 1)*$limit;
				$query->setStart($start);
				$query->setRows($limit);
				
				$resultset = $this->client->select($query);
				$paginator = [
					'result' => $resultset,
					'count' => $resultset->getNumFound(),
					'limit' => $limit,
					'page' => $page,
					'query' => $query
				];
                			
                $resultset = $resultset->getData()['response']['docs'];
                # Storing session valeue
				if($request->input('filters')){
					$request->session()->put('filters', $request->input('filters'));
                }else{
					$diagnostic_test = \App\Model\Test::where('name', $test)->get()->first(); 
					$filters = [
						'tests'=>[$diagnostic_test->id],
						'max_distance' => $settings->value,
						'rating' =>'',
						'sorting' =>'relevence'
					];
					$request->session()->put('filters', $filters);
				}
				/**
				 * Save recent search
				 */
				if(auth()->user() && $request->session()->get('address')){														
					$request->country_name = $address['country_name'];	
					$request->request->add([
						'q' => $test,
						'locality' => $address['locality'],
						'address_one' => $address['address_one'],
						'latitude' => $address['latitude'],
						'longitude' => $address['longitude'],
						'city_name' => $address['city_name'],
						'state_name' => $address['state_name'],
						'country_name' => $address['country_name'],
						'entity' => 'diagnostic',
					]);
					$this->saveSearch(auth()->user()->id, $request);               
				}
				
				/* End */
				$results =[
                    'q'=>$test,
                    'results'=>$resultset,
                    'location'=>session('address')['address_one'],
                    'latitude'=>session('address')['latitude'],
                    'longitude'=>session('address')['longitude'],
                    'locality'=>session('address')['locality'],
                    'city_name'=>session('address')['city_name'],
                    'state_name'=>session('address')['state_name'],
                    'country_name'=>session('address')['country_name'],
                    'pincode'=>session('address')['pincode'],
                    'tests' => \App\Model\Test::take(5)->get(),
                    'test_all' => \App\Model\Test::skip(5)->take(134)->get(),
					'paginator' =>$paginator,
					'default_distance' => $settings->value
                ];
            } 
            catch (\Solarium\Exception $e) {
                $response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
                $response = json_encode($response);
                return $response;
            }
            return view('search.diagnostic', $results);
    }
	public function diagnostic_details($name_slug,Request $request)
	{
            if(!$name_slug){
                    abort(404);
            }
            /**
             * Finding similar doctor
             */
            $query = $this->client->createSelect();
            $helper = $query->getHelper();
            $query->addFilterQuery(array('key'=>'role', 'query'=>'role:4', 'tag'=>'exclude'));
            $diagnostic = \App\Model\Diagnostic::where('name_slug',$name_slug)->first();
            # Adding distance field
            $address = $request->session()->get('address');
            $latitude = ($address['latitude']?$address['latitude']:28.7040592);
            $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
            $query->addField('distance:' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                )
            ); 
            /**
             * Start field
             */
            $settings = \App\Model\Setting::where('title','radius')->get()->first();              
			if(isset($request->input('filters')['max_distance']) && $request->input('filters')['max_distance'] !='0KM'){
				$distance = substr($request->input('filters')['max_distance'],0,-2);
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($distance)
					)
				);
			}else{
				$query->createFilterQuery('distance')->setQuery(
					$helper->geofilt(
						'latlon', 
						doubleval($latitude),
						doubleval($longitude),
						doubleval($settings->value)
					)
				);
			}
			/****
             * Finding diagnostic tests
             */
            $tests = "(";
            $i=1;
            foreach( $diagnostic->tests as $var){
                $tests.=$var->id;
                if(count($diagnostic->tests)>$i++){
                    $tests.=' or ';
                }
            }
            $tests.=')';
			$query->addFilterQuery(array('key'=>'Tests', 'query'=>'test_id:'.$tests, 'tag'=>'exclude'));
            /****
			 * Applying sorting.
			 */
			$request->request->add([
				'filters' =>['sorting'=>'distance-asc']
			]);
			$query = $this->helper->solriumSort($query, $request,$latitude,$longitude);
			
            $query->setStart(0);
            $query->setRows(3);
            ## Paggination finished
            $resultset = $this->client->select($query);				
            $resultset = $resultset->getData()['response']['docs'];
            // Get all reviews that are not spam for the product and paginate them
            $reviews = $diagnostic->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100);
            $data = [
                'diagnostic'=>$diagnostic,
                'reviews'=>$reviews,
                'similar_diagnostics'=>$resultset,
				'latitude'=>$latitude,
                'longitude'=>$longitude
            ];
            return view('search.diagnostic_details', $data);
	}
	public function appendURL($data, $prefix)
	{
		// operate on the item passed by reference, adding the url based on slug
		foreach ($data as $key => & $item) {
			$item['url'] = url($prefix.'/'.$item['name']);
		}
		return $data;		
	}

	public function ajaxSearch(Request $request)
	{
            $query = e($request->input('q',''));

            if(!$query && $query == '') return response(array(), 400);

            $doctors = \App\Model\Doctor::where('name','like','%'.$query.'%')
                    ->where('name','like','%'.$query.'%')
                    ->orderBy('name','asc')
                    ->take(5)
                    ->get(array('id','name'))->toArray();

            $hospitals = \App\Model\Hospital::where('name','like','%'.$query.'%')
                    ->take(5)
                    ->get(array('id', 'name'))
                    ->toArray();
            $diagnostics = \App\Model\Diagnostic::where('name','like','%'.$query.'%')
                    ->take(5)
                    ->get(array('id', 'name'))
                    ->toArray();
            // Data normalization
            $hospitals = $this->appendValue($hospitals, url('public/img/icons/category-icon.png'),'icon');
            $doctors = $this->appendValue($doctors, url('public/img/icons/category-icon.png'),'icon');
            $diagnostics = $this->appendValue($doctors, url('public/img/icons/category-icon.png'),'icon');

            $doctors 	= $this->appendURL($doctors, 'search/doctors');
            $hospitals  = $this->appendURL($hospitals, 'search/hospitals');
            $diagnostics  = $this->appendURL($diagnostics, 'search/diagnostic_centers');

            // Add type of data to each item of each set of results
            $doctors = $this->appendValue($doctors, 'doctor', 'class');
            $hospitals = $this->appendValue($hospitals, 'hospital', 'class');
            $diagnostics = $this->appendValue($diagnostics, 'diagnostic', 'class');

            // Merge all data into one array
            $data = array_merge($doctors, $hospitals,$diagnostics);
            return response(['data'=>$data]);
	}
    public function autoComplete(Request $request){
        if($request->isMethod('post')){
            $address = $request->session()->get('address');
            if($request->session()->has('filters')) {
                $filter = $request->session()->get('filters');
                $distance = $filter['max_distance'];
            }else{
                $settings = \App\Model\Setting::where('title','radius')->get()->first();
                $distance =$settings->value;
            }
            $latitude = ($address['latitude']?$address['latitude']:28.7040592);
            $longitude = ($address['longitude']?$address['longitude']:77.10249019999992); 
			
			$sql = "SELECT name, ( 3959 * acos ( cos ( radians('".$latitude."') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('".$longitude."') ) + sin ( radians('".$latitude."') ) * sin( radians( latitude ) ) ) )*1.609344 AS distance FROM doctors HAVING  name like '" . $request->input("keyword") . "%' AND "
            . "distance <= '".$distance."'"
            . " UNION SELECT name, ( 3959 * acos ( cos ( radians('".$latitude."') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('".$longitude."') ) + sin ( radians('".$latitude."') ) * sin( radians( latitude ) ) ) )*1.609344 AS distance FROM hospitals HAVING  name like '" . $request->input("keyword") . "%' AND "
            . "distance <= '".$distance."'"        
            . " UNION SELECT name, ( 3959 * acos ( cos ( radians('".$latitude."') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('".$longitude."') ) + sin ( radians('".$latitude."') ) * sin( radians( latitude ) ) ) )*1.609344 AS distance FROM diagnostics HAVING  name like '" . $request->input("keyword") . "%' AND "
            . "distance <= '".$distance."'"
            . " UNION SELECT name, 0 as distance FROM specialties WHERE name like '" . $request->input("keyword") . "%'"
            . " UNION SELECT name, 0 as distance FROM service_type WHERE name like '" . $request->input("keyword") . "%'"
            . " ORDER BY name LIMIT 0,10";
            $result =  DB::select($sql);
            return view('search.autocomplete_ajax', ['result'=>$result]);              
        }
    }
	private function saveSearch($user_id,$request){
		#Save search
		if($user_id){
			if(\App\Model\RecentSearch::where('user_id',$user_id)->get()->count()>9){
				#Delete old entries & and new entry
				$id = \App\Model\RecentSearch::where('user_id',$user_id)->orderBy('id','asc')->get()->first()->id;
				\App\Model\RecentSearch::destroy($id);                        
				if(\App\Model\RecentSearch::where('q',$request->input('q'))->where('user_id',$user_id)->get()->count()>0){
					$recentId = \App\Model\RecentSearch::where('q',$request->input('q'))->where('user_id',$user_id)->get()->first()->id;
					\App\Model\RecentSearch::destroy($recentId);  
					\App\Model\RecentSearch::create([
						"user_id"=>$user_id,
						"q"=>$request->input('q'),
						"locality"=>$request->input('locality'),
						"address_one"=>$request->input('address_one'),
						"address_two"=>$request->input('address_two'),
						"latitude"=>$request->input('latitude'),
						"latitude"=>$request->input('latitude'),
						"longitude"=>$request->input('longitude'),
						"city_name"=>$request->input('city_name'),
						"state_name"=>$request->input('state_name'),
						"country_name"=>$request->input('country_name'),
						"entity"=>$request->input('entity'),
					]);
				}else{
					\App\Model\RecentSearch::create([
						"user_id"=>$user_id,
						"q"=>$request->input('q'),
						"locality"=>$request->input('locality'),
						"address_one"=>$request->input('address_one'),
						"address_two"=>$request->input('address_two'),
						"latitude"=>$request->input('latitude'),
						"latitude"=>$request->input('latitude'),
						"longitude"=>$request->input('longitude'),
						"city_name"=>$request->input('city_name'),
						"state_name"=>$request->input('state_name'),
						"country_name"=>$request->input('country_name'),
						"entity"=>$request->input('entity'),
					]);
				}
			}else{
				if(\App\Model\RecentSearch::where('q',$request->input('q'))->where('user_id',$user_id)->get()->count()>0){
					$recentId = \App\Model\RecentSearch::where('q',$request->input('q'))->where('user_id',$user_id)->get()->first()->id;
					\App\Model\RecentSearch::destroy($recentId); 
					\App\Model\RecentSearch::create([
						"user_id"=>$user_id,
						"q"=>$request->input('q'),
						"locality"=>$request->input('locality'),
						"address_one"=>$request->input('address_one'),
						"address_two"=>$request->input('address_two'),
						"latitude"=>$request->input('latitude'),
						"latitude"=>$request->input('latitude'),
						"longitude"=>$request->input('longitude'),
						"city_name"=>$request->input('city_name'),
						"state_name"=>$request->input('state_name'),
						"country_name"=>$request->input('country_name'),
						"entity"=>$request->input('entity'),
					]); 
				}else{
				   \App\Model\RecentSearch::create([
						"user_id"=>$user_id,
						"q"=>$request->input('q'),
						"locality"=>$request->input('locality'),
						"address_one"=>$request->input('address_one'),
						"address_two"=>$request->input('address_two'),
						"latitude"=>$request->input('latitude'),
						"latitude"=>$request->input('latitude'),
						"longitude"=>$request->input('longitude'),
						"city_name"=>$request->input('city_name'),
						"state_name"=>$request->input('state_name'),
						"country_name"=>$request->input('country_name'),
						"entity"=>$request->input('entity'),
					]); 
				}
			}
		}
		
	}
}
