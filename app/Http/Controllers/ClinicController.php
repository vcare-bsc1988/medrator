<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\ServiceDetail;
use App\Model\Service;
use App\Model\Specialty;
use App\Model\SpecialtyDetail;
use App\Model\Clinics;
use Validator;
use Image;
class ClinicController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
        return view('clinic.create', ['services'=>$serviceArr,'specialties'=>$specialties]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'service' => 'required',
            'specialty' => 'required',
			'country' => 'required',
			'state' => 'required',
			'address' => 'required',
			'city' => 'required',
			'pincode' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/clinic/create')
                ->withInput()
                ->withErrors($validator);
        }
        $formData = $request->except(['_token','specialty','service']);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploaded/images/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $destinationPath = public_path('uploaded/images');
            $image->move($destinationPath, $input['imagename']);
            $formData['image']= $input['imagename'];

        }
        $clinic = Clinics::create($formData);
        foreach($request->service as $temp){
            $serviceList[] = array('service_id'=>$temp, 'clinic_id'=>$clinic->id);
        }
        ServiceDetail::insert($serviceList);
        
        foreach($request->specialty as $var){
            $specialties[] = array('speciality_id'=>$var, 'clinic_id'=>$clinic->id);
        }
        SpecialtyDetail::insert($specialties);
        $request->session()->flash('success', 'Clinic added successfully !');
        return redirect('/clinic/create');
    }
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
