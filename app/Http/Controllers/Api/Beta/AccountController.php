<?php

namespace App\Http\Controllers\Api\Beta;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use Validator;
use App\Model\Users;
use App\Model\Doctor;
use App\Model\Diagnostic;
use App\Model\Hospital;
use Auth;
use App\User;
use Hash;
use DB;
use App\Model\Clinics;
use App\Model\Gallery;
use App\Model\Country;
use App\Model\City;
use App\Model\ClaimedProfile;
use Illuminate\Support\Facades\Mail;
class AccountController extends Controller
{
    use ResetsPasswords;
	protected $helper;
    public function __construct(\Solarium\Client $client)
    {
		$this->helper = new \App\Helpers\Helper;
    }
    public function index()
    {
        
    }
    public function add_doctor(Request $request)
    {   
		try{
			$validator =Validator::make($request->all(), [
				'user_id' => 'required',
				'name' => 'required',
				'gender' => 'required',
				//'pincode' => 'required',
				'phone' => 'required|numeric',
			]);
			$doctors = DB::table('doctors')
					->where('name', '=', $request->name)
					->where('phone', '=', $request->phone)
					->where('address_one', '=', $request->address_one)
					->get();
			#return $doctors;
			if($doctors){
				$response = array('status'=>0, 'message'=>'Doctor already exist.'); 
				$response = json_encode($response);
				return $response;
			}
			if ($validator->fails()) {
				$error = $validator->errors()->first();
				$response = array('status'=>0, 'message'=>$error); 
				$response = json_encode($response);
				return $response;
			}
			$formData = $request->except(['_token','specialties','services','qualifications','user_id']);
			if ($request->input('image')) {
				$imgData	='image/png;base64,'.$request->input('image');
				$new_data = explode(";", $imgData);
				$type = $new_data[0];
				$data = explode(",", $new_data[1]);
				$data = base64_decode($data[1]);
				$filename = time() . '.png';
				file_put_contents(public_path('uploaded/images/') . $filename, $data);
				$formData['image'] = url('/').'/public/uploaded/images/' . $filename;
			}
			$formData['recommend_by'] = $request->user_id;
			$doctor = Doctor::create($formData);
			$objCountry = new Country();
			$objCity = new City();
			if(!$objCountry->isAvail($request->country_name)){
				Country::create(['name'=>$request->country_name]);
			}
			if(!$objCity->isAvail($request->city_name)){
				City::create(['name'=>$request->city_name]);
			}
			if($request->qualifications){
				foreach($request->qualifications as $qualification){
					$qualifications [] =DB::table('doctor_qualification')->insert([
						'qualification_id' => $qualification['id'],
						'doctor_id' => $doctor->id,
						'college' => $qualification['college'],
						'year' => $qualification['year']
					]);
				}
			}
			#Storing Services
			if($request->services){
				foreach($request->services as $service){
					DB::table('service_details')->insert([
						'service_id' => $service['id'],
						'doctor_id' => $doctor->id,
					]);
				}
			}
			#String Specialties
			if($request->specialties){
				foreach($request->specialties as $specialty){
					DB::table('specialty_details')->insert([
						'speciality_id' => $specialty['id'],
						'doctor_id' => $doctor->id,
					]);
				}
			}
			#Getting Qualification
			$qualifications = DB::table('doctor_qualification')
				->join('qualifications', 'doctor_qualification.qualification_id', '=', 'qualifications.id')
				->where('doctor_qualification.doctor_id', '=', $doctor->id)
				->select('qualifications.id','qualifications.name','doctor_qualification.college','doctor_qualification.year')
				->get();
			$doctor->qualifications = $qualifications;
			
			#Getting Specialties
			$specialties = DB::table('specialty_details')
				->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
				->where('specialty_details.doctor_id', '=', $doctor->id)
				->select('specialties.id','specialties.name')
				->get();
			$doctor->specialties = $specialties;
			
			#Getting Services
			$services = DB::table('service_details')
				->join('service_type', 'service_details.service_id', '=', 'service_type.id')
				->where('service_details.doctor_id', '=', $doctor->id)
				->select('service_type.id','service_type.name')
				->get();
			$doctor->services = $services;
			
			###### Sending notification
			$user = \App\User::find($request->user_id);
			if($user){
				$deviceToken = $user->device_token;
				$gcmApiKey = config('constants.gcm_apikey');
				$deviceType = $user->device_type;
				$name = $doctor->name;
				$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
				\App\Model\Notification::create([
					'title'=>config('message.new_profile_notification_title'),
					'message'=>str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'receiver'=>$request->user_id,
					'type'=>0,
					'status'=>0
				]);
				# Sending user notification
				$notifications= $notifications->send([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.new_profile_notification_title')),
					'subtitle'	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'tickerText'	=> 'Medrator',
					'vibrate'	=> 1,
					'sound'		=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
				]);
				/**
				 * Sending admin notification.
				 */
				$this->helper->sendAdminNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending moderator notification.
				 */
				$this->helper->sendModeratorNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending mail to User.
				 *
				 */
				$user=\App\User::find($request->user_id);
				$data = [
					'email' => $user->email,
					'user_name'=>$user->user->name,
					'entity_name'=>$doctor->name
				];
				Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New profile added.');
				});
				/**
				 * Generate points
				 */
				\App\Model\Point::create([
					'user_id' => $user->id,
					'entity_id' => $doctor->id,
					'point_collected' => 50,
					'descriptioins' => 'Points collected for adding doctor profile.'
				]);
			}
			$response = array('status'=>1, 'message'=>'success','data'=>$doctor); 
		}catch(\Illuminate\Database\QueryException $e){
			$response = array('status'=>0, 'message'=>$e->getMessage()); 	
		}
		$response = json_encode($response);
		return $response;
    }
	public function add_associate_doctor(Request $request)
    {   
		$validator =Validator::make($request->all(), [
            'user_id' => 'required',
			'name' => 'required',
			'gender' => 'required',
			'phone' => 'required',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		if((int)$request->doctor_id)
		{
			if(!\App\Model\Doctor::find($request->doctor_id)){
				$response = array('status'=>1, 'message'=>'No doctor found associated to doctor ID'); 
				return $response;
			}
			$formData = $request->except(['_token','specialties','services','qualifications','doctor_id']);
			if ($request->input('image')) {
				$imgData	='image/png;base64,'.$request->input('image');
				$new_data = explode(";", $imgData);
				$type = $new_data[0];
				$data = explode(",", $new_data[1]);
				$data = base64_decode($data[1]);
				$filename = time() . '.png';
				file_put_contents(public_path('uploaded/images/') . $filename, $data);
				$formData['image'] = url('/').'/public/uploaded/images/' . $filename;
			}
			$doctor = Doctor::where('id',$request->doctor_id)
				->update ($formData);
			$objCountry = new Country();
			$objCity = new City();
			if(!$objCountry->isAvail($request->country_name)){
				Country::create(['name'=>$request->country_name]);
			}
			if(!$objCity->isAvail($request->city_name)){
				City::create(['name'=>$request->city_name]);
			}
			#Storing doctor qualifications.
			$doctor = \App\Model\Doctor::find($request->doctor_id);
			$doctor->qualifications()->detach();
			foreach($request->qualifications as $qualification){
				$doctor->qualifications()->attach($qualification['id'], ['college'=>$qualification['college'],'year'=>$qualification['year']]);
			}
			
			#Storing Services
			$doctor->serviceDetails()->where('doctor_id',$request->doctor_id)->delete();
			foreach($request->services as $service){
				$doctor->serviceDetails()->create([
					'service_id'=>$service['id']
				]);
			}
			#Storing Specialties
			$doctor->specialtyDetail()->where('doctor_id',$request->doctor_id)->delete();
			foreach($request->specialties as $specialty){
				
				$doctor->specialtyDetail()->create([
					'speciality_id'=>$specialty['id']
				]);
			}
			$doctor = Doctor::where('id',$request->doctor_id)->first();
		}
		else{
			$formData = $request->except(['_token','specialties','services','qualifications','doctor_id']);
			$formData['mobile'] = $request->input('phone');
			if(ClaimedProfile::where('user_id',$request->input('user_id'))->count()){
				return array('status'=>0, 'message'=>'You have already claimed request.'); 
			}
			if(Doctor::where('user_id',$request->input('user_id'))->where('claim_status',1)->count()){
				return array('status'=>0, 'message'=>'You have already associated profile.'); 
			}
			$doctors = DB::table('doctors')
                ->where('name', '=', $request->name)
				->where('phone', '=', $request->phone)
				->where('address_one', '=', $request->address_one)
                ->get();
			if($doctors){
				$response = array('status'=>0, 'message'=>'Doctor already exist.'); 
				$response = json_encode($response);
				return $response;
			}
			if ($request->input('image')) {
				$imgData	='image/png;base64,'.$request->input('image');
				$new_data = explode(";", $imgData);
				$type = $new_data[0];
				$data = explode(",", $new_data[1]);
				$data = base64_decode($data[1]);
				$filename = time() . '.png';
				file_put_contents(public_path('uploaded/images/') . $filename, $data);
				$formData['image'] = url('/').'/public/uploaded/images/' . $filename;
			}
			$doctor = Doctor::create($formData);
			foreach($request->qualifications as $qualification){
				DB::table('doctor_qualification')->insert([
					'qualification_id' => $qualification['id'],
					'doctor_id' => $doctor->id,
					'college' => $qualification['college'],
					'year' => $qualification['year']
				]);
			}
			#Storing Services
			foreach($request->services as $service){
				DB::table('service_details')->insert([
					'service_id' => $service['id'],
					'doctor_id' => $doctor->id,
				]);
			}
			#Storing Specialties
			foreach($request->specialties as $specialty){
				DB::table('specialty_details')->insert([
					'speciality_id' => $specialty['id'],
					'doctor_id' => $doctor->id,
				]);
			}
			/**
			 * Sending mail to User.
			 *
			 */
			$user = \App\User::find($request->user_id);
			$data = [
				'email' => $user->email,
				'user_name'=>$user->user->name,
				'entity_name'=>$doctor->name
			];
			Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
				$message->to($data['email'])->subject('New profile added.');
			});
			
			/**
			 * Generate points
			 */
			\App\Model\Point::create([
				'user_id' => $user->id,
				'entity_id' => $doctor->id,
				'point_collected' => 50,
				'descriptioins' => 'Points collected for adding doctor profile.'
			]);
		}
		#Getting Qualification
		$qualifications = DB::table('doctor_qualification')
            ->join('qualifications', 'doctor_qualification.qualification_id', '=', 'qualifications.id')
            ->where('doctor_qualification.doctor_id', '=', $doctor->id)
			->select('qualifications.id','qualifications.name','doctor_qualification.college','doctor_qualification.year')
            ->get();
		$doctor->qualifications = $qualifications;
		
		#Getting Specialties
		$specialties = DB::table('specialty_details')
            ->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
            ->where('specialty_details.doctor_id', '=', $doctor->id)
			->select('specialties.id','specialties.name')
            ->get();
		$doctor->specialties = $specialties;
		
		#Getting Services
		$services = DB::table('service_details')
            ->join('service_type', 'service_details.service_id', '=', 'service_type.id')
            ->where('service_details.doctor_id', '=', $doctor->id)
			->select('service_type.id','service_type.name')
            ->get();
		$doctor->services = $services;

		$response = array('status'=>1, 'message'=>'success','data'=>$doctor); 
		$response = json_encode($response);
		return $response;
    }
	public function associate_hospital(Request $request)
	{
		$validator =Validator::make($request->all(), [
            'user_id' => 'required',
			'doctor_id' => 'required',
			'name' => 'required',
			'phone' => 'required'
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		$hospital = DB::table('hospitals')
                ->where('name', '=', $request->name)
				->where('phone', '=', $request->phone)
				->where('address_one', '=', $request->address_one)
                ->get();
		if($hospital){
			$response = array('status'=>0, 'message'=>'Hospital or clinic already exist.'); 
			$response = json_encode($response);
			return $response;
		}
		$formData = $request->except(['_token','specialties','services','qualifications','user_id','doctor_id']);
		$formData['24_hours_emergency']=$formData['hours24_emergency'];
		unset($formData['hours24_emergency']);
		$hospital = Hospital::create($formData);
		$objCountry = new Country();
		$objCity = new City();
		if(!$objCountry->isAvail($request->country_name)){
			Country::create(['name'=>$request->country_name]);
		}
		if(!$objCity->isAvail($request->city_name)){
			City::create(['name'=>$request->city_name]);
		}
		#String Specialties
		foreach($request->specialties as $specialty){
			DB::table('specialty_details')->insert([
				'speciality_id' => $specialty['id'],
				'hospital_id' => $hospital->id,
			]);
		}
		#Storing Services
		foreach($request->services as $service){
			DB::table('service_details')->insert([
				'service_id' => $service['id'],
				'hospital_id' => $hospital->id,
			]);
		}
		#Getting Specialties
		$specialties = DB::table('specialty_details')
            ->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
            ->where('specialty_details.hospital_id', '=', $hospital->id)
			->select('specialties.id','specialties.name')
            ->get();
		$hospital->specialties = $specialties;
		
		#Getting Services
		$services = DB::table('service_details')
            ->join('service_type', 'service_details.service_id', '=', 'service_type.id')
            ->where('service_details.hospital_id', '=', $hospital->id)
			->select('service_type.id','service_type.name')
            ->get();
		$hospital->services = $services;
		# Relate Hospital to a doctor.
		DB::table('doctor_associated')->insert([
			'hospital_id' =>$hospital->id,
			'doctor_id' => $request->doctor_id,
		]);
		/**
		 * Sending mail to User.
		 *
		 */
		$user=\App\User::find($request->user_id);
		$data = [
			'email' => $user->email,
			'user_name'=>$user->user->name,
			'entity_name'=>$hospital->name
		];
		Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
			$message->to($data['email'])->subject('New profile added.');
		});
		
		/**
		 * Generate points
		 */
		\App\Model\Point::create([
			'user_id' => $user->id,
			'entity_id' => $hospital->id,
			'point_collected' => 50,
			'descriptioins' => 'Points collected for adding doctor profile.'
		]);
		$response = array('status'=>1, 'message'=>'success','data'=>$hospital); 
		$response = json_encode($response);
		return $response;
	}
	function remove_elm($arr, $key, $val, $within = FALSE) {
		foreach ($arr as $i => $array)
				if ($within && stripos($array[$key], $val) !== FALSE && (gettype($val) === gettype($array[$key])))
					unset($arr[$i]);
				elseif ($array[$key] === $val)
					unset($arr[$i]);

		return array_values($arr);
	}
	public function associate_clinic(Request $request)
	{
		$validator =Validator::make($request->all(), [
            'user_id' => 'required',
			'doctor_id' => 'required',
			'name' => 'required',
			'phone' => 'required',
			'consultation_fee' => 'required',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		# Add clinic here.
		$clinic = Clinics::create([
			'name'=>$request->name,
			'consultation_fee'=>$request->consultation_fee,
			'phone'=>$request->phone,
			'pincode'=>$request->pincode
		]);
		# Update clinic working time
		/*foreach($request->working_time as $time){
			DB::table('working_datetime')->insert([
				'week_day' => $time['day'],// 0-6 corresponding to Sun-Mon
				'start_time' => $time['from'],//Timestamp
				'end_time' => $time['to'], // Timestamp
				'clinic_id' => $clinic->id,
			]);
		}*/
		# Relate clinic to a doctor.
		DB::table('doctor_associated')->insert([
			'clinic_id' =>$clinic->id,
			'doctor_id' => $request->doctor_id,
		]);
		#Getting clinic working time
		/*$workingtime = DB::table('working_datetime')
			->where('working_datetime.clinic_id', '=', $clinic->id)
			->select('working_datetime.week_day','working_datetime.start_time','working_datetime.end_time')
            ->get();
		$clinic->working_time=$workingtime;*/
		$response = array('status'=>1, 'message'=>'success','data'=>$clinic); 
		$response = json_encode($response);
		return $response;
	}
	public function get_doctors(Request $request){
		$doctors = Doctor::where('user_id','=',$request->user_id)
			->get();
		$doctorList = array();
		foreach($doctors as $doctor)
		{
			#Getting Qualification
			$doctorList;
			$qualifications = DB::table('doctor_qualification')
				->join('qualifications', 'doctor_qualification.qualification_id', '=', 'qualifications.id')
				->where('doctor_qualification.doctor_id', '=', $doctor->id)
				->select('qualifications.id','qualifications.name')
				->get();
			$doctor->qualifications = $qualifications;
			
			#Getting Specialties
			$specialties = DB::table('specialty_details')
				->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
				->where('specialty_details.doctor_id', '=', $doctor->id)
				->select('specialties.id','specialties.name')
				->get();
			$doctor->specialties = $specialties;
			
			#Getting Services
			$services = DB::table('service_details')
				->join('service_type', 'service_details.service_id', '=', 'service_type.id')
				->where('service_details.doctor_id', '=', $doctor->id)
				->select('service_type.id','service_type.name')
				->get();
			$doctor->services = $services;
		}	
		$response = array('status'=>1, 'message'=>'success','data'=>$doctor); 
		$response = json_encode($response);
		return $response;
	}
	public function getClinics(Request $request)
	{
		$validator =Validator::make($request->all(), [
			'doctor_id' => 'required',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		$clinicsObj = new Clinics();
		$clinics = $clinicsObj->getClinics($request->doctor_id);
		if(empty($clinics)){
			$response = array('status'=>0, 'message'=>'No clinic found.'); 
			$response = json_encode($response);
			return $response;
		}
		$response = array('status'=>1, 'message'=>'success','data'=>$clinics); 
		$response = json_encode($response);
		return $response;
	}
	public function getHospitals(Request $request)
	{	
		$validator =Validator::make($request->all(), [
			'doctor_id' => 'required',
        ]);
		if ($validator->fails()) {
            $error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		$hospitals= DB::table('doctor_associated')
			->join('hospitals', 'doctor_associated.hospital_id', '=', 'hospitals.id')
			->where('doctor_associated.doctor_id', '=', $request->doctor_id)
			->select('hospitals.*')
			->get();
		$i=0; 		
		foreach($hospitals as $hospital){
			
			$specialties = DB::table('specialty_details')
				->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
				->where('specialty_details.hospital_id', '=', $hospital->id)
				->select('specialties.id','specialties.name')
				->get();
			
			$hospitals[$i]->specialties = $specialties;
			
			#Getting Services
			$services = DB::table('service_details')
				->join('service_type', 'service_details.service_id', '=', 'service_type.id')
				->where('service_details.hospital_id', '=', $hospital->id)
				->select('service_type.id','service_type.name')
				->get();
			$hospitals[$i++]->services = $services;
		}
		if(!$hospitals){
			$response = array('status'=>0, 'message'=>'No data found.'); 
			$response = json_encode($response);
			return $response;
		}
		$response = array('status'=>1, 'message'=>'success','data'=>$hospitals); 
		$response = json_encode($response);
		return $response;
	}
	/**
	* This function is deprecated
	public function updateClinic(Request $request)
	{
		$validator =Validator::make($request->all(), [
			'clinic_id' => 'required|exists:clinics,id',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		$data = $request->all();
		$data['id'] = $request->clinic_id;
		unset($data['clinic_id']);
		$clinicsObj = new Clinics();
		$clinics = $clinicsObj->updateClinics($request->clinic_id,$data);
		$response = array('status'=>1, 'message'=>'success','data'=>$clinics); 
		$response = json_encode($response);
		return $response;
	}
	*/
	##Update doctor associate hospital.
	public function updateHospital(Request $request)
	{
		$validator =Validator::make($request->all(), [
			'hospital_id' => 'required|exists:hospitals,id',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		$formData = $request->except(['_token','specialties','services','qualifications','hospital_id']);
		$formData['24_hours_emergency']=$formData['hours24_emergency'];
		unset($formData['hours24_emergency']);
		$hospital = Hospital::where('id',$request->hospital_id)->update($formData);
		
		#Storing Services
		$services = \App\Model\ServiceDetail::where('hospital_id',$request->hospital_id)->get();
		$i=0;
		foreach($services as $service){
			DB::table('service_details')
			->where('id', $service->id)
			->update([
				'service_id' => $request->services[$i++]['id'],
			]);
		}
		$specialties = \App\Model\SpecialtyDetail::where('hospital_id',$request->hospital_id)->get();
		#Storing Specialties
		$i=0;
		foreach($specialties as $specialty){
			DB::table('specialty_details')
			->where('id', $specialty->id)
			->update([
				'speciality_id' => $request->specialties[$i++]['id'],
			]);
		}
		
		################################################
		$objHospital = new Hospital();
		$result = $objHospital->getHospital($request->hospital_id);
		$response = array('status'=>1, 'message'=>'success','data'=>$result); 
		$response = json_encode($response);
		return $response;
	}
	public function profiles(Request $request)
	{
		$validator =Validator::make($request->all(), [
			'user_id' => 'required|exists:login,id',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		#Updated on 13th Aug 16
		$user = User::select('login.*','up.*','role_user.role_id as role')
				->join('role_user', 'login.id', '=', 'role_user.user_id')
				->join('users as up', 'login.id', '=', 'up.user_id')
				->where('login.id', $request->user_id)->first();
				
		$doctor =Doctor::select('doctors.*')
			->where('user_id',$user->user_id)->first();
		
		if($doctor):
		#Getting Qualification
		$qualifications = DB::table('doctor_qualification')
			->join('qualifications', 'doctor_qualification.qualification_id', '=', 'qualifications.id')
			->where('doctor_qualification.doctor_id', '=', $doctor->id)
			->select('qualifications.id','qualifications.name')
			->get();
		$doctor->qualifications = $qualifications;
		
		#Getting Specialties
		$specialties = DB::table('specialty_details')
			->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
			->where('specialty_details.doctor_id', '=', $doctor->id)
			->select('specialties.id','specialties.name')
			->get();
		$doctor->specialties = $specialties;
		
		#Getting Services
		$services = DB::table('service_details')
			->join('service_type', 'service_details.service_id', '=', 'service_type.id')
			->where('service_details.doctor_id', '=', $doctor->id)
			->select('service_type.id','service_type.name')
			->get();
		$doctor->services = $services;
		endif;
		$has_doctor_profile=0;
		if(count($doctor)>0)
			$has_doctor_profile=1;
		
		$result = array(
			"basic_profile" =>$user,
			"has_associate_doctor_profile"=>$has_doctor_profile,
			"associate_doctor_profile"=>$doctor
		);
		$response = array('status'=>1, 'message'=>'success','data'=>$result); 
		$response = json_encode($response);
		return $response;
	}
    public function uploadImage(Request $request)
    {
        $validator =Validator::make($request->all(), [
            'user_id' => 'required',
            'role' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		################# Changed on 9th Aug ##########################################
		$image = $request->file('image');
		$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
		$destinationPath = public_path('uploaded/images/thumbnail');
		$img = Image::make($image->getRealPath());
		$img->resize(100, 100, function ($constraint) {
				$constraint->aspectRatio();
		})->save($destinationPath.'/'.$input['imagename']);

		$destinationPath = public_path('uploaded/images');
		$image->move($destinationPath, $input['imagename']);
		switch ($request->role) {
		case 2:
			$user = User::find($request->user_id);
			$doctor = $user->doctor;
			$doctor->image = url('/').'/public/uploaded/images/'.$input['imagename'];
			$doctor->save();
			$response= array('status'=>1, 'message'=>'Success','data'=>$doctor);
			break;
		case 3:
			$user = User::find($request->user_id);
			$user = $user->user;
			$user->image = url('/').'/public/uploaded/images/'.$input['imagename'];
			$user->save();
			$response= array('status'=>1, 'message'=>'Success','data'=>$user);
			break;
		case 4:
			$user = User::find($request->user_id);
			$diagnostic = $user->diagnostic;
			$diagnostic->image = url('/').'/public/uploaded/images/'.$input['imagename'];
			$diagnostic->save();
			$response= array('status'=>1, 'message'=>'Success','data'=>$gallery);
			break;
		case 5:
			$user = User::find($request->user_id);
			$hospital = $user->hospital;
			$hospital->image = url('/').'/public/uploaded/images/'.$input['imagename'];
			$hospital->save();
			$response= array('status'=>1, 'message'=>'Success','data'=>$gallery);
			break;
		case 6:
			$user = User::find($request->user_id);
			$clinic = $user->clinic;
			$clinic->image = url('/').'/public/uploaded/images/'.$input['imagename'];
			$clinic->save();
			$response= array('status'=>1, 'message'=>'Success','data'=>$gallery);
			break;
		default:
			$response= array('status'=>0, 'message'=>'Failed please try latter.');
		}
        return $response;
    }
	public function uploadGalleryImage(Request $request)
	{
		$validator =Validator::make($request->all(), [
            'user_id' => 'required',
            'role' => 'required',
			'id' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		
		$image = $request->file('image');
		$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
		$destinationPath = public_path('uploaded/images/thumbnail');
		$img = Image::make($image->getRealPath());
		$img->resize(100, 100, function ($constraint) {
				$constraint->aspectRatio();
		})->save($destinationPath.'/'.$input['imagename']);

		$destinationPath = public_path('uploaded/images');
		$image->move($destinationPath, $input['imagename']);
		$imageUrl = url('/').'/public/uploaded/images/'.$input['imagename'];
		if((int)$request->image_id)
		{
			$gallery = Gallery::find($request->image_id);
			$gallery->image_url = $imageUrl;
			$gallery->save();
			return $gallery;
		}
		else{
			switch ($request->role) {
			case 2:
				$gallery= Gallery::create([
					'user_id'=>$request->user_id,
					'doctor_id'=>$request->id,
					'image_url'=>$imageUrl
					
				]);
				$response= array('status'=>1, 'message'=>'Success','data'=>$gallery);
				break;
			case 4:
				$gallery= Gallery::create([
					'user_id'=>$request->user_id,
					'diagnostic_id'=>$request->id,
					'image_url'=>$imageUrl
					
				]);
				$response= array('status'=>1, 'message'=>'Success','data'=>$gallery);
				break;
			case 5:
				$gallery= Gallery::create([
					'user_id'=>$request->user_id,
					'hospital_id'=>$request->id,
					'image_url'=>$imageUrl
					
				]);
				$response= array('status'=>1, 'message'=>'Success','data'=>$gallery);
				break;
			case 6:
				$gallery= Gallery::create([
					'user_id'=>$request->user_id,
					'clinic_id'=>$request->id,
					'image_url'=>$imageUrl
					
				]);
				$response= array('status'=>1, 'message'=>'Success','data'=>$gallery);
				break;
			default:
				$response= array('status'=>0, 'message'=>'Failed please try latter.');
			}
		}
		return $response;
	}
	public function edit(Request $request)
	{
		try {
			$validator =Validator::make($request->all(), [
				'user_id' => 'required',
				'name' => 'required',
				'gender' => 'required',
				'dob' => 'required',
			]);
			if ($validator->fails()) {
				$error = $validator->errors()->first();
				$response = array('status'=>0, 'message'=>$error); 
				$response = json_encode($response);
				return $response;
			}
			if(\App\Model\Users::where('user_id',$request->input('user_id'))->get()->count()<1){
				$response = array('status'=>0, 'message'=>'No associated profile found.'); 
				return $response;
			}
			$formData = $request->except(['_token','api_key','image']);
			$user = \App\Model\Users::where('user_id',$request->input('user_id'))->get()->first();
			if($user->mobile != $request->input('mobile')){
				$formData['is_mobile_verified'] = 0;
			}
			if ($request->hasFile('image')) {
				$image = $request->file('image');
				$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('uploaded/images/thumbnail');
				$img = Image::make($image->getRealPath());
				$img->resize(100, 100, function ($constraint) {
								$constraint->aspectRatio();
				})->save($destinationPath.'/'.$input['imagename']);

				$destinationPath = public_path('uploaded/images');
				$image->move($destinationPath, $input['imagename']);
				$formData['image'] =url('/').'/public/uploaded/images/'.$input['imagename'];
			}
			Users::where('user_id',$request->user_id)->update($formData);
			$profile = \App\Model\Users::basicProfile($request->user_id);
			$profile->otp= '1234';
			$response = array('status'=>1, 'message'=>config('constants.message.profile_updated'),'data'=>$profile); 
		}catch(\Illuminate\Database\QueryException $e){
			$response = array('status'=>0, 'message'=>$e->getMessage()); 	
		}
		return $response;		
	}
	public function get_profile(Request $request)
	{
		$profile = Users::select('users.*','countries.name as country_name','cities.name as city_name')
			->leftJoin('countries', 'users.country', '=', 'countries.id')
			->leftJoin('cities', 'users.city', '=', 'cities.id')
			->where('users.user_id', $request->input('user_id'))->first();
		if(!$profile){
			$response = array('status'=>0, 'message'=>'No data found.'); 
			$response = json_encode($response);
			return $response;
		}
		
		$response = array('status'=>1, 'message'=>'success','data'=>$profile); 
		$response = json_encode($response);
		return $response;
		
	}
    public function forgotPassword(Request $request){
        return $this->sendResetLinkEmail($request);
    }
	public function sendResetLinkEmail($request){
		$this->validateSendResetLinkEmail($request);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );

        switch ($response) {
            case Password::RESET_LINK_SENT:
                $response = array('status'=>1, 'message'=>'An email with a password reset link has been sent.'); 
				$response = json_encode($response);
				return $response;
            case Password::INVALID_USER:
				$response = array('status'=>0, 'message'=>'Invalid user.'); 
				$response = json_encode($response);
				return $response;
            default:
                $response = array('status'=>0, 'message'=>'Failed! Please try latter.'); 
				$response = json_encode($response);
				return $response;
        }
	}
	public function changePassword(Request $request)
	{
		$validator =Validator::make($request->all(), [
            'user_id' => 'required',
            'old_password' => 'required',
            'new_password' => 'required|min:6',
        ]);
		if ($validator->fails()) {
            $error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			return $response;
		}
		$user = User::find($request->user_id);
		if(!$user){
			$response = array('status'=>0, 'message'=>'User not found.'); 
		}
		if (Auth::attempt(array('email' => $user->email, 'password' => trim($request->old_password)))) {
			$user->password = Hash::make($request->new_password);
			$user->save();
			$response = array('status'=>1, 'message'=>'Your password has been changed.'); 
		}else{
			$response = array('status'=>0, 'message'=>'Incorrect current password.');
		}
		return $response;
	}
	public function addHospital(Request $request)
	{
		try {
			$validator =Validator::make($request->all(), [
				'user_id' => 'required',
				'name' => 'required',
				//'no_of_doctors' => 'required',
				'no_of_beds' => 'required',
				'icu_facility' => 'required',
				'diagnostic_lab_facility'=>'required',
				'cashless_mediclaim'=>'required',
				//'pincode' => 'required',
				'phone' => 'required',
			]);
			if ($validator->fails()) {
				$error = $validator->errors()->first();
				$response = array('status'=>0, 'message'=>$error); 
				$response = json_encode($response);
				return $response;
			}		
			$hospital = DB::table('hospitals')
					->where('name', '=', $request->name)
					->where('phone', '=', $request->phone)
					->where('address_one', '=', $request->address_one)
					->get();
			if($hospital){
				$response = array('status'=>0, 'message'=>'Hospital or clinic already exist.'); 
				$response = json_encode($response);
				return $response;
			}
			$formData = $request->except(['_token','specialties','services','qualifications','user_id']);
			$formData['24_hours_emergency']=$formData['hours24_emergency'];
			$formData['recommend_by'] = $request->user_id;
			unset($formData['hours24_emergency']);
			#### Uploading profile picture
			if ($request->input('image')) {
				$imgData	='image/png;base64,'.$request->input('image');
				$new_data = explode(";", $imgData);
				$type = $new_data[0];
				$data = explode(",", $new_data[1]);
				$data = base64_decode($data[1]);
				$filename = time() . '.png';
				file_put_contents(public_path('uploaded/images/') . $filename, $data);
				$formData['image'] = url('/').'/public/uploaded/images/' . $filename;
			}
			#### Finished
			$hospital = Hospital::create($formData);
			$objCountry = new Country();
			$objCity = new City();
			if(!$objCountry->isAvail($request->country_name)){
				Country::create(['name'=>$request->country_name]);
			}
			if(!$objCity->isAvail($request->city_name)){
				City::create(['name'=>$request->city_name]);
			}
			#String Specialties
			if($request->specialties){
				foreach($request->specialties as $specialty){
					DB::table('specialty_details')->insert([
						'speciality_id' => $specialty['id'],
						'hospital_id' => $hospital->id,
					]);
				}
			}
			#Storing Services
			if($request->services){
				foreach($request->services as $service){
					DB::table('service_details')->insert([
						'service_id' => $service['id'],
						'hospital_id' => $hospital->id,
					]);
				}
			}
			#Getting Specialties
			$specialties = DB::table('specialty_details')
				->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
				->where('specialty_details.hospital_id', '=', $hospital->id)
				->select('specialties.id','specialties.name')
				->get();
			$hospital->specialties = $specialties;
			
			#Getting Services
			$services = DB::table('service_details')
				->join('service_type', 'service_details.service_id', '=', 'service_type.id')
				->where('service_details.hospital_id', '=', $hospital->id)
				->select('service_type.id','service_type.name')
				->get();
			$hospital->services = $services;
			###### Sending notification
			$user = \App\User::find($request->user_id);
			if($user){
				$deviceToken = $user->device_token;
				$gcmApiKey = config('constants.gcm_apikey');
				$deviceType = $user->device_type;
				$name = $hospital->name;
				$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
				\App\Model\Notification::create([
					'title'=>config('message.new_profile_notification_title'),
					'message'=>str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'receiver'=>$request->user_id,
					'type'=>0,
					'status'=>0
				]);
				# Sending user notification
				$notifications= $notifications->send([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.new_profile_notification_title')),
					'subtitle'	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'tickerText'	=> 'Medrator',
					'vibrate'	=> 1,
					'sound'		=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
				]);
				/**
				 * Sending admin notification.
				 */
				$this->helper->sendAdminNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending moderator notification.
				 */
				$this->helper->sendModeratorNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending mail to User.
				 *
				 */
				$user=\App\User::find($request->user_id);
				$data = [
					'email' => $user->email,
					'user_name'=>$user->user->name,
					'entity_name'=>$hospital->name
				];
				Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New profile added.');
				});
				
				/**
				 * Generate points
				 */
				\App\Model\Point::create([
					'user_id' => $user->id,
					'entity_id' => $hospital->id,
					'point_collected' => 50,
					'descriptioins' => 'Points collected for adding doctor profile.'
				]);
			}
			$response = array('status'=>1, 'message'=>'success','data'=>$hospital); 
			$response = json_encode($response);
			return $response;
		}catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
		}
	}
	public function addDiagnostic(Request $request)
	{
		try{	
			$validator =Validator::make($request->all(), [
				'user_id' => 'required',
				'name' => 'required',
				'online_bookings' => 'required',
				'home_collection_facility' => 'required',
				'online_reports' => 'required',
				//'pincode' => 'required',
				'phone' => 'required',
			]);
			$diagnostic = DB::table('diagnostics')
					->where('name', '=', $request->name)
					->where('phone', '=', $request->phone)
					->where('address_one', '=', $request->address_one)
					->get();
			if($diagnostic){
				$response = array('status'=>0, 'message'=>'Diagnostic already exist.'); 
				$response = json_encode($response);
				return $response;
			}
			if ($validator->fails()) {
				$error = $validator->errors()->first();
				$response = array('status'=>0, 'message'=>$error); 
				$response = json_encode($response);
				return $response;
			}
			$formData = $request->except(['_token','diagnostic_tests','user_id']);
			$formData['recommend_by'] = $request->user_id;
			#### Uploading profile picture
			if ($request->input('image')) {
				$imgData	='image/png;base64,'.$request->input('image');
				$new_data = explode(";", $imgData);
				$type = $new_data[0];
				$data = explode(",", $new_data[1]);
				$data = base64_decode($data[1]);
				$filename = time() . '.png';
				file_put_contents(public_path('uploaded/images/') . $filename, $data);
				$formData['image'] = url('/').'/public/uploaded/images/' . $filename;
			}
			#### Finished
			$diagnostic = Diagnostic::create($formData);
			
			$objCountry = new Country();
			$objCity = new City();
			if(!$objCountry->isAvail($request->country_name)){
				Country::create(['name'=>$request->country_name]);
			}
			if(!$objCity->isAvail($request->city_name)){
				City::create(['name'=>$request->city_name]);
			}		
			#Storing Diagnostic Test
			foreach($request->diagnostic_tests as $test){
				DB::table('diagnostic_test')->insert([
					'test_id' => $test['id'],
					'diagnostic_id' => $diagnostic->id,
				]);
			}
			#Getting Diagnostic Tests
			$tests = DB::table('diagnostic_test')
				->join('tests', 'diagnostic_test.test_id', '=', 'tests.id')
				->where('diagnostic_test.diagnostic_id', '=', $diagnostic->id)
				->select('tests.id','tests.name')
				->get();
			$diagnostic->tests = $tests;
			###### Sending notification
			$user = \App\User::find($request->user_id);
			if($user){
				$deviceToken = $user->device_token;
				$gcmApiKey = config('constants.gcm_apikey');
				$deviceType = $user->device_type;
				$name = $diagnostic->name;
				$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
				\App\Model\Notification::create([
					'title'=>config('message.new_profile_notification_title'),
					'message'=>str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'receiver'=>$request->user_id,
					'type'=>0,
					'status'=>0
				]);
				# Sending user notification
				$notifications= $notifications->send([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.new_profile_notification_title')),
					'subtitle'	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'tickerText'	=> 'Medrator',
					'vibrate'	=> 1,
					'sound'		=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
				]);
				/**
				 * Sending admin notification.
				 */
				$this->helper->sendAdminNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending moderator notification.
				 */
				$this->helper->sendModeratorNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending mail to User.
				 *
				 */
				$user=\App\User::find($request->user_id);
				$data = [
					'email' => $user->email,
					'user_name'=>$user->user->name,
					'entity_name'=>$diagnostic->name
				];
				Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New profile added.');
				});
				
				/**
				 * Generate points
				 */
				\App\Model\Point::create([
					'user_id' => $user->id,
					'entity_id' => $diagnostic->id,
					'point_collected' => 50,
					'descriptioins' => 'Points collected for adding doctor profile.'
				]);
			}
			$response = array('status'=>1, 'message'=>'success','data'=>$diagnostic); 
			$response = json_encode($response);
			return $response;
		}catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
		}	
	}
	public function addClinic(Request $request)
	{
		$validator =Validator::make($request->all(), [
            'user_id' => 'required',
			'name' => 'required',
			'phone' => 'required',
			'consultation_fee' => 'required'
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		# Add clinic here.
		$clinic = Clinics::create([
			'name'=>$request->name,
			'consultation_fee'=>$request->consultation_fee,
			'phone'=>$request->phone,
			'address_one'=>$request->address_one,
			'city'=>$request->city,
			'country'=>$request->country,
			'pincode'=>$request->pincode,
			'last_update_user'=>$request->user_id,
			'status'=>0
		]);
		$country = \App\Model\Country::select('id','name')->find($request->country)->first();
		# Update clinic working time
		/*foreach($request->working_time as $time){
			DB::table('working_datetime')->insert([
				'week_day' => $time['day'],// 0-6 corresponding to Sun-Mon
				'start_time' => $time['from'],//Timestamp
				'end_time' => $time['to'], // Timestamp
				'clinic_id' => $clinic->id,
			]);
		}*/
		#Getting clinic working time
		/*$workingtime = DB::table('working_datetime')
			->where('working_datetime.clinic_id', '=', $clinic->id)
			->select('working_datetime.week_day','working_datetime.start_time','working_datetime.end_time')
            ->get();
		$clinic->working_time=$workingtime;*/
		$response = array('status'=>1, 'message'=>'success','data'=>$clinic); 
		$response = json_encode($response);
		return $response;
	}
	public function getOtp(Request $request)
	{
		$validator =Validator::make($request->all(), [
            'user_id' => 'required',
			'mobile' => 'required',
			'role' => 'required',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		$otp = rand (500, 5000);
		switch ($request->role) {
		case 2:
			$user = User::find($request->user_id);
			$user  = $user->doctor;
			$user->mobile = $request->mobile;
			$user->save();
			$response= array('status'=>1, 'message'=>'Success','data'=>array('otp'=>$otp));
			break;
		case 3:
			$user = User::find($request->user_id);
			$user  = $user->user;
			$user->mobile = $request->mobile;
			$user->save();
			$response= array('status'=>1, 'message'=>'Success', 'data'=>array('otp'=>$otp));
			break;
		default:
			$response= array('status'=>0, 'message'=>'Failed, please try latter.');
		}
		return $response;
	}
	public function verifyMobile(Request $request)
	{
		$validator =Validator::make($request->all(), [
            'user_id' => 'required',
			'mobile' => 'required',
			'role' => 'required',
        ]);
		if ($validator->fails()) {
			$error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
		}
		switch ($request->role) {
		case 2:
			$user = User::find($request->user_id);
			$user  = $user->doctor;
			if($user->mobile != $request->mobile){
				$response = array('status'=>0, 'message'=>'Invalid mobile number.'); 
				$response = json_encode($response);
				return $response;
			}
			$user->is_mobile_verified = 1;
			$user->save();
			$response= array('status'=>1, 'message'=>'Mobile verified successfully.');
			break;
		case 3:
			$user = User::find($request->user_id);
			$user  = $user->user;
			if((int)$user->mobile != (int)$request->mobile){
				$response = array('status'=>0, 'message'=>'Invalid mobile number.'); 
				$response = json_encode($response);
				return $response;
			}
			$user->is_mobile_verified = 1;
			$user->save();
			$response= array('status'=>1, 'message'=>'Mobile verified successfully.');
			break;
		default:
			$response= array('status'=>0, 'message'=>'Mobile verification failed, please try latter.');
		}
		return $response;
	}
	public function addSchedule(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'doctor_id' => 'required',
            'schedules' => 'required',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
        }
		foreach($request->schedules as $schedule){
			for($i=0; count($schedule['timeslot'])> $i; $i++){
				$schedule = \App\Model\TimeScheduler::create([
					'doctor_id'=>$request->doctor_id,
					'hospital_id'=>$request->hospital_id,
					'days_of_week'=>$schedule['days_of_week'],
					'avail_from'=>$schedule['timeslot'][$i][0],
					'avail_to'=>$schedule['timeslot'][$i][1]
				]);
			}
        }
		$response= array('status'=>1, 'message'=>'Saved successfully.');
		$response = json_encode($response);
		return $response;
	}
    public function saveImages()
    {
        
    }
	public function claimProfile(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'profile_id' => 'required',
			//'entity' => 'required' 
        ]);
		
        if ($validator->fails()) {
            $error = $validator->errors()->first();
			$response = array('status'=>0, 'message'=>$error); 
			$response = json_encode($response);
			return $response;
        }
		$entity = 2;
		$count = \App\Model\ClaimedProfile::where('user_id',$request->input('user_id'))
		->where('profile_claimed',$request->input('profile_id'))->count();
		if($count){
			$response = array('status'=>0, 'message'=>'Your request to claim this profile has been submited for approval.'); 
			$response = json_encode($response);
			return $response;
		}
		
		if(Doctor::where('user_id',$request->input('user_id'))->get()->count()){
			$response = array('status'=>0, 'message'=>'You have already associated profile.');
			return $response;
		}
		$isAssociated = Doctor::select('user_id')->where('id',$request->input('profile_id'))->get()->first()->user_id;
		if($isAssociated){
			$response = array('status'=>0, 'message'=>'Profile is already associate with someone.');
			return $response;
		}
		switch ($entity){
		case 2:			
			$claimed_profile= new \App\Model\ClaimedProfile([
				'profile_claimed'=>$request->input('profile_id'),
				'doctor_id'=>$request->input('profile_id')
			]);
			$user = \App\User::find($request->input('user_id'));
			if(!\App\User::where('id',$request->input('user_id'))->get()->count()){
				return array('status'=>0, 'message'=>'User ID does not exist.');
			}
			$claimed_profile = $claimed_profile->user()->associate($user);
			$claimed_profile = $claimed_profile->save();
			$doctor = Doctor::find($request->input('profile_id'));
			$doctor->claimed_by =$request->input('user_id');
			$doctor->claim_status =0; //0=for pending. 1=for approved
			$doctor->save();
			/***
			 * Sending notification
			 */
			$deviceToken = $user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->device_type;
			$name = $doctor->name;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_claimed_notification_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'receiver'=>$user->id,
				'type'=>0,
				'status'=>0
			]);
			# Sending user notification
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
			/**
			 * Sending admin notification.
			 */
			$this->helper->sendAdminNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			/**
			 * Sending moderator notification.
			 */
			$this->helper->sendModeratorNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			#Sending email
			$response= array('status'=>1, 'message'=>'Your request to claim this profile has been submited for approval.');
			break;
		case 5:
			if(!Hospital::where('id',$request->input('profile_id'))->get()->count()){
				return array('status'=>0, 'message'=>'Profile does not exist.');
			}
			$claimed_profile= new \App\Model\ClaimedProfile([
				'profile_claimed'=>$request->input('profile_id'),
				'entity'=>$entity,
				'doctor_id'=>$request->input('doctor_id')
			]);
			$user = \App\User::find($request->input('user_id'));
			$claimed_profile = $claimed_profile->user()->associate($user);
			$claimed_profile = $claimed_profile->save();
			$hospital = Hospital::find($request->input('profile_id'));
			$hospital->claimed_by =$request->input('user_id');
			$hospital->claim_status =0; //0=for pending. 1=for approved
			$hospital->save();
			/***
			 * Sending notification
			 */
			$deviceToken = $user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->device_type;
			$name = $hospital->name;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_claimed_notification_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'receiver'=>$user->id,
				'type'=>0,
				'status'=>0
			]);
			# Sending user notification
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
			/**
			 * Sending admin notification.
			 */
			$this->helper->sendAdminNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			/**
			 * Sending moderator notification.
			 */
			$this->helper->sendModeratorNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			$response= array('status'=>1, 'message'=>'Your request to claim this profile has been submited for approval.');
			break;
		case 4:
			if(!Diagnostic::where('id',$request->input('profile_id'))->get()->count()){
				return array('status'=>0, 'message'=>'Profile does not exist.');
			}
			$claimed_profile= new \App\Model\ClaimedProfile([
				'profile_claimed'=>$request->input('profile_id'),
				'entity'=>$request->input('entity'),
			]);
			$user = \App\User::find($request->input('user_id'));
			$claimed_profile = $claimed_profile->user()->associate($user);
			$claimed_profile = $claimed_profile->save();
			$diagnostic = Diagnostic::find($request->input('profile_id'));
			$diagnostic->claimed_by =$request->input('user_id');
			$diagnostic->claim_status =0; //0=for pending. 1=for approved
			$diagnostic->save();
			/***
			 * Sending notification
			 */
			$deviceToken = $user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->device_type;
			$name = $diagnostic->name;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_claimed_notification_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'receiver'=>$user->id,
				'type'=>0,
				'status'=>0
			]);
			# Sending user notification
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
			/**
			 * Sending admin notification.
			 */
			$this->helper->sendAdminNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			/**
			 * Sending moderator notification.
			 */
			$this->helper->sendModeratorNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			$response= array('status'=>1, 'message'=>'Your request to claim this profile has been submited for approval.');
			break;
		default:
			$response= array('status'=>0, 'message'=>'Claimed process failed, please try latter.');
		}
		return $response;		
	}
}
