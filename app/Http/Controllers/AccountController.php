<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use Image;
use App\Model\ServiceDetail;
use App\Model\Service;
use App\Model\Specialty;
use App\Model\SpecialtyDetail;
use Hash;
use App\Model\Doctor;
use App\Model\Hospital;
use App\Model\Diagnostic;
use DB;
use Illuminate\Support\Facades\Mail;
class AccountController extends Controller
{
    protected $client;
	protected $helper;
    public function __construct(\Solarium\Client $client)
    {
            $this->client = $client;
            $this->middleware('auth');
			$this->helper = new \App\Helpers\Helper;
    }
    public function index()
    {
		
    }
    public function profile(){
        $user = Auth::user();
        $user = User::find($user->id);
		$wp_tags = get_tags();
        $interests = [];
        for($i=0; $i<count($wp_tags); $i++)
        {
            $interests[$wp_tags[$i]->term_id] = $wp_tags[$i]->name;
        }
		$user_interests = $user->interests()->lists('interest_id')->toArray();
        $data =[
            'user'=>$user,
            'interests' =>$interests,
			'user_interests' => $user_interests
        ];
        return view('account.profile',$data);
    }
	
	public function doctoradd(){
        $user = Auth::user();
        $user = User::find($user->id);
        $data =[
            'user'=>$user
        ];
        return view('account.doctor-add',$data);
    }
	public function hospitaladd(){
        $user = Auth::user();
        $user = User::find($user->id);
        $data =[
            'user'=>$user
        ];
        return view('account.hospital-add',$data);
    }
	public function diagnosticcenteradd(){
        $user = Auth::user();
        $user = User::find($user->id);
        $data =[
            'user'=>$user
        ];
        return view('account.diagnostic-center-add',$data);
    }
    public function save(Request $request){
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'gender' => 'required',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
				//'interests' => 'required'
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator)
                    ->with(['status'=>0,'message'=>$validator->errors()->first()]);;
            }       
            $user = User::find($request->id);
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
                $profile->image= $input['imagename'];
                
            }                
            $user->user->name = $request->name;
            $user->user->mobile = ($request->mobile?$request->mobile:'');
            $user->user->dob =($request->dob?$request->dob:'');
            $user->user->gender =($request->dob?$request->gender:'');
            $user->user->about =($request->about?$request->about:'');
            $user->user->city_name = ($request->city_name?$request->city_name:'');
            $user->user->state_name = ($request->state_name?$request->state_name:'');
            $user->user->country_name = ($request->country_name?$request->country_name:'');
            $user->user->country_code = ($request->country_code?$request->country_code:'');
            $user->user->address_one = ($request->address_one?$request->address_one:'');
            $user->user->address_two = ($request->address_two?$request->address_two:'');
            $user->user->locality = ($request->locality?$request->locality:'');
            $user->user->latitude = ($request->latitude?$request->latitude:'');
            $user->user->longitude = ($request->longitude?$request->longitude:'');
            $user->user->save();
			/***
             * @ Save User Interests
             */
			 if($request->has('interests')):
            \App\Model\UserInterest::where('user_id',$request->id)->delete();
            foreach($request->input('interests') as $interest){
                $interest = \App\Model\UserInterest::updateOrCreate(
                [ 
                        'user_id'=> $request->id,
                        'interest_id' =>$interest
                ],
                [
                        'interest_id' =>$interest,
						'name' => get_tag($interest)->name
                ]);
            }
			endif;
            return back()->with(['status'=>1,'message'=>'Updated successfully.']);
        }
    }  
    
    /**
    * Save new doctor profile associated with user if no search result found.
    */
    public function saveDoctor(Request $request){ 
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'degree' => 'required',
            'college' => 'required',
            'year' => 'required',
            'specialties' => 'required',
            'gender' => 'required',
            'address_one' => 'required',
            'reg_no' => 'required',
            'awards' => 'required',
            'recognition' => 'required',
            'experience' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }
        $user = Auth::user();
        $doctor = Doctor::updateOrCreate(
        ['id' => $request->id],
        [
            'user_id'=>$user->id,
            'name'=>($request->name?$request->name:''),
			'about'=>($request->about?$request->about:''),
            'mobile'=>($request->mobile?$request->mobile:''),
            'dob'=>($request->dob?$request->dob:''),
            'city_name' => ($request->city_name?$request->city_name:''),
            'state_name'=> ($request->state_name?$request->state_name:''),
            'country_name' => ($request->country_name?$request->country_name:''),
            'address_one' => ($request->address_one?$request->address_one:''),
            'locality' => ($request->locality?$request->locality:''),
            'latitude' => ($request->latitude?$request->latitude:''),
            'longitude' => ($request->longitude?$request->longitude:''),
            'reg_no' => $request->reg_no,
            'awards' => $request->awards,
            'recognition' => $request->recognition,
            'experience' => $request->experience,
            'gender' => $request->gender,
        ]);
        ## Adding doctor specialties	
        $doctor->specialtyDetail()->where('doctor_id',$request->doctor_id)->delete();
        foreach($request->specialties as $specialty){
			$doctor->specialtyDetail()->create([
					'speciality_id'=>$specialty
			]);
        }			
        ## Adding qualification
        $doctor->qualifications()->detach();
        foreach($request->degree as $index=>$degree){ 
            $college=$request->college; 
            $year = $request->year;
            $collegeName = \App\Model\College::find($college[$index])->name;
            $doctor->qualifications()->attach($degree, ['college'=>$collegeName,'college_id'=>$college[$index],'year'=>$year[$index]]);	

        }      
        $user->roles()->detach();
        $user->roles()->attach(2);
        /****
         @ claiming profile
        */
        $claimedProfile = new \App\Model\ClaimedProfile([
            'profile_claimed' => $doctor->id,
            'user_id' => $user->id,
            'claim_status' => 0 
        ]);
        $claimedProfile->doctor()->associate($doctor);
        $claimedProfile->save();
        $doctor->claimed_by = $user->id;
        $doctor->claim_status = 1;
        $doctor->save();
        /****
        @ Sending claim mail to administrator
        */
        $data = [
                'email' => $user->email,
                'user_name'=>$user->user->name,
                'entity_name'=>$request->name
        ];
        Mail::send('email.profile_claim',['data'=>$data], function ($message) use ($data) {
                $message->to(env('ADMIN_EMAIL'))->subject('New profile request');
        });
		/****
        @ Sending new profile mail to user
        */
        $data = [
                'email' => $user->email,
                'user_name'=>$user->user->name,
                'entity_name'=>$request->name
        ];
        Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
                $message->to($data['email'])->subject('New profile request');
        });
		/**
		 * Generate points
		 */
		\App\Model\Point::create([
			'user_id' => $user->id,
			'entity_id' => $doctor->id,
			'point_collected' => 50,
			'descriptioins' => 'Points collected for adding doctor profile.'
		]);
        return redirect('claimed-profile')->with(['status'=>1,'message'=>'Updated successfully.']);
    }
	/**
	* Save new hospital profile associated with user if no search result found.
	*/
	public function saveHospital(Request $request){
		$user = Auth::user();
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
        $qualifications = \App\Model\Qualifications::lists('name','id');
		if ($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
				'name' => 'required|max:255',
				'specialties' => 'required',
				'country_name' => 'required',
				'state_name' => 'required',
				'address_one' => 'required',
				'city_name' => 'required',
				'address_one' => 'required',
				'locality' => 'required',
				'latitude' => 'required',
				'longitude' => 'required'
			]);

			if ($validator->fails()) {
				return back()
					->withInput()
					->withErrors($validator);
			}
			$image_url = "";
			if ($request->hasFile('image')) {
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
                $image_url = url('/').'/public/uploaded/images/'.$input['imagename'];            
            }   
			$hospital = Hospital::updateOrCreate(
			['id' => $request->id],
			[
				'user_id'=>$user->id,
				'name'=>($request->name?$request->name:''),
				'phone'=>($request->phone?$request->phone:''),
				'city_name' => ($request->city_name?$request->city_name:''),
				'state_name'=> ($request->state_name?$request->state_name:''),
				'country_name' => ($request->country_name?$request->country_name:''),
				'address_one' => ($request->address_one?$request->address_one:''),
				'locality' => ($request->locality?$request->locality:''),
				'latitude' => ($request->latitude?$request->latitude:''),
				'longitude' => ($request->longitude?$request->longitude:''),
				'no_of_doctors' => $request->input('no_of_doctors'),
				'no_of_beds' => $request->input('no_of_beds'),
				'icu_facility' => $request->input('icu_facility'),
                                '24_hours_emergency' => $request->input('24_hours_emergency'),
				'diagnostic_lab_facility' => $request->input('diagnostic_lab_facility'),
				'cashless_mediclaim' => $request->input('cashless_mediclaim'),
				'image' => $image_url,
			]);
			## Adding doctor specialties
			$hospital->specialtyList()->detach();
			$specialties =[];
			foreach($request->specialties as $var){
				$specialties[] = array('speciality_id'=>$var, 'hospital_id'=>$hospital->id);
			}
			SpecialtyDetail::insert($specialties);   
			
			$user->roles()->detach();
			$user->roles()->attach(5);
			/****
			@ claiming profile
			*/
			$claimedProfile = new \App\Model\ClaimedProfile([
			   'profile_claimed' => $hospital->id,
			   'user_id' => $user->id,
			   'claim_status' => 0 
			]);
			$claimedProfile->hospital()->associate($hospital);
			$claimedProfile->save();
			$hospital->claimed_by = $user->id;
			$hospital->claim_status = 1;
			$hospital->save();
			/****
			@ Sending claim mail to administrator
			*/
			$data = [
				   'email' => $user->email,
				   'user_name'=>$user->user->name,
				   'entity_name'=>$request->name
			];
			Mail::send('email.profile_claim',['data'=>$data], function ($message) use ($data) {
				   $message->to(env('ADMIN_EMAIL'))->subject('New profile claimed request');
			});
			/****
			@ Sending new profile mail to user
			*/
			$data = [
					'email' => $user->email,
					'user_name'=>$user->user->name,
					'entity_name'=>$request->name
			];
			Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New profile request');
			});
			/**
			 * Generate points
			 */
			\App\Model\Point::create([
				'user_id' => $user->id,
				'entity_id' => $hospital->id,
				'point_collected' => 50,
				'descriptioins' => 'Points collected for adding doctor profile.'
			]);
			return redirect('claimed-profile')->with(['status'=>1,'message'=>'Updated successfully.']);
		}
        $data =[
            'user'=>$user,
            'specialties'=>$specialties,
         ];
        return view('account.add-hospital', $data);
	}	
	/**
	* Save new diagnostic profile associated with user if no search result found.
	*/
	public function saveDiagnostic(Request $request){
		$user = Auth::user();
		$tests = \App\Model\Test::lists('name','id');		
		if ($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
				'name' => 'required|max:255',
				'tests' => 'required',
				'country_name' => 'required',
				'state_name' => 'required',
				'address_one' => 'required',
				'city_name' => 'required',
				'address_one' => 'required',
				'locality' => 'required',
				'latitude' => 'required',
				'longitude' => 'required'
			]);

			if ($validator->fails()) {
				return back()
					->withInput()
					->withErrors($validator);
			}
			$image_url = "";
			if ($request->hasFile('image')) {
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
                $image_url = url('/').'/public/uploaded/images/'.$input['imagename'];            
            }
			$diagnostic = Diagnostic::updateOrCreate(
			['id' => $request->id],
			[
				'user_id'=>$user->id,
				'name'=>($request->name?$request->name:''),
				'image'=> $image_url,
				'phone'=>($request->phone?$request->phone:''),
				'city_name' => ($request->city_name?$request->city_name:''),
				'state_name'=> ($request->state_name?$request->state_name:''),
				'country_name' => ($request->country_name?$request->country_name:''),
				'address_one' => ($request->address_one?$request->address_one:''),
				'locality' => ($request->locality?$request->locality:''),
				'latitude' => ($request->latitude?$request->latitude:''),
				'longitude' => ($request->longitude?$request->longitude:''),
				'home_collection_facility' => $request->input('home_collection_facility'),
				'online_bookings' => $request->input('online_bookings'),
				'online_reports' => $request->input('online_reports')
			]);
			## Adding doctor specialties
			$diagnostic->tests()->detach();
			$tests =[];
			foreach($request->tests as $var){
				$tests[] = array('test_id'=>$var, 'diagnostic_id'=>$diagnostic->id);
			}
			\App\Model\DiagnosticTest::insert($tests);   
			$user->roles()->detach();
			$user->roles()->attach(4);
			/****
			@ claiming profile
		   */
		   $claimedProfile = new \App\Model\ClaimedProfile([
			   'profile_claimed' => $diagnostic->id,
			   'user_id' => $user->id,
			   'claim_status' => 0 
		   ]);
		   $claimedProfile->diagnostic()->associate($diagnostic);
		   $claimedProfile->save();
		   $diagnostic->claimed_by = $user->id;
		   $diagnostic->claim_status = 1;
		   $diagnostic->save();
		   /****
		   @ Sending claim mail to administrator
		   */
		   $data = [
				'email' => $user->email,
				'user_name'=>$user->user->name,
				'entity_name'=>$request->name
			];
			Mail::send('email.profile_claim',['data'=>$data], function ($message) use ($data) {
					$message->to(env('ADMIN_EMAIL'))->subject('New profile claimed request');
			});
			/****
			@ Sending new profile mail to user
			*/
			$data = [
					'email' => $user->email,
					'user_name'=>$user->user->name,
					'entity_name'=>$request->name
			];
			Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New profile request');
			});
			/**
			 * Generate points
			 */
			\App\Model\Point::create([
				'user_id' => $user->id,
				'entity_id' => $diagnostic->id,
				'point_collected' => 50,
				'descriptioins' => 'Points collected for adding doctor profile.'
			]);
			return redirect('claimed-profile')->with(['status'=>1,'message'=>'Updated successfully.']);
		}
        $data =[
            'user'=>$user,
            'tests'=>$tests,
        ];
        return view('account.add-diagnostic', $data);
	}
    public function claimedProfile(){
        $user = Auth::user();
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
        $qualifications = \App\Model\Qualifications::lists('name','id');
        $tests = \App\Model\Test::lists('name','id');	
        ## Finding associted clinic/hospital with doctor   
        $years = [];
        for($i = date("Y"); $i > 1950; $i--){
                $years[$i]= $i;
        }
        //return $user->hospital;
        $colleges = ['' => 'Select Colege'] + \App\Model\College::lists('name','id')->all();		
        $data =[
            'doctor'=>($user->doctor?$user->doctor:[]),
            'hospital'=>($user->hospital?$user->hospital:[]),
            'diagnostic'=>($user->diagnostic?$user->diagnostic:[]),
            'services'=>$serviceArr,
            'specialties'=>$specialties,
            'qualifications'=>$qualifications,
            'clinics' => (isset($user->doctor->hospitals)?$user->doctor->hospitals:[]),
            'user' =>$user,
            'tests' => $tests,
            'colleges' => $colleges,
            'years' =>$years
        ];
        return view('account.claimed-profile', $data);
    }
    public function searchProfile(Request $request){
        try {
            $query = $this->client->createSelect();
            $helper = $query->getHelper();
            $query->setQuery('*'.$request->q.'*');
			$query->addFilterQuery(array('key'=>'role', 'query'=>'role:2 or role:4 or role:5', 'tag'=>'exclude'));
            # Adding distance feild
            $address = $request->session()->get('address');
            $latitude = ($address['latitude']?$address['latitude']:28.7040592);
            $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
            $query->addField('distance:' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                )
            ); 
            #Finished distance feild
            $resultset = $this->client->select($query);				
            $resultset = $resultset->getData()['response']['docs'];
            $i=0;
            foreach($resultset as $temp){ 
                ## Fimding rating and review.
                $resultset[$i]['days_of_week'] = array(1,2,3,4);
                if(@$temp['role'] ==2):
                    $resultset[$i++]['qualifications']= \App\Model\Doctor::getQualifications($temp['id']);
                endif;
            }
            $user = Auth::user();
            $results =[
                'q'=>$request->input('q'),
                'results'=>$resultset,
                'user' => $user 
            ];
        } 
        catch (\Solarium\Exception $e) {
            $response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
            $response = json_encode($response);
            return $response;
        }
        return view('account.search-profile', $results);
    }
    public function myRatings(Request $request){
        try{                       
            $resultset = [];
			$user = Auth::user();
            if(\App\Model\Review::where('user_id',$user->id)->get()->count()){
                $result = \App\Model\Review::where('user_id',$user->id)->get();
                $id = "(";
                $i=1;
                foreach($result as $var){
                    if($var['doctor_id']){
                        $id.=$var['doctor_id'];
                        if(count($result)>$i++){
                                        $id.=' or ';
                        }
                    }
                    if($var['hospital_id']){
                        $id.=$var['hospital_id'];
                        if(count($result)>$i++){
                                        $id.=' or ';
                        }
                    }
                    if($var['diagnostic_id']){
                        $id.=$var['diagnostic_id'];
                        if(count($result)>$i++){
                                        $id.=' or ';
                        }
                    }
                }
                $id.=')';
                $query = $this->client->createSelect();
                $helper = $query->getHelper();			
                $query->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$id, 'tag'=>'exclude'));
                $address = $request->session()->get('address');
                $latitude = ($address['latitude']?$address['latitude']:28.7040592);
                $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
                $query->addField('distance:' . $helper->geodist(
                        'latlon', 
                        doubleval($latitude), 
                        doubleval($longitude)
                    )
                );             
                $resultset = $this->client->select($query);
                $resultset = $resultset->getData()['response']['docs'];
                $i=0;
                foreach($resultset as $temp){ 
                    ## Fimding rating and review.
                    $resultset[$i]['days_of_week'] = array(1,2,3,4);
                    if($temp['role'] ==2):
                        $resultset[$i++]['qualifications']= \App\Model\Doctor::getQualifications($temp['id']);
                    endif;
                }  
            } 
            $data =[
                'results' => $resultset,
                'user' => $user
            ];
        } catch (\Solarium\Exception $ex) {
           abor(505);
        } catch(\Illuminate\Database\QueryException $e){
           abor(505);        
        }
        return view('account.my-ratings', $data);
    }
    public function addProfile(){ 
        $user = Auth::user();
        $user = User::find($user->id);
        $data =[
            'user'=>$user
        ];
        return view('account.add-profile',$data);
    }
    public function profileForm(Request $request){
        $user = Auth::user();
        $tests = \App\Model\Test::lists('name','id');	
        $serviceArr=  ['' => 'Select Services'] + Service::lists('name','id')->all();
        $specialties = ['' => 'Select Specialties'] + Specialty::lists('name','id')->all();
        $qualifications = ['' => 'Select Qualification'] + \App\Model\Qualifications::lists('name','id')->all();
		$colleges = ['' => 'Select Colege'] + \App\Model\College::lists('name','id')->all();
		$tests = \App\Model\Test::lists('name','id');	
        $years = [];
		for($i = date("Y"); $i > 1950; $i--){
			$years[$i]= $i;
		}
        $data =[
            'user'=>$user,
            'doctor'=>($user->doctor?$user->doctor:[]),
            'services'=>$serviceArr,
            'specialties'=>$specialties,
            'qualifications'=>$qualifications,
            'colleges' => $colleges,
            'years' =>$years,
            'tests' =>$tests
        ];
        $entity_type = $request->input('entity');
        if($entity_type==2){
                return view('account.doctor-form', $data);
        }
        if($entity_type==4){
                return view('account.hospital-form', $data);
        }
        if($entity_type==5){
                return view('account.diagnostic-form', $data);
        }     
    }
    public function clinics(){
        $user = Auth::user();
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
        $qualifications = \App\Model\Qualifications::lists('name','id');
        ## Finding associted clinic/hospital with doctor 
		$clinics = [];
		$i =0;
		if(!empty($user->doctor->hospitals)){
			foreach($user->doctor->hospitals as $hospital){
				$clinics[$i] = $hospital;
				$schedules = Hospital::getSchedules($hospital);
				$clinics[$i++]['schedules'] =$schedules;
			}
		}       
        $data =[
            'doctor'=>($user->doctor?$user->doctor:[]),
            'services'=>$serviceArr,
            'specialties'=>$specialties,
            'qualifications'=>$qualifications,
            'clinics' => $clinics,
            'user' =>$user,
            'hospital' => $user->hospital,
        ];
        return view('account.clinics', $data);
    }
    public function hospital(){
        $user = Auth::user();
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
       
        $data =[
            'services'=>$serviceArr,
            'specialties'=>$specialties,
            'hospital' => $user->hospital,
            'user' =>$user
        ];
        return view('account.hospital', $data);
    }
    public function diagnostic(){
        $user = Auth::user();
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
        $qualifications = \App\Model\Qualifications::lists('name','id');
        ## Finding associted clinic/hospital with doctor 
		$clinics = [];
		$i =0;
		if(!empty($user->doctor->hospitals)){
			foreach($user->doctor->hospitals as $hospital){
				$clinics[$i] = $hospital;
				$schedules = Hospital::getSchedules($hospital);
				$clinics[$i++]['schedules'] =$schedules;
			}
		}
        $data =[
            'doctor'=>($user->doctor?$user->doctor:[]),
            'services'=>$serviceArr,
            'specialties'=>$specialties,
            'qualifications'=>$qualifications,
            'clinics' => $clinics,
            'user' =>$user
        ];
        return view('account.diagnostic', $data);
    }
    public function updateHospital($id){
        $user = Auth::user();
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
        $data =[
            'hospital'=>($user->hospital?$user->hospital:[]),
            'services'=>$serviceArr,
            'specialties'=>$specialties,
            'user' =>$user
        ];
        return view('account.update-hospital', $data);
    }
    public function updateDiagnostic($id){
        $user = Auth::user();
        $tests = \App\Model\Test::lists('name','id');	
        $data =[
            'diagnostic'=>($user->diagnostic?$user->diagnostic:[]),
            'tests' => $tests,
            'user' =>$user
        ];
        return view('account.update-diagnostic', $data);
    }
    public function updateClinic($id){
        if(!$id){
            abort(404);
        }
        $user = Auth::user();
        $doctor = [];
        if($user->doctor){
            $doctor = $user->doctor;
        }
        $clinics = [];
        if($user->doctor->hospitals && $id){
            $clinics = $doctor->hospitals()->where('hospitals.id', $id)->first();
        }
        ### Finding schedular
        $schedular = [];
        if($doctor->schedular){
            $schedular = \App\Model\Doctor::getSchedules($doctor);
        }
        $specialties = Specialty::lists('name','id');
        ## Finding associted clinic/hospital with doctor      
        $data =[
            'doctor'=>$doctor,
            'clinics' => $clinics,
            'user' =>$user,
            'specialties' =>$specialties,
            'schedular' => $schedular
        ];
        return view('account.update-clinic', $data);
    }
    public function saveClinic(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'specialties' => 'required',
            'country_name' => 'required',
            'state_name' => 'required',
            'address_one' => 'required',
            'city_name' => 'required',
            'address_one' => 'required',
            'locality' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        } 
        $user = Auth::user();
        $doctor = $user->doctor;
        $data = [
            'name'=>($request->name?$request->name:''),
            'phone'=>($request->phone?$request->phone:''),
            'city_name' => ($request->city_name?$request->city_name:''),
            'state_name'=> ($request->state_name?$request->state_name:''),
            'country_name' => ($request->country_name?$request->country_name:''),
            'address_one' => ($request->address_one?$request->address_one:''),
            'locality' => ($request->locality?$request->locality:''),
            'latitude' => ($request->latitude?$request->latitude:''),
			'consultation_fee' => ($request->consultation_fee?$request->consultation_fee:0),
            'longitude' => ($request->longitude?$request->longitude:'')
        ];
        $hospital = Hospital::updateOrCreate(
            ['id' => $request->id],$data
        );
        $doctor->hospitals()->detach($hospital->id);
        $doctor->hospitals()->attach($hospital->id); 
		## Adding doctor specialties	
		$hospital->specialties()->where('hospital_id',$hospital->id)->delete();
		foreach($request->specialties as $specialty){
			$hospital->specialties()->create([
				'speciality_id'=>$specialty
			]);
		}
        ### Adding/updating schedular
        /*if(isset($request->scheduler) && !empty($request->scheduler)){              
            \App\Model\WorkingDatetimes::where(['doctor_id'=>$doctor->id, 'hospital_id'=>$hospital->id])->delete();
            foreach($request->scheduler as $schedule){                  
               if($schedule['timings']){
                   $timings = explode(",",$schedule['timings']);
                   foreach ($timings as $timing){
                       $timing =explode('-',$timing);
                       $data =[
                            'week_day'=>$schedule['week_day'],
                            'doctor_id'=>$doctor->id,
                            'hospital_id'=>$hospital->id,
                            'start_time' =>trim($timing[0]),
                            'end_time' =>trim($timing[1])
                        ];
                       \App\Model\WorkingDatetimes::Create($data);
                   }
               }                   
            }
        }*/
         /****
        @ claiming profile
       */
       if(!$request->id){
            $claimedProfile = new \App\Model\ClaimedProfile([
                'profile_claimed' => $hospital->id,
                'user_id' => $user->id,
                'claim_status' => 0 
            ]);
            $claimedProfile->hospital()->associate($hospital);
            $claimedProfile->save();
            $hospital->claimed_by = $user->id;
            $hospital->claim_status = 1;
            $hospital->save();       
             /****
             @ Sending claim mail to administrator
             */
             $data = [
                     'email' => $user->email,
                     'user_name'=>$user->user->name,
                     'entity_name'=>$request->name
             ];
             Mail::send('email.profile_claim',['data'=>$data], function ($message) use ($data) {
                     $message->to(env('ADMIN_EMAIL'))->subject('New profile claimed request');
             });
        }
        return redirect('account/clinics')->with(['status'=>1,'message'=>'Updated successfully.']);
    }
    public function searchClinics(Request $request){
        try {
            $query = $this->client->createSelect();
            $helper = $query->getHelper();
            $query->setQuery('*'.$request->q.'*');
			$query->addFilterQuery(array('key'=>'role', 'query'=>'role:5', 'tag'=>'exclude'));
            # Adding distance feild
            $address = $request->session()->get('address');
            $latitude = ($address['latitude']?$address['latitude']:28.7040592);
            $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
            $query->addField('distance:' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                )
            ); 
            #Finished distance feild
            $resultset = $this->client->select($query);				
            $resultset = $resultset->getData()['response']['docs'];          
            $user = Auth::user();
            $results =[
                'q'=>$request->input('q'),
                'results'=>$resultset,
                'user' => $user 
            ];
        } 
        catch (\Solarium\Exception $e) {
            $response = array('status'=>1, 'message'=>'success','data'=>$e->getMessage); 
            $response = json_encode($response);
            return $response;
        }
        return view('account.search-clinics', $results);
    }
    public function addClinic(){
        $user = Auth::user();
        
        $doctor = [];
        if($user->doctor){
            $doctor = $user->doctor;
        }
        $specialties = Specialty::lists('name','id');
        ## Finding associted clinic/hospital with doctor      
        $data =[
            'doctor'=>$doctor,
            'user' =>$user,
            'specialties' =>$specialties,
        ];
        return view('account.add-clinic', $data);
    }
    public function changePassword(Request $request)
    {
        if ($request->isMethod('post')){
            $user = Auth::user();
            $profile = User::find($user->id)->user;
            $validator = Validator::make($request->all(), [
                'password' 		=> 'required|min:6',
                'old_password'	=> 'required|min:6',
                'confirm_password'=> 'required|same:password'
            ]);
            if ($validator->fails()) {
                return redirect('account/change-password')
                    ->withInput()
                    ->withErrors($validator);
            }
            if($request->password != $request->confirm_password){
                
                return redirect('/account/associate/doctor');
            }
            $user = User::find(Auth::user()->id);
            $old_password 	= $request->old_password;
            $password 		= $request->password;
            if(Hash::check($old_password, $user->getAuthPassword())){
		$user->password = Hash::make($password);
                if($user->save()) {
                    return redirect('account/change-password')
                        ->with('success', 'Your password has been changed.');
                }
            } else {
                return redirect('account/change-password')
                    ->with('success', 'Your old password is incorrect.');
            }
            return redirect('account/change-password')
		->with('success', 'Your password could not be changed.');
        }
        $user = Auth::user();
        $data =[
            'user' =>$user,
        ];
        return view('account.change-password', $data);
    }
	public function claimDoctor($id, Request $request){
		try {
                $id = \Crypt::decrypt($id);
            } catch (DecryptException $e) {
                    abort(404);
            }
            if(!$id){
                    abort(404);
            }
            /**
             * Finding similar doctor
             */
            $query = $this->client->createSelect();
            $helper = $query->getHelper();
            $query->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
            $doctor =\App\Model\Doctor::find($id);
            # Adding distance feild
            $address = $request->session()->get('address');
            $latitude = ($address['latitude']?$address['latitude']:28.7040592);
            $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
            $query->addField('distance:' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                )
            ); 
            #Finished distance field
            /**
             * Start filet.
             */
            $settings = \App\Model\Setting::where('title','radius')->get()->first();              
            $query->createFilterQuery('distance')->setQuery(
                $helper->geofilt(
                    'latlon', 
                    doubleval($latitude),
                    doubleval($longitude),
                    doubleval($settings->value)
                )
            );
            $query->setStart(0);
            $query->setRows(3);
            ## Paggination finished
            $resultset = $this->client->select($query);				
            $resultset = $resultset->getData()['response']['docs'];
            // Get all reviews that are not spam for the product and paginate them
            $reviews = $doctor->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100);
            $data = [
                'doctor'=>$doctor,
                'reviews'=>$reviews,
                'similar_doctors'=>$resultset
            ];
            return view('account.claim-doctor', $data);
	}
	public function claimingDoctor(Request $request){
		if ($request->isMethod('post')){
				$validator = Validator::make($request->all(), [
				'id' => 'required',
			]);

			if ($validator->fails()) {
					return back()
							->withInput()
							->withErrors($validator);
			}
			$user = Auth::user();
			$count = \App\Model\ClaimedProfile::where('user_id',$user->id)
			->where('profile_claimed',$request->input('id'))->count();
			if($count){
				return back()
					->withErrors('Your request to claim this profile has been submited for approval.');
			}
			if(Doctor::where('user_id',$request->input('user_id'))->get()->count()){
				return back()
					->withErrors('You have already associated profile.');
			}
			$isAssociated = Doctor::select('user_id')->where('id',$request->input('id'))->get()->first()->user_id;
			if($isAssociated){
				return back()
					->withErrors('Profile is already associate with someone.');
			}	
			$doctor =\App\Model\Doctor::find($request->input('id'));
			/****
			@ claiming profile
			*/
			$claimedProfile = new \App\Model\ClaimedProfile([
			   'profile_claimed' => $doctor->id,
			   'user_id' => $user->id,
			   'claim_status' => 0 
			]);
			$claimedProfile->doctor()->associate($doctor);
			$claimedProfile->save();
			$doctor->claimed_by = $user->id;
			$doctor->claim_status = 0;
			$doctor->save();
			/***
			 * Sending notification
			 */
			$deviceToken = $user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->device_type;
			$name = $doctor->name;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_claimed_notification_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'receiver'=>$user->id,
				'type'=>0,
				'status'=>0
			]);
			# Sending user notification
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
			/**
			 * Sending admin notification.
			 */
			$this->helper->sendAdminNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			/**
			 * Sending moderator notification.
			 */
			$this->helper->sendModeratorNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			/****
			@ Sending claim mail to administrator
			*/
			$data = [
				'email' => $user->email,
				'user_name'=>$user->user->name,
				'entity_name'=>$doctor->name
			];
			Mail::send('email.profile_claim',['data'=>$data], function ($message) use ($data) {
				$message->to(env('ADMIN_EMAIL'))->subject('New profile claimed request');
			});
			
			/****
			@ Sending claim mail to user
			*/
			$data = [
				'email' => $user->email,
				'user_name'=>$user->user->name,
				'entity_name'=>$doctor->name
			];
			Mail::send('email.user-profile-claim',['data'=>$data], function ($message) use ($data) {
				$message->to($data['email'])->subject('Profile claimed request');
			});
			return redirect('doctor/details/'.\Crypt::encrypt($request->id))
				   ->with('success', 'Your request to claim this profile has been submited for approval.');
		}
	}
	public function claimHospital($id,Request $request){
		try {
					$id = \Crypt::decrypt($id);
		} catch (DecryptException $e) {
					abort(404);
		}
		if(!$id){
				abort(404);
		}
		/**
		 * Finding similar doctor
		 */
		$query = $this->client->createSelect();
		$helper = $query->getHelper();
		$query->addFilterQuery(array('key'=>'role', 'query'=>'role:5', 'tag'=>'exclude'));
		$hospital =  \App\Model\Hospital::find($id);
		# Adding distance feild
		$address = $request->session()->get('address');
		$latitude = ($address['latitude']?$address['latitude']:28.7040592);
		$longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
		$query->addField('distance:' . $helper->geodist(
				'latlon', 
				doubleval($latitude), 
				doubleval($longitude)
			)
		); 
		#Finished distance feild
		/**
		 * Start filet.
		 */
		$settings = \App\Model\Setting::where('title','radius')->get()->first();              
		$query->createFilterQuery('distance')->setQuery(
			$helper->geofilt(
				'latlon', 
				doubleval($latitude),
				doubleval($longitude),
				doubleval($settings->value)
			)
		);
		$query->setStart(0);
		$query->setRows(3);
		## Paggination finished
		$resultset = $this->client->select($query);				
		$resultset = $resultset->getData()['response']['docs'];
		// Get all reviews that are not spam for the product and paginate them
		$reviews = $hospital->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100);
		$data = [
			'hospital'=>$hospital,
			'reviews'=>$reviews,
			'similar_hospitals'=>$resultset
		];
		return view('account.claim-hospital', $data);
	}
	public function claimingHospital(Request $request){
		if ($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
					'id' => 'required',
			]);

			if ($validator->fails()) {
					return back()
							->withInput()
							->withErrors($validator);
			} 
			$user = Auth::user();
			$count = \App\Model\ClaimedProfile::where('user_id',$user->id)
			->where('profile_claimed',$request->input('id'))->count();
			if($count){
				return back()
					->withErrors('Your request to claim this profile has been submited for approval.');
			}
			if(Hospital::where('user_id',$request->input('user_id'))->get()->count()){
				return back()
					->withErrors('You have already associated profile.');
			}
			$isAssociated = Hospital::select('user_id')->where('id',$request->input('id'))->get()->first()->user_id;
			if($isAssociated){
				return back()
					->withErrors('Profile is already associate with someone.');
			}	
			$hospital =  \App\Model\Hospital::find($request->input('id'));
			/****
			@ claiming profile
			*/
			$claimedProfile = new \App\Model\ClaimedProfile([
			   'profile_claimed' => $hospital->id,
			   'user_id' => $user->id,
			   'claim_status' => 0 
			]);
			$claimedProfile->hospital()->associate($hospital);
			$claimedProfile->save();
			$hospital->claimed_by = $user->id;
			$hospital->claim_status = 0;
			$hospital->save();
			/***
			 * Sending notification
			 */
			$deviceToken = $user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->device_type;
			$name = $hospital->name;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_claimed_notification_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'receiver'=>$user->id,
				'type'=>0,
				'status'=>0
			]);
			# Sending user notification
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
			/**
			 * Sending admin notification.
			 */
			$this->helper->sendAdminNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			/**
			 * Sending moderator notification.
			 */
			$this->helper->sendModeratorNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			/****
			@ Sending claim mail to administrator
			*/
			$data = [
				'email' => $user->email,
				'user_name'=>$user->user->name,
				'entity_name'=>$hospital->name
			];
			Mail::send('email.profile_claim',['data'=>$data], function ($message) use ($data) {
				$message->to(env('ADMIN_EMAIL'))->subject('New profile claimed request');
			});
			/****
			@ Sending claim mail to user
			*/
			$data = [
				'email' => $user->email,
				'user_name'=>$user->user->name,
				'entity_name'=>$hospital->name
			];
			Mail::send('email.user-profile-claim',['data'=>$data], function ($message) use ($data) {
				$message->to($data['email'])->subject('Profile claimed request');
			});
			return redirect('hospital/details/'.\Crypt::encrypt($request->id))
					->with('success', 'Your request to claim this profile has been submited for approval.');
		}
	}
	public function claimDiagnostic($id,Request $request){
		try {
                $id = \Crypt::decrypt($id);
            } catch (DecryptException $e) {
                    abort(404);
            }
            if(!$id){
                    abort(404);
            }
            /**
             * Finding similar doctor
             */
            $query = $this->client->createSelect();
            $helper = $query->getHelper();
            $query->addFilterQuery(array('key'=>'role', 'query'=>'role:4', 'tag'=>'exclude'));
            $diagnostic = \App\Model\Diagnostic::find($id);
            # Adding distance feild
            $address = $request->session()->get('address');
            $latitude = ($address['latitude']?$address['latitude']:28.7040592);
            $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
            $query->addField('distance:' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                )
            ); 
            #Finished distance feild
            /**
             * Start filet.
             */
            $settings = \App\Model\Setting::where('title','radius')->get()->first();              
            /*$query->createFilterQuery('distance')->setQuery(
                $helper->geofilt(
                    'latlon', 
                    doubleval($latitude),
                    doubleval($longitude),
                    doubleval($settings->value)
                )
            );*/
            $query->setStart(0);
            $query->setRows(3);
            ## Paggination finished
            $resultset = $this->client->select($query);				
            $resultset = $resultset->getData()['response']['docs'];
            // Get all reviews that are not spam for the product and paginate them
            $reviews = $diagnostic->reviews()->with('user')->approved()->notSpam()->orderBy('created_at','desc')->paginate(100);
            $data = [
                'diagnostic'=>$diagnostic,
                'reviews'=>$reviews,
                'similar_diagnostics'=>$resultset
            ];
            return view('account.claim-diagnostic', $data);
	}
	public function claimingDiagnostic(Request $request){
		if ($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
					'id' => 'required',
			]);

			if ($validator->fails()) {
					return back()
							->withInput()
							->withErrors($validator);
			} 
			$user = Auth::user();
			$count = \App\Model\ClaimedProfile::where('user_id',$user->id)
			->where('profile_claimed',$request->input('id'))->count();
			if($count){
				return back()
					->withErrors('Your request to claim this profile has been submited for approval.');
			}
			if(Diagnostic::where('user_id',$request->input('user_id'))->get()->count()){
				return back()
					->withErrors('You have already associated profile.');
			}
			$isAssociated = Diagnostic::select('user_id')->where('id',$request->input('id'))->get()->first()->user_id;
			if($isAssociated){
				return back()
					->withErrors('Profile is already associate with someone.');
			}
			$diagnostic = \App\Model\Diagnostic::find($request->input('id'));
			/****
			@ claiming profile
			*/
			$claimedProfile = new \App\Model\ClaimedProfile([
			   'profile_claimed' => $diagnostic->id,
			   'user_id' => $user->id,
			   'claim_status' => 0 
			]);
			$claimedProfile->diagnostic()->associate($diagnostic);
			$claimedProfile->save();
			$diagnostic->claimed_by = $user->id;
			$diagnostic->claim_status = 0;
			$diagnostic->save();
			/***
			 * Sending notification
			 */
			$deviceToken = $user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->device_type;
			$name = $diagnostic->name;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_claimed_notification_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'receiver'=>$user->id,
				'type'=>0,
				'status'=>0
			]);
			# Sending user notification
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_claimed_notification_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
			/**
			 * Sending admin notification.
			 */
			$this->helper->sendAdminNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			/**
			 * Sending moderator notification.
			 */
			$this->helper->sendModeratorNotification([
				'title' 	=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_title')),
				'message'		=> str_replace('[NAME]',$name,config('message.admin_profile_claimed_notification_message')),
			]);
			/****
			@ Sending claim mail to administrator
			*/
			$data = [
				'email' => $user->email,
				'user_name'=>$user->user->name,
				'entity_name'=>$diagnostic->name
			];
			Mail::send('email.profile_claim',['data'=>$data], function ($message) use ($data) {
				$message->to(env('ADMIN_EMAIL'))->subject('New profile claimed request');
			});
			/****
			@ Sending claim mail to user
			*/
			$data = [
				'email' => $user->email,
				'user_name'=>$user->user->name,
				'entity_name'=>$diagnostic->name
			];
			Mail::send('email.user-profile-claim',['data'=>$data], function ($message) use ($data) {
				$message->to($data['email'])->subject('Profile claimed request');
			});
			return redirect('diagnostic/details/'.\Crypt::encrypt($request->id))
			->with('success', 'Your request to claim this profile has been submited for approval.');
		}
	}
    public function bookmarks(Request $request){
        $user = Auth::user();
        $address = $request->session()->get('address');
        $latitude = ($address['latitude']?$address['latitude']:28.7040592);
        $longitude = ($address['longitude']?$address['longitude']:77.10249019999992);
        $result = \App\Model\Bookmark::where('user_id',$user->id)->get();
        $resultset = [];
		if($result->count()>0){
			$id = "(";
			$i=1;
			foreach($result as $var){
				$id.=$var['entity_id'];
				if(count($result)>$i++){
								$id.=' or ';
				}
			}
			$id.=')';
			$query = $this->client->createSelect();
			$helper = $query->getHelper();			
			$query->addFilterQuery(array('key'=>'id', 'query'=>'id:'.$id, 'tag'=>'exclude'));
			$query->addField('distance:' . $helper->geodist(
					'latlon', 
					doubleval($latitude), 
					doubleval($longitude)
				)
			); 
			$query->setStart(0);
			$query->setRows(5000);
			$resultset = $this->client->select($query);
			$resultset = $resultset->getData()['response']['docs'];		
			$i=0;
			foreach($resultset as $temp){ 
				## Fimding rating and review.
				$resultset[$i]['days_of_week'] = array(1,2,3,4);
				if($temp['role'] ==2):
						$resultset[$i++]['qualifications']= \App\Model\Doctor::getQualifications($temp['id']);
				endif;
			}
		}
        $results =[
            'results'=>$resultset,   
            'user' =>$user,
        ];
        return view('account.bookmarks', $results);
    }
    public function clearBookmarks(){
        
    }
    public function saveSunSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveMonSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',2)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveTueSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',3)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveWedSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',4)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveThrSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',5)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveFriSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',6)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveSatSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',7)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
    public function getClinic(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $hospital = \App\Model\Hospital::find($request->input('id')); 
            
            $working_times = $hospital->workingTimes;
            $doctor = $hospital->doctors()->get()->first();
            $data =[
                'doctor' => $doctor,
                'hospital' => $hospital
            ];
            return view('account.schedules', $data);
        }
    }
    public function deleteGallery($id){
        if(!$id){
            abort(404);
        }
        $gallery= \App\Model\Gallery::destroy($id);
        return redirect('claimed-profile')->with(['status'=>1,'message'=>'Delete successfully.']);
    }
	public function pointsCollected(){
		$user = auth()->user();
		$level = \App\Model\ContributorLevel::all();
		$points = \App\Model\Point::where('user_id',$user->id)->sum('point_collected');
		$data = [
			'points_collected' => $points?$points:0,
			'levels' => $level,
			'user' =>$user
		];
		return view('account.points_collected', $data);
	}
	public function verifyMobile(Request $request){
		if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'otp' => 'required'
            ]);
            if ($validator->fails()) {
                return back()->with(['error_status'=>'failed', 'erro_message'=>'Please enter your OTP !']);
            }
			$user = auth()->user();	
			if($user->user->otp != $request->otp){
				return back()->with(['error_status'=>'failed', 'erro_message'=>'Enter valid OTP !']);
			}
			$user->user->otp = "";
			$user->user->is_mobile_verified = 1;
			$user->user->save();
            return back()->with(['status'=>1, 'message'=>'Mobile verified successfully.']);
        }
	}
	public function recentSearch(){
		$user = auth()->user();
		$data['recent_search_list'] = \App\Model\RecentSearch::orderBy('id','desc')->get();
		$data['user'] =$user;
        return view('account.recent-search', $data);
    }
}
