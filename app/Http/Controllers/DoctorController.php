<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Model\Doctor;
use App\Model\ServiceDetail;
use App\Model\Service;
use App\Model\Specialty;
use App\Model\SpecialtyDetail;
use Image;
class DoctorController extends Controller
{
    public function uploadImage(Request $request){ 
       if ($request->ajax()) {
            $image_url = "";
			if ($request->hasFile('photo')) {
                $image = $request->file('photo');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
				$image_url = url('/').'/public/uploaded/images/' . $input['imagename'];
            }
			$doctor = \App\Model\Doctor::find($request->id);
			$doctor->image = $image_url;
			$doctor->save();
			$data = [
				'doctor' =>$doctor
			];
            return view('doctor.upload-image',$data);
        }
        return false;
    }
}
