<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CityController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if($request->input('state_id')){         
                $results = \App\Model\City::where('state_id',$request->input('state_id'))->get();
                $response = '<option value="">Select City</option>';
                foreach($results as $city):
                    $response .='<option value="'.$city["id"].'">'.$city["name"].'</option>';
                endforeach;
                return $response;
            }
            return false;
        }
        return false;
    }
}
