<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Model\Doctor;
use App\Model\ServiceDetail;
use App\Model\Service;
use App\Model\Specialty;
use App\Model\SpecialtyDetail;
use Image;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('user.index');
    }
    public function edit(Request $request){
        if ($request->isMethod('post')) {
            
            $this->validate($request, [
                'mobile' => 'required|numeric|min:5',
            ]);
            
            $profile = new UserProfile;
            $profile->mobile = $request->mobile;
            $profile->save();
            return redirect('user');
        }
        return view('user.index');
    }
    public function uploadImage(Request $request){ 
       if ($request->ajax()) {
            $image_url = "";
                if ($request->hasFile('photo')) {
                $image = $request->file('photo');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
				$image_url = url('/').'/public/uploaded/images/' . $input['imagename'];
            }
            $user = \App\User::find($request->id);
            $user->user->image = $image_url;
            $user->user->save();
            $data = [
                    'user' =>$user
            ];
            return view('user.upload-image',$data);
        }
        return false;
    }
}
