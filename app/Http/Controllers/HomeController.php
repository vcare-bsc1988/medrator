<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Cookie;
use App\Model\Specialties;
use App\Model\Test;
use Illuminate\Support\Facades\Validator;
use Image;
use App\Model\ServiceDetail;
use App\Model\Service;
use App\Model\Specialty;
use App\Model\SpecialtyDetail;
use Hash;
use App\Model\Doctor;
use App\Model\Hospital;
use App\Model\Diagnostic;
use DB;
use Illuminate\Support\Facades\Mail;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $client;
	protected $helper;
    public function __construct(\Solarium\Client $client)
    {
        $this->client = $client;
		$this->helper = new \App\Helpers\Helper;
    }
    public function index(Request $request)
    {      
        /****
         * Getting user related interest blogs
         */
		if(session('wp_redirect') ==1){
			//echo $redirected_from_url = $request->session()->get('redirected_from_url'); exit;
		}
		elseif ($request->session()->has('redirected_from_url')) {
			$redirected_from_url = $request->session()->get('redirected_from_url');
			$request->session()->forget('redirected_from_url');
			return redirect($redirected_from_url);
		}		
        $user = auth()->user();
        if($user){
            $tags =  \App\User::find($user->id)->interests()->get();
            if ($tags->count()>0) {
                    $tag_ids = array();
                    foreach($tags as $individual_tag) 
                            $tag_ids[] = $individual_tag->interest_id;           
                    $args=array(
                            'tag__in' => $tag_ids,
                            'posts_per_page'=>10, // Number of related posts to display.
                            'caller_get_posts'=>1
                    );
                    $posts = get_posts($args);  
            }else{
                    $args=array(
                            'posts_per_page'=>10, // Number of related posts to display.
                            'caller_get_posts'=>1
                    );
                    $posts = get_posts($args);  
            }

        }else{
            $args=array(
                'posts_per_page'=>10, // Number of related posts to display.
                'caller_get_posts'=>1
            );
            $posts = get_posts($args);  
        }       
        $i=0;
        foreach($posts as $post):
			$images = [];
			$mediaImages = get_attached_media( 'image', $post->ID);
			foreach($mediaImages as $image){
					$images[]['img_url'] = $image->guid;
			}
			$posts[$i]->images= $images;
			$content = strip_tags($posts[$i]->post_content);
			$content= preg_replace('/[ ]{2,}|[\r\n]/', ' ', trim($content));
			$posts[$i]->post_content = $content;
			$i++;
        endforeach;
        $data['blogs'] = $posts;
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();		
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address']) && !empty($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip);	
            $data['address'] = $location;
        }
        $result = $this->entityList($data['address'],$request);
        $data = array_merge($data,$result);
        return view('home', $data);
    }
    public function entityList($address,$request)
    {	
        $latitude= $address['latitude'];
        $longitude = $address['longitude'];
        $query = $this->client->createSelect();
        $helper = $query->getHelper();
        $facets = $query->getFacetSet();
        $facets->createFacetField(array('field'=>'speciality', 'exclude'=>'exclude'));
        $query->addField('distance:' . $helper->geodist(
                'latlon', 
                doubleval($latitude), 
                doubleval($longitude)
            )
        );
        #finding overall top 13 combine
        $result = $this->client->select($query
            ->setStart(0)
            ->setRows(13))
            ->getData()['response']['docs'];
        $combine = [];
        for($i=0; $i < count($result); $i++){
            $combine[$i] = $result[$i];
            $combine[$i]['days_of_week'] = array(1,2,3,4);  
            if(isset($result[$i]['role']) && $result[$i]['role']==2){
                    $combine[$i]['qualifications'] = \App\Model\Doctor::getQualifications($result[$i]['id']);			 
            }
        }
        #finding overall top 13 doctor
        $query->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
        $result = $this->client->select($query
            ->setStart(0)
            ->setRows(13))
            ->getData()['response']['docs'];
        $doctors = [];
        for($i=0; $i < count($result); $i++){
            $doctors[$i] = $result[$i];
            $doctors[$i]['days_of_week'] = array(1,2,3,4);
            $doctors[$i]['qualifications'] = \App\Model\Doctor::getQualifications($result[$i]['id']);
        }
        #Finding top 13 location based doctor.
        $queryDoc = $this->client->createSelect();
        $helperDoc = $queryDoc->getHelper();
        $facets = $queryDoc->getFacetSet();
        $queryDoc->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
        $settings = \App\Model\Setting::where('title','radius')->get()->first();              
        $queryDoc ->createFilterQuery('distance')->setQuery(
            $helperDoc->geofilt(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude),
                    doubleval($settings->value)
            )
        );
		/**
         * filter best rated doctor.
         */
        $request->request->add([
            'filters' =>['sorting'=>'ratings']
        ]);
        $queryDoc = $this->helper->solriumSort($queryDoc, $request,$latitude,$longitude);
        $resultDoc = $this->client->select($queryDoc
            ->setStart(0)
            ->setRows(13))
            ->getData()['response']['docs'];
        $locDoctors=[];
        for($i=0; $i < count($resultDoc); $i++){
            $locDoctors[$i] = $resultDoc[$i];
            $locDoctors[$i]['qualifications'] = \App\Model\Doctor::getQualifications($result[$i]['id']);
        } 
        switch (count($locDoctors)) {
            case 0:
                    $homeDoctorList = [];
                    for($i=0; ($i < 13 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            case 1:
                    $homeDoctorList = [];
                    for($i=0; ($i < 12 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            case 2:
                    $homeDoctorList = [];
                    for($i=0; ($i < 11 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            case 3:
                    $homeDoctorList = [];
                    for($i=0; ($i < 10 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            case 4:
                    $homeDoctorList = [];
                    for($i=0; ($i < 9 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            case 5:
                    $homeDoctorList = [];
                    for($i=0; ($i < 8 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            case 6:
                    $homeDoctorList = [];
                    for($i=0; ($i < 7 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            case 7:
                    $homeDoctorList = [];
                    for($i=0; ($i < 6 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            case 8:
                    $homeDoctorList = [];
                    for($i=0; ($i < 5 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            case 9:
                    $homeDoctorList = [];
                    for($i=0; ($i < 4 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
			case 10:
                    $homeDoctorList = [];
                    for($i=0; ($i < 3 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
			case 11:
                    $homeDoctorList = [];
                    for($i=0; ($i < 2 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
			case 12:
                    $homeDoctorList = [];
                    for($i=0; ($i < 1 && $i<count($doctors)); $i++){
                            $homeDoctorList[$i] = $doctors[$i];
                            $homeDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeDoctorList = array_merge($locDoctors,$homeDoctorList);
                    break;
            default:
                    $homeDoctorList = $locDoctors;
        }
		#Finding top 13 location search based  doctor.
        $queryDoc1 = $this->client->createSelect();
        $helperDoc1 = $queryDoc1->getHelper();
        $facets = $queryDoc1->getFacetSet();
        $queryDoc1->addFilterQuery(array('key'=>'role', 'query'=>'role:2', 'tag'=>'exclude'));
        $queryDoc1 ->createFilterQuery('distance')->setQuery(
            $helperDoc->geofilt(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude),
                    doubleval($settings->value)
            )
        );
		if (Auth::check()) {
			$user = Auth::user();
			$last_search = \App\Model\RecentSearch::where('user_id',$user->id)->whereIn('entity',array('free-text','doctor'))->first();
			if($last_search){
				$special_characters = array('/', '&', '!', '.', '-', '+');
				$q = str_replace($special_characters,'',$last_search->q);
				$queryDoc1->setQuery('*'.$q.'*');
				$queryDoc1->setQueryDefaultOperator('AND');
			}
		}		
		$resultDoc1 = $this->client->select($queryDoc1
            ->setStart(0)
            ->setRows(13))
            ->getData()['response']['docs'];
        $searchDoctors=[];
        for($i=0; $i < count($resultDoc1); $i++){
            $searchDoctors[$i] = $resultDoc1[$i];
            $searchDoctors[$i]['qualifications'] = \App\Model\Doctor::getQualifications($result[$i]['id']);
        } 
		switch (count($searchDoctors)) {
            case 0:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 13 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            case 1:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 12 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            case 2:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 11 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            case 3:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 10 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            case 4:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 9 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            case 5:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 8 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            case 6:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 7 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            case 7:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 6 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            case 8:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 5 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            case 9:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 4 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
			case 10:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 3 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
			case 11:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 2 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
			case 12:
                    $homeSearchDoctorList = [];
                    for($i=0; ($i < 1 && $i<count($doctors)); $i++){
                            $homeSearchDoctorList[$i] = $doctors[$i];
                            $homeSearchDoctorList[$i]['qualifications'] = \App\Model\Doctor::getQualifications($doctors[$i]['id']);
                    }   
                    $homeSearchDoctorList = array_merge($searchDoctors,$homeSearchDoctorList);
                    break;
            default:
                    $homeSearchDoctorList = $searchDoctors;
        }
        $entities = compact("homeDoctorList","homeSearchDoctorList", "combine");
        return $entities;
    }
    public function wpLogin(Request $request){
        if($request->input('action')){
			$request->session()->put('redirected_from_url', url()->previous());
			return redirect('home')->with(['wp_redirect'=>1,'status'=>0,'message'=>'','type'=>1]);
        }
    }
	public function aboutUs(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		return view('home.about-us',$data);
	}
	public function disclaimer(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		return view('home.disclaimer',$data);
	}
	public function copyright(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		return view('home.copyright',$data);
	}
	public function privacyPolicy(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		return view('home.privacy-policy',$data);
	}
	public function healtTools(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		//return view('home.healt-tools',$data);
		return view('home.comming-soon',['title'=>'HEALTH LOOLS']);
	}
	public function organDonation(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		//return view('home.organ-donation',$data);
		return view('home.comming-soon',['title'=>'ORGAN DONATION']);
	}
	public function medicalTourism(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		//return view('home.medical-tourism',$data);
		return view('home.comming-soon',['title'=>'MEDICAL TOURISM']);
	}
	public function buyInsurance(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		return view('home.buy-insurance',$data);
	}
	public function reportErrors(Request $request){
		if ($request->isMethod('post')){
			try{ 
				$validator = Validator::make($request->all(), [
					'id' =>'required',
					'title' => 'required',
					'message' => 'required'
				]);
				if ($validator->fails()) {
					return back()
						->withInput()
						->withErrors($validator);
				} 
				$data = [
					'email' => auth()->user()->email,
					'user_name'=>auth()->user()->user->name,
					'title'=>$request->title,
					'message'=>$request->message
				];
				Mail::send('email.report-errors',['data'=>$data], function ($message) use ($data) {
					$message->to('medratorsystem@gmail.com')->subject('Error Reports');
				});
				/****
				@ Sending error report mail to user
				*/
				$entity = [];
				if(\App\Model\Doctor::where('id',$request->id)->get()->count()>0)
					$entity = \App\Model\Doctor::find($request->id);
				
				if(\App\Model\Hospital::where('id',$request->id)->get()->count()>0)
					$entity = \App\Model\Hospital::find($request->id);
				
				if(\App\Model\Diagnostic::where('id',$request->id)->get()->count()>0)
					$entity = \App\Model\Diagnostic::find($request->id);
				
				$data = [
					'email' => auth()->user()->email,
					'user_name'=>auth()->user()->user->name,
					'email' => auth()->user()->email,
					'entity_name'=>$entity->name,
				];
				Mail::send('email.user-report-errors',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('Reporting an error');
				});
				return back()
					->with('message', 'Error reported successfully.');
			} catch(\Illuminate\Database\QueryException $e){
				abor(505);        
			}
		}
		return view('home.report-errors');
	}
	public function newsletterSubscriber(Request $request){
		if ($request->isMethod('post')){
			try{ 
				$validator = Validator::make($request->all(), [
					'email' => 'required',
				]);
				if ($validator->fails()) {
					 return array('status'=>0, 'message'=>'Invalid Email.'); 	
				} 
				### Sending subscribing email.
				$response = array('status'=>1, 'message'=>'Submitted successfully.'); 	
			} catch(\Illuminate\Database\QueryException $e){
				$response = array('status'=>0, 'message'=>$e->getMessage());
			}
			return $response;
		}
		return false;
	}
	public function getSpecialties(Request $request){
		if ($request->ajax()) {
			$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
			$data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));               
			return view('home.specialties', $data);
		}
	}
	public function homeSpecilties(Request $request){
		if ($request->ajax()) {
			$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
			$data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));               
			return view('home.home-specialties', $data);
		}
	}
	public function getTests(Request $request){
		if ($request->ajax()) {
			$data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
			$data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));               
			return view('home.tests', $data);
		}
	}
	public function homeTests(Request $request){
		if ($request->ajax()) {
			$data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
			$data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));               
			return view('home.home-tests', $data);
		}
	}
	public function addDoctor(Request $request){
        $user = Auth::user();
        $serviceArr=  ['' => 'Select Services'] + Service::lists('name','id')->all();
        $specialties = Specialty::lists('name','id')->all();
        $qualifications = ['' => 'Select Qualification'] + \App\Model\Qualifications::lists('name','id')->all();
		$colleges = ['' => 'Select Colege'] + \App\Model\College::lists('name','id')->all();
		if ($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
				'name' => 'required|max:255',
				'specialties' => 'required',
				'gender' => 'required',
				'address_one' => 'required',
			]);

			if ($validator->fails()) {
				return back()
					->withInput()
					->withErrors($validator);
			}
			$image_url = "";
			if ($request->hasFile('image')) {
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
                $image_url = url('/').'/public/uploaded/images/'.$input['imagename'];            
            }   
			$doctor = Doctor::updateOrCreate(
			[
				'id' => $request->id,
			],
			[
				'name'=>($request->name?$request->name:''),
				'image'=> $image_url,
				'mobile'=>($request->mobile?$request->mobile:''),
				'dob'=>($request->dob?$request->dob:''),
				'city_name' => ($request->city_name?$request->city_name:''),
				'state_name'=> ($request->state_name?$request->state_name:''),
				'country_name' => ($request->country_name?$request->country_name:''),
				'address_one' => ($request->address_one?$request->address_one:''),
				'locality' => ($request->locality?$request->locality:''),
				'latitude' => ($request->latitude?$request->latitude:''),
				'longitude' => ($request->longitude?$request->longitude:''),
				'reg_no' => $request->reg_no,
				'awards' => $request->awards,
				'recognition' => $request->recognition,
				'experience' => $request->experience,
				'gender' => $request->gender,
				'status'=>0,
				'recommend_by'=> auth()->check()?auth()->user()->id:0
			]);
			## Adding doctor specialties	
			$doctor->specialtyDetail()->where('doctor_id',$request->doctor_id)->delete();
			foreach($request->specialties as $specialty){
				$doctor->specialtyDetail()->create([
					'speciality_id'=>$specialty
				]);
			}			
			## Adding qualification
			$doctor->qualifications()->detach();
			$doctor->qualifications()->attach($request->qualification, ['college'=>$request->college,'year'=>$request->year]);			
			
			if(Auth::check()){
				$user = Auth::user();
				###### Sending notification
				$deviceToken = $user->device_token;
				$gcmApiKey = config('constants.gcm_apikey');
				$deviceType = $user->device_type;
				$name = $doctor->name;
				$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
				\App\Model\Notification::create([
					'title'=>config('message.new_profile_notification_title'),
					'message'=>str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'receiver'=>$user->id,
					'type'=>0,
					'status'=>0
				]);
				# Sending user notification
				$notifications= $notifications->send([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.new_profile_notification_title')),
					'subtitle'	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'tickerText'	=> 'Medrator',
					'vibrate'	=> 1,
					'sound'		=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
				]);
				/**
				 * Sending admin notification.
				 */
				$this->helper->sendAdminNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending moderator notification.
				 */
				$this->helper->sendModeratorNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending mail to User.
				 *
				 */
				$data = [
					'email' => $user->email,
					'user_name'=>$user->user->name,
					'entity_name'=>$doctor->name
				];
				Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New profile added.');
				});
				/**
				 * Generate points
				 */
				\App\Model\Point::create([
					'user_id' => $user->id,
					'entity_id' => $doctor->id,
					'point_collected' => 50,
					'descriptioins' => 'Points collected for adding doctor profile.'
				]);
			}
			/*****
			 * Sending email to admin
			 */
			$data = [
				'email' => config('constants.admin_email'),
				'entity_name'=>$doctor->name
			];
			Mail::send('email.new-profile-admin',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New Profile Added');
			});
			return back()->with(['status'=>1,'message'=>'Added successfully.']);
		}
		$years = [];
		for($i = date("Y"); $i > 1950; $i--){
			$years[$i]= $i;
		}
        $data =[
            'services'=>$serviceArr,
            'specialties'=>$specialties,
            'qualifications'=>$qualifications,
			'colleges' => $colleges,
			'years' =>$years
        ];
        return view('account.add-doctor', $data);
    }
	public function addHospital(Request $request){
		$user = Auth::user();
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
        $qualifications = \App\Model\Qualifications::lists('name','id');
		if ($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
				'name' => 'required|max:255',
				'specialties' => 'required',
				'country_name' => 'required',
				'state_name' => 'required',
				'address_one' => 'required',
				'city_name' => 'required',
				'address_one' => 'required',
				'locality' => 'required',
				'latitude' => 'required',
				'longitude' => 'required'
			]);

			if ($validator->fails()) {
				return back()
					->withInput()
					->withErrors($validator);
			}
			$image_url = "";
			if ($request->hasFile('image')) {
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
                $image_url = url('/').'/public/uploaded/images/'.$input['imagename'];            
            }   
			$hospital = Hospital::Create([
				'name'=>($request->name?$request->name:''),
				'image'=> $image_url,
				'phone'=>($request->mobile?$request->mobile:''),
				'city_name' => ($request->city_name?$request->city_name:''),
				'state_name'=> ($request->state_name?$request->state_name:''),
				'country_name' => ($request->country_name?$request->country_name:''),
				'address_one' => ($request->address_one?$request->address_one:''),
				'locality' => ($request->locality?$request->locality:''),
				'latitude' => ($request->latitude?$request->latitude:''),
				'longitude' => ($request->longitude?$request->longitude:''),
				'image' => $image_url,
				'status'=>0,
				'recommend_by'=>auth()->check()?auth()->user()->id:0
			]);
			## Adding doctor specialties
			$specialties =[];
			foreach($request->specialties as $var){
				$specialties[] = array('speciality_id'=>$var, 'hospital_id'=>$hospital->id);
			}
			SpecialtyDetail::insert($specialties);
			if(Auth::check()){
				$user = Auth::user();
				###### Sending notification
				$deviceToken = $user->device_token;
				$gcmApiKey = config('constants.gcm_apikey');
				$deviceType = $user->device_type;
				$name = $hospital->name;
				$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
				\App\Model\Notification::create([
					'title'=>config('message.new_profile_notification_title'),
					'message'=>str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'receiver'=>$user->id,
					'type'=>0,
					'status'=>0
				]);
				# Sending user notification
				$notifications= $notifications->send([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.new_profile_notification_title')),
					'subtitle'	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'tickerText'	=> 'Medrator',
					'vibrate'	=> 1,
					'sound'		=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
				]);
				/**
				 * Sending admin notification.
				 */
				$this->helper->sendAdminNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending moderator notification.
				 */
				$this->helper->sendModeratorNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending mail to User.
				 *
				 */
				$data = [
					'email' => $user->email,
					'user_name'=>$user->user->name,
					'entity_name'=>$doctor->name
				];
				Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New profile added.');
				});
				/**
				 * Generate points
				 */
				\App\Model\Point::create([
					'user_id' => $user->id,
					'entity_id' => $hospital->id,
					'point_collected' => 50,
					'descriptioins' => 'Points collected for adding doctor profile.'
				]);
			}
			/*****
			 * Sending email to admin
			 */
			$data = [
				'email' => config('constants.admin_email'),
				'entity_name'=>$hospital->name
			];
			Mail::send('email.new-profile-admin',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New Profile Added');
			});
			return back()->with(['status'=>1,'message'=>'Profile added successfully.']);
		}
        $data =[
            'user'=>$user,
            'specialties'=>$specialties,
         ];
        return view('account.add-hospital', $data);
	}
	public function addDiagnostic(Request $request){
		$user = Auth::user();
		$tests = \App\Model\Test::lists('name','id');		
		if ($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
				'name' => 'required|max:255',
				'tests' => 'required',
				'address_one' => 'required',
			]);

			if ($validator->fails()) {
				return back()
					->withInput()
					->withErrors($validator);
			}
			$image_url = "";
			if ($request->hasFile('image')) {
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
                $image_url = url('/').'/public/uploaded/images/'.$input['imagename'];            
            }   
			$diagnostic = Diagnostic::Create([
				'name'=>($request->name?$request->name:''),
				'image'=> $image_url,
				'phone'=>($request->mobile?$request->mobile:''),
				'city_name' => ($request->city_name?$request->city_name:''),
				'state_name'=> ($request->state_name?$request->state_name:''),
				'country_name' => ($request->country_name?$request->country_name:''),
				'address_one' => ($request->address_one?$request->address_one:''),
				'locality' => ($request->locality?$request->locality:''),
				'latitude' => ($request->latitude?$request->latitude:''),
				'longitude' => ($request->longitude?$request->longitude:''),
				'image' => $image_url,
				'status'=>0,
				'recommend_by'=>auth()->check()?auth()->user()->id:0
			]);
			## Adding doctor specialties
			$tests =[];
			foreach($request->tests as $var){
				$tests[] = array('test_id'=>$var, 'diagnostic_id'=>$diagnostic->id);
			}
			\App\Model\DiagnosticTest::insert($tests); 
			if(Auth::check()){
				$user = Auth::user();
				###### Sending notification
				$deviceToken = $user->device_token;
				$gcmApiKey = config('constants.gcm_apikey');
				$deviceType = $user->device_type;
				$name = $diagnostic->name;
				$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
				\App\Model\Notification::create([
					'title'=>config('message.new_profile_notification_title'),
					'message'=>str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'receiver'=>$user->id,
					'type'=>0,
					'status'=>0
				]);
				# Sending user notification
				$notifications= $notifications->send([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.new_profile_notification_title')),
					'subtitle'	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'tickerText'	=> 'Medrator',
					'vibrate'	=> 1,
					'sound'		=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
				]);
				/**
				 * Sending admin notification.
				 */
				$this->helper->sendAdminNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending moderator notification.
				 */
				$this->helper->sendModeratorNotification([
					'message' 	=> str_replace('[NAME]',$name,config('message.new_profile_notification_message')),
					'title'		=> str_replace('[NAME]',$name,config('message.admin_new_profile_notification_message')),
				]);
				/**
				 * Sending mail to User.
				 *
				 */
				$data = [
					'email' => $user->email,
					'user_name'=>$user->user->name,
					'entity_name'=>$diagnostic->name
				];
				Mail::send('email.new-entity-added',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New profile added.');
				});
				/**
				 * Generate points
				 */
				\App\Model\Point::create([
					'user_id' => $user->id,
					'entity_id' => $diagnostic->id,
					'point_collected' => 50,
					'descriptioins' => 'Points collected for adding doctor profile.'
				]);
			}
			/*****
			 * Sending email to admin
			 */
			$data = [
				'email' => config('constants.admin_email'),
				'entity_name'=>$diagnostic->name
			];
			Mail::send('email.new-profile-admin',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('New Profile Added');
			});
			return back()->with(['status'=>1,'message'=>'Profile added successfully.']);
		}
        $data =[
            'user'=>$user,
            'tests'=>$tests,
        ];
        return view('account.add-diagnostic', $data);
	}
	
    public function contactUs(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		return view('home.contact-us', $data);
	}
	public function termsOfService(){
		$data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
        $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
        if(isset($_COOKIE['address'])){
            $data['address'] = (array) json_decode(stripslashes($_COOKIE['address']));
        }else{
            $ip = \Vcareall\Admin\Helper::getIP(); 
            $location = \Vcareall\Admin\Helper::getAddress($ip); 
            $data['address'] = $location;
        }
		return view('home.terms-of-service', $data);	
    }
    public function notificationUnread($id){
        if($id){
            $notification = \App\Model\Notification::find($id);
            $notification->status = 0;
            $notification->save();
            return back();
        }
    }
    public function notificationRead($id){
        if($id){
            $notification = \App\Model\Notification::find($id);
            $notification->status = 1;
            $notification->save();
            return back();
        }
    }
    public function clearNotification($id){
        if($id){
            \App\Model\Notification::destroy($id);
            return back();
        }
    }
    public function clearNotifications($id){
        if(!$id){
          abort(404); 
        }
        $notification = \App\Model\Notification::where('receiver',$id)->delete();
        return back();
    }
    public function notificationAll(){
        $data= [
            'notifications' => \App\Helpers\Helper::getAllNotifications(auth()->user()->id,'both'),
            'user' =>auth()->user()
        ];
        return view('account.notifications', $data);
    }
}
