<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\ServiceDetail;
use App\Model\Service;
use App\Model\Specialty;
use App\Model\SpecialtyDetail;
use App\Model\Hospital;
use Validator;
use Image;
class HospitalController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
        return view('hospital.create', ['services'=>$serviceArr,'specialties'=>$specialties]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'service' => 'required',
            'specialty' => 'required',
			'country' => 'required',
			'state' => 'required',
			'address' => 'required',
			'city' => 'required',
			'pincode' => 'required',
			'no_of_doctors' => 'required',
			'pincode' => 'required',
			'no_of_beds' => 'required',
			'icu_facility' => 'required',
			'diagnostic_lab_facility' => 'required',
			'24_hours_emergency' => 'required',
			
        ]);

        if ($validator->fails()) {
            return redirect('/hospital/create')
                ->withInput()
                ->withErrors($validator);
        }
        $formData = $request->except(['_token','specialty','service']);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploaded/images/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $destinationPath = public_path('uploaded/images');
            $image->move($destinationPath, $input['imagename']);
            $formData['image']= $input['imagename'];

        }
        $hospital = Hospital::create($formData);
        foreach($request->service as $temp){
            $serviceList[] = array('service_id'=>$temp, 'hospital_id'=> $hospital->id);
        }
        ServiceDetail::insert($serviceList);
        foreach($request->specialty as $var){
            $specialties[] = array('speciality_id'=>$var, 'hospital_id'=> $hospital->id);
        }
        SpecialtyDetail::insert($specialties);
        $request->session()->flash('success', 'Hospital added successfully !');
        return redirect('/hospital/create');
    }
    public function uploadGalleries(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'image' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
                $image_url = url('/').'/public/uploaded/images/' . $input['imagename'];
                
                $hospital = \App\Model\Hospital::find($request->input('id'));
                $gallery = new \App\Model\Gallery([
                    'image_url' => $image_url
                ]);
                $gallery->hospital()->associate($hospital);
                $gallery->save();
            }
            return back()->with(['status'=>1,'message'=>'Uploaded successfully.']);
        }
    }
    public function schedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $hospital = \App\Model\Hospital::find($request->input('id')); 
            $working_times = $hospital->workingTimes;
            $data =[
                'hospital' => $hospital
            ];
            return view('account.hospital-schedules', $data);
        }
    }
    public function saveSunSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $hospital = \App\Model\Hospital::find($request->input('hospital_id'));
            $hospital->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to']
                ]);
		$working_time->hospital()->associate($hospital);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveMonSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $hospital = \App\Model\Hospital::find($request->input('hospital_id'));
            $hospital->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                ]);
		$working_time->hospital()->associate($hospital);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveTueSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $hospital = \App\Model\Hospital::find($request->input('hospital_id'));
            $hospital->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to']
                ]);
		$working_time->hospital()->associate($hospital);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveWedSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $hospital = \App\Model\Hospital::find($request->input('hospital_id'));
            $hospital->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to']
                ]);
		$working_time->hospital()->associate($hospital);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveThrSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $hospital = \App\Model\Hospital::find($request->input('hospital_id'));
            $hospital->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to']
                ]);
		$working_time->hospital()->associate($hospital);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveFriSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $hospital = \App\Model\Hospital::find($request->input('hospital_id'));
            $hospital->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to']
                ]);
		$working_time->hospital()->associate($hospital);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveSatSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $hospital = \App\Model\Hospital::find($request->input('hospital_id'));
            $hospital->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to']
                ]);
                $working_time->hospital()->associate($hospital);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
}
