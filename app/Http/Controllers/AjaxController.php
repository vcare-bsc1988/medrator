<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

/**
 * Description of AjaxController
 *
 * @author VCareAll
 */
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Cookie;
use App\Model\Specialties;
use App\Model\Test;
use Validator;
use DB;
class AjaxController extends Controller {
    
    public function sessionForCity(Request $request)
    {
        if ($request->ajax()) {
            $data['specialties'] = Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
            $data['tests'] = Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
            $data['address'] = [
                'latitude'=> $request->input('latitude'),
                'longitude'=> $request->input('longitude'),
				'address_one' => $request->input('location'),
                'locality'=> $request->input('locality'),
                'city_name'=> $request->input('city_name'),
                'state_name'=> $request->input('state_name'),
                'country_name'=> $request->input('country_name'),
                'pincode'=> $request->input('pincode')
            ];
			#### Storing location to the session
            $request->session()->put('address', $data['address']);
			#### Storing location to cookie
			setcookie('address', json_encode($data['address']), time() + (86400 * 30), "/"); 
            return view('ajax.entity-type', $data);
        }
        return false;
    }
    public function reviewHelpfullVote(Request $request)
    {
        try {
            $helpfullVote = new \App\Model\HelpfullVote(['user_id'=>$request->input('user_id'),'vote'=>$request->input('voting')]);	
            $validator = Validator::make($request->all(), $helpfullVote->getCreateVotingRules());
            if (!$validator->passes()) {
                $response = array('status'=>0, 'message'=>$validator->errors()->first()); 
                return $response;
            }
            if(\App\Model\HelpfullVote::where(array('user_id'=>$request->input('user_id'),'review_id'=>$request->input('review_id')))->count()){
                    $status = \App\Model\HelpfullVote::where(array('user_id'=>$request->input('user_id'),'review_id'=>$request->input('review_id')))->first()->vote;
                    switch ($status) {
                            case 0:
                                    $response = array('status'=>0, 'message'=>'Already submitted thumbs down.');
                                    break;
                            case 1:
                                    $response = array('status'=>0, 'message'=>'Already submitted thumbs up.');
                                    break;					
                    }
                    return  $response;
            }
            $review = \App\Model\Review::find($request->input('review_id'));
            $helpfullVote->review()->associate($review);
            $helpfullVote->save();
            $up_count = $review->helpfullVote()->where('vote',1)->get()->count();
            $down_count = $review->helpfullVote()->where('vote',0)->get()->count();
        }catch(\Illuminate\Database\QueryException $e){
                return array('status'=>0, 'message'=>$e->getMessage());
        }
        $response = array('status'=>1, 'message'=>'Thank you for your feedback.','up_count'=>$up_count,'down_count'=>$down_count); 	
        return $response;		
    }
    public function addBookmark(Request $request){
        #Save search
        try{
            $validator =Validator::make($request->all(), [
                'user_id' => 'required',
                'profile_id'=>'required'
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return array('status'=>0, 'message'=>$error); 
            }
            if(\App\User::where('id',$request->input('user_id'))->get()->count()<1){
                return array('status'=>0, 'message'=>config('constants.message.user_not_found'));  
            }
            $role = DB::table('role_user')->where('user_id',$request->input('user_id'))->first()->role_id;
            switch ($role) {               
                case 3:
                    $user = \App\User::find($request->input('user_id'));
                    if($user->doctor()->where('id',$request->input('profile_id'))->get()->count()>0){
                        return array('status'=>0, 'message'=>config('constants.message.add_bookmark')); 
                    }
                    break;
                case 4:
                    $user = \App\User::find($request->input('user_id'));
                    if($user->diagnostic()->where('id',$request->input('profile_id'))->get()->count()>0){
                        return array('status'=>0, 'message'=>config('constants.message.add_bookmark')); 
                    }
                    break;
                case 5:
                    $user = \App\User::find($request->input('user_id'));
                    if($user->hospital()->where('id',$request->input('profile_id'))->get()->count()>0){
                        return array('status'=>0, 'message'=>config('constants.message.add_bookmark')); 
                    }
                    break;
            }
            if(\App\Model\Bookmark::where('user_id',$request->input('user_id'))->Where('entity_id',$request->input('profile_id'))->get()->count()>0){
                return array('status'=>1, 'message'=>'You have already bookmarked.'); 
            }
            \App\Model\Bookmark::create([
                "user_id"=>$request->input('user_id'),
                "entity_id"=>$request->input('profile_id'),              
            ]);
            $response = array('status'=>1, 'message'=>'Added successfully'); 
        } catch(\Illuminate\Database\QueryException $e){
			return array('status'=>0, 'message'=>$e->getMessage());
        }
        return $response;
    }
	public function nameAutoComplete(Request $request){
        if($request->isMethod('post')){
			$search_results = stripslashes($request->search_results);
			$search_results = json_decode($search_results);	
			$result	= [];		
			foreach($search_results as $search_result){
				if (stripos($search_result->name, $request->keyword) !== false) {
					$result[] = [
						'name'=>$search_result->name,
						'distance'=>$search_result->distance,
					];
				}				
			}
            /**
			 * ############### Deprecated on 21 Sep 017 #################################
			 *
			 $address = $request->session()->get('address');
            if($request->session()->has('filters')) {
                $filter = $request->session()->get('filters');
                $distance = $filter['max_distance'];
            }else{
                $settings = \App\Model\Setting::where('title','radius')->get()->first();
                $distance =$settings->value;
            }
            $latitude = ($address['latitude']?$address['latitude']:28.7040592);
            $longitude = ($address['longitude']?$address['longitude']:77.10249019999992); 
            
            $sql = "SELECT name, ( 3959 * acos ( cos ( radians('".$latitude."') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('".$longitude."') ) + sin ( radians('".$latitude."') ) * sin( radians( latitude ) ) ) )*1.609344 AS distance FROM doctors HAVING  name like '" . $request->input("keyword") . "%' AND "
            . "distance <= '".$distance."'"
            . " UNION SELECT name, ( 3959 * acos ( cos ( radians('".$latitude."') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('".$longitude."') ) + sin ( radians('".$latitude."') ) * sin( radians( latitude ) ) ) )*1.609344 AS distance FROM hospitals HAVING  name like '" . $request->input("keyword") . "%' AND "
            . "distance <= '".$distance."'"        
            . " UNION SELECT name, ( 3959 * acos ( cos ( radians('".$latitude."') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('".$longitude."') ) + sin ( radians('".$latitude."') ) * sin( radians( latitude ) ) ) )*1.609344 AS distance FROM diagnostics HAVING  name like '" . $request->input("keyword") . "%' AND "
            . "distance <= '".$distance."'"
            . " ORDER BY distance ASC LIMIT 0,10";
            $result =  DB::select($sql);
			return $result;
			*/
            return view('ajax.name-auto-complete', ['result'=>$result]);             
        }
    }
	public function sendOtp(Request $request){
		if ($request->ajax()) {
			$user = User::find($request->id);
			$authKey = "jxQJF5JPDPsJSRE06lKVYI2fdqZUBTFYTe-miBFuZkZtcuRIq6TDqxO51KWHpTIPxyZ28BYkWGe2M4M5jPeL82qYtM_dgPTx6F3xyM4feKA0U_wBnz1AjOcjMay1VbI7ucYrwwrJM2TF-tTRmq5rqg==";
            $fields = [
				"countryCode"=> "91",
                "mobileNumber" =>$user->user->mobile,
                "getGeneratedOTP" => true
			];
            $headers = [
				'Application-Key:'.$authKey,
				'Content-Type: application/json'
			];
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://sendotp.msg91.com/api/generateOTP' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );
            $result = json_decode($result);
            if ($result === FALSE) {
                die('Whoops failed: ' . curl_error($ch));
            }
			$otp = $result->response->oneTimePassword;
            $user->user->otp =  $otp;
			$user->user->save();
			curl_close($ch);
			return $otp;
		}
	}
    
}
