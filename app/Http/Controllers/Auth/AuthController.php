<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\ActivationService;
use Auth;
use Image;
use Illuminate\Support\Facades\Mail;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $activationService;
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(ActivationService $activationService)
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->activationService = $activationService;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator($request)
    {
        return Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:login',
            'mobile' => 'required',
            'gender' => 'required',
            'dob' => 'required',
            'password' => 'required|min:6',
			'agree_terms' =>'required'
        ],[
			'agree_terms.required' =>'Please agree to the Terms and Conditions.'
		]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create($request)
    {
        $image_url="";
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploaded/images/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $destinationPath = public_path('uploaded/images');
            $image->move($destinationPath, $input['imagename']);
            $image_url  =   url('/').'/public/uploaded/images/' . $input['imagename'];
        }
		### wordpress registration start here
        $wp_user = \wp_insert_user([
			'user_pass'=>$request->password,
			'user_login'=>$request->email,
			'user_email'=>$request->email,
			'role'=>'contributor'
        ]);
        ### Finished
        $profile = new \App\Model\Users([
            'name'=>$request->name,
            'mobile'=>$request->mobile,
			'image'=>$image_url,
            'dob'=>($request->dob?$request->dob:''),
            'city_name' => ($request->city_name?$request->city_name:''),
            'state_name'=> ($request->state_name?$request->state_name:''),
            'country_name' => ($request->country_name?$request->country_name:''),
            'country_code' => ($request->country_code?$request->country_code:''),
            'address_one' => ($request->address_one?$request->address_one:''),
            'address_two' => ($request->address_two?$request->address_two:''),
            'locality' => ($request->locality?$request->locality:''),
            'latitude' => ($request->latitude?$request->latitude:''),
            'longitude' => ($request->longitude?$request->longitude:' ')
        ]);
        User::insert([
			'id' => $wp_user,
            'email' => $request->email,
            'password' => bcrypt($request->password),
			'agree_terms' => $request->agree_terms
        ]); 
		$user = \App\User::find($wp_user);
        $user->roles()->attach(3);
        $profile->user()->associate($user);
        $profile->save();       
        return $user;
    }
    public function register(Request $request)
    {
        $validator = $this->validator($request);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator)
                ->with(['status'=>0,'message'=>'Failed.','type'=>2]);
        }
        $user = $this->create($request);
        $this->activationService->sendActivationMail($user);
		/**
		 * Sending welcome mail to user
		 */
		$data = [
			'email' => $user->email,
			'user_name'=>$user->user->name
		];
		Mail::send('email.welcome',['data'=>$data], function ($message) use ($data) {
				$message->to($data['email'])->subject('Welcome Email');
		});
        return back()->with(['status'=>1,'message'=>'We sent you an activation code. Check your email.', 'type'=>4]);
    }
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator)
                ->with(['status'=>0,'message'=>'Failed.','type'=>1]);
        }
        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->has('remember'))) {
            return back()
                ->withInput()
                ->withErrors(['email'=>'These credentials do not match our records.'])
                ->with(['status'=>0,'message'=>'Failed.','type'=>1]);
        }
        $user = auth()->user();
        if (!$user->activated) {
                $this->activationService->sendActivationMail($user);
                auth()->logout();
                return back()
                        ->withErrors(['email'=>'You need to confirm your account. We have sent you an activation code, please check your email.'])
                        ->with(['status'=>0,'message'=>'You need to confirm your account. We have sent you an activation code, please check your email.','type'=>1]);
        }
        if (!$user->status) { 
                auth()->logout();
                return back()
                        ->withErrors(['email'=>'Your account has been disabled. Please contact to admin.'])
                        ->with(['status'=>0,'message'=>'Your account has been disabled. Please contact to admin.','type'=>1]);
        }
        # wordpress code start here
        $wp_login = \wp_signon(array('user_login'=>$request->input('email'),'user_password'=>$request->input('password')));
        if(auth()->user()->hasRole(['administrator','super-admin','data-entry-user','moderator','manager'])){
			auth()->logout();
			return back()
				->withErrors(['email'=>'Permission denied.Contact to admin.'])
				->with(['status'=>0,'message'=>'Permission denied.Contact to admin','type'=>1]);
        }
		return back();
    }
    public function authenticated(Request $request, $user)
    {
        if (!$user->activated) {
                $this->activationService->sendActivationMail($user);
                auth()->logout();
                return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        # wordpress code start here
        $wp_login = \wp_signon(array('user_login'=>$request->input('email'),'user_password'=>$request->input('password')));
        return redirect()->intended($this->redirectPath());
    }
    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
			/**
			 *
			 * @Commented by Baleshwar
			 *
			 auth()->login($user);
			 *
			 *
			 return redirect($this->redirectPath());
			 */
			return redirect('/')->with(['status'=>1,'message'=>'You have successfully verified your account. You may now login.','type'=>1]);
        }
        abort(404);
    }
    public function logout()
    {
        Auth::guard($this->getGuard())->logout();
        #Log the current user out form wordpress session.
        \wp_logout();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }
}
