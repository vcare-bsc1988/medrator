<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use RedirectsUsers;
use Validator;
use App\Http\Controllers\HomeController;
class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
    protected $redirectTo = '/';
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
	/***
	 * Added by Baleshwar
	 *
	 */
	protected $homeController;
    public function __construct(HomeController $homeController)
    {
        $this->middleware('guest');
		/***
		 * Added by Baleshwar
		 *
		 */
		$this->homeController = $homeController;
    }

    /**
     * Get the name of the guest middleware.
     *
     * @return string
     */
    protected function guestMiddleware()
    {
        $guard = $this->getGuard();

        return $guard ? 'guest:'.$guard : 'guest';
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmail()
    {
        return $this->showLinkRequestForm();
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        if (property_exists($this, 'linkRequestView')) {
            return view($this->linkRequestView);
        }

        if (view()->exists('auth.passwords.email')) {
            //return view('auth.passwords.email');
            $data['specialties'] = \App\Model\Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
            $data['tests'] = \App\Model\Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
            $data['address'] = session('address');
            return view('home', $data);
        }

        return view('auth.password');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetLinkEmail(Request $request)
    {
        $validator = $this->validateSendResetLinkEmail($request);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator)
                ->with(['status'=>0,'message'=>'Failed.','type'=>3]);
        }
        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->getSendResetLinkEmailSuccessResponse($response);
            case Password::INVALID_USER:
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    }

    /**
     * Validate the request of sending reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateSendResetLinkEmail(Request $request)
    {
        //$this->validate($request, ['email' => 'required|email']);
        return Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
    }

    /**
     * Get the needed credentials for sending the reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getSendResetLinkEmailCredentials(Request $request)
    {
        return $request->only('email');
    }

    /**
     * Get the Closure which is used to build the password reset email message.
     *
     * @return \Closure
     */
    protected function resetEmailBuilder()
    {
        return function (Message $message) {
            $message->subject($this->getEmailSubject());
        };
    }

    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
    }

    /**
     * Get the response for after the reset link has been successfully sent.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailSuccessResponse($response)
    {
        //return redirect()->back()->with('status', trans($response));
        return back()
                ->withInput()
                ->with(['status'=>1,'message'=>trans($response),'type'=>3]);
    }

    /**
     * Get the response for after the reset link could not be sent.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailFailureResponse($response)
    {
        //return redirect()->back()->withErrors(['email' => trans($response)]);
        return back()
                ->withInput()
                ->withErrors(['email' => trans($response)])
                ->with(['status'=>0,'message'=>trans($response),'type'=>3]);
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function getReset(Request $request, $token = null)
    {
        return $this->showResetForm($request, $token);
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->getEmail();
        }

        $email = $request->input('email');

        if (property_exists($this, 'resetView')) {
            return view($this->resetView)->with(compact('token', 'email'));
        }
		/*****
		 * Comented by Baleshwar
         * @if (view()->exists('auth.passwords.reset')) {
         *   @return view('auth.passwords.reset')->with(compact('token', 'email'));
         * @}
         *
         * @return view('auth.reset')->with(compact('token', 'email'));
		 */
		/****
         * Getting user related interest blogs by Baleshwar
         */
        $user = auth()->user();
        if($user){
            $tags =  \App\User::find($user->id)->interests()->get();
            if ($tags->count()>0) {
                    $tag_ids = array();
                    foreach($tags as $individual_tag) 
                            $tag_ids[] = $individual_tag->interest_id;           
                    $args=array(
                            'tag__in' => $tag_ids,
                            'posts_per_page'=>4, // Number of related posts to display.
                            'caller_get_posts'=>1
                    );
                    $posts = get_posts($args);  
            }else{
                    $args=array(
                            'posts_per_page'=>4, // Number of related posts to display.
                            'caller_get_posts'=>1
                    );
                    $posts = get_posts($args);  
            }

        }else{
            $args=array(
                'posts_per_page'=>4, // Number of related posts to display.
                'caller_get_posts'=>1
            );
            $posts = get_posts($args);  
        }       
        $i=0;
        foreach($posts as $post):
			$images = [];
			$mediaImages = get_attached_media( 'image', $post->ID);
			foreach($mediaImages as $image){
					$images[]['img_url'] = $image->guid;
			}
			$posts[$i]->images= $images;
			$content = strip_tags($posts[$i]->post_content);
			$content= preg_replace('/[ ]{2,}|[\r\n]/', ' ', trim($content));
			$posts[$i]->post_content = $content;
			$i++;
        endforeach;
        $data['blogs'] = $posts;
		$data['specialties'] = \App\Model\Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
		$data['tests'] = \App\Model\Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
		$data['address'] = session('address');
		$result = $this->homeController->entityList($data['address']);
        $data = array_merge($data,$result);
		$popup_type = 5;// for reste password
		return view('home', $data)->with(compact('token', 'email', 'popup_type'));
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        return $this->reset($request);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        $this->validate(
            $request,
            $this->getResetValidationRules(),
            $this->getResetValidationMessages(),
            $this->getResetValidationCustomAttributes()
        );

        $credentials = $this->getResetCredentials($request);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);
            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function getResetValidationRules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    /**
     * Get the password reset validation messages.
     *
     * @return array
     */
    protected function getResetValidationMessages()
    {
        return [];
    }

    /**
     * Get the password reset validation custom attributes.
     *
     * @return array
     */
    protected function getResetValidationCustomAttributes()
    {
        return [];
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getResetCredentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();

        Auth::guard($this->getGuard())->login($user);
    }

    /**
     * Get the response for after a successful password reset.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetSuccessResponse($response)
    {
        //return redirect($this->redirectPath())->with('status', trans($response));
		//return redirect($this->redirectPath())->with(['status'=>0,'message'=>trans($response),'type'=>3]);
		return redirect($this->redirectPath());
		
    }

    /**
     * Get the response for after a failing password reset.
     *
     * @param  Request  $request
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetFailureResponse(Request $request, $response)
    {
        return redirect()->back()
            ->withInput($request->only('email'))
            ->withErrors(['email' => trans($response)]);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return string|null
     */
    public function getBroker()
    {
        return property_exists($this, 'broker') ? $this->broker : null;
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return string|null
     */
    protected function getGuard()
    {
        return property_exists($this, 'guard') ? $this->guard : null;
    }
}
