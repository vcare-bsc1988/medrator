<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\ServiceDetail;
use App\Model\Service;
use App\Model\Specialty;
use App\Model\SpecialtyDetail;
use Validator;
use App\Model\Diagnostic;
use Image;
class DiagnosticController extends Controller
{
    public function index()
    {
    }
    public function create()
    {
        $serviceArr=  Service::lists('name','id');
        $specialties = Specialty::lists('name','id');
        return view('diagnostic.create', ['services'=>$serviceArr,'specialties'=>$specialties]);
    }
    public function store(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
			'country' => 'required',
			'state' => 'required',
			'address' => 'required',
			'city' => 'required',
			'pincode' => 'required',
			'home_collection_facility'=>'required',
			'online_bookings'=>'required',
			'online_reports'=>'required',
			'online_reports'=>'required',
			'online_reports'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect('/diagnostic/create')
                ->withInput()
                ->withErrors($validator);
        }
        $formData = $request->except(['_token','specialty','service']);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploaded/images/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $destinationPath = public_path('uploaded/images');
            $image->move($destinationPath, $input['imagename']);
            $formData['image'] =$input['imagename'];

        }
        Diagnostic::create($formData);
        $request->session()->flash('success', 'Diagnostic added successfully !');
        return redirect('/diagnostic/create');
    }
    public function uploadGalleries(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'image' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('uploaded/images/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['imagename']);

                $destinationPath = public_path('uploaded/images');
                $image->move($destinationPath, $input['imagename']);
                $image_url = url('/').'/public/uploaded/images/' . $input['imagename'];
                
                $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
                $gallery = new \App\Model\Gallery([
                    'image_url' => $image_url
                ]);
                $gallery->diagnostic()->associate($diagnostic);
                $gallery->save();
            }
            return back()->with(['status'=>1,'message'=>'Uploaded successfully.']);
        }
    }
    public function schedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $diagnostic = \App\Model\Diagnostic::find($request->input('id')); 
            
            $working_times = $diagnostic->workingTimes;
            $data =[
                'diagnostic' => $diagnostic
            ];
            return view('account.diagnostic-schedules', $data);
        }
    }
    public function saveSunSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'diagnostic_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $diagnostic = \App\Model\Diagnostic::find($request->input('diagnostic_id'));
            $diagnostic->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to']
                ]);
		$working_time->diagnostic()->associate($diagnostic);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveMonSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'diagnostic_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $diagnostic = \App\Model\Diagnostic::find($request->input('diagnostic_id'));
            $diagnostic->schedular()->where('week_day',2)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'diagnostic_id' => $request->input('diagnostic_id'),
                ]);
		$working_time->diagnostic()->associate($diagnostic);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveTueSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'diagnostic_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $diagnostic = \App\Model\Diagnostic::find($request->input('diagnostic_id'));
            $diagnostic->schedular()->where('week_day',3)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'diagnostic_id' => $request->input('diagnostic_id'),
                ]);
		$working_time->diagnostic()->associate($diagnostic);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveWedSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'diagnostic_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $diagnostic = \App\Model\Diagnostic::find($request->input('diagnostic_id'));
            $diagnostic->schedular()->where('week_day',4)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'diagnostic_id' => $request->input('diagnostic_id'),
                ]);
		$working_time->diagnostic()->associate($diagnostic);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveThrSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'diagnostic_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $diagnostic = \App\Model\Diagnostic::find($request->input('diagnostic_id'));
            $diagnostic->schedular()->where('week_day',5)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'diagnostic_id' => $request->input('diagnostic_id'),
                ]);
		$working_time->diagnostic()->associate($diagnostic);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveFriSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'diagnostic_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $diagnostic = \App\Model\Diagnostic::find($request->input('diagnostic_id'));
            $diagnostic->schedular()->where('week_day',6)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'diagnostic_id' => $request->input('diagnostic_id'),
                ]);
		$working_time->diagnostic()->associate($diagnostic);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveSatSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'diagnostic_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
            $diagnostic = \App\Model\Diagnostic::find($request->input('diagnostic_id'));
            $diagnostic->schedular()->where('week_day',6)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'diagnostic_id' => $request->input('diagnostic_id'),
                ]);
                $working_time->diagnostic()->associate($diagnostic);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
}
