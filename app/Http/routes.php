<?php
Route::group(['middleware' => ['acl']], function () {
	Route::get('/', 'HomeController@index');
	Route::auth();
	Route::get('find', 'SearchController@index');
	Route::post('find/autocomplet', 'SearchController@autoComplete');
	Route::post('name/autocomplet', 'AjaxController@nameAutoComplete');
	Route::get('find/doctor/{state}/{specialty}', 'SearchController@doctor');
	Route::get('find/hospital/{state}/{distance}', 'SearchController@hospital');
	Route::get('find/diagnostic/{state}/{test}', 'SearchController@diagnostic');
	Route::get('doctor/{name_slug}', 'SearchController@doctor_details');
	Route::get('login', function()
	{
		$data['specialties'] = \App\Model\Specialties::select('id','name')->orderBy('is_top', 'desc')->get();
		$data['tests'] = \App\Model\Test::select('id','name')->orderBy('is_top', 'desc')->get(); 
		$data['address'] = session('address');
		return view('home', $data);
	});
	Route::get('profile', 'AccountController@profile');
	Route::post('profile/save', 'AccountController@save');
	Route::post('account/verify-mobile', 'AccountController@verifyMobile');
	Route::get('claimed-profile', 'AccountController@claimedProfile');
	Route::get('account/my-ratings', 'AccountController@myRatings');
	Route::get('change-password', 'AccountController@changePassword');
	Route::get('account/add-doctor', 'HomeController@addDoctor');
	Route::post('account/add-doctor', 'HomeController@addDoctor');
	Route::resource('account/add-hospital', 'HomeController@addHospital');
	Route::resource('account/add-diagnostic', 'HomeController@addDiagnostic');
	/**
	* Save new hospital profile associated with user if no search result found.
	*/
	Route::post('account/save-doctor', 'AccountController@saveDoctor');
	/**
	* Save new hospital profile associated with user if no search result found.
	*/
	Route::post('account/save-hospital', 'AccountController@saveHospital');
	/**
	* Save new diagnostic profile associated with user if no search result found.
	*/
	Route::post('account/save-diagnostic', 'AccountController@saveDiagnostic');

	Route::post('doctor/upload-image', 'DoctorController@uploadImage');
	Route::post('user/upload-image', 'UserController@uploadImage');
	Route::get('add-profile', 'AccountController@addProfile');
	Route::get('account/profile-form', 'AccountController@profileForm');
	Route::get('account/search-profile', 'AccountController@searchProfile');
	Route::get('account/clinics', 'AccountController@clinics');
	Route::get('account/hospital', 'AccountController@hospital');
	Route::get('account/diagnostic', 'AccountController@diagnostic');
	/***
	 * Update clinic/hospital/diagnostic
	 */
	Route::get('account/update-hospital/{id}', 'AccountController@updateHospital');
	Route::get('account/update-diagnostic/{id}', 'AccountController@updateDiagnostic');
	Route::get('account/update-clinic/{id}', 'AccountController@updateClinic');
	Route::get('account/add-clinic', 'AccountController@addClinic');
	Route::post('account/save-clinic', 'AccountController@saveClinic');

	Route::get('account/search-clinics', 'AccountController@searchClinics');
	Route::get('account/add-clinic', 'AccountController@addClinic');
	Route::resource('account/change-password', 'AccountController@changePassword');
	Route::resource('test', 'TestController@index');
	Route::get('wp-login', 'HomeController@WpLogin');
	Route::get('account/claim-doctor/{id}', 'AccountController@claimDoctor');
	Route::get('account/claim-hospital/{id}', 'AccountController@claimHospital');
	Route::post('account/claiming-hospital', ['middleware' => ['auth.user'],'uses'=> 'AccountController@claimingHospital']);
	Route::get('account/claim-doctor/{id}', 'AccountController@claimDoctor');
	Route::post('account/claiming-doctor', ['middleware' => ['auth.user'],'uses'=> 'AccountController@claimingDoctor']);
	Route::get('account/claim-diagnostic/{id}', 'AccountController@claimDiagnostic');
	Route::post('account/claiming-diagnostic', ['middleware' => ['auth.user'],'uses'=> 'AccountController@claimingDiagnostic']);
	Route::get('about-us', 'HomeController@aboutUs');
	Route::get('disclaimer', 'HomeController@disclaimer');
	Route::get('copyright', 'HomeController@copyright');
	Route::get('privacy-policy', 'HomeController@privacyPolicy');
	Route::get('healt-tools', 'HomeController@healtTools');
	Route::get('organ-donation', 'HomeController@organDonation');
	Route::get('medical-tourism', 'HomeController@medicalTourism');
	Route::get('buy-insurance', 'HomeController@buyInsurance');
	Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');
	Route::get('/redirect/{provider}', 'SocialAuthController@redirect');
	Route::get('/auth/{provider}', 'SocialAuthController@auth');
	Route::post('session/stor_city', 'AjaxController@sessionForCity');
	Route::get('/ping', 'SolariumController@ping');
	Route::get('/hospital/{name_slug}', 'SearchController@hospital_details');
	Route::get('/diagnostic/{name_slug}', 'SearchController@diagnostic_details');
	Route::get('/doctor/{name_slug}', 'SearchController@doctor_details');
	Route::get('/home', 'HomeController@index');
});
Route::group(['middleware' => ['auth.user']], function () {
	Route::resource('report-errors', 'HomeController@reportErrors');
	Route::get('notification-unread/{id}', 'HomeController@notificationUnread');
	Route::get('notification-read/{id}', 'HomeController@notificationRead');
	Route::get('clear-notification/{id}', 'HomeController@clearNotification');
	Route::get('account/notifications', 'HomeController@notificationAll');
	Route::post('account/send-otp', 'AjaxController@sendOtp');
});

// Route that handles submission of review - rating/comment
Route::post('doctor/{name_slug}', array('middleware' => 'auth.user', function($name_slug)
{
	$id = Request::get('id');
    $collection = collect(Request::get('rating'));
    $input = array(
        'title' => Request::get('title'),
        'comment' => Request::get('comment'),
        'rating'  => $collection->avg(),
        'ratings'  => Request::get('ratings'),
        'disease_visited'  => Request::get('disease_visited'),
		'post_anonymous'  => Request::get('post_anonymous'),
    );
    // instantiate Rating model
    $review = new App\Model\Review;

    // Validate that the user's input corresponds to the rules specified in the review model
    $validator = Validator::make( $input, $review->getCreateRules());

    // If input passes validation - store the review in DB, otherwise return to product page with error message 
    if ($validator->passes()) {
		if(\App\Model\Review::where('user_id', Auth::user()->id)->where(function ($query) use ($id) {
            $query->where('doctor_id', $id)
                    ->orWhere('hospital_id', $id)
                    ->orWhere('diagnostic_id', $id);
		})->count()){
			return Redirect::to('doctor/'.$name_slug.'#reviews-anchor')->withErrors(['You already submitted review.'])->withInput();
		}
		
        $review->storeReviewForWebDoctor($input,$id,$input['title'],$input['comment'], $input['rating'],$input['ratings'],Auth::user()->id);
        return Redirect::to('doctor/'.$name_slug.'#reviews-anchor')->with('review_posted',true);
    }
    return Redirect::to('doctor/'.$name_slug.'#reviews-anchor')->withErrors($validator)->withInput();
}));
Route::post('hospital/{name_slug}', array('middleware' => 'auth.user', function($name_slug)
{
    $id = Request::get('id');
	$collection = collect(Request::get('rating'));
    $input = array(
        'title' => Request::get('title'),
        'comment' => Request::get('comment'),
        'rating'  => $collection->avg(),
        'ratings'  => Request::get('ratings'),
        'disease_visited'  => Request::get('disease_visited'),
		'post_anonymous'  => Request::get('post_anonymous'),
    );
    // instantiate Rating model
    $review = new App\Model\Review;
    // Validate that the user's input corresponds to the rules specified in the review model
    $validator = Validator::make( $input, $review->getCreateRules());
    // If input passes validation - store the review in DB, otherwise return to product page with error message 
    if ($validator->passes()) {
		if(\App\Model\Review::where('user_id', Auth::user()->id)->where(function ($query) use ($id) {
                $query->where('doctor_id', $id)
                        ->orWhere('hospital_id', $id)
                        ->orWhere('diagnostic_id', $id);
        })->count()){
			return Redirect::to('hospital/'.$id.'#reviews-anchor')->withErrors(['You already submitted review.'])->withInput();
		}
		
        $review->storeReviewForWebHospital($input,$id, $input['title'], $input['comment'], $input['rating'],$input['ratings'],Auth::user()->id);
        return Redirect::to('hospital/'.$name_slug.'#reviews-anchor')->with('review_posted',true);
    }
    return Redirect::to('hospital/'.$name_slug.'#reviews-anchor')->withErrors($validator)->withInput();
}));
Route::post('diagnostic/{name_slug}', array('middleware' => 'auth.user', function($name_slug)
{
    $id = Request::get('id');
	$collection = collect(Request::get('rating'));
    $input = array(
        'title' => Request::get('title'),
        'comment' => Request::get('comment'),
        'rating'  => $collection->avg(),
        'ratings'  => Request::get('ratings'),
        'disease_visited'  => Request::get('disease_visited'),
		'post_anonymous'  => Request::get('post_anonymous'),
    );
    // instantiate Rating model
    $review = new App\Model\Review;

    // Validate that the user's input corresponds to the rules specified in the review model
    $validator = Validator::make( $input, $review->getCreateRules());
    
    // If input passes validation - store the review in DB, otherwise return to product page with error message 
    if ($validator->passes()) {
		if(\App\Model\Review::where('user_id', Auth::user()->id)->where(function ($query) use ($id) {
                    $query->where('doctor_id', $id)
                            ->orWhere('hospital_id', $id)
                            ->orWhere('diagnostic_id', $id);
            })->count()){
            return Redirect::to('diagnostic/'.$name_slug.'#reviews-anchor')->withErrors(['You already submitted review.'])->withInput();
		}
		$review->storeReviewForWebDiagnostic($input, $id, $input['title'],$input['comment'], $input['rating'],$input['ratings'],Auth::user()->id);
		return Redirect::to('diagnostic/'.$name_slug.'#reviews-anchor')->with('review_posted',true);
    }
    return Redirect::to('diagnostic/'.$name_slug.'#reviews-anchor')->withErrors($validator)->withInput();
}));
Route::group(['middleware' => ['acl']], function () {
	Route::get('importExport', 'MaatwebsiteDemoController@importExport');
	Route::get('downloadExcel/{type}', 'MaatwebsiteDemoController@downloadExcel');
	Route::post('importExcel', 'MaatwebsiteDemoController@importExcel');
	Route::get('test',['middleware' => ['auth.user'],'uses'=> 'TestController@index']);
	Route::get('review/select-specialty', 'ReviewController@selectSpecialty');
	Route::get('review/doctor/{specialty}', 'ReviewController@doctor');
	Route::get('review/hospital', 'ReviewController@hospital');
	Route::get('review/diagnostic', 'ReviewController@diagnostic');
	Route::post('ajax/thusm-down', 'AjaxController@reviewHelpfullVote');
	Route::post('ajax/thums-up', 'AjaxController@reviewHelpfullVote');
	Route::post('ajax/add_bookmark', 'AjaxController@addBookmark');
	Route::get('account/bookmarks', 'AccountController@bookmarks');
	Route::get('account/points-collected', 'AccountController@pointsCollected');
	Route::get('account/recent-search', 'AccountController@recentSearch');
	/**
	 * Saving clinic schedules
	 */
	Route::post('account/save-sun-schedules', 'AccountController@saveSunSchedules');
	Route::post('account/save-mon-schedules', 'AccountController@saveMonSchedules');
	Route::post('account/save-tue-schedules', 'AccountController@saveTueSchedules');
	Route::post('account/save-wed-schedules', 'AccountController@saveWedSchedules');
	Route::post('account/save-thr-schedules', 'AccountController@saveThrSchedules');
	Route::post('account/save-fri-schedules', 'AccountController@saveFriSchedules');
	Route::post('account/save-sat-schedules', 'AccountController@saveSatSchedules');
	Route::post('account/get-clinic', 'AccountController@getClinic');
	/***
	 * Stroing diagnostic schedules
	 */
	Route::post('diagnostic/schedules', 'DiagnosticController@schedules');
	Route::post('diagnostic/save-sun-schedules', 'DiagnosticController@saveSunSchedules');
	Route::post('diagnostic/save-mon-schedules', 'DiagnosticController@saveMonSchedules');
	Route::post('diagnostic/save-tue-schedules', 'DiagnosticController@saveTueSchedules');
	Route::post('diagnostic/save-wed-schedules', 'DiagnosticController@saveWedSchedules');
	Route::post('diagnostic/save-thr-schedules', 'DiagnosticController@saveThrSchedules');
	Route::post('diagnostic/save-fri-schedules', 'DiagnosticController@saveFriSchedules');
	Route::post('diagnostic/save-sat-schedules', 'DiagnosticController@saveSatSchedules');
	/****
	 * Saving hospital schedules
	 */
	Route::post('hospital/schedules', 'HospitalController@schedules');
	Route::post('hospital/save-sun-schedules', 'HospitalController@saveSunSchedules');
	Route::post('hospital/save-mon-schedules', 'HospitalController@saveMonSchedules');
	Route::post('hospital/save-tue-schedules', 'HospitalController@saveTueSchedules');
	Route::post('hospital/save-wed-schedules', 'HospitalController@saveWedSchedules');
	Route::post('hospital/save-thr-schedules', 'HospitalController@saveThrSchedules');
	Route::post('hospital/save-fri-schedules', 'HospitalController@saveFriSchedules');
	Route::post('hospital/save-sat-schedules', 'HospitalController@saveSatSchedules');
	/***
	 * Upload/delete Hospital/clinic/Diagnostic gallery
	 */
	Route::post('hospital/upload-image', 'HospitalController@uploadGalleries');
	Route::post('diagnostic/upload-image', 'DiagnosticController@uploadGalleries');
	Route::get('account/delete-gallery/{id}', 'AccountController@deleteGallery');

	Route::get('reviews', 'ReviewController@reviews');
	Route::post('news-letter/subscriber', 'HomeController@newsletterSubscriber');
	Route::post('get-specialties', 'HomeController@getSpecialties');
	Route::post('home-specialties', 'HomeController@homeSpecilties');
	Route::post('get-tests', 'HomeController@getTests');
	Route::post('home-tests', 'HomeController@homeTests');
	Route::get('home/medical-tourism', 'HomeController@medicalTourism');
	Route::get('home/organ-donation', 'HomeController@organDonation');
	Route::get('home/health-tools', 'HomeController@healtTools');
	/**
	 * Route for notifications
	 */
	Route::get('home/clear-notifications/{id}', 'HomeController@clearNotifications');
	/***
	 * Routes for static pages
	 */
	Route::get('contact-us', 'HomeController@contactUs');
	Route::get('terms-of-service', 'HomeController@termsOfService');
});


/***
 * Routes for webservices pages.
 */
Route::group(['prefix' => '/api/v1.0'], function()
{
	Route::post('login', 'Api\Beta\LoginController@index');
    Route::post('register', 'Api\Beta\RegisterController@index');
    Route::post('social_login', 'Api\Beta\SocialLoginController@index');
    Route::post('account/add', 'Api\Beta\AccountController@add');
    Route::post('account/add_doctor', 'Api\Beta\AccountController@add_doctor');
    Route::post('account/add_associate_doctorwithuser', 'Api\Beta\AccountController@add_associate_doctor');
    #Route::post('doctor/add_associate_clinicwithdoctor', 'Api\V1\AccountController@associate_clinic');
    Route::post('doctor/add_associate_hospitalwithdoctor', 'Api\Beta\AccountController@associate_hospital');
    Route::post('account/upload_image', 'Api\Beta\AccountController@uploadImage');
    Route::get('service/countries', 'Api\Beta\ServiceController@countries');
    Route::post('service/cities', 'Api\Beta\ServiceController@cities');
    Route::get('service/specialties', 'Api\Beta\ServiceController@specialties');
    Route::post('account/edit', 'Api\Beta\AccountController@edit');
    Route::post('change/password', 'Api\Beta\AccountController@changePassword');
    Route::get('qualifications', 'Api\Beta\ServiceController@qualifications');
    Route::get('diagnostic_tests', 'Api\Beta\ServiceController@diagnosticTests');
    Route::post('account/profile', 'Api\Beta\AccountController@get_profile');
    Route::get('service_list', 'Api\Beta\ServiceController@service_list');
    Route::post('test', 'Api\Beta\ServiceController@test');
    Route::post('account/get_doctors', 'Api\Beta\AccountController@get_doctors');
    #Route::post('account/get_clinics', 'Api\V1\AccountController@getClinics');
    Route::post('account/get_hospitals', 'Api\Beta\AccountController@getHospitals');
    #Route::post('account/update_clinics', 'Api\V1\AccountController@updateClinic');
    Route::post('account/update_hospital', 'Api\Beta\AccountController@updateHospital');
    Route::post('account/profiles', 'Api\Beta\AccountController@profiles');
    Route::post('account/forgot_password', 'Api\Beta\AccountController@forgotPassword');
    Route::post('account/add_hospital', 'Api\Beta\AccountController@addHospital');
    Route::post('account/add_diagnostic', 'Api\Beta\AccountController@addDiagnostic');
    Route::post('account/add_clinic', 'Api\Beta\AccountController@addClinic');
    Route::post('account/upload_gallery', 'Api\Beta\AccountController@uploadGalleryImage');
    Route::post('account/get_otp', 'Api\Beta\AccountController@getOtp');
    Route::post('account/verify_mobile', 'Api\Beta\AccountController@verifyMobile');
    Route::resource('search', 'Api\Beta\SearchController@index');
    Route::resource('search/doctor', 'Api\Beta\SearchController@doctor');
    Route::resource('search/hospital_clinic', 'Api\Beta\SearchController@hospital');
    Route::resource('search/diagnostic', 'Api\Beta\SearchController@diagnostic');
    Route::resource('search/clinic', 'Api\Beta\SearchController@clinic');
    Route::resource('doctor/add_schedular', 'Api\Beta\AccountController@addSchedule');
    Route::resource('claim_profile', 'Api\Beta\AccountController@claimProfile');
    Route::post('rating_review', 'Api\Beta\ServiceController@ratingReview');
    Route::post('review/helpfull_vote', 'Api\Beta\ServiceController@reviewHelpfullVote');
    Route::post('reviews_list', 'Api\Beta\ServiceController@reviewsList');
    Route::post('service/home', 'Api\Beta\ServiceController@entityList');
    Route::get('groupwise-specialties', 'Api\Beta\ServiceController@groupwiseSpecialties');
    Route::get('service/entity-items', 'Api\Beta\ServiceController@entitityItems');
    Route::post('service/associted-hospitals', 'Api\Beta\ServiceController@associatedHospitals');
    Route::post('service/near-by-doctors', 'Api\Beta\ServiceController@nearByDoctors');
    Route::post('service/doctor-descriptions', 'Api\Beta\ServiceController@doctorDescription');
    Route::post('service/hospital-descriptions', 'Api\Beta\ServiceController@hospitalDescription');
    Route::post('service/diagnostic-descriptions', 'Api\Beta\ServiceController@diagnosticDescription');
    Route::post('service/recent_search', 'Api\Beta\ServiceController@recentSearch');
    Route::post('service/add_bookmark', 'Api\Beta\ServiceController@addBookmark');
    Route::post('service/bookmarks', 'Api\Beta\ServiceController@bookmarks');
    Route::post('service/delete_account', 'Api\Beta\ServiceController@deleteAccount');
    Route::post('service/send_feedback', 'Api\Beta\ServiceController@sendFeedback');
    Route::post('service/user_associated_doctor_profile', 'Api\Beta\ServiceController@getUserAssociatedDoctorProfile');
    Route::post('service/report_errors', 'Api\Beta\ServiceController@reportErrors');
    Route::post('service/get_user_review_list', 'Api\Beta\ServiceController@getUserReviewList');   
	Route::post('service/user_interests', 'Api\Beta\ServiceController@userInterests');
    Route::post('service/add_interest', 'Api\Beta\ServiceController@addInterest');	
	Route::post('service/get_notifications', 'Api\Beta\ServiceController@getNotifications');
    Route::post('service/set_read_notification', 'Api\Beta\ServiceController@setReadNotification');
	Route::post('testing/push_notification','Api\Beta\TestingController@pushNotification');
    Route::post('service/points', 'Api\Beta\ServiceController@points');
	Route::post('service/schedules', 'Api\Beta\ServiceController@schedules');
    Route::post('service/send-otp', 'Api\Beta\ServiceController@sendOtp');
    Route::post('service/verify-mobile', 'Api\Beta\ServiceController@verifyMobile');
	Route::post('claiming-search', 'Api\Beta\SearchController@claimingSearch');
});
Route::group(['prefix' => '/api/beta'], function()
{
    Route::post('login', 'Api\Beta\LoginController@index');
    Route::post('register', 'Api\Beta\RegisterController@index');
    Route::post('social_login', 'Api\Beta\SocialLoginController@index');
    Route::post('account/add', 'Api\Beta\AccountController@add');
    Route::post('account/add_doctor', 'Api\Beta\AccountController@add_doctor');
    Route::post('account/add_associate_doctorwithuser', 'Api\Beta\AccountController@add_associate_doctor');
    #Route::post('doctor/add_associate_clinicwithdoctor', 'Api\V1\AccountController@associate_clinic');
    Route::post('doctor/add_associate_hospitalwithdoctor', 'Api\Beta\AccountController@associate_hospital');
    Route::post('account/upload_image', 'Api\Beta\AccountController@uploadImage');
    Route::get('service/countries', 'Api\Beta\ServiceController@countries');
    Route::post('service/cities', 'Api\Beta\ServiceController@cities');
    Route::get('service/specialties', 'Api\Beta\ServiceController@specialties');
    Route::post('account/edit', 'Api\Beta\AccountController@edit');
    Route::post('change/password', 'Api\Beta\AccountController@changePassword');
    Route::get('qualifications', 'Api\Beta\ServiceController@qualifications');
    Route::get('diagnostic_tests', 'Api\Beta\ServiceController@diagnosticTests');
    Route::post('account/profile', 'Api\Beta\AccountController@get_profile');
    Route::get('service_list', 'Api\Beta\ServiceController@service_list');
    Route::post('test', 'Api\Beta\ServiceController@test');
    Route::post('account/get_doctors', 'Api\Beta\AccountController@get_doctors');
    #Route::post('account/get_clinics', 'Api\V1\AccountController@getClinics');
    Route::post('account/get_hospitals', 'Api\Beta\AccountController@getHospitals');
    #Route::post('account/update_clinics', 'Api\V1\AccountController@updateClinic');
    Route::post('account/update_hospital', 'Api\Beta\AccountController@updateHospital');
    Route::post('account/profiles', 'Api\Beta\AccountController@profiles');
    Route::post('account/forgot_password', 'Api\Beta\AccountController@forgotPassword');
    Route::post('account/add_hospital', 'Api\Beta\AccountController@addHospital');
    Route::post('account/add_diagnostic', 'Api\Beta\AccountController@addDiagnostic');
    Route::post('account/add_clinic', 'Api\Beta\AccountController@addClinic');
    Route::post('account/upload_gallery', 'Api\Beta\AccountController@uploadGalleryImage');
    Route::post('account/get_otp', 'Api\Beta\AccountController@getOtp');
    Route::post('account/verify_mobile', 'Api\Beta\AccountController@verifyMobile');
    Route::resource('search', 'Api\Beta\SearchController@index');
    Route::resource('search/doctor', 'Api\Beta\SearchController@doctor');
    Route::resource('search/hospital_clinic', 'Api\Beta\SearchController@hospital');
    Route::resource('search/diagnostic', 'Api\Beta\SearchController@diagnostic');
    Route::resource('search/clinic', 'Api\Beta\SearchController@clinic');
    Route::resource('doctor/add_schedular', 'Api\Beta\AccountController@addSchedule');
    Route::resource('claim_profile', 'Api\Beta\AccountController@claimProfile');
    Route::post('rating_review', 'Api\Beta\ServiceController@ratingReview');
    Route::post('review/helpfull_vote', 'Api\Beta\ServiceController@reviewHelpfullVote');
    Route::post('reviews_list', 'Api\Beta\ServiceController@reviewsList');
    Route::post('service/home', 'Api\Beta\ServiceController@entityList');
    Route::get('groupwise-specialties', 'Api\Beta\ServiceController@groupwiseSpecialties');
    Route::get('service/entity-items', 'Api\Beta\ServiceController@entitityItems');
    Route::post('service/associted-hospitals', 'Api\Beta\ServiceController@associatedHospitals');
    Route::post('service/near-by-doctors', 'Api\Beta\ServiceController@nearByDoctors');
    Route::post('service/doctor-descriptions', 'Api\Beta\ServiceController@doctorDescription');
    Route::post('service/hospital-descriptions', 'Api\Beta\ServiceController@hospitalDescription');
    Route::post('service/diagnostic-descriptions', 'Api\Beta\ServiceController@diagnosticDescription');
    Route::post('service/recent_search', 'Api\Beta\ServiceController@recentSearch');
    Route::post('service/add_bookmark', 'Api\Beta\ServiceController@addBookmark');
    Route::post('service/bookmarks', 'Api\Beta\ServiceController@bookmarks');
    Route::post('service/delete_account', 'Api\Beta\ServiceController@deleteAccount');
    Route::post('service/send_feedback', 'Api\Beta\ServiceController@sendFeedback');
    Route::post('service/user_associated_doctor_profile', 'Api\Beta\ServiceController@getUserAssociatedDoctorProfile');
    Route::post('service/report_errors', 'Api\Beta\ServiceController@reportErrors');
    Route::post('service/get_user_review_list', 'Api\Beta\ServiceController@getUserReviewList');   
	Route::post('service/user_interests', 'Api\Beta\ServiceController@userInterests');
    Route::post('service/add_interest', 'Api\Beta\ServiceController@addInterest');	
	Route::post('service/get_notifications', 'Api\Beta\ServiceController@getNotifications');
    Route::post('service/set_read_notification', 'Api\Beta\ServiceController@setReadNotification');
	Route::post('testing/push_notification','Api\Beta\TestingController@pushNotification');
    Route::post('service/points', 'Api\Beta\ServiceController@points');
	Route::post('service/schedules', 'Api\Beta\ServiceController@schedules');
    Route::post('service/send-otp', 'Api\Beta\ServiceController@sendOtp');
    Route::post('service/verify-mobile', 'Api\Beta\ServiceController@verifyMobile');
	Route::post('claiming-search', 'Api\Beta\SearchController@claimingSearch');
	Route::post('service/get_blogs', 'Api\Beta\ServiceController@getBlogs');
});
