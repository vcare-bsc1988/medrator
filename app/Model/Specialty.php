<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
    protected $guarded = [];
	public $timestamps = false;
	protected $table = 'specialties';
}
