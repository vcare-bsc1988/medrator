<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReviewDetail extends Model
{
	protected $guarded = [];
	public $timestamps = false;
	public function reviews()
	{
		 return $this->belongsTo('App\Model\Review');
	}
	public function reviewParams()
	{
		return $this->belongsTo('App\Model\ReviewParameter');
	}
	public function param()
	{
		return $this->belongsTo('App\Model\ReviewParameter','review_parameter_id');
	}
}
