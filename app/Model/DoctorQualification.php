<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorQualification extends Model
{
    public $timestamps = false;
	protected $table = 'doctor_qualification';
	public function degree(){
		return $this->belongsTo('App\Model\Qualifications');
	}	
}
