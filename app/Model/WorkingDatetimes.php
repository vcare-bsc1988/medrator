<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WorkingDatetimes extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    public function doctor()
    {
        return $this->belongsTo('App\Model\Doctor');
    }
    public function hospital()
    {
        return $this->belongsTo('App\Model\Hospital');
    }
    public function diagnostic()
    {
        return $this->belongsTo('App\Model\Diagnostic');
    }
    public function schedules($doctor_id,$hospital_id){
       ## Finding monday schedule.
        $data = [];
        for($i=0; $i<7; $i++){
                $timings = \App\Model\WorkingDatetimes::select('start_time','end_time')
                                ->where('doctor_id', $doctor_id)
                                ->where('hospital_id', $hospital_id)
                                 ->where('week_day', $i)
                                ->get();
                if($timings->count()>0){
                        $data[$i] =['week_day'=>$i,'timings'=>$timings];
                }		
        }
        return $data;
    }
}
