<?php

namespace App\Model;
use App\Model\Doctor;
use Illuminate\Database\Eloquent\Model;

class ServiceDetail extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $table = 'service_details';
    public function doctor()
    {
        return $this->belongsTo(Doctor::class);
    }
    public function service(){
        return $this->belongsTo(\App\Model\Service::class);
    }
}
