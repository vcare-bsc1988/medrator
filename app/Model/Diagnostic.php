<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Diagnostic extends Model
{
     protected $table = 'diagnostics';
     protected $guarded = [];
	 public static function boot() {
        parent::boot();

        static::creating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = is_null($value) ? " " : $value;
            }
			$model->updated_at = \Carbon\Carbon::today()->format('Y-m-d H:i:s');
			$title = $model->name." ". $model->locality;
			$name_slug = str_slug($title, "-");
			$model->name_slug = $name_slug;
        });
		static::updating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = is_null($value) ? " " : $value;
            }
			$model->updated_at = \Carbon\Carbon::today()->format('Y-m-d H:i:s');
			$title = $model->name." ". $model->locality;
			$name_slug = str_slug($title, "-");
			$model->name_slug = $name_slug;
        });
    }
	 public function claimedProfiles()
	 {
		 return $this->hasMany('App\Model\ClaimedProfile');
	 }
	 public function reviews()
	{
	    return $this->hasMany('App\Model\Review');
	}

	// The way average rating is calculated (and stored) is by getting an average of all ratings, 
	// storing the calculated value in the rating_cache column (so that we don't have to do calculations later)
	// and incrementing the rating_count column by 1

    public function recalculateRating($rating)
    {
    	$reviews = $this->reviews()->notSpam()->approved();
	    $avgRating = $reviews->avg('rating');
		switch ($rating) {
            case 1:
                $this->rating_one_star_count = $reviews->where('rating',1)->count(); 
                break;
            case 2:
                $this->rating_two_star_count = $reviews->where('rating',2)->count();
                break;
            case 3:
                $this->rating_three_star_count = $reviews->where('rating',3)->count();
                break;
            case 4:
                 $this->rating_four_star_count = $reviews->where('rating',4)->count();
                break;
            case 5:
                $this->rating_five_star_count = $reviews->where('rating',5)->count();
                break;
        }
		$this->rating_cache = round($avgRating,1);
		$this->rating_count = $this->reviews()->notSpam()->approved()->count();
    	$this->save();
    }
	public static function diagnosticList(){
		$diagnostics= DB::table('diagnostics')->take(5)->get();
		$i=0;
		foreach($diagnostics as $diagnostic){
			#Getting Diagnostic Tests
			$tests = DB::table('diagnostic_test')
				->join('tests', 'diagnostic_test.test_id', '=', 'tests.id')
				->where('diagnostic_test.diagnostic_id', '=', $diagnostic->id)
				->select('tests.id','tests.name')
				->get();
			$diagnostics[$i++]->tests = $tests;
		}
		return $diagnostics;
	}
	public function doctors()
    {
        return $this->belongsToMany('App\Model\Doctor');
    }
    public function tests(){
        return $this->belongsToMany('App\Model\Test','diagnostic_test');
    }
    public function  schedular(){
        return $this->hasMany('App\Model\WorkingDatetimes');
    }
    public static function getSchedules($diagnostic){
        if(!$diagnostic->schedular()->get()->count())
                return $schedular=[];
        ## Finding sunday schedules
        $sunShedules = $diagnostic->schedular()->where('week_day',1)->get();
        $schedular[0]['week_day'] = 1;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[0]['timings'] = $timings;

        ## Finding moday schedules
        $sunShedules = $diagnostic->schedular()->where('week_day',2)->get();
        $schedular[1]['week_day'] = 2;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[1]['timings'] = $timings;

        ## Finding tusday schedules
        $sunShedules = $diagnostic->schedular()->where('week_day',3)->get();
        $schedular[2]['week_day'] = 3;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[2]['timings'] = $timings;

        ## Finding wednesday schedules
        $sunShedules = $diagnostic->schedular()->where('week_day',4)->get();
        $schedular[3]['week_day'] = 4;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[3]['timings'] = $timings;

        ## Finding thrusday schedules
        $sunShedules = $diagnostic->schedular()->where('week_day',5)->get();
        $schedular[4]['week_day'] = 5;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[4]['timings'] = $timings;

        ## Finding friday schedules
        $sunShedules = $diagnostic->schedular()->where('week_day',6)->get();
        $schedular[5]['week_day'] = 6;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[5]['timings'] = $timings;

        ## Finding saturday schedules
        $sunShedules = $diagnostic->schedular()->where('week_day',7)->get();
        $schedular[6]['week_day'] = 7;
        $timings = "";
        $i=1;
        foreach($sunShedules as $schedule){

           $timings .= $schedule['start_time']."-".$schedule['end_time'];
           if($i++ < count($sunShedules)){
               $timings .= ',';
           }
        }
        $schedular[6]['timings'] = $timings;
        return $schedular;
    }
    public function workingTimes(){
        return $this->hasMany('App\Model\WorkingDatetimes');
    }
    public function test()
    {
        return $this->hasMany('App\Model\DiagnosticTest');
    }
    public function galleries()
    {
        return $this->hasMany('App\Model\Gallery');
    }
}
