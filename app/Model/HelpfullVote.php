<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HelpfullVote extends Model
{
    protected $guarded = [];
	public $timestamps = false;
	public function review()
	{
		return $this->belongsTo('App\Model\Review');
	}
	public function getCreateVotingRules()
    {
        return array(
            'user_id'=>'required|exists:login,id',
            'review_id'=>'required|exists:reviews,id',
			'voting'=>'required'
        );
    }
	public function setVoteAttribute($value) {
		if ( empty($value) ) { 
			$this->attributes['vote'] = 0;
		} else {
			$this->attributes['vote'] = $value;
		}
	}
}
