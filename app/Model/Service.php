<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $guarded = [];
	public $timestamps = false;
    protected $table = 'service_type';
}
