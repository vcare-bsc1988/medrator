<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Diagnostic;
class DiagnosticCenterTest extends Model
{
    protected $fillable = ['diagnostic_test_id', 'diagnostic_center_id'];

    public function diagnostic()
    {
        return $this->belongsTo(Diagnostic::class);
    }
}
