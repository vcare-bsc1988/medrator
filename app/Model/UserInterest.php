<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserInterest extends Model
{
    public $timestamps = false;
	protected $guarded = [];
	public function user()
    {
        return $this->belongsTo('App\User');
    }
}
