<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Users extends Model
{
	protected $table = 'users';
	public $primaryKey = 'id';
	public $timestamps = false;
	protected $guarded = ['api_key'];
    public static function boot() {
        parent::boot();
        static::creating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = (is_null($value) ? '' : $value);
            }
        });
		static::saving(function($model){  
			foreach ($model->attributes as $key => $value) {
                $model->{$key} = (is_null($value) ? '' : $value);
            }
        });
    }
	public function user()
	{
        return $this->belongsTo('App\User');
    }
	public static function profileInfos($email = null, $userId=null)
	{
		$query = User::select('login.*','up.*','role_user.role_id as role','countries.name as country_name','cities.name as city_name')
			->join('role_user', 'login.id', '=', 'role_user.user_id')
			->join('users as up', 'login.id', '=', 'up.user_id')
			->leftJoin('countries', 'up.country', '=', 'countries.id')
			->leftJoin('cities', 'up.city', '=', 'cities.id');
		if($email !=""){
			$query = $query->where('email', $email);
		}
		if($userId != ""){
			$query = $query->where('user_id', $userId);
		}
		$user = $query->first();
		$user->device_type = $request->device_type;
		$user->device_token = $request->device_token;
		$user->save();
		
		$doctor =Doctor::select('doctors.*','countries.name as country_name','cities.name as city_name')
			->leftJoin('countries', 'doctors.country', '=', 'countries.id')
			->leftJoin('cities', 'doctors.city', '=', 'cities.id')
			->where('user_id',$user->user_id)->first();
		#Getting Qualification
		$qualifications = DB::table('doctor_qualifications')
			->join('qualifications', 'doctor_qualifications.qualification_id', '=', 'qualifications.id')
			->where('doctor_qualifications.doctor_id', '=', $doctor->id)
			->select('qualifications.id','qualifications.name','doctor_qualifications.college','doctor_qualifications.year_of_passing')
			->get();
		$doctor->qualifications = $qualifications;
		
		#Getting Specialties
		$specialties = DB::table('specialty_details')
			->join('specialties', 'specialty_details.speciality_id', '=', 'specialties.id')
			->where('specialty_details.doctor_id', '=', $doctor->id)
			->select('specialties.id','specialties.name')
			->get();
		$doctor->specialties = $specialties;
		
		#Getting Services
		$services = DB::table('service_details')
			->join('service_type', 'service_details.service_id', '=', 'service_type.id')
			->where('service_details.doctor_id', '=', $doctor->id)
			->select('service_type.id','service_type.name')
			->get();
		$doctor->services = $services;
		
		$has_doctor_profile=0;
		if(count($doctor)>0)
			$has_doctor_profile=1;
		
		$result = array(
			"basic_profile" =>$user,
			"has_associate_doctor_profile"=>$has_doctor_profile,
			"associate_doctor_profile"=>$doctor
		);
		return $result;
	}
	public static function basicProfile($userId=null)
	{
		$query = User::select('login.*','up.*','role_user.role_id as role')
			->join('role_user', 'login.id', '=', 'role_user.user_id')
			->join('users as up', 'login.id', '=', 'up.user_id');
		if($userId !=""){
			$query = $query->where('up.user_id', $userId);
		}
		$result = $query->first();
		return $result;
	}
}
