<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;
use App\User;
use Illuminate\Database\Eloquent\Model;
/**
 * Description of Role
 *
 * @author BALESHAWR
 */
class Role extends Model{
    protected $table = 'role_user';
    protected $fillable = [
        'user_id','role_id'
    ];
    public $timestamps = false;
    public function user()
    {
        return $this->belongsTo('User::class');
    }
}
