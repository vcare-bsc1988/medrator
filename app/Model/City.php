<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $guarded = [];
	public $timestamps = false;
    public function isAvail($name)
	{
		if(City::where('name', $name)->count() >0)
			return true;
		else
			return false;
	}
}
