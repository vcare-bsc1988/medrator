<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Clinics extends Model
{
    protected $guarded = [];
	public function getClinics($id)
	{
		return DB::table('doctor_associated')
			->join('clinics', 'doctor_associated.clinic_id', '=', 'clinics.id')
			->where('doctor_associated.doctor_id', '=', $id)
			->join('cities as ct','clinics.city','=','ct.id')
			->join('countries as c','clinics.country','=','c.id')
			->select('clinics.*','ct.name as city_name', 'c.name as country_name')
			->get();
	}
	public function updateClinics($id,$data)
	{
		Clinics::where('id',$id)
			->update($data);
		return $this->getClinicInfos($id);
	}
	public function getClinicInfos($clinicId=null)
	{
		$query= Clinics::join('cities as ct','clinics.city','=','ct.id')
			->join('countries as c','clinics.country','=','c.id');
		if($clinicId !=""){
			$query->where('clinics.id',$clinicId);
		}
		$query->select('clinics.*','ct.name as city_name', 'c.name as country_name');
		return $query->get();
			
	}
}
