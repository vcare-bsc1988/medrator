<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DiagnosticTest extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $table = 'diagnostic_test';
}
