<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'notifications';
	public static function boot() {
        parent::boot();
        static::creating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = is_null($value) ? "" : $value;
            }
			$model->created_at = \Carbon\Carbon::now();
			$model->created_at = \Carbon\Carbon::now();
        });
		static::saving(function($model){  
			$model->created_at = \Carbon\Carbon::now();
			$model->updated_at = \Carbon\Carbon::now();
        });
    }
}
