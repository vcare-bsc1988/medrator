<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
   protected $fillable = ['id','user_id','doctor_id','hospital_id','clinic_id','diagnostic_id','image_url'];
   protected $guarded = [];
   
   public function hospital(){
       return $this->belongsTo('App\Model\Doctor');
   }
   public function diagnostic(){
       return $this->belongsTo('App\Model\Diagnostic');
   }
}
