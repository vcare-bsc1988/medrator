<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $guarded = [];
    public static function boot() {
        parent::boot();
        static::creating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = (is_null($value) ? '' : $value);
            }
        });
    }
}
