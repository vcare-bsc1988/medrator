<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;
use Zizaco\Entrust\EntrustRole;
/**
 * Description of Role
 *
 * @author BALESHAWR
 */
class Role extends EntrustRole{
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
