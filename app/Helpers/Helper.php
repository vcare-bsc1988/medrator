<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author VCareAll
 */
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Model\Notification;
use App\Model\Doctor;
class Helper {
    public static function getNotifications($id=0,$type=null, $limit=3)
    {           
        /**
         * @Param type = read, unread, both
         * 
         */
        if($type =='read')
           $result = \App\Model\Notification::where('status',0)->where('receiver',$id)->orderBy('id', 'desc')->skip(0)->take($limit)->get();
        
        if($type =='unread')
           $result = \App\Model\Notification::where('status',0)->where('receiver',$id)->orderBy('id', 'desc')->skip(0)->take($limit)->get();
        
        if($type =='both')
           $result = \App\Model\Notification::where('receiver',$id)->orderBy('id', 'desc')->skip(0)->take($limit)->get();
        
        for($i=0; $i<count($result); $i++){
            $date = \Carbon\Carbon::createFromTimeStamp(strtotime($result[$i]->created_at))->diffForHumans();
            $result[$i]->notification_time= $date;
        }
        $response = [];
        if($result->count()){
            $response = $result;
        }
        return $response;
    }
    public static function getAllNotifications($id=0,$type=null)
    {           
        /**
         * @Param type = read, unread, both
         * 
         */
        if($type =='read')
           $result = \App\Model\Notification::where('status',0)->where('receiver',$id)->orderBy('id', 'desc')->get();
        
        if($type =='unread')
           $result = \App\Model\Notification::where('status',0)->where('receiver',$id)->orderBy('id', 'desc')->get();
        
        if($type =='both')
           $result = \App\Model\Notification::where('receiver',$id)->orderBy('id', 'desc')->get();
        
        for($i=0; $i<count($result); $i++){
            $date = \Carbon\Carbon::createFromTimeStamp(strtotime($result[$i]->created_at))->diffForHumans();
            $result[$i]->notification_time= $date;
        }
        $response = [];
        if($result->count()){
            $response = $result;
        }
        return $response;
    }
    /****
    * Getting user related interest blogs by Baleshwar
    */
    public function userInterestblogs(){
        $user = auth()->user();
        if($user){
            $tags =  \App\User::find($user->id)->interests()->get();
            if ($tags->count()>0) {
                    $tag_ids = array();
                    foreach($tags as $individual_tag) 
                            $tag_ids[] = $individual_tag->interest_id;           
                    $args=array(
                            'tag__in' => $tag_ids,
                            'posts_per_page'=>4, // Number of related posts to display.
                            'caller_get_posts'=>1
                    );
                    $posts = get_posts($args);  
            }else{
                    $args=array(
                            'posts_per_page'=>4, // Number of related posts to display.
                            'caller_get_posts'=>1
                    );
                    $posts = get_posts($args);  
            }

        }else{
            $args=array(
                'posts_per_page'=>4, // Number of related posts to display.
                'caller_get_posts'=>1
            );
            $posts = get_posts($args);  
        }       
        $i=0;
        foreach($posts as $post):
            $images = [];
            $mediaImages = get_attached_media( 'image', $post->ID);
            foreach($mediaImages as $image){
                            $images[]['img_url'] = $image->guid;
            }
            $posts[$i]->images= $images;
            $content = strip_tags($posts[$i]->post_content);
            $content= preg_replace('/[ ]{2,}|[\r\n]/', ' ', trim($content));
            $posts[$i]->post_content = $content;
            $i++;
        endforeach;
        return $posts;
    }
    public function solriumSort($query, $request,$latitude,$longitude){
        $helper = $query->getHelper();
        switch ($request->input('filters')['sorting']) {
            /**
             * 1-Start sorting by distance
             */
            case 'distance-asc':
                $query->addSort('{!func}' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                ),'asc');
                break;
            case 'distance-desc':
                $query->addSort('{!func}' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                ),'desc');
                break;
            /***
            * 2-Sort by Ratings
            */
            case 'ratings':
                $query->addSort('rating', 'desc');
                break;
            /**
            * 3-Sort by rating count
            */
            case 'ratings-count':
                $query->addSort('review_count', 'desc');
                break;
            /**
            * 4-Sort by Fee
            */
            case 'fee-asc':
                $query->addSort('consultation_fee', 'asc');
                break;
            case 'fee-desc':
                $query->addSort('consultation_fee', 'desc');
                break;
            /**
             * Sort by experience
             */
            case 'experience':
                $query->addSort('experience', 'desc');
                break;
            default:
                $query->addSort('{!func}' . $helper->geodist(
                    'latlon', 
                    doubleval($latitude), 
                    doubleval($longitude)
                ),'asc');
        }
        return $query;
    }
	public function removeResultByDistance($result, $distance){
		if(count($result)>0){
			$removedResult =[];
			foreach($result as $temp){
				if(round($temp['distance']) >= $distance){
					$removedResult[] = $temp;
				}
			}
			$result = $removedResult;
		}
		return $result;
	}
	public function sendAdminNotification($notification){
		$admins = User::whereHas('roles', function($q){
            $q->Where('name','administrator');
        })->get();
		if($admins->count()>0){
			$notificationArr = array();
			foreach($admins as $admin){
				$notificationArr[] = [
					'title' =>$notification['title'],
					'message' =>$notification['message'],
					'sender' => 0,
					'receiver' =>$admin->id
				];
			}
			Notification::insert($notificationArr);
		}
		return true;
		
	}
	public function sendModeratorNotification($notification){
		$moderators = User::whereHas('roles', function($q){
            $q->Where('name','moderator');
        })->get();
		if($moderators->count()>0){
			$notificationArr = array();
			foreach($moderators as $moderator){
				$notificationArr[] = [
					'title' =>$notification['title'],
					'message' =>$notification['message'],
					'sender' => 0,
					'receiver' =>$moderator->id
				];
			}
			Notification::insert($notificationArr);
		}
		return true;
	}
	public function sendDataEntryOperatorNotification($notification){
		$dataOperators = User::whereHas('roles', function($q){
            $q->Where('name','data-entry-user');
        })->get();
		if($dataOperators->count()>0){
			$notificationArr = array();
			foreach($dataOperators as $dataOperator){
				$notificationArr[] = [
					'title' =>$notification['title'],
					'message' =>$notification['message'],
					'sender' => 0,
					'receiver' =>$dataOperator->id
				];
			}
			Notification::insert($notificationArr);
		}
		return true;
	}
	public static function paginator($total,$per_page,$page){
		$current_url = url()->full();
		if (strpos($current_url, "?")){
			if(strpos($current_url, "?page=") && !strpos($current_url, "&")){
				$page_url= $current_url."?";
			}
			$pattern_match = "/[?&]page=(\d)+/i";
			if (preg_match($pattern_match, $current_url)) {
				$page_url= preg_replace($pattern_match,"", $current_url);
				$page_url=$current_url."&";
			}else{
				$page_url =$current_url."&";
			}
		}else{
			$page_url= $current_url."?";
		}
		$adjacents = "2"; 
		$page = ($page == 0 ? 1 : $page);  
		$start = ($page - 1) * $per_page;								
		$prev = $page - 1;							
		$next = $page + 1;
		$setLastpage = ceil($total/$per_page);
		$lpm1 = $setLastpage - 1;
		$setPaginate = "";
		if($setLastpage > 1)
		{	
			$setPaginate .= "<ul class='setPaginate'>";
			$setPaginate .= "<li class='setPage'>Page $page of $setLastpage</li>";
			if ($setLastpage < 7 + ($adjacents * 2))
			{	
				for ($counter = 1; $counter <= $setLastpage; $counter++)
				{
						if ($counter == $page)
								$setPaginate.= "<li><a class='current_page'>$counter</a></li>";
						else
								$setPaginate.= "<li><a href='{$page_url}page=$counter'>$counter</a></li>";					
				}
			}
			elseif($setLastpage > 5 + ($adjacents * 2))
			{
				if($page < 1 + ($adjacents * 2))		
				{
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
						{
								if ($counter == $page)
										$setPaginate.= "<li><a class='current_page'>$counter</a></li>";
								else
										$setPaginate.= "<li><a href='{$page_url}page=$counter'>$counter</a></li>";					
						}
						$setPaginate.= "<li class='dot'>...</li>";
						$setPaginate.= "<li><a href='{$page_url}page=$lpm1'>$lpm1</a></li>";
						$setPaginate.= "<li><a href='{$page_url}page=$setLastpage'>$setLastpage</a></li>";		
				}
				elseif($setLastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
						$setPaginate.= "<li><a href='{$page_url}page=1'>1</a></li>";
						$setPaginate.= "<li><a href='{$page_url}page=2'>2</a></li>";
						$setPaginate.= "<li class='dot'>...</li>";
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
						{
								if ($counter == $page)
										$setPaginate.= "<li><a class='current_page'>$counter</a></li>";
								else
										$setPaginate.= "<li><a href='{$page_url}page=$counter'>$counter</a></li>";					
						}
						$setPaginate.= "<li class='dot'>..</li>";
						$setPaginate.= "<li><a href='{$page_url}page=$lpm1'>$lpm1</a></li>";
						$setPaginate.= "<li><a href='{$page_url}page=$setLastpage'>$setLastpage</a></li>";		
				}
				else
				{
						$setPaginate.= "<li><a href='{$page_url}page=1'>1</a></li>";
						$setPaginate.= "<li><a href='{$page_url}page=2'>2</a></li>";
						$setPaginate.= "<li class='dot'>..</li>";
						for ($counter = $setLastpage - (2 + ($adjacents * 2)); $counter <= $setLastpage; $counter++)
						{
								if ($counter == $page)
										$setPaginate.= "<li><a class='current_page'>$counter</a></li>";
								else
										$setPaginate.= "<li><a href='{$page_url}page=$counter'>$counter</a></li>";					
						}
				}
			}

			if ($page < $counter - 1){ 
				$setPaginate.= "<li><a href='{$page_url}page=$next'>Next</a></li>";
				$setPaginate.= "<li><a href='{$page_url}page=$setLastpage'>Last</a></li>";
			}else{
				$setPaginate.= "<li><a class='current_page'>Next</a></li>";
				$setPaginate.= "<li><a class='current_page'>Last</a></li>";
			}

			$setPaginate.= "</ul>\n";		
		}
		return $setPaginate;
	}
	public static function widgetTimingsDoctor($doctor){
		$html ="";
		if($doctor->getSchedules($doctor)):
			foreach($doctor->getSchedules($doctor) as $schedule) :
				$class="other-schedule1";
				if($schedule['week_day']==1 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'" > <strong class="set-first">SUN</strong>&nbsp;&nbsp;'.strtoupper($schedule['timings']) .' </p>';
				elseif($schedule['week_day']==2 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">MON</strong>&nbsp;&nbsp;'. strtoupper($schedule['timings']) .' </p>';
				elseif($schedule['week_day']==3 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">TUE</strong>&nbsp;&nbsp;'. strtoupper($schedule['timings']) .' </p>';
				elseif($schedule['week_day']==4 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">WED</strong>&nbsp;&nbsp;'. strtoupper($schedule['timings']) .' </p>';
				elseif($schedule['week_day']==5 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">THU</strong>&nbsp;&nbsp;'. strtoupper($schedule['timings']) .' </p>';
				elseif($schedule['week_day']==6 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">FRI</strong>&nbsp;&nbsp;'. strtoupper($schedule['timings']) .' </p>';
				elseif($schedule['week_day']==7 && !empty($schedule['timings'])): 
					$html .= '<p class="schedule-times color-grey  '.$class.'"><strong class="set-first">SAT</strong>&nbsp;&nbsp;'. strtoupper($schedule['timings']) .' </p>';
				endif;
			endforeach;
			//$html .= '<button class="btn btn-primary btn-md pull-left transparent" id="default-schedule">show more</button>';
			//$html .= '<button class="btn btn-primary btn-md pull-left trns transparent" id="other-schedule">show less</button>';
		else :
			$html .= '<p class="schedule-times color-grey">Not Updated</p>';
		endif;
		return $html;
	}
	public static function widgetTimingsHospital($hospital){
		$html ="";
		if($hospital->getSchedules($hospital)):
			foreach($hospital->getSchedules($hospital) as $schedule) :
				$class="other-schedule1";
				if($schedule['week_day']==1 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'" > <strong class="set-first">SUN</strong>&nbsp;&nbsp;'.$schedule['timings'] .' </p>';
				elseif($schedule['week_day']==2 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">MON</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==3 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">TUE</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==4 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">WED</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==5 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">THU</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==6 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">FRI</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==7 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey  '.$class.'"><strong class="set-first">SAT</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				endif;
			endforeach;
			//$html .= '<button class="btn btn-primary btn-md pull-left transparent" id="default-schedule">show more</button>';
			//$html .= '<button class="btn btn-primary btn-md pull-left trns transparent" id="other-schedule">show less</button>';
		else:
			$html .= '<p class="schedule-times color-grey">Not Updated</p>';
		endif;
		return $html;
	}
	public static function widgetTimingsDiagnostic($diagnostic){
		$html ="";
		if($diagnostic->getSchedules($diagnostic)):
			foreach($doctor->getSchedules($diagnostic) as $schedule) :
				$class="other-schedule";
				if($schedule['week_day']==1 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'" > <strong class="set-first">SUN</strong>&nbsp;&nbsp;'.$schedule['timings'] .' </p>';
				elseif($schedule['week_day']==2 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">MON</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==3 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">TUE</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==4 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">WED</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==5 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">THU</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==6 && !empty($schedule['timings'])):
					$html .= '<p class="schedule-times color-grey '.$class.'"><strong class="set-first">FRI</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				elseif($schedule['week_day']==7 && !empty($schedule['timings'])) :
					$html .= '<p class="schedule-times color-grey  '.$class.'"><strong class="set-first">SAT</strong>&nbsp;&nbsp;'. $schedule['timings'] .' </p>';
				endif;
			endforeach;
			//$html .= '<button class="btn btn-primary btn-md pull-left transparent" id="default-schedule">show more</button>';
			//$html .= '<button class="btn btn-primary btn-md pull-left trns transparent" id="other-schedule">show less</button>';
		else:
			$html .= '<p class="schedule-times color-grey">Not Updated</p>';
		endif;
		return $html;
	}
}
