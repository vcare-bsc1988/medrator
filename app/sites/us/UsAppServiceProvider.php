<?php

namespace App\Sites\US;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class UsAppServiceProvider extends ServiceProvider
{
    protected $defer = false;
    public function boot(Router $router)
    {
        $site_name = strtoupper(getenv('SITE_CODE_US'));
        $namespace = "\\App\\Sites\\{$site_name}\\Controllers";
        $site_path = app_path(). '/sites/' .strtolower($site_name);
        $this->loadViewsFrom(
            $site_path. '/Views', strtolower($site_name)
        );

        $this->mergeConfigFrom(
            $site_path. '/config.php', strtolower($site_name)
        );
        $this->loadTranslationsFrom(
            $site_path.'/Translations', strtolower($site_name)
        );
        $this->publishes([
            $site_path.'/assets' => public_path('vendor/us'),
        ], 'public');

        if (! $this->app->routesAreCached()) {
            require $site_path.'/routes.php';
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       //
    }
}
