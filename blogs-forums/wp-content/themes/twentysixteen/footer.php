<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<link rel="stylesheet" type="text/css" href="http://139.59.24.155/blogs-forums/wp-content/themes/bp-default/fontawesome/css/fw.css">

		</div><!-- .site-content -->

<footer id="colophon" class="site-footer footer p82-topbot" role="contentinfo">
			
<div class="container">
<div class="row">
	<div class="col-sm-6 col-md-3 col-lg-3 all-need">
		<h2>Address</h2>
		<div class="faddress">
		   Medrator<br>Sector 31, Gurugram, Haryana (India)<br>
		   <span>General Email:</span> <a href="mailto:contactus@medrator.com">contactus@medrator.com</a><br>
		   <span>Technical Issues :</span> <a href="mailto:administrator@medrator.com">administrator@medrator.com</a>
		</div>
	</div><!-- col-sm-3 -->
	<div class="col-sm-6 col-md-3 col-lg-3 all-need list-styles">
		<?php $city_name= json_decode(stripslashes($_COOKIE['address']))->city_name;
			$url1 = "http://139.59.24.155/find/doctor/".$city_name."/Gastroenterologist";
			$url2 = "http://139.59.24.155/find/doctor/".$city_name."/Physiotherapist";
			$url3 = "http://139.59.24.155/find/doctor/".$city_name."/Infertility";
			$url4 = "http://139.59.24.155/find/doctor/".$city_name."/Dermatologist";
			$url5 = "http://139.59.24.155/find/doctor/".$city_name."/Homeopath";
			$url6 = "http://139.59.24.155/find/doctor/".$city_name."/Dentist";
			$url7 = "http://139.59.24.155/find/doctor/".$city_name."/Cardiologist";
		?>
		<h2>Service</h2>
		<ul>
			<li><a href="<?php echo $url1?>">Cardiology</a></li>
			<li><a href="<?php echo $url2?>">Neurology</a></li>
			<li><a href="<?php echo $url3?>">Cardiac Clinic</a></li>
			<li><a href="<?php echo $url4?>">Laboratory Analysis</a></li>
			<li><a href="<?php echo $url5?>">Dental Clinic</a></li>
			<li><a href="<?php echo $url6?>">Gynecological Clinic</a></li>
			<li><a href="<?php echo $url7?>">Psychological Counseling</a></li>
		</ul>
	</div>
	<div class="col-sm-6 col-md-3 col-lg-3 all-need list-styles">
		<h2>Medrator</h2>
		<ul>
			<li><a href="http://139.59.24.155/about-us" target="new">About Us</a></li>
			<li><a href="http://139.59.24.155/contact-us" target="new">Contact Us</a></li>
			<li><a href="http://139.59.24.155/copyright" target="new">Copyright</a></li>
			<li><a href="http://139.59.24.155/privacy-policy" target="new">Privacy Policy</a></li>
			<li><a href="http://139.59.24.155/terms-of-service" target="new">Terms of Service</a></li>
			<li><a href="http://139.59.24.155/organ-donation" target="new">Organ donation with third part link</a></li>
			<li><a href="http://139.59.24.155/medical-tourism" target="new">Medical Tourism Section</a></li>
			<li><a href="http://139.59.24.155/buy-insurance" target="new">Buy Insurance / Fitness product</a></li>	
		</ul>
	</div>
	<div class="col-sm-6 col-md-3 col-lg-3 all-need list-styles">
		<h2>news letter</h2>
		<div class="news-letter">
			<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
			<style type="text/css">
				#mc_embed_signup{background:transparent; clear:left; font:14px Helvetica,Arial,sans-serif; }
				#mc-embedded-subscribe{position: absolute !important;float: none !important;right: 0 !important;top: 0 !important;height: 44px !important;background:#333 !important;width:auto !important;margin:0px !important;}
				#mce-responses{    margin: 0px !important;
				padding: 0px !important;}
				/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
				   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
			</style>
			<div id="mc_embed_signup">
			<form action="//medrator.us15.list-manage.com/subscribe/post?u=9fe5a7aa817162a89ceba0d82&amp;id=7dd23c553d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" style="padding:0px !important;" class="validate" target="_blank" novalidate>
				<div id="mc_embed_signup_scroll">
				
					<!--<div class="mc-field-group">
						<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
					</label>-->
						<input type="email" value="" name="EMAIL" class="required email" placeholder="Enter Your Email" id="mce-EMAIL">
					</div>
					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9fe5a7aa817162a89ceba0d82_7dd23c553d" tabindex="-1" value=""></div>
					<div class="clear"><input type="submit" value="Go" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none;color:white;"></div>
					</div> 
				</div>
			</form>
			</div>
			<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
			<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);setTimeout(function () {$('.response').hide();},4000);</script>
			<!--End mc_embed_signup-->
		</div><!-- /.new-letter -->
	</div><!-- col-sm-3 -->
</div><!-- ./row -->
</div><!-- /.container --> 
	   
</footer><!-- .site-footer -->
<div class="copyrights text-left">
	<div class="container">
		<span class="pull-left white">Medrator © 2016 | All Rights Reserved</span>
		<ul class="nav navbar-nav pull-right-large social-icons">
			<li class="pull-left-small"><a href="javascript:;" class="padding-0 white"><i class="fa fa-twitter" style="font-family: 'FontAwesome';width:15px;"></i></a></li>
			<li class="pull-left-small"><a href="javascript:;" class="padding-0 white"><i class="fa fa-facebook" style="font-family: 'FontAwesome';width:15px;"></i></a></li>
			<li class="pull-left-small"><a href="javascript:;" class="padding-0 white"><i class="fa fa-linkedin" style="font-family: 'FontAwesome';width:15px;"></i></a></li>
		</ul>
	</div>
</div><!-- /.copy-rights -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	$('#mc-embedded-subscribe').click(function(){
		$.ajax({
			type: "POST",
			url: "http://139.59.24.155/news-letter/subscriber",
			data: $('#newsletter-form').serialize(),
			success: function(result){ 
				console.log(result);
				$("#success-msg").html(result.message);
				$('#success-msg').show();
				setTimeout(function () {$('#success-msg').hide();},4000);
			},
			error:function(result){
				console.log(result);
			}
		});
	})
});
</script>
</body>
</html>
