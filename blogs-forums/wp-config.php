<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'medrator-v1.0');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z)u#wGTMYBxd$RR);u-7Z.mY=!{=fUbBn#_MPT`#EH#T9w802iNtva<nHLNRkxzw');
define('SECURE_AUTH_KEY',  '8Lcg4nr~m9Cy#wx![y})q,?*^|Y/mNr@cWFTs%zmaqw81xy^OH;0:)n@3lxyuIL&');
define('LOGGED_IN_KEY',    'o`DV9iL[/^sL<RTBy5q4~N1/M#?wKC3Y,oATG$vxhoawx8aw yB}e$UJ.ei+J{|I');
define('NONCE_KEY',        '!;:_iQ4i57t=`-Ar*L)ZQy6IlD8;J4p^rw}Fr8/9]n,$6X-xND E|`>pbmXOmG(N');
define('AUTH_SALT',        '2 v8P?dsAnsp@ y)5Fjcum<zrS#I)4F82i|_;-LiEuXau^#nMt9,[]p`518yurfT');
define('SECURE_AUTH_SALT', 'w%bmXU?KwX}zb7xj7Alkb34B MlpH$DxbbTth8zd?g#XI5<^ad{}X?&B5<Jln7cy');
define('LOGGED_IN_SALT',   '?J:fV|Fa;Nq/PV1VCZ)H_z0{V2T<8n<IT_!]?QNe_MaHR.ep>xIn41?BEt+ctER.');
define('NONCE_SALT',       'Dx DbB| .QAE+%gw;6 jwtP?11+pBKB =^h5$>O#{]+q]f#e(x3bQ&v8 V.!GqKd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'blogs_forums_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**
 * Author @Baleshwar Chauhan.
 * Custome global variable
 */
define('MEDRATOR_URL', 'http://139.59.24.155');