<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Bala;

/**
 * Description of Notification
 *
 * @author VCareAll
 */
class Notification {
    
    function __construct($device_id,$gcm_api_key,$device_type) {
       $this->dvice_id = $device_id;
       $this->gcm_api_key = $gcm_api_key;
       $this->device_type = $device_type;
    }
    public static function send($message)
    {
        if($this->device_type == 'android'){
                $this->androidPushNotification($message);
        }
        if($this->device_type == 'iphone'){
                $this->iPhonePushNotification($message);
        }
    }
    public static function iPhonePushNotification($message)
    {				
            $ch = curl_init('http://heartover.com/callhai/apn.php');		

            ## Put your device token here (without spaces):
            $deviceToken = $this->dvice_id;
            ### Put your private key's passphrase here:
            $passphrase = 'vcareall12345';
            echo 1; exit;
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'Cer.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

            ## Open a connection to the APNS server
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err,
                    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

            ## Sever connection
            echo 'Connected to APNS' . PHP_EOL; exit;

            $body['aps'] = array(
                    'alert' => $message,
                    'sound' => 'default',	 
            );

            ##Encode the payload as JSON
            $payload = json_encode($body);

            ## Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

            ## Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));

            if (!$result)
            echo 'Message not delivered' . PHP_EOL; exit;

            ## Close the connection to the server
            fclose($fp);

            return true;
    }
    public static function androidPushNotification($message)
    {
        $reg_id =  $this->dvice_id;
        $registrationIds = array($reg_id);
        $fields = array
        (
                'registration_ids' 	=> $registrationIds,
                'data'			=> $message
        );
        $headers = array
        (
                'Authorization: key='.$this->gcm_api_key,
                'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        if ($result === FALSE) {
            curl_close($ch);
            return false;
        }
        else
        {
            curl_close($ch);
            return true;
        }
    }	
}
