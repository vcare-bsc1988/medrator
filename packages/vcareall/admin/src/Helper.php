<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin;

/**
 * Description of Helper
 *
 * @author VCareAll
 */
use App\User;
use App\Model\Notification;
class Helper {
    function __construct($device_id,$gcm_api_key,$device_type) {
       $this->dvice_id = $device_id;
       $this->gcm_api_key = $gcm_api_key;
       $this->device_type = $device_type;
    }
    public static function findRole($userId){
        $role =0;
        if($user = User::find($userId)){
            $role = $user->role;
        }
        return $role;
    } 
    public static function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
          return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
          } else {
            return $miles;
          }
    }
    public function send($message)
    {
        if($this->device_type == 1){
			return $this->androidPushNotification($message);
        }
        if($this->device_type == 2){
			return $this->iPhonePushNotification($message);
        }
    }
    protected function iPhonePushNotification($message)
    {							
			$data_string = "message=".$message['message']."&dvice_id=".$this->dvice_id;
			$ch = curl_init('http://heartover.com/callhai/apn_medrator.php');
		
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");			
		
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                     		
			$result = curl_exec($ch);
			/*		
            ## Put your device token here (without spaces):
            $deviceToken = $this->dvice_id;
            ### Put your private key's passphrase here:
            $passphrase = 'medrator123456';
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'Cer.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

            ## Open a connection to the APNS server
            $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err,
                    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL); exit;

            ## Sever connection
            echo 'Connected to APNS' . PHP_EOL; exit;

            $body['aps'] = array(
				'alert' => $message,
				'sound' => 'default',	 
            );

            ##Encode the payload as JSON
            $payload = json_encode($body);

            ## Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

            ## Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));
			print_r($result); exit;
            if (!$result)
            echo 'Message not delivered' . PHP_EOL; exit;

            ## Close the connection to the server
            fclose($fp);*/
            return true;
    }
    protected function androidPushNotification($message)
    {
        $reg_id =  $this->dvice_id;
        $registrationIds = array($reg_id);
        $fields = array
        (
                'registration_ids' 	=> $registrationIds,
                'data'			=> $message
        );
        $headers = array
        (
                'Authorization: key='.$this->gcm_api_key,
                'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        if ($result === FALSE) {
            curl_close($ch);
            return false;
        }
        else
        {
            curl_close($ch);
            return true;
        }
    }
	public static function getLatLong($address) {
		$array = array();
		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');

		// We convert the JSON to an array
		$geo = json_decode($geo, true);
		//echo "<pre>";
		//print_r($geo['results'][0]); exit;
		//echo "</pre>";
		// If everything is cool
		if ($geo['status'] = 'OK') {
		  $latitude = @$geo['results'][0]['geometry']['location']['lat'];
		  $longitude = @$geo['results'][0]['geometry']['location']['lng'];	  
		  ##### Finding locality
		  $locality = @$geo['results'][0]['address_components'][0]['long_name'];
		  $country = @$geo['results'][0]['address_components'][3]['long_name'];
		  $satte = @$geo['results'][0]['address_components'][2]['long_name'];
		  $city =	@$geo['results'][0]['address_components'][1]['long_name'];
		  $formatted_address = @$geo['results'][0]['formatted_address'];
		  $array = array(
			'lat'=> $latitude ,
			'lng'=>$longitude,
			'locality'=>$locality,
            'address_one'=>$formatted_address,
			'country'=>$country,
			'state'=>$satte,
			'city'=>$city	  
		  );
		}
		return $array;
	}
	public static function getIP(){
		return $ip = getenv('HTTP_CLIENT_IP')?:
		getenv('HTTP_X_FORWARDED_FOR')?:
		getenv('HTTP_X_FORWARDED')?:
		getenv('HTTP_FORWARDED_FOR')?:
		getenv('HTTP_FORWARDED')?:
		getenv('REMOTE_ADDR');
	}
	public static function getAddress($ip){		
		$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
		$result=[
			'address_one' =>htmlentities($geo["geoplugin_city"]),
			'locality' =>htmlentities($geo["geoplugin_city"]),
			'state_name' =>htmlentities($geo["geoplugin_regionName"]),
			'city_name' =>htmlentities($geo["geoplugin_city"]),
			'country_name' =>htmlentities($geo["geoplugin_countryName"]),
			'latitude' =>htmlentities($geo["geoplugin_latitude"]),
			'longitude' =>htmlentities($geo["geoplugin_longitude"]),
			'pincode' =>htmlentities($geo["geoplugin_regionCode"])
		];
		return $result;
	}
	protected static function my_ofset($text){
		preg_match('/^\D*(?=\d)/', $text, $m);
		return isset($m[0]) ? strlen($m[0]) : false;
	}
	protected static function getTiming($s){
		$days=["SU"=>1,"MO"=>2,"TU"=>3,"WE"=>4,"TH"=>5,"FR"=>6,"SA"=>7];
		$offset=self::my_ofset($s);

		$d=substr($s,0,$offset);
		$d_range=explode(",",$d);

		$t=substr($s,$offset);
		$t_range=explode(",",$t);
		$from=$to="00:00";
		$duration_int=[];
		foreach($t_range as $tr){
			$timing=[];
			$tt=trim($tr);
			/**
			 * For 12 Hrs formate with AM or PM @reg_match("/(\d{1,2}:\d{1,2}\s*(?:AM|PM))-(\d{1,2}:\d{1,2}\s*(?:AM|PM))/",$tt,$time_match);
			 */
			preg_match("/(\d{2}:\d{2})-(\d{2}:\d{2})/",$tt,$time_match);
			$timing['from']=$time_match[1];
			$timing['to']=$time_match[2];
			$duration_int[]=$timing;
		}
		$res=[];
		foreach($d_range as $dr){
			$timera=[];
				$dt=trim($dr);
			if(strpos($dt,'-')){
				$dy=explode('-',$dt);
				$start=$dy[0];
				$end=$dy[1];
				$start_index=$days["$start"];
				$end_index=$days["$end"];
				
				if($start_index>$end_index){
					$end_index=$end_index+7;
				}
				for($i=$start_index;$i<=$end_index;$i=$i+1){
					if($i>7){
						$timera['day']=$i-7;
					}else{
						$timera['day']=$i;
					}
					$timera['durations']=$duration_int;
					$res[]=$timera;
				}
			}else{
				$timera['day']=$days["$dt"];
					$timera['durations']=$duration_int;
					$res[]=$timera;
			}	
		}
		return $res; 
	}
	public static function workingTimeFormate($s, $hospital_id,$doctor_id =0,$diagnostic_id =0){
		/**
		 *
		 * Formate of input datetine string
		 *
		 * @$s=["TUE, THU, SAT 4:00 PM-8:00 AM"];
		 * @$s=["MON-WED, THU, SAT 4:00 PM-8:00 AM"];
		 * @$s=["MON-WED, THU, SAT 4:00 PM-8:00 AM 8:00 PM-9:00 AM"];
		 */
		$final_timing=[];
		foreach($s as $t){
			$final_timing[]=self::getTiming($t);
		}
		foreach($final_timing as $formatedDateTime){
			foreach($formatedDateTime as $daytimes){
				foreach($daytimes['durations'] as $times){
					$schedule[] = [
						'week_day'=>$daytimes['day'],
						'start_time'=>date('g:i A', strtotime($times['from'])),
						'end_time'=>date('g:i A', strtotime($times['to'])),
						'hospital_id'=>$hospital_id,
						'doctor_id'=>$doctor_id,
						'diagnostic_id'=>$diagnostic_id
					];
				}
			}
		}
		return $schedule;
    }
	public static function removeDuplicateArr($arrayList){
        $uniqueArray =[];
        $ids = [];
        foreach($arrayList as $temp){
            if(!in_array($temp['id'],$ids)){
                $uniqueArray[] = $temp;
                $ids[]=$temp['id'];
            }
        }
        return $uniqueArray;
    }
	public static function removeDuplicateArray($arrayList){
        $uniqueArray =[];
        $ids = [];
        foreach($arrayList as $temp){
            if(!in_array($temp['entity_id'],$ids)){
                $uniqueArray[] = $temp;
                $ids[]=$temp['entity_id'];
            }
        }
        return $uniqueArray;
    }
}
