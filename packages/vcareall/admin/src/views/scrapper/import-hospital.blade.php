@extends('admin::template')

@section('admin::content')
    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <div id="main-success-alert" class="alert alert-success fade in alert-dismissable" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <span id="main-success-message"></span>
            </div>
            <div id="main-failure-alert" class="alert alert-success fade in alert-dismissable" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <span id="main-failure-message"></span>
            </div>
            <!-- Begin page heading -->
            <h1 class="page-heading">Import Hospital Data
            </h1>
            <!-- End page heading -->

            <div class="the-box">
                <div class="table-responsive">
                    @if (session('message'))
					<div id="alert" class="alert alert-success fade in alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
						<span id="failure-message">{{ session('message') }}</span>
					</div>
                    @endif
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
                    <!--<a href="{{ URL::to('admin/download-excel/xls') }}"><button class="btn btn-success">Download Excel xls</button></a>
                    <a href="{{ URL::to('admin/download-excel/xlsx') }}"><button class="btn btn-success">Download Excel xlsx</button></a>
                    <a href="{{ URL::to('admin-download-excel/csv') }}"><button class="btn btn-success">Download CSV</button></a> -->
                    <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ url('/admin/import-excel/hospital') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="file" name="import_file" /><br>
                        <button class="btn btn-primary">Import File</button>
                    </form>
                </div><!-- /.table-responsive -->
            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
        </div><!-- /.container-fluid -->
    </div><!-- /.page-content -->
@endsection
