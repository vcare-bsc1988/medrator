<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Medrator - Search Doctor, Hospital, Diagnostic">
    <meta name="author" content="Medrator">
    <meta name="keyword" content="Doctor, Hospital, Diagnostic">
    <link rel="shortcut icon" href="{{ asset("public/admin/img/favicon.png") }}">

    <title>Medrator - Search Doctor, Hospital, Diagnostic</title>

    <!-- Bootstrap CSS -->    
    <link href="{{ asset("public/admin/css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{{ asset("public/admin/css/bootstrap-theme.css") }}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{{ asset("public/admin/css/elegant-icons-style.css") }}" rel="stylesheet" />
    <link href="{{ asset("public/admin/css/font-awesome.css") }}" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="{{ asset("public/admin/css/style.css") }}" rel="stylesheet">
    <link href="{{ asset("public/admin/css/style-responsive.css") }}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="{{ asset("public/admin/js/html5shiv.js") }}"></script>
    <script src="{{ asset("public/admin/js/respond.min.js") }}"></script>
    <![endif]-->
</head>

  <body class="login-img3-body">

    <div class="container">
        <form class="login-form" action="{{ url('admin/login')}}" method="post">   
        {{ csrf_field() }}
        <div class="login-wrap">
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            <div class="input-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <span class="input-group-addon"><i class="icon_profile"></i></span>
                <input type="text" name="username" class="form-control" placeholder="Username" autofocus>
                @if ($errors->has('username'))
                  <span class="help-block">
                      <strong>{{ $errors->first('username') }}</strong>
                  </span>
                @endif
            </div>
            <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" name="password" class="form-control" placeholder="Password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                {{-- <span class="pull-right"> <a href="#"> Forgot Password?</a></span> --}}
            </label>
			@if(Session::has('failed'))
			<div class="input-group">
                <div class="alert alert-block alert-danger fade in">
				  <button data-dismiss="alert" class="close close-sm" type="button">
					  <i class="icon-remove"></i>
				  </button>
				  <strong>Oh snap!</strong> {{ session("failed") }}
			  </div>
            </div>
			@endif
            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
        </div>
      </form>

    </div>


  </body>
</html>
