@extends('admin::template')

@section('admin::content')
<style type="text/css">
.pac-container{z-index:1100000;}
</style>
<link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css'/>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Pending Approval</h3>
            <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                    <li><a href="{{ url('admin/pending-approval') }}"><i class="fa fa-bars"></i>Pending Approval</a></li>
            </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <!-- profile-widget -->
    <div class="col-lg-12">
        <section class="panel">
        <div class="profile-widget" style="background:white;color:#000;">
          <div class="panel-body">
            <div class="col-lg-2 col-sm-2">
              <!-- <h4>Jenifer Smith</h4>   -->  
              <style>
                  .follow-ava{width:82px;height:82px;overflow:hidden;position:relative;}
                  .btn-hides{position: absolute;left: 0;right: 0; bottom: -51px;}
                  .follow-ava:hover .btn-hides{bottom:0px;transition-duration: 1s;}
                  
              </style>
              <div class="follow-ava">
                  @if(!empty($doctor->image))
                  <img src="{{ $doctor->image }}" width="100">
                  @else
                     <img src="{{ asset("public/admin/img/user-placeholder.png") }}" width="100">
                  @endif
              </div>
              <!-- <h6>Administrator</h6> -->
            </div>
            <div class="col-lg-6 col-sm-6 follow-info">
                <h3 class="margin-0">{{ $doctor->name}} </h3>
                <p class="margin-0">
                        {{ $doctor->address_one}}
                </p>
                <p class="margin-0">
                        <label><b>Consulting Fee </b></label> {{ $doctor->consultation_fee}}
                </p>
                @for ($i=1; $i <= 5 ; $i++)
                    <span class="glyphicon glyphicon-star{{ ($i <= $doctor->rating_cache) ? '' : '-empty'}}"></span>
                @endfor
                {{ $doctor->rating_count}} Reviews <br>
				@if($doctor->status==0)<span class="label label-danger">Pending</span>@elseif($doctor->status==1)<span class="label label-success">Approved</span> @elseif($doctor->status==2) <span class="label label-danger">Declined</span>@endif
            </div>
            <div class="col-lg-2 col-sm-4 follow-info text-right pull-right">
                {!! Form::open(['url' => 'admin/approve-doctor','method' => 'post','files' => true, 'id'=>'delete-specialty-form','onsubmit'=>'return confirm("Are you sure ?")']) !!}
                {{ Form::hidden('id', $doctor->id) }}
                {{ Form::submit('Approve',['class'=>'btn btn-info  btn-sm']) }}
                {!! Form::close() !!}
            </div>
			<div class="col-lg-2 col-sm-4 follow-info text-right pull-right">
                {!! Form::open(['url' => 'admin/decline-doctor','method' => 'post','files' => true, 'id'=>'delete-specialty-form','onsubmit'=>'return confirm("Are you sure ?")']) !!}
                {{ Form::hidden('id', $doctor->id) }}
                {{ Form::submit('Declined',['class'=>'btn btn-danger  btn-sm']) }}
                {!! Form::close() !!}
            </div>
                                <div class="row" style="float: left;width: 100%;padding: 0px;">
                                <div class="col-xs-12">
                                        <hr>
                                        <div class="col-lg-4 col-sm-4 follow-info">
                                                <h3>Speciality</h3>
                                                <hr>
                                                <ul class="nav nav-stacked custome-speciality">
                                                    @foreach($doctor->specialties as $specialty) 
                                                    <li>
                                                        <span>{{ $specialty->specialty->name }}</span>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                        </div>
                                        <p>
                                                </p><div class="col-lg-4 col-sm-4 follow-info">
                                                        <h3>Services</h3>
                                                        <hr>
                                                        <ul class="nav nav-stacked custome-speciality">
                                                            @foreach($doctor->services as $service)    
                                                            <li>
                                                                <span>{{ $service->service->name }}</span>
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                </div>
                                        <p></p>
                                </div>
                                </div>
        </div>
        </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
       <section class="panel">
             <header class="panel-heading tab-bg-info">
                 <ul class="nav nav-tabs my-tabs-01">
                     <li class="active">
                         <a data-toggle="tab" href="#recent-activity">
                             Review & Rating
                         </a>
                     </li>
                     <li class="">
                         <a data-toggle="tab" href="#profile">
                             Associated Hospitals
                         </a>
                     </li>
                 </ul>
             </header>
             <div class="panel-body">
                 <div class="tab-content">
                     <div id="recent-activity" class="tab-pane active">
                         <div class="profile-activity">                                          
                             @if($doctor->reviews()->get()->count()>0)
                             @foreach($doctor->reviews as $review)
                             <div class="act-time">                                      
                                 <div class="activity-body act-in">
                                     <span class="arrow"></span>
									 <div class="col-xs-12 padding-0">
										<div class="col-md-1" style="padding:0px;">
											<a href="javascript::void(0)" class="activity-img activity-img1">
												@if($review->user->image)
												 <img class="avatar" width="70" src="{{ $review->user->image }}">
												 @else
												 <img class="avatar" width="70" src="{{ asset('public/admin/img/user-placeholder.png') }}">
												 @endif
											</a>
										</div>
										<div class="col-md-10">
											<p class="attribution act-in" style="margin-bottom:0px;"><a href="javascript::void(0)">{{ $review->user->user->name }}</a></p>
											<p style="margin-bottom:0px;">{{ $review->user->email }}</p>
										</div>
										<div class="clearfix" style="margin-bottom:10px;"></div>
										<p>@for ($i=1; $i <= 5 ; $i++)
                                            <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                                            @endfor
										</p>
										<b><strong>{{ $review->title }}</strong></b><br>
										<span>{{ $review->comment }}</span>
									</div>
                                 </div>
                             </div>
                             @endforeach
                             @else
                             <p>No review was found.</p>
                             @endif
                         </div>
                     </div>
                     <!-- profile -->
                     <div id="profile" class="tab-pane">
                       <section class="panel">
                        @if($doctor->hospitals()->get()->count()>0)
                        @foreach($doctor->hospitals as $hospital)
                        <div class="act-time">                                      
                            <div class="activity-body act-in">
                                <span class="arrow"></span>
								 <div class="col-xs-12 padding-0">
									<div class="col-md-1" style="padding:0px;">
										<a href="javascript::void(0)" class="activity-img activity-img1">
											@if($hospital->image)
											<img class="avatar" width="70" src="{{ $hospital->image }}">
											@else
											<img class="avatar" width="70" src="{{ asset('public/admin/img/user-placeholder.png') }}">
											@endif
										</a>
									</div>
									<div class="col-md-10">
										<p class="attribution act-in" style="margin-bottom:0px;"><a href="javascript::void(0)">{{ $hospital->name }}</a></p>
										<p style="margin-bottom:0px;">{{ $hospital->address_one }}</p>
										<p class="margin-0">
												<label><b>Consulting Fee </b></label> {{ $hospital->consultation_fee}}
										</p>
									</div>
									<div class="clearfix" style="margin-bottom:10px;"></div>
									<p> 
										@for ($i=1; $i <= 5 ; $i++)
                                        <span class="glyphicon glyphicon-star{{ ($i <= $hospital->rating_cache) ? '' : '-empty'}}"></span>
										@endfor
										{{ $hospital->rating_count }} Reviews
									</p>
								</div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <p>No hospital was found.</p>
                        @endif
                       </section>
                         <section>
                             <div class="row">                                              
                             </div>
                         </section>
                     </div>
                 </div>
             </div>
         </section>
    </div>
 </div>
@endsection
