@extends('admin::template')

@section('admin::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style>form label{float:right;}</style>
    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <div id="main-success-alert" class="alert alert-success fade in alert-dismissable" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <span id="main-success-message"></span>
            </div>
            <div id="main-failure-alert" class="alert alert-success fade in alert-dismissable" style="display: none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <span id="main-failure-message"></span>
            </div>
            <!-- Begin page heading -->
            <h1 class="page-heading">Send Notifications</h1>
            <!-- End page heading -->
			<div class="the-box">
                <div class="table-responsive">
                    @include('common.errors')
					@if (session('message'))
					<div class="alert alert-success">
						<strong>Success!</strong> {{ session('message') }}
					</div>
					@endif
					{{ Form::open(['url' => 'admin/user/send-notifications','methos'=>'post', 'style'=>'border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;']) }}
                        {{csrf_field()}}
                        <div class="form-group col-lg-12">
                            Seletc User:
                            <div class="clear" id="content2">
                                {{ Form::select('ids[]',$users, null,['class'=>'1col active form-control','multiple'=>true]) }}
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                                Title : {{ Form::text('title', null,['placeholder' => 'Enter Title','class'=>'form-control']) }}<br>
                        </div>
                        <div class="form-group col-lg-12">
                                Message : {{ Form::textarea ('message', null,['placeholder' => 'Enter Title','class'=>'form-control']) }}
                        </div>
                        <div class="form-group col-lg-12">
                                {{ Form::submit('Send') }}
                        </div>
                        {{ Form::close() }}
                </div><!-- /.table-responsive -->
            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
        </div><!-- /.container-fluid -->
    </div><!-- /.page-content -->
<script type="text/javascript" src="{{  asset('public/js/skin.js') }}"></script>
<script type="text/javascript" src="{{  asset('public/js/index.js') }}"></script>
@endsection
