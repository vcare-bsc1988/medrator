@extends('admin::template')

@section('admin::content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa fa-bars"></i> Pending Approval</h3>
        <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                <li><i class="fa fa-bars"></i>Pending Approval</li>
        </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <div class="col-sm-12">
        <section class="panel">
			{!! Form::open(['url' => 'admin/approve-all','method' => 'post']) !!}
            <header class="panel-heading no-border">
               <div class="col-sm-6"> <span>Pending Approval </span> </div>
				<div class="col-sm-6 text-right"><span><label class="col-sm-4 col-sm-offset-4">Action: </label><div class="col-sm-4">{{ Form::select('action', [1 => 'Approve', 2 => 'Decline'],null,['class'=>'']) }} {{ Form::submit('Apply',['class'=>'btn btn-danger  btn-sm']) }}</div></span> </div>
            </header>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Sr. No <input type="checkbox" id="selectall"/></th>
					<th>Image</th>
                    <th>Name</th>
					<th>Contact</th>
					<th>Register Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                <?php $i=1; ?>
                @foreach($results as $result)
                <tr>
                    <td>{{ $i++ }} <input type="checkbox" class="case" name="ids[]" value="{{ $result->id }}"/></td>
					<td><img src="{{ $result->image?$result->image:asset('public/admin/img/user-placeholder.png') }}" style="width:50px;height:70px;" alt="No Image" /></td>
                    <td>{{ $result->name }}</td>
					<td>@if(isset($result->mobile)){{$result->mobile}}@elseif(isset($result->phone)){{$result->phone}} @endif</td>
					<td>{{ $result->created_at }}</td>
                    <td>@if($result->status==0)<span class="label label-danger">Pending</span>@elseif($result->status==2) <span class="label label-danger">Declined</span> @endif()</td>
                    <td><a href="@if($result->role==2){{ url('admin/approve-doctor')}}?id={{ $result->id }}@elseif($result->role==4){{ url('admin/approve-diagnostic') }}?id={{ $result->id }} @elseif($result->role==5){{ url('admin/approve-hospital') }}?id={{ $result->id }}@endif"><span class="btn btn-success">View</span></a></td>
                </tr>
                @endforeach
                </thead>
                <tbody>
                
               
               </tbody>
            </table>
			{!! Form::close() !!}
        </section>
    </div>
 </div>
 <SCRIPT language="javascript">
	$(function(){

		// add multiple select / deselect functionality
		$("#selectall").click(function () {
			  $('.case').attr('checked', this.checked);
		});

		// if all checkbox are selected, check the selectall checkbox
		// and viceversa
		$(".case").click(function(){

			if($(".case").length == $(".case:checked").length) {
				$("#selectall").attr("checked", "checked");
			} else {
				$("#selectall").removeAttr("checked");
			}

		});
	});
 </SCRIPT>
@endsection