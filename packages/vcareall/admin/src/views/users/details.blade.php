@extends('admin::template')

@section('admin::content')
<style type="text/css">
.pac-container{z-index:1100000;}
</style>
<link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css'/>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> User Details</h3>
            <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                    <li><i class="fa fa-bars"></i>User</li>
                    <li><i class="fa fa-square-o"></i>Details</li>
            </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <!-- profile-widget -->
    <div class="col-lg-12">
        <section class="panel">
        <div class="profile-widget" style="background:white;color:#000;">
          <div class="panel-body">
            <div class="col-lg-2 col-sm-2">
              <!-- <h4>Jenifer Smith</h4>   -->  
              <style>
                  .follow-ava{width:82px;height:82px;overflow:hidden;position:relative;}
                  .btn-hides{position: absolute;left: 0;right: 0; bottom: -51px;}
                  .follow-ava:hover .btn-hides{bottom:0px;transition-duration: 1s;}
                  
              </style>
              <div class="follow-ava">
                  @if(!empty($user->image))
                  <img src="{{ $user->image }}" width="100">
                  @else
                     <img src="{{ asset("public/admin/img/user-placeholder.png") }}" width="100">
                  @endif
                  <p class="btn-hides">
                 <!-- <button class="btn btn-danger" data-toggle="modal" data-target="#delete_pic_model"><i class="icon_trash"></i></button>-->
                   <button class="btn btn-success"  data-toggle="modal" data-target="#upload_pic_modal"><i class="icon_pencil"></i></button>
                  </p>
              </div>
              <!-- <h6>Administrator</h6> -->
            </div>
            <div class="col-lg-6 col-sm-6 follow-info">
                <h3 class="margin-0">{{ $user->name}}<span class="pull-right btn btn-default" data-toggle="modal" data-target="#edit_profile_modal"><i class="fa fa-pencil"></i></span></h3>
                <p class="margin-0">
                        {{ $user->address_one}}
                </p>
				<p class="margin-0">{{ $user->mobile}}</p>
				<p>{!! $login->status?'<span class="label label-success">Active</span>':'<span class="label label-danger">Inactive</span>' !!}</p>
            </div>
            <div class="col-lg-2 col-sm-4 follow-info text-right pull-right">
                    <button class="btn btn-info  btn-sm" data-toggle="modal" data-target="#profile_accept_model">Active</button>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#profile_declined_modal">Inactive</button>
            </div>
        </div>
        </div>
        </section>
    </div>
</div>
<!-- Edit profile Modal -->
<div class="modal fade" id="edit_profile_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::model($user,['url' => 'admin/user/update','method' => 'post','class'=>'form-horizontal','files' => true]) !!}
            {{ Form::hidden('id', $user->user_id,['id'=>'user_id']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Basic Informations</h4>
            </div>
            <div class="modal-body">
                @if(session('message') && session('status')==0)
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon_close"></i>
                    </button>
                    {{ session('message') }}
                    @foreach ($errors->all() as $error)
                    <br>{{ $error }}
                    @endforeach
                </div> 
                @endif
                <div class="panel-body details">
                    <div class="form-group">
                         <label class="control-label col-sm-4">Name</label>
                         <div class="col-sm-6">
                             {{ Form::text('name', null,['class'=>'form-control']) }}
                         </div>
                     </div>
                    <div class="form-group">
                         <label class="control-label col-sm-4">Gender</label>
                         <div class="col-sm-6">
                              Male {{ Form::radio('gender', 1, $user->gender==1?true:false) }}
                              Female {{ Form::radio('gender', 2, $user->gender==2?true:false) }}
                         </div>
                     </div>
                    <div class="form-group">
                         <label class="control-label col-sm-4">Mobile</label>
                         <div class="col-sm-6">
                               {{ Form::number('mobile', null,['class'=>'form-control']) }}
                         </div>
                     </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4"> About</label>
                        <div class="col-sm-6">
                            {{ Form::textarea ('about', null,['class'=>'form-control','rows'=>3]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Address</label>
                        <div class="col-sm-6">
                            {{ Form::text('address_one', null,['class'=>'geocomplete form-control']) }}
                            {{ Form::hidden('latitude', null,['data-geo'=>'lat']) }}
                            {{ Form::hidden('longitude', null,['data-geo'=>'lng']) }}
                            {{ Form::hidden('locality', null,['data-geo'=>'locality']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">City</label>
                        <div class="col-sm-6">
                            {{ Form::text('city_name', null,['class'=>'form-control','data-geo'=>'locality']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">State</label>
                        <div class="col-sm-6">
                            {{ Form::text('state_name', null,['class'=>'form-control','data-geo'=>'administrative_area_level_1']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Country</label>
                        <div class="col-sm-6">
                            {{ Form::text('country_name', null,['class'=>'form-control','data-geo'=>'country']) }}
                            {{ Form::hidden('pincode', null,['data-geo'=>'postal_code']) }}
                        </div>
                    </div>
                 </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-info  btn-sm" type="submit">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Profile accept model -->
<div class="modal fade" id="profile_accept_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/user/profile-accept','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $user->user_id,['id'=>'user_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Accept Profile</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0))
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Profile declined model -->
<div class="modal fade" id="profile_declined_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/user/profile-declined','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $user->user_id,['id'=>'user_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Decline Profile</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0))
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Delete profile picture -->
<div class="modal fade" id="delete_pic_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/user/delete-image','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $user->user_id,['id'=>'user_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Image</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Upload profile picture -->
<div class="modal fade" id="upload_pic_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/user/upload-image','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $user->user_id,['id'=>'user_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Upload Profile Picture</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                    <div class="form-group">
                        <label class="control-label col-sm-4">Image</label>
                        <div class="col-sm-5">
                            <input type ="file" id="image" name="image"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Save</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC00fGCV5rT-LH9xZZVwjnTn39jW64URLM&amp;libraries=places"></script>
<script src="{{ asset('public/js/jquery.geocomplete.min.js') }}"></script>
<script type="text/javascript">
/**
 * Note
 * type = 1 for add service
 * type = 2 delete services
 * type = 3 add specialties
 * type = 4 approve profile
 * type = 5 declined profile
 * type = 6 upload image
 * type = 7 delete image
 */
$(document).ready(function(){
    $(".geocomplete").geocomplete({
        details: ".details",
        detailsAttribute: "data-geo"
    });
    var popup = "{{ session('status') }}";
    var type = "{{ session('type') }}";
    if(popup && type==1){
        $('#add_modal').modal('show');
    }
    if(popup && type==2){
        $('#confirm_delete_service').modal('show');
    }
    if(popup && type==3){
        $('#add_specialty_modal').modal('show');
    }
    if(popup && type==4){
        $('#confirm_delete_specialtyl').modal('show');
    }
    if(popup && type==5){
        $('#profile_declined_modal').modal('show');
    }
    if(popup && type==6){
        $('#profile_accept_model').modal('show');
    }
    if(popup && type==7){
        $('#upload_pic_model').modal('show');
    }
    if(popup && type==8){
        $('#delete_pic_model').modal('show');
    }
    if(popup && type==9){
        $('#edit_profile_modal').modal('show');
    }
    $('#delete-service').click(function(){
        var id = $(this).attr('rel');
        $("#delete-services-form input[name=id]").val(id);
    });
    $('#delete-specialty').click(function(){
        var id = $(this).attr('rel');
        $("#delete-specialty-form input[name=id]").val(id);
    });
    $('#edit-profile').click(function(){
        var id = $(this).attr('rel');
        $("#delete-specialty-form input[name=id]").val(id);
    });
    $('.ddlCars').multiselect();
})
</script>
@endsection
