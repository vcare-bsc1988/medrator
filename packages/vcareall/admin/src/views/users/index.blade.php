@extends('admin::template')

@section('admin::content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
	 
<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Users</h3>
            <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                    <li><i class="fa fa-bars"></i>Users</li>
            </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="table-responsive">
                <table class="table display" id="employee_grid">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact No</th>
                        <th>Gender</th>
                        <th>Dob</th>
                        <th>Date of Registration</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                </table>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
    $('#employee_grid').DataTable({
        "order": [[ 0, "desc" ]],
        "bProcessing": true,
        "serverSide": true,
        "ajax":{
           url :"{{ url('admin/user-list') }}", // json datasource
           type: "post",  // type of method  ,GET/POST/DELETE
           error: function(){ 
             $("#employee_grid_processing").css("display","none");
           }
         }
    });   
});
</script>
@endsection