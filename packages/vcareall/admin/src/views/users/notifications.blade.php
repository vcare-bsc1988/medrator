@extends('admin::template')

@section('admin::content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa fa-bars"></i> Notifications</h3>
        <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                <li><i class="fa fa-bars"></i>Notifications</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <section class="panel">
            <header class="panel-heading no-border">
                <table width="100%"><tr><td>Notifications </td><td align="right"><a href="{{ url('admin/clear-notifications') }}/{{ Auth::user()->id }}">Clear All</a></td>
            </header>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Title</th>
                    <th>Message</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php  $i =1 ;?>
                @foreach($notifications as $notification)
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $notification->title }}</td>
                    <td>{{ $notification->message }}</td>
                    <td>Mark as <a href="{{ $notification->status==1?url('admin/notification-unread/'.$notification->id):url('admin/notification-read/'.$notification->id) }}">{{ $notification->status==1?'Unread':'Read' }}</a> <a href="{{ url('admin/clear-notification/'.$notification->id) }}">Clear</a></td>
                    <?php $i++; ?>
                </tr>
                @endforeach
                <tr>
                    <td colspan="4">{{ $notifications->links() }}</td>
                </tr>
               </tbody>
            </table>
        </section>
    </div>
 </div>
@endsection