@extends('admin::template')

@section('admin::content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
	 
<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Manage Entities</h3>
            <ol class="breadcrumb">
                    <li><i class="fa fa-bars"></i>Diagnostics</li>
            </ol>
    </div>
</div>
@if(session('message'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            <strong>{{ session('message') }}</strong> 
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            <strong> {{ session('message') }}</strong>
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="table-responsive">
				<div class="clearfix"></div>
				<div class="col-xs-12" style="margin-top:10px;"><a href="{{ url('admin/diagnostic/create')}}"  class="pull-right btn btn-outline btn-info btn-xs"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Add Centre</a></div>
                <table class="table display" id="employee_grid">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Online Booking</th>
                        <th>Online Report</th>
						<th>Date of Registration</th>
						<th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                </table>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
    $('#employee_grid').DataTable({
        "order": [[ 0, "desc" ]],
        "bProcessing": true,
        "serverSide": true,
        "ajax":{
           url :"{{ url('admin/diagnostics') }}", // json datasource
           type: "post",  // type of method  ,GET/POST/DELETE
           error: function(){ 
             $("#employee_grid_processing").css("display","none");
           }
         }
    });   
});
</script>
@endsection