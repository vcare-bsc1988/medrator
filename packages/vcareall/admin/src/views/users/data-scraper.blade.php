@extends('admin::template')

@section('admin::content')
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Pages</h3>
            <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                    <li><i class="fa fa-bars"></i>Scrap Doctor</li>
            </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <div class="col-lg-6">
        <section class="panel">
            <header class="panel-heading">Scrap Doctor
            </header>
            <div class="panel-body">
                {{ Form::open(['url' => 'admin/scrap/doctors','method'=>'post', 'class'=>'form-vertical']) }}
                    <div class="form-group col-lg-12">
                        Select City :{{ Form::select('city', $cities, null, ['placeholder' => 'Select city','class'=>'form-control']) }}
                    </div>
					<div class="form-group col-lg-12">
                        Select specialty: {{ Form::select('speciality', $categories, null, ['placeholder' => 'Select specialty','class'=>'form-control']) }}
                    </div>
                    <div class="form-group col-lg-12">
                        <button type="submit" class="btn btn-primary">Next >></button>
                    </div>
                {{ Form::close() }}
            </div>
        </section>
    </div>

</div>
@endsection