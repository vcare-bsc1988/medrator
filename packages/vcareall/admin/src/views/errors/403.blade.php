@extends('admin::template')

@section('admin::content')
<!--==================================
	 Header parts starts here
==================================-->						 
<div class="first-section p82-topbot">
	<div class="container">
		<div class="row text-center">
			<h1>You don't have permission.</h1>
		</div><!-- /.row -->
	</div><!-- /.container -->   
</div>
@endsection

