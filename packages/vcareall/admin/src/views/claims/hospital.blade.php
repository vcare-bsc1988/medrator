@extends('admin::template')

@section('admin::content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa fa-bars"></i> Claimed Hospitals Profile</h3>
        <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Claimed Request</a></li>
                <li><i class="fa fa-bars"></i>Hospitals</li>
        </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr class="panel-heading">
                    <th>#</th>
                    <th>Claimed By</th>
                    <th>Claimed Profile</th>
                    <th>Moderation Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $sr=1;?>
                  @foreach($claimed_profiles as $claimed)
                  <?php //print_r($claimed_profile); exit; ?>
                  <tr>
                    <td>{{ $sr++ }}</td>
                    <td>                   
                        <div class="col-xs-12 padding-0">
                            <div class="col-md-1" style="padding:0px;">
                                <a href="javascript::void(0)" class="activity-img activity-img1">
                                   <img src="{{ $claimed['claimed_by']->user->image?$claimed['claimed_by']->user->image:  asset('public/admin/img/user-placeholder.png') }}" style="width:50px;height:70px;" alt="No Image" />
				</a>
                            </div>
                            <div class="col-md-10">
                                <p class="attribution act-in" style="margin-bottom:0px;"><a href="javascript::void(0)">{{ $claimed['claimed_by']->user->name }}</a></p>
                                <p style="margin-bottom:0px;">{{ $claimed['claimed_by']->email }}</p>
                            </div>
                            <div class="clearfix" style="margin-bottom:10px;"></div>
                        </div>
                    </td>
                    <td>
                        <div class="col-xs-12 padding-0">
                            <div class="col-md-1" style="padding:0px;">
                                <a href="javascript::void(0)" class="activity-img activity-img1">
                                   <img src="{{ $claimed['claimed_profile']->image?$claimed['claimed_profile']->image:  asset('public/admin/img/user-placeholder.png') }}" style="width:50px;height:70px;" alt="No Image" />
				</a>
                            </div>
                            <div class="col-md-10">
                                <p class="attribution act-in" style="margin-bottom:0px;"><a href="javascript::void(0)">{{ $claimed['claimed_profile']->name }}</a></p>
                                <p class="margin-0">
                                        {{ $claimed['claimed_profile']->address_one}}
                                </p>
                                {{--@if($claimed['claimed_profile']->serviceList()->get()->count()>0)
                                <p class="margin-0">
                                        @foreach($claimed['claimed_profile']->serviceList as $service)
                                        <br>{{ $service->name }}
                                        @endforeach        
                                </p>
                                @endif --}}
                                <p class="margin-0">
                                        <label><b>Consulting Fee <i class="fa fa-inr"></i></b></label> {{ $claimed['claimed_profile']->consultation_fee}}
                                </p>
                            </div>
                            <div class="clearfix" style="margin-bottom:10px;"></div>
                        </div>
                    </td>
                    <td>
						@if($claimed['claimed_profile']->claim_status ==0) <span class="label label-danger">Pending</span> @endif
						@if($claimed['claimed_profile']->claim_status ==1) <span class="label label-success">Approved</span> @endif
						@if($claimed['claimed_profile']->claim_status ==2) <span class="label label-danger">Disapproved</span> @endif
					</td>
                    <td>
                        <a class="btn btn-success btn-sm" onclick="return confirm('Are you sure ?')" href="{{ url('admin/claime/approve_hospital')}}?id={{ $claimed['claimed_profile']->id}}&uid={{ $claimed['claimed_by']->id}}" title="Bootstrap 3 themes generator">Approve</a>
                        <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure ?')" href="{{ url('admin/claime/disapprove_hospital')}}?id={{ $claimed['claimed_profile']->id}}&uid={{ $claimed['claimed_by']->id}}" title="Bootstrap 3 themes generator">Disapprove</a>
						<a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure ?')" href="{{ url('admin/claime/delete_hospital')}}?id={{ $claimed['claimed_profile']->id}}&uid={{ $claimed['claimed_by']->id}}" title="Bootstrap 3 themes generator">Delete</a>
					</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

        </section>
    </div>
</div>
@endsection