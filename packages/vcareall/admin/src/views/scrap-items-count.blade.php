@extends('admin::template')

@section('admin::content')
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Pages</h3>
            <ol class="breadcrumb">
				<li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
				<li><i class="fa fa-bars"></i>Scrap Doctor</li>
            </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <div class="col-lg-6">
        <section class="panel">
            <header class="panel-heading">Scrap Doctor
            </header>
            <div class="panel-body">
				<h1>You have selected <strong>{{ $city }} & {{ $speciality }}
				</strong></h1>
				<h3>Total {{$tolta_items}} matches found for:  {{ $speciality }} in {{ $city }}</h3>
                {{ Form::open(['url' => 'admin/scrap','method'=>'post', 'class'=>'form-vertical']) }}
                    <div class="form-group col-lg-12">
						{{ Form::hidden('city',$city) }}
                    </div>
					<div class="form-group col-lg-12">
						{{ Form::hidden('speciality', $speciality) }}
                    </div>
					@if(@$tolta_items>10)
					<div class="form-group col-lg-12">
                        <?php 
						$pageArr= array();
						for($i=1; $i<=round($tolta_items/10); $i++){
							$pageArr[$i] ='Page'.$i;
						}
						?>
						Select Page From :{{ Form::select('page_from', $pageArr, 1, ['placeholder' => 'Select Page No','id'=>'page_from','class'=>'form-control']) }}
                    </div>
					<div class="form-group col-lg-12">
                        <?php 
						$pageArr= array();
						for($i=1; $i<=round($tolta_items/10); $i++){
							$pageArr[$i] ='Page'.$i;
						}
						?>
						Select Page To :{{ Form::select('page_to', $pageArr, 1, ['placeholder' => 'Select Page No','id'=>'page_to','class'=>'form-control']) }}
                    </div>
					@endif
                    <div class="form-group col-lg-12">
                        <button type="submit" class="btn btn-primary">Download Excel</button>
                    </div>
                {{ Form::close() }}
            </div>
        </section>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function () {    
        $("#page_from").change(function () {         
            var opt = '';
            var from1 = parseInt($(this).val());
            var to = parseInt("{{ (int)round($tolta_items/10) }}");
            console.log(to);
            for (var i = from1 ; i <= to; i++) {
                opt += "<option value='" + i + "'>"+"Page-" + i + "</option>";
            }
            $("#page_to").html(opt);
        });
    });
</script>
@endsection