@extends('admin::template')

@section('admin::content')
<!--overview start-->
            <div class="row">
                  <div class="col-lg-12">
                          <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
                          <ol class="breadcrumb">
                                  <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                                  <li><i class="fa fa-laptop"></i>Dashboard</li>						  	
                          </ol>
                  </div>
          </div>

<div class="row">
                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                          <a href="{{ url('admin/doctors')}}">
							  <div class="info-box blue-bg">
								<img src="{{ asset ("public/images/med_icons/doctor.png") }}">
								<div class="count">{{ $doctor->count() }}</div>
								<div class="title">Doctors</div>						
							  </div><!--/.info-box-->	
						  </a>		
                  </div><!--/.col-->

                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<a href="{{ url('admin/hospitals-clinics')}}">
						  <div class="info-box brown-bg">
                            <img src="{{ asset ("public/images/med_icons/hospital.png") }}">
                            <div class="count">{{ $hospital->count() }}</div>
                            <div class="title">Hospitals</div>						
                          </div><!--/.info-box-->
						</a>						  
                  </div><!--/.col-->	

                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <a href="{{ url('admin/diagnostic-centers')}}">
						  <div class="info-box dark-bg">
                            <img src="{{ asset ("public/images/med_icons/diagonstic_center.png") }}">
                            <div class="count">{{ $diagnostic->count() }}</div>
                            <div class="title">Diagnostics</div>						
                          </div><!--/.info-box-->
						</a>
                  </div><!--/.col-->

                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					    <a href="{{ url('admin/users')}}">
						  <div class="info-box green-bg">
                              <img src="{{ asset ("public/images/med_icons/user_icon.png") }}" height="85">
                            <div class="count">{{ $user->count() }}</div>
                            <div class="title">Users</div>						
                          </div><!--/.info-box-->
						</a>
                  </div><!--/.col-->

          </div><!--/.row-->

</div>  


    <!-- Today status end -->



          <div class="row">

                  <div class="col-lg-9 col-md-12">	
                          <div class="panel panel-default">
                                  <div class="panel-heading">
                                          <h2><i class="fa fa-flag-o red"></i><strong>Recently  Joined Members</strong></h2>
                                          <div class="panel-actions"></div>
                                  </div>
                                  <div class="panel-body">
                                        <table class="table bootstrap-datatable countries">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>County</th>
													<th>Facebook Connect</th>
													<th>Google+ Connect</th>
                                                    <th>Registered Date</th>
                                                </tr>
                                            </thead>   
                                            <tbody>
                                                <?php $i=1;?>
                                                @foreach($user as $usr)  
                                                <tr>
                                                      <td>{{ $i++ }}</td>
                                                      <td>{{ $usr->user['name'] }}</td>
                                                      <td>{{ $usr->email }}</td>
                                                      <td>  
                                                          {{ $usr->user['country_name'] }}
                                                      </td>
													  <td>
														{{ ($usr->socialAccount()->where('provider','facebook')->get()->count()>0)?'Yes':'No'}}
													  </td>
													  <td>
														{{ ($usr->socialAccount()->where('provider','googleplus')->get()->count()>0)?'Yes':'No'}}
													  </td>
                                                      <td>{{ $usr->created_at }}</td>
                                                  </tr>
                                                  @endforeach
                                            </tbody>
                                        </table>
                                  </div>

                          </div>	

                  </div><!--/col-->           
</div>
  <!-- statics end -->
@endsection