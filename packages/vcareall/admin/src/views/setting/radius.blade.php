@extends('admin::template')

@section('admin::content')
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Setting</h3>
            <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                    <li><i class="fa fa-bars"></i>Setting</li>
                    <li><i class="fa fa-square-o"></i>Radius</li>
            </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <div class="col-lg-6">
        <section class="panel">
            <header class="panel-heading">
                Basic Forms
            </header>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="post" action="{{ url('admin/setting/radius')}}">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Radius</label>
                        <div class="col-lg-10">
                            <input type="number" name="radius" value="{{ $radius }}" class="form-control" id="number" placeholder="Radius">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-danger">Update</button>
                        </div>
                    </div>
                </form>

            </div>
        </section>
    </div>
</div>
@endsection