@extends('admin::template')

@section('admin::content')
<link rel="stylesheet" href="{{ asset('public/css/index.css') }}" type ="text/css" />
<style type="text/css">
.pac-container{z-index:1100000;}
form label{float:right;}
</style>
<link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css'/>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Hospital Details</h3>
            <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                    <li><i class="fa fa-bars"></i>Hospital</li>
                    <li><i class="fa fa-square-o"></i>Details</li>
            </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <!-- profile-widget -->
    <div class="col-lg-12">
        <section class="panel">
        <div class="profile-widget" style="background:white;color:#000;">
          <div class="panel-body">
            <div class="col-lg-2 col-sm-2">
              <!-- <h4>Jenifer Smith</h4>   -->  
              <style>
                  .follow-ava{width:82px;height:82px;overflow:hidden;position:relative;}
                  .btn-hides{position: absolute;margin-top:0px;}
                  .follow-ava:hover .btn-hides{margin-top:-40px;transition-duration: 1s;}
                  
              </style>
              <div class="follow-ava">
                  @if(!empty($hospital->image))
                  <img src="{{ $hospital->image }}" width="100">
                  @else
                     <img src="{{ asset("public/images/med_icons/hospital.png") }}" width="100">
                  @endif
                  <p class="btn-hides">
                  <!--<button class="btn btn-danger" data-toggle="modal" data-target="#delete_pic_model"><i class="fa fa-trash"></i></button>-->
                   <button class="btn btn-success"  data-toggle="modal" data-target="#upload_pic_modal"><i class="icon_pencil"></i></button>
                  </p>
              </div>
              <!-- <h6>Administrator</h6> -->
            </div>
            <div class="col-lg-6 col-sm-6 follow-info">
                <h3 class="margin-0">{{ $hospital->name}}<span rel="{{ $hospital->id}}" class="pull-right btn btn-default" id="edit-profile" data-toggle="modal" data-target="#edit_profile_modal"><i class="fa fa-pencil"></i></span></h3>
                <p class="margin-0">
                        {{ $hospital->address_one}}
                </p>
                @if($hospital->specialties()->get()->count()>0)
                <p class="margin-0">
                        @foreach($hospital->specialtyList as $specialty)
                        <br>{{ $specialty->name }}
                        @endforeach 
                </p>
                @endif
                <p class="margin-0">
                        <label><b>Consulting Fee </b></label> {{ $hospital->consultation_fee}}
                </p>
                @for ($i=1; $i <= 5 ; $i++)
                    <span class="glyphicon glyphicon-star{{ ($i <= $hospital->rating_cache) ? '' : '-empty'}}"></span>
                @endfor
                {{ $hospital->rating_count}} Reviews <br>
				{!! $hospital->status?'<span class="label label-success">Approved</span>':'<span class="label label-danger">Dissapproved</span>' !!}
            </div>
            <div class="col-lg-2 col-sm-4 follow-info text-right pull-right">
                    @role(['administrator','super-admin','manager'])
					<button class="btn btn-info  btn-sm" data-toggle="modal" data-target="#profile_accept_model">Accept</button>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#profile_declined_modal">Reject</button>
					@endrole
            </div>
			<div class="row" style="float: left;width: 100%;padding: 0px;">
				<div class="col-xs-12">
						<hr>
						<div class="col-lg-4 col-sm-4 follow-info">
								<h3>Speciality <span class="pull-right btn btn-default"  data-toggle="modal" data-target="#add_specialty_modal"><i class="fa fa-plus"></i></span></h3>
								<hr>
								<ul class="nav nav-stacked custome-speciality">
										<ul class="nav nav-stacked custome-speciality">
											@foreach($hospital->specialties as $specialty) 
											<li>
												<span>{{ $specialty->specialty->name }} <i class="pull-right fa fa-minus-circle btn btn-danger delete-specialty" rel="{{$specialty->id}}" data-toggle="modal" data-target="#confirm_delete_specialty"></i></span>
											</li>
											@endforeach
										</ul>
								</ul>
						</div>
						<p>
								</p>
						<p></p>
				</div>
			</div>
			<div class="row" style="float: left;width: 100%;padding: 0px;">
				<div class="col-xs-12">
					<hr>
					<div class="col-lg-4 col-sm-4 follow-info">
						<h3>Timings <button rel="{{ $hospital->id }}" class="btn btn-primary btn-xs add-schedules">Add/Edit</button></h3>
						<section class="demos" >
							<article class="schedule-artcle">
							{!! Helper::widgetTimingsDoctor($hospital) !!}
							</article>
						</section>
					</div>					
				</div>
			</div>
        </div>
        </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
       <section class="panel">
             <header class="panel-heading tab-bg-info">
                 <ul class="nav nav-tabs my-tabs-01">
                     <li class="active">
                         <a data-toggle="tab" href="#recent-activity">
                             Review & Rating
                         </a>
                     </li>
                     <li class="">
                         <a data-toggle="tab" href="#profile">
                             Associated Doctors
                         </a>
                     </li>
                 </ul>
             </header>
             <div class="panel-body">
                 <div class="tab-content">
                     <div id="recent-activity" class="tab-pane active">
                         <div class="profile-activity">                                          
                             @if($hospital->reviews()->get()->count()>0)
                             @foreach($hospital->reviews as $review)
                             <div class="act-time">                                      
                                 <div class="activity-body act-in">
                                     <span class="arrow"></span>
									 <div class="col-xs-12 padding-0">
										<div class="col-md-1" style="padding:0px;">
											<a href="{{ url('admin/user-details/'.$review->user->id) }}" class="activity-img activity-img1">
												@if($review->user->image)
												 <img class="avatar" width="70" src="{{ $review->user->image }}">
												 @else
												 <img class="avatar" width="70" src="{{ asset('public/admin/img/user-placeholder.png') }}">
												 @endif
											</a>
										</div>
										<div class="col-md-10">
											<p class="attribution act-in" style="margin-bottom:0px;"><a href="{{ url('admin/user-details/'.$review->user->id) }}">{{ $review->user->user->name }}</a></p>
											<p style="margin-bottom:0px;">{{ $review->user->email }}</p>
										</div>
										<div class="clearfix" style="margin-bottom:10px;"></div>
										<p>@for ($i=1; $i <= 5 ; $i++)
                                            <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                                            @endfor
										</p>
										<b><strong>{{ $review->title }}</strong></b><br>
										<span>{{ $review->comment }}</span>
									</div>
                                 </div>
                             </div>
                             @endforeach
                             @else
                             <p>No review was found.</p>
                             @endif
                         </div>
                     </div>
                     <!-- profile -->
                     <div id="profile" class="tab-pane">
                       <section class="panel">
                        @if($hospital->doctors()->get()->count()>0)
                        @foreach($hospital->doctors as $doctors)
                        <div class="act-time">                                      
                            <div class="activity-body act-in">
                                <span class="arrow"></span>
								<div class="col-xs-12 padding-0">
									<div class="col-md-1" style="padding:0px;">
										<a href="{{ url('admin/doctor-details/'.$doctors->id) }}" class="activity-img activity-img1">
											@if($doctors->image)
											<img class="avatar" width="70" src="{{ $doctors->image }}">
											@else
											<img class="avatar" width="70" src="{{ asset('public/admin/img/user-placeholder.png') }}">
											@endif
										</a>
									</div>
									<div class="col-md-10">
										<p class="attribution act-in" style="margin-bottom:0px;"><a href="{{ url('admin/doctor-details/'.$doctors->id) }}">{{ $doctors->name }}</a></p>
										<p style="margin-bottom:0px;">{{ $doctors->address_one }}</p>
										<p class="margin-0">
												<label><b>Consulting Fee </b></label> {{ $doctors->consultation_fee}}
										</p>
									</div>
									<div class="clearfix" style="margin-bottom:10px;"></div>
									<p> 
										@for ($i=1; $i <= 5 ; $i++)
                                        <span class="glyphicon glyphicon-star{{ ($i <= $hospital->rating_cache) ? '' : '-empty'}}"></span>
										@endfor
										{{ $doctors->rating_count }} Reviews
									</p>
								</div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <p>No hospital was found.</p>
                        @endif
                       </section>
                         <section>
                             <div class="row">                                              
                             </div>
                         </section>
                     </div>
                 </div>
             </div>
         </section>
    </div>
 </div>
<!-- Edit profile Modal -->
<div class="modal fade" id="edit_profile_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::model($hospital,['url' => 'admin/hospital/update','method' => 'post','id'=>'edit_profile_form','class'=>'form-horizontal','files' => true]) !!}
            {{ Form::hidden('id', null) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Basic Informations</h4>
            </div>
            <div class="modal-body details">
                @if(session('message') && session('status')==0)
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon_close"></i>
                    </button>
                    <strong>Oh snap!</strong> {{ session('message') }}
                    @foreach ($errors->all() as $error)
                    <br>{{ $error }}
                    @endforeach
                </div> 
                @endif
                <div class="panel-body">
                    <div class="form-group">
                         <label class="control-label col-sm-4">Name</label>
                         <div class="col-sm-6">
                             {{ Form::text('name', null,['class'=>'form-control']) }}
                         </div>
                     </div>
                    <div class="form-group">
                         <label class="control-label col-sm-4">Phone</label>
                         <div class="col-sm-6">
                               {{ Form::number('phone', null,['class'=>'form-control']) }}
                         </div>
                     </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4"> Consultation Fee</label>
                        <div class="col-sm-6">
                            {{ Form::text('consultation_fee', null,['class'=>'form-control']) }}
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-sm-4"> No of Doctors</label>
                        <div class="col-sm-6">
                            {{ Form::text('no_of_doctors', null,['class'=>'form-control']) }}
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-sm-4"> 	No of Beds</label>
                        <div class="col-sm-6">
                            {{ Form::text('no_of_beds', null,['class'=>'form-control']) }}
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-sm-4"> 	ICU Facility</label>
                        <div class="col-sm-6">
                            {{ Form::checkbox('icu_facility',1) }}
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-sm-4"> 	24 Hours Emergency</label>
                        <div class="col-sm-6">
                            {{ Form::checkbox('24_hours_emergency',1) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Address</label>
                        <div class="col-sm-6">
                            {{ Form::text('address_one', null,['class'=>'geocomplete form-control']) }}
                            {{ Form::hidden('latitude', null,['data-geo'=>'lat']) }}
                            {{ Form::hidden('longitude', null,['data-geo'=>'lng']) }}
                            {{ Form::hidden('locality', null,['data-geo'=>'locality']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">City</label>
                        <div class="col-sm-6">
                            {{ Form::text('city_name', null,['class'=>'form-control','data-geo'=>'locality']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">State</label>
                        <div class="col-sm-6">
                            {{ Form::text('state_name', null,['class'=>'form-control','data-geo'=>'administrative_area_level_1']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Country</label>
                        <div class="col-sm-6">
                            {{ Form::text('country_name', null,['class'=>'form-control','data-geo'=>'country']) }}
                            {{ Form::hidden('pincode', null,['data-geo'=>'postal_code']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Website</label>
                        <div class="col-sm-6">
                            {{ Form::text('website', null,['class'=>'form-control']) }}
                        </div>
                    </div>
                 </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-info  btn-sm" type="submit">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Add specialty model-->
<div class="modal fade" id="add_specialty_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/hospital/add-specialties','method' => 'post','files' => true]) !!}
            {{ Form::hidden('id', $hospital->id) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Specialty</h4>
            </div>
            <div class="modal-body">
                @if(session('message') && session('status')==0)
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon_close"></i>
                    </button>
                    <strong>Oh snap!</strong> {{ session('message') }}
                </div> 
                @endif
                <div class="form-group ">
                    <label for="specialty" class="control-label col-lg-2">Specialty <span class="required">*</span></label>
                    <div class="col-sm-6">
                        {{ Form::select('specialty_id[]', $specialties, null, ['class' => 'form-control m-bot15 ddlCars', 'multiple'=>'multiple']) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-info  btn-sm" type="submit">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Confirm delete specialty Modal START -->
<div class="modal fade" id="confirm_delete_specialty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/hospital/delete-specialty','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', null,['id'=>'service_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Specialty</h4>
                </div>
                <div class="modal-body">
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-warning" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Profile accept model -->
<div class="modal fade" id="profile_accept_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/hospital/profile-accept','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $hospital->id,['id'=>'doctor_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Accept Profile</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0))
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Profile declined model -->
<div class="modal fade" id="profile_declined_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/hospital/profile-declined','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $hospital->id) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Decline Profile</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0))
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Delete profile picture -->
<div class="modal fade" id="delete_pic_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/hospital/delete-image','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $hospital->id,['id'=>'hospital_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Image</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Upload profile picture -->
<div class="modal fade" id="upload_pic_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/hospital/upload-image','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $hospital->id,['id'=>'doctor_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Upload Profile Picture</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                    <div class="form-group">
                        <label class="control-label col-sm-4">Image</label>
                        <div class="col-sm-5">
                            <input type ="file" id="image" name="image"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Save</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
 <!-- Modal for adding schedular -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p><h4 class="modal-title">Add/Edit Schedules</h4> <span id="schedule-msg"></span></p>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs schedule-ul" role="tablist">
                        <li role="presentation" class="active"><a href="#sun" aria-controls="sun" role="tab" data-toggle="tab">SUN</a></li>
                        <li role="presentation"><a href="#mon" aria-controls="mon" role="tab" data-toggle="tab">MON</a></li>
                        <li role="presentation"><a href="#tue" aria-controls="tue" role="tab" data-toggle="tab">TUE</a></li>
                        <li role="presentation"><a href="#wed" aria-controls="wed" role="tab" data-toggle="tab">WED</a></li>
                        <li role="presentation"><a href="#thu" aria-controls="thu" role="tab" data-toggle="tab">THU</a></li>
                        <li role="presentation"><a href="#fri" aria-controls="fri" role="tab" data-toggle="tab">FRI</a></li>
                        <li role="presentation"><a href="#sat" aria-controls="sat" role="tab" data-toggle="tab">SAT</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tab-content1">
                        {{-- Content is loading from ajax --}}
                        
                </div>
            </div>
            <!--<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>-->
        </div>
    </div>
</div> 
 
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC00fGCV5rT-LH9xZZVwjnTn39jW64URLM&amp;libraries=places"></script>
<script src="{{ asset('public/js/jquery.geocomplete.min.js') }}"></script>
<script src="{{  asset('public/js/jquery.wizard.js') }}"></script>
<script type="text/javascript">
/**
 * Note
 * type = 3 add specialties
 * type = 4 approve profile
 * type = 5 declined profile
 * type = 6 upload image
 * type = 7 delete image
 */
$(document).ready(function(){
    $(".geocomplete").geocomplete({
        details: ".details",
        detailsAttribute: "data-geo"
    });
    var popup = "{{ session('status') }}";
    var type = "{{ session('type') }}";
    if(popup && type==1){
        $('#add_modal').modal('show');
    }
    if(popup && type==2){
        $('#confirm_delete_service').modal('show');
    }
    if(popup && type==3){
        $('#add_specialty_modal').modal('show');
    }
    if(popup && type==4){
        $('#confirm_delete_specialtyl').modal('show');
    }
    if(popup && type==5){
        $('#profile_declined_modal').modal('show');
    }
    if(popup && type==6){
        $('#profile_accept_model').modal('show');
    }
    if(popup && type==7){
        $('#upload_pic_model').modal('show');
    }
    if(popup && type==8){
        $('#delete_pic_model').modal('show');
    }
    if(popup && type==9){
        $('#edit_profile_modal').modal('show');
    }
    $('.delete-specialty').click(function(){
        var id = $(this).attr('rel');
        $("#delete-specialty-form input[name=id]").val(id);
    });
    $('#edit-profile').click(function(){
        var id = $(this).attr('rel');
        $("#edit_profile_form input[name=id]").val(id);
    });
    $('.ddlCars').multiselect();
})
</script>
<script type="text/javascript">
    "use strict"; 
    /**
	 * @ Schedular start.
     * @ Addinf dynamic field for schedular
     */
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper_sun         = $(".input_fields_wrap_sun"); //Fields wrapper
    var add_button_sun      = $(".add_field_button_sun"); //Add button ID
    
    var wrapper_mon         = $(".input_fields_wrap_mon"); //Fields wrapper
    var add_button_mon      = $(".add_field_button_mon"); //Add button ID
    
    var wrapper_tue         = $(".input_fields_wrap_tue"); //Fields wrapper
    var add_button_tue      = $(".add_field_button_tue"); //Add button ID
    
    var wrapper_wed         = $(".input_fields_wrap_wed"); //Fields wrapper
    var add_button_wed       = $(".add_field_button_wed"); //Add button ID
    
    var wrapper_thr         = $(".input_fields_wrap_thr"); //Fields wrapper
    var add_button_thr      = $(".add_field_button_thr"); //Add button ID
    
    var wrapper_fri       = $(".input_fields_wrap_fri"); //Fields wrapper
    var add_button_fri      = $(".add_field_button_fri"); //Add button ID
    
    var wrapper_sat         = $(".input_fields_wrap_sat"); //Fields wrapper
    var add_button_sat      = $(".add_field_button_sat"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button_sun).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            var append_data = '<div class="add-containers" style="margin-bottom:3px;overflow:hidden;"><div class="col-xs-12 col-sm-5"><input class="form-control trns" name="schedular[' + x + '][from]" type="text"></div><div class="col-xs-12 col-sm-5"><input class="form-control trns" name="schedular[' + x + '][to]" type="text"></div><div class="col-sm-2 col-xs-12"><a href="#" class="remove_field"><i class="fa fa-minus-circle" style="margin-top:12px;color:darkred;"></i></a></div></div></div>';
            x++; //text box increment
            $(wrapper_sun).append(append_data); //add input box
        }
    });
    
    $(wrapper_sun).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parent().remove(); x--;
    })
    /**
    * @ Populate add schedules form popup
    */
    $('.add-schedules').click(function(){
        var uid = "{{ Auth::user()->id }}";
        var id = $(this).attr("rel");
        var csrf_field = "{{ csrf_token() }}";
        $.ajax({
            type: "POST",
            url: "{{ url('admin/hospital-clinic/find') }}",
            data: {'id':id, '_token':csrf_field},
            beforeSend: function(){
                $('#scheduler_loader').css('display','block');
            },
            success: function(result){ 
                console.log(result);
                $('#scheduler_loader').css('display','none');
		$("#myModal").modal();
                $(".tab-content1").html(result);                
            },
            error:function(result){
                console.log(result);
            }
        });
    })
	/**
    * @ Save schedules
    */
    $('.save-schedule-btn').click(function(){
        var uid = "{{ Auth::user()->id }}";
        
        $.ajax({
            type: "POST",
            url: "{{ url('account/save-schedules') }}",
            data: $('#shcedule-form-sun').serialize(),
            success: function(result){ 
                console.log(result);
                $("#schedule-msg").html(result.message);
                $('#schedule-msg').show();
                setTimeout(function () {$('#schedule-msg').hide();},4000);
            },
            error:function(result){
                console.log(result);
            }
        });
    })
    /**
    * @ Populate add schedules form popup
    */
    $('.clinic-upload_gallry').click(function(){
        var uid = "{{ Auth::user()->id }}";
        var id = $(this).attr("rel");
        $("#clinic_id").val(id);
        return true;
    })
    /**
    * @ Save schedules
    */
</script>
@endsection
