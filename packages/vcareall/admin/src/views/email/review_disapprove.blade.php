<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Review</title>
</head>
<p>Dear Member,</p>
<h1></h1>
<p>Thank you for taking the time to write to write a review of {{ $data['entity_name'] }}. We would like to publish your review but we are unable to do the same because of one of the policy violations:</p>
<h1></h1>
<p><ul>
	<li>Use of inappropriate/ abusive language</li>
	<li>Incorrect information</li>
	<li>Does not comply with our guidelines</li>
</ul> </p>
<h1></h1>
<p>Please take out a few minutes to read our review guidelines <a href="{{ url('system-guidelines')}}">here.</a></p>
<h1></h1>
<p>Please consider re-writing your review as per our guidelines as it helps the community to find best doctors.</p>
<h1></h1>
<p>Best Regards,<br>Medrator Team</p>
<body>
</body>
</html>