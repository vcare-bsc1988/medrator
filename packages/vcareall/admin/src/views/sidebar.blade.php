<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">                
            <li class="{{request()->segment(2)=='dashboard'?'active':''}}">
                <a class="" href="{{ url('admin/dashboard') }}">
                    <i class="icon_house_alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            @role(['administrator','super-admin','manager'])
            <li class="sub-menu {{request()->segment(2)=='import-excel'?'active':''}}">
                <a href="javascript:;" class="">
                    <i class="icon_genius"></i>
                    <span>Import Excel</span>
                    <span class="menu-arrow arrow_carrot-{{request()->segment(2)=='import-excel'?'down':'right'}}"></span>
                </a>
                <ul class="sub">                          
                    <li><a class="" href="{{ url('admin/import-excel') }}">Doctor</a></li>
                    <li><a class="" href="{{ url('admin/import-excel/hospital') }}">Hospital</a></li>
                    <li><a class="" href="{{ url('admin/import-excel/diagnostic') }}">Diagnostic</a></li>
                </ul>
            </li>
            @endrole
            @role(['super-admin'])
            <li class="sub-menu {{request()->segment(2)=='staffs'?'active':''}}">
                <a class="" href="{{ url('admin/staffs') }}">
                    <i class="icon_briefcase"></i>
                    <span>Staff Accounts</span>
                </a>
            </li>
            <li class="sub-menu {{request()->segment(2)=='users'?'active':''}}">
                <a class="" href="{{ url('admin/users') }}">
                    <i class="icon_group"></i>
                    <span>Manage Users</span>
                </a>
            </li>
            @endrole
            <li class="sub-menu {{request()->segment(2)=='doctors' || request()->segment(2)=='hospitals-clinics' || request()->segment(2)=='diagnostic-centers' || request()->segment(2)=='diagnostic' || request()->segment(2)=='hospital' || request()->segment(2)=='doctor'|| request()->segment(2)=='pending-approval'|| request()->segment(2)=='approve-doctor'|| request()->segment(2)=='approve-hospital'|| request()->segment(2)=='approve-diagnostic'?'active':''}}">
                <a href="javascript:;" class="">
                    <i class="icon_grid-2x2"></i>
                    <span>Manage Entities</span>
                    <span class="menu-arrow arrow_carrot-{{request()->segment(2)=='doctors' || request()->segment(2)=='hospitals-clinics' || request()->segment(2)=='diagnostic-centers' || request()->segment(2)=='diagnostic' || request()->segment(2)=='hospital' || request()->segment(2)=='doctor' || request()->segment(2)=='pending-approval'|| request()->segment(2)=='approve-doctor'|| request()->segment(2)=='approve-hospital'|| request()->segment(2)=='approve-diagnostic'?'down':'right'}}"></span>
                </a>
                <ul class="sub">                          
                    <li><a class="" href="{{ url('admin/doctors') }}">Doctors</a></li>
                    <li><a class="" href="{{ url('admin/hospitals-clinics') }}"><span>Hospitals/Clinics</span></a></li>
                    <li><a class="" href="{{ url('admin/diagnostic-centers') }}">Diagnostic Centers</a></li>
					@role(['administrator','super-admin','manager'])
					<li><a class="" href="{{ url('admin/pending-approval') }}">Pending Approval</a></li>
					@endrole
				</ul>
            </li>
            @role(['administrator','super-admin','manager','moderator'])
            <li class="sub-menu {{request()->segment(2)=='reviews'?'active':''}}">
                <a class="" href="{{ url('admin/reviews') }}">
                    <i class="icon_group"></i>
                    <span>Review & Rating</span>
                </a>
            </li>
            @endrole
            @role(['administrator','super-admin'])
            <li class="sub-menu {{request()->segment(2)=='setting'?'active':''}}">
                <a href="javascript:;" class="">
                    <i class="icon_tools"></i>
                    <span>Setting</span>
                    <span class="menu-arrow arrow_carrot-{{request()->segment(2)=='setting'?'down':'right'}}"></span>
                </a>
                <ul class="sub">                          
                    <li><a class="" href="{{ url('admin/setting/radius') }}">Radius</a></li>
                </ul>
            </li>
            @endrole
            @role(['administrator','super-admin','manager'])
            <li class="sub-menu {{request()->segment(2)=='data-scraper'?'active':''}}">
                <a class="" href="{{ url('admin/data-scraper') }}">
                    <i class="icon_genius"></i>
                    <span>Scrap Doctors</span>
                </a>
            </li>
            @endrole
            @role(['administrator','super-admin','manager','moderator'])
            <li class="sub-menu {{request()->segment(2)=='wp-admin'?'active':''}}">
                <a class="" href="{{ url('blogs-forums/wp-admin/') }}">
                    <i class="icon_genius"></i>
                    <span>Manage Blog & Forum</span>
                </a>
            </li>
            @endrole
            @role(['administrator','super-admin','manager'])
            <li class="sub-menu {{request()->segment(2)=='user' || request()->segment(2)=='notifications'?'active':''}}">
                <a href="javascript:;" class="">
                    <i class="icon_grid-2x2"></i>
                    <span>Notifications</span>
                    <span class="menu-arrow arrow_carrot-{{request()->segment(2)=='user' || request()->segment(2)=='notifications'?'down':'right'}}"></span>
                </a>
                <ul class="sub">                          
                    <li><a class="" href="{{ url('admin/user/send-notifications') }}">Send Notification</a></li>
                    <li><a class="" href="{{ url('admin/notifications') }}"><span>Notifications</span></a></li>
                </ul>
            </li>
            @endrole
            @role(['administrator','super-admin','manager','moderator'])
            <li class="sub-menu {{request()->segment(2)=='claime'?'active':''}}"">
                <a href="javascript:;" class="">
                    <i class="icon_genius"></i>
                    <span>Claimed Request</span>
                    <span class="menu-arrow arrow_carrot-{{request()->segment(2)=='claime'?'down':'right'}}"></span>
                </a>
                <ul class="sub">                          
                    <li><a class="" href="{{ url('admin/claime/doctor') }}">Doctor</a></li>
					<li><a class="" href="{{ url('admin/claime/hospital') }}">Hospital</a></li>
					<li><a class="" href="{{ url('admin/claime/diagnostic') }}">Diagnostic</a></li>
                </ul>
            </li>
            @endrole
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>