@extends('admin::template')

@section('admin::content')
<style type="text/css">
.pac-container{z-index:1100000;}
</style>
<link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css'/>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Doctor Details</h3>
            <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                    <li><i class="fa fa-bars"></i>Doctor</li>
                    <li><i class="fa fa-square-o"></i>Details</li>
            </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <!-- profile-widget -->
    <div class="col-lg-12">
        <section class="panel">
        <div class="profile-widget" style="background:white;color:#000;">
          <div class="panel-body">
            <div class="col-lg-2 col-sm-2">
              <!-- <h4>Jenifer Smith</h4>   -->  
              <style>
                  .follow-ava{width:82px;height:82px;overflow:hidden;position:relative;}
                  .btn-hides{position: absolute;left: 0;right: 0; bottom: -51px;}
                  .follow-ava:hover .btn-hides{bottom:0px;transition-duration: 1s;}
                  
              </style>
              <div class="follow-ava">
                  @if(!empty($doctor->image))
                  <img src="{{ $doctor->image }}" width="100">
                  @else
                     <img src="{{ asset("public/admin/img/user-placeholder.png") }}" width="100">
                  @endif
                  <p class="btn-hides">
                  <!--<button class="btn btn-danger" data-toggle="modal" data-target="#delete_pic_model"><i class="fa fa-trash"></i></button>-->
                  <button class="btn btn-success"  data-toggle="modal" data-target="#upload_pic_modal"><i class="icon_pencil"></i></button>
                  </p>
              </div>
              <!-- <h6>Administrator</h6> -->
            </div>
            <div class="col-lg-6 col-sm-6 follow-info">
                <h3 class="margin-0">{{ $doctor->name}} <span rel="{{ $doctor->id}}" class="pull-right btn btn-default" data-toggle="modal" id="edit-profile" data-target="#edit_profile_modal"><i class="fa fa-pencil"></i></span></h3>
                <p class="margin-0">
                        {{ $doctor->address_one}}
                </p>
                @if($doctor->serviceList()->get()->count()>0)
                <p class="margin-0">
                        @foreach($doctor->serviceList as $service)
                        <br>{{ $service->name }}
                        @endforeach 
                        {{ $doctor->experience}} Year Exp.
                </p>
                @endif
                <p class="margin-0">
                        <label><b>Consulting Fee </b></label> {{ $doctor->consultation_fee}}
                </p>
                @for ($i=1; $i <= 5 ; $i++)
                    <span class="glyphicon glyphicon-star{{ ($i <= $doctor->rating_cache) ? '' : '-empty'}}"></span>
                @endfor
                {{ $doctor->rating_count}} Reviews <br>
				{!! $doctor->status?'<span class="label label-success">Approved</span>':'<span class="label label-danger">Dissapproved</span>' !!}
            </div>
            <div class="col-lg-2 col-sm-4 follow-info text-right pull-right">
                    @role(['administrator','super-admin','manager'])
					<button class="btn btn-info  btn-sm" data-toggle="modal" data-target="#profile_accept_model">Accept</button>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#profile_declined_modal">Reject</button>
					@endrole
            </div>
			<div class="row" style="float: left;width: 100%;padding: 0px;">
			<div class="col-xs-12">
					<hr>
					<div class="col-lg-4 col-sm-4 follow-info">
							<h3>Speciality <span class="pull-right btn btn-default"  data-toggle="modal" data-target="#add_specialty_modal"><i class="fa fa-plus"></i></span></h3>
							<hr>
							<ul class="nav nav-stacked custome-speciality">
								@foreach($doctor->specialties as $specialty) 
								<li>
									<span>{{ $specialty->specialty->name }} <i class="pull-right fa fa-minus-circle btn btn-danger delete-specialty" rel="{{$specialty->id}}" data-toggle="modal" data-target="#confirm_delete_specialty"></i></span>
								</li>
								@endforeach
							</ul>
					</div>
					<p>
							</p><div class="col-lg-4 col-sm-4 follow-info">
									<h3>Services <span class="pull-right btn btn-default"  data-toggle="modal" data-target="#add_modal"><i class="fa fa-plus"></i></span></h3>
									<hr>
									<ul class="nav nav-stacked custome-speciality">
										@foreach($doctor->services as $service)    
										<li>
											<span>{{ $service->service->name }} <i class="pull-right fa fa-minus-circle btn btn-danger delete-service" rel="{{$service->id}}" data-toggle="modal" data-target="#confirm_delete_service"></i></span>
										</li>
										@endforeach
									</ul>
							</div>
			</div>
			</div>
			<div class="row" style="float: left;width: 100%;padding: 0px;">
				<div class="col-xs-12">
					<hr>
					<div class="col-lg-4 col-sm-4 follow-info">
						<h3>Timings</h3>
						<section class="demos" >
							<article class="schedule-artcle">
							{!! Helper::widgetTimingsDoctor($doctor) !!}
							</article>
						</section>
					</div>					
				</div>
			</div>

        </div>
        </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
       <section class="panel">
             <header class="panel-heading tab-bg-info">
                 <ul class="nav nav-tabs my-tabs-01">
                     <li class="active">
                         <a data-toggle="tab" href="#recent-activity">
                             Review & Rating
                         </a>
                     </li>
                     <li class="">
                         <a data-toggle="tab" href="#profile">
                             Associated Hospitals
                         </a>
                     </li>
                 </ul>
             </header>
             <div class="panel-body">
                 <div class="tab-content">
                     <div id="recent-activity" class="tab-pane active">
                         <div class="profile-activity">                                          
                             @if($doctor->reviews()->get()->count()>0)
                             @foreach($doctor->reviews as $review)
                             <div class="act-time">                                      
                                 <div class="activity-body act-in">
                                     <span class="arrow"></span>
									 <div class="col-xs-12 padding-0">
										<div class="col-md-1" style="padding:0px;">
											<a href="{{ url('admin/user-details/'.$review->user->id) }}" class="activity-img activity-img1">
												@if($review->user->image)
												 <img class="avatar" width="70" src="{{ $review->user->image }}">
												 @else
												 <img class="avatar" width="70" src="{{ asset('public/admin/img/user-placeholder.png') }}">
												 @endif
											</a>
										</div>
										<div class="col-md-10">
											<p class="attribution act-in" style="margin-bottom:0px;"><a href="{{ url('admin/user-details/'.$review->user->id) }}">{{ $review->user->user->name }}</a></p>
											<p style="margin-bottom:0px;">{{ $review->user->email }}</p>
										</div>
										<div class="clearfix" style="margin-bottom:10px;"></div>
										<p>@for ($i=1; $i <= 5 ; $i++)
                                            <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                                            @endfor
										</p>
										<b><strong>{{ $review->title }}</strong></b><br>
										<span>{{ $review->comment }}</span>
									</div>
                                 </div>
                             </div>
                             @endforeach
                             @else
                             <p>No review was found.</p>
                             @endif
                         </div>
                     </div>
                     <!-- profile -->
                     <div id="profile" class="tab-pane">
                       <section class="panel">
                        @if($doctor->hospitals()->get()->count()>0)
                        @foreach($doctor->hospitals as $hospital)
                        <div class="act-time">                                      
                            <div class="activity-body act-in">
                                <span class="arrow"></span>
								 <div class="col-xs-12 padding-0">
									<div class="col-md-1" style="padding:0px;">
										<a href="{{ url('admin/hospital-clinic-details/'.$hospital->id) }}" class="activity-img activity-img1">
											@if($hospital->image)
											<img class="avatar" width="70" src="{{ $hospital->image }}">
											@else
											<img class="avatar" width="70" src="{{ asset('public/images/med_icons/hospital.png') }}">
											@endif
										</a>
									</div>
									<div class="col-md-10">
										<p class="attribution act-in" style="margin-bottom:0px;"><a href="{{ url('admin/hospital-clinic-details/'.$hospital->id) }}">{{ $hospital->name }}</a></p>
										<p style="margin-bottom:0px;">{{ $hospital->address_one }}</p>
										<p class="margin-0">
											<label><b>Consulting Fee </b></label> {{ $hospital->consultation_fee}}
										</p>
									</div>
									<div class="clearfix" style="margin-bottom:10px;"></div>
									<p> 
										@for ($i=1; $i <= 5 ; $i++)
                                        <span class="glyphicon glyphicon-star{{ ($i <= $hospital->rating_cache) ? '' : '-empty'}}"></span>
										@endfor
										{{ $hospital->rating_count }} Reviews
									</p>
								</div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <p>No hospital was found.</p>
                        @endif
                       </section>
                         <section>
                             <div class="row">                                              
                             </div>
                         </section>
                     </div>
                 </div>
             </div>
         </section>
    </div>
 </div>
<!-- Confirm delete service Modal START -->
<div class="modal fade" id="confirm_delete_service" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/doctor/delete-services','method' => 'post','files' => true, 'id'=>'delete-services-form']) !!}
            {{ Form::hidden('id', $doctor->id,['id'=>'service_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Service</h4>
                </div>
                <div class="modal-body">
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-warning" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!--confirm delete modal END-->
<!-- Edit profile Modal -->
<div class="modal fade" id="edit_profile_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::model($doctor,['url' => 'admin/doctor/update','method' => 'post','class'=>'form-horizontal','id'=>'edit-profile-form','files' => true]) !!}
            {{ Form::hidden('id', null) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Basic Informations</h4>
            </div>
            <div class="modal-body">
                @if(session('message') && session('status')==0)
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon_close"></i>
                    </button>
                    {{ session('message') }}
                    @foreach ($errors->all() as $error)
                    <br>{{ $error }}
                    @endforeach
                </div> 
                @endif
                <div class="panel-body">
                    <div class="form-group">
                         <label class="control-label col-sm-4">Name</label>
                         <div class="col-sm-6">
                             {{ Form::text('name', null,['class'=>'form-control']) }}
                         </div>
                     </div>
                    <div class="form-group">
                         <label class="control-label col-sm-4">Gender</label>
                         <div class="col-sm-6">
                              Male {{ Form::radio('gender', 1, $doctor->gender==1?true:false) }}
                              Female {{ Form::radio('gender', 2, $doctor->gender==2?true:false) }}
                         </div>
                     </div>
                    <div class="form-group">
                         <label class="control-label col-sm-4">Mobile</label>
                         <div class="col-sm-6">
                               {{ Form::number('mobile', null,['class'=>'form-control']) }}
                         </div>
                     </div>
					 {{--<div class="form-group">
                        <label class="control-label col-sm-4">Degree</label>
                        <div class="col-sm-6">
                            {{ Form::select('qualification', $qualifications,($doctor->qualifications()->get()->count()>0)?$doctor->qualifications()->get()->first()->id:null,['class'=>'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">College</label>
                        <div class="col-sm-6">
                            {{ Form::text('college', ($doctor->colleges()->get()->count()>0)?$doctor->colleges()->get()->first()->college:null,['class'=>'form-control']) }}
                        </div>
					 </div> --}}
                    <div class="form-group">
                        <label class="control-label col-sm-4"> Passing Year</label>
                        <div class="col-sm-6">
                            {{ Form::date('year', \Carbon\Carbon::now(),['class'=>'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4"> Experience</label>
                        <div class="col-sm-6">
                            {{ Form::text('experience', null,['class'=>'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4"> Recognition</label>
                        <div class="col-sm-6">
                            {{ Form::text ('recognition', null,['class'=>'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4"> About</label>
                        <div class="col-sm-6">
                            {{ Form::textarea ('about', null,['class'=>'form-control','rows'=>3]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Address</label>
                        <div class="col-sm-6">
                            {{ Form::text('address_one', null,['class'=>'geocomplete form-control']) }}
                            {{ Form::hidden('latitude', null,['data-geo'=>'lat']) }}
                            {{ Form::hidden('longitude', null,['data-geo'=>'lng']) }}
                            {{ Form::hidden('locality', null,['data-geo'=>'locality']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">City</label>
                        <div class="col-sm-6">
                            {{ Form::text('city_name', null,['class'=>'form-control','data-geo'=>'locality']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">State</label>
                        <div class="col-sm-6">
                            {{ Form::text('state_name', null,['class'=>'form-control','data-geo'=>'administrative_area_level_1']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Country</label>
                        <div class="col-sm-6">
                            {{ Form::text('country_name', null,['class'=>'form-control','data-geo'=>'country_short']) }}
                            {{ Form::hidden('pincode', null,['data-geo'=>'postal_code']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Website</label>
                        <div class="col-sm-6">
                            {{ Form::text('website', null,['class'=>'form-control']) }}
                        </div>
                    </div>
                 </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-info  btn-sm" type="submit">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!--Edit  modal save-->
<!-- Add Modal -->
<div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/doctor/add-services','method' => 'post','class'=>'form-horizontal','files' => true]) !!}
            {{ Form::hidden('id', $doctor->id) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Service</h4>
            </div>
            <div class="modal-body">
                @if(session('message') && session('status')==0))
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon_close"></i>
                    </button>
                    {{ session('message') }}
                </div> 
                @endif
                <div class="form-group ">
                    <label for="service" class="control-label col-lg-2">Service <span class="required">*</span></label>
                    <div class="col-sm-6">
                        {{ Form::select('service_id[]', $services, null, ['class' => 'form-control m-bot15 ddlCars','multiple'=>true]) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-info  btn-sm" type="submit">Save changes</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Add specialty model-->
<div class="modal fade" id="add_specialty_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/doctor/add-specialties','method' => 'post','class'=>'form-horizontal','files' => true]) !!}
            {{ Form::hidden('id', $doctor->id) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Specialty</h4>
            </div>
            <div class="modal-body">
                @if(session('message') && session('status')==0)
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon_close"></i>
                    </button>
                    <strong>Oh snap!</strong> {{ session('message') }}
                </div> 
                @endif
                <div class="form-group ">
                    <label for="specialty" class="control-label col-lg-2">Specialty <span class="required">*</span></label>
                    <div class="col-sm-6">
                        {{ Form::select('specialty_id[]', $specialties, null, ['class' => 'form-control m-bot15 ddlCars', 'multiple'=>'multiple']) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-info  btn-sm" type="submit">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Confirm delete specialty Modal START -->
<div class="modal fade" id="confirm_delete_specialty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/doctor/delete-specialty','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', null,['id'=>'service_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Specialty</h4>
                </div>
                <div class="modal-body">
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-warning" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Profile accept model -->
<div class="modal fade" id="profile_accept_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/doctor/profile-accept','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $doctor->id,['id'=>'doctor_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Accept Profile</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0))
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Profile declined model -->
<div class="modal fade" id="profile_declined_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/doctor/profile-declined','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $doctor->id,['id'=>'doctor_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Decline Profile</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0))
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Delete profile picture -->
<div class="modal fade" id="delete_pic_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/doctor/delete-image','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $doctor->id,['id'=>'doctor_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Image</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Upload profile picture -->
<div class="modal fade" id="upload_pic_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/doctor/upload-image','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $doctor->id,['id'=>'doctor_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Upload Profile Picture</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                    <div class="form-group">
                        <label class="control-label col-sm-4">Image</label>
                        <div class="col-sm-5">
                            <input type ="file" id="image" name="image"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Save</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC00fGCV5rT-LH9xZZVwjnTn39jW64URLM&amp;libraries=places"></script>
<script src="{{ asset('public/js/jquery.geocomplete.min.js') }}"></script>
<script type="text/javascript">
/**
 * Note
 * type = 1 for add service
 * type = 2 delete services
 * type = 3 add specialties
 * type = 4 approve profile
 * type = 5 declined profile
 * type = 6 upload image
 * type = 7 delete image
 */
$(document).ready(function(){
    $(".geocomplete").geocomplete({
        details: ".details",
        detailsAttribute: "data-geo"
    });
    var popup = "{{ session('status') }}";
    var type = "{{ session('type') }}";
    if(popup && type==1){
        $('#add_modal').modal('show');
    }
    if(popup && type==2){
        $('#confirm_delete_service').modal('show');
    }
    if(popup && type==3){
        $('#add_specialty_modal').modal('show');
    }
    if(popup && type==4){
        $('#confirm_delete_specialtyl').modal('show');
    }
    if(popup && type==5){
        $('#profile_declined_modal').modal('show');
    }
    if(popup && type==6){
        $('#profile_accept_model').modal('show');
    }
    if(popup && type==7){
        $('#upload_pic_model').modal('show');
    }
    if(popup && type==8){
        $('#delete_pic_model').modal('show');
    }
    if(popup && type==9){
        $('#edit_profile_modal').modal('show');
    }
    $('.delete-service').click(function(){
        var id = $(this).attr('rel');
        $("#delete-services-form input[name=id]").val(id);
    });
    $('.delete-specialty').click(function(){ 
        var id = $(this).attr('rel');
        $("#delete-specialty-form input[name=id]").val(id);
    });
    $('#edit-profile').click(function(){ 
        var id = $(this).attr('rel');
        $("#edit-profile-form input[name=id]").val(id);
    });
    $('.ddlCars').multiselect();
})
</script>
@endsection
