@extends('admin::template')

@section('admin::content')
<link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css'/>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa fa-bars"></i> Manage Entities</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-bars"></i>Doctor</li>
            <li><i class="fa fa-square-o"></i>Create</li>
        </ol>
    </div>
</div>
@if(session('message'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="first-section ptb">
    <div class="col-xs-12" style="padding:0px;">
		<section class="panel">
			<div class="panel-body">
            {{ Form::open(array('url' =>'admin/doctor/save', 'method'=>'post','class'=>'form-horizontal', 'files' => true)) }}
                {{ csrf_field() }}  
                {{ Form::hidden('id', null) }} 
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-md-4 col-xs-12">
                                <label class="pull-right" style="margin-top:8px;font-weight:bold;">Name</label>
                        </div>
                        <div class="col-md-6 col-xs-12">
                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                        </div>
                </div>
                <div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Address</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        {{ Form::text('address_one', null, ['class'=>'form-control location-input geocomplete', 'placeholder'=>'Location']) }}
                        <div class="location-details">
                            <input type="hidden" value="" name="latitude" data-geo="lat">
                            <input type="hidden" value="" name="longitude" data-geo="lng">
                            <input type="hidden" value="" name="locality" data-geo="locality">
                            <input type="hidden" value="" name="city_name" data-geo="locality">
                            <input type="hidden" value="" name="state_name" data-geo="administrative_area_level_1">
                            <input type="hidden" value="" name="country_name" data-geo="country">
                            <input type="hidden" value="" name="pincode" data-geo="postal_code">
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Mobile Number</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        {{ Form::number('mobile', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)']) }}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                        <label class="pull-right" style="font-weight:bold;">Gender</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                         {{ Form::radio('gender', 1)}} Male &nbsp;&nbsp;&nbsp;&nbsp; {{Form::radio('gender', 2)}}  Female
                    </div>
                </div>
                <div class="form-group{{ $errors->has('degree') ||$errors->has('college') || $errors->has('year') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                        <label class="pull-right" style="margin-top:8px;font-weight:bold;">Qualification</label>
                    </div>
                    <div class="padding-0 col-md-8 padding-0 repeat-quali" style="position:relative;padding:0px;">
                            <div class="col-md-3 col-xs-12">
                                    <div id="content" class="clear">
                                            {{ Form::select("degree[]", $qualifications, null, ['class' => 'form-control']) }}
                                     </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                    {{ Form::select("college[]", $colleges, null, ['class' => 'form-control']) }}
                            </div>
                            <div class="col-md-3 col-xs-12">
                                    {{ Form::select("year[]",$years, null, ['class' => 'form-control']) }}
                            </div>
                            <div class="col-md-2">
                                    <i class="cur fa fa-plus btn btn-info btn-xs add-quali" style="margin-top:10px;"></i>
                            </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Speciality</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="clear" id="content1">
                            {{ Form::select('specialties[]', $specialties,null, ['class' => 'form-control m-bot15 ddlCars','multiple'=>true]) }}
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('services') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Services</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="clear" id="content1">
                            {{ Form::select('services[]', $specialties,null, ['class' => 'form-control m-bot15 ddlCars','multiple'=>true]) }}
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('reg_no') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Registration Number</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                         {{ Form::text('reg_no', null, ['class'=>'form-control']) }}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('awards') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Award</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                         {{ Form::text('awards', null, ['class'=>'form-control']) }}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('recognition') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Recognition</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                         {{ Form::text('recognition', null, ['class'=>'form-control']) }}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Experience</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                         {{ Form::text('experience', null, ['class'=>'form-control']) }}
                    </div>
                </div>
                                                <div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                        <label class="pull-right" style="margin-top:8px;font-weight:bold;">Upload Photo (optional)</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                                                                {{ Form::file('image',['class'=>'form-control','onkeypress'=>'return isNumber(event)']) }}
                    </div>
                </div>
                                                <hr>
                <div class="form-group">
                    <div class="col-md-4 col-xs-12">

                    </div>							
                    <div class="col-md-6 col-xs-12">
                            <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Save" />
                    </div>							
                </div>
            {{ Form::close() }}
        </div>
		</section>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC00fGCV5rT-LH9xZZVwjnTn39jW64URLM&amp;libraries=places"></script>
<script src="{{ asset('public/js/jquery.geocomplete.min.js') }}"></script>
<script type="text/javascript">
    $(".geocomplete").geocomplete({
        details: ".details",
        detailsAttribute: "data-geo"
    });
    var x = 1;
    $('.add-quali').click(function(){
        var append_data = '<div class="rp-container"><div class="clearfix" style="float:left;width:100%;margin-top:2px;"></div><div class="col-md-3 col-xs-12"><div id="content" class="clear">{{ Form::select("degree[]", $qualifications, null, ["class" => "2col active form-control"]) }}</div></div><div class="col-md-3 col-xs-12">{{ Form::select("college[]", $colleges, null, ["class"=>"form-control"]) }}</div><div class="col-md-3 col-xs-12">{{ Form::select("year[]",$years, null, ["class"=>"form-control"]) }}</div><div class="col-md-2"><i class="cur fa fa-minus btn btn-danger btn-xs remove-repeat" style="margin-top:10px;"></i></div></div>';
        x++;
        $('.repeat-quali').append(append_data);
    })
    $('.repeat-quali').on('click','.remove-repeat',function(){
        $(this).parents('.rp-container').remove(); x--;
    })
    $('.d-ul-left li:nth-child(2)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(2)').addClass('active');	
    $('.ddlCars').multiselect();
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
        }
        return true;
    }
</script>
@endsection