@extends('admin::template')

@section('admin::content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa fa-bars"></i> Staffs</h3>
        <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                <li><i class="fa fa-bars"></i>Staffs</li>
        </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            <strong>{{ session('message') }}</strong> 
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            <strong> {{ session('message') }}</strong>
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
			<div class="clearfix"></div>
				<div class="col-xs-12" style="margin-top:10px;"><a href="javascript:;" data-toggle="modal" data-target="#add_staff_model" class="pull-right btn btn-outline btn-info btn-xs"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Add Staff</a></div>
		
          <!-- <h1 title="Add new staff"><span class="pull-left btn btn-default" data-toggle="modal" data-target="#add_staff_model"><i class="icon_plus-box"></i></span></h1>-->
           <table class="table table-striped table-advance table-hover" id="myTable">
             
                <thead>
                <tr>
                   <th><i class="icon_profile"></i> Name</th>
                   <th><i class="icon_mail_alt"></i> Email</th>
                   <th><i class=""></i> Status</th>
                   <th><i class=""></i> Account Acess</th>
                   <th><i class="icon_calendar"></i> Registration Date</th>
                   <th><i class="icon_cogs"></i> Action</th>
                </tr>
                </thead>
                <tbody>
                @if($staffs->count()>0)
                @foreach($staffs as $staff) 
                <tr>
                   <td>{{ $staff->user->name }}</td>
                   <td>{{ $staff->email }}</td>
                   <td>{!! $staff->status?'<span class="label label-success">Active</span>':'<span class="label label-danger">Inactive</span>' !!}</td>
                   <td>
                       @foreach($staff->roles as $role)
                       {{ $role->display_name  }}
                       @endforeach
                   </td>
                   <td>{{ $staff->created_at }}</td>
                   <td>
                    <div class="btn-group">
                        <span rel="{{ $staff->id }}" class="btn btn-default edit-profile"  data-toggle="modal" data-target="#edit_staff_model"><i class="icon_pencil-edit"></i></span>
                        <a href="{{ url('admin/staff/delete') }}/{{ $staff->id }}" onclick="return confirm('Are you sure you want to delete ?')"><span class="glyphicon glyphicon-trash"></span></a>
                    </div>
                    </td>
                </tr>
                @endforeach
                @endif
             </tbody>
          </table>
        </section>
    </div>
</div>
<!-- Add staff model -->
<div class="modal fade" id="add_staff_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/staff/add','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', null,['id'=>'id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Add Staff Account</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong> {{ session('message') }}</strong>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div> 
                    @endif
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
							{{ Form::text('name', null, ['class'=>'form-control']) }}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
							{{ Form::text('email', null, ['class'=>'form-control']) }}
                        </div>
                        <div class="form-group ">
                            <label for="exampleInputPassword1">Password</label>
							{{ Form::password('password',['class'=>'form-control']) }}
                        </div>
                        <div class="form-group ">
                            <label for="exampleInputPassword1">Role</label>
                            {{ Form::select('role', $roles->toArray(), 'S',['class'=>'form-control input-sm m-bot15']) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Save</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- edit staff model -->
<div class="modal fade" id="edit_staff_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/staff-update','method' => 'post','files' => true, 'id'=>'edit-profile-form']) !!}
            {{ Form::hidden('id', null,['id'=>'id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Staff Details</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong> {{ session('message') }}</strong>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div> 
                    @endif
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
							{{ Form::text('name', null, ['class'=>'form-control']) }}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
							{{ Form::text('email', null, ['class'=>'form-control']) }}
                        </div>
                        <div class="form-group ">
                            <label for="exampleInputPassword1">Password</label>
							{{ Form::password('password',['class'=>'form-control']) }}
                        </div>
                        <div class="form-group ">
                            <label for="exampleInputPassword1">Role</label>
                            {{ Form::select('role', $roles->toArray(), 'S',['class'=>'form-control input-sm m-bot15']) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Save</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
/**
 * Note
 * type = 1 Add staff account
 * type = 2 Edit staff account
 */
$(document).ready(function(){
    var popup = "{{ session('status') }}";
    var type = "{{ session('type') }}";
	var staffId = "{{ session('staff_id') }}";
    if(popup && type==1){
        $('#add_staff_model').modal('show');
    }
	if(popup && type==2 && staffId){
        $('#edit_staff_model').modal('show');
		var csrf_token = " {{csrf_token()}} ";
        var url = "{{url('admin/staff-edit')}}";
        var data = {_token: csrf_token, id: staffId};
        $.ajax({
            url: url,
            data: data,
            method: 'get',
            success: function (data, status, xhr) {					
				var result = data.staff;
				$("#edit-profile-form input[name=id]").val(data['id']);
                $("#edit-profile-form input[name=name]").val(data['name']);
                $("#edit-profile-form input[name=email]").val(data['email']);
                $("#edit-profile-form select[name=role]").val(data['role'][0].id);
            },
            failure: function (status) {
                console.log(status);
            }
        });
    }
    $('.edit-profile').click(function(){
        var csrf_token = " {{csrf_token()}} ";
        var url = "{{url('admin/staff-edit')}}";
		var staffId = $(this).attr('rel');
        var data = {_token: csrf_token, id: staffId};
        $.ajax({
            url: url,
            data: data,
            method: 'get',
            success: function (data, status, xhr) {					
				var result = data.staff;
				$("#edit-profile-form input[name=id]").val(data['id']);
                $("#edit-profile-form input[name=name]").val(data['name']);
                $("#edit-profile-form input[name=email]").val(data['email']);
                $("#edit-profile-form select[name=role]").val(data['role'][0].id);
            },
            failure: function (status) {
                console.log(status);
            }
        });
    });
    /***
     * Initialize Data Table.
     */
    $('#myTable').DataTable();
})

</script>
@endsection