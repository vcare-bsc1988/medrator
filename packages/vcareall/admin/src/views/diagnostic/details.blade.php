@extends('admin::template')

@section('admin::content')
<style type="text/css">
.pac-container{z-index:1100000;}
</style>
<link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css'/>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>
<div class="row">
    <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa fa-bars"></i> Diagnostic Details</h3>
            <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                    <li><i class="fa fa-bars"></i>Diagnostic</li>
                    <li><i class="fa fa-square-o"></i>Details</li>
            </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <!-- profile-widget -->
    <div class="col-lg-12">
        <section class="panel">
        <div class="profile-widget" style="background:white;color:#000;">
          <div class="panel-body">
            <div class="col-lg-2 col-sm-2">
              <!-- <h4>Jenifer Smith</h4>   -->  
              <style>
                  .follow-ava{width:82px;height:82px;overflow:hidden;position:relative;}
                  .btn-hides{position: absolute;margin-top:0px;}
                  .follow-ava:hover .btn-hides{margin-top:-40px;transition-duration: 1s;}
                  
              </style>
              <div class="follow-ava">
                  <img src="{{ $diagnostic->image }}" width="100">
                  <p class="btn-hides">
                  <button class="btn btn-danger" data-toggle="modal" data-target="#delete_pic_model"><i class="fa fa-trash"></i></button>
                   <button class="btn btn-default"  data-toggle="modal" data-target="#upload_pic_modal"><i class="fa fa-pencil"></i></button>
                  </p>
              </div>
              <!-- <h6>Administrator</h6> -->
            </div>
            <div class="col-lg-6 col-sm-6 follow-info">
                <h3 class="margin-0">{{ $diagnostic->name}}<span class="pull-right btn btn-default" data-toggle="modal" data-target="#edit_profile_modal"><i class="fa fa-pencil"></i></span></h3>
                <p class="margin-0">
                        {{ $diagnostic->address_one}}
                </p>
                @if($diagnostic->tests()->get()->count()>0)
                <p class="margin-0">
                        @foreach($diagnostic->tests as $test)
                        <br>{{ $test->name }}
                        @endforeach 
                </p>
                @endif
                <p class="margin-0">
                        <label><b>Consulting Fee </b></label> {{ $diagnostic->consultation_fee}}
                </p>
                @for ($i=1; $i <= 5 ; $i++)
                    <span class="glyphicon glyphicon-star{{ ($i <= $diagnostic->rating_cache) ? '' : '-empty'}}"></span>
                @endfor
                {{ $diagnostic->rating_count}} Reviews <br>
				{!! $diagnostic->status?'<span class="label label-success">Approved</span>':'<span class="label label-danger">Dissapproved</span>' !!}
            </div>
            <div class="col-lg-2 col-sm-4 follow-info text-right pull-right">
				@role(['administrator','super-admin','manager'])
                    <button class="btn btn-info  btn-sm" data-toggle="modal" data-target="#profile_accept_model">Accept</button>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#profile_declined_modal">Reject</button>
				@endrole
            </div>
                                <div class="row" style="float: left;width: 100%;padding: 0px;">
                                <div class="col-xs-12">
                                        <hr>
                                        <div class="col-lg-4 col-sm-4 follow-info">
                                                <h3>Diagnostic Tests <span class="pull-right btn btn-default"  data-toggle="modal" data-target="#add_test_modal"><i class="fa fa-plus"></i></span></h3>
                                                <hr>
                                                <ul class="nav nav-stacked custome-speciality">
                                                        <ul class="nav nav-stacked custome-speciality">
                                                            @foreach($diagnostic->tests as $test)
                                                            <li>
                                                                <span>{{ $test->name }} <i class="pull-right fa fa-minus-circle btn btn-danger delete-test" rel="{{$test->id}}" data-toggle="modal" data-target="#confirm_delete_test"></i></span>
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                </ul>
                                        </div>
                                        <p>
                                                </p>
                                        <p></p>
                                </div>
                                </div>
        </div>
        </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
       <section class="panel">
             <header class="panel-heading tab-bg-info">
                 <ul class="nav nav-tabs my-tabs-01">
                     <li class="active">
                         <a data-toggle="tab" href="#recent-activity">
                             Review & Rating
                         </a>
                     </li>
                 </ul>
             </header>
             <div class="panel-body">
                 <div class="tab-content">
                     <div id="recent-activity" class="tab-pane active">
                         <div class="profile-activity">                                          
                             @if($diagnostic->reviews()->get()->count()>0)
                             @foreach($diagnostic->reviews as $review)
                             <div class="act-time">                                      
                                 <div class="activity-body act-in">
                                     <span class="arrow"></span>
									 <div class="col-xs-12 padding-0">
										<div class="col-md-1" style="padding:0px;">
											<a href="{{ url('admin/user-details/'.$review->user->id) }}" class="activity-img activity-img1">
												@if($review->user->image)
												 <img class="avatar" width="70" src="{{ $review->user->image }}">
												 @else
												 <img class="avatar" width="70" src="{{ asset('public/admin/img/user-placeholder.png') }}">
												 @endif
											</a>
										</div>
										<div class="col-md-10">
											<p class="attribution act-in" style="margin-bottom:0px;"><a href="{{ url('admin/user-details/'.$review->user->id) }}">{{ $review->user->user->name }}</a></p>
											<p style="margin-bottom:0px;">{{ $review->user->email }}</p>
										</div>
										<div class="clearfix" style="margin-bottom:10px;"></div>
										<p>@for ($i=1; $i <= 5 ; $i++)
                                            <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                                            @endfor
										</p>
										<b><strong>{{ $review->title }}</strong></b><br>
										<span>{{ $review->comment }}</span>
									</div>
                                 </div>
                             </div>
                             @endforeach
                             @else
                             <p>No review was found.</p>
                             @endif
                         </div>
                     </div>
                 </div>
             </div>
         </section>
    </div>
 </div>
<!-- Edit profile Modal -->
<div class="modal fade" id="edit_profile_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content details">
            {!! Form::model($diagnostic,['url' => 'admin/diagnostic/update','method' => 'post','class'=>'form-horizontal','files' => true]) !!}
            {{ Form::hidden('id', null) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Update Basic Informations</h4>
            </div>
            <div class="modal-body">
                @if(session('message') && session('status')==0)
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon_close"></i>
                    </button>
                    <strong>Oh snap!</strong> {{ session('message') }}
                    @foreach ($errors->all() as $error)
                    <br>{{ $error }}
                    @endforeach
                </div> 
                @endif
                <div class="panel-body">
                    <div class="form-group">
                         <label class="control-label col-sm-4">Name</label>
                         <div class="col-sm-6">
                             {{ Form::text('name', null,['class'=>'form-control']) }}
                         </div>
                     </div>
                    <div class="form-group">
                         <label class="control-label col-sm-4">Phone</label>
                         <div class="col-sm-6">
                               {{ Form::number('phone', null,['class'=>'form-control']) }}
                         </div>
                     </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4"> Home Collection Facility</label>
                        <div class="col-sm-6">
                            {{ Form::checkbox('home_collection_facility', 1) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4"> Online Bookings</label>
                        <div class="col-sm-6">
                            {{ Form::checkbox('online_bookings', 1) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4"> Online Reports</label>
                        <div class="col-sm-6">
                            {{ Form::checkbox ('online_reports', 1) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Address</label>
                        <div class="col-sm-6">
                            {{ Form::text('address_one', null,['class'=>'geocomplete form-control']) }}
                            {{ Form::hidden('latitude', null,['data-geo'=>'lat']) }}
                            {{ Form::hidden('longitude', null,['data-geo'=>'lng']) }}
                            {{ Form::hidden('locality', null,['data-geo'=>'locality']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">City</label>
                        <div class="col-sm-6">
                            {{ Form::text('city_name', null,['class'=>'form-control','data-geo'=>'locality']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">State</label>
                        <div class="col-sm-6">
                            {{ Form::text('state_name', null,['class'=>'form-control','data-geo'=>'administrative_area_level_1']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Country</label>
                        <div class="col-sm-6">
                            {{ Form::text('country_name', null,['class'=>'form-control','data-geo'=>'country']) }}
                            {{ Form::hidden('pincode', null,['data-geo'=>'postal_code']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">Website</label>
                        <div class="col-sm-6">
                            {{ Form::text('website', null,['class'=>'form-control']) }}
                        </div>
                    </div>
                 </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-info  btn-sm" type="submit">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Add test model-->
<div class="modal fade" id="add_test_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/diagnostic/add-tests','method' => 'post','files' => true]) !!}
            {{ Form::hidden('id', $diagnostic->id) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Tests</h4>
            </div>
            <div class="modal-body">
                @if(session('message') && session('status')==0)
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="icon_close"></i>
                    </button>
                    {{ session('message') }}
                </div> 
                @endif
                <div class="form-group ">
                    <label for="specialty" class="control-label col-lg-2">Specialty <span class="required">*</span></label>
                    <div class="col-sm-6">
                        {{ Form::select('test_id[]', $tests, null, ['class' => 'form-control m-bot15 ddlCars', 'multiple'=>'multiple']) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-info  btn-sm" type="submit">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Confirm delete diagnostic Modal START -->
<div class="modal fade" id="confirm_delete_test" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/diagnostic/delete-test','method' => 'post','files' => true, 'id'=>'delete-test-form']) !!}
            {{ Form::hidden('id', null,['id'=>'test_id']) }}
			{{ Form::hidden('diagnostic_id', $diagnostic->id) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Specialty</h4>
                </div>
                <div class="modal-body">
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-warning" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Profile accept model -->
<div class="modal fade" id="profile_accept_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/diagnostic/profile-accept','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $diagnostic->id,['id'=>'doctor_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Accept Profile</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Profile declined model -->
<div class="modal fade" id="profile_declined_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/diagnostic/profile-declined','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $diagnostic->id) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Decline Profile</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0))
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Delete profile picture -->
<div class="modal fade" id="delete_pic_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/diagnostic/delete-image','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $diagnostic->id,['id'=>'diagnostic_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete Image</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                        Are you sure ?
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Confirm</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<!-- Upload profile picture -->
<div class="modal fade" id="upload_pic_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'admin/diagnostic/upload-image','method' => 'post','files' => true, 'id'=>'delete-specialty-form']) !!}
            {{ Form::hidden('id', $diagnostic->id,['id'=>'doctor_id']) }}
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Upload Profile Picture</h4>
                </div>
                <div class="modal-body">
                    @if(session('message') && session('status')==0)
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon_close"></i>
                        </button>
                        <strong>Oh snap!</strong> {{ session('message') }}
                    </div> 
                    @endif
                    <div class="form-group">
                        <label class="control-label col-sm-4">Image</label>
                        <div class="col-sm-5">
                            <input type ="file" id="image" name="image"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-info  btn-sm" type="submit"> Save</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC00fGCV5rT-LH9xZZVwjnTn39jW64URLM&amp;libraries=places"></script>
<script src="{{ asset('public/js/jquery.geocomplete.min.js') }}"></script>
<script type="text/javascript">
/**
 * Note
 * type = 3 add specialties
 * type = 4 approve profile
 * type = 5 declined profile
 * type = 6 upload image
 * type = 7 delete image
 */
$(document).ready(function(){
    $(".geocomplete").geocomplete({
        details: ".details",
        detailsAttribute: "data-geo"
    });
    var popup = "{{ session('status') }}";
    var type = "{{ session('type') }}";
    if(popup && type==1){
        $('#add_modal').modal('show');
    }
    if(popup && type==3){
        $('#add_test_modal').modal('show');
    }
    if(popup && type==4){
        $('#confirm_delete_test').modal('show');
    }
    if(popup && type==5){
        $('#profile_declined_modal').modal('show');
    }
    if(popup && type==6){
        $('#profile_accept_model').modal('show');
    }
    if(popup && type==7){
        $('#upload_pic_model').modal('show');
    }
    if(popup && type==8){
        $('#delete_pic_model').modal('show');
    }
    if(popup && type==9){
        $('#edit_profile_modal').modal('show');
    }
    $('.delete-test').click(function(){
        var id = $(this).attr('rel');
        $("#delete-test-form input[name=id]").val(id);
    });
    $('#edit-profile').click(function(){
        var id = $(this).attr('rel');
        $("#delete-specialty-form input[name=id]").val(id);
    });
    $('.ddlCars').multiselect();
})
</script>
@endsection
