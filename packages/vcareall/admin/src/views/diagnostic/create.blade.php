@extends('admin::template')

@section('admin::content')
<link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css'/>
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa fa-bars"></i> Manage Entities</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-bars"></i>Diagnostic</li>
            <li><i class="fa fa-square-o"></i>Create</li>
        </ol>
    </div>
</div>
@if(session('message'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="first-section ptb">
    <div class="col-xs-12" style="padding:0px;">
		<section class="panel">
			<div class="panel-body">
            {{ Form::open(array('url' =>'admin/diagnostic/save','class'=>'form-horizontal', 'files' => true)) }}
                {{ csrf_field() }}  
                {{ Form::hidden('id', null) }} 
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-md-4 col-xs-12">
                                <label class="pull-right" style="margin-top:8px;font-weight:bold;">Name</label>
                        </div>
                        <div class="col-md-6 col-xs-12">
                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                        </div>
                </div>
                <div class="form-group{{ $errors->has('address_one') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Address</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        {{ Form::text('address_one', null, ['class'=>'form-control location-input geocomplete', 'placeholder'=>'Location']) }}
                        <div class="location-details">
                            <input type="hidden" value="" name="latitude" data-geo="lat">
                            <input type="hidden" value="" name="longitude" data-geo="lng">
                            <input type="hidden" value="" name="locality" data-geo="locality">
                            <input type="hidden" value="" name="city_name" data-geo="locality">
                            <input type="hidden" value="" name="state_name" data-geo="administrative_area_level_1">
                            <input type="hidden" value="" name="country_name" data-geo="country">
                            <input type="hidden" value="" name="pincode" data-geo="postal_code">
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Phone Number</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        {{ Form::number('phone', null, ['class'=>'form-control','onkeypress'=>'return isNumber(event)']) }}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('home_collection_facility') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                        <label class="pull-right" style="font-weight:bold;">Home Collection Facility</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                                                                {{ Form::checkbox('home_collection_facility', 1,null,['class'=>'squaredFives']) }}
                    </div>
                </div>
                                                <div class="form-group{{ $errors->has('online_bookings') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                        <label class="pull-right" style="font-weight:bold;">Online Bookings</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                                                                {{ Form::checkbox('online_bookings', 1,null,['class'=>'squaredFives']) }}
                    </div>
                </div>
                                                <div class="form-group{{ $errors->has('online_reports') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                        <label class="pull-right" style="font-weight:bold;">Online Reports</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                                                                {{ Form::checkbox('online_reports', 1,null,['class'=>'squaredFives']) }}
                    </div>
                </div>
                                                <div class="form-group{{ $errors->has('specialties') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                            <label class="pull-right" style="margin-top:8px;font-weight:bold;">Tests</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                                                                <div class="clear" id="content1">
                                                                        {{ Form::select('tests[]', $tests,null, ['class' => 'form-control m-bot15 ddlCars','multiple'=>true]) }}
                                                                </div>
                    </div>
                </div>
                                                <div class="form-group{{ $errors->has('cashless_mediclaim') ? ' has-error' : '' }}">
                    <div class="col-md-4 col-xs-12">
                        <label class="pull-right" style="margin-top:8px;font-weight:bold;">Upload Photo (optional)</label>
                    </div>
                    <div class="col-md-6 col-xs-12">
                                                                {{ Form::file('image',['class'=>'form-control']) }}
                    </div>
                </div>
                                                <hr>
                <div class="form-group">
                    <div class="col-md-4 col-xs-12">

                    </div>							
                    <div class="col-md-6 col-xs-12">
                            <input type="submit" class="btn btn-info my-btn-0 btn-lg radius-0" value="Save" />
                    </div>							
                </div>
            {{ Form::close() }}
        </div>
		</section>
    </div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC00fGCV5rT-LH9xZZVwjnTn39jW64URLM&amp;libraries=places"></script>
<script src="{{ asset('public/js/jquery.geocomplete.min.js') }}"></script>
<script type="text/javascript">
    $(".geocomplete").geocomplete({
        details: ".details",
        detailsAttribute: "data-geo"
    });
    $('.d-ul-left li:nth-child(2)').siblings().removeClass('active');
    $('.d-ul-left li:nth-child(2)').addClass('active');	
    $('.ddlCars').multiselect();
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
        }
        return true;
    }
</script>
@endsection