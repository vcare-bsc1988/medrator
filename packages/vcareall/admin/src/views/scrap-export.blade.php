<!DOCTYPE html>
<html>
<head></head>
<body>
<table>
	<tr>
		<th>Name</th>
		<th>Experience</th>
		<!--<th>Galleries</th>-->
		<th>Locality</th>
		<th>Latitude</th>
		<th>Longitude</th>
		<th>CosultingFee</th>
		<th>Shedules</th>
		<!--<th>Review Count</th> -->
		<th>About</th>
		<!--<th>Doctor Votes</th> -->
		<!--<th>Verification Level</th>-->
		<!--<th>Reviews</th> -->
		<th>Services</th>
		<th>specialties</th>
		<th>qualifications</th>
		<th>awards</th>
		<!--<th>memberships</th>-->
		<!--<th>registrations</th> -->
		<th>clinics</th>
	</tr>
	@foreach($doctors as $doctor)
	<tr>
		<td>{{ @$doctor['doctorname'] }}</td> 
		<td>{{ (isset($doctor['experience'])?$doctor['experience']:'Not Updated') }}</td>
		{{--<td>{{ @$doctor['galleries'] }}</td>--}}
		<td>{{ @$doctor['locality'] }}</td>	
		<td>{{ @$doctor['latitude'] }}</td>	
		<td>{{ @$doctor['longitude'] }}</td>			
		<td>{{ @$doctor['cosulting_fee'] }}</td>
		<td>{{ @$doctor['shedules'] }}</td>
		{{--<td>{{ @$doctor['review_count'] }}</td>--}}
		<td>{{ @$doctor['about'] }}</td>
		{{--<td>{{ @$doctor['doctor-votes'] }}</td>--}}
		{{--<td>{{ @$doctor['verification-level'] }}</td>--}}
		{{--<td>{{ @$doctor['reviews'] }}</td>--}}
		<td>{{ @$doctor['services'] }}</td>
		<td>{{ @$doctor['specialties'] }}</td>
		<td>{{ @$doctor['qualifications'] }}</td>
		<td>{{ @$doctor['awards'] }}</td>
		{{--<td>{{ @$doctor['memberships'] }}</td>--}}
		{{--<td>{{ @$doctor['registrations'] }}</td>--}}
		<td>{{ @$doctor['clinics'] }}</td>
	</tr>
	@endforeach
</table>
</body>
</html>