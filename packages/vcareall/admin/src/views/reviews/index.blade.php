@extends('admin::template')

@section('admin::content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa fa-bars"></i> Reviews & Rating</h3>
        <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="{{ url('admin/dashboard')}}">Home</a></li>
                <li><i class="fa fa-bars"></i>Reviews & Rating</li>
        </ol>
    </div>
</div>
@if(session('message') && !session('type'))
<div class="row">
    <div class="col-lg-12">
         @if(session('status')==1)
        <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div>
         @endif
        @if(session('status')==0)
        <div class="alert alert-block alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="icon_close"></i>
            </button>
            {{ session('message') }}
        </div> 
        @endif
    </div>
</div>
@endif
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr class="panel-heading">
                    <!-- <th>#</th> -->
                    <th width="69%">Review Details</th>
                    <th>Moderation Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php static $sr=1;?>
                   <tr><td colspan="4" align="right">{{ $reviews->links() }}</td></tr>
                  @foreach($reviews as $review)
                  <tr>
                    <!--<td>{{ $sr++ }}</td> -->
                    <td> 
						<div class="col-xs-12 padding-0">
                            <div class="col-md-1" style="padding:0px;">
                                <a href="javascript::void(0)" class="activity-img activity-img1">
                                   <img src="{{ $review->user->user->image?$review->user->user->image:  asset('public/admin/img/user-placeholder.png') }}" style="width:50px;height:70px;" alt="No Image" />
								</a>
                            </div>
                            <div class="col-md-10">
                                <p class="attribution act-in" style="margin-bottom:0px;"><a href="javascript::void(0)">{{ $review->user->user->name }}</a></p>
                                <p style="margin-bottom:0px;">{{ $review->user->email }}</p>
                            </div>
                            <div class="clearfix" style="margin-bottom:10px;"></div>
                            <p><span class="spec">
                                @if($review->doctor_id)
                                {{ $review->doctor->name }}
                                @elseif($review->hospital_id)
                                    {{ $review->hospital->name }}
                                @elseif($review->diagnostic_id)
                                    {{ $review->diagnostic->name }}
                                @endif</span>
                            </p>
                            <p>Overall Rating
								@for ($i=1; $i <= 5 ; $i++)
                                {{-- <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span> --}}
								<span class="fa fa-star<?php if($i < $review->rating or $i == $review->rating){ echo '';}elseif($i-1 < $review->rating &&  $review->rating < $i){ echo '-half'; } else{ echo '-o';} ?>"></span>
                                @endfor
							</p>
							<p>
							@if($review->reviewDetail)
								@foreach($review->reviewDetail as $temp)
									<span class="color-grey">{{ $temp->param->name }}<span> 
									@for ($i=1; $i <= 5 ; $i++)
										<span class="glyphicon glyphicon-star{{ ($i <= $temp->rate) ? '' : '-empty'}}"></span>
									@endfor
									<br>
								@endforeach
							@endif
							</p>
							<h3>{{ $review->title }}</h3><br/>
							<span>{!! $review->comment !!}</span><br>
							{{ $review->timeago }}
                        </div>
                    </td>
                    <td>
						@if($review->approved)<span class="label label-success">Approved</span> @endif
						@if(!$review->approved && !$review->spam)<span class="label label-danger">Pending</span> @endif
						@if($review->spam)<span class="label label-danger">Declined</span> @endif
                    <td>
					@if($review->approved ==0 or $review->spam)
                        <a class="btn btn-success btn-sm" onclick="return confirm('Are you sure ?')" href="{{ url('admin/reviews/approve')}}/{{ $review->id}}" title="Bootstrap 3 themes generator">Approve</a>
					@endif	
					@if($review->approved ==1 or $review->approved ==0)
                        <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure ?')" href="{{ url('admin/reviews/disapprove')}}/{{ $review->id}}" title="Bootstrap 3 themes generator">Disapprove</a>
					@endif
                    </td>
                  </tr>
                  @endforeach
                  <tr><td colspan="4" align="right">{{ $reviews->links() }}</td></tr>
                </tbody>
              </table>
            </div>

        </section>
    </div>
</div>
@endsection