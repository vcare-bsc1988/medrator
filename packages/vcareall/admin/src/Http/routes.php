<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'LoginController@index');
    Route::get('logout', 'LoginController@logout');
    Route::post('login', 'LoginController@login');    
});
Route::group(['middleware' => ['web','auth.admin']], function () {
    Route::get('dashboard', 'HomeController@index');
    Route::get('users', 'UsersController@index');
    Route::post('user-list', 'UsersController@userList');
    Route::get('user-details/{id}', 'UsersController@details');
    Route::get('doctors', 'UsersController@doctorList');
    Route::post('doctors', 'UsersController@doctors');
    Route::get('hospitals-clinics', 'UsersController@hospitalClinicList');
    Route::post('hospitals-clinics', 'UsersController@hospitalsClinics');
    Route::get('diagnostic-centers', 'UsersController@diagnosticList');
    Route::post('diagnostics', 'UsersController@diagnostics');
    Route::get('doctor-details/{id}', 'DoctorController@details');
    Route::get('hospital-clinic-details/{id}', 'HospitalClinicController@details');
    Route::get('diagnostic-details/{id}', 'DiagnosticController@details');
    /***
     * Manage doctor profile
     */
    Route::post('doctor/add-services', 'DoctorController@addServices');
    Route::post('doctor/delete-services', 'DoctorController@deleteService');
    Route::post('doctor/add-specialties', 'DoctorController@addSpecialties');
    Route::post('doctor/delete-specialty', 'DoctorController@deleteSpecialty');
    Route::post('doctor/profile-accept', 'DoctorController@profileAccept');
    Route::post('doctor/profile-declined', 'DoctorController@profileDeclined');
    Route::post('doctor/upload-image', 'DoctorController@uploadImage');
    Route::post('doctor/delete-image', 'DoctorController@deleteImage');
    Route::post('doctor/update', 'DoctorController@doctor/update');
    Route::get('doctor/delete/{id}',['middleware' => ['role:administrator|manager|super-admin'],'uses'=> 'DoctorController@delete']);
    Route::get('doctor/create', 'DoctorController@create');
    Route::post('doctor/save', 'DoctorController@save');
    /****
     * Manage hospital profile
     */
    Route::post('hospital/add-specialties', 'HospitalClinicController@addSpecialties');
    Route::post('hospital/delete-specialty', 'HospitalClinicController@deleteSpecialty');
    Route::post('hospital/profile-accept', 'HospitalClinicController@profileAccept');
    Route::post('hospital/profile-declined', 'HospitalClinicController@profileDeclined');
    Route::post('hospital/upload-image', 'HospitalClinicController@uploadImage');
    Route::post('hospital/delete-image', 'HospitalClinicController@deleteImage');
    Route::post('hospital/update', 'HospitalClinicController@update');
    Route::get('hospital/delete/{id}',['middleware' => ['role:administrator|manager|super-admin'],'uses'=> 'HospitalClinicController@delete']);
    Route::get('hospital/create', 'HospitalClinicController@create');
    Route::post('hospital/save', 'HospitalClinicController@save');
	/***
     * Routes Hospital Clinic Schedues
     */
	Route::post('hospital-clinic/find', 'HospitalClinicController@find');
	Route::post('hospital-clinic/save-sun-schedules', 'HospitalClinicController@saveSunSchedules');
	Route::post('hospital-clinic/save-mon-schedules', 'HospitalClinicController@saveMonSchedules');
	Route::post('hospital-clinic/save-tue-schedules', 'HospitalClinicController@saveTueSchedules');
	Route::post('hospital-clinic/save-wed-schedules', 'HospitalClinicController@saveWedSchedules');
	Route::post('hospital-clinic/save-thr-schedules', 'HospitalClinicController@saveThrSchedules');
	Route::post('hospital-clinic/save-fri-schedules', 'HospitalClinicController@saveFriSchedules');
	Route::post('hospital-clinic/save-sat-schedules', 'HospitalClinicController@saveSatSchedules');
    /***
     * Manage diagnostic profile
     */
    Route::post('diagnostic/add-tests', 'DiagnosticController@addTests');
    Route::post('diagnostic/delete-test', 'DiagnosticController@deleteTest');
    Route::post('diagnostic/profile-accept', 'DiagnosticController@profileAccept');
    Route::post('diagnostic/profile-declined', 'DiagnosticController@profileDeclined');
    Route::post('diagnostic/upload-image', 'DiagnosticController@uploadImage');
    Route::post('diagnostic/delete-image', 'DiagnosticController@deleteImage');
    Route::post('diagnostic/update', 'DiagnosticController@update');
    Route::get('diagnostic/delete/{id}',['middleware' => ['role:administrator|manager|super-admin'],'uses'=> 'DiagnosticController@delete']);
    Route::get('diagnostic/create', 'DiagnosticController@create');
    Route::post('diagnostic/save', 'DiagnosticController@save');
    /****
     * Mange user profile
     */
    Route::post('user/profile-accept', 'UsersController@profileAccept');
    Route::post('user/profile-declined', 'UsersController@profileDeclined');
    Route::post('user/upload-image', 'UsersController@uploadImage');
    Route::post('user/delete-image', 'UsersController@deleteImage');
    Route::post('user/update', 'UsersController@update');
    Route::get('reviews', 'ReviewsController@index');
    Route::get('reviews/approve/{id}', 'ReviewsController@approve');
    Route::get('reviews/disapprove/{id}', 'ReviewsController@disapprove');
    Route::resource('setting/radius', 'SettingController@radius');
    Route::get('data-scraper', 'ScrapperController@dataScraper');
    Route::post('scrap', 'ScrapperController@scrap');
    Route::post('scrap/doctors', 'ScrapperController@itemsCount');
    Route::get('import-excel', 'ScrapperController@importExport');
    Route::post('import-excel', 'ScrapperController@importExcel');
    Route::get('download-excel/{type}', 'ScrapperController@downloadExcel');
    /**
	 * Routes for notifications.
	 */
	Route::get('user/send-notifications', 'UsersController@getNotifications');
    Route::get('notifications', 'UsersController@notifications');
    Route::post('user/send-notifications', 'UsersController@PostNotifications');
    Route::get('user/reset-notifications', 'UsersController@resetNotifications');
	
	Route::get('claime/doctor', 'ClaimController@doctor');
    Route::get('claime/approve_doctor',['middleware' => ['role:administrator|manager|super-admin'],'uses'=> 'ClaimController@approveDoctor']);
    Route::get('claime/disapprove_doctor', 'ClaimController@disapproveDoctor');
	Route::get('claime/delete_doctor', 'ClaimController@deleteDoctor');
    Route::get('claime/hospital', 'ClaimController@hospital');
    Route::get('claime/approve_hospital',['middleware' => ['role:administrator|manager|super-admin'],'uses'=> 'ClaimController@approveHospital']);
    Route::get('claime/disapprove_hospital', 'ClaimController@disapproveHospital');
	Route::get('claime/delete_hospital', 'ClaimController@deleteHospital');
	Route::get('claime/diagnostic', 'ClaimController@diagnostic');
    Route::get('claime/approve_diagnostic',['middleware' => ['role:administrator|manager|super-admin'],'uses'=> 'ClaimController@approveDiagnostic']);
    Route::get('claime/disapprove_diagnostic', 'ClaimController@disapproveDiagnostic');
	Route::get('claime/delete_diagnostic', 'ClaimController@deleteDiagnostic');
    Route::resource('import-excel/hospital', 'ScrapperController@importHospital');
    Route::resource('import-excel/diagnostic', 'ScrapperController@importDiagnostic');
    /***
     * Routes for managing staffs
     */
    Route::post('staff-update', 'StaffController@update');
    Route::post('staff/add', 'StaffController@add');
    Route::get('staff-edit', 'StaffController@edit');
    Route::get('staffs', 'StaffController@index');
    Route::get('staff/delete/{id}',['middleware' => ['role:super-admin'], 'uses'=>'StaffController@delete']);
	
    /** 
     * Routing for pending for approval
     */
     Route::get('pending-approval', 'UsersController@pendingApproval');
     Route::get('approve-doctor', 'UsersController@approveDoctor');
     Route::post('approve-doctor', 'UsersController@approveDoctor');
     Route::get('approve-hospital', 'UsersController@approveHospital');
     Route::post('approve-hospital', 'UsersController@approveHospital');
     Route::get('approve-diagnostic', 'UsersController@approveDiagnostic');
     Route::post('approve-diagnostic', 'UsersController@approveDiagnostic');
	 Route::post('decline-doctor', 'UsersController@declineDoctor');
	 Route::post('decline-hospital', 'UsersController@declineHospital');
	 Route::post('decline-diagnostic', 'UsersController@declineDiagnostic');
	 Route::post('approve-all', 'UsersController@approveAll');
     
     /*
      * Routing for Handling Notifications
      */
    Route::get('notification-unread/{id}', 'UsersController@notificationUnread');
    Route::get('notification-read/{id}', 'UsersController@notificationRead');
    Route::get('clear-notification/{id}', 'UsersController@clearNotification');
    Route::get('account/notifications', 'UsersController@notificationAll');
    Route::get('clear-notifications/{id}', 'UsersController@clearNotifications');
});


