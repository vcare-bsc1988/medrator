<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Vcareall\Admin\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\User;
use DB;
use Excel;
use Illuminate\Support\Facades\Input;
/**
 * Description of HomeController
 *
 * @author BALESHAWR
 */
class HomeController extends Controller {
    public function index()
    {
        $data['doctor']= \App\Model\Doctor::get();
        $data['hospital']= \App\Model\Hospital::get();
        $data['diagnostic']= \App\Model\Diagnostic::get();
        $data['user']= \App\User::orderBy('id','desc')->skip(0)->take(10)->get();
        return view('admin::test')->with($data);
    }
    public function importExcel(){
        return view('admin::import-excel');
    }
}
