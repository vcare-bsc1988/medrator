<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;

/**
 * Description of StaffController
 *
 * @author BALESHAWR
 */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Service;
use App\Model\Specialty;
use Illuminate\Support\Facades\Validator;
use Image;
use App\User;
class StaffController extends Controller {
    public function index(){
        $result['staffs']= User::whereHas('roles', function($q){
            $q->where('name','administrator')
               ->orWhere('name','manager')
               ->orWhere('name','moderator')
               ->orWhere('name','super-admin')
               ->orWhere('name','data-entry-user');
        })->get();
        $result['roles'] = \App\Role::where('name','administrator')
                ->orWhere('name','manager')
                ->orWhere('name','moderator')
                ->orWhere('name','data-entry-user')
                ->orWhere('name','super-admin')
                ->lists('display_name','id');
        return view('admin::staff.index',$result);
    }
    public function add(Request $request){
        try{
            if ($request->isMethod('post')) {
                
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|email|unique:login,email',
                    'password' => 'required',
                    'role' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>1]);
                } 
                $role_name = \App\Role::find($request->input('role'))->name;
                switch ($role_name) {
                    case 'administrator':
                        $wp_permission = 'administrator';
                        break;
                    case 'super-admin':
                        $wp_permission = 'administrator';
                        break;
                    case 'manager':
                        $wp_permission = 'administrator';
                        break;
                    case 'moderator':
                        $wp_permission = 'editor';
                        break;
                    case 'data-entry-user':
                        $wp_permission = 'subscriber';
                        break;
                }
                ### wordpress registration start here
                $wp_user = \wp_insert_user([
                    'user_pass'=>$request->password,
                    'user_login'=>$request->email,
                    'user_email'=>$request->email,
                    'role'=>$wp_permission
                ]);
                if ( is_wp_error( $wp_user )) {
                   return back()
                        ->withInput(['email'])
                        ->withErrors(['User already exist.'])
                        ->with(['status'=>0,'message'=>'Failed.','type'=>1]);
                }
                $profile = new \App\Model\Users([
                    'name'=>$request->input('name'),
                ]);
                $user = \App\User::insert([
                    'id' => $wp_user,
                    'email' => $request->input('email'),
                    'username' => $request->input('email'),
                    'status' => 1,
                    'password' => bcrypt($request->input('password')),
                ]);
                $user = \App\User::find($wp_user);
                $profile->user()->associate($user);
                $profile->save();
                $role = \App\Role::find($request->input('role'));
                $user->attachRole($role); 
                return back()->with(['status'=>1,'message'=>'Staff account added successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>1]);
        }
    }
    public function edit(Request $request){
        if($request->input('id')){
            $user = \App\User::find($request->input('id'));
            $data=[
				'id' => $user->id,
                'email' => $user->email,
                'name' => $user->user->name,
                'role' => $user->roles
            ];
            return  $data;
        }
    }
	public function update(Request $request){
		if ($request->isMethod('post')) {

			try{
				$validator = Validator::make($request->all(), [
					'name' => 'required',
					'email' => 'required|email',
					'role' => 'required',
				]);
				if ($validator->fails()) {
					return back()
						->withInput()
						->withErrors($validator)
						->with(['status'=>0,'message'=>'Failed.','type'=>2,'staff_id'=>$request->input('id')]);;
				}   
				$user = \App\User::find($request->input('id'));
				if(\App\User::where('email',$request->input('email'))
					->where('id','!=',$request->input('id'))->get()->count()>0){
					return back()
                        ->withInput(['email'])
                        ->withErrors(['Email already exist.'])
                        ->with(['status'=>0,'message'=>'Email already exist.']);	
				}
				$user->email = $request->input('email');
				$user->status = 1;
				$user->user->name = $request->input('name');
				if($request->input('password')){
					$user->password = bcrypt($request->input('password'));
				}
				$user->user->save();
				$user->save();
				$role_name = \App\Role::find($request->input('role'))->name;
                switch ($role_name) {
                    case 'administrator':
                        $wp_permission = 'administrator';
                        break;
                    case 'super-admin':
                        $wp_permission = 'administrator';
                        break;
                    case 'manager':
                        $wp_permission = 'administrator';
                        break;
                    case 'moderator':
                        $wp_permission = 'editor';
                        break;
                    case 'data-entry-user':
                        $wp_permission = 'subscriber';
                        break;
                }
				//echo $wp_permission;
				### wordpress registration start here
                $wp_user = \wp_insert_user([
					'ID' =>$user->id,
                    'user_login'=>$request->email,
                    'user_email'=>$request->email,
                    'role'=>$wp_permission
                ]);
				//print_r($wp_user); exit;
                if ( is_wp_error( $wp_user )) {
                   return back()
                        ->withInput(['email'])
                        ->withErrors(['User already exist.'])
                        ->with(['status'=>0,'message'=>'Failed.']);
                }
				$user->roles()->detach();
				$role = \App\Role::find($request->input('role'));
                $user->attachRole($role); 
				return back()->with(['status'=>1,'message'=>'Staff account updated successfully.']);
			}
			catch(\Illuminate\Database\QueryException $e){
				$response= array('status'=>0, 'message'=>$e->getMessage()); 
				return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>2,'staff_id'=>$request->input('id')]);
			}	
		}
    }
    public function delete($id){
        if(!$id){
            abort(404);
        }
        $user = \App\User::find($id);
        $user->delete();
        return back()
            ->with(['status'=>1,'message'=>'Deleted successfully.']);
    }
		
}
