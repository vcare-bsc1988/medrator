<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;

/**
 * Description of ReviewsController
 *
 * @author BALESHAWR
 */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Service;
use App\Model\Specialty;
use Illuminate\Support\Facades\Validator;
use Image;
use Illuminate\Support\Facades\Mail;
class ReviewsController extends Controller {
	protected $helper;
	protected $client;
	public function __construct(\Solarium\Client $client)
    {
		$this->client = $client;
		$this->helper = new \App\Helpers\Helper;
    }
    public function index(){
        $result['reviews']= \App\Model\Review::orderBy('id','desc')->paginate(10);
        return view('admin::reviews.index',$result);
    }
    public function approve($id){
        if(!$id){
            abort(404);
        }
        try{
            $review = \App\Model\Review::find($id);
            $review->approved =1;
            $review->spam =0;
            $review->save();
            ### Updating overall doctor review
            if($review->doctor_id){
				$data['id'] = $review->doctor_id;
				$data['entity'] = 'doctor';
                $doctor = \App\Model\Doctor::find($review->doctor_id);
                $reviews = $doctor->reviews()->notSpam()->approved();
                $avgRating = $reviews->avg('rating');
                switch ($review->rating) {
                        case 1:
                                $doctor->rating_one_star_count = $reviews->where('rating',1)->count();
                                $data['rating_one_star_count'] = $reviews->where('rating',1)->count();
                                break;
                        case 2:
                                $doctor->rating_two_star_count = $reviews->where('rating',2)->count();
                                $data['rating_two_star_count'] = $reviews->where('rating',2)->count();
                                break;
                        case 3:
                                $doctor->rating_three_star_count = $reviews->where('rating',3)->count();
                                $data['rating_three_star_count'] = $reviews->where('rating',3)->count();
                                break;
                        case 4:
                                $doctor->rating_four_star_count = $reviews->where('rating',4)->count();
                                $data['rating_four_star_count'] = $reviews->where('rating',4)->count();;
                                break;
                        case 5:
                                $doctor->rating_five_star_count = $reviews->where('rating',5)->count();
                                $data['rating_five_star_count'] = $reviews->where('rating',5)->count();
                                break;
                }
                $doctor->rating_cache = round($avgRating,1);
                $doctor->rating_count = $doctor->reviews()->notSpam()->approved()->count();
                $data['rating_cache'] = $doctor->rating_cache;
                $data['rating_count'] = $doctor->rating_count;
                $doctor->save();
                $name = $review->doctor->name;
            }
            ### Updating overall hospital review
            if($review->hospital_id){
                $data['entity'] = 'hospital';   
				$data['id'] = $review->hospital_id;		
                $hospital = \App\Model\Hospital::find($review->hospital_id);
                $reviews = $hospital->reviews()->notSpam()->approved();
                $avgRating = $reviews->avg('rating');
                    switch ($review->rating) {
                        case 1:
                                $hospital->rating_one_star_count = $reviews->where('rating',1)->count();
                                $data['rating_one_star_count'] = $reviews->where('rating',1)->count();
                                break;
                        case 2:
                                $hospital->rating_two_star_count = $reviews->where('rating',2)->count();
                                $data['rating_two_star_count'] = $reviews->where('rating',2)->count();
                                break;
                        case 3:
                                $hospital->rating_three_star_count = $reviews->where('rating',3)->count();
                                $data['rating_three_star_count'] =  $reviews->where('rating',3)->count();
                                break;
                        case 4:
                                 $hospital->rating_four_star_count = $reviews->where('rating',4)->count();
                                 $data['rating_four_star_count'] = $reviews->where('rating',4)->count();
                                break;
                        case 5:
                                $hospital->rating_five_star_count = $reviews->where('rating',5)->count();
                                $data['rating_five_star_count'] = $reviews->where('rating',5)->count();
                                break;
                    }
                    $hospital->rating_cache = round($avgRating,1);
                    $hospital->rating_count = $hospital->reviews()->notSpam()->approved()->count();
                    $data['rating_cache'] = $hospital->rating_cache;
                    $data['rating_count'] = $hospital->rating_count;
                    $hospital->save();
                    $name = $review->hospital->name;
            }
            ### Updating overall diagnostic review
            if($review->diagnostic_id){
                    $data['entity'] = 'diagnostic';
					$data['id'] = $review->diagnostic_id;	
                    $diagnostic = \App\Model\Diagnostic::find($review->diagnostic_id);
                    $reviews = $diagnostic->reviews()->notSpam()->approved();
                    $avgRating = $reviews->avg('rating');
                    switch ($review->rating) {
                            case 1:
                                    $diagnostic->rating_one_star_count = $reviews->where('rating',1)->count(); 
                                    $data['rating_one_star_count'] = $reviews->where('rating',1)->count(); 
                                    break;
                            case 2:
                                    $diagnostic->rating_two_star_count = $reviews->where('rating',2)->count();
                                    $data['rating_two_star_count'] = $reviews->where('rating',2)->count();
                                    break;
                            case 3:
                                    $diagnostic->rating_three_star_count = $reviews->where('rating',3)->count();
                                    $data['rating_three_star_count'] = $reviews->where('rating',3)->count();
                                    break;
                            case 4:
                                    $diagnostic->rating_four_star_count = $reviews->where('rating',4)->count();
                                    $data['rating_four_star_count'] = $reviews->where('rating',4)->count();
                                    break;
                            case 5:
                                    $diagnostic->rating_five_star_count = $reviews->where('rating',5)->count();
                                    $data['rating_five_star_count'] = $reviews->where('rating',5)->count();
                                    break;
                    }
                    $diagnostic->rating_cache = round($avgRating,1);
                    $diagnostic->rating_count = $diagnostic->reviews()->notSpam()->approved()->count();
                    $data['rating_cache'] = $diagnostic->rating_cache;
                    $data['rating_count'] = $diagnostic->rating_count;
                    $diagnostic->save();
                    $name = $review->diagnostic->name;
            }
            $this->updateSolrIndex($data);
			
			/**
			 * Updating review solr index.
			 */
			$this->updateReviewSolrIndex($review);
			
            # Sending user notification
            $deviceToken = $review->user->device_token;
            $gcmApiKey = config('constants.gcm_apikey');
            $deviceType = $review->user->device_type;
            $notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
            \App\Model\Notification::create([
                    'title'=>config('message.review_notification_approval_title'),
                    'message'=>str_replace('[NAME]',$name,config('message.review_notification_approval_message')),
                    'receiver'=>$review->user->id,
                    'type'=>1,
                    'status'=>0
            ]);
            $notifications= $notifications->send([
                    'message' 	=> str_replace('[NAME]',$name,config('message.review_notification_approval_message')),
                    'title'		=> str_replace('[NAME]',$name,config('message.review_notification_approval_title')),
                    'subtitle'	=> str_replace('[NAME]',$name,config('message.review_notification_approval_title')),
                    'tickerText'	=> 'Medrator',
                    'vibrate'	=> 1,
                    'sound'		=> 1,
                    'largeIcon'	=> 'large_icon',
                    'smallIcon'	=> 'small_icon'
            ]);
            /**
             * Sending admin notification.
             */
            $this->helper->sendAdminNotification([
                    'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_approval_title')),
                    'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_approval_message')),
            ]);
            /**
             * Sending moderator notification.
             */
            $this->helper->sendModeratorNotification([
                    'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_approval_title')),
                    'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_approval_message')),
            ]);
            /**
             * Sending email.
             */
            $data = [
                            'email' => $review->user->email,
                            'user_name'=>$review->user->user->name,
                            'entity_name'=>$name
            ];
            Mail::send('admin::email.review_approve',['data'=>$data], function ($message) use ($data) {
                            $message->to($data['email'])->subject('Review approved');
            });
            return back()->with(['status'=>1,'message'=>'Approved successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function disapprove($id){
        if(!$id){
            abort(404);
        }
        try{
            $review = \App\Model\Review::find($id);
            $review->approved =0;
            $review->spam =1;
            $review->save();
            ### Updating overall doctor review
            if($review->doctor_id){
                $doctor = \App\Model\Doctor::find($review->doctor_id);
                $reviews = $doctor->reviews()->notSpam()->approved();
                $avgRating = $reviews->avg('rating');			
                $doctor->rating_cache = round($avgRating,1);
                $doctor->rating_count = $doctor->reviews()->notSpam()->approved()->count();
                $doctor->rating_one_star_count = $doctor->rating_one_star_count-($review->rating==1?1:0);
                $doctor->rating_two_star_count =$doctor->rating_two_star_count-($review->rating==2?1:0);
                $doctor->rating_three_star_count =$doctor->rating_three_star_count-($review->rating==3?1:0);
                $doctor->rating_four_star_count = $doctor->rating_four_star_count-($review->rating==4?1:0);
                $doctor->rating_five_star_count =$doctor->rating_five_star_count-($review->rating==5?1:0);
                $doctor->save();
                $this->updateSolrIndex([
                    'id' => $doctor->id,
                    'entity'=> 'doctor',
                    'approved' => $review->approved,
                    'spam' => $review->spam,
                    'rating_cache' => $doctor->rating_cache,
                    'rating_count' => $doctor->rating_count,
                    'rating_one_star_count' => $doctor->rating_one_star_count,
                    'rating_two_star_count' => $doctor->rating_two_star_count,
                    'rating_three_star_count' => $doctor->rating_three_star_count,
                    'rating_four_star_count' => $doctor->rating_four_star_count,
                    'rating_five_star_count' => $doctor->rating_five_star_count
                ]);
                $name = $review->doctor->name;
            }
            ### Updating overall hospital review
            if($review->hospital_id){
                $hospital = \App\Model\Hospital::find($review->hospital_id);
                $reviews = $hospital->reviews()->notSpam()->approved();
                $avgRating = $reviews->avg('rating');			
                $hospital->rating_cache = round($avgRating,1);
                $hospital->rating_count = $hospital->reviews()->notSpam()->approved()->count();

                $hospital->rating_one_star_count = $hospital->rating_one_star_count-($review->rating==1?1:0);
                $hospital->rating_two_star_count =$hospital->rating_two_star_count-($review->rating==2?1:0);
                $hospital->rating_three_star_count =$hospital->rating_three_star_count-($review->rating==3?1:0);
                $hospital->rating_four_star_count = $hospital->rating_four_star_count-($review->rating==4?1:0);
                $hospital->rating_five_star_count =$hospital->rating_five_star_count-($review->rating==5?1:0);
                $hospital->save();
                $name = $review->hospital->name;
                $this->updateSolrIndex([
                    'id' => $hospital->id,
                    'entity'=> 'hospital',
                    'approved' => $review->approved,
                    'spam' => $review->spam,
                    'rating_cache' => $hospital->rating_cache,
                    'rating_count' => $hospital->rating_count,
                    'rating_one_star_count' => $hospital->rating_one_star_count,
                    'rating_two_star_count' => $hospital->rating_two_star_count,
                    'rating_three_star_count' => $hospital->rating_three_star_count,
                    'rating_four_star_count' => $hospital->rating_four_star_count,
                    'rating_five_star_count' => $hospital->rating_five_star_count
                ]);
            }
            ### Updating overall diagnostic review
            if($review->diagnostic_id){
                $diagnostic = \App\Model\Diagnostic::find($review->diagnostic_id);
                $reviews = $diagnostic->reviews()->notSpam()->approved();
                $avgRating = $reviews->avg('rating');			
                $diagnostic->rating_cache = round($avgRating,1);
                $diagnostic->rating_count = $diagnostic->reviews()->notSpam()->approved()->count();

                $diagnostic->rating_one_star_count = $diagnostic->rating_one_star_count-($review->rating==1?1:0);
                $diagnostic->rating_two_star_count =$diagnostic->rating_two_star_count-($review->rating==2?1:0);
                $diagnostic->rating_three_star_count =$diagnostic->rating_three_star_count-($review->rating==3?1:0);
                $diagnostic->rating_four_star_count = $diagnostic->rating_four_star_count-($review->rating==4?1:0);
                $diagnostic->rating_five_star_count =$diagnostic->rating_five_star_count-($review->rating==5?1:0);
                $diagnostic->save();
                $name = $review->diagnostic->name;
                $this->updateSolrIndex([
                    'id' => $diagnostic->id,
                    'entity'=> 'diagnostic',
                    'approved' => $review->approved,
                    'spam' => $review->spam,
                    'rating_cache' => $diagnostic->rating_cache,
                    'rating_count' => $diagnostic->rating_count,
                    'rating_one_star_count' => $diagnostic->rating_one_star_count,
                    'rating_two_star_count' => $diagnostic->rating_two_star_count,
                    'rating_three_star_count' => $diagnostic->rating_three_star_count,
                    'rating_four_star_count' => $diagnostic->rating_four_star_count,
                    'rating_five_star_count' => $diagnostic->rating_five_star_count
                ]);
            }
			/**
			 * Updating review solr index.
			 */
			$this->updateReviewSolrIndex($review);
			
			# Sending user notification
			$deviceToken = $review->user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $review->user->device_type;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.review_notification_disapproved_title'),
				'message'=>str_replace('[NAME]',$name,config('message.review_notification_disapproved_message')),
				'receiver'=>$review->user->id,
				'type'=>2,
				'status'=>0
			]);
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.review_notification_disapproved_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.review_notification_disapproved_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.review_notification_disapproved_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
			
			/**
			 * Sending admin notification.
			 */
			$this->helper->sendAdminNotification([
				'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_disapproved_title')),
				'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_disapproved_message')),
			]);
			/**
			 * Sending moderator notification.
			 */
			$this->helper->sendModeratorNotification([
				'message' 	=> str_replace('[NAME]',$name,config('message.admin_review_notification_disapproved_title')),
				'title'		=> str_replace('[NAME]',$name,config('message.admin_review_notification_disapproved_message')),
			]);
			/**
			 * Sending email.
			 */
			$data = [
					'email' => $review->user->email,
					'user_name'=>$review->user->user->name,
					'entity_name'=>$name
			];
			Mail::send('admin::email.review_disapprove',['data'=>$data], function ($message) use ($data) {
					$message->to($data['email'])->subject('Review Declined');
			});
            return back()->with(['status'=>1,'message'=>'Disapproved successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    private function updateSolrIndex($data){
        $update = $this->client->createUpdate();
        $doc1 = $update->createDocument();	
        $entity_id = $data['entity'].'_'.$data['id']; 
        $doc1->setKey('entity_id', $entity_id);
		$doc1->setKey('entity', $data['entity']);				
        if(array_key_exists("name",$data)){ 
            $doc1->setField('name', $data['name']);
            $doc1->setFieldModifier('name', 'set');
        }
        if(array_key_exists("rating_cache",$data)){
			$doc1->setField('rating', $data['rating_cache']);
            $doc1->setFieldModifier('rating', 'set');
        }
        if(array_key_exists("rating_count",$data)){
            $doc1->setField('review_count', $data['rating_count']);
            $doc1->setFieldModifier('review_count', 'set');
        }
        if(array_key_exists("rating_one_star_count",$data)){
            $doc1->setField('rating_one_star_count', $data['rating_one_star_count']);
            $doc1->setFieldModifier('rating_one_star_count', 'set');
        }
        if(array_key_exists("rating_two_star_count",$data)){
            $doc1->setField('rating_two_star_count', $data['rating_two_star_count']);
            $doc1->setFieldModifier('rating_two_star_count', 'set');
        }
        if(array_key_exists("rating_three_star_count",$data)){
            $doc1->setField('rating_three_star_count', $data['rating_three_star_count']);
            $doc1->setFieldModifier('rating_three_star_count', 'set');
        }
        if(array_key_exists("rating_four_star_count",$data)){
            $doc1->setField('rating_four_star_count', $data['rating_four_star_count']);
            $doc1->setFieldModifier('rating_four_star_count', 'set');
        }
        if(array_key_exists("rating_five_star_count",$data)){
            $doc1->setField('rating_five_star_count', $data['rating_five_star_count']);
            $doc1->setFieldModifier('rating_five_star_count', 'set');
        }
        if(array_key_exists("approved",$data)){
            $doc1->setField('approved', $data['approved']);
            $doc1->setFieldModifier('approved', 'set');
        }
        if(array_key_exists("spam",$data)){
            $doc1->setField('spam', $data['spam']);
            $doc1->setFieldModifier('spam', 'set');
        }
        $update->addDocuments(array($doc1));
        $update->addCommit();	
        $result = $this->client->update($update);
        return $result;
    }
	public function updateReviewSolrIndex($review){
		
		$update = $this->client->createUpdate();
        $doc2 = $update->createDocument();	
        $entity_id = 'review_'.$review->id; 
        $doc2->setKey('entity_id', $entity_id);
		
		$doc2->setField('disease_visited', $review->disease_visited);
        $doc2->setFieldModifier('disease_visited', 'set');
		
		$doc2->setField('rating', $review->rating);
        $doc2->setFieldModifier('rating', 'set');
		
		$doc2->setField('created_at', $review->created_at);
        $doc2->setFieldModifier('created_at', 'set');
		
		$doc2->setField('title', $review->title);
        $doc2->setFieldModifier('title', 'set');
		
		$doc2->setField('hospital_id', $review->hospital_id);
        $doc2->setFieldModifier('hospital_id', 'set');
		
		$doc2->setField('diagnostic_id', $review->diagnostic_id);
        $doc2->setFieldModifier('diagnostic_id', 'set');
		
		$doc2->setField('doctor_id', $review->doctor_id);
        $doc2->setFieldModifier('doctor_id', 'set');
		
		$doc2->setField('approved', $review->approved);
        $doc2->setFieldModifier('approved', 'set');
		
		$doc2->setField('updated_at', $review->updated_at);
        $doc2->setFieldModifier('updated_at', 'set');
		
		$doc2->setField('user_id', $review->user_id);
        $doc2->setFieldModifier('user_id', 'set');
		
		$doc2->setField('comment', $review->comment);
        $doc2->setFieldModifier('comment', 'set');
		
		$doc2->setField('id', $review->id);
        $doc2->setFieldModifier('id', 'set');
		
		$doc2->setField('spam', $review->spam);
        $doc2->setFieldModifier('spam', 'set');
		
		$doc2->setField('entity', 'review');
        $doc2->setFieldModifier('entity', 'set');
		
		$doc2->setField('post_anonymous', $review->post_anonymous);
        $doc2->setFieldModifier('entity', 'set');
		
		$update->addDocuments(array($doc2));
        $update->addCommit();	
        $result = $this->client->update($update);
		//print_r($result); exit;
        return $result;
	}
}
