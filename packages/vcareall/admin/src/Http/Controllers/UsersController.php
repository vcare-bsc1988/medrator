<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Service;
use App\Model\Specialty;
use Illuminate\Support\Facades\Validator;
use Image;
use App\Model\Notification;
use Auth;
use Illuminate\Support\Facades\Mail;
/**
 * Description of UsersController
 *
 * @author VCareAll
 */
class UsersController extends Controller {
    public function index(){
        return view('admin::users.index');
    }
    public function userList(Request $request){
            $params = $request->all();
            $columns = array( 
                    0 =>'user_id',
                    1 =>'image',
                    2 =>'name', 
                    3 => 'email',
                    4 => 'mobile',
                    5 => 'gender',
                    6 => 'dob',
                    7 => 'created_at',
                    8 => 'status',
                    9 => 'action'
            );
            $where="";
            if( !empty($params['search']['value']) ) {   
                $where .=" WHERE ";
                $where .=" ( name LIKE '".$params['search']['value']."%' ";    
                $where .=" OR email LIKE '".$params['search']['value']."%' ";

                $where .=" OR mobile LIKE '".$params['search']['value']."%' )";
            }
            $sqlTot="";
            $sqlRec ="";
            $sql = "SELECT * FROM `login` join `users` on login.id=users.user_id ";
            $sqlTot .= $sql;
            $sqlRec .= $sql;
            #concatenate search sql if value exist
            if(isset($where) && $where != '') {
                $sqlTot .= $where;
                $sqlRec .= $where;
            }
            $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
            $queryTot = DB::select($sqlTot);
            $totalRecords = count($queryTot);
            $queryRecords =  DB::select($sqlRec);
            #iterate on results row and create new index array of data
            $data = [];
            if(count($queryRecords)>0){
                foreach($queryRecords as $temp){
                    $action = '<a href="'.url('admin/user-details/'.$temp->user_id).'" class="btn btn-nothing" data-toggle="tooltip" data-placement="top" title="View Details"><i class="icon_pencil-edit"></i></a>';
                    $gender = "Not Updated";
                    if($temp->gender == 1)
                        $gender = "Male";
                    elseif($temp->gender==2)
                        $gender = "Female";
                    $imageUrl = $temp->image?$temp->image:  asset('public/admin/img/user-placeholder.png');                
                    $image = '<img src="'.$imageUrl.'" style="width:50px;height:70px;" alt="No Image" />';
                    $status = ($temp->status==1)?'<span class="label label-success">Active</span>':'<span class="label label-danger">Inactive</span>';
                    $data[] = array($temp->id,$image,$temp->name,$temp->email,$temp->mobile,$gender,$temp->dob,$temp->created_at,$status,$action);
                };
            }
            $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   # total data array
            );
            return $json_data; 
    }
    public function details($id){
        if(!$id){
            abort(404);
        }
        $result['user'] = \App\User::find($id)->user;
		$result['login'] = \App\User::find($id);
        return view('admin::users.details', $result);
    }
    public function doctorList(){
        return view('admin::users.doctor');
    }
    public function doctors(Request $request){
            $params = $request->all();
            $columns = array( 
                    0 =>'id',
                    1 =>'image',
                    2 =>'name', 
                    3 => 'email',
                    4 => 'mobile',
                    5 => 'gender',
					6 => 'dob',
					7 => 'created_at',
					8 => 'status',
                    9 => 'action'
            );
            $where="";
            if( !empty($params['search']['value']) ) {   
                $where .=" WHERE ";
                $where .=" ( name LIKE '".$params['search']['value']."%' ";    
                $where .=" OR email LIKE '".$params['search']['value']."%' ";

                $where .=" OR mobile LIKE '".$params['search']['value']."%' )";
            }
            $sqlTot="";
            $sqlRec ="";
            $sql = "SELECT * FROM `login` right join `doctors` on login.id=doctors.user_id ";
            $sqlTot .= $sql;
            $sqlRec .= $sql;
            #concatenate search sql if value exist
            if(isset($where) && $where != '') {
                $sqlTot .= $where;
                $sqlRec .= $where;
            }
            $sqlRec .=  " ORDER BY doctors.". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
            $queryTot = DB::select($sqlTot);
            $totalRecords = count($queryTot);
            $queryRecords =  DB::select($sqlRec);
            #iterate on results row and create new index array of data
            $data = [];
            if(count($queryRecords)>0){
                foreach($queryRecords as $temp){
                    $viewUrl = url('admin/doctor-details/'.$temp->id);
                    $deleteUrl = url('admin/doctor/delete/'.$temp->id);
                    $action = '<a href="'.$viewUrl.'" class="btn btn-nothing" data-toggle="tooltip" data-placement="top" title="View Credits"><i class="icon_pencil-edit"></i></a>'
                            .'<a href="'.$deleteUrl.'" class="btn btn-nothing" data-toggle="tooltip" data-placement="top" title="View Credits"><i class="glyphicon glyphicon-trash"></i></a>';
                    $gender = "Not Updated";
                    if($temp->gender == 1)
                        $gender = "Male";
                    elseif($temp->gender==2)
                        $gender = "Female";
                    $imageUrl = $temp->image?$temp->image:  asset('public/admin/img/user-placeholder.png');                
                    $image = '<img src="'.$imageUrl.'" style="width:50px;height:70px;" alt="No Image" />';
                                    $status = $temp->status?'<span class="label label-success">Approved</span>':'<span class="label label-danger">Dissapproved</span>';
                    $data[] = array($temp->id,$image,$temp->name,$temp->email,$temp->mobile,$gender,$temp->dob,$temp->created_at,$status,$action);
                }
            }
            $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   # total data array
            );
            return $json_data; 
    }
    public function hospitalClinicList(){
        return view('admin::users.hospital_clinic');
    }
    public function hospitalsClinics(Request $request){
            $params = $request->all();
            $columns = array( 
                    0 =>'id',
                    1 =>'image',
                    2 =>'name', 
                    3 => 'address_one',
                    4 => 'phone',
                    5 => 'consultation_fee',
					6 => 'created_at',
					7 => 'status',
                    8 => 'action'
            );
            $where="";
            if( !empty($params['search']['value']) ) {   
                $where .=" WHERE ";
                $where .=" ( name LIKE '".$params['search']['value']."%' ";    
                $where .=" OR address_one LIKE '".$params['search']['value']."%' ";

                $where .=" OR phone LIKE '".$params['search']['value']."%' )";
            }
            $sqlTot="";
            $sqlRec ="";
            $sql = "SELECT * FROM `hospitals` ";
            $sqlTot .= $sql;
            $sqlRec .= $sql;
            #concatenate search sql if value exist
            if(isset($where) && $where != '') {
                $sqlTot .= $where;
                $sqlRec .= $where;
            }
            $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
            $queryTot = DB::select($sqlTot);
            $totalRecords = count($queryTot);
            $queryRecords =  DB::select($sqlRec);
            #iterate on results row and create new index array of data
            $data = [];
            if(count($queryRecords)>0){
                foreach($queryRecords as $temp){
                    $deleteUrl = url('admin/hospital/delete/'.$temp->id);
                    $action = '<a href="'. url('admin/hospital-clinic-details/'.$temp->id).'" class="btn btn-nothing" data-toggle="tooltip" data-placement="top" title="View Details"><i class="icon_pencil-edit"></i></a>'
                            .'<a href="'.$deleteUrl.'" class="btn btn-nothing" data-toggle="tooltip" data-placement="top" title="View Credits"><i class="glyphicon glyphicon-trash"></i></a>';
                    $gender = "Not Updated";
                    $imageUrl = $temp->image?$temp->image:  asset('public/admin/img/user-placeholder.png');                
                    $image = '<img src="'.$imageUrl.'" style="width:50px;height:70px;" alt="No Image" />';
                                    $status = $temp->status?'<span class="label label-success">Approved</span>':'<span class="label label-danger">Dissapproved</span>';
                    $data[] = array($temp->id,$image,$temp->name,$temp->address_one,$temp->phone,$temp->consultation_fee,$temp->created_at,$status,$action);
                };
            }
            $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   # total data array
            );
            return $json_data; 
    }
    public function diagnosticList(){
        return view('admin::users.diagnostic');
    }
    public function diagnostics(Request $request){
            $params = $request->all();
            $columns = array( 
                    0 =>'user_id',
                    1 =>'image',
                    2 =>'name', 
                    3 => 'address_one',
                    4 => 'phone',
                    5 => 'online_reports',
                    6 =>'online_bookings',
					7 => 'created_at',
					8 => 'status',
                    9 => 'action'
            );
            $where="";
            if( !empty($params['search']['value']) ) {   
                $where .=" WHERE ";
                $where .=" ( name LIKE '".$params['search']['value']."%' ";    
                $where .=" OR address_one LIKE '".$params['search']['value']."%' ";

                $where .=" OR phone LIKE '".$params['search']['value']."%' )";
            }
            $sqlTot="";
            $sqlRec ="";
            $sql = "SELECT * FROM `diagnostics`";
            $sqlTot .= $sql;
            $sqlRec .= $sql;
            #concatenate search sql if value exist
            if(isset($where) && $where != '') {
                $sqlTot .= $where;
                $sqlRec .= $where;
            }
            $sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";
            $queryTot = DB::select($sqlTot);
            $totalRecords = count($queryTot);
            $queryRecords =  DB::select($sqlRec);
            #iterate on results row and create new index array of data
            $data = [];
            if(count($queryRecords)>0){
                foreach($queryRecords as $temp){
                    $deleteUrl = url('admin/diagnostic/delete/'.$temp->id);
                    $action = '<a href="'.url('admin/diagnostic-details/'.$temp->id).'" class="btn btn-nothing" data-toggle="tooltip" data-placement="top" title="View Credits"><i class="icon_pencil-edit"></i></a>'
                            .'<a href="'.$deleteUrl.'" class="btn btn-nothing" data-toggle="tooltip" data-placement="top" title="View Credits"><i class="glyphicon glyphicon-trash"></i></a>';
                    $imageUrl = $temp->image?$temp->image:  asset('public/admin/img/user-placeholder.png');                
                    $image = '<img src="'.$imageUrl.'" style="width:50px;height:70px;" alt="No Image" />';                
                    $status = $temp->status?'<span class="label label-success">Approved</span>':'<span class="label label-danger">Dissapproved</span>';
                    $data[] = array($temp->id,$image,$temp->name,$temp->address_one,$temp->phone,$temp->online_reports,$temp->online_bookings,$temp->created_at,$status,$action);
                };
            }
            $json_data = array(
                "draw"            => intval( $params['draw'] ),   
                "recordsTotal"    => intval( $totalRecords ),  
                "recordsFiltered" => intval($totalRecords),
                "data"            => $data   # total data array
            );
            return $json_data; 
    }
    public function profileAccept(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>6]);;
                } 
                $user = \App\User::find($request->input('id'));
                $user->activated = 1;
				$user->status = 1;
                $user->save(); 
                return back()->with(['status'=>1,'message'=>'Accepted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>6]);
        }
    }
    public function profileDeclined(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>5]);;
                } 
                $user = \App\User::find($request->input('id'));
                $user->status = 0;
				$user->activated = 2;
                $user->save();
                return back()->with(['status'=>1,'message'=>'Declined successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>5]);
        }
    }
    public function uploadImage(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>7]);;
                } 
                $image ="";
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('uploaded/images/thumbnail');
                    $img = Image::make($image->getRealPath());
                    $img->resize(100, 100, function ($constraint) {
                            $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$input['imagename']);

                    $destinationPath = public_path('uploaded/images');
                    $image->move($destinationPath, $input['imagename']);
                    $image =url('/').'/public/uploaded/images/'.$input['imagename'];

                }
                $user = \App\User::find($request->input('id'));
                $user->user->image = $image;
                $user->user->save();
                return back()->with(['status'=>1,'message'=>'Uploaded successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>7]);
        }
    }
    public function deleteImage(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>8]);;
                } 
                $user = \App\User::find($request->input('id'));
                $user->user->image = "";
                $user->user->save();
                return back()->with(['status'=>1,'message'=>'Image deleted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function update(Request $request){
        try{
            if ($request->isMethod('post')) { 
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'name' => 'required',
                    'gender' => 'required',
                    'mobile' => 'required',
                    'about' => 'required',
                    'address_one' => 'required',
                    'city_name' => 'required',
                    'state_name' => 'required',
                    'country_name' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>9]);;
                } 
                $user = \App\User::find($request->input('id'))->user;
                $user->name = $request->input('name');
                $user->gender = $request->input('gender');
                $user->mobile = $request->input('mobile');
                $user->about = $request->input('about');
                $user->address_one = $request->input('address_one');
                $user->latitude = $request->input('latitude');
                $user->longitude = $request->input('longitude');
                $user->locality = $request->input('locality');
                $user->city_name = $request->input('city_name');
                $user->state_name = $request->input('state_name');
                $user->country_name = $request->input('country_name');
                $user->save();

                return back()->with(['status'=>1,'message'=>'Profile updated successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>9]);
        }
    }
	public function dataScraper(){
		$url ="https://www.practo.com/bangalore/doctors";
		$practohtml = $this->curl_get_item($url);
		$productdetail = array();			
		$product = $this->getdetail($url,$practohtml,$productdetail);
		
		echo '<pre>';
		print_r($product);
		die;
		
		return $product;
		//return view('admin::users.data-scraper');
	}
	public function curl_get_item($url) { 
		$ch = @curl_init();
		@curl_setopt ($ch, CURLOPT_URL, $url);
		@curl_setopt ($ch, CURLOPT_HEADER, 0);
		@curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		@curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$result = @curl_exec ($ch);
		@curl_close ($ch);  
		return $result;
	}
	private function getdetail($url,$html,$productdetail) {		
		@$dom= new \DOMDocument();
		@$dom->loadHTML($html);
		$result = [];
		
		$xpath = new \DOMXPath(@$dom);
		$tags = $xpath->query('//div[@class="listing-row fm-container"]');
		
		foreach($tags as $tag){
			$singledoctor = array();
			
			$doctorhtml = $dom->saveHTML($tag);
						
			@$newdom= new \DOMDocument();
			@$newdom->loadHTML($doctorhtml);
			
			$xpath = new \DOMXPath(@$newdom);
			$alldoctor = $xpath->query('//h2[@itemprop="name"]');
			foreach($alldoctor as $doctor){
				$singledoctor['doctorname'] = trim($doctor->nodeValue);
			}
			
						
			$xpath = new \DOMXPath(@$newdom);
			$alldoctor = $xpath->query('//p[@class="doc-exp-years"]');
			foreach($alldoctor as $doctor){
				$singledoctor['experience'] = trim($doctor->nodeValue);
			}
			
			$result[] = $singledoctor;
		}	
		return $result;
	}
	public function getNotifications(){
		$users = \App\Model\Users::lists('name','user_id');
		$data = [
			'users' => $users
		];
		return view('admin::users.send-notifications', $data);
	}
	public function notifications(Request $request){
		$user = Auth::user();
		$data = [
			'notifications' => \App\Model\Notification::where('receiver',$user->id)->paginate(10)
		];
		return view('admin::users.notifications', $data);
	}
        public function notificationUnread($id){
            if($id){
                $notification = \App\Model\Notification::find($id);
                $notification->status = 0;
                $notification->save();
                return back();
            }
        }
        public function notificationRead($id){
            if($id){
                $notification = \App\Model\Notification::find($id);
                $notification->status = 1;
                $notification->save();
                return back();
            }
        }
        public function clearNotification($id){
            if($id){
                \App\Model\Notification::destroy($id);
                return back();
            }
        }
        public function clearNotifications($id){
            if(!$id){
              abort(404); 
            }
            $notification = \App\Model\Notification::where('receiver',$id)->delete();
            return back();
        }
	public function PostNotifications(Request $request){
		if ($request->isMethod('post')){
			try{ 
				$validator = Validator::make($request->all(), [
					'ids' => 'required',
					'title' => 'required',
					'message' => 'required'
				]);

				if ($validator->fails()) {
					return back()
						->withInput()
						->withErrors($validator);
				} 
				foreach($request->input('ids') as $id){
					$user = \App\User::find($id);
					###### Sending notification
					$deviceToken = $user->device_token;
					$gcmApiKey = config('constants.gcm_apikey');
					$deviceType = $user->device_type;
					$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
					\App\Model\Notification::create([
						'title'=>$request->input('title'),
						'message'=>$request->input('message'),
						'receiver'=>$id,
						'type'=>0,
						'status'=>0
					]);
					# Sending user notification
					$notifications= $notifications->send([
						'message'=>$request->input('message'),
						'title'		=> $request->input('title'),
						'subtitle'	=> $request->input('title'),
						'tickerText'	=> 'Medrator',
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon'
					]);
				}
				return redirect('admin/user/send-notifications')
					->with('message', 'Notification sent successfully.');
			} catch(\Illuminate\Database\QueryException $e){
				abor(505);        
			}
		}
	}
	public function resetNotifications(){
            try{ 
                    $user = Auth::user();
                    Notification::where('receiver',$user->id)->update(['status'=>1]);
                    return back()
                            ->with('message', 'Notification deleted successfully.');
            } catch(\Illuminate\Database\QueryException $e){
                    abor(505);        
            }
	}
	public function pendingApproval(){
		$sql = "SELECT id,name,image,mobile, 2 as role,status,created_at FROM doctors WHERE status =0 OR status =2 UNION SELECT id,name,image,phone, 5 as role,status,created_at FROM hospitals WHERE status =0 OR status =2 UNION SELECT id,name,image,phone, 4 as role,status,created_at  FROM diagnostics WHERE status =0 OR status =2 ORDER BY name";
		$results =  DB::select($sql);
		$data = [
			'results' => $results
		];
		return view('admin::users.pending-approval',$data);
	}
	public function approveDoctor(Request $request){
            if(!$request->has('id')){
                abort(404);
            }
            if($request->isMethod('post')){
                $doctor = \App\Model\Doctor::find($request->id);
                $doctor->status = 1;
                $doctor->save();
				$user = [];
				if($doctor->user_id){
					$user = \App\User::find($doctor->user_id);
				}
				if($doctor->recommend_by){
					$user = \App\User::find($doctor->recommend_by);
					/**
					 * Sending mail to user.
					 */
					$data = [
						'email' => $user->email,
						'user_name'=>$user->user->name,
						'entity_name'=>$doctor->name
					];
					Mail::send('email.new-entity-approved',['data'=>$data], function ($message) use ($data) {
						$message->to($data['email'])->subject('Profile approved');
					});
					
				}
				return back()->with(['status'=>1,'message'=>'Approved successfully.']);
            }
            $user = Auth::user();
            $result['user'] = $user;
            $result['doctor'] = \App\Model\Doctor::find($request->id);
            $result['specialties'] =  Specialty::lists('name','id');
            $result['qualifications'] = \App\Model\Qualifications::lists('name','id');
            $result['services'] =  Service::lists('name','id');
            return view('admin::users.doctor-details',$result);
        }
        public function approveHospital(Request $request){
            if(!$request->has('id')){
                abort(404);
            } 
            if($request->isMethod('post')){
                $hospital = \App\Model\Hospital::find($request->id);
                $hospital->status = 1;
                $hospital->save();
				/****
				@ Sending claim mail to user
				*/
				$user = [];
				if($hospital->user_id){
					$user = \App\User::find($hospital->user_id);
				}
				if($hospital->recommend_by){
					$user = \App\User::find($hospital->recommend_by);
					$data = [
						'email' => $user->email,
						'user_name'=>$user->user->name,
						'entity_name'=>$hospital->name
					];
					Mail::send('email.new-entity-approved',['data'=>$data], function ($message) use ($data) {
						$message->to($data['email'])->subject('Profile approved');
					});
				}
				return back()->with(['status'=>1,'message'=>'Approved successfully.']);
            }
            $user = Auth::user();
            $result['user'] = $user;
            $result['hospital'] = \App\Model\Hospital::find($request->id);
            $result['specialties'] =  Specialty::lists('name','id');
            $result['services'] =  Service::lists('name','id');
            return view('admin::users.hospital-details',$result);
	}
        public function approveDiagnostic(Request $request){
            if(!$request->has('id')){
            abort(404);
            }
            if($request->isMethod('post')){
                $diagnostic = \App\Model\Diagnostic::find($request->id);
                $diagnostic->status = 1;
                $diagnostic->save();
				/****
				@ Sending claim mail to user
				*/
				$user = [];
				if($diagnostic->user_id){
					$user = \App\User::find($diagnostic->user_id);
				}
				if($diagnostic->recommend_by){
					$user = \App\User::find($diagnostic->recommend_by);
					$data = [
						'email' => $user->email,
						'user_name'=>$user->user->name,
						'entity_name'=>$diagnostic->name
					];
					Mail::send('email.new-entity-approved',['data'=>$data], function ($message) use ($data) {
						$message->to($data['email'])->subject('Profile approved');
					});
				}
				return back()->with(['status'=>1,'message'=>'Approved successfully.']);
            }
			
            $user = Auth::user();
            $result['user'] = $user;
            $result['diagnostic'] = \App\Model\Diagnostic::find($request->id);
            $result['tests'] = \App\Model\Test::lists('name','id');
            return view('admin::users.diagnostic-details',$result);
	}
	public function declineDoctor(Request $request){
            if(!$request->has('id')){
                abort(404);
            }
			if($request->isMethod('post')){
                $doctor = \App\Model\Doctor::find($request->id);
                $doctor->status = 2;
                $doctor->save();
				/**
				 * Sending mail to user.
				 */
				$user = [];
				if($doctor->user_id){
					$user = \App\User::find($doctor->user_id);
				}
				if($doctor->recommend_by){
					$user = \App\User::find($doctor->recommend_by);
					$data = [
						'email' => $user->email,
						'user_name'=>$user->user->name,
						'entity_name'=>$doctor->name
					];
					Mail::send('email.new-entity-declined',['data'=>$data], function ($message) use ($data) {
						$message->to($data['email'])->subject('Profile approved');
					});
				
				}
				return back()->with(['status'=>1,'message'=>'Delined successfully.']);
            }
			$user = Auth::user();
            $result['user'] = $user;
            $result['doctor'] = \App\Model\Doctor::find($request->id);
            $result['specialties'] =  Specialty::lists('name','id');
            $result['qualifications'] = \App\Model\Qualifications::lists('name','id');
            $result['services'] =  Service::lists('name','id');
            return view('admin::users.doctor-details',$result);
        }
        public function declineHospital(Request $request){
            if(!$request->has('id')){
                abort(404);
            }
			if($request->isMethod('post')){
                $hospital = \App\Model\Hospital::find($request->id);
                $hospital->status = 2;
                $hospital->save();
				/****
				@ Sending claim mail to user
				*/
				$user = [];
				if($hospital->user_id){
					$user = \App\User::find($hospital->user_id);
				}
				if($hospital->recommend_by){
					$user = \App\User::find($hospital->recommend_by);
					$data = [
						'email' => $user->email,
						'user_name'=>$user->user->name,
						'entity_name'=>$hospital->name
					];
					Mail::send('email.new-entity-declined',['data'=>$data], function ($message) use ($data) {
						$message->to($data['email'])->subject('Profile approved');
					});
				}
				return back()->with(['status'=>1,'message'=>'Delined successfully.']);
            }
            $user = Auth::user();
            $result['user'] = $user;
            $result['hospital'] = \App\Model\Hospital::find($request->id);
            $result['specialties'] =  Specialty::lists('name','id');
            $result['services'] =  Service::lists('name','id');
            return view('admin::users.hospital-details',$result);
	}
	public function declineDiagnostic(Request $request){
            if(!$request->has('id')){
            abort(404);
            }
			if($request->isMethod('post')){
                $diagnostic = \App\Model\Diagnostic::find($request->id);
                $diagnostic->status = 2;
                $diagnostic->save();
				/****
				@ Sending claim mail to user
				*/
				$user = [];
				if($diagnostic->user_id){
					$user = \App\User::find($diagnostic->user_id);
				}
				if($diagnostic->recommend_by){
					$user = \App\User::find($diagnostic->recommend_by);
					$data = [
						'email' => $user->email,
						'user_name'=>$user->user->name,
						'entity_name'=>$diagnostic->name
					];
					Mail::send('email.new-entity-declined',['data'=>$data], function ($message) use ($data) {
						$message->to($data['email'])->subject('Profile approved');
					});
				}
				return back()->with(['status'=>1,'message'=>'Delined successfully.']);
            }
			$user = Auth::user();
            $result['user'] = $user;
            $result['diagnostic'] = \App\Model\Diagnostic::find($request->id);
            $result['tests'] = \App\Model\Test::lists('name','id');
            return view('admin::users.diagnostic-details',$result);
	}
	public function approveAll(Request $request){
		if($request->isMethod('post')){
			$ids = $request->ids;
			if(empty($ids)){
				return back()->with(['status'=>1,'message'=>'Select atleast one record.']);
			}
			if($request->action ==1){
				foreach($ids as $id){
					$doctor = \App\Model\Doctor::find($id);
					$hospital = \App\Model\Hospital::find($id);
					$diagnostic = \App\Model\Diagnostic::find($id);
					if(count($doctor)>0){
						$doctor->status = 1;
						$doctor->save();
						$user = [];
						if($doctor->user_id){
							$user = \App\User::find($doctor->user_id);
						}
						if($doctor->recommend_by){
							$user = \App\User::find($doctor->recommend_by);
							/**
							 * Sending mail to user.
							 */
							$data = [
								'email' => $user->email,
								'user_name'=>$user->user->name,
								'entity_name'=>$doctor->name
							];
							Mail::send('email.new-entity-approved',['data'=>$data], function ($message) use ($data) {
								$message->to($data['email'])->subject('Profile approved');
							});
						}
					}
					if(count($hospital)>0){
						$hospital->status = 1;
						$hospital->save();
						/****
						@ Sending claim mail to user
						*/
						$user = [];
						if($hospital->user_id){
							$user = \App\User::find($hospital->user_id);
						}
						if($hospital->recommend_by){
							$user = \App\User::find($hospital->recommend_by);
							$data = [
								'email' => $user->email,
								'user_name'=>$user->user->name,
								'entity_name'=>$hospital->name
							];
							Mail::send('email.new-entity-approved',['data'=>$data], function ($message) use ($data) {
								$message->to($data['email'])->subject('Profile approved');
							});
						}					
					}
					if(count($diagnostic)>0){
						$diagnostic->status = 1;
						$diagnostic->save();
						/****
						@ Sending claim mail to user
						*/
						$user = [];
						if($diagnostic->user_id){
							$user = \App\User::find($diagnostic->user_id);
						}
						if($diagnostic->recommend_by){
							$user = \App\User::find($diagnostic->recommend_by);
							$data = [
								'email' => $user->email,
								'user_name'=>$user->user->name,
								'entity_name'=>$diagnostic->name
							];
							Mail::send('email.new-entity-approved',['data'=>$data], function ($message) use ($data) {
								$message->to($data['email'])->subject('Profile approved');
							});
						}
					}
				}
				return back()->with(['status'=>1,'message'=>'Approved successfully.']);
			}
			if($request->action ==2){
				foreach($ids as $id){
					$doctor = \App\Model\Doctor::find($id);
					$hospital = \App\Model\Hospital::find($id);
					$diagnostic = \App\Model\Diagnostic::find($id);
					if(count($doctor)>0){
						$doctor = \App\Model\Doctor::find($request->id);
						$doctor->status = 2;
						$doctor->save();
						/**
						 * Sending mail to user.
						 */
						$user = [];
						if($doctor->user_id){
							$user = \App\User::find($doctor->user_id);
						}
						if($doctor->recommend_by){
							$user = \App\User::find($doctor->recommend_by);
						}
						$data = [
							'email' => $user->email,
							'user_name'=>$user->user->name,
							'entity_name'=>$doctor->name
						];
						Mail::send('email.new-entity-declined',['data'=>$data], function ($message) use ($data) {
							$message->to($data['email'])->subject('Profile approved');
						});
					}
					if(count($hospital)>0){
						$hospital = \App\Model\Hospital::find($request->id);
						$hospital->status = 2;
						$hospital->save();
						/****
						@ Sending claim mail to user
						*/
						$user = [];
						if($hospital->user_id){
							$user = \App\User::find($hospital->user_id);
						}
						if($hospital->recommend_by){
							$user = \App\User::find($hospital->recommend_by);
						}
						$data = [
							'email' => $user->email,
							'user_name'=>$user->user->name,
							'entity_name'=>$hospital->name
						];
						Mail::send('email.new-entity-declined',['data'=>$data], function ($message) use ($data) {
							$message->to($data['email'])->subject('Profile approved');
						});
					}
					if(count($diagnostic)>0){
						$diagnostic = \App\Model\Diagnostic::find($request->id);
						$diagnostic->status = 2;
						$diagnostic->save();
						/****
						@ Sending claim mail to user
						*/
						$user = [];
						if($diagnostic->user_id){
							$user = \App\User::find($diagnostic->user_id);
						}
						if($diagnostic->recommend_by){
							$user = \App\User::find($diagnostic->recommend_by);
						}
						$data = [
							'email' => $user->email,
							'user_name'=>$user->user->name,
							'entity_name'=>$diagnostic->name
						];
						Mail::send('email.new-entity-declined',['data'=>$data], function ($message) use ($data) {
							$message->to($data['email'])->subject('Profile approved');
						});
					}
				}
				return back()->with(['status'=>1,'message'=>'Delined successfully.']);
			}		
		}
	}
}
