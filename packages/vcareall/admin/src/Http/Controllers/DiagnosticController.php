<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;

/**
 * Description of DiagnosticController
 *
 * @author VCareAll
 */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Service;
use App\Model\Specialty;
use Illuminate\Support\Facades\Validator;
use Image;
use App\Model\Test;
class DiagnosticController extends Controller{
    public function details($id){
        if(!$id){
            abort(404);
        }
        $result['diagnostic'] = \App\Model\Diagnostic::find($id);
        $result['tests'] = Test::lists('name','id');
        return view('admin::diagnostic.details',$result);
    }
    public function addTests(Request $request){
        try{
            if ($request->isMethod('post')) {
                
                $validator = Validator::make($request->all(), [
                    'test_id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>3]);;
                }   
                $tests = [];
                for($i=0; $i<count($request->input('test_id')); $i++){
                    $tests[$i] = new \App\Model\DiagnosticTest(['test_id'=>$request->input('test_id')[$i]]);
                }
                $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
                $diagnostic->test()->saveMany($tests);
                return back()->with(['status'=>1,'message'=>'Test addes successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>3]);
        }
    }
    public function deleteTest(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>4]);;
                } 
                \App\Model\DiagnosticTest::where('test_id',$request->input('id'))->where('diagnostic_id',$request->input('diagnostic_id'))->delete(); 
                return back()->with(['status'=>1,'message'=>'Test deleted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>4]);
        }
    }
    public function profileAccept(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>6]);;
                } 
                $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
                $diagnostic->status = 1;
                $diagnostic->save(); 
				###### Sending notification
				$user = \App\User::find($diagnostic->user_id);
				if($user){
					$deviceToken = $user->device_token;
					$gcmApiKey = config('constants.gcm_apikey');
					$deviceType = $user->device_type;
					$name = $diagnostic->name;
					$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
					\App\Model\Notification::create([
						'title'=>config('message.profile_notification_approved_title'),
						'message'=>str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
						'receiver'=>$diagnostic->user_id,
						'type'=>0,
						'status'=>0
					]);
					# Sending user notification
					$notifications= $notifications->send([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'tickerText'	=> 'Medrator',
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon'
					]);
					/**
					 * Sending admin notification.
					 */
					$this->helper->sendAdminNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
					]);
					/**
					 * Sending moderator notification.
					 */
					$this->helper->sendModeratorNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
					]);
				}	
                return back()->with(['status'=>1,'message'=>'Accepted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>6]);
        }
    }
    public function profileDeclined(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>5]);;
                } 
                $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
                $diagnostic->status = 0;
                $diagnostic->save();
				###### Sending notification
				$user = \App\User::find($diagnostic->user_id);
				if($user){
					$deviceToken = $user->device_token;
					$gcmApiKey = config('constants.gcm_apikey');
					$deviceType = $user->device_type;
					$name = $diagnostic->name;
					$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
					\App\Model\Notification::create([
						'title'=>config('message.profile_notification_desclined_title'),
						'message'=>str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
						'receiver'=>$diagnostic->user_id,
						'type'=>0,
						'status'=>0
					]);
					# Sending user notification
					$notifications= $notifications->send([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'tickerText'	=> 'Medrator',
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon'
					]);
					/**
					 * Sending admin notification.
					 */
					$this->helper->sendAdminNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
					]);
					/**
					 * Sending moderator notification.
					 */
					$this->helper->sendModeratorNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
					]);
				}	
                return back()->with(['status'=>1,'message'=>'Declined successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>5]);
        }
    }
    public function uploadImage(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>7]);;
                } 
                $image ="";
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('uploaded/images/thumbnail');
                    $img = Image::make($image->getRealPath());
                    $img->resize(100, 100, function ($constraint) {
                            $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$input['imagename']);

                    $destinationPath = public_path('uploaded/images');
                    $image->move($destinationPath, $input['imagename']);
                    $image =url('/').'/public/uploaded/images/'.$input['imagename'];

                }
                $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
                $diagnostic->image = $image;
                $diagnostic->save();
                return back()->with(['status'=>1,'message'=>'Uploaded successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>7]);
        }
    }
    public function deleteImage(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>8]);;
                } 
                $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
                $diagnostic->image = "";
                $diagnostic->save();
                return back()->with(['status'=>1,'message'=>'Image deleted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function update(Request $request){
        try{
            if ($request->isMethod('post')) { 
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'name' => 'required',                  
                    'address_one' => 'required',
                    'city_name' => 'required',
                    'state_name' => 'required',
                    'country_name' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>9]);;
                } 
                $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
                $diagnostic->name = $request->input('name');
                $diagnostic->home_collection_facility = $request->input('home_collection_facility');
                $diagnostic->online_bookings = $request->input('online_bookings');
                $diagnostic->online_reports = $request->input('online_reports');
                $diagnostic->address_one = $request->input('address_one');
                $diagnostic->latitude = $request->input('latitude');
                $diagnostic->longitude = $request->input('longitude');
                $diagnostic->locality = $request->input('locality');
                $diagnostic->city_name = $request->input('city_name');
                $diagnostic->state_name = $request->input('state_name');
                $diagnostic->country_name = $request->input('country_name');
				$diagnostic->website = $request->input('website');
                $diagnostic->save();
                return back()->with(['status'=>1,'message'=>'Profile updated successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>9]);
        }
    }
    public function delete($id){
        if(!$id){
            abort(404);
        }
        $diagnostic = \App\Model\Diagnostic::find($id);
        if($diagnostic->user_id){
            $user = \App\User::find($doctor->user_id);
            $user->delete();
            $user->roles()->detach();
            //$user->roles()->attach(2);
        }
        $diagnostic->tests()->delete();
        $diagnostic->reviews()->delete();
		$$diagnostic->schedular()->delete();
        $diagnostic->delete();
        return back()
            ->with(['status'=>1,'message'=>'Deleted successfully.']);
    }
    public function create(){
        $result['tests'] =  Test::lists('name','id');
        return view('admin::diagnostic.create',$result);
    }
    public function save(Request $request){
        try{
            if ($request->isMethod('post')) { 
                $validator = Validator::make($request->all(), [
                    'name' => 'required',                  
                    'address_one' => 'required',
                    'tests' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>$validator->errors()->first()]);
                } 
                $diagnostic = \App\Model\Diagnostic::create([
                    'name' => $request->input('name'),
                    'home_collection_facility' => $request->input('home_collection_facility'),
                    'online_bookings' => $request->input('online_bookings'),
                    'online_reports' => $request->input('online_reports'),
                    'address_one' => $request->input('address_one'),
                    'latitude' => $request->input('latitude'),
                    'longitude' => $request->input('longitude'),
                    'locality' => $request->input('locality'),
                    'city_name' => $request->input('city_name'),
                    'state_name' => $request->input('state_name'),
                    'country_name' => $request->input('country_name'),
                    'website' => $request->input('website')
                ]);
                /**
                 * Adding diagnostic test
                 */
                $diagnostic->tests()->detach();
                $tests =[];
                foreach($request->tests as $var){
                     $tests[] = array('test_id'=>$var);
                }
                $diagnostic->tests()->attach($tests);
                return back()->with(['status'=>1,'message'=>'Profile added successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            abort(405,$e->getMessage());
        }
    }
}
