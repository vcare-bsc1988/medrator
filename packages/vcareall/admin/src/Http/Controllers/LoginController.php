<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;

/**
 * Description of LoginController
 *
 * @author VCareAll
 */
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Validator;
class LoginController extends Controller {
   public function index(){
       return view('admin::login');
   }
   public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password'	=> 'required',
        ]);
        if ($validator->fails()) {
            return redirect('admin')
                ->withInput()
                ->withErrors($validator);
        }
        $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials, $request->has('remember'))){
            ## WP Administration too login
            # wordpress code start here
            $wp_login = \wp_signon(array('user_login'=>$request->input('username'),'user_password'=>$request->input('password')));
			return redirect('admin/dashboard')
                ->with('success', 'Welcome to admin dashboard.');
        }else{
                return back()->with('failed', 'Invalid Credential.');
        }
   }
   public function logout(){
       Auth::logout();
       return redirect('admin')
            ->with('message', 'Logged out successfully.');
   }
}
