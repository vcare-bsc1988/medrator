<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;
use App\Http\Controllers\Controller;
/**
 * Description of ScrapperController
 *
 * @author VCareAll
 */
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use DB;
use App\Model\Doctor;
use App\Model\Hospital;
use App\Model\Diagnostic;
use App\Model\DiagnosticTest;
use App\Model\SpecialtyDetail;
use App\Model\ServiceDetails;
use App\Model\Qualifications;
use App\Model\College;
use Vcareall\Admin\Helper;
use App\Model\WorkingDatetimes;
class ScrapperController extends Controller {
    var $baseUrl;
	function __construct() {
       $this->baseUrl = 'https://www.practo.com/';
	}
	public function dataScraper(){
        /*$practohtml = $this->curl_get_item($url);
        $productdetail = array();			
        $product = $this->getdetail($url,$practohtml,$productdetail);

        echo '<pre>';
        print_r($product);
        die;

        return $product;*/
		$specialities = $this->getDoctorCategories($this->baseUrl);
		$data = [
			'cities'=> $this->getCities(),
			'categories'=> $specialities
		];
        return view('admin::users.data-scraper',$data);
    }
	public function itemsCount(Request $request){
		if ($request->isMethod('post')) {
			$validator = Validator::make($request->all(), [
				'city' => 'required',
				'speciality' => 'required',
			]);
			if ($validator->fails()) {
				return back()
					->withInput()
					->withErrors($validator)
					->with(['status'=>0,'message'=>$validator->errors()->first(),'type'=>0]);;
			}
			###### Finding total number of items on given links
			$cities= $this->getCities();
			$specialities = $this->getDoctorCategories($this->baseUrl);
			//array_pop($specialities);
			$url = $this->baseUrl.$cities[$request->input('city')].'/'.$specialities[$request->input('speciality')];

			$practohtml = $this->curl_get_item($url);
			$totalItems = $this->findTotalItems($practohtml);
			#### Finding total no of page
			$pages = round($totalItems/10);			
			$data =[
				'city'=> $cities[$request->input('city')],
				'speciality' => $specialities[$request->input('speciality')],
				'tolta_items' =>$totalItems,
				'pages' =>$pages,
				'item_per_page' =>10
			];
			//return $data; 
			return view('admin::scrap-items-count',$data);
		}
	}
	public function scrap(Request $request){ 
		ini_set('memory_limit', '-1');  
		if ($request->isMethod('post')) {
			$validator = Validator::make($request->all(), [
				'city' => 'required',
				'speciality' => 'required',
				//'page_from' => 'required',
				//'page_to' => 'required'
			]);
			if ($validator->fails()) {
				return back()
					->withInput()
					->withErrors($validator)
					->with(['status'=>0,'message'=>$validator->errors()->first(),'type'=>0]);;
			}  
			$cities= $this->getCities();
			$specialities = $this->getDoctorCategories($this->baseUrl);
			array_pop($specialities);
			
			//$url = $this->baseUrl.$cities[$request->input('city')].'/'.$specialities[$request->input('speciality')];
			
			$url = $this->baseUrl.$request->input('city').'/'.$request->input('speciality');
			
			###### Finding total number of items on given links
			$practohtml = $this->curl_get_item($url);
			$totalItems = $this->findTotalItems($practohtml);
			$pages = round($totalItems/10);
			$resultDoctors = array();
			if($pages>1){
				#### Paggination
				$pageFrom = $request->input('page_from');
				$pageTo = $request->input('page_to');				
				for($i=$pageFrom; $i<=$pageTo; $i++){
					if($i==1)
						$newurl = $url;
					else
						$newurl = $url.'?page='.$i;
					$productdetail = array();
					$practonewhtml = $this->curl_get_item($newurl);
					$doctors = $this->getdetail($newurl,$practonewhtml,$productdetail);
					$resultDoctors = array_merge($resultDoctors,$doctors);
				} 
			}else{
				$newurl = $url;
				$productdetail = array();
				$practonewhtml = $this->curl_get_item($newurl);
				$doctors = $this->getdetail($newurl,$practonewhtml,$productdetail);
				$resultDoctors = array_merge($resultDoctors,$doctors);
			}
			/*$resultDoctors = array();
			$page = $request->input('page_no');
			if($page==1)
				$newurl = $url;
			else
				$newurl = $url.'?page='.$page;
			$productdetail = array();
			$practonewhtml = $this->curl_get_item($newurl);
			$doctors = $this->getdetail($newurl,$practonewhtml,$productdetail);
			$resultDoctors = array_merge($resultDoctors,$doctors);*/
			
			//$this->pr($resultDoctors);
			Excel::create('doctors-data', function($excel) use($resultDoctors) {
				$excel->sheet('data', function($sheet) use($resultDoctors) {
					$sheet->loadView('admin::scrap-export',['doctors'=>$resultDoctors]);
				});
			})->store('csv', storage_path('excel/exports'), true);
			return response()->download(storage_path('excel/exports/doctors-data.csv'));
		}
	}
	protected function pr($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>"; 
		exit;
	}
    public function curl_get_item($url) { 
        $ch = @curl_init();
        @curl_setopt ($ch, CURLOPT_URL, $url);
        @curl_setopt ($ch, CURLOPT_HEADER, 0);
        @curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        @curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
        @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result = @curl_exec ($ch);
        @curl_close ($ch);  
        return $result;
    }
    private function getdetail($url,$html,$productdetail) {		
		@$dom= new \DOMDocument();
        @$dom->loadHTML($html);
        $result = [];

        $xpath = new \DOMXPath(@$dom);
        $tags = $xpath->query('//div[@class="listing-row fm-container"]');
        foreach($tags as $tag){
                $singledoctor = array();

                $doctorhtml = $dom->saveHTML($tag);

                @$newdom= new \DOMDocument();
                @$newdom->loadHTML($doctorhtml);

                $xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//h2[@itemprop="name"]');
                foreach($alldoctor as $doctor){
                        $singledoctor['doctorname'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($doctor->nodeValue));
                }
                $xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//p[@class="doc-exp-years"]');
                foreach($alldoctor as $doctor){
                        $singledoctor['experience'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($doctor->nodeValue));
                }
				### Finding galleries 
				$xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//p[@class="doc-clinic-photos"]/a');
                foreach($alldoctor as $doctor){
					$galleries[] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($doctor->getAttribute('href')));
                }
				$singledoctor['galleries'] = json_encode(@$galleries);
				### Finding locality 
				$xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//p[@class="locality"]/a');
                foreach($alldoctor as $doctor){
					$singledoctor['locality'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($doctor->nodeValue));
                }
				### Finding latitude 
				$singledoctor['latitude'] =0;
				$xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//span[@itemprop="geo"]/meta[@itemprop="latitude"]');
                foreach($alldoctor as $doctor){
					$singledoctor['latitude'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($doctor->getAttribute('content')));
                }
				### Finding longitude 
				$singledoctor['longitude'] =0;
				$xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//span[@itemprop="geo"]/meta[@itemprop="longitude"]');
                foreach($alldoctor as $doctor){
					$singledoctor['longitude'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($doctor->getAttribute('content')));
                }
				### Finding doctor fee 
				$xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//p[@class="fees"]/span');
                foreach($alldoctor as $doctor){
					$singledoctor['cosulting_fee'] = trim(preg_replace('/[ ]{2,}|[\t\n,]/', '', trim($doctor->nodeValue)), ',');
                }
		        ### Finding doctor schedular 
                $schedule = [];
                $xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//meta[@itemprop="openingHours"]');
                foreach($alldoctor as $doctor){
                    $schedule[] =strtoupper(trim($doctor->getAttribute('content')));
                }
                $singledoctor['shedules'] = json_encode($schedule);
                ### Finding review count 
                $xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//p[@class="reviews-count"]/a');
                foreach($alldoctor as $doctor){
                    $singledoctor['review_count'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($doctor->nodeValue));
                }
				
                ####### Finding doctor details page descriptions
                $xpath = new \DOMXPath(@$newdom);
                $alldoctor = $xpath->query('//a[@itemprop="url"]');
                foreach($alldoctor as $doctor){
                    $detailsUrl = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($doctor->getAttribute('href')));
                }
                ## Load details Html
                $detailshtml = $this->curl_get_item($detailsUrl);
                @$detailsdom= new \DOMDocument();
                @$detailsdom->loadHTML($detailshtml);

                ## Finding about
                $xpath = new \DOMXPath(@$detailsdom);
                $details = $xpath->query('//span[@id="summaryTextFull"]');
                    foreach($details as $detail){
                    $singledoctor['about'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                }
                ## Finding votes
                $xpath = new \DOMXPath(@$detailsdom);
                $details = $xpath->query('//span[@class="doctor-votes"]');
                foreach($details as $detail){
                    $singledoctor['doctor-votes'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                }
                ## Finding verification level
                $xpath = new \DOMXPath(@$detailsdom);
                $details = $xpath->query('//span[@class="verification-label"]');
                    foreach($details as $detail){
                    $singledoctor['verification-level'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                }
                ## Finding review/rating
                $xpath = new \DOMXPath(@$detailsdom);
                $reviews = $xpath->query('//div[@class="info-tab-review-card"]');
                $reviewAll = [];
                foreach($reviews as $tag){
                        $reviewhtml = $detailsdom->saveHTML($tag);
                        @$reviewdom= new \DOMDocument();
                        @$reviewdom->loadHTML($reviewhtml);

                        $xpath = new \DOMXPath(@$reviewdom);
                        $details = $xpath->query('//div[@itemprop="name"]');
                        foreach($details as $detail){
                                $review['name'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                        }
                        $xpath = new \DOMXPath(@$reviewdom);
                        $details = $xpath->query('//span[@class="review-time"]');
                        foreach($details as $detail){
                                $review['review-time'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                        }
                        $xpath = new \DOMXPath(@$reviewdom);
                        $details = $xpath->query('//div[@class="visited-for"]');
                        foreach($details as $detail){
                                $review['visited-for'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                        }
                        $xpath = new \DOMXPath(@$reviewdom);
                        $details = $xpath->query('//div[@class="review-text"]');
                        foreach($details as $detail){
                                $review['review-text'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                        }
                        $reviewAll[]= $review;					
                }
                $singledoctor['reviews'] =  preg_replace('/\s\s+/', ' ', stripslashes(json_encode($reviewAll)));
                ### Finding Services
                $xpath = new \DOMXPath(@$detailsdom);
                $profileInfos = $xpath->query('//div[@class="tabs info-tab"]');
                $services = [];
                $specialties = [];
                $qualifications = [];
                $experiences = [];
                $awards = [];
                $memberships = [];
                $registrations = [] ;
                foreach($profileInfos as $tag){
                    $profilehtml = $detailsdom->saveHTML($tag);
                    @$profiledom= new \DOMDocument();
                    @$profiledom->loadHTML($profilehtml);
                    
                    ## Finding services
                    $xpath = new \DOMXPath(@$profiledom);
                    $detailService = $xpath->query('//div[@itemprop="availableService"]');
                    foreach($detailService as $tag){ 
                        $servicehtml = @$profiledom->saveHTML($tag);
                        @$servicedom= new \DOMDocument();
                        @$servicedom->loadHTML($servicehtml);
                        $xpath = new \DOMXPath(@$servicedom);
                        $details = $xpath->query('//div[@class="service-row responsive-hide  "]/div[@itemprop="name"]');
                        foreach($details as $detail){ 
                            $services[] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                        }
                    }
                    if(empty($services)){
                        ## Finding services
                        $xpath = new \DOMXPath(@$profiledom);
                        $detailService = $xpath->query('//div[@class="services-block"]/div/div/div[@itemprop="name"]/a');
                        foreach($detailService as $detail){ 
                            $services[] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                        }
                    }
                    ## Finding specialties
                    $xpath = new \DOMXPath(@$profiledom);
                    $details = $xpath->query('//div[@class="doc-info-section specialties-block"]/p/a');
                    foreach($details as $detail){ 
                            $specialties[] = stripslashes(preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue)));	
                    }
					if(count($specialties<1)){
						$details = $xpath->query('//div[@class="doc-info-section specialties-block"]/h2/a');
						foreach($details as $detail){ 
							 $specialty = stripslashes(preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue)));
							 $spArr = explode('/',$specialty);	
							 if(count($spArr)){
								 foreach($spArr as $temp){
									 $specialties[] = trim($temp);
								 }
							 }else{
								$specialties[] = $specialty;
							 }
						}
					}
					## Finding Educations 
                    $xpath = new \DOMXPath(@$profiledom);
                    $details = $xpath->query('//div[@class="doc-info-section qualifications-block"]/p');
                    foreach($details as $detail){ 
                            $qualifications[] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));	
                    }
                    ## Finding experiences
                    $xpath = new \DOMXPath(@$profiledom);
                    $details = $xpath->query('//div[@class="doc-info-section organizations-block"]/div');
                    foreach($details as $detail){ 
                            $experiences[] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));	
                    }
                    ## Finding awards
                    $xpath = new \DOMXPath(@$profiledom);
                    $details = $xpath->query('//div[@class="doc-info-section awards-block"]/p');
                    foreach($details as $detail){ 
                            $awards[] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                    }

                    ## Finding Memberships
                    $xpath = new \DOMXPath(@$profiledom);
                    $details = $xpath->query('//div[@class="doc-info-section memberships-block"]/p');
                    foreach($details as $detail){ 
                            $memberships[] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                    }

                    ## Finding Registration
                    $xpath = new \DOMXPath(@$profiledom);
                    $details = $xpath->query('//div[@class="doc-info-section registrations-block"]/div[@class="registration-row responsive-hide  last"]');
                    foreach($details as $detail){ 
                            $registrations[] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                    }
                }							
                $singledoctor['services'] =  preg_replace('/\s\s+/', ' ', stripslashes(json_encode(@$services)));
                $singledoctor['specialties'] =  preg_replace('/\s\s+/', ' ', stripslashes(json_encode(@$specialties)));
                $singledoctor['qualifications'] =  preg_replace('/\s\s+/', ' ', stripslashes(json_encode(@$qualifications)));
                $singledoctor['awards'] =  preg_replace('/\s\s+/', ' ', stripslashes(json_encode(@$awards)));
                $singledoctor['memberships'] =  preg_replace('/\s\s+/', ' ', stripslashes(json_encode(@$memberships)));
                $singledoctor['registrations'] = preg_replace('/\s\s+/', ' ', stripslashes(json_encode(@$registrations)));
                $singledoctor['experiences'] = preg_replace('/\s\s+/', ' ', stripslashes(json_encode(@$experiences)));;
                #### Finding associated clinics				
                $allclinics =array();
                $xpath = new \DOMXPath(@$detailsdom);
                $profileInfos = $xpath->query('//div[@class="doctor-practice-container doctor-info-section u-white-fill"]/div[@itemprop="memberOf"]');
                foreach($profileInfos as $tag){
                        $clinics = array();
                        $clinicstml = $detailsdom->saveHTML($tag);
                        @$clinicstmldom= new \DOMDocument();
                        @$clinicstmldom->loadHTML($clinicstml);

                        ## Finding address
                        $xpath = new \DOMXPath(@$clinicstmldom);
                        $details = $xpath->query('//div[@class="clinic-locality"]');
                        foreach($details as $detail){ 
                                $clinics['locality'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                        }
                        ## Finding name
                        $xpath = new \DOMXPath(@$clinicstmldom);
                        $details = $xpath->query('//div[@class="clinic-address"]/h2/a');
                        foreach($details as $detail){ 
                                $clinics['name'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->nodeValue));
                                $clinics['url'] = preg_replace('/[ ]{2,}|[\t\n]/', ' ', trim($detail->getAttribute('href')));
                        }
                        ## Finding complete address
                        $xpath = new \DOMXPath(@$clinicstmldom);
                        $details = $xpath->query('//p[@class="clinic-street-address"]');
                        foreach($details as $detail){ 
                                $clinics['fulladdress'] = preg_replace('/[ ]{2,}|[\t\n\#\:]/', ' ', trim($detail->nodeValue));
                        }
                        $addComp = explode('Landmark',$clinics['fulladdress']);
                        $clinics['fulladdress'] = trim(@$addComp[0]).' '.trim(@$addComp[1]);

                        ## Finding latitude
                        $clinics['latitude']=0;
                        $xpath = new \DOMXPath(@$clinicstmldom);
                        $details = $xpath->query('//span[@itemprop="geo"]/meta[@itemprop="latitude"]');
                        foreach($details as $detail){ 
                                $clinics['latitude'] = preg_replace('/[ ]{2,}|[\t\n\#\:]/', ' ', trim($detail->getAttribute('content')));
                        }
                        ## Finding longitude
                        $clinics['longitude'] = 0;
                        $xpath = new \DOMXPath(@$clinicstmldom);
                        $details = $xpath->query('//span[@itemprop="geo"]/meta[@itemprop="longitude"]');
                        foreach($details as $detail){ 
                                $clinics['longitude'] = preg_replace('/[ ]{2,}|[\t\n\#\:]/', ' ', trim($detail->getAttribute('content')));
                        }
                        ## Finding galleries
                        $xpath = new \DOMXPath(@$clinicstmldom);
                        $details = $xpath->query('//p[@class="clinic-photos-container"]/a');

                        foreach($details as $detail){ 
                                $clinics['galleries'][] = preg_replace('/[ ]{2,}|[\t]/', ' ', trim($detail->getAttribute('href')));
                        }
                        ## Finding Clinic Timing
                        $xpath = new \DOMXPath(@$clinicstmldom);
                        $details = $xpath->query('//meta[@itemprop="openingHours"]');
                        $schedule = [];
                        foreach($details as $detail){ 
                            $schedule[] =strtoupper(trim($detail->getAttribute('content')));
                        }
                        $clinics['scheduler']=$schedule;
                        ## Finding Clinic consulting fees
                        $clinics['consultingprice']=0;
                        $xpath = new \DOMXPath(@$clinicstmldom);
                        $details = $xpath->query('//span[@itemprop="priceRange"]');
                        foreach($details as $detail){ 
                                $clinics['consultingprice']= trim(preg_replace('/[ ]{2,}|[\t\n,]/', '', trim($detail->nodeValue)), ',');
                        }
                        ### Finding clinic services
                        ## Load clinic details Html
                        $services= [];
                        $detailshtml1 = $this->curl_get_item($clinics['url']);
                        @$detailsdom1= new \DOMDocument();
                        @$detailsdom1->loadHTML($detailshtml1);

                        $xpath = new \DOMXPath(@$detailsdom1);
                        $details = $xpath->query('//div[@itemprop="availableService"]/div[@class="service-row"]/div[@class="service-cell"]');
                        foreach($details as $detail){
                            $services[] = preg_replace('/[ ]{2,}|[\t\n]\s\s+/', ' ', trim($detail->nodeValue));
                        }
                        $clinics['services'] = $services;
                        ## Finding

                        $allclinics[]=$clinics;					
                }		
        $singledoctor['clinics'] = preg_replace('/\s\s+/', ' ', stripslashes(json_encode($allclinics)));
        $result[] = $singledoctor;
    }
    return $result;
}
protected function findTotalItems($html){
    @$dom= new \DOMDocument();
	@$dom->loadHTML($html);
	$result = [];
	$totalItems =0;
	
    $xpath = new \DOMXPath(@$dom);
	$tags = $xpath->query('//div[@class="results-container-wrapper"]');	
	foreach($tags as $tag){              
		$newhtml = $dom->saveHTML($tag);
		@$newdom= new \DOMDocument();
		@$newdom->loadHTML($newhtml);
		$xpath = new \DOMXPath(@$newdom);
		$alldoctor = $xpath->query('//h4');
		foreach($alldoctor as $doctor){
				$totalItems = trim(str_replace(' matches found for:','',$doctor->nodeValue));
		}		
	}
	
	// if not match or template change
	if($totalItems == 0) {
		$tags = $xpath->query('//div[@class="chat-icon-enabled summary-text fold "]');
		foreach($tags as $tag){              
			$newhtml = $dom->saveHTML($tag);
			@$newdom= new \DOMDocument();
			@$newdom->loadHTML($newhtml);
			$xpath = new \DOMXPath(@$newdom);
			$alldoctor = $xpath->query('//h4');
			foreach($alldoctor as $doctor){
					$totalItems = trim(str_replace(' matches found for:','',$doctor->nodeValue));
			}		
		}
	}
	
	return $totalItems;
	
	
	
	
}
private function getDoctorCategories($url){
	$html = $this->curl_get_item($url); 
	@$dom= new \DOMDocument();
	@$dom->loadHTML($html);
	$result = [];
	$xpath = new \DOMXPath(@$dom);
	$tags = $xpath->query('//section[@id="doctor-tab"]/div[@class="tests-container"]/a');
	foreach($tags as $tag){

		$result[] = $tag->getAttribute("data-speciality");

	}
	if(count($result)<1){
		$tags = $xpath->query('//div[@class="other-speciality-container"]/div/a');
		foreach($tags as $tag){
			$result[] = $tag->getAttribute("data-speciality");
		}
	}
    //$this->pr($result);
	$dentis_spececialties = $this->dentistSpecialties();
    $result = array_merge($result,$dentis_spececialties);	
	array_push($result,"general-physician");	
	return array_unique($result);
}
private function getCities(){
	$html = '<div class="other-city-list"><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="agra" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Agra</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="ahmedabad" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Ahmedabad</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="allahabad" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Allahabad</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="ambala" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Ambala*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="amravati" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Amravati*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="amritsar" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Amritsar</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="aurangabad" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Aurangabad*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="aurangabad" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Aurangabad</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="bangalore" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Bangalore</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="bardhaman" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Bardhaman*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="bareilly" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Bareilly*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="bathinda" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Bathinda*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="bharuch" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Bharuch*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="bhopal" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Bhopal</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="bhubaneswar" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Bhubaneswar*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="chandigarh" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Chandigarh</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="chennai" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Chennai</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="coimbatore" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Coimbatore</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="cuttack" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Cuttack*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="dehradun" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Dehradun*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="delhi" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Delhi</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="dhanbad" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Dhanbad*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="ernakulam" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Ernakulam</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="faridabad" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Faridabad</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="gandhinagar" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Gandhinagar*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="ghaziabad" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Ghaziabad</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="guntur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Guntur*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="gurgaon" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Gurgaon</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="guwahati" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Guwahati*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="gwalior" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Gwalior*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="hisar" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Hisar*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="howrah" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Howrah*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="hubli" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Hubli*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="hyderabad" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Hyderabad</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="indore" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Indore</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="jabalpur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Jabalpur*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="jaipur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Jaipur</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="jalandhar" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Jalandhar*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="jamshedpur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Jamshedpur*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="jodhpur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Jodhpur</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="kanchipuram" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Kanchipuram*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="kanpur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Kanpur</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="kolhapur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Kolhapur*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="kolkata" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Kolkata</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="kota" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Kota*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="lucknow" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Lucknow</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="ludhiana" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Ludhiana</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="madurai" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Madurai*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="mangalore" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Mangalore*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="mathura" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Mathura*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="meerut" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Meerut</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="mohali" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Mohali</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="moradabad" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Moradabad*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="mumbai" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Mumbai</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="mysore" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Mysore</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="nagpur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Nagpur</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="nashik" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Nashik</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="navi-mumbai" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Navi Mumbai</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="navsari" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Navsari*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="nellore" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Nellore*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="noida" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Noida</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="north-goa" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">North Goa*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="panchkula" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Panchkula</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="patiala" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Patiala*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="patna" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Patna</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="puducherry" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Puducherry</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="pune" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Pune</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="raipur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Raipur</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="rajkot" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Rajkot</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="ranchi" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Ranchi</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="salem" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Salem*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="solapur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Solapur*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="sonipat" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Sonipat*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="south-goa" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">South Goa*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="srinagar" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Srinagar*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="surat" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Surat</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="thane" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Thane</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="thiruvananthapuram" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Thiruvananthapuram</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="udaipur" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Udaipur*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="vadodara" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Vadodara</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="varanasi" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Varanasi</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="vellore" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Vellore*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="vijayawada" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Vijayawada</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="visakhapatnam" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Visakhapatnam</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="warangal" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Warangal*</a></div><div class="city-dropdown-top-city-container"><a href="javascript:void(0);" data-slug="yamunanagar" data-country="India" class="city-dropdown-option city-dropdown-city city-dropdown-top-city-alpha">Yamunanagar*</a></div></div>';
	@$dom= new \DOMDocument();
	@$dom->loadHTML($html);
	$result = [];

	$xpath = new \DOMXPath(@$dom);
	$tags = $xpath->query('//div[@class="city-dropdown-top-city-container"]/a');
	foreach($tags as $tag){

	$result[] = $tag->getAttribute("data-slug");

	}
	return $result;

}
public function dentistSpecialties(){
    $html = '<section id="dentist-tab" class="content-current">
            <div class="tests-container">
                                                                                                                                                                                                                    <a href="https://www.practo.com/bangalore/dentist" data-speciality="dentist" class="speciality-icons">
                                    <p class="test-name">Dentist</p>
                            </a>
                                                                                                                                                                                                                    <a href="https://www.practo.com/bangalore/prosthodontist" data-speciality="prosthodontist" class="speciality-icons">
                                    <p class="test-name">Prosthodontist</p>
                            </a>
                                                                                                                                                                                                                    <a href="https://www.practo.com/bangalore/orthodontist" data-speciality="orthodontist" class="speciality-icons">
                                    <p class="test-name">Orthodontist</p>
                            </a>
                                                                                                                                                                                                                    <a href="https://www.practo.com/bangalore/pediatric-dentist" data-speciality="pediatric-dentist" class="speciality-icons">
                                    <p class="test-name">Pediatric Dentist</p>
                            </a>
                                                                                                                                                                                                                    <a href="https://www.practo.com/bangalore/endodontist" data-speciality="endodontist" class="speciality-icons">
                                    <p class="test-name">Endodontist</p>
                            </a>
                                                                                                                                                                                                                    <a href="https://www.practo.com/bangalore/implantologist" data-speciality="implantologist" class="speciality-icons">
                                    <p class="test-name">Implantologist</p>
                            </a>
															<div class="clear"></div>
			</div>
		</section>';
		@$dom= new \DOMDocument();
        @$dom->loadHTML($html);
        $result = [];

        $xpath = new \DOMXPath(@$dom);
        $tags = $xpath->query('//div[@class="tests-container"]/a');
        foreach($tags as $tag){
            
            $result[] = $tag->getAttribute("data-speciality");
            
        }
		//$this->pr($result);
		$result = ['dermatologist','pediatrician','gynecologist-obstetrician'];
        return $result;
	}
	public function importExport()
    {
        return view('admin::import');
    }
    public function importExcel()
    {
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
					$failedArr = [];
					## Finding address component
                    $address = \Vcareall\Admin\Helper::getLatLong($value->locality);
                    /**
                     * Checking profile exist 
                     */
                    $doctor = \App\Model\Doctor::where([
							'name'=>trim($value->name),
							'mobile' => trim($value->mobile)
							//'locality'=>trim($value->locality)
						])->get();
                    if(!empty($address) && $value->specialties && $value->qualifications && $doctor->count()<1){
						$awardArr = json_decode($value->awards);
						$award= "";
						if(!empty($awardArr) && $awardArr !=""){
							
							$award = $awardArr[0];
						}
						$insert = [
                            'name' => (trim($value->name)?trim($value->name):''), 
                            'address_one' => (trim($address['address_one'])?trim($address['address_one']):trim($value->locality)),
                            'latitude' => (trim($address['lat'])?trim($address['lat']):trim($value->latitude)),
							'longitude' => (trim($address['lng'])?trim($address['lng']):trim($value->longitude)),
                            'locality' => trim($value->locality),
                            'city_name' => (trim($address['city'])?trim($address['city']):''),
                            'state_name' => (trim($address['state'])?trim($address['state']):''),
                            'country_name' => (trim($address['country'])?trim($address['country']):''),
                            'about' =>'', //(trim($value->about)?trim($value->about):''),
                            'experience' => (trim($value->experience)?trim($value->experience):''),
                            'mobile' => (trim($value->mobile)?trim($value->mobile):''),
                            'recognition' => (trim($value->recognition)?trim($value->recognition):''),
                            'website' => (trim($value->website)?trim($value->website):''),
                            'awards' => $award,
                            'consultation_fee' => (trim(str_replace("INR","",$value->cosultingfee))?trim(str_replace("INR","",$value->cosultingfee)):''),
							'status' => 1 	
                        ];
                        $doctor = Doctor::Create($insert);
                        ## Finding Qualifications
						$qualifications = json_decode($value->qualifications);
                        if(!empty($qualifications)){  
							foreach($qualifications as $qualification){
								$education = explode(',',$qualification);
								$count = count($education);
								$year = @$education[$count-1];
								$temp = explode('-',@$education[0]);
								$degree = @$temp[0];
								$collge_name = @$temp[count($temp)-1].', '.@$education[$count-2];
								$qualification = DB::table('qualifications')
										->where('name', 'like', '%'.$degree.'%')
										->first();
								if(!$qualification){
									$qualification = Qualifications::create(['name'=>$degree]);
								}
								$college = DB::table('colleges')
										->where('name', 'like', '%'.$collge_name.'%')
										->first();
								if(!$college){
									$college = College::create(['name'=>$collge_name]);
								}								
								$doctor->qualifications()->attach($qualification->id, ['college'=>$collge_name?$collge_name:'','college_id'=>$college->id,'year'=>$year?$year:'']);
							}
						}
                        ## Adding doctor specialties
						$specialties = json_decode($value->specialties);
						if(!empty($specialties)){
                            foreach($specialties as $var){
								## Finding specielty ID
                                $specialty = DB::table('specialties')
                                    ->where('name', '=', $var)
                                    ->first();
                                if($specialty){
                                    $specialtiesList = array('speciality_id'=>$specialty->id, 'doctor_id'=>$doctor->id);
                                    
                                }else{
									$specialty = \App\Model\Specialty::create([
										'name' => $var
									]);
									$specialtiesList = array('speciality_id'=>$specialty->id, 'doctor_id'=>$doctor->id);
								}
								SpecialtyDetail::updateOrCreate(
									['speciality_id' => $specialty->id, 'doctor_id'=>$doctor->id],
									$specialtiesList                                   
								);
							}
                        } 
						
						### Adding doctor services 
						$services = json_decode($value->services);
						if(!empty($services)){
							foreach($services as $var){
                                ## Finding specielty ID
                                $service = DB::table('service_type')
                                    ->where('name', '=', $var)
                                    ->first();
                                if($service){
                                    $serviceList = array(
									'service_id'=>$service->id, 'doctor_id'=>$doctor->id);                                  
                                }else{
									$service = \App\Model\Service::create([
										'name' => $var
									]);
									$serviceList = array(
									'service_id'=>$service->id, 'doctor_id'=>$doctor->id); 
								}
								ServiceDetails::updateOrCreate(
									['service_id' => $service->id,'doctor_id'=>$doctor->id],
									$serviceList                                   
								);
							}
						}
						## clinic associated doctor
						$clinics = json_decode($value->clinics);
						$count = 1;
						if(!empty($clinics)){
                            foreach($clinics as $clinic){
								if($count ++ == 1){
									$phone = $value->phone1;
								}
								if($count ++ == 2){
									$phone = $value->phone2;
								}
								if($count ++ == 3){
									$phone = $value->phone3;
								}
								$address = \Vcareall\Admin\Helper::getLatLong($clinic->locality);    
								/**
                                * Checking clinic existance
                                */
								$clinicExist = \App\Model\Hospital::where([
                                   'name'=>trim($clinic->name),
                                   'locality'=>trim($clinic->locality)
								])->get();
								if($clinicExist->count()<1){
                                    $data = [
                                        'name'=>(trim($clinic->name)?trim($clinic->name):''), 
                                        'phone'=>'',
                                        'address_one' => (trim($address['address_one'])?trim($address['address_one']):trim($clinic->locality)),
                                        'latitude' => (trim($address['lat'])?trim($address['lat']):trim($clinic->latitude)),
										'longitude' => (trim($address['lng'])?trim($address['lng']):trim($clinic->longitude)),
										'locality' => trim($clinic->locality),
                                        'city_name' => (trim($address['city'])?trim($address['city']):''),
                                        'state_name' => (trim($address['state'])?trim($address['state']):''),
                                        'country_name' => (trim($address['country'])?trim($address['country']):''),
										'status' => 1,
                                        'consultation_fee' => (trim(str_replace("INR","",$clinic->consultingprice))?trim(str_replace("INR","",$clinic->consultingprice)):0)
                                    ];
                                    $hospital = \App\Model\Hospital::Create($data);
                                    if(!empty($value->specialties)){
                                            $specialties = json_decode($value->specialties);
                                            foreach($specialties as $var){
                                                    ## Finding specielty ID
                                                    $specialty = DB::table('specialties')
                                                            ->where('name', '=', $var)
                                                            ->first();
                                                    if($specialty){
                                                            $specialtiesList = array('speciality_id'=>$specialty->id, 'hospital_id'=>$hospital->id);
                                                    }else{
                                                            $specialty = \App\Model\Specialty::create([
                                                                    'name' => $var
                                                            ]);
                                                            $specialtiesList = array('speciality_id'=>$specialty->id, 'hospital_id'=>$hospital->id);
                                                    }
                                                    SpecialtyDetail::updateOrCreate(
                                                            ['speciality_id' => $specialty->id,'hospital_id'=>$hospital->id],
                                                            $specialtiesList                                   
                                                    );
                                            }
                                    }
                                    ## For adding hospital services
                                    $services = $clinic->services;
                                    if(!empty($services)){
                                            foreach($services as $var){
                                                    ## Finding specielty ID
                                                    $service = DB::table('service_type')
                                                            ->where('name', '=', $var)
                                                            ->first();
                                                    if($service){
                                                            $serviceList = array(
                                                            'service_id'=>$service->id, 'hospital_id'=>$hospital->id);                                  
                                                    }else{
                                                            $service = \App\Model\Service::create([
                                                                    'name' => $var
                                                            ]);
                                                            $serviceList = array(
                                                            'service_id'=>$service->id, 'hospital_id'=>$hospital->id); 
                                                    }
                                                    ServiceDetails::updateOrCreate(
                                                            ['service_id' => $service->id],
                                                            $serviceList                                   
                                                    );
                                            }
                                    }
									/*****
									 * Adding doctor clinics schedulars
									 */
									$schedules = $clinic->scheduler;
									if(!empty($schedules)){
										$schedulers = Helper::workingTimeFormate($schedules,$hospital->id,$doctor->id);
										$working_datetimes = WorkingDatetimes::insert($schedulers);
									}
									/*****
									 * Associate doctor with clinics
									 */
                                    $doctor->hospitals()->detach($hospital->id);
                                    $doctor->hospitals()->attach($hospital->id); 
                                }else{
									$hospital=$clinicExist->first();
									$doctor->hospitals()->detach($hospital->id);
                                    $doctor->hospitals()->attach($hospital->id); 
								}
                            }
                        }
                    }elseif(!empty($address)){
                        $doctor = $doctor->first();
						## doctor associated clinics
						$clinics = json_decode($value->clinics);
						if(!empty($clinics)){
                            foreach($clinics as $clinic){
								$address = \Vcareall\Admin\Helper::getLatLong($clinic->locality);    
								/**
                                * Checking clinic existance
                                */
								$clinicExist = \App\Model\Hospital::where([
                                   'name'=>trim($clinic->name),
                                   'locality'=>trim($clinic->locality)
								])->get();
								if($clinicExist->count()<1){
                                    $data = [
                                        'name'=>(trim($clinic->name)?trim($clinic->name):''), 
                                        'phone'=>'',
                                        'address_one' => (trim($address['address_one'])?trim($address['address_one']):trim($clinic->locality)),
                                        'latitude' => (trim($address['lat'])?trim($address['lat']):trim($value->latitude)),
										'longitude' => (trim($address['lng'])?trim($address['lng']):trim($value->longitude)),
										'locality' => trim($clinic->locality),
                                        'city_name' => (trim($address['city'])?trim($address['city']):''),
                                        'state_name' => (trim($address['state'])?trim($address['state']):''),
                                        'country_name' => (trim($address['country'])?trim($address['country']):''),
										'status' => 1,
                                        'consultation_fee' => (trim(str_replace("INR","",$clinic->consultingprice))?trim(str_replace("INR","",$clinic->consultingprice)):0)
                                    ];
                                    $hospital = \App\Model\Hospital::Create($data);
                                    if(!empty($value->specialties)){
										$specialties = json_decode($value->specialties);
										foreach($specialties as $var){
											## Finding specielty ID
											$specialty = DB::table('specialties')
													->where('name', '=', $var)
													->first();
											if($specialty){
													$specialtiesList = array('speciality_id'=>$specialty->id, 'hospital_id'=>$hospital->id);
											}else{
													$specialty = \App\Model\Specialty::create([
															'name' => $var
													]);
													$specialtiesList = array('speciality_id'=>$specialty->id, 'hospital_id'=>$hospital->id);
											}
											SpecialtyDetail::updateOrCreate(
													['speciality_id' => $specialty->id,'hospital_id'=>$hospital->id],
													$specialtiesList                                   
											);
										}
                                    }
                                    ## For adding hospital services
                                    ### Adding doctor services 
                                    $services = $clinic->services;
                                    if(!empty($services)){
										foreach($services as $var){
											## Finding specielty ID
											$service = DB::table('service_type')
												->where('name', '=', $var)
												->first();
											if($service){
												$serviceList = array(
												'service_id'=>$service->id, 'hospital_id'=>$hospital->id);                                  
											}else{
												$service = \App\Model\Service::create([
														'name' => $var
												]);
												$serviceList = array(
												'service_id'=>$service->id, 'hospital_id'=>$hospital->id); 
											}
											ServiceDetails::updateOrCreate(
												['service_id' => $service->id],
												$serviceList                                   
											);
										}
                                    }
									/*****
									 * Adding doctor clinics schedulars
									 */
									$schedules = $clinic->scheduler;
									if(!empty($schedules)){
										$schedulers = Helper::workingTimeFormate($schedules,$hospital->id,$doctor->id);
										$working_datetimes = WorkingDatetimes::insert($schedulers);
									}
									/*****
									 * Associate doctor with clinics
									 */
									$doctor->hospitals()->detach($hospital->id);
                                    $doctor->hospitals()->attach($hospital->id); 
                                }else{
									$hospital=$clinicExist->first();
									$doctor->hospitals()->detach($hospital);
                                    $doctor->hospitals()->attach($hospital); 
								}
                            }
                        }
					}else{
						### Failed entries ##########
                        $failedArr[] = ['title' => $value->name, 'description' => $value->name];
						
					}
                }
                return redirect('admin/import-excel')->with(['message' => 'Uploaded successfully.','failedData'=>$failedArr]);
            }
        }
        return back()->with(['message' => 'Failed.']);
    }
	public function downloadExcel($type)
    {
        $data = User::get()->toArray();
        return Excel::create('user_data', function ($excel) use ($data) {
            $excel->sheet('mySheet', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
	public function importHospital(Request $request){
		if($request->isMethod('post') && Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
					$failedArr = [];
                    ## Finding address component
                    $address = \Vcareall\Admin\Helper::getLatLong($value->address); 
					$hospital = \App\Model\Hospital::where([
							'name'=>trim($value->name),
							'phone' => trim($address['phone']) 
							//'locality'=>trim($address['locality'])
						])->get();
                    if(!empty($address) && $value->name && $value->phone && $value->address && $hospital ->count()<1){						
						$insert = [
                            'name' => (trim($value->name)?trim($value->name):''), 
                            'address_one' => (trim($address['address_one'])?trim($address['address_one']):''),
							'phone' => (trim($value->phone)?trim($value->phone):''),
                            'latitude' => (trim($address['lat'])?trim($address['lat']):''),
                            'longitude' => (trim($address['lng'])?trim($address['lng']):''),
                            'locality' => (trim($address['locality'])?trim($address['locality']):''),
                            'city_name' => (trim($address['city'])?trim($address['city']):''),
                            'state_name' => (trim($address['state'])?trim($address['state']):''),
                            'country_name' => (trim($address['country'])?trim($address['country']):''),
                            'consultation_fee' => (trim($value->consultation_fee)?trim($value->consultation_fee):''),
                            'no_of_doctors' => (trim($value->no_of_doctors)?trim($value->no_of_doctors):''),
                            'no_of_beds' => (trim($value->no_of_beds)?trim($value->no_of_beds):''),
                            'icu_facility' => (trim($value->icu_facility)?trim($value->icu_facility):''),
							'diagnostic_lab_facility' => (trim($value->diagnostic_lab_facility)?trim($value->diagnostic_lab_facility):''),
							'24_hours_emergency' => (trim($value->hours24_emergency)?trim($value->hours24_emergency):''),
							'cashless_mediclaim' => (trim($value->cashless_mediclaim)?trim($value->hours24_emergency):''),
                            'website' => (trim($value->website)?trim($value->website):''),
                        ];
                        $hospital = Hospital::Create($insert);                       
                        ## Adding doctor specialties
						$specialties = json_decode($value->specialties);
						if(!empty($specialties)){
                            foreach($specialties as $var){
								## Finding specielty ID
                                $specialty = DB::table('specialties')
                                    ->where('name', 'like', '%'.$var.'%')
                                    ->first();
                                if($specialty){
                                    $specialtiesList = array('speciality_id'=>$specialty->id, 'hospital_id'=>$hospital->id);
                                    
                                }else{
									$specialty = \App\Model\Specialty::create([
										'name' => $var
									]);
									$specialtiesList = array('speciality_id'=>$specialty->id, 'hospital_id'=>$hospital->id);
								}
								SpecialtyDetail::updateOrCreate(
									['speciality_id' => $specialty->id, 'hospital_id'=>$hospital->id],
									$specialtiesList                                   
								);
							}
                        } 
						
						### Adding doctor services 
						$services = json_decode($value->services);
						if(!empty($services)){
							foreach($services as $var){
                                ## Finding specielty ID
                                $service = DB::table('service_type')
                                    ->where('name', 'like', '%'.$var.'%')
                                    ->first();
                                if($service){
                                    $serviceList = array(
									'service_id'=>$service->id, 'hospital_id'=>$hospital->id);                                  
                                }else{
									$service = \App\Model\Service::create([
										'name' => $var
									]);
									$serviceList = array(
									'service_id'=>$service->id, 'hospital_id'=>$hospital->id); 
								}
								ServiceDetails::updateOrCreate(
									['service_id' => $service->id,'hospital_id'=>$hospital->id],
									$serviceList                                   
								);
							}
						}
						/*****
						 * Adding hospital schedulars
						 */
						$schedules = json_decode($value->schedules);
						if(!empty($schedules)){
							$schedulers = Helper::workingTimeFormate($schedules,$hospital->id);
							$working_datetimes = WorkingDatetimes::insert($schedulers);
						}
					}else{
                        ### Failed entries ##########
                        $failedArr[] = $value;
                    }
                }
                return redirect('admin/import-excel/hospital')->with(['message' => 'Uploaded successfully.']);
            }
        }
        return view('admin::scrapper.import-hospital');
	}
	public function importDiagnostic(Request $request){
		if($request->isMethod('post') && Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
					$failedArr = [];
                    ## Finding address component
                    $address = \Vcareall\Admin\Helper::getLatLong($value->address);
					$diagnostic = \App\Model\Diagnostic::where([
							'name'=>trim($value->name),
							'phone'=>trim($address['phone'])
							//'locality'=>trim($address['locality'])
						])->get();
                    if(!empty($address) && $value->name && $value->phone && $value->address  && $diagnostic->count()<1){						
						$insert = [
                            'name' => (trim($value->name)?trim($value->name):''), 
                            'address_one' => (trim($address['address_one'])?trim($address['address_one']):''),
							'phone' => (trim($value->phone)?trim($value->phone):''),
                            'latitude' => (trim($address['lat'])?trim($address['lat']):''),
                            'longitude' => (trim($address['lng'])?trim($address['lng']):''),
                            'locality' => (trim($address['locality'])?trim($address['locality']):''),
                            'city_name' => (trim($address['city'])?trim($address['city']):''),
                            'state_name' => (trim($address['state'])?trim($address['state']):''),
                            'country_name' => (trim($address['country'])?trim($address['country']):''),
                            'home_collection_facility' => (trim($value->home_collection_facility)?trim($value->home_collection_facility):''),
                            'online_bookings' => (trim($value->online_bookings)?trim($value->online_bookings):''),
                            'online_reports' => (trim($value->online_reports)?trim($value->online_reports):''),
                            'website' => (trim($value->website)?trim($value->website):''),
                        ];
                        $diagnostic = Diagnostic::Create($insert);                       
                        ## Adding doctor specialties
						$tests = json_decode($value->tests);
						if(!empty($tests)){
                            foreach($tests as $var){
								## Finding specielty ID
                                $test = DB::table('tests')
                                    ->where('name', 'like', '%'.$var.'%')
                                    ->first();
                                if($test){
                                    $testList = array('test_id'=>$test->id, 'diagnostic_id'=>$diagnostic->id);
                                    
                                }else{
									$test = \App\Model\Test::create([
										'name' => $var
									]);
									$testList = array('test_id'=>$test->id, 'diagnostic_id'=>$diagnostic->id);
								}
								DiagnosticTest::updateOrCreate(
									['test_id' => $test->id, 'diagnostic_id'=>$diagnostic->id],
									$testList                                   
								);
							}
                        } 
						/*****
						 * Adding diagnostic schedulars
						 */
						$schedules = json_decode($value->schedules);
						if(!empty($schedules)){
							$schedulers = Helper::workingTimeFormate($schedules,0,0,$diagnostic->id);
							$working_datetimes = WorkingDatetimes::insert($schedulers);
						}
					}else{
                        ### Failed entries ##########
                        $failedArr[] = $value;
                    }
                }
                return redirect('admin/import-excel/diagnostic')->with(['message' => 'Uploaded successfully.']);
            }
        }
		return view('admin::scrapper.import-diagnostic');
	}
	
}
