<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;

/**
 * Description of ReviewsController
 *
 * @author BALESHAWR
 */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Service;
use App\Model\Specialty;
use Illuminate\Support\Facades\Validator;
use Image;
use Illuminate\Support\Facades\Mail;
class ClaimController extends Controller {
    public function doctor(){ 
        $profiles = \App\Model\ClaimedProfile::orderBy('id','desc')->get();
        $i=0;
        $claimed_profiles=[];
        foreach($profiles as $profile){
			$doctor = \App\Model\Doctor::where('id',$profile->doctor_id)->get();
            if($doctor->count()>0){
              $claimed_profiles[$i]['profile']= $doctor->first(); 
              $claimed_profiles[$i]['claimed_by']= \App\User::find($profile->user_id);
			  $claimed_profiles[$i++]['profile']->claim_status = $profile->claim_status; 
            }             
        } 
        $result['reviews']= \App\Model\Review::all();
        $result['claimed_profiles']= $claimed_profiles;
		
        return view('admin::claims.doctor',$result);
    }
    public function approveDoctor(Request $request){
        if(!$request->input('id') or !$request->input('uid')){
            abort(404);
        }
        try{
            /****
             @ claiming profile
             */
			$count = \App\Model\ClaimedProfile::where('user_id',$request->input('uid'))
			->where('profile_claimed',$request->input('id'))
			->where('claim_status',1)->count();
			
			if($count){
				return back()
					->with(['status'=>0,'message'=>'Profile is already associate with someone.']);
			}
			$claimedProfile = \App\Model\ClaimedProfile::where('doctor_id',$request->input('id'))
				->where('user_id',$request->input('uid'))->update([
                'claim_status' =>1
            ]);
            $doctor = \App\Model\Doctor::find($request->input('id'));
            $doctor->user_id = $request->input('uid');
            $doctor->claim_status =1;
            $doctor->save();
            $user = \App\User::find($request->input('uid'));
			/***
			 * Sending notification
			 */
			$name = $doctor->name;
			$deviceToken = $user->user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->user->device_type;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_notification_approved_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
				'receiver'=>$user->id,
				'type'=>1,
				'status'=>0 
			]);
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
            /****
            @ Sending claim mail to user
            */
            $data = [
                    'email' => $user->email,
                    'user_name'=>$user->user->name,
                    'entity_name'=>$doctor->name
            ];
            Mail::send('email.profile_claimed_approved',['data'=>$data], function ($message) use ($data) {
                    $message->to($data['email'])->subject('Profile claimed request approved.');
            });
            return back()->with(['status'=>1,'message'=>'Approved successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function disapproveDoctor(Request $request){
        if(!$request->input('id') or !$request->input('uid')){
            abort(404);
        }
        try{
            /****
            @ claiming profile
            */
            $claimedProfile = \App\Model\ClaimedProfile::where('doctor_id',$request->input('id'))->where('user_id',$request->input('uid'))->update([
                'claim_status' =>0
            ]);
            $doctor = \App\Model\Doctor::find($request->input('id'));
            $doctor->user_id = 0;
            $doctor->claim_status =2;
            $doctor->save();
            $user = \App\User::find($request->input('uid'));
			/***
			 * Sending notification
			 */
			$name = $doctor->name;
			$deviceToken = $user->user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->user->device_type;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_notification_desclined_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
				'receiver'=>$user->id,
				'type'=>1,
				'status'=>0
			]);
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
            /****
            @ Sending claim mail to user
            */
            $data = [
                    'email' => $user->email,
                    'user_name'=>$user->user->name,
                    'entity_name'=>$doctor->name
            ];
            Mail::send('email.profile_claimed_disapproved',['data'=>$data], function ($message) use ($data) {
                    $message->to($data['email'])->subject('Profile claimed request declined.');
            });
            return back()->with(['status'=>1,'message'=>'Declined successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
	public function deleteDoctor(Request $request){
        if(!$request->input('id') or !$request->input('uid')){
            abort(404);
        }
        try{
            /****
             @ deleting profile
             */
            \App\Model\ClaimedProfile::where('doctor_id',$request->input('id'))
				->where('user_id',$request->input('uid'))->delete();
            $doctor = \App\Model\Doctor::find($request->input('id'));
            $doctor->user_id = 0;
            $doctor->claim_status =0;
            $doctor->save();
            $user = \App\User::find($request->input('uid'));			
            /****
             @ Sending claim mail to user
             */
            $data = [
                    'email' => $user->email,
                    'user_name'=>$user->user->name,
                    'entity_name'=>$doctor->name
            ];
            Mail::send('email.profile_claimed_deleted',['data'=>$data], function ($message) use ($data) {
                    $message->to($data['email'])->subject('Profile claimed request deleted.');
            });
            return back()->with(['status'=>1,'message'=>'Deleted successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function hospital(){
        $profiles = \App\Model\ClaimedProfile::orderBy('id','desc')->get();
        $i=0;
        $claimed_profiles=[];
        foreach($profiles as $profile){
            $hospital = \App\Model\Hospital::where('id',$profile->hospital_id)->get();
            if($hospital->count()>0){
              $claimed_profiles[$i]['claimed_profile']= $hospital->first(); 
              $claimed_profiles[$i++]['claimed_by']= \App\User::find($profile->user_id); 
            }             
        } 
        $result['claimed_profiles']= $claimed_profiles;
        return view('admin::claims.hospital',$result);
    }
    public function approveHospital(Request $request){
        if(!$request->input('id') or !$request->input('uid')){
            abort(404);
        }
        try{
            /****
             @ claiming profile
             */
			$count = \App\Model\ClaimedProfile::where('user_id',$request->input('uid'))
			->where('profile_claimed',$request->input('id'))
			->where('claim_status',1)->count();
			
			if($count){
				return back()
					->with(['status'=>0,'message'=>'Profile is already associate with someone.']);
			}
            $claimedProfile = \App\Model\ClaimedProfile::where('hospital_id',$request->input('id'))->where('user_id',$request->input('uid'))->update([
                'claim_status' =>1
            ]);
            $hospital = \App\Model\Hospital::find($request->input('id'));
            $hospital->user_id = $request->input('uid');
            $hospital->claim_status =1;
            $hospital->save();
            $user = \App\User::find($request->input('uid'));
			/***
			 * Sending notification
			 */
			$name = $hospital->name;
			$deviceToken = $user->user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->user->device_type;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_notification_approved_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
				'receiver'=>$user->id,
				'type'=>1,
				'status'=>0
			]);
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
            /****
            @ Sending claim mail to user
            */
            $data = [
                    'email' => $user->email,
                    'user_name'=>$user->user->name,
                    'entity_name'=>$hospital->name
            ];
            Mail::send('email.profile_claimed_approved',['data'=>$data], function ($message) use ($data) {
                    $message->to($data['email'])->subject('Profile claimed request approved.');
            });
            return back()->with(['status'=>1,'message'=>'Approved successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function disapproveHospital(Request $request){
        if(!$request->input('id') or !$request->input('uid')){
            abort(404);
        }
        try{
            /****
            @ claiming profile
            */
            $claimedProfile = \App\Model\ClaimedProfile::where('hospital_id',$request->input('id'))->where('user_id',$request->input('uid'))->update([
                'claim_status' =>0
            ]);
            $hospital = \App\Model\Hospital::find($request->input('id'));
            $hospital->user_id = 0;
            $hospital->claim_status =2;
            $hospital->save();
            $user = \App\User::find($request->input('uid'));
			/***
			 * Sending notification
			 */
			$name = $hospital->name;
			$deviceToken = $user->user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->user->device_type;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_notification_desclined_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
				'receiver'=>$user->id,
				'type'=>1,
				'status'=>0
			]);
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
            /****
            @ Sending claim mail to user
            */
            $data = [
                    'email' => $user->email,
                    'user_name'=>$user->user->name,
                    'entity_name'=>$hospital->name
            ];
            Mail::send('email.profile_claimed_disapproved',['data'=>$data], function ($message) use ($data) {
                    $message->to($data['email'])->subject('Profile claimed request declined.');
            });
            return back()->with(['status'=>1,'message'=>'Declined successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
	public function deleteHospital(Request $request){
        if(!$request->input('id') or !$request->input('uid')){
            abort(404);
        }
        try{
            /****
             @ deleting profile
             */
            \App\Model\ClaimedProfile::where('hospital_id',$request->input('id'))
				->where('user_id',$request->input('uid'))->delete();
            $hospital = \App\Model\Hospital::find($request->input('id'));
            $hospital->user_id = 0;
            $hospital->claim_status =0;
            $hospital->save();
            $user = \App\User::find($request->input('uid'));			
            /****
             @ Sending claim mail to user
             */
            $data = [
                    'email' => $user->email,
                    'user_name'=>$user->user->name,
                    'entity_name'=>$hospital->name
            ];
            Mail::send('email.profile_claimed_deleted',['data'=>$data], function ($message) use ($data) {
                    $message->to($data['email'])->subject('Profile claimed request deleted.');
            });
            return back()->with(['status'=>1,'message'=>'Deleted successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function diagnostic(){
        $profiles = \App\Model\ClaimedProfile::orderBy('id','desc')->get();
        $i=0;
        $claimed_profiles=[];
        foreach($profiles as $profile){
            $diagnostic = \App\Model\Diagnostic::where('id',$profile->diagnostic_ic)->get();
            if($diagnostic->count()>0){
              $claimed_profiles[$i]['claimed_profile']= $diagnostic->first(); 
              $claimed_profiles[$i++]['claimed_by']= \App\User::find($profile->user_id); 
            }             
        } 
        $result['claimed_profiles']= $claimed_profiles;
        return view('admin::claims.diagnostic',$result);
    }
    public function approveDiagnostic(Request $request){
        if(!$request->input('id') or !$request->input('uid')){
            abort(404);
        }
        try{
            /****
             @ claiming profile
             */
			$count = \App\Model\ClaimedProfile::where('user_id',$request->input('uid'))
			->where('profile_claimed',$request->input('id'))
			->where('claim_status',1)->count();
			
			if($count){
				return back()
					->with(['status'=>0,'message'=>'Profile is already associate with someone.']);
			}
            $claimedProfile = \App\Model\ClaimedProfile::where('diagnostic_id',$request->input('id'))->where('user_id',$request->input('uid'))->update([
                'claim_status' =>1
            ]);
            $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
            $diagnostic->user_id = $request->input('uid');
            $diagnostic->claim_status =1;
            $diagnostic->save();
            $user = \App\User::find($request->input('uid'));
			/***
			 * Sending notification
			 */
			$name = $diagnostic->name;
			$deviceToken = $user->user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->user->device_type;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_notification_approved_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
				'receiver'=>$user->id,
				'type'=>1,
				'status'=>0
			]);
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
            /****
            @ Sending claim mail to user
            */
            $data = [
                'email' => $user->email,
                'user_name'=>$user->user->name,
                'entity_name'=>$diagnostic->name
            ];
            Mail::send('email.profile_claimed_approved',['data'=>$data], function ($message) use ($data) {
                    $message->to($data['email'])->subject('Profile claimed request approved.');
            });
            return back()->with(['status'=>1,'message'=>'Approved successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function disapproveDiagnostic(Request $request){
        if(!$request->input('id') or !$request->input('uid')){
            abort(404);
        }
        try{
            /****
            @ claiming profile
            */
            $claimedProfile = \App\Model\ClaimedProfile::where('diagnostic_id',$request->input('id'))->where('user_id',$request->input('uid'))->update([
                'claim_status' =>0
            ]);
            $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
            $diagnostic->user_id = 0;
            $diagnostic->claim_status =2;
            $diagnostic->save();
            $user = \App\User::find($request->input('uid'));
			/***
			 * Sending notification
			 */
			$name = $diagnostic->name;
			$deviceToken = $user->user->device_token;
			$gcmApiKey = config('constants.gcm_apikey');
			$deviceType = $user->user->device_type;
			$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
			\App\Model\Notification::create([
				'title'=>config('message.profile_notification_desclined_title'),
				'message'=>str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
				'receiver'=>$user->id,
				'type'=>1,
				'status'=>0
			]);
			$notifications= $notifications->send([
				'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
				'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
				'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
				'tickerText'	=> 'Medrator',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			]);
            /****
            @ Sending claim mail to user
            */
            $data = [
                    'email' => $user->email,
                    'user_name'=>$user->user->name,
                    'entity_name'=>$diagnostic->name
            ];
            Mail::send('email.profile_claimed_disapproved',['data'=>$data], function ($message) use ($data) {
                    $message->to($data['email'])->subject('Profile claimed request declined.');
            });
            return back()->with(['status'=>1,'message'=>'Declined successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
	public function deleteDiagnostic(Request $request){
        if(!$request->input('id') or !$request->input('uid')){
            abort(404);
        }
        try{
            /****
             @ deleting profile
             */
            \App\Model\ClaimedProfile::where('diagnostic_id',$request->input('id'))
				->where('user_id',$request->input('uid'))->delete();
            $diagnostic = \App\Model\Diagnostic::find($request->input('id'));
            $diagnostic->user_id = 0;
            $diagnostic->claim_status =0;
            $diagnostic->save();
            $user = \App\User::find($request->input('uid'));			
            /****
             @ Sending claim mail to administrator
             */
            $data = [
                    'email' => $user->email,
                    'user_name'=>$user->user->name,
                    'entity_name'=>$diagnostic->name
            ];
            Mail::send('email.profile_claimed_deleted',['data'=>$data], function ($message) use ($data) {
                    $message->to($data['email'])->subject('Profile claimed request deleted.');
            });
            return back()->with(['status'=>1,'message'=>'Deleted successfully.']);
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
}
