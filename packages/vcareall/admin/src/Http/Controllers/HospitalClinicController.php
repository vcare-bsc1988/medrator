<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;

/**
 * Description of HospitalClinicController
 *
 * @author VCareAll
 */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Service;
use App\Model\Specialty;
use Illuminate\Support\Facades\Validator;
use Image;
class HospitalClinicController extends Controller{
   public function details($id){
       if(!$id){
          abort(404);
       } 
       $result['hospital'] = \App\Model\Hospital::find($id);
       $result['specialties'] =  Specialty::lists('name','id');
       $result['services'] =  Service::lists('name','id');
       return view('admin::hospital-clinic.details',$result);
    }
    public function addSpecialties(Request $request){
        try{
            if ($request->isMethod('post')) {
                
                $validator = Validator::make($request->all(), [
                    'specialty_id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>3]);;
                }   
                $specialties = [];
                for($i=0; $i<count($request->input('specialty_id')); $i++){
                    $specialties[$i] = new \App\Model\SpecialtyDetail(['speciality_id'=>$request->input('specialty_id')[$i]]);
                }
                $doctor = \App\Model\Hospital::find($request->input('id'));
                $doctor->specialties()->saveMany($specialties);
                return back()->with(['status'=>1,'message'=>'Servicess addes successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>3]);
        }
    }
    public function deleteSpecialty(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>4]);;
                } 
                \App\Model\SpecialtyDetail::destroy($request->input('id')); 
                return back()->with(['status'=>1,'message'=>'Specialty deleted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>4]);
        }
    }
    public function profileAccept(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>6]);;
                } 
                $hospital = \App\Model\Hospital::find($request->input('id'));
                $hospital->status = 1;
                $hospital->save(); 
				###### Sending notification
				$user = \App\User::find($hospital->user_id);
				if($user){
					$deviceToken = $user->device_token;
					$gcmApiKey = config('constants.gcm_apikey');
					$deviceType = $user->device_type;
					$name = $hospital->name;
					$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
					\App\Model\Notification::create([
						'title'=>config('message.profile_notification_approved_title'),
						'message'=>str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
						'receiver'=>$hospital->user_id,
						'type'=>0,
						'status'=>0
					]);
					# Sending user notification
					$notifications= $notifications->send([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'tickerText'	=> 'Medrator',
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon'
					]);
					/**
					 * Sending admin notification.
					 */
					$this->helper->sendAdminNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
					]);
					/**
					 * Sending moderator notification.
					 */
					$this->helper->sendModeratorNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
					]);
				}	
                return back()->with(['status'=>1,'message'=>'Approved successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>6]);
        }
    }
    public function profileDeclined(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>5]);;
                } 
                $hospital = \App\Model\Hospital::find($request->input('id'));
                $hospital->status = 0;
                $hospital->save();
				###### Sending notification
				$user = \App\User::find($hospital->user_id);
				if($user){
					$deviceToken = $user->device_token;
					$gcmApiKey = config('constants.gcm_apikey');
					$deviceType = $user->device_type;
					$name = $hospital->name;
					$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
					\App\Model\Notification::create([
						'title'=>config('message.profile_notification_desclined_title'),
						'message'=>str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
						'receiver'=>$hospital->user_id,
						'type'=>0,
						'status'=>0
					]);
					# Sending user notification
					$notifications= $notifications->send([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'tickerText'	=> 'Medrator',
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon'
					]);
					/**
					 * Sending admin notification.
					 */
					$this->helper->sendAdminNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
					]);
					/**
					 * Sending moderator notification.
					 */
					$this->helper->sendModeratorNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
					]);
				}	
                return back()->with(['status'=>1,'message'=>'Declined successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>5]);
        }
    }
    public function uploadImage(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>7]);;
                } 
                $image ="";
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('uploaded/images/thumbnail');
                    $img = Image::make($image->getRealPath());
                    $img->resize(100, 100, function ($constraint) {
                            $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$input['imagename']);

                    $destinationPath = public_path('uploaded/images');
                    $image->move($destinationPath, $input['imagename']);
                    $image =url('/').'/public/uploaded/images/'.$input['imagename'];

                }
                $hospital = \App\Model\Hospital::find($request->input('id'));
                $hospital->image = $image;
                $hospital->save();
                return back()->with(['status'=>1,'message'=>'Uploaded successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>7]);
        }
    }
    public function deleteImage(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>8]);;
                } 
                $hospital = \App\Model\Hospital::find($request->input('id'));
                $hospital->image = "";
                $hospital->save();
                return back()->with(['status'=>1,'message'=>'Image deleted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function update(Request $request){
        try{
            if ($request->isMethod('post')) { 
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>9]);;
                } 
                $hospital = \App\Model\Hospital::find($request->input('id'));             
                \App\Model\Hospital::where('id', $request->input('id'))->update([
					'name' => $request->input('name'),
					'phone' => $request->input('phone'),
					'consultation_fee' => $request->input('consultation_fee'),
					'no_of_doctors' => $request->input('no_of_doctors'),
					'no_of_beds' => $request->input('no_of_beds'),
					'icu_facility' => $request->input('icu_facility'),
					'24_hours_emergency' => $request->input('24_hours_emergency'),
					'address_one' => $request->input('address_one'),
					'latitude' => $request->input('latitude'),
					'longitude' => $request->input('longitude'),
					'locality' => $request->input('locality'),
					'city_name' => $request->input('city_name'),
					'state_name' => $request->input('state_name'),
					'country_name' => $request->input('country_name'),
					'website' => $request->input('website')
				]);
                return back()->with(['status'=>1,'message'=>'Profile updated successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>9]);
        }
    }
    public function delete($id){
        if(!$id){
            abort(404);
        }
        $hospital = \App\Model\Hospital::find($id);
        if($hospital->user_id){
            $user = \App\User::find($hospital->user_id);
            $user->delete();
            $user->roles()->detach();
            //$user->roles()->attach(2);
        }
        $hospital->services()->delete();
        $hospital->specialties()->delete();
        $hospital->reviews()->delete();
        $hospital->doctors()->delete();
		$hospital->schedular()->delete();
        $hospital->delete();
        return back()
            ->with(['status'=>1,'message'=>'Deleted successfully.']);
    }
    public function create(){
        $result['specialties'] =  Specialty::lists('name','id');
        $result['services'] =  Service::lists('name','id');
        return view('admin::hospital-clinic.create',$result);
    }
    public function save(Request $request){
        if ($request->isMethod('post')) { 
            $validator = Validator::make($request->all(), [
                'address_one' => 'required',
                'name' => 'required',
                'specialties' => 'required',
            ]);
            if ($validator->fails()) {
                return back()
                    ->withInput()
                    ->withErrors($validator)
                    ->with(['status'=>0,'message'=>$validator->errors()->first()]);
            } 
            try
            {
                $hospital = \App\Model\Hospital::create([
                    'name' => $request->input('name'),
                    'phone' => $request->input('phone'),
                    'consultation_fee' => $request->input('consultation_fee'),
                    'no_of_doctors' => $request->input('no_of_doctors'),
                    'no_of_beds' => $request->input('no_of_beds'),
                    'icu_facility' => $request->input('icu_facility'),
                    '24_hours_emergency' => $request->input('24_hours_emergency'),
                    'address_one' => $request->input('address_one'),
                    'latitude' => $request->input('latitude'),
                    'longitude' => $request->input('longitude'),
                    'locality' => $request->input('locality'),
                    'city_name' => $request->input('city_name'),
                    'state_name' => $request->input('state_name'),
                    'country_name' => $request->input('country_name'),
                    'website' => $request->input('website')
                ]);
                /**
                 * Adding hospital specilties.
                 */
                $hospital->specialties()->delete();
                $specialties =[];
                foreach($request->services as $var){
                     $specialties[] = array('speciality_id'=>$var);
                }
                $hospital->specialties()->insert($specialties);
                /**
                 * Adding services
                 */
                $hospital->services()->delete();
                if($request->services){
                    $services =[];
                    foreach($request->services as $var){
                         $services[] = array('service_id'=>$var);
                    }
                    $hospital->services()->insert($services);
                }
                return redirect('admin/doctors')->with(['status'=>1,'message'=>'Added successfully.']);
            }
            catch(\Illuminate\Database\QueryException $e){
                print_r($e->getMessage()); exit;
                abort(405,$e->getMessage());
            }
        }       
    }
	public function find(Request $request){
		if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $hospital = \App\Model\Hospital::find($request->input('id')); 
            
            $working_times = $hospital->workingTimes;
            $doctor = $hospital->doctors()->get()->first();
            $data =[
                'doctor' => $doctor,
                'hospital' => $hospital
            ];
			return view('admin::hospital-clinic.schedules',$data);
            //return view('account.schedules', $data);
        }
	}
	public function saveSunSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',1)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveMonSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',2)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveTueSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',3)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveWedSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',4)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveThrSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',5)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveFriSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',6)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
	public function saveSatSchedules(Request $request){
        if ($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'schedular' => 'required',
                'week_day' => 'required',
                'doctor_id' => 'required',
                'hospital_id' => 'required',
            ]);
            if ($validator->fails()) {
                return array('status'=>0, 'message'=>'Failed, Server Error !');
            } 
            $schedulars = $request->input('schedular');
			//return $request->all();
			$doctor = \App\Model\Doctor::find($request->input('doctor_id'));
			$doctor->schedular()->where('week_day',7)->delete();
            foreach($schedulars as $schedular){
                $working_time = new \App\Model\WorkingDatetimes([
                    'week_day' =>$request->input('week_day'),
                    'start_time' => $schedular['from'],
                    'end_time' => $schedular['to'],
                    'hospital_id' => $request->input('hospital_id'),
                ]);
				$working_time->doctor()->associate($doctor);
                $working_time->save();
            }
            $response = array('status'=>1, 'message'=>'Saved successfully'); 
            return $response;
        }
    }
}
