<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;

/**
 * Description of DoctorController
 *
 * @author VCareAll
 */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Service;
use App\Model\Specialty;
use Illuminate\Support\Facades\Validator;
use Image;
use App\Model\Doctor;
use Illuminate\Support\Facades\Mail;
class DoctorController extends Controller {
    protected $helper;
	protected $client;
    public function __construct(\Solarium\Client $client)
    {
        $this->helper = new \App\Helpers\Helper;
		$this->client = $client;
    }
	public function index(){
        
    }
    public function details($id){
        $result['doctor'] = \App\Model\Doctor::find($id);
        $result['specialties'] =  Specialty::lists('name','id');
        $result['qualifications'] = \App\Model\Qualifications::lists('name','id');
        $result['services'] =  Service::skip(0)->take(50)->lists('name','id');
		return view('admin::doctor.details',$result);
    }
    public function addServices(Request $request){
        try{
            if ($request->isMethod('post')) {
                
                $validator = Validator::make($request->all(), [
                    'service_id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>0]);;
                }   
                $services = [];
                for($i=0; $i<count($request->input('service_id')); $i++){
                    $services[$i] = new \App\Model\ServiceDetail(['service_id'=>$request->input('service_id')[$i]]);
                }
                $doctor = \App\Model\Doctor::find($request->input('id'));
                $doctor->services()->saveMany($services);
                return back()->with(['status'=>1,'message'=>'Servicess addes successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>1]);
        }
    }
    public function deleteService(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>2]);;
                } 
                \App\Model\ServiceDetail::destroy($request->input('id')); 
                return back()->with(['status'=>1,'message'=>'Service deleted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>2]);
        }
    }
    public function addSpecialties(Request $request){
        try{
            if ($request->isMethod('post')) {
                
                $validator = Validator::make($request->all(), [
                    'specialty_id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>3]);;
                }   
                $specialties = [];
                for($i=0; $i<count($request->input('specialty_id')); $i++){
                    $specialties[$i] = new \App\Model\SpecialtyDetail(['speciality_id'=>$request->input('specialty_id')[$i]]);
                }
                $doctor = \App\Model\Doctor::find($request->input('id'));
                $doctor->specialties()->saveMany($specialties);
                return back()->with(['status'=>1,'message'=>'Servicess addes successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>3]);
        }
    }
    public function deleteSpecialty(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>4]);;
                } 
                \App\Model\SpecialtyDetail::destroy($request->input('id')); 
                return back()->with(['status'=>1,'message'=>'Specialty deleted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>4]);
        }
    }
    public function profileAccept(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>6]);;
                } 
                $doctor = \App\Model\Doctor::find($request->input('id'));
                $doctor->status = 1;
                $doctor->save();
				###### Sending notification
				$user = \App\User::find($doctor->user_id);
				if($user){
					$deviceToken = $user->device_token;
					$gcmApiKey = config('constants.gcm_apikey');
					$deviceType = $user->device_type;
					$name = $doctor->name;
					$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
					\App\Model\Notification::create([
						'title'=>config('message.profile_notification_approved_title'),
						'message'=>str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
						'receiver'=>$doctor->user_id,
						'type'=>0,
						'status'=>0
					]);
					# Sending user notification
					$notifications= $notifications->send([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'tickerText'	=> 'Medrator',
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon'
					]);
					/**
					 * Sending admin notification.
					 */
					$this->helper->sendAdminNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
					]);
					/**
					 * Sending moderator notification.
					 */
					$this->helper->sendModeratorNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_approved_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_approved_message')),
					]);
				}	
                return back()->with(['status'=>1,'message'=>'Approved successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>6]);
        }
    }
    public function profileDeclined(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>5]);;
                } 
                $doctor = \App\Model\Doctor::find($request->input('id'));
                $doctor->status = 0;
                $doctor->save();
				###### Sending notification
				$user = \App\User::find($doctor->user_id);
				if($user){
					$deviceToken = $user->device_token;
					$gcmApiKey = config('constants.gcm_apikey');
					$deviceType = $user->device_type;
					$name = $doctor->name;
					$notifications = new \Vcareall\Admin\Helper($deviceToken,$gcmApiKey,$deviceType);
					\App\Model\Notification::create([
						'title'=>config('message.profile_notification_desclined_title'),
						'message'=>str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
						'receiver'=>$doctor->user_id,
						'type'=>0,
						'status'=>0
					]);
					# Sending user notification
					$notifications= $notifications->send([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'subtitle'	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'tickerText'	=> 'Medrator',
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon'
					]);
					/**
					 * Sending admin notification.
					 */
					$this->helper->sendAdminNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
					]);
					/**
					 * Sending moderator notification.
					 */
					$this->helper->sendModeratorNotification([
						'message' 	=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_title')),
						'title'		=> str_replace('[NAME]',$name,config('message.profile_notification_desclined_message')),
					]);
				}	
                return back()->with(['status'=>1,'message'=>'Declined successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>5]);
        }
    }
    public function uploadImage(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                    'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>7]);;
                } 
                $image ="";
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('uploaded/images/thumbnail');
                    $img = Image::make($image->getRealPath());
                    $img->resize(100, 100, function ($constraint) {
                            $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$input['imagename']);

                    $destinationPath = public_path('uploaded/images');
                    $image->move($destinationPath, $input['imagename']);
                    $image =url('/').'/public/uploaded/images/'.$input['imagename'];

                }
                $doctor = \App\Model\Doctor::find($request->input('id'));
                $doctor->image = $image;
                $doctor->save();
                /**
                 * Upddate solr index also
                 */
                $this->updateSolrIndex([
                        'id' =>$doctor->id,
                        'image' => $doctor->image
                ]);
                return back()->with(['status'=>1,'message'=>'Uploaded successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>7]);
        }
    }
    private function updateSolrIndex($data){
        $update = $this->client->createUpdate();
        $doc1 = $update->createDocument();	
        $entity_id = 'doctor_'.$data['id'];
        $doc1->setKey('entity_id', $entity_id);		
        if(array_key_exists("name",$data)){ 
            $doc1->setField('name', $data['name']);
            $doc1->setFieldModifier('name', 'set');
        }
        if(array_key_exists("image",$data)){
            $doc1->setField('image', $data['image']);
            $doc1->setFieldModifier('image', 'set');
        }
        $update->addDocuments(array($doc1));
        $update->addCommit();	
        $result = $this->client->update($update);
        return $result;
    }
    public function deleteImage(Request $request){
        try{
            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>8]);;
                } 
                $doctor = \App\Model\Doctor::find($request->input('id'));
                $doctor->image = "";
                $doctor->save();
                return back()->with(['status'=>1,'message'=>'Image deleted successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>8]);
        }
    }
    public function update(Request $request){
        try{
            if ($request->isMethod('post')) { 
                $validator = Validator::make($request->all(), [
                    'id' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>9]);;
                } 
                $doctor = \App\Model\Doctor::find($request->input('id'));
                $doctor->name = $request->input('name');
                $doctor->gender = $request->input('gender');
                $doctor->mobile = $request->input('mobile');
                $doctor->experience = $request->input('experience');
                $doctor->recognition = $request->input('recognition');
                $doctor->about = $request->input('about');
                $doctor->address_one = $request->input('address_one');
                $doctor->latitude = $request->input('latitude');
                $doctor->longitude = $request->input('longitude');
                $doctor->locality = $request->input('locality');
                $doctor->city_name = $request->input('city_name');
                $doctor->state_name = $request->input('state_name');
                $doctor->country_name = $request->input('country_name');
                $doctor->save();
                $doctor->qualifications()->detach();
                $doctor->qualifications()->attach($request->input('qualification'), [
                    'college' => $request->input('college'),
                    'year' => $request->input('year'),
                ]);
                return back()->with(['status'=>1,'message'=>'Profile updated successfully.']);
            } 
        }
        catch(\Illuminate\Database\QueryException $e){
            $response= array('status'=>0, 'message'=>$e->getMessage()); 
            return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>9]);
        }
    }
    public function delete($id){
        if(!$id){
            abort(404);
        }
        $doctor = \App\Model\Doctor::find($id);
        if($doctor->user_id){
            $user = \App\User::find($doctor->user_id);
            $user->delete();
            $user->roles()->detach();
            //$user->roles()->attach(2);
        }
        $doctor->qualifications()->detach();
        $doctor->hospitals()->detach();
        $doctor->services()->delete();
        $doctor->specialties()->delete();
        $doctor->reviews()->delete();
		$doctor->schedular()->delete();
        $doctor->delete();
        return back()
            ->with(['status'=>1,'message'=>'Deleted successfully.']);
    }
    public function create(){
        $years = [];
        for($i = date("Y"); $i > 1950; $i--){
            $years[$i]= $i;
        }
        $result['specialties'] =  Specialty::lists('name','id');
        $result['qualifications'] = \App\Model\Qualifications::lists('name','id');
        $result['services'] =  Service::lists('name','id');
        $result['colleges'] = ['' => 'Select Colege'] + \App\Model\College::lists('name','id')->all();	
        $result['years']  = $years;
        return view('admin::doctor.create',$result);
    }
    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'degree' => 'required',
            'college' => 'required',
            'year' => 'required',
            'specialties' => 'required',
            'gender' => 'required',
            'address_one' => 'required'           
        ]);
        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator)
                ->with(['status'=>0,'message'=>'Failed ! Please try again.']);;
        }
        try{
            $doctor = Doctor::updateOrCreate(
            ['id' => $request->id],
            [
                'name'=>($request->name?$request->name:''),
                'mobile'=>($request->mobile?$request->mobile:''),
                'dob'=>($request->dob?$request->dob:''),
                'city_name' => ($request->city_name?$request->city_name:''),
                'state_name'=> ($request->state_name?$request->state_name:''),
                'country_name' => ($request->country_name?$request->country_name:''),
                'address_one' => ($request->address_one?$request->address_one:''),
                'locality' => ($request->locality?$request->locality:''),
                'latitude' => ($request->latitude?$request->latitude:''),
                'longitude' => ($request->longitude?$request->longitude:''),
                'reg_no' => ($request->reg_no?$request->reg_no:''),
                'awards' => ($request->awards?$request->awards:''),
                'recognition' => ($request->recognition?$request->recognition:''),
                'experience' => ($request->experience?$request->experience:0),
                'gender' => ($request->gender?$request->gender:0),
            ]);
            ## Adding doctor specialties	
            $doctor->specialtyDetail()->where('doctor_id',$request->doctor_id)->delete();
            foreach($request->specialties as $specialty){
                    $doctor->specialtyDetail()->create([
                            'speciality_id'=>$specialty
                    ]);
            }			
            ## Adding qualification
            $doctor->qualifications()->detach();
            foreach($request->degree as $index=>$degree){ 
                $college=$request->college; 
                $year = $request->year;
                $collegeName = \App\Model\College::find($college[$index])->name;
                $doctor->qualifications()->attach($degree, ['college'=>$collegeName,'college_id'=>$college[$index],'year'=>$year[$index]]);	

            } 
            /**
             * Adding services
             */
            $doctor->services()->delete();
            if($request->services){
                $services =[];
                foreach($request->services as $var){
                     $services[] = array('service_id'=>$var);
                }
                $doctor->services()->insert($services);
            }
            /****
             @ claiming profile
            */
            $doctor->save();
            /****
            @ Sending create profile mail to administrator
            */
            $data = [
                'entity_name'=> $doctor->name
            ];
            Mail::send('email.new-profile-admin',['data'=>$data], function ($message) use ($data) {
                    $message->to(env('ADMIN_EMAIL'))->subject('New profile added.');
            });
            return redirect('admin/doctors')->with(['status'=>1,'message'=>'Added successfully.']);
        }catch(\Illuminate\Database\QueryException $e){
            return back()->with(['status'=>0,'message'=>$e->getMessage()]);
        }
    }
}
