<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vcareall\Admin\Http\Controllers;

/**
 * Description of SettingController
 *
 * @author VCareAll
 */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\Service;
use App\Model\Specialty;
use Illuminate\Support\Facades\Validator;
use Image;
class SettingController extends Controller {
   public function radius(Request $request){
       if ($request->isMethod('post')) {
           try{
                $validator = Validator::make($request->all(), [
                    'radius' => 'required',
                ]);
                if ($validator->fails()) {
                    return back()
                        ->withInput()
                        ->withErrors($validator)
                        ->with(['status'=>0,'message'=>'Failed.','type'=>0]);;
                }
                $settings = \App\Model\Setting::find(1);
                $settings->value = $request->input('radius');
                $settings->save();
                return back()->with(['status'=>1,'message'=>'Updated successfully.']);
            }
            catch(\Illuminate\Database\QueryException $e){
                $response= array('status'=>0, 'message'=>$e->getMessage()); 
                return back()->with(['status'=>0,'message'=>$e->getMessage(),'type'=>1]);
            }
        } 
        $settings = \App\Model\Setting::findOrFail(1);
        $result['radius'] =$settings->value;
        return  view('admin::setting.radius',$result);
   }
}
