<?php
namespace Vcareall\Admin;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminServiceProvider
 *
 * @author BALESHAWR
 */
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Auth;
class AdminServiceProvider extends ServiceProvider {
    
    protected $defer = false;
    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__.'/views'), 'admin');
        $this->setupRoutes($this->app->router);
        // this  for conig
        $this->publishes([
            __DIR__.'/config/admn.php' => config_path('admn.php'),
        ]);
		

    }
    public function register()
    {
        $this->registerAdmin();
        config([
            'config/admin.php',
        ]);
    }
    public function setupRoutes(Router $router)
    {
        $router->group(['prefix' => 'admin', 'namespace' => 'Vcareall\Admin\Http\Controllers'], function($router)
        {
                require __DIR__.'/Http/routes.php';
        });
    }
    private function registerAdmin()
    {
        $this->app->bind('admin',function($app){
                return new Contact($app);
        });
    }
    private function registerBala()
    {
        $this->app->bind('bala',function($app){
                return new Contact($app);
        });
    }
}
